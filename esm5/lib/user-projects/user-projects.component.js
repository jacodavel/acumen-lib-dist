/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ActorService } from '../services/actor.service';
import { ProjectService } from '../services/project.service';
import { AcumenConfiguration } from '../services/acumen-configuration';
var UserProjectsComponent = /** @class */ (function () {
    function UserProjectsComponent(actorService, projectService, acumenConfiguration) {
        this.actorService = actorService;
        this.projectService = projectService;
        this.acumenConfiguration = acumenConfiguration;
        this.onProjectSelected = new EventEmitter();
        this.projects = [];
        this.loading = false;
        if (acumenConfiguration.backendServiceUrl) {
            this.serverUrl = acumenConfiguration.backendServiceUrl;
        }
        else {
            this.serverUrl = "http://www.healthacumen.co.za/insight/";
        }
    }
    /**
     * @return {?}
     */
    UserProjectsComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.loading = true;
        if (this.username) {
            this.actorService.getSystemUser(this.username).then((/**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                if (result) {
                    _this.userId = result.id;
                    _this.actorId = result.actor.id;
                }
                _this.refreshProjects();
            })).catch((/**
             * @param {?} error
             * @return {?}
             */
            function (error) {
                _this.loading = false;
            }));
        }
        else {
            this.refreshProjects();
        }
    };
    /**
     * @return {?}
     */
    UserProjectsComponent.prototype.refreshProjects = /**
     * @return {?}
     */
    function () {
        var _this = this;
        // if (this.userId) {
        //   this.projectService.getUserProjects(this.userId).then(result => {
        //     this.projects = result;
        //     this.loading = false;
        //   }).catch(error => {
        //     this.loading = false;
        //   });
        // } else {
        //   this.loading = false;
        // }
        if (this.actorId) {
            this.projects = [];
            this.projectService.getProjects(this.actorId).then((/**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                var e_1, _a;
                /** @type {?} */
                var projectIds = {};
                try {
                    for (var result_1 = tslib_1.__values(result), result_1_1 = result_1.next(); !result_1_1.done; result_1_1 = result_1.next()) {
                        var project = result_1_1.value;
                        if (!projectIds[project.id]) {
                            _this.projects.push(project);
                            projectIds[project.id] = project;
                        }
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (result_1_1 && !result_1_1.done && (_a = result_1.return)) _a.call(result_1);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
                _this.loading = false;
            })).catch((/**
             * @param {?} error
             * @return {?}
             */
            function (error) {
                _this.loading = false;
            }));
        }
        else {
            this.loading = false;
        }
    };
    /**
     * @param {?} project
     * @return {?}
     */
    UserProjectsComponent.prototype.projectSelected = /**
     * @param {?} project
     * @return {?}
     */
    function (project) {
        this.onProjectSelected.emit(project);
    };
    UserProjectsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'acn-user-projects',
                    template: "<div class=\"user-projects-component\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n\n  <div *ngIf=\"projects\" class=\"project-list\">\n    <div *ngFor=\"let project of projects; odd as isOdd\" class=\"project-row\" [ngClass]=\"{ 'oddRow': isOdd }\" (click)=\"projectSelected(project)\">\n      <div class=\"project-image\">\n        <img src=\"{{ serverUrl }}spring/projectIcon/image.png?id={{ project.id }}\">\n      </div>\n      <div class=\"description\">\n        {{ project.projectName }}\n\n      </div>\n      <div class=\"cursor-image\">\n        <img src=\"data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAUVJREFUeJzt2EtOw0AURNELbCw7o9hZdgaTtBigBLv9+vM+JfUscnyPZw220+OknIDvx9HSN1kw8RufDkH8jU+DIJ7Hh0cQ/8eHRRDH48MhiPPxYRBEf7x7BHE93i2CsIt3hyDs47dBeF/8/58sRvg48Js78AbcBr3D7fH8+6Dnv9wRAAiMcBQAgiKcAYCACGcBIBhCDwAEQugFgCAIVwAgAMJVAHCOYAEAjhGsAMApgiUAOESwBgBnCCMAwBHCKABwgjASABwgjAaAzRFmAMDGCLMAYFOE1XeCqSbG3S5vccP8aqLiK77ijY9mhfRMVHzFV7zx0ayQnomKr/iKNz6aFdIzUfEVX/      HGR7NCeiYqPmb86jvBLzb/+m0i2JfvmUgc3yYSx7eJxPFtInF8m0gc3yYSx7eJxPFtInF8m0gc3yacxf8AAP323bWbSe8AAAAASUVORK5CYII=\">\n      </div>\n    </div>\n  </div>\n</div>\n",
                    styles: [".user-projects-component{width:100%;height:100%;position:relative}.user-projects-component .project-list{width:100%;height:100%;overflow-y:scroll}.user-projects-component .project-list .project-row{display:table;width:100%;padding:5px;cursor:pointer}.user-projects-component .project-list .project-row .project-image{display:table-cell;width:32px;text-align:center;vertical-align:middle}.user-projects-component .project-list .project-row .project-image img{width:24px;height:24px}.user-projects-component .project-list .project-row .description{display:table-cell;width:calc(100% - 64px);padding-left:10px;padding-top:3px;font-size:14px;vertical-align:middle}.user-projects-component .project-list .project-row .cursor-image{display:table-cell;width:32px;text-align:center;vertical-align:middle}.user-projects-component .project-list .project-row .cursor-image img{width:16px;height:16px}.user-projects-component .project-list .oddRow{background-color:#f2f2f2}"]
                }] }
    ];
    /** @nocollapse */
    UserProjectsComponent.ctorParameters = function () { return [
        { type: ActorService },
        { type: ProjectService },
        { type: AcumenConfiguration }
    ]; };
    UserProjectsComponent.propDecorators = {
        username: [{ type: Input }],
        actorId: [{ type: Input }],
        userId: [{ type: Input }],
        onProjectSelected: [{ type: Output }]
    };
    return UserProjectsComponent;
}());
export { UserProjectsComponent };
if (false) {
    /** @type {?} */
    UserProjectsComponent.prototype.username;
    /** @type {?} */
    UserProjectsComponent.prototype.actorId;
    /** @type {?} */
    UserProjectsComponent.prototype.userId;
    /** @type {?} */
    UserProjectsComponent.prototype.onProjectSelected;
    /** @type {?} */
    UserProjectsComponent.prototype.serverUrl;
    /** @type {?} */
    UserProjectsComponent.prototype.projects;
    /** @type {?} */
    UserProjectsComponent.prototype.loading;
    /**
     * @type {?}
     * @private
     */
    UserProjectsComponent.prototype.actorService;
    /**
     * @type {?}
     * @private
     */
    UserProjectsComponent.prototype.projectService;
    /**
     * @type {?}
     * @private
     */
    UserProjectsComponent.prototype.acumenConfiguration;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci1wcm9qZWN0cy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL3VzZXItcHJvamVjdHMvdXNlci1wcm9qZWN0cy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRS9FLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUN6RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFFN0QsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFFdkU7SUFlRSwrQkFBb0IsWUFBMEIsRUFBVSxjQUE4QixFQUMxRSxtQkFBd0M7UUFEaEMsaUJBQVksR0FBWixZQUFZLENBQWM7UUFBVSxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDMUUsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQVAxQyxzQkFBaUIsR0FBRyxJQUFJLFlBQVksRUFBVyxDQUFDO1FBRzFELGFBQVEsR0FBbUIsRUFBRSxDQUFDO1FBQzlCLFlBQU8sR0FBWSxLQUFLLENBQUM7UUFJdkIsSUFBSSxtQkFBbUIsQ0FBQyxpQkFBaUIsRUFBRTtZQUN6QyxJQUFJLENBQUMsU0FBUyxHQUFHLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDO1NBQ3hEO2FBQU07WUFDTCxJQUFJLENBQUMsU0FBUyxHQUFHLHdDQUF3QyxDQUFDO1NBQzNEO0lBQ0gsQ0FBQzs7OztJQUVELHdDQUFROzs7SUFBUjtRQUFBLGlCQWdCQztRQWZDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQixJQUFJLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSTs7OztZQUFDLFVBQUEsTUFBTTtnQkFDeEQsSUFBSSxNQUFNLEVBQUU7b0JBQ1YsS0FBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDO29CQUN4QixLQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDO2lCQUNoQztnQkFFRCxLQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDekIsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztZQUFDLFVBQUEsS0FBSztnQkFDWixLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztZQUN2QixDQUFDLEVBQUMsQ0FBQztTQUNKO2FBQU07WUFDTCxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7U0FDeEI7SUFDSCxDQUFDOzs7O0lBRUQsK0NBQWU7OztJQUFmO1FBQUEsaUJBNEJDO1FBM0JDLHFCQUFxQjtRQUNyQixzRUFBc0U7UUFDdEUsOEJBQThCO1FBQzlCLDRCQUE0QjtRQUM1Qix3QkFBd0I7UUFDeEIsNEJBQTRCO1FBQzVCLFFBQVE7UUFDUixXQUFXO1FBQ1gsMEJBQTBCO1FBQzFCLElBQUk7UUFDSixJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDaEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7WUFDbkIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUk7Ozs7WUFBQyxVQUFBLE1BQU07OztvQkFDbkQsVUFBVSxHQUFHLEVBQUU7O29CQUNuQixLQUFvQixJQUFBLFdBQUEsaUJBQUEsTUFBTSxDQUFBLDhCQUFBLGtEQUFFO3dCQUF2QixJQUFJLE9BQU8sbUJBQUE7d0JBQ2QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLEVBQUU7NEJBQzNCLEtBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDOzRCQUM1QixVQUFVLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQzt5QkFDbEM7cUJBQ0Y7Ozs7Ozs7OztnQkFDRCxLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztZQUN2QixDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1lBQUMsVUFBQSxLQUFLO2dCQUNaLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQ3ZCLENBQUMsRUFBQyxDQUFDO1NBQ0o7YUFBTTtZQUNMLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1NBQ3RCO0lBQ0gsQ0FBQzs7Ozs7SUFFRCwrQ0FBZTs7OztJQUFmLFVBQWdCLE9BQWdCO1FBQzlCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDdkMsQ0FBQzs7Z0JBMUVGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsbUJBQW1CO29CQUM3QixxdENBQTZDOztpQkFFOUM7Ozs7Z0JBVFEsWUFBWTtnQkFDWixjQUFjO2dCQUVkLG1CQUFtQjs7OzJCQVF6QixLQUFLOzBCQUNMLEtBQUs7eUJBQ0wsS0FBSztvQ0FDTCxNQUFNOztJQWtFVCw0QkFBQztDQUFBLEFBM0VELElBMkVDO1NBdEVZLHFCQUFxQjs7O0lBQ2hDLHlDQUEwQjs7SUFDMUIsd0NBQXlCOztJQUN6Qix1Q0FBd0I7O0lBQ3hCLGtEQUEwRDs7SUFFMUQsMENBQVU7O0lBQ1YseUNBQThCOztJQUM5Qix3Q0FBeUI7Ozs7O0lBRWIsNkNBQWtDOzs7OztJQUFFLCtDQUFzQzs7Ozs7SUFDbEYsb0RBQWdEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBBY3RvclNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9hY3Rvci5zZXJ2aWNlJztcbmltcG9ydCB7IFByb2plY3RTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvcHJvamVjdC5zZXJ2aWNlJztcbmltcG9ydCB7IFByb2plY3QgfSBmcm9tICcuLi9kb21haW4vcHJvamVjdCc7XG5pbXBvcnQgeyBBY3VtZW5Db25maWd1cmF0aW9uIH0gZnJvbSAnLi4vc2VydmljZXMvYWN1bWVuLWNvbmZpZ3VyYXRpb24nO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhY24tdXNlci1wcm9qZWN0cycsXG4gIHRlbXBsYXRlVXJsOiAnLi91c2VyLXByb2plY3RzLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vdXNlci1wcm9qZWN0cy5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIFVzZXJQcm9qZWN0c0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIEBJbnB1dCgpIHVzZXJuYW1lOiBzdHJpbmc7XG4gIEBJbnB1dCgpIGFjdG9ySWQ6IG51bWJlcjtcbiAgQElucHV0KCkgdXNlcklkOiBudW1iZXI7XG4gIEBPdXRwdXQoKSBvblByb2plY3RTZWxlY3RlZCA9IG5ldyBFdmVudEVtaXR0ZXI8UHJvamVjdD4oKTtcbiAgLy8gc2VydmVyVXJsID0gJ2h0dHA6Ly93d3cuaGVhbHRoYWN1bWVuLmNvLnphL2luc2lnaHQvJztcbiAgc2VydmVyVXJsO1xuICBwcm9qZWN0czogQXJyYXk8UHJvamVjdD4gPSBbXTtcbiAgbG9hZGluZzogYm9vbGVhbiA9IGZhbHNlO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgYWN0b3JTZXJ2aWNlOiBBY3RvclNlcnZpY2UsIHByaXZhdGUgcHJvamVjdFNlcnZpY2U6IFByb2plY3RTZXJ2aWNlLFxuICAgICAgcHJpdmF0ZSBhY3VtZW5Db25maWd1cmF0aW9uOiBBY3VtZW5Db25maWd1cmF0aW9uKSB7XG4gICAgaWYgKGFjdW1lbkNvbmZpZ3VyYXRpb24uYmFja2VuZFNlcnZpY2VVcmwpIHtcbiAgICAgIHRoaXMuc2VydmVyVXJsID0gYWN1bWVuQ29uZmlndXJhdGlvbi5iYWNrZW5kU2VydmljZVVybDtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zZXJ2ZXJVcmwgPSBcImh0dHA6Ly93d3cuaGVhbHRoYWN1bWVuLmNvLnphL2luc2lnaHQvXCI7XG4gICAgfVxuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcbiAgICBpZiAodGhpcy51c2VybmFtZSkge1xuICAgICAgdGhpcy5hY3RvclNlcnZpY2UuZ2V0U3lzdGVtVXNlcih0aGlzLnVzZXJuYW1lKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICAgIGlmIChyZXN1bHQpIHtcbiAgICAgICAgICB0aGlzLnVzZXJJZCA9IHJlc3VsdC5pZDtcbiAgICAgICAgICB0aGlzLmFjdG9ySWQgPSByZXN1bHQuYWN0b3IuaWQ7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnJlZnJlc2hQcm9qZWN0cygpO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnJlZnJlc2hQcm9qZWN0cygpO1xuICAgIH1cbiAgfVxuXG4gIHJlZnJlc2hQcm9qZWN0cygpIHtcbiAgICAvLyBpZiAodGhpcy51c2VySWQpIHtcbiAgICAvLyAgIHRoaXMucHJvamVjdFNlcnZpY2UuZ2V0VXNlclByb2plY3RzKHRoaXMudXNlcklkKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgLy8gICAgIHRoaXMucHJvamVjdHMgPSByZXN1bHQ7XG4gICAgLy8gICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgIC8vICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgIC8vICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAvLyAgIH0pO1xuICAgIC8vIH0gZWxzZSB7XG4gICAgLy8gICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAvLyB9XG4gICAgaWYgKHRoaXMuYWN0b3JJZCkge1xuICAgICAgdGhpcy5wcm9qZWN0cyA9IFtdO1xuICAgICAgdGhpcy5wcm9qZWN0U2VydmljZS5nZXRQcm9qZWN0cyh0aGlzLmFjdG9ySWQpLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgICAgbGV0IHByb2plY3RJZHMgPSB7fTtcbiAgICAgICAgZm9yIChsZXQgcHJvamVjdCBvZiByZXN1bHQpIHtcbiAgICAgICAgICBpZiAoIXByb2plY3RJZHNbcHJvamVjdC5pZF0pIHtcbiAgICAgICAgICAgIHRoaXMucHJvamVjdHMucHVzaChwcm9qZWN0KTtcbiAgICAgICAgICAgIHByb2plY3RJZHNbcHJvamVjdC5pZF0gPSBwcm9qZWN0O1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgfVxuICB9XG5cbiAgcHJvamVjdFNlbGVjdGVkKHByb2plY3Q6IFByb2plY3QpIHtcbiAgICB0aGlzLm9uUHJvamVjdFNlbGVjdGVkLmVtaXQocHJvamVjdCk7XG4gIH1cbn1cbiJdfQ==