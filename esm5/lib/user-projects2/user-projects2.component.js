/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ActorService } from '../services/actor.service';
import { ProjectService } from '../services/project.service';
import { AcumenConfiguration } from '../services/acumen-configuration';
var UserProjects2Component = /** @class */ (function () {
    function UserProjects2Component(actorService, projectService, acumenConfiguration) {
        this.actorService = actorService;
        this.projectService = projectService;
        this.acumenConfiguration = acumenConfiguration;
        this.onProjectsReceived = new EventEmitter();
        if (acumenConfiguration.backendServiceUrl) {
            this.serverUrl = acumenConfiguration.backendServiceUrl;
        }
        else {
            this.serverUrl = "http://www.healthacumen.co.za/insight/";
        }
    }
    /**
     * @return {?}
     */
    UserProjects2Component.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.username) {
            this.actorService.getSystemUser(this.username).then((/**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                if (result) {
                    _this.userId = result.id;
                    _this.actorId = result.actor.id;
                }
                _this.refreshProjects();
            })).catch((/**
             * @param {?} error
             * @return {?}
             */
            function (error) {
                console.log(error);
            }));
        }
        else {
            this.refreshProjects();
        }
    };
    /**
     * @return {?}
     */
    UserProjects2Component.prototype.refreshProjects = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.actorId) {
            /** @type {?} */
            var projects_1 = [];
            this.projectService.getProjects(this.actorId).then((/**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                var e_1, _a;
                /** @type {?} */
                var projectIds = {};
                try {
                    for (var result_1 = tslib_1.__values(result), result_1_1 = result_1.next(); !result_1_1.done; result_1_1 = result_1.next()) {
                        var project = result_1_1.value;
                        if (!projectIds[project.id]) {
                            project.iconUrl = _this.serverUrl + "spring/projectIcon/image.png?id=" + project.id;
                            projects_1.push(project);
                            projectIds[project.id] = project;
                        }
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (result_1_1 && !result_1_1.done && (_a = result_1.return)) _a.call(result_1);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
                _this.onProjectsReceived.emit(projects_1);
            })).catch((/**
             * @param {?} error
             * @return {?}
             */
            function (error) {
                console.error(error);
            }));
        }
        else {
            this.onProjectsReceived.emit([]);
        }
    };
    UserProjects2Component.decorators = [
        { type: Component, args: [{
                    selector: 'acn-user-projects2',
                    template: "<div class=\"user-projects2-component\">\n  <ng-content></ng-content>\n</div>\n",
                    styles: [".user-projects2-component{width:100%;height:100%;position:relative}"]
                }] }
    ];
    /** @nocollapse */
    UserProjects2Component.ctorParameters = function () { return [
        { type: ActorService },
        { type: ProjectService },
        { type: AcumenConfiguration }
    ]; };
    UserProjects2Component.propDecorators = {
        username: [{ type: Input }],
        onProjectsReceived: [{ type: Output }]
    };
    return UserProjects2Component;
}());
export { UserProjects2Component };
if (false) {
    /** @type {?} */
    UserProjects2Component.prototype.username;
    /** @type {?} */
    UserProjects2Component.prototype.onProjectsReceived;
    /**
     * @type {?}
     * @private
     */
    UserProjects2Component.prototype.actorId;
    /**
     * @type {?}
     * @private
     */
    UserProjects2Component.prototype.userId;
    /**
     * @type {?}
     * @private
     */
    UserProjects2Component.prototype.serverUrl;
    /**
     * @type {?}
     * @private
     */
    UserProjects2Component.prototype.actorService;
    /**
     * @type {?}
     * @private
     */
    UserProjects2Component.prototype.projectService;
    /**
     * @type {?}
     * @private
     */
    UserProjects2Component.prototype.acumenConfiguration;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci1wcm9qZWN0czIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWN1bWVuLWxpYi8iLCJzb3VyY2VzIjpbImxpYi91c2VyLXByb2plY3RzMi91c2VyLXByb2plY3RzMi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRS9FLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUN6RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFFN0QsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFFdkU7SUFZRSxnQ0FBb0IsWUFBMEIsRUFBVSxjQUE4QixFQUMxRSxtQkFBd0M7UUFEaEMsaUJBQVksR0FBWixZQUFZLENBQWM7UUFBVSxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDMUUsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQU4xQyx1QkFBa0IsR0FBRyxJQUFJLFlBQVksRUFBa0IsQ0FBQztRQU9oRSxJQUFJLG1CQUFtQixDQUFDLGlCQUFpQixFQUFFO1lBQ3pDLElBQUksQ0FBQyxTQUFTLEdBQUcsbUJBQW1CLENBQUMsaUJBQWlCLENBQUM7U0FDeEQ7YUFBTTtZQUNMLElBQUksQ0FBQyxTQUFTLEdBQUcsd0NBQXdDLENBQUM7U0FDM0Q7SUFDSCxDQUFDOzs7O0lBRUQseUNBQVE7OztJQUFSO1FBQUEsaUJBZUM7UUFkQyxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakIsSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUk7Ozs7WUFBQyxVQUFBLE1BQU07Z0JBQ3hELElBQUksTUFBTSxFQUFFO29CQUNWLEtBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLEVBQUUsQ0FBQztvQkFDeEIsS0FBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQztpQkFDaEM7Z0JBRUQsS0FBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1lBQ3pCLENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7WUFBQyxVQUFBLEtBQUs7Z0JBQ1osT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNyQixDQUFDLEVBQUMsQ0FBQztTQUNKO2FBQU07WUFDTCxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7U0FDeEI7SUFDSCxDQUFDOzs7O0lBRUQsZ0RBQWU7OztJQUFmO1FBQUEsaUJBb0JDO1FBbkJDLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTs7Z0JBQ1osVUFBUSxHQUFtQixFQUFFO1lBQ2pDLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJOzs7O1lBQUMsVUFBQSxNQUFNOzs7b0JBQ25ELFVBQVUsR0FBRyxFQUFFOztvQkFDbkIsS0FBb0IsSUFBQSxXQUFBLGlCQUFBLE1BQU0sQ0FBQSw4QkFBQSxrREFBRTt3QkFBdkIsSUFBSSxPQUFPLG1CQUFBO3dCQUNkLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxFQUFFOzRCQUMzQixPQUFPLENBQUMsT0FBTyxHQUFHLEtBQUksQ0FBQyxTQUFTLEdBQUcsa0NBQWtDLEdBQUcsT0FBTyxDQUFDLEVBQUUsQ0FBQzs0QkFDbkYsVUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzs0QkFDdkIsVUFBVSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUM7eUJBQ2xDO3FCQUNGOzs7Ozs7Ozs7Z0JBRUQsS0FBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxVQUFRLENBQUMsQ0FBQztZQUN6QyxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1lBQUMsVUFBQSxLQUFLO2dCQUNaLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDdkIsQ0FBQyxFQUFDLENBQUM7U0FDSjthQUFNO1lBQ0wsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUNsQztJQUNILENBQUM7O2dCQTFERixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtvQkFDOUIsMkZBQThDOztpQkFFL0M7Ozs7Z0JBVFEsWUFBWTtnQkFDWixjQUFjO2dCQUVkLG1CQUFtQjs7OzJCQVF6QixLQUFLO3FDQUNMLE1BQU07O0lBb0RULDZCQUFDO0NBQUEsQUEzREQsSUEyREM7U0F0RFksc0JBQXNCOzs7SUFDakMsMENBQTBCOztJQUMxQixvREFBa0U7Ozs7O0lBQ2xFLHlDQUF3Qjs7Ozs7SUFDeEIsd0NBQXVCOzs7OztJQUN2QiwyQ0FBMEI7Ozs7O0lBRWQsOENBQWtDOzs7OztJQUFFLGdEQUFzQzs7Ozs7SUFDbEYscURBQWdEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBBY3RvclNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9hY3Rvci5zZXJ2aWNlJztcbmltcG9ydCB7IFByb2plY3RTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvcHJvamVjdC5zZXJ2aWNlJztcbmltcG9ydCB7IFByb2plY3QgfSBmcm9tICcuLi9kb21haW4vcHJvamVjdCc7XG5pbXBvcnQgeyBBY3VtZW5Db25maWd1cmF0aW9uIH0gZnJvbSAnLi4vc2VydmljZXMvYWN1bWVuLWNvbmZpZ3VyYXRpb24nO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhY24tdXNlci1wcm9qZWN0czInLFxuICB0ZW1wbGF0ZVVybDogJy4vdXNlci1wcm9qZWN0czIuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi91c2VyLXByb2plY3RzMi5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIFVzZXJQcm9qZWN0czJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBASW5wdXQoKSB1c2VybmFtZTogc3RyaW5nO1xuICBAT3V0cHV0KCkgb25Qcm9qZWN0c1JlY2VpdmVkID0gbmV3IEV2ZW50RW1pdHRlcjxBcnJheTxQcm9qZWN0Pj4oKTtcbiAgcHJpdmF0ZSBhY3RvcklkOiBudW1iZXI7XG4gIHByaXZhdGUgdXNlcklkOiBudW1iZXI7XG4gIHByaXZhdGUgc2VydmVyVXJsOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBhY3RvclNlcnZpY2U6IEFjdG9yU2VydmljZSwgcHJpdmF0ZSBwcm9qZWN0U2VydmljZTogUHJvamVjdFNlcnZpY2UsXG4gICAgICBwcml2YXRlIGFjdW1lbkNvbmZpZ3VyYXRpb246IEFjdW1lbkNvbmZpZ3VyYXRpb24pIHtcbiAgICBpZiAoYWN1bWVuQ29uZmlndXJhdGlvbi5iYWNrZW5kU2VydmljZVVybCkge1xuICAgICAgdGhpcy5zZXJ2ZXJVcmwgPSBhY3VtZW5Db25maWd1cmF0aW9uLmJhY2tlbmRTZXJ2aWNlVXJsO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNlcnZlclVybCA9IFwiaHR0cDovL3d3dy5oZWFsdGhhY3VtZW4uY28uemEvaW5zaWdodC9cIjtcbiAgICB9XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICBpZiAodGhpcy51c2VybmFtZSkge1xuICAgICAgdGhpcy5hY3RvclNlcnZpY2UuZ2V0U3lzdGVtVXNlcih0aGlzLnVzZXJuYW1lKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICAgIGlmIChyZXN1bHQpIHtcbiAgICAgICAgICB0aGlzLnVzZXJJZCA9IHJlc3VsdC5pZDtcbiAgICAgICAgICB0aGlzLmFjdG9ySWQgPSByZXN1bHQuYWN0b3IuaWQ7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnJlZnJlc2hQcm9qZWN0cygpO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5yZWZyZXNoUHJvamVjdHMoKTtcbiAgICB9XG4gIH1cblxuICByZWZyZXNoUHJvamVjdHMoKSB7XG4gICAgaWYgKHRoaXMuYWN0b3JJZCkge1xuICAgICAgbGV0IHByb2plY3RzOiBBcnJheTxQcm9qZWN0PiA9IFtdO1xuICAgICAgdGhpcy5wcm9qZWN0U2VydmljZS5nZXRQcm9qZWN0cyh0aGlzLmFjdG9ySWQpLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgICAgbGV0IHByb2plY3RJZHMgPSB7fTtcbiAgICAgICAgZm9yIChsZXQgcHJvamVjdCBvZiByZXN1bHQpIHtcbiAgICAgICAgICBpZiAoIXByb2plY3RJZHNbcHJvamVjdC5pZF0pIHtcbiAgICAgICAgICAgIHByb2plY3QuaWNvblVybCA9IHRoaXMuc2VydmVyVXJsICsgXCJzcHJpbmcvcHJvamVjdEljb24vaW1hZ2UucG5nP2lkPVwiICsgcHJvamVjdC5pZDtcbiAgICAgICAgICAgIHByb2plY3RzLnB1c2gocHJvamVjdCk7XG4gICAgICAgICAgICBwcm9qZWN0SWRzW3Byb2plY3QuaWRdID0gcHJvamVjdDtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLm9uUHJvamVjdHNSZWNlaXZlZC5lbWl0KHByb2plY3RzKTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5vblByb2plY3RzUmVjZWl2ZWQuZW1pdChbXSk7XG4gICAgfVxuICB9XG59XG4iXX0=