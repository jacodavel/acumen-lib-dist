/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { AcumenConfiguration } from '../services/acumen-configuration';
var MaintenanceService = /** @class */ (function () {
    // private backendServiceUrl = 'http://www.healthacumen.co.za/insight/';
    // private backendServiceUrl = 'https://frank.rmsui.co.za/insight/';
    // private backendServiceUrl = 'http://localhost:8888/';
    function MaintenanceService(httpClient, acumenConfiguration) {
        this.httpClient = httpClient;
        this.acumenConfiguration = acumenConfiguration;
        if (acumenConfiguration.backendServiceUrl) {
            this.backendServiceUrl = acumenConfiguration.backendServiceUrl;
        }
        else {
            this.backendServiceUrl = "https://frank.rmsui.co.za/insight/";
        }
    }
    // getTitles(): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/titles`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    //
    // getEmployers(): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/employers`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    //
    // getWorksites(): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/worksites`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       let workSites = response;
    //       for (let i = 0; i < workSites.length; i++) {
    //           workSites[i].defaultOption = workSites[i]["defaultWorkSite"];
    //       }
    //
    //       return workSites;
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    //   }
    //
    // getBuildings(employerId: number): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/buildings/${employerId}`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    //
    // getMedicalAids(): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/medicalaids`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    //
    // getSubSites(workSiteId: number): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/subSites/${workSiteId}`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    // getTitles(): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/titles`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    //
    // getEmployers(): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/employers`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    //
    // getWorksites(): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/worksites`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       let workSites = response;
    //       for (let i = 0; i < workSites.length; i++) {
    //           workSites[i].defaultOption = workSites[i]["defaultWorkSite"];
    //       }
    //
    //       return workSites;
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    //   }
    //
    // getBuildings(employerId: number): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/buildings/${employerId}`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    //
    // getMedicalAids(): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/medicalaids`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    //
    // getSubSites(workSiteId: number): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/subSites/${workSiteId}`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    /**
     * @param {?} actorId
     * @return {?}
     */
    MaintenanceService.prototype.getActorFields = 
    // getTitles(): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/titles`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    //
    // getEmployers(): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/employers`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    //
    // getWorksites(): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/worksites`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       let workSites = response;
    //       for (let i = 0; i < workSites.length; i++) {
    //           workSites[i].defaultOption = workSites[i]["defaultWorkSite"];
    //       }
    //
    //       return workSites;
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    //   }
    //
    // getBuildings(employerId: number): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/buildings/${employerId}`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    //
    // getMedicalAids(): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/medicalaids`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    //
    // getSubSites(workSiteId: number): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/subSites/${workSiteId}`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    /**
     * @param {?} actorId
     * @return {?}
     */
    function (actorId) {
        /** @type {?} */
        var url = this.backendServiceUrl + ("rest/maintenance/actorType/actorFields/" + actorId);
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} actorFieldId
     * @return {?}
     */
    MaintenanceService.prototype.getActorFieldOptions = /**
     * @param {?} actorFieldId
     * @return {?}
     */
    function (actorFieldId) {
        /** @type {?} */
        var url = this.backendServiceUrl + ("rest/maintenance/actorType/actorFields/actorFieldOptions/" + actorFieldId);
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    MaintenanceService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    MaintenanceService.ctorParameters = function () { return [
        { type: HttpClient },
        { type: AcumenConfiguration }
    ]; };
    return MaintenanceService;
}());
export { MaintenanceService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    MaintenanceService.prototype.backendServiceUrl;
    /**
     * @type {?}
     * @private
     */
    MaintenanceService.prototype.httpClient;
    /**
     * @type {?}
     * @private
     */
    MaintenanceService.prototype.acumenConfiguration;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbnRlbmFuY2Uuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvbWFpbnRlbmFuY2Uuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFTLGVBQWUsQ0FBQztBQUM5QyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbEQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBS25ELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBRXZFO0lBR0Usd0VBQXdFO0lBQ3hFLG9FQUFvRTtJQUNwRSx3REFBd0Q7SUFFeEQsNEJBQW9CLFVBQXNCLEVBQVUsbUJBQXdDO1FBQXhFLGVBQVUsR0FBVixVQUFVLENBQVk7UUFBVSx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQzFGLElBQUksbUJBQW1CLENBQUMsaUJBQWlCLEVBQUU7WUFDekMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDO1NBQ2hFO2FBQU07WUFDTCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsb0NBQW9DLENBQUM7U0FDL0Q7SUFDSCxDQUFDO0lBRUQsNENBQTRDO0lBQzVDLGtFQUFrRTtJQUNsRSxzQ0FBc0M7SUFDdEMsb0NBQW9DO0lBQ3BDLFFBQVE7SUFDUixFQUFFO0lBQ0YsdUZBQXVGO0lBQ3ZGLDBCQUEwQjtJQUMxQiwrREFBK0Q7SUFDL0QsMEJBQTBCO0lBQzFCLHNDQUFzQztJQUN0QyxVQUFVO0lBQ1YsSUFBSTtJQUNKLEVBQUU7SUFDRiwrQ0FBK0M7SUFDL0MscUVBQXFFO0lBQ3JFLHNDQUFzQztJQUN0QyxvQ0FBb0M7SUFDcEMsUUFBUTtJQUNSLEVBQUU7SUFDRix1RkFBdUY7SUFDdkYsMEJBQTBCO0lBQzFCLCtEQUErRDtJQUMvRCwwQkFBMEI7SUFDMUIsc0NBQXNDO0lBQ3RDLFVBQVU7SUFDVixJQUFJO0lBQ0osRUFBRTtJQUNGLCtDQUErQztJQUMvQyxxRUFBcUU7SUFDckUsc0NBQXNDO0lBQ3RDLG9DQUFvQztJQUNwQyxRQUFRO0lBQ1IsRUFBRTtJQUNGLHVGQUF1RjtJQUN2RiwwQkFBMEI7SUFDMUIsa0NBQWtDO0lBQ2xDLHFEQUFxRDtJQUNyRCwwRUFBMEU7SUFDMUUsVUFBVTtJQUNWLEVBQUU7SUFDRiwwQkFBMEI7SUFDMUIsMEJBQTBCO0lBQzFCLHNDQUFzQztJQUN0QyxVQUFVO0lBQ1YsTUFBTTtJQUNOLEVBQUU7SUFDRixpRUFBaUU7SUFDakUsbUZBQW1GO0lBQ25GLHNDQUFzQztJQUN0QyxvQ0FBb0M7SUFDcEMsUUFBUTtJQUNSLEVBQUU7SUFDRix1RkFBdUY7SUFDdkYsMEJBQTBCO0lBQzFCLCtEQUErRDtJQUMvRCwwQkFBMEI7SUFDMUIsc0NBQXNDO0lBQ3RDLFVBQVU7SUFDVixJQUFJO0lBQ0osRUFBRTtJQUNGLGlEQUFpRDtJQUNqRCx1RUFBdUU7SUFDdkUsc0NBQXNDO0lBQ3RDLG9DQUFvQztJQUNwQyxRQUFRO0lBQ1IsRUFBRTtJQUNGLHVGQUF1RjtJQUN2RiwwQkFBMEI7SUFDMUIsK0RBQStEO0lBQy9ELDBCQUEwQjtJQUMxQixzQ0FBc0M7SUFDdEMsVUFBVTtJQUNWLElBQUk7SUFDSixFQUFFO0lBQ0YsZ0VBQWdFO0lBQ2hFLGtGQUFrRjtJQUNsRixzQ0FBc0M7SUFDdEMsb0NBQW9DO0lBQ3BDLFFBQVE7SUFDUixFQUFFO0lBQ0YsdUZBQXVGO0lBQ3ZGLDBCQUEwQjtJQUMxQiwrREFBK0Q7SUFDL0QsMEJBQTBCO0lBQzFCLHNDQUFzQztJQUN0QyxVQUFVO0lBQ1YsSUFBSTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBRUosMkNBQWM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQUFkLFVBQWUsT0FBZTs7WUFDeEIsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsSUFBRyw0Q0FBMEMsT0FBUyxDQUFBOztZQUNoRixPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDOUIsUUFBUSxFQUFFLGtCQUFrQjtTQUM3QixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBZSxHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDMUUsSUFBSTs7OztRQUFDLFVBQUEsUUFBUTtZQUNaLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQWdCLENBQUMsQ0FBQztRQUNuRCxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsVUFBQSxLQUFLO1lBQ1osT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCxpREFBb0I7Ozs7SUFBcEIsVUFBcUIsWUFBb0I7O1lBQ25DLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLElBQUcsOERBQTRELFlBQWMsQ0FBQTs7WUFDdkcsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzlCLFFBQVEsRUFBRSxrQkFBa0I7U0FDN0IsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQXFCLEdBQUcsRUFBRSxFQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRTthQUNoRixJQUFJOzs7O1FBQUMsVUFBQSxRQUFRO1lBQ1osT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLG1CQUFBLFFBQVEsRUFBc0IsQ0FBQyxDQUFDO1FBQ3pELENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxVQUFBLEtBQUs7WUFDWixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOztnQkFsSUYsVUFBVTs7OztnQkFSRixVQUFVO2dCQU1WLG1CQUFtQjs7SUFxSTVCLHlCQUFDO0NBQUEsQUFuSUQsSUFtSUM7U0FsSVksa0JBQWtCOzs7Ozs7SUFDN0IsK0NBQTBCOzs7OztJQUtkLHdDQUE4Qjs7Ozs7SUFBRSxpREFBZ0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gICAgZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgSHR0cEhlYWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5cbi8vIGltcG9ydCB7IFNlbGVjdGlvbk9wdGlvbiB9IGZyb20gJy4uL2RvbWFpbi9zZWxlY3Rpb24tb3B0aW9uJztcbmltcG9ydCB7IEFjdG9yRmllbGQgfSBmcm9tICcuLi9kb21haW4vYWN0b3ItZmllbGQnO1xuaW1wb3J0IHsgQWN0b3JGaWVsZE9wdGlvbiB9IGZyb20gJy4uL2RvbWFpbi9hY3Rvci1maWVsZC1vcHRpb24nO1xuaW1wb3J0IHsgQWN1bWVuQ29uZmlndXJhdGlvbiB9IGZyb20gJy4uL3NlcnZpY2VzL2FjdW1lbi1jb25maWd1cmF0aW9uJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIE1haW50ZW5hbmNlU2VydmljZSB7XG4gIHByaXZhdGUgYmFja2VuZFNlcnZpY2VVcmw7XG4gIC8vIHByaXZhdGUgYmFja2VuZFNlcnZpY2VVcmwgPSAnaHR0cDovL3d3dy5oZWFsdGhhY3VtZW4uY28uemEvaW5zaWdodC8nO1xuICAvLyBwcml2YXRlIGJhY2tlbmRTZXJ2aWNlVXJsID0gJ2h0dHBzOi8vZnJhbmsucm1zdWkuY28uemEvaW5zaWdodC8nO1xuICAvLyBwcml2YXRlIGJhY2tlbmRTZXJ2aWNlVXJsID0gJ2h0dHA6Ly9sb2NhbGhvc3Q6ODg4OC8nO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cENsaWVudDogSHR0cENsaWVudCwgcHJpdmF0ZSBhY3VtZW5Db25maWd1cmF0aW9uOiBBY3VtZW5Db25maWd1cmF0aW9uKSB7XG4gICAgaWYgKGFjdW1lbkNvbmZpZ3VyYXRpb24uYmFja2VuZFNlcnZpY2VVcmwpIHtcbiAgICAgIHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgPSBhY3VtZW5Db25maWd1cmF0aW9uLmJhY2tlbmRTZXJ2aWNlVXJsO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsID0gXCJodHRwczovL2ZyYW5rLnJtc3VpLmNvLnphL2luc2lnaHQvXCI7XG4gICAgfVxuICB9XG5cbiAgLy8gZ2V0VGl0bGVzKCk6IFByb21pc2U8U2VsZWN0aW9uT3B0aW9uW10+IHtcbiAgLy8gICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybCArIGByZXN0L21haW50ZW5hbmNlL3RpdGxlc2A7XG4gIC8vICAgY29uc3QgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gIC8vICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAvLyAgIH0pO1xuICAvL1xuICAvLyAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PFNlbGVjdGlvbk9wdGlvbltdPih1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgLy8gICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgLy8gICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBTZWxlY3Rpb25PcHRpb25bXSk7XG4gIC8vICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gIC8vICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gIC8vICAgICB9KTtcbiAgLy8gfVxuICAvL1xuICAvLyBnZXRFbXBsb3llcnMoKTogUHJvbWlzZTxTZWxlY3Rpb25PcHRpb25bXT4ge1xuICAvLyAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgYHJlc3QvbWFpbnRlbmFuY2UvZW1wbG95ZXJzYDtcbiAgLy8gICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgLy8gICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gIC8vICAgfSk7XG4gIC8vXG4gIC8vICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8U2VsZWN0aW9uT3B0aW9uW10+KHVybCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAvLyAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAvLyAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIFNlbGVjdGlvbk9wdGlvbltdKTtcbiAgLy8gICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgLy8gICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgLy8gICAgIH0pO1xuICAvLyB9XG4gIC8vXG4gIC8vIGdldFdvcmtzaXRlcygpOiBQcm9taXNlPFNlbGVjdGlvbk9wdGlvbltdPiB7XG4gIC8vICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBgcmVzdC9tYWludGVuYW5jZS93b3Jrc2l0ZXNgO1xuICAvLyAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAvLyAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgLy8gICB9KTtcbiAgLy9cbiAgLy8gICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldDxTZWxlY3Rpb25PcHRpb25bXT4odXJsLCB7aGVhZGVyczogaGVhZGVyc30pLnRvUHJvbWlzZSgpXG4gIC8vICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gIC8vICAgICAgIGxldCB3b3JrU2l0ZXMgPSByZXNwb25zZTtcbiAgLy8gICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB3b3JrU2l0ZXMubGVuZ3RoOyBpKyspIHtcbiAgLy8gICAgICAgICAgIHdvcmtTaXRlc1tpXS5kZWZhdWx0T3B0aW9uID0gd29ya1NpdGVzW2ldW1wiZGVmYXVsdFdvcmtTaXRlXCJdO1xuICAvLyAgICAgICB9XG4gIC8vXG4gIC8vICAgICAgIHJldHVybiB3b3JrU2l0ZXM7XG4gIC8vICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gIC8vICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gIC8vICAgICB9KTtcbiAgLy8gICB9XG4gIC8vXG4gIC8vIGdldEJ1aWxkaW5ncyhlbXBsb3llcklkOiBudW1iZXIpOiBQcm9taXNlPFNlbGVjdGlvbk9wdGlvbltdPiB7XG4gIC8vICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBgcmVzdC9tYWludGVuYW5jZS9idWlsZGluZ3MvJHtlbXBsb3llcklkfWA7XG4gIC8vICAgY29uc3QgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gIC8vICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAvLyAgIH0pO1xuICAvL1xuICAvLyAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PFNlbGVjdGlvbk9wdGlvbltdPih1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgLy8gICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgLy8gICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBTZWxlY3Rpb25PcHRpb25bXSk7XG4gIC8vICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gIC8vICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gIC8vICAgICB9KTtcbiAgLy8gfVxuICAvL1xuICAvLyBnZXRNZWRpY2FsQWlkcygpOiBQcm9taXNlPFNlbGVjdGlvbk9wdGlvbltdPiB7XG4gIC8vICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBgcmVzdC9tYWludGVuYW5jZS9tZWRpY2FsYWlkc2A7XG4gIC8vICAgY29uc3QgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gIC8vICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAvLyAgIH0pO1xuICAvL1xuICAvLyAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PFNlbGVjdGlvbk9wdGlvbltdPih1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgLy8gICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgLy8gICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBTZWxlY3Rpb25PcHRpb25bXSk7XG4gIC8vICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gIC8vICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gIC8vICAgICB9KTtcbiAgLy8gfVxuICAvL1xuICAvLyBnZXRTdWJTaXRlcyh3b3JrU2l0ZUlkOiBudW1iZXIpOiBQcm9taXNlPFNlbGVjdGlvbk9wdGlvbltdPiB7XG4gIC8vICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBgcmVzdC9tYWludGVuYW5jZS9zdWJTaXRlcy8ke3dvcmtTaXRlSWR9YDtcbiAgLy8gICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgLy8gICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gIC8vICAgfSk7XG4gIC8vXG4gIC8vICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8U2VsZWN0aW9uT3B0aW9uW10+KHVybCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAvLyAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAvLyAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIFNlbGVjdGlvbk9wdGlvbltdKTtcbiAgLy8gICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgLy8gICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgLy8gICAgIH0pO1xuICAvLyB9XG5cbiAgZ2V0QWN0b3JGaWVsZHMoYWN0b3JJZDogbnVtYmVyKTogUHJvbWlzZTxBY3RvckZpZWxkW10+IHtcbiAgICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybCArIGByZXN0L21haW50ZW5hbmNlL2FjdG9yVHlwZS9hY3RvckZpZWxkcy8ke2FjdG9ySWR9YDtcbiAgICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldDxBY3RvckZpZWxkW10+KHVybCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIEFjdG9yRmllbGRbXSk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIGdldEFjdG9yRmllbGRPcHRpb25zKGFjdG9yRmllbGRJZDogbnVtYmVyKTogUHJvbWlzZTxBY3RvckZpZWxkT3B0aW9uW10+IHtcbiAgICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybCArIGByZXN0L21haW50ZW5hbmNlL2FjdG9yVHlwZS9hY3RvckZpZWxkcy9hY3RvckZpZWxkT3B0aW9ucy8ke2FjdG9yRmllbGRJZH1gO1xuICAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICB9KTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PEFjdG9yRmllbGRPcHRpb25bXT4odXJsLCB7aGVhZGVyczogaGVhZGVyc30pLnRvUHJvbWlzZSgpXG4gICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgQWN0b3JGaWVsZE9wdGlvbltdKTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG59XG4iXX0=