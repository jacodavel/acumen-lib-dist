/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { AcumenConfiguration } from '../services/acumen-configuration';
var ActorService = /** @class */ (function () {
    // private backendServiceUrl = 'http://www.healthacumen.co.za/insight/';
    // private backendServiceUrl = 'https://frank.rmsui.co.za/insight/';
    // private backendServiceUrl = 'http://localhost:8888/';
    function ActorService(httpClient, acumenConfiguration) {
        this.httpClient = httpClient;
        this.acumenConfiguration = acumenConfiguration;
        if (acumenConfiguration.backendServiceUrl) {
            this.backendServiceUrl = acumenConfiguration.backendServiceUrl;
        }
        else {
            this.backendServiceUrl = "https://frank.rmsui.co.za/insight/";
        }
    }
    /**
     * @param {?} username
     * @return {?}
     */
    ActorService.prototype.getSystemUser = /**
     * @param {?} username
     * @return {?}
     */
    function (username) {
        /** @type {?} */
        var url = this.backendServiceUrl + "rest/actor/systemUser/" + username;
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} externalActorId
     * @return {?}
     */
    ActorService.prototype.getActorFromExternalId = /**
     * @param {?} externalActorId
     * @return {?}
     */
    function (externalActorId) {
        /** @type {?} */
        var url = this.backendServiceUrl + "rest/actor/externalId/" + externalActorId;
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} externalActorId
     * @param {?} projectId
     * @return {?}
     */
    ActorService.prototype.getProjectActorFromExternalId = /**
     * @param {?} externalActorId
     * @param {?} projectId
     * @return {?}
     */
    function (externalActorId, projectId) {
        /** @type {?} */
        var url = this.backendServiceUrl + "rest/actor/projectParticipant/" + externalActorId + "/" + projectId;
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} actorId
     * @param {?} projectId
     * @return {?}
     */
    ActorService.prototype.getProjectActor = /**
     * @param {?} actorId
     * @param {?} projectId
     * @return {?}
     */
    function (actorId, projectId) {
        /** @type {?} */
        var url = this.backendServiceUrl + "rest/project/projectActor/" + projectId + "/" + actorId;
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} actorId
     * @return {?}
     */
    ActorService.prototype.getActorFieldValues = /**
     * @param {?} actorId
     * @return {?}
     */
    function (actorId) {
        /** @type {?} */
        var url = this.backendServiceUrl + ("rest/actor/actorType/actorFields/actorFieldValues/" + actorId);
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} actorId
     * @param {?} actorFieldValues
     * @return {?}
     */
    ActorService.prototype.updateActorFieldValues = /**
     * @param {?} actorId
     * @param {?} actorFieldValues
     * @return {?}
     */
    function (actorId, actorFieldValues) {
        /** @type {?} */
        var url = this.backendServiceUrl + "rest/actor/actorType/actorFields/actorFieldValues/update";
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json'
        });
        /** @type {?} */
        var parameters = {
            actorId: actorId,
            actorFieldValues: actorFieldValues
        };
        return this.httpClient.post(url, parameters, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    ActorService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    ActorService.ctorParameters = function () { return [
        { type: HttpClient },
        { type: AcumenConfiguration }
    ]; };
    return ActorService;
}());
export { ActorService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    ActorService.prototype.backendServiceUrl;
    /**
     * @type {?}
     * @private
     */
    ActorService.prototype.httpClient;
    /**
     * @type {?}
     * @private
     */
    ActorService.prototype.acumenConfiguration;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWN0b3Iuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvYWN0b3Iuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFTLGVBQWUsQ0FBQztBQUM5QyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbEQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBTW5ELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBRXZFO0lBR0Usd0VBQXdFO0lBQ3hFLG9FQUFvRTtJQUNwRSx3REFBd0Q7SUFFeEQsc0JBQW9CLFVBQXNCLEVBQVUsbUJBQXdDO1FBQXhFLGVBQVUsR0FBVixVQUFVLENBQVk7UUFBVSx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQzFGLElBQUksbUJBQW1CLENBQUMsaUJBQWlCLEVBQUU7WUFDekMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDO1NBQ2hFO2FBQU07WUFDTCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsb0NBQW9DLENBQUM7U0FDL0Q7SUFDSCxDQUFDOzs7OztJQUVELG9DQUFhOzs7O0lBQWIsVUFBYyxRQUFnQjs7WUFDeEIsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyx3QkFBd0IsR0FBRyxRQUFROztZQUNoRSxPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDOUIsUUFBUSxFQUFFLGtCQUFrQjtTQUM3QixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBYSxHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDeEUsSUFBSTs7OztRQUFDLFVBQUEsUUFBUTtZQUNaLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQWMsQ0FBQyxDQUFDO1FBQ2pELENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxVQUFBLEtBQUs7WUFDWixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVELDZDQUFzQjs7OztJQUF0QixVQUF1QixlQUF1Qjs7WUFDeEMsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyx3QkFBd0IsR0FBRyxlQUFlOztZQUN2RSxPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDOUIsUUFBUSxFQUFFLGtCQUFrQjtTQUM3QixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBUSxHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDbkUsSUFBSTs7OztRQUFDLFVBQUEsUUFBUTtZQUNaLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQVMsQ0FBQyxDQUFDO1FBQzVDLENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxVQUFBLEtBQUs7WUFDWixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7SUFFRCxvREFBNkI7Ozs7O0lBQTdCLFVBQThCLGVBQXVCLEVBQUUsU0FBaUI7O1lBQ2xFLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsZ0NBQWdDLEdBQUcsZUFBZSxHQUFHLEdBQUcsR0FBRyxTQUFTOztZQUNqRyxPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDOUIsUUFBUSxFQUFFLGtCQUFrQjtTQUM3QixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBUSxHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDbkUsSUFBSTs7OztRQUFDLFVBQUEsUUFBUTtZQUNaLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQVMsQ0FBQyxDQUFDO1FBQzVDLENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxVQUFBLEtBQUs7WUFDWixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7SUFFRCxzQ0FBZTs7Ozs7SUFBZixVQUFnQixPQUFlLEVBQUUsU0FBaUI7O1lBQzVDLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsNEJBQTRCLEdBQUcsU0FBUyxHQUFHLEdBQUcsR0FBRyxPQUFPOztZQUNyRixPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDOUIsUUFBUSxFQUFFLGtCQUFrQjtTQUM3QixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBZSxHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDMUUsSUFBSTs7OztRQUFDLFVBQUEsUUFBUTtZQUNaLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQWdCLENBQUMsQ0FBQztRQUNuRCxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsVUFBQSxLQUFLO1lBQ1osT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCwwQ0FBbUI7Ozs7SUFBbkIsVUFBb0IsT0FBZTs7WUFDN0IsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsSUFBRyx1REFBcUQsT0FBUyxDQUFBOztZQUMzRixPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDOUIsUUFBUSxFQUFFLGtCQUFrQjtTQUM3QixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBb0IsR0FBRyxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQy9FLElBQUk7Ozs7UUFBQyxVQUFBLFFBQVE7WUFDWixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFxQixDQUFDLENBQUM7UUFDeEQsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLFVBQUEsS0FBSztZQUNaLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7OztJQUVELDZDQUFzQjs7Ozs7SUFBdEIsVUFBdUIsT0FBZSxFQUFFLGdCQUF3Qzs7WUFDMUUsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRywwREFBMEQ7O1lBQ3pGLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQztZQUM1QixRQUFRLEVBQUUsa0JBQWtCO1NBQzdCLENBQUM7O1lBRUUsVUFBVSxHQUFHO1lBQ2YsT0FBTyxFQUFFLE9BQU87WUFDaEIsZ0JBQWdCLEVBQUUsZ0JBQWdCO1NBQ25DO1FBRUQsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsVUFBVSxFQUFFLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQzNFLElBQUk7Ozs7UUFBQyxVQUFBLFFBQVE7WUFDWixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFVLENBQUMsQ0FBQztRQUM3QyxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsVUFBQSxLQUFLO1lBQ1osT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Z0JBdEdGLFVBQVU7Ozs7Z0JBVEYsVUFBVTtnQkFPVixtQkFBbUI7O0lBeUc1QixtQkFBQztDQUFBLEFBdkdELElBdUdDO1NBdEdZLFlBQVk7Ozs7OztJQUN2Qix5Q0FBMEI7Ozs7O0lBS2Qsa0NBQThCOzs7OztJQUFFLDJDQUFnRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSAgICBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBIdHRwSGVhZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcblxuaW1wb3J0IHsgU3lzdGVtVXNlciB9IGZyb20gJy4uL2RvbWFpbi9zeXN0ZW0tdXNlcic7XG5pbXBvcnQgeyBBY3RvciB9IGZyb20gJy4uL2RvbWFpbi9hY3Rvcic7XG5pbXBvcnQgeyBQcm9qZWN0QWN0b3IgfSBmcm9tICcuLi9kb21haW4vcHJvamVjdC1hY3Rvcic7XG5pbXBvcnQgeyBBY3RvckZpZWxkVmFsdWUgfSBmcm9tICcuLi9kb21haW4vYWN0b3ItZmllbGQtdmFsdWUnO1xuaW1wb3J0IHsgQWN1bWVuQ29uZmlndXJhdGlvbiB9IGZyb20gJy4uL3NlcnZpY2VzL2FjdW1lbi1jb25maWd1cmF0aW9uJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEFjdG9yU2VydmljZSB7XG4gIHByaXZhdGUgYmFja2VuZFNlcnZpY2VVcmw7XG4gIC8vIHByaXZhdGUgYmFja2VuZFNlcnZpY2VVcmwgPSAnaHR0cDovL3d3dy5oZWFsdGhhY3VtZW4uY28uemEvaW5zaWdodC8nO1xuICAvLyBwcml2YXRlIGJhY2tlbmRTZXJ2aWNlVXJsID0gJ2h0dHBzOi8vZnJhbmsucm1zdWkuY28uemEvaW5zaWdodC8nO1xuICAvLyBwcml2YXRlIGJhY2tlbmRTZXJ2aWNlVXJsID0gJ2h0dHA6Ly9sb2NhbGhvc3Q6ODg4OC8nO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cENsaWVudDogSHR0cENsaWVudCwgcHJpdmF0ZSBhY3VtZW5Db25maWd1cmF0aW9uOiBBY3VtZW5Db25maWd1cmF0aW9uKSB7XG4gICAgaWYgKGFjdW1lbkNvbmZpZ3VyYXRpb24uYmFja2VuZFNlcnZpY2VVcmwpIHtcbiAgICAgIHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgPSBhY3VtZW5Db25maWd1cmF0aW9uLmJhY2tlbmRTZXJ2aWNlVXJsO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsID0gXCJodHRwczovL2ZyYW5rLnJtc3VpLmNvLnphL2luc2lnaHQvXCI7XG4gICAgfVxuICB9XG5cbiAgZ2V0U3lzdGVtVXNlcih1c2VybmFtZTogc3RyaW5nKTogUHJvbWlzZTxTeXN0ZW1Vc2VyPiB7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBcInJlc3QvYWN0b3Ivc3lzdGVtVXNlci9cIiArIHVzZXJuYW1lO1xuICAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICB9KTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PFN5c3RlbVVzZXI+KHVybCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIFN5c3RlbVVzZXIpO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxuICBnZXRBY3RvckZyb21FeHRlcm5hbElkKGV4dGVybmFsQWN0b3JJZDogc3RyaW5nKTogUHJvbWlzZTxBY3Rvcj4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgXCJyZXN0L2FjdG9yL2V4dGVybmFsSWQvXCIgKyBleHRlcm5hbEFjdG9ySWQ7XG4gICAgY29uc3QgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8QWN0b3I+KHVybCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIEFjdG9yKTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgZ2V0UHJvamVjdEFjdG9yRnJvbUV4dGVybmFsSWQoZXh0ZXJuYWxBY3RvcklkOiBzdHJpbmcsIHByb2plY3RJZDogbnVtYmVyKTogUHJvbWlzZTxBY3Rvcj4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgXCJyZXN0L2FjdG9yL3Byb2plY3RQYXJ0aWNpcGFudC9cIiArIGV4dGVybmFsQWN0b3JJZCArIFwiL1wiICsgcHJvamVjdElkO1xuICAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICB9KTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PEFjdG9yPih1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBBY3Rvcik7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIGdldFByb2plY3RBY3RvcihhY3RvcklkOiBudW1iZXIsIHByb2plY3RJZDogbnVtYmVyKTogUHJvbWlzZTxQcm9qZWN0QWN0b3I+IHtcbiAgICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybCArIFwicmVzdC9wcm9qZWN0L3Byb2plY3RBY3Rvci9cIiArIHByb2plY3RJZCArIFwiL1wiICsgYWN0b3JJZDtcbiAgICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldDxQcm9qZWN0QWN0b3I+KHVybCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIFByb2plY3RBY3Rvcik7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIGdldEFjdG9yRmllbGRWYWx1ZXMoYWN0b3JJZDogbnVtYmVyKTogUHJvbWlzZTxBY3RvckZpZWxkVmFsdWVbXT4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgYHJlc3QvYWN0b3IvYWN0b3JUeXBlL2FjdG9yRmllbGRzL2FjdG9yRmllbGRWYWx1ZXMvJHthY3RvcklkfWA7XG4gICAgY29uc3QgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8QWN0b3JGaWVsZFZhbHVlW10+KHVybCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIEFjdG9yRmllbGRWYWx1ZVtdKTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgdXBkYXRlQWN0b3JGaWVsZFZhbHVlcyhhY3RvcklkOiBudW1iZXIsIGFjdG9yRmllbGRWYWx1ZXM6IEFycmF5PEFjdG9yRmllbGRWYWx1ZT4pOiBQcm9taXNlPGFueT4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgYHJlc3QvYWN0b3IvYWN0b3JUeXBlL2FjdG9yRmllbGRzL2FjdG9yRmllbGRWYWx1ZXMvdXBkYXRlYDtcbiAgICBsZXQgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nXG4gICAgfSk7XG5cbiAgICBsZXQgcGFyYW1ldGVycyA9IHtcbiAgICAgIGFjdG9ySWQ6IGFjdG9ySWQsXG4gICAgICBhY3RvckZpZWxkVmFsdWVzOiBhY3RvckZpZWxkVmFsdWVzXG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5wb3N0KHVybCwgcGFyYW1ldGVycywgeyBoZWFkZXJzOiBoZWFkZXJzIH0pLnRvUHJvbWlzZSgpXG4gICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgc3RyaW5nKTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG59XG4iXX0=