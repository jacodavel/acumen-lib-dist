/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { AcumenConfiguration } from '../services/acumen-configuration';
var AnalysisService = /** @class */ (function () {
    // private backendServiceUrl = 'http://www.healthacumen.co.za/insight/';
    // private backendServiceUrl = 'https://frank.rmsui.co.za/insight/';
    // private backendServiceUrl = 'http://localhost:8888/';
    function AnalysisService(httpClient, acumenConfiguration) {
        this.httpClient = httpClient;
        this.acumenConfiguration = acumenConfiguration;
        if (acumenConfiguration.backendServiceUrl) {
            this.backendServiceUrl = acumenConfiguration.backendServiceUrl;
        }
        else {
            this.backendServiceUrl = "https://frank.rmsui.co.za/insight/";
        }
    }
    /**
     * @param {?} projectGuid
     * @return {?}
     */
    AnalysisService.prototype.getProjectCompletionCount = /**
     * @param {?} projectGuid
     * @return {?}
     */
    function (projectGuid) {
        /** @type {?} */
        var url = this.backendServiceUrl + ("countserv?pr=" + projectGuid);
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @return {?}
     */
    AnalysisService.prototype.getWellnessData = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var url = this.backendServiceUrl + "welldashdplot?actor=359817&project=123";
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    AnalysisService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    AnalysisService.ctorParameters = function () { return [
        { type: HttpClient },
        { type: AcumenConfiguration }
    ]; };
    return AnalysisService;
}());
export { AnalysisService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    AnalysisService.prototype.backendServiceUrl;
    /**
     * @type {?}
     * @private
     */
    AnalysisService.prototype.httpClient;
    /**
     * @type {?}
     * @private
     */
    AnalysisService.prototype.acumenConfiguration;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5hbHlzaXMuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvYW5hbHlzaXMuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFTLGVBQWUsQ0FBQztBQUM5QyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbEQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBRW5ELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBRXZFO0lBR0Usd0VBQXdFO0lBQ3hFLG9FQUFvRTtJQUNwRSx3REFBd0Q7SUFFeEQseUJBQW9CLFVBQXNCLEVBQVUsbUJBQXdDO1FBQXhFLGVBQVUsR0FBVixVQUFVLENBQVk7UUFBVSx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQzFGLElBQUksbUJBQW1CLENBQUMsaUJBQWlCLEVBQUU7WUFDekMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDO1NBQ2hFO2FBQU07WUFDTCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsb0NBQW9DLENBQUM7U0FDL0Q7SUFDSCxDQUFDOzs7OztJQUVELG1EQUF5Qjs7OztJQUF6QixVQUEwQixXQUFtQjs7WUFDdkMsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsSUFBRyxrQkFBZ0IsV0FBYSxDQUFBOztZQUMxRCxPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDOUIsUUFBUSxFQUFFLGtCQUFrQjtTQUM3QixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDNUQsSUFBSTs7OztRQUFDLFVBQUEsUUFBUTtZQUNaLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQVUsQ0FBQyxDQUFDO1FBQzdDLENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxVQUFBLEtBQUs7WUFDWixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQseUNBQWU7OztJQUFmOztZQUNNLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsd0NBQXdDOztZQUNyRSxPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDOUIsUUFBUSxFQUFFLGtCQUFrQjtTQUM3QixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDNUQsSUFBSTs7OztRQUFDLFVBQUEsUUFBUTtZQUNaLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQVUsQ0FBQyxDQUFDO1FBQzdDLENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxVQUFBLEtBQUs7WUFDWixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOztnQkF6Q0YsVUFBVTs7OztnQkFMRixVQUFVO2dCQUdWLG1CQUFtQjs7SUE0QzVCLHNCQUFDO0NBQUEsQUExQ0QsSUEwQ0M7U0F6Q1ksZUFBZTs7Ozs7O0lBQzFCLDRDQUEwQjs7Ozs7SUFLZCxxQ0FBOEI7Ozs7O0lBQUUsOENBQWdEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9ICAgIGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IEh0dHBIZWFkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuXG5pbXBvcnQgeyBBY3VtZW5Db25maWd1cmF0aW9uIH0gZnJvbSAnLi4vc2VydmljZXMvYWN1bWVuLWNvbmZpZ3VyYXRpb24nO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgQW5hbHlzaXNTZXJ2aWNlIHtcbiAgcHJpdmF0ZSBiYWNrZW5kU2VydmljZVVybDtcbiAgLy8gcHJpdmF0ZSBiYWNrZW5kU2VydmljZVVybCA9ICdodHRwOi8vd3d3LmhlYWx0aGFjdW1lbi5jby56YS9pbnNpZ2h0Lyc7XG4gIC8vIHByaXZhdGUgYmFja2VuZFNlcnZpY2VVcmwgPSAnaHR0cHM6Ly9mcmFuay5ybXN1aS5jby56YS9pbnNpZ2h0Lyc7XG4gIC8vIHByaXZhdGUgYmFja2VuZFNlcnZpY2VVcmwgPSAnaHR0cDovL2xvY2FsaG9zdDo4ODg4Lyc7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwQ2xpZW50OiBIdHRwQ2xpZW50LCBwcml2YXRlIGFjdW1lbkNvbmZpZ3VyYXRpb246IEFjdW1lbkNvbmZpZ3VyYXRpb24pIHtcbiAgICBpZiAoYWN1bWVuQ29uZmlndXJhdGlvbi5iYWNrZW5kU2VydmljZVVybCkge1xuICAgICAgdGhpcy5iYWNrZW5kU2VydmljZVVybCA9IGFjdW1lbkNvbmZpZ3VyYXRpb24uYmFja2VuZFNlcnZpY2VVcmw7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgPSBcImh0dHBzOi8vZnJhbmsucm1zdWkuY28uemEvaW5zaWdodC9cIjtcbiAgICB9XG4gIH1cblxuICBnZXRQcm9qZWN0Q29tcGxldGlvbkNvdW50KHByb2plY3RHdWlkOiBzdHJpbmcpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgYGNvdW50c2Vydj9wcj0ke3Byb2plY3RHdWlkfWA7XG4gICAgY29uc3QgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQodXJsLCB7aGVhZGVyczogaGVhZGVyc30pLnRvUHJvbWlzZSgpXG4gICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgc3RyaW5nKTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgZ2V0V2VsbG5lc3NEYXRhKCk6IFByb21pc2U8YW55PiB7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBgd2VsbGRhc2hkcGxvdD9hY3Rvcj0zNTk4MTcmcHJvamVjdD0xMjNgO1xuICAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICB9KTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0KHVybCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIHN0cmluZyk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxufVxuIl19