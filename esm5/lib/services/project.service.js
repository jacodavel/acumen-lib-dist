/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { AcumenConfiguration } from '../services/acumen-configuration';
var ProjectService = /** @class */ (function () {
    // private backendServiceUrl = 'http://www.healthacumen.co.za/insight/';
    // private backendServiceUrl = 'https://frank.rmsui.co.za/insight/';
    // private backendServiceUrl = 'http://localhost:8888/';
    function ProjectService(httpClient, acumenConfiguration) {
        this.httpClient = httpClient;
        this.acumenConfiguration = acumenConfiguration;
        if (acumenConfiguration.backendServiceUrl) {
            this.backendServiceUrl = acumenConfiguration.backendServiceUrl;
        }
        else {
            this.backendServiceUrl = "https://frank.rmsui.co.za/insight/";
        }
    }
    /**
     * @param {?} projectGuid
     * @param {?} actorId
     * @return {?}
     */
    ProjectService.prototype.startWizardProject = /**
     * @param {?} projectGuid
     * @param {?} actorId
     * @return {?}
     */
    function (projectGuid, actorId) {
        /** @type {?} */
        var url = this.backendServiceUrl;
        if (actorId) {
            url += "rest/project/projects/start2/" + projectGuid + "/" + actorId;
        }
        else {
            url += "rest/project/projects/start/wizard/" + projectGuid;
        }
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} businessProcessInstanceId
     * @return {?}
     */
    ProjectService.prototype.getBusinessProcessState = /**
     * @param {?} businessProcessInstanceId
     * @return {?}
     */
    function (businessProcessInstanceId) {
        /** @type {?} */
        var url = this.backendServiceUrl + "rest/project/projects/state/" + businessProcessInstanceId + "?timestamp=" + new Date().getTime();
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} projectGuid
     * @return {?}
     */
    ProjectService.prototype.getProject = /**
     * @param {?} projectGuid
     * @return {?}
     */
    function (projectGuid) {
        /** @type {?} */
        var url = this.backendServiceUrl + ("rest/project/project/" + projectGuid);
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} actorId
     * @return {?}
     */
    ProjectService.prototype.getProjects = /**
     * @param {?} actorId
     * @return {?}
     */
    function (actorId) {
        /** @type {?} */
        var url = this.backendServiceUrl + ("rest/project/actorProjects/" + actorId);
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} systemUserId
     * @return {?}
     */
    ProjectService.prototype.getUserProjects = /**
     * @param {?} systemUserId
     * @return {?}
     */
    function (systemUserId) {
        /** @type {?} */
        var url = this.backendServiceUrl + ("rest/project/projects/" + systemUserId);
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} projectId
     * @return {?}
     */
    ProjectService.prototype.getProjectRoles = /**
     * @param {?} projectId
     * @return {?}
     */
    function (projectId) {
        /** @type {?} */
        var url = this.backendServiceUrl + ("rest/project/projectRoles/" + projectId);
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} fromRecord
     * @param {?} recordCount
     * @param {?} projectId
     * @param {?} projectRoleId
     * @param {?} firstname
     * @param {?} surname
     * @param {?} idNr
     * @param {?} memberNr
     * @param {?} dependantCode
     * @param {?} assigneeId
     * @return {?}
     */
    ProjectService.prototype.getProjectActors = /**
     * @param {?} fromRecord
     * @param {?} recordCount
     * @param {?} projectId
     * @param {?} projectRoleId
     * @param {?} firstname
     * @param {?} surname
     * @param {?} idNr
     * @param {?} memberNr
     * @param {?} dependantCode
     * @param {?} assigneeId
     * @return {?}
     */
    function (fromRecord, recordCount, projectId, projectRoleId, firstname, surname, idNr, memberNr, dependantCode, assigneeId) {
        if (!firstname)
            firstname = "null";
        if (!surname)
            surname = "null";
        if (!idNr)
            idNr = "null";
        if (!memberNr)
            memberNr = "null";
        if (!dependantCode)
            dependantCode = "null";
        // assigneeId = -1;
        /** @type {?} */
        var url = this.backendServiceUrl + ("rest/project/projectActors/" + fromRecord + "/" + recordCount + "/" + projectId + "/" + projectRoleId + "/" + firstname + "/" + surname + "/" + idNr + "/" + memberNr + "/" + dependantCode + "/" + assigneeId);
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} fromRecord
     * @param {?} recordCount
     * @param {?} projectId
     * @param {?} projectRoleId
     * @return {?}
     */
    ProjectService.prototype.getProjectActors2 = /**
     * @param {?} fromRecord
     * @param {?} recordCount
     * @param {?} projectId
     * @param {?} projectRoleId
     * @return {?}
     */
    function (fromRecord, recordCount, projectId, projectRoleId) {
        /** @type {?} */
        var url = this.backendServiceUrl + ("rest/project/projectActors2/" + fromRecord + "/" + recordCount + "/" + projectId + "/" + projectRoleId);
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} fromRecord
     * @param {?} recordCount
     * @param {?} projectRoleId
     * @param {?} searchField
     * @return {?}
     */
    ProjectService.prototype.getProjectActors3 = /**
     * @param {?} fromRecord
     * @param {?} recordCount
     * @param {?} projectRoleId
     * @param {?} searchField
     * @return {?}
     */
    function (fromRecord, recordCount, projectRoleId, searchField) {
        if (!searchField)
            searchField = "null";
        /** @type {?} */
        var url = this.backendServiceUrl + ("rest/project/projectActors3/" + fromRecord + "/" + recordCount + "/" + projectRoleId + "/" + searchField);
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} participantId
     * @return {?}
     */
    ProjectService.prototype.getStepInstances = /**
     * @param {?} participantId
     * @return {?}
     */
    function (participantId) {
        /** @type {?} */
        var url = this.backendServiceUrl + ("rest/project/stepInstances/" + participantId);
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} projectGuid
     * @param {?} userId
     * @param {?} assigneeId
     * @param {?} participantId
     * @return {?}
     */
    ProjectService.prototype.getStepInstances2 = /**
     * @param {?} projectGuid
     * @param {?} userId
     * @param {?} assigneeId
     * @param {?} participantId
     * @return {?}
     */
    function (projectGuid, userId, assigneeId, participantId) {
        if (!projectGuid) {
            projectGuid = "null";
        }
        if (!userId) {
            userId = -1;
        }
        if (!assigneeId) {
            assigneeId = -1;
        }
        if (!participantId) {
            participantId = -1;
        }
        /** @type {?} */
        var url = this.backendServiceUrl + ("rest/project/stepInstances/" + projectGuid + "/" + userId + "/" + assigneeId + "/" + participantId);
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} stepId
     * @return {?}
     */
    ProjectService.prototype.getStepParameters = /**
     * @param {?} stepId
     * @return {?}
     */
    function (stepId) {
        /** @type {?} */
        var url = this.backendServiceUrl + "rest/project/stepInstances/parameters/" + stepId;
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve(response);
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} actorId
     * @return {?}
     */
    ProjectService.prototype.getLatestStepAssignmentDate = /**
     * @param {?} actorId
     * @return {?}
     */
    function (actorId) {
        /** @type {?} */
        var url = this.backendServiceUrl + "rest/project/stepInstances/latestAssignment/" + actorId;
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} stepInstanceId
     * @param {?} systemUserId
     * @return {?}
     */
    ProjectService.prototype.completeStep = /**
     * @param {?} stepInstanceId
     * @param {?} systemUserId
     * @return {?}
     */
    function (stepInstanceId, systemUserId) {
        /** @type {?} */
        var url = this.backendServiceUrl;
        if (systemUserId)
            url += "rest/project/stepInstance/completeStep?stepInstanceId=" + stepInstanceId + "&systemUserId=" + systemUserId;
        else
            url += "rest/project/stepInstance/completeStep2?stepInstanceId=" + stepInstanceId;
        /** @type {?} */
        var headers = new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        return this.httpClient.post(url, JSON.stringify({})).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} stepInstanceId
     * @param {?} latitude
     * @param {?} longitude
     * @return {?}
     */
    ProjectService.prototype.updateStepLocation = /**
     * @param {?} stepInstanceId
     * @param {?} latitude
     * @param {?} longitude
     * @return {?}
     */
    function (stepInstanceId, latitude, longitude) {
        /** @type {?} */
        var url = this.backendServiceUrl;
        url += "rest/project/updateStepLocation?stepInstanceId=" + stepInstanceId + "&latitude=" + latitude + "&longitude=" + longitude;
        /** @type {?} */
        var headers = new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        return this.httpClient.post(url, JSON.stringify({})).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} stepInstanceId
     * @return {?}
     */
    ProjectService.prototype.getQuestionnaire = /**
     * @param {?} stepInstanceId
     * @return {?}
     */
    function (stepInstanceId) {
        /** @type {?} */
        var url = this.backendServiceUrl + ("rest/project/stepInstance/questionnaire/" + stepInstanceId);
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json'
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} stepInstanceId
     * @param {?} responses
     * @param {?} submit
     * @param {?} systemUserId
     * @return {?}
     */
    ProjectService.prototype.updateQuestionnaire = /**
     * @param {?} stepInstanceId
     * @param {?} responses
     * @param {?} submit
     * @param {?} systemUserId
     * @return {?}
     */
    function (stepInstanceId, responses, submit, systemUserId) {
        /** @type {?} */
        var url = this.backendServiceUrl + 'rest/project/stepInstance/questionnaire/responses';
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        /** @type {?} */
        var questionnaireResponse = {
            stepInstanceId: stepInstanceId,
            responses: responses,
            submit: submit,
            systemUserId: systemUserId
        };
        return this.httpClient.post(url, JSON.stringify(questionnaireResponse), { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} stepInstanceId
     * @return {?}
     */
    ProjectService.prototype.getMessageStepHtml = /**
     * @param {?} stepInstanceId
     * @return {?}
     */
    function (stepInstanceId) {
        /** @type {?} */
        var url = this.backendServiceUrl + ("rest/project/stepInstance/message/" + stepInstanceId);
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} businessProcessInstanceId
     * @param {?} idNr
     * @return {?}
     */
    ProjectService.prototype.verifyActorIdNr = /**
     * @param {?} businessProcessInstanceId
     * @param {?} idNr
     * @return {?}
     */
    function (businessProcessInstanceId, idNr) {
        /** @type {?} */
        var url = this.backendServiceUrl + 'rest/project/verifyActorIdNr';
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        /** @type {?} */
        var idNrLookUp = {
            businessProcessInstanceId: businessProcessInstanceId,
            idNr: idNr
        };
        return this.httpClient.post(url, idNrLookUp, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} businessProcessInstanceId
     * @param {?} key
     * @return {?}
     */
    ProjectService.prototype.getProcessVariableValue = /**
     * @param {?} businessProcessInstanceId
     * @param {?} key
     * @return {?}
     */
    function (businessProcessInstanceId, key) {
        /** @type {?} */
        var url = this.backendServiceUrl + ("rest/project/businessProcessInstance/" + businessProcessInstanceId + "/" + key);
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} businessProcessInstanceId
     * @param {?} key
     * @param {?} value
     * @return {?}
     */
    ProjectService.prototype.setProcessVariableValue = /**
     * @param {?} businessProcessInstanceId
     * @param {?} key
     * @param {?} value
     * @return {?}
     */
    function (businessProcessInstanceId, key, value) {
        /** @type {?} */
        var url = this.backendServiceUrl + ("rest/project/setProcessVariableValue/" + businessProcessInstanceId + "/" + key + "/" + value);
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} actor
     * @return {?}
     */
    ProjectService.prototype.updateActor = /**
     * @param {?} actor
     * @return {?}
     */
    function (actor) {
        /** @type {?} */
        var url = this.backendServiceUrl + 'rest/project/updateActor';
        /** @type {?} */
        var headers = new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        return this.httpClient.post(url, actor).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} participantId
     * @param {?} providerId
     * @param {?} timeSlotType
     * @return {?}
     */
    ProjectService.prototype.getTimeSlots = /**
     * @param {?} participantId
     * @param {?} providerId
     * @param {?} timeSlotType
     * @return {?}
     */
    function (participantId, providerId, timeSlotType) {
        if (!participantId)
            participantId = -1;
        if (!providerId)
            providerId = -1;
        if (!timeSlotType)
            timeSlotType = null;
        /** @type {?} */
        var url = this.backendServiceUrl + ("rest/project/timeSlots/" + participantId + "/" + providerId + "/" + timeSlotType);
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} timeSlots
     * @return {?}
     */
    ProjectService.prototype.updateTimeSlots = /**
     * @param {?} timeSlots
     * @return {?}
     */
    function (timeSlots) {
        /** @type {?} */
        var url = this.backendServiceUrl + 'rest/project/updateTimeSlots';
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        /** @type {?} */
        var params = {
            timeSlots: timeSlots
        };
        return this.httpClient.post(url, params, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} username
     * @param {?} password
     * @param {?} actorId
     * @return {?}
     */
    ProjectService.prototype.createSystemUser = /**
     * @param {?} username
     * @param {?} password
     * @param {?} actorId
     * @return {?}
     */
    function (username, password, actorId) {
        /** @type {?} */
        var url = this.backendServiceUrl + ("rest/login/createUser/" + username + "/" + password + "/" + actorId);
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} stepInstanceId
     * @param {?} imageData
     * @return {?}
     */
    ProjectService.prototype.updateFacialAuthenticationReference = /**
     * @param {?} stepInstanceId
     * @param {?} imageData
     * @return {?}
     */
    function (stepInstanceId, imageData) {
        /** @type {?} */
        var url = this.backendServiceUrl + 'rest/project/facialAuth/reference';
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        /** @type {?} */
        var params = {
            stepInstanceId: stepInstanceId,
            imageData: imageData
        };
        return this.httpClient.post(url, params, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} stepInstanceId
     * @param {?} imageData
     * @return {?}
     */
    ProjectService.prototype.facialAuthentication = /**
     * @param {?} stepInstanceId
     * @param {?} imageData
     * @return {?}
     */
    function (stepInstanceId, imageData) {
        /** @type {?} */
        var url = this.backendServiceUrl + 'rest/project/facialAuth/authenticate';
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        /** @type {?} */
        var params = {
            stepInstanceId: stepInstanceId,
            imageData: imageData
        };
        return this.httpClient.post(url, params, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} projectGuid
     * @param {?} actorId
     * @return {?}
     */
    ProjectService.prototype.getProcessHistory = /**
     * @param {?} projectGuid
     * @param {?} actorId
     * @return {?}
     */
    function (projectGuid, actorId) {
        /** @type {?} */
        var url = this.backendServiceUrl + ("rest/project/stepInstance/progress/" + projectGuid + "/" + actorId);
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} stepIds
     * @return {?}
     */
    ProjectService.prototype.getActiveStepInstances = /**
     * @param {?} stepIds
     * @return {?}
     */
    function (stepIds) {
        /** @type {?} */
        var url = this.backendServiceUrl + 'rest/project/stepInstance/active';
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        for (var i = 0; i < stepIds.length; i++) {
            if (i === 0) {
                url += "?stepIds=" + stepIds[i];
            }
            else {
                url += "&stepIds=" + stepIds[i];
            }
        }
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    /**
     * @param {?} stepId
     * @param {?} externalId
     * @return {?}
     */
    ProjectService.prototype.isStepInstanceActive = /**
     * @param {?} stepId
     * @param {?} externalId
     * @return {?}
     */
    function (stepId, externalId) {
        /** @type {?} */
        var url = this.backendServiceUrl + 'rest/project/stepInstance/active/' + stepId + "/" + externalId;
        /** @type {?} */
        var headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return Promise.resolve(response);
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return Promise.reject(error);
        }));
    };
    ProjectService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    ProjectService.ctorParameters = function () { return [
        { type: HttpClient },
        { type: AcumenConfiguration }
    ]; };
    return ProjectService;
}());
export { ProjectService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    ProjectService.prototype.backendServiceUrl;
    /**
     * @type {?}
     * @private
     */
    ProjectService.prototype.httpClient;
    /**
     * @type {?}
     * @private
     */
    ProjectService.prototype.acumenConfiguration;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWN1bWVuLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9zZXJ2aWNlcy9wcm9qZWN0LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQVluRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUV2RTtJQUdFLHdFQUF3RTtJQUN4RSxvRUFBb0U7SUFDcEUsd0RBQXdEO0lBRXhELHdCQUFvQixVQUFzQixFQUFVLG1CQUF3QztRQUF4RSxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQVUsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUMxRixJQUFJLG1CQUFtQixDQUFDLGlCQUFpQixFQUFFO1lBQ3pDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxtQkFBbUIsQ0FBQyxpQkFBaUIsQ0FBQztTQUNoRTthQUFNO1lBQ0wsSUFBSSxDQUFDLGlCQUFpQixHQUFHLG9DQUFvQyxDQUFDO1NBQy9EO0lBQ0gsQ0FBQzs7Ozs7O0lBRUQsMkNBQWtCOzs7OztJQUFsQixVQUFtQixXQUFtQixFQUFFLE9BQWU7O1lBQ2pELEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCO1FBQ2hDLElBQUksT0FBTyxFQUFFO1lBQ1QsR0FBRyxJQUFJLGtDQUFnQyxXQUFXLFNBQUksT0FBUyxDQUFDO1NBQ25FO2FBQU07WUFDSCxHQUFHLElBQUksd0NBQXNDLFdBQWEsQ0FBQztTQUM5RDs7WUFFSyxPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDOUIsUUFBUSxFQUFFLGtCQUFrQjtTQUM3QixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBMEIsR0FBRyxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQ3JGLElBQUk7Ozs7UUFBQyxVQUFBLFFBQVE7WUFDWixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUEyQixDQUFDLENBQUM7UUFDOUQsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLFVBQUEsS0FBSztZQUNaLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBRUQsZ0RBQXVCOzs7O0lBQXZCLFVBQXdCLHlCQUFpQzs7WUFDbkQsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyw4QkFBOEIsR0FBRyx5QkFBeUIsR0FBRyxhQUFhLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUU7O1lBQzlILE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQztZQUM5QixRQUFRLEVBQUUsa0JBQWtCO1NBQzdCLENBQUM7UUFFRixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUF1QixHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDbEYsSUFBSTs7OztRQUFDLFVBQUEsUUFBUTtZQUNaLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQXdCLENBQUMsQ0FBQztRQUMzRCxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsVUFBQSxLQUFLO1lBQ1osT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCxtQ0FBVTs7OztJQUFWLFVBQVcsV0FBbUI7O1lBQ3hCLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLElBQUcsMEJBQXdCLFdBQWEsQ0FBQTs7WUFDbEUsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzlCLFFBQVEsRUFBRSxrQkFBa0I7U0FDN0IsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQVUsR0FBRyxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQ3JFLElBQUk7Ozs7UUFBQyxVQUFBLFFBQVE7WUFDWixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFXLENBQUMsQ0FBQztRQUM5QyxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsVUFBQSxLQUFLO1lBQ1osT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCxvQ0FBVzs7OztJQUFYLFVBQVksT0FBZTs7WUFDckIsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsSUFBRyxnQ0FBOEIsT0FBUyxDQUFBOztZQUNwRSxPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDOUIsUUFBUSxFQUFFLGtCQUFrQjtTQUM3QixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBWSxHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDdkUsSUFBSTs7OztRQUFDLFVBQUEsUUFBUTtZQUNaLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQWEsQ0FBQyxDQUFDO1FBQ2hELENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxVQUFBLEtBQUs7WUFDWixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVELHdDQUFlOzs7O0lBQWYsVUFBZ0IsWUFBb0I7O1lBQzlCLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLElBQUcsMkJBQXlCLFlBQWMsQ0FBQTs7WUFDcEUsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzlCLFFBQVEsRUFBRSxrQkFBa0I7U0FDN0IsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQVksR0FBRyxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQ3ZFLElBQUk7Ozs7UUFBQyxVQUFBLFFBQVE7WUFDWixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFhLENBQUMsQ0FBQztRQUNoRCxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsVUFBQSxLQUFLO1lBQ1osT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCx3Q0FBZTs7OztJQUFmLFVBQWdCLFNBQWlCOztZQUMzQixHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixJQUFHLCtCQUE2QixTQUFXLENBQUE7O1lBQ3JFLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQztZQUM5QixRQUFRLEVBQUUsa0JBQWtCO1NBQzdCLENBQUM7UUFFRixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFnQixHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDM0UsSUFBSTs7OztRQUFDLFVBQUEsUUFBUTtZQUNaLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQWlCLENBQUMsQ0FBQztRQUNwRCxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsVUFBQSxLQUFLO1lBQ1osT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7SUFFRCx5Q0FBZ0I7Ozs7Ozs7Ozs7Ozs7SUFBaEIsVUFBaUIsVUFBa0IsRUFBRSxXQUFtQixFQUFFLFNBQWlCLEVBQUUsYUFBcUIsRUFDMUYsU0FBaUIsRUFBRSxPQUFlLEVBQUUsSUFBWSxFQUFFLFFBQWdCLEVBQUUsYUFBcUIsRUFBRSxVQUFrQjtRQUNuSCxJQUFJLENBQUMsU0FBUztZQUNWLFNBQVMsR0FBRyxNQUFNLENBQUM7UUFFdkIsSUFBSSxDQUFDLE9BQU87WUFDUixPQUFPLEdBQUcsTUFBTSxDQUFDO1FBRXJCLElBQUksQ0FBQyxJQUFJO1lBQ0wsSUFBSSxHQUFHLE1BQU0sQ0FBQztRQUVsQixJQUFJLENBQUMsUUFBUTtZQUNULFFBQVEsR0FBRyxNQUFNLENBQUM7UUFFdEIsSUFBSSxDQUFDLGFBQWE7WUFDZCxhQUFhLEdBQUcsTUFBTSxDQUFDOzs7WUFHdkIsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsSUFBRyxnQ0FBOEIsVUFBVSxTQUFJLFdBQVcsU0FBSSxTQUFTLFNBQUksYUFBYSxTQUFJLFNBQVMsU0FBSSxPQUFPLFNBQUksSUFBSSxTQUFJLFFBQVEsU0FBSSxhQUFhLFNBQUksVUFBWSxDQUFBOztZQUMvTCxPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDOUIsUUFBUSxFQUFFLGtCQUFrQjtTQUM3QixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBZ0IsR0FBRyxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQzNFLElBQUk7Ozs7UUFBQyxVQUFBLFFBQVE7WUFDWixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFpQixDQUFDLENBQUM7UUFDcEQsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLFVBQUEsS0FBSztZQUNaLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7Ozs7O0lBRUQsMENBQWlCOzs7Ozs7O0lBQWpCLFVBQWtCLFVBQWtCLEVBQUUsV0FBbUIsRUFBRSxTQUFpQixFQUFFLGFBQXFCOztZQUM3RixHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixJQUFHLGlDQUErQixVQUFVLFNBQUksV0FBVyxTQUFJLFNBQVMsU0FBSSxhQUFlLENBQUE7O1lBQ3JILE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQztZQUM5QixRQUFRLEVBQUUsa0JBQWtCO1NBQzdCLENBQUM7UUFFRixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFnQixHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDM0UsSUFBSTs7OztRQUFDLFVBQUEsUUFBUTtZQUNaLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQWlCLENBQUMsQ0FBQztRQUNwRCxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsVUFBQSxLQUFLO1lBQ1osT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7Ozs7SUFFRCwwQ0FBaUI7Ozs7Ozs7SUFBakIsVUFBa0IsVUFBa0IsRUFBRSxXQUFtQixFQUFFLGFBQXFCLEVBQUUsV0FBbUI7UUFDbkcsSUFBSSxDQUFDLFdBQVc7WUFDWixXQUFXLEdBQUcsTUFBTSxDQUFDOztZQUVyQixHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixJQUFHLGlDQUErQixVQUFVLFNBQUksV0FBVyxTQUFJLGFBQWEsU0FBSSxXQUFhLENBQUE7O1lBQ3ZILE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQztZQUM5QixRQUFRLEVBQUUsa0JBQWtCO1NBQzdCLENBQUM7UUFFRixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFnQixHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDM0UsSUFBSTs7OztRQUFDLFVBQUEsUUFBUTtZQUNaLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQWlCLENBQUMsQ0FBQztRQUNwRCxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsVUFBQSxLQUFLO1lBQ1osT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCx5Q0FBZ0I7Ozs7SUFBaEIsVUFBaUIsYUFBcUI7O1lBQ2hDLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLElBQUcsZ0NBQThCLGFBQWUsQ0FBQTs7WUFDMUUsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzlCLFFBQVEsRUFBRSxrQkFBa0I7U0FDN0IsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQWlCLEdBQUcsRUFBRSxFQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRTthQUM1RSxJQUFJOzs7O1FBQUMsVUFBQSxRQUFRO1lBQ1osT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLG1CQUFBLFFBQVEsRUFBa0IsQ0FBQyxDQUFDO1FBQ3JELENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxVQUFBLEtBQUs7WUFDWixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7OztJQUVELDBDQUFpQjs7Ozs7OztJQUFqQixVQUFrQixXQUFtQixFQUFFLE1BQWMsRUFBRSxVQUFrQixFQUFFLGFBQXFCO1FBQzlGLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDaEIsV0FBVyxHQUFHLE1BQU0sQ0FBQztTQUN0QjtRQUVELElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDWCxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7U0FDYjtRQUVELElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDZixVQUFVLEdBQUcsQ0FBQyxDQUFDLENBQUM7U0FDakI7UUFFRCxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ2xCLGFBQWEsR0FBRyxDQUFDLENBQUMsQ0FBQztTQUNwQjs7WUFFRyxHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixJQUFHLGdDQUE4QixXQUFXLFNBQUksTUFBTSxTQUFJLFVBQVUsU0FBSSxhQUFlLENBQUE7O1lBQ2pILE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQztZQUM5QixRQUFRLEVBQUUsa0JBQWtCO1NBQzdCLENBQUM7UUFFRixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFpQixHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDNUUsSUFBSTs7OztRQUFDLFVBQUEsUUFBUTtZQUNaLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQWtCLENBQUMsQ0FBQztRQUNyRCxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsVUFBQSxLQUFLO1lBQ1osT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCwwQ0FBaUI7Ozs7SUFBakIsVUFBa0IsTUFBYzs7WUFDMUIsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyx3Q0FBd0MsR0FBRyxNQUFNOztZQUM5RSxPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDOUIsUUFBUSxFQUFFLGtCQUFrQjtTQUM3QixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBTSxHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDakUsSUFBSTs7OztRQUFDLFVBQUEsUUFBUTtZQUNaLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNuQyxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsVUFBQSxLQUFLO1lBQ1osT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCxvREFBMkI7Ozs7SUFBM0IsVUFBNEIsT0FBZTs7WUFDckMsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyw4Q0FBOEMsR0FBRyxPQUFPOztZQUNyRixPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDOUIsUUFBUSxFQUFFLGtCQUFrQjtTQUM3QixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBTyxHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDbEUsSUFBSTs7OztRQUFDLFVBQUEsUUFBUTtZQUNaLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQVEsQ0FBQyxDQUFDO1FBQzNDLENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxVQUFBLEtBQUs7WUFDWixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7SUFFRCxxQ0FBWTs7Ozs7SUFBWixVQUFhLGNBQXNCLEVBQUUsWUFBb0I7O1lBQ25ELEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCO1FBQ2hDLElBQUksWUFBWTtZQUNaLEdBQUcsSUFBSSwyREFBeUQsY0FBYyxzQkFBaUIsWUFBYyxDQUFDOztZQUU5RyxHQUFHLElBQUksNERBQTBELGNBQWdCLENBQUM7O1lBRWxGLE9BQU8sR0FBRyxJQUFJLE9BQU8sQ0FBQztZQUN4QixRQUFRLEVBQUUsa0JBQWtCO1lBQzVCLGNBQWMsRUFBRSxrQkFBa0I7U0FDbkMsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDN0QsSUFBSTs7OztRQUFDLFVBQUEsUUFBUTtZQUNaLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQVUsQ0FBQyxDQUFDO1FBQzdDLENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxVQUFBLEtBQUs7WUFDWixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7O0lBRUQsMkNBQWtCOzs7Ozs7SUFBbEIsVUFBbUIsY0FBc0IsRUFBRSxRQUFnQixFQUFFLFNBQWlCOztZQUN4RSxHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQjtRQUNoQyxHQUFHLElBQUksb0RBQWtELGNBQWMsa0JBQWEsUUFBUSxtQkFBYyxTQUFXLENBQUM7O1lBRWxILE9BQU8sR0FBRyxJQUFJLE9BQU8sQ0FBQztZQUN4QixRQUFRLEVBQUUsa0JBQWtCO1lBQzVCLGNBQWMsRUFBRSxrQkFBa0I7U0FDbkMsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDN0QsSUFBSTs7OztRQUFDLFVBQUEsUUFBUTtZQUNaLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQVUsQ0FBQyxDQUFDO1FBQzdDLENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxVQUFBLEtBQUs7WUFDWixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVELHlDQUFnQjs7OztJQUFoQixVQUFpQixjQUFzQjs7WUFDakMsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsSUFBRyw2Q0FBMkMsY0FBZ0IsQ0FBQTs7WUFDMUYsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzVCLFFBQVEsRUFBRSxrQkFBa0I7U0FDN0IsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQzVELElBQUk7Ozs7UUFBQyxVQUFBLFFBQVE7WUFDWixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFpQixDQUFDLENBQUM7UUFDcEQsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLFVBQUEsS0FBSztZQUNaLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7Ozs7O0lBRUQsNENBQW1COzs7Ozs7O0lBQW5CLFVBQW9CLGNBQXNCLEVBQUUsU0FBNkIsRUFBRSxNQUFlLEVBQUUsWUFBb0I7O1lBQzFHLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsbURBQW1EOztZQUNsRixPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDNUIsUUFBUSxFQUFFLGtCQUFrQjtZQUM1QixjQUFjLEVBQUUsa0JBQWtCO1NBQ25DLENBQUM7O1lBRUUscUJBQXFCLEdBQUc7WUFDMUIsY0FBYyxFQUFFLGNBQWM7WUFDOUIsU0FBUyxFQUFFLFNBQVM7WUFDcEIsTUFBTSxFQUFFLE1BQU07WUFDZCxZQUFZLEVBQUUsWUFBWTtTQUMzQjtRQUVELE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMscUJBQXFCLENBQUMsRUFBRSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDLFNBQVMsRUFBRTthQUN0RyxJQUFJOzs7O1FBQUMsVUFBQSxRQUFRO1lBQ1osT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLG1CQUFBLFFBQVEsRUFBVSxDQUFDLENBQUM7UUFDN0MsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLFVBQUEsS0FBSztZQUNaLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBRUQsMkNBQWtCOzs7O0lBQWxCLFVBQW1CLGNBQXNCOztZQUNuQyxHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixJQUFHLHVDQUFxQyxjQUFnQixDQUFBOztZQUNsRixPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDOUIsUUFBUSxFQUFFLGtCQUFrQjtTQUM3QixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDNUQsSUFBSTs7OztRQUFDLFVBQUEsUUFBUTtZQUNaLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQVUsQ0FBQyxDQUFDO1FBQzdDLENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxVQUFBLEtBQUs7WUFDWixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7SUFFRCx3Q0FBZTs7Ozs7SUFBZixVQUFnQix5QkFBaUMsRUFBRSxJQUFZOztZQUN6RCxHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFHLDhCQUE4Qjs7WUFDN0QsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzVCLFFBQVEsRUFBRSxrQkFBa0I7WUFDNUIsY0FBYyxFQUFFLGtCQUFrQjtTQUNuQyxDQUFDOztZQUVFLFVBQVUsR0FBRztZQUNmLHlCQUF5QixFQUFFLHlCQUF5QjtZQUNwRCxJQUFJLEVBQUUsSUFBSTtTQUNYO1FBRUQsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsVUFBVSxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQ3hFLElBQUk7Ozs7UUFBQyxVQUFBLFFBQVE7WUFDWixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFVLENBQUMsQ0FBQztRQUM3QyxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsVUFBQSxLQUFLO1lBQ1osT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1IsQ0FBQzs7Ozs7O0lBRUQsZ0RBQXVCOzs7OztJQUF2QixVQUF3Qix5QkFBaUMsRUFBRSxHQUFXOztZQUNoRSxHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixJQUFHLDBDQUF3Qyx5QkFBeUIsU0FBSSxHQUFLLENBQUE7O1lBQ3ZHLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQztZQUM5QixRQUFRLEVBQUUsa0JBQWtCO1NBQzdCLENBQUM7UUFFRixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxFQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRTthQUM1RCxJQUFJOzs7O1FBQUMsVUFBQSxRQUFRO1lBQ1osT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLG1CQUFBLFFBQVEsRUFBVSxDQUFDLENBQUM7UUFDN0MsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLFVBQUEsS0FBSztZQUNaLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7Ozs7SUFFRCxnREFBdUI7Ozs7OztJQUF2QixVQUF3Qix5QkFBaUMsRUFBRSxHQUFXLEVBQUUsS0FBYTs7WUFDL0UsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsSUFBRywwQ0FBd0MseUJBQXlCLFNBQUksR0FBRyxTQUFJLEtBQU8sQ0FBQTs7WUFDaEgsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzlCLFFBQVEsRUFBRSxrQkFBa0I7U0FDN0IsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQzVELElBQUk7Ozs7UUFBQyxVQUFBLFFBQVE7WUFDWixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFVLENBQUMsQ0FBQztRQUM3QyxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsVUFBQSxLQUFLO1lBQ1osT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCxvQ0FBVzs7OztJQUFYLFVBQVksS0FBWTs7WUFDbEIsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRywwQkFBMEI7O1lBQ3pELE9BQU8sR0FBRyxJQUFJLE9BQU8sQ0FBQztZQUN4QixRQUFRLEVBQUUsa0JBQWtCO1lBQzVCLGNBQWMsRUFBRSxrQkFBa0I7U0FDbkMsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDLFNBQVMsRUFBRTthQUNoRCxJQUFJOzs7O1FBQUMsVUFBQSxRQUFRO1lBQ1osT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLG1CQUFBLFFBQVEsRUFBVSxDQUFDLENBQUM7UUFDN0MsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLFVBQUEsS0FBSztZQUNaLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7Ozs7SUFFRCxxQ0FBWTs7Ozs7O0lBQVosVUFBYSxhQUFxQixFQUFFLFVBQWtCLEVBQUUsWUFBb0I7UUFDMUUsSUFBSSxDQUFDLGFBQWE7WUFDZCxhQUFhLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFFdkIsSUFBSSxDQUFDLFVBQVU7WUFDWCxVQUFVLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFFcEIsSUFBSSxDQUFDLFlBQVk7WUFDYixZQUFZLEdBQUcsSUFBSSxDQUFDOztZQUVwQixHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixJQUFHLDRCQUEwQixhQUFhLFNBQUksVUFBVSxTQUFJLFlBQWMsQ0FBQTs7WUFDcEcsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzlCLFFBQVEsRUFBRSxrQkFBa0I7U0FDN0IsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQzVELElBQUk7Ozs7UUFBQyxVQUFBLFFBQVE7WUFDWixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFjLENBQUMsQ0FBQztRQUNqRCxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsVUFBQSxLQUFLO1lBQ1osT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCx3Q0FBZTs7OztJQUFmLFVBQWdCLFNBQXFCOztZQUMvQixHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFHLDhCQUE4Qjs7WUFDN0QsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzFCLFFBQVEsRUFBRSxrQkFBa0I7WUFDNUIsY0FBYyxFQUFFLGtCQUFrQjtTQUNyQyxDQUFDOztZQUVFLE1BQU0sR0FBRztZQUNULFNBQVMsRUFBRSxTQUFTO1NBQ3ZCO1FBRUQsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsTUFBTSxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQ3BFLElBQUk7Ozs7UUFBQyxVQUFBLFFBQVE7WUFDWixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFjLENBQUMsQ0FBQztRQUNqRCxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsVUFBQSxLQUFLO1lBQ1osT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1IsQ0FBQzs7Ozs7OztJQUVELHlDQUFnQjs7Ozs7O0lBQWhCLFVBQWlCLFFBQWdCLEVBQUUsUUFBZ0IsRUFBRSxPQUFlOztZQUM5RCxHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixJQUFHLDJCQUF5QixRQUFRLFNBQUksUUFBUSxTQUFJLE9BQVMsQ0FBQTs7WUFDdkYsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzlCLFFBQVEsRUFBRSxrQkFBa0I7U0FDN0IsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQzVELElBQUk7Ozs7UUFBQyxVQUFBLFFBQVE7WUFDWixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFVLENBQUMsQ0FBQztRQUM3QyxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsVUFBQSxLQUFLO1lBQ1osT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7O0lBRUQsNERBQW1DOzs7OztJQUFuQyxVQUFvQyxjQUFzQixFQUFFLFNBQWlCOztZQUN2RSxHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFHLG1DQUFtQzs7WUFDbEUsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzFCLFFBQVEsRUFBRSxrQkFBa0I7WUFDNUIsY0FBYyxFQUFFLGtCQUFrQjtTQUNyQyxDQUFDOztZQUVFLE1BQU0sR0FBRztZQUNULGNBQWMsRUFBRSxjQUFjO1lBQzlCLFNBQVMsRUFBRSxTQUFTO1NBQ3ZCO1FBRUQsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsTUFBTSxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQ3BFLElBQUk7Ozs7UUFBQyxVQUFBLFFBQVE7WUFDWixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFjLENBQUMsQ0FBQztRQUNqRCxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsVUFBQSxLQUFLO1lBQ1osT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1IsQ0FBQzs7Ozs7O0lBRUQsNkNBQW9COzs7OztJQUFwQixVQUFxQixjQUFzQixFQUFFLFNBQWlCOztZQUN4RCxHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFHLHNDQUFzQzs7WUFDckUsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzFCLFFBQVEsRUFBRSxrQkFBa0I7WUFDNUIsY0FBYyxFQUFFLGtCQUFrQjtTQUNyQyxDQUFDOztZQUVFLE1BQU0sR0FBRztZQUNULGNBQWMsRUFBRSxjQUFjO1lBQzlCLFNBQVMsRUFBRSxTQUFTO1NBQ3ZCO1FBRUQsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsTUFBTSxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQ3BFLElBQUk7Ozs7UUFBQyxVQUFBLFFBQVE7WUFDWixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFjLENBQUMsQ0FBQztRQUNqRCxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsVUFBQSxLQUFLO1lBQ1osT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1IsQ0FBQzs7Ozs7O0lBRUQsMENBQWlCOzs7OztJQUFqQixVQUFrQixXQUFtQixFQUFFLE9BQWU7O1lBQ2hELEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLElBQUcsd0NBQXNDLFdBQVcsU0FBSSxPQUFTLENBQUE7O1lBQzNGLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQztZQUM5QixRQUFRLEVBQUUsa0JBQWtCO1NBQzdCLENBQUM7UUFFRixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxFQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRTthQUM1RCxJQUFJOzs7O1FBQUMsVUFBQSxRQUFRO1lBQ1osT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLG1CQUFBLFFBQVEsRUFBdUIsQ0FBQyxDQUFDO1FBQzFELENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxVQUFBLEtBQUs7WUFDWixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVELCtDQUFzQjs7OztJQUF0QixVQUF1QixPQUFzQjs7WUFDdkMsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxrQ0FBa0M7O1lBQy9ELE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQztZQUM5QixRQUFRLEVBQUUsa0JBQWtCO1NBQzdCLENBQUM7UUFFRixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN2QyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ1gsR0FBRyxJQUFJLFdBQVcsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDakM7aUJBQU07Z0JBQ0wsR0FBRyxJQUFJLFdBQVcsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDakM7U0FDRjtRQUVELE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQzVELElBQUk7Ozs7UUFBQyxVQUFBLFFBQVE7WUFDWixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUF1QixDQUFDLENBQUM7UUFDMUQsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLFVBQUEsS0FBSztZQUNaLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7OztJQUVELDZDQUFvQjs7Ozs7SUFBcEIsVUFBcUIsTUFBYyxFQUFFLFVBQWtCOztZQUNqRCxHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFHLG1DQUFtQyxHQUFHLE1BQU0sR0FBRyxHQUFHLEdBQUcsVUFBVTs7WUFDNUYsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzlCLFFBQVEsRUFBRSxrQkFBa0I7U0FDN0IsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQzVELElBQUk7Ozs7UUFBQyxVQUFBLFFBQVE7WUFDWixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDbkMsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLFVBQUEsS0FBSztZQUNaLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7O2dCQXJoQkYsVUFBVTs7OztnQkFmRixVQUFVO2dCQWFWLG1CQUFtQjs7SUF3aEI1QixxQkFBQztDQUFBLEFBdGhCRCxJQXNoQkM7U0FyaEJZLGNBQWM7Ozs7OztJQUN6QiwyQ0FBMEI7Ozs7O0lBS2Qsb0NBQThCOzs7OztJQUFFLDZDQUFnRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBIdHRwSGVhZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcblxuaW1wb3J0IHsgQnVzaW5lc3NQcm9jZXNzSW5zdGFuY2UgfSBmcm9tICcuLi9kb21haW4vYnVzaW5lc3MtcHJvY2Vzcy1pbnN0YW5jZSc7XG5pbXBvcnQgeyBTdGVwSW5zdGFuY2UgfSBmcm9tICcuLi9kb21haW4vc3RlcC1pbnN0YW5jZSc7XG5pbXBvcnQgeyBCdXNpbmVzc1Byb2Nlc3NTdGF0ZSB9IGZyb20gJy4uL2RvbWFpbi9idXNpbmVzcy1wcm9jZXNzLXN0YXRlJztcbmltcG9ydCB7IFByb2plY3QgfSBmcm9tICcuLi9kb21haW4vcHJvamVjdCc7XG5pbXBvcnQgeyBQcm9qZWN0Um9sZSB9IGZyb20gJy4uL2RvbWFpbi9wcm9qZWN0LXJvbGUnO1xuaW1wb3J0IHsgVGFibGVEYXRhUGFnZSB9IGZyb20gJy4uL2RvbWFpbi90YWJsZS1kYXRhLXBhZ2UnO1xuaW1wb3J0IHsgQWN0b3IgfSBmcm9tICcuLi9kb21haW4vYWN0b3InO1xuaW1wb3J0IHsgUXVlc3Rpb25uYWlyZSB9IGZyb20gJy4uL2RvbWFpbi9xdWVzdGlvbm5haXJlJztcbmltcG9ydCB7IFF1ZXN0aW9uUmVzcG9uc2UgfSBmcm9tICcuLi9kb21haW4vcXVlc3Rpb24tcmVzcG9uc2UnO1xuaW1wb3J0IHsgVGltZVNsb3QgfSBmcm9tICcuLi9kb21haW4vdGltZS1zbG90JztcbmltcG9ydCB7IEFjdW1lbkNvbmZpZ3VyYXRpb24gfSBmcm9tICcuLi9zZXJ2aWNlcy9hY3VtZW4tY29uZmlndXJhdGlvbic7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBQcm9qZWN0U2VydmljZSB7XG4gIHByaXZhdGUgYmFja2VuZFNlcnZpY2VVcmw7XG4gIC8vIHByaXZhdGUgYmFja2VuZFNlcnZpY2VVcmwgPSAnaHR0cDovL3d3dy5oZWFsdGhhY3VtZW4uY28uemEvaW5zaWdodC8nO1xuICAvLyBwcml2YXRlIGJhY2tlbmRTZXJ2aWNlVXJsID0gJ2h0dHBzOi8vZnJhbmsucm1zdWkuY28uemEvaW5zaWdodC8nO1xuICAvLyBwcml2YXRlIGJhY2tlbmRTZXJ2aWNlVXJsID0gJ2h0dHA6Ly9sb2NhbGhvc3Q6ODg4OC8nO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cENsaWVudDogSHR0cENsaWVudCwgcHJpdmF0ZSBhY3VtZW5Db25maWd1cmF0aW9uOiBBY3VtZW5Db25maWd1cmF0aW9uKSB7XG4gICAgaWYgKGFjdW1lbkNvbmZpZ3VyYXRpb24uYmFja2VuZFNlcnZpY2VVcmwpIHtcbiAgICAgIHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgPSBhY3VtZW5Db25maWd1cmF0aW9uLmJhY2tlbmRTZXJ2aWNlVXJsO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsID0gXCJodHRwczovL2ZyYW5rLnJtc3VpLmNvLnphL2luc2lnaHQvXCI7XG4gICAgfVxuICB9XG5cbiAgc3RhcnRXaXphcmRQcm9qZWN0KHByb2plY3RHdWlkOiBzdHJpbmcsIGFjdG9ySWQ6IG51bWJlcik6IFByb21pc2U8QnVzaW5lc3NQcm9jZXNzSW5zdGFuY2U+IHtcbiAgICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybDtcbiAgICBpZiAoYWN0b3JJZCkge1xuICAgICAgICB1cmwgKz0gYHJlc3QvcHJvamVjdC9wcm9qZWN0cy9zdGFydDIvJHtwcm9qZWN0R3VpZH0vJHthY3RvcklkfWA7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgdXJsICs9IGByZXN0L3Byb2plY3QvcHJvamVjdHMvc3RhcnQvd2l6YXJkLyR7cHJvamVjdEd1aWR9YDtcbiAgICB9XG5cbiAgICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldDxCdXNpbmVzc1Byb2Nlc3NJbnN0YW5jZT4odXJsLCB7aGVhZGVyczogaGVhZGVyc30pLnRvUHJvbWlzZSgpXG4gICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgQnVzaW5lc3NQcm9jZXNzSW5zdGFuY2UpO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxuICBnZXRCdXNpbmVzc1Byb2Nlc3NTdGF0ZShidXNpbmVzc1Byb2Nlc3NJbnN0YW5jZUlkOiBudW1iZXIpOiBQcm9taXNlPEJ1c2luZXNzUHJvY2Vzc1N0YXRlPiB7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBcInJlc3QvcHJvamVjdC9wcm9qZWN0cy9zdGF0ZS9cIiArIGJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlSWQgKyBcIj90aW1lc3RhbXA9XCIgKyBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcbiAgICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldDxCdXNpbmVzc1Byb2Nlc3NTdGF0ZT4odXJsLCB7aGVhZGVyczogaGVhZGVyc30pLnRvUHJvbWlzZSgpXG4gICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgQnVzaW5lc3NQcm9jZXNzU3RhdGUpO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxuICBnZXRQcm9qZWN0KHByb2plY3RHdWlkOiBzdHJpbmcpOiBQcm9taXNlPFByb2plY3Q+IHtcbiAgICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybCArIGByZXN0L3Byb2plY3QvcHJvamVjdC8ke3Byb2plY3RHdWlkfWA7XG4gICAgY29uc3QgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8UHJvamVjdD4odXJsLCB7aGVhZGVyczogaGVhZGVyc30pLnRvUHJvbWlzZSgpXG4gICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgUHJvamVjdCk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIGdldFByb2plY3RzKGFjdG9ySWQ6IG51bWJlcik6IFByb21pc2U8UHJvamVjdFtdPiB7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBgcmVzdC9wcm9qZWN0L2FjdG9yUHJvamVjdHMvJHthY3RvcklkfWA7XG4gICAgY29uc3QgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8UHJvamVjdFtdPih1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBQcm9qZWN0W10pO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxuICBnZXRVc2VyUHJvamVjdHMoc3lzdGVtVXNlcklkOiBudW1iZXIpOiBQcm9taXNlPFByb2plY3RbXT4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgYHJlc3QvcHJvamVjdC9wcm9qZWN0cy8ke3N5c3RlbVVzZXJJZH1gO1xuICAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICB9KTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PFByb2plY3RbXT4odXJsLCB7aGVhZGVyczogaGVhZGVyc30pLnRvUHJvbWlzZSgpXG4gICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgUHJvamVjdFtdKTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgZ2V0UHJvamVjdFJvbGVzKHByb2plY3RJZDogbnVtYmVyKTogUHJvbWlzZTxQcm9qZWN0Um9sZVtdPiB7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBgcmVzdC9wcm9qZWN0L3Byb2plY3RSb2xlcy8ke3Byb2plY3RJZH1gO1xuICAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICB9KTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PFByb2plY3RSb2xlW10+KHVybCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIFByb2plY3RSb2xlW10pO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxuICBnZXRQcm9qZWN0QWN0b3JzKGZyb21SZWNvcmQ6IG51bWJlciwgcmVjb3JkQ291bnQ6IG51bWJlciwgcHJvamVjdElkOiBudW1iZXIsIHByb2plY3RSb2xlSWQ6IG51bWJlcixcbiAgICAgICAgICBmaXJzdG5hbWU6IHN0cmluZywgc3VybmFtZTogc3RyaW5nLCBpZE5yOiBzdHJpbmcsIG1lbWJlck5yOiBzdHJpbmcsIGRlcGVuZGFudENvZGU6IHN0cmluZywgYXNzaWduZWVJZDogbnVtYmVyKTogUHJvbWlzZTxUYWJsZURhdGFQYWdlPiB7XG4gICAgaWYgKCFmaXJzdG5hbWUpXG4gICAgICAgIGZpcnN0bmFtZSA9IFwibnVsbFwiO1xuXG4gICAgaWYgKCFzdXJuYW1lKVxuICAgICAgICBzdXJuYW1lID0gXCJudWxsXCI7XG5cbiAgICBpZiAoIWlkTnIpXG4gICAgICAgIGlkTnIgPSBcIm51bGxcIjtcblxuICAgIGlmICghbWVtYmVyTnIpXG4gICAgICAgIG1lbWJlck5yID0gXCJudWxsXCI7XG5cbiAgICBpZiAoIWRlcGVuZGFudENvZGUpXG4gICAgICAgIGRlcGVuZGFudENvZGUgPSBcIm51bGxcIjtcblxuICAgICAgLy8gYXNzaWduZWVJZCA9IC0xO1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgYHJlc3QvcHJvamVjdC9wcm9qZWN0QWN0b3JzLyR7ZnJvbVJlY29yZH0vJHtyZWNvcmRDb3VudH0vJHtwcm9qZWN0SWR9LyR7cHJvamVjdFJvbGVJZH0vJHtmaXJzdG5hbWV9LyR7c3VybmFtZX0vJHtpZE5yfS8ke21lbWJlck5yfS8ke2RlcGVuZGFudENvZGV9LyR7YXNzaWduZWVJZH1gO1xuICAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICB9KTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PFRhYmxlRGF0YVBhZ2U+KHVybCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIFRhYmxlRGF0YVBhZ2UpO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxuICBnZXRQcm9qZWN0QWN0b3JzMihmcm9tUmVjb3JkOiBudW1iZXIsIHJlY29yZENvdW50OiBudW1iZXIsIHByb2plY3RJZDogbnVtYmVyLCBwcm9qZWN0Um9sZUlkOiBudW1iZXIpOiBQcm9taXNlPFRhYmxlRGF0YVBhZ2U+IHtcbiAgICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybCArIGByZXN0L3Byb2plY3QvcHJvamVjdEFjdG9yczIvJHtmcm9tUmVjb3JkfS8ke3JlY29yZENvdW50fS8ke3Byb2plY3RJZH0vJHtwcm9qZWN0Um9sZUlkfWA7XG4gICAgY29uc3QgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8VGFibGVEYXRhUGFnZT4odXJsLCB7aGVhZGVyczogaGVhZGVyc30pLnRvUHJvbWlzZSgpXG4gICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgVGFibGVEYXRhUGFnZSk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIGdldFByb2plY3RBY3RvcnMzKGZyb21SZWNvcmQ6IG51bWJlciwgcmVjb3JkQ291bnQ6IG51bWJlciwgcHJvamVjdFJvbGVJZDogbnVtYmVyLCBzZWFyY2hGaWVsZDogc3RyaW5nKTogUHJvbWlzZTxUYWJsZURhdGFQYWdlPiB7XG4gICAgaWYgKCFzZWFyY2hGaWVsZClcbiAgICAgICAgc2VhcmNoRmllbGQgPSBcIm51bGxcIjtcblxuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgYHJlc3QvcHJvamVjdC9wcm9qZWN0QWN0b3JzMy8ke2Zyb21SZWNvcmR9LyR7cmVjb3JkQ291bnR9LyR7cHJvamVjdFJvbGVJZH0vJHtzZWFyY2hGaWVsZH1gO1xuICAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICB9KTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PFRhYmxlRGF0YVBhZ2U+KHVybCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIFRhYmxlRGF0YVBhZ2UpO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxuICBnZXRTdGVwSW5zdGFuY2VzKHBhcnRpY2lwYW50SWQ6IG51bWJlcik6IFByb21pc2U8QXJyYXk8U3RlcEluc3RhbmNlPj4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgYHJlc3QvcHJvamVjdC9zdGVwSW5zdGFuY2VzLyR7cGFydGljaXBhbnRJZH1gO1xuICAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICB9KTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PFN0ZXBJbnN0YW5jZVtdPih1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBTdGVwSW5zdGFuY2VbXSk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIGdldFN0ZXBJbnN0YW5jZXMyKHByb2plY3RHdWlkOiBzdHJpbmcsIHVzZXJJZDogbnVtYmVyLCBhc3NpZ25lZUlkOiBudW1iZXIsIHBhcnRpY2lwYW50SWQ6IG51bWJlcik6IFByb21pc2U8QXJyYXk8U3RlcEluc3RhbmNlPj4ge1xuICAgIGlmICghcHJvamVjdEd1aWQpIHtcbiAgICAgIHByb2plY3RHdWlkID0gXCJudWxsXCI7XG4gICAgfVxuXG4gICAgaWYgKCF1c2VySWQpIHtcbiAgICAgIHVzZXJJZCA9IC0xO1xuICAgIH1cblxuICAgIGlmICghYXNzaWduZWVJZCkge1xuICAgICAgYXNzaWduZWVJZCA9IC0xO1xuICAgIH1cblxuICAgIGlmICghcGFydGljaXBhbnRJZCkge1xuICAgICAgcGFydGljaXBhbnRJZCA9IC0xO1xuICAgIH1cblxuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgYHJlc3QvcHJvamVjdC9zdGVwSW5zdGFuY2VzLyR7cHJvamVjdEd1aWR9LyR7dXNlcklkfS8ke2Fzc2lnbmVlSWR9LyR7cGFydGljaXBhbnRJZH1gO1xuICAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICB9KTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PFN0ZXBJbnN0YW5jZVtdPih1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBTdGVwSW5zdGFuY2VbXSk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIGdldFN0ZXBQYXJhbWV0ZXJzKHN0ZXBJZDogbnVtYmVyKTogUHJvbWlzZTxhbnk+IHtcbiAgICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybCArIFwicmVzdC9wcm9qZWN0L3N0ZXBJbnN0YW5jZXMvcGFyYW1ldGVycy9cIiArIHN0ZXBJZDtcbiAgICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldDxhbnk+KHVybCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlKTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgZ2V0TGF0ZXN0U3RlcEFzc2lnbm1lbnREYXRlKGFjdG9ySWQ6IG51bWJlcik6IFByb21pc2U8RGF0ZT4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgXCJyZXN0L3Byb2plY3Qvc3RlcEluc3RhbmNlcy9sYXRlc3RBc3NpZ25tZW50L1wiICsgYWN0b3JJZDtcbiAgICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldDxEYXRlPih1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBEYXRlKTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgY29tcGxldGVTdGVwKHN0ZXBJbnN0YW5jZUlkOiBudW1iZXIsIHN5c3RlbVVzZXJJZDogbnVtYmVyKTogUHJvbWlzZTxhbnk+IHtcbiAgICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybDtcbiAgICBpZiAoc3lzdGVtVXNlcklkKVxuICAgICAgICB1cmwgKz0gYHJlc3QvcHJvamVjdC9zdGVwSW5zdGFuY2UvY29tcGxldGVTdGVwP3N0ZXBJbnN0YW5jZUlkPSR7c3RlcEluc3RhbmNlSWR9JnN5c3RlbVVzZXJJZD0ke3N5c3RlbVVzZXJJZH1gO1xuICAgIGVsc2VcbiAgICAgICAgdXJsICs9IGByZXN0L3Byb2plY3Qvc3RlcEluc3RhbmNlL2NvbXBsZXRlU3RlcDI/c3RlcEluc3RhbmNlSWQ9JHtzdGVwSW5zdGFuY2VJZH1gO1xuXG4gICAgbGV0IGhlYWRlcnMgPSBuZXcgSGVhZGVycyh7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJ1xuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5wb3N0KHVybCwgSlNPTi5zdHJpbmdpZnkoe30pKS50b1Byb21pc2UoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIHN0cmluZyk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIHVwZGF0ZVN0ZXBMb2NhdGlvbihzdGVwSW5zdGFuY2VJZDogbnVtYmVyLCBsYXRpdHVkZTogbnVtYmVyLCBsb25naXR1ZGU6IG51bWJlcik6IFByb21pc2U8YW55PiB7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmw7XG4gICAgdXJsICs9IGByZXN0L3Byb2plY3QvdXBkYXRlU3RlcExvY2F0aW9uP3N0ZXBJbnN0YW5jZUlkPSR7c3RlcEluc3RhbmNlSWR9JmxhdGl0dWRlPSR7bGF0aXR1ZGV9JmxvbmdpdHVkZT0ke2xvbmdpdHVkZX1gO1xuXG4gICAgbGV0IGhlYWRlcnMgPSBuZXcgSGVhZGVycyh7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJ1xuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5wb3N0KHVybCwgSlNPTi5zdHJpbmdpZnkoe30pKS50b1Byb21pc2UoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIHN0cmluZyk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIGdldFF1ZXN0aW9ubmFpcmUoc3RlcEluc3RhbmNlSWQ6IG51bWJlcik6IFByb21pc2U8UXVlc3Rpb25uYWlyZT4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgYHJlc3QvcHJvamVjdC9zdGVwSW5zdGFuY2UvcXVlc3Rpb25uYWlyZS8ke3N0ZXBJbnN0YW5jZUlkfWA7XG4gICAgbGV0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJ1xuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQodXJsLCB7aGVhZGVyczogaGVhZGVyc30pLnRvUHJvbWlzZSgpXG4gICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgUXVlc3Rpb25uYWlyZSk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIHVwZGF0ZVF1ZXN0aW9ubmFpcmUoc3RlcEluc3RhbmNlSWQ6IG51bWJlciwgcmVzcG9uc2VzOiBRdWVzdGlvblJlc3BvbnNlW10sIHN1Ym1pdDogYm9vbGVhbiwgc3lzdGVtVXNlcklkOiBudW1iZXIpIHtcbiAgICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybCArICdyZXN0L3Byb2plY3Qvc3RlcEluc3RhbmNlL3F1ZXN0aW9ubmFpcmUvcmVzcG9uc2VzJztcbiAgICBsZXQgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJ1xuICAgIH0pO1xuXG4gICAgbGV0IHF1ZXN0aW9ubmFpcmVSZXNwb25zZSA9IHtcbiAgICAgIHN0ZXBJbnN0YW5jZUlkOiBzdGVwSW5zdGFuY2VJZCxcbiAgICAgIHJlc3BvbnNlczogcmVzcG9uc2VzLFxuICAgICAgc3VibWl0OiBzdWJtaXQsXG4gICAgICBzeXN0ZW1Vc2VySWQ6IHN5c3RlbVVzZXJJZFxuICAgIH07XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LnBvc3QodXJsLCBKU09OLnN0cmluZ2lmeShxdWVzdGlvbm5haXJlUmVzcG9uc2UpLCB7IGhlYWRlcnM6IGhlYWRlcnMgfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBzdHJpbmcpO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxuICBnZXRNZXNzYWdlU3RlcEh0bWwoc3RlcEluc3RhbmNlSWQ6IG51bWJlcik6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBgcmVzdC9wcm9qZWN0L3N0ZXBJbnN0YW5jZS9tZXNzYWdlLyR7c3RlcEluc3RhbmNlSWR9YDtcbiAgICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldCh1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBzdHJpbmcpO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxuICB2ZXJpZnlBY3RvcklkTnIoYnVzaW5lc3NQcm9jZXNzSW5zdGFuY2VJZDogbnVtYmVyLCBpZE5yOiBzdHJpbmcpOiBQcm9taXNlPGFueT4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgJ3Jlc3QvcHJvamVjdC92ZXJpZnlBY3RvcklkTnInO1xuICAgIGxldCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nXG4gICAgfSk7XG5cbiAgICBsZXQgaWROckxvb2tVcCA9IHtcbiAgICAgIGJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlSWQ6IGJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlSWQsXG4gICAgICBpZE5yOiBpZE5yXG4gICAgfTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQucG9zdCh1cmwsIGlkTnJMb29rVXAsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIHN0cmluZyk7XG4gICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgICB9KTtcbiAgfVxuXG4gIGdldFByb2Nlc3NWYXJpYWJsZVZhbHVlKGJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlSWQ6IG51bWJlciwga2V5OiBzdHJpbmcpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgYHJlc3QvcHJvamVjdC9idXNpbmVzc1Byb2Nlc3NJbnN0YW5jZS8ke2J1c2luZXNzUHJvY2Vzc0luc3RhbmNlSWR9LyR7a2V5fWA7XG4gICAgY29uc3QgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQodXJsLCB7aGVhZGVyczogaGVhZGVyc30pLnRvUHJvbWlzZSgpXG4gICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgc3RyaW5nKTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgc2V0UHJvY2Vzc1ZhcmlhYmxlVmFsdWUoYnVzaW5lc3NQcm9jZXNzSW5zdGFuY2VJZDogbnVtYmVyLCBrZXk6IHN0cmluZywgdmFsdWU6IHN0cmluZyk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBgcmVzdC9wcm9qZWN0L3NldFByb2Nlc3NWYXJpYWJsZVZhbHVlLyR7YnVzaW5lc3NQcm9jZXNzSW5zdGFuY2VJZH0vJHtrZXl9LyR7dmFsdWV9YDtcbiAgICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldCh1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBzdHJpbmcpO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxuICB1cGRhdGVBY3RvcihhY3RvcjogQWN0b3IpOiBQcm9taXNlPGFueT4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgJ3Jlc3QvcHJvamVjdC91cGRhdGVBY3Rvcic7XG4gICAgbGV0IGhlYWRlcnMgPSBuZXcgSGVhZGVycyh7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJ1xuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5wb3N0KHVybCwgYWN0b3IpLnRvUHJvbWlzZSgpXG4gICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgc3RyaW5nKTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgZ2V0VGltZVNsb3RzKHBhcnRpY2lwYW50SWQ6IG51bWJlciwgcHJvdmlkZXJJZDogbnVtYmVyLCB0aW1lU2xvdFR5cGU6IHN0cmluZyk6IFByb21pc2U8VGltZVNsb3RbXT4ge1xuICAgIGlmICghcGFydGljaXBhbnRJZClcbiAgICAgICAgcGFydGljaXBhbnRJZCA9IC0xO1xuXG4gICAgaWYgKCFwcm92aWRlcklkKVxuICAgICAgICBwcm92aWRlcklkID0gLTE7XG5cbiAgICBpZiAoIXRpbWVTbG90VHlwZSlcbiAgICAgICAgdGltZVNsb3RUeXBlID0gbnVsbDtcblxuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgYHJlc3QvcHJvamVjdC90aW1lU2xvdHMvJHtwYXJ0aWNpcGFudElkfS8ke3Byb3ZpZGVySWR9LyR7dGltZVNsb3RUeXBlfWA7XG4gICAgY29uc3QgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQodXJsLCB7aGVhZGVyczogaGVhZGVyc30pLnRvUHJvbWlzZSgpXG4gICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgVGltZVNsb3RbXSk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIHVwZGF0ZVRpbWVTbG90cyh0aW1lU2xvdHM6IFRpbWVTbG90W10pOiBQcm9taXNlPGFueT4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgJ3Jlc3QvcHJvamVjdC91cGRhdGVUaW1lU2xvdHMnO1xuICAgIGxldCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJ1xuICAgIH0pO1xuXG4gICAgbGV0IHBhcmFtcyA9IHtcbiAgICAgICAgdGltZVNsb3RzOiB0aW1lU2xvdHNcbiAgICB9O1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5wb3N0KHVybCwgcGFyYW1zLCB7aGVhZGVyczogaGVhZGVyc30pLnRvUHJvbWlzZSgpXG4gICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBUaW1lU2xvdFtdKTtcbiAgICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgIH0pO1xuICB9XG5cbiAgY3JlYXRlU3lzdGVtVXNlcih1c2VybmFtZTogc3RyaW5nLCBwYXNzd29yZDogc3RyaW5nLCBhY3RvcklkOiBudW1iZXIpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgYHJlc3QvbG9naW4vY3JlYXRlVXNlci8ke3VzZXJuYW1lfS8ke3Bhc3N3b3JkfS8ke2FjdG9ySWR9YDtcbiAgICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldCh1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBzdHJpbmcpO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxuICB1cGRhdGVGYWNpYWxBdXRoZW50aWNhdGlvblJlZmVyZW5jZShzdGVwSW5zdGFuY2VJZDogbnVtYmVyLCBpbWFnZURhdGE6IHN0cmluZyk6IFByb21pc2U8YW55PiB7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyAncmVzdC9wcm9qZWN0L2ZhY2lhbEF1dGgvcmVmZXJlbmNlJztcbiAgICBsZXQgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbidcbiAgICB9KTtcblxuICAgIGxldCBwYXJhbXMgPSB7XG4gICAgICAgIHN0ZXBJbnN0YW5jZUlkOiBzdGVwSW5zdGFuY2VJZCxcbiAgICAgICAgaW1hZ2VEYXRhOiBpbWFnZURhdGFcbiAgICB9O1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5wb3N0KHVybCwgcGFyYW1zLCB7aGVhZGVyczogaGVhZGVyc30pLnRvUHJvbWlzZSgpXG4gICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBUaW1lU2xvdFtdKTtcbiAgICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgIH0pO1xuICB9XG5cbiAgZmFjaWFsQXV0aGVudGljYXRpb24oc3RlcEluc3RhbmNlSWQ6IG51bWJlciwgaW1hZ2VEYXRhOiBzdHJpbmcpOiBQcm9taXNlPGFueT4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgJ3Jlc3QvcHJvamVjdC9mYWNpYWxBdXRoL2F1dGhlbnRpY2F0ZSc7XG4gICAgbGV0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nXG4gICAgfSk7XG5cbiAgICBsZXQgcGFyYW1zID0ge1xuICAgICAgICBzdGVwSW5zdGFuY2VJZDogc3RlcEluc3RhbmNlSWQsXG4gICAgICAgIGltYWdlRGF0YTogaW1hZ2VEYXRhXG4gICAgfTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQucG9zdCh1cmwsIHBhcmFtcywge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgVGltZVNsb3RbXSk7XG4gICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgICB9KTtcbiAgfVxuXG4gIGdldFByb2Nlc3NIaXN0b3J5KHByb2plY3RHdWlkOiBzdHJpbmcsIGFjdG9ySWQ6IG51bWJlcik6IFByb21pc2U8QXJyYXk8U3RlcEluc3RhbmNlPj4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgYHJlc3QvcHJvamVjdC9zdGVwSW5zdGFuY2UvcHJvZ3Jlc3MvJHtwcm9qZWN0R3VpZH0vJHthY3RvcklkfWA7XG4gICAgY29uc3QgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQodXJsLCB7aGVhZGVyczogaGVhZGVyc30pLnRvUHJvbWlzZSgpXG4gICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgQXJyYXk8U3RlcEluc3RhbmNlPik7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIGdldEFjdGl2ZVN0ZXBJbnN0YW5jZXMoc3RlcElkczogQXJyYXk8bnVtYmVyPik6IFByb21pc2U8QXJyYXk8U3RlcEluc3RhbmNlPj4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgJ3Jlc3QvcHJvamVjdC9zdGVwSW5zdGFuY2UvYWN0aXZlJztcbiAgICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgfSk7XG5cbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHN0ZXBJZHMubGVuZ3RoOyBpKyspIHtcbiAgICAgIGlmIChpID09PSAwKSB7XG4gICAgICAgIHVybCArPSBcIj9zdGVwSWRzPVwiICsgc3RlcElkc1tpXTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHVybCArPSBcIiZzdGVwSWRzPVwiICsgc3RlcElkc1tpXTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldCh1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBBcnJheTxTdGVwSW5zdGFuY2U+KTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgaXNTdGVwSW5zdGFuY2VBY3RpdmUoc3RlcElkOiBudW1iZXIsIGV4dGVybmFsSWQ6IHN0cmluZyk6IFByb21pc2U8YW55PiB7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyAncmVzdC9wcm9qZWN0L3N0ZXBJbnN0YW5jZS9hY3RpdmUvJyArIHN0ZXBJZCArIFwiL1wiICsgZXh0ZXJuYWxJZDtcbiAgICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldCh1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxufVxuIl19