/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { ProjectService } from '../services/project.service';
import { ActorService } from '../services/actor.service';
import { AcumenConfiguration } from '../services/acumen-configuration';
var InTrayAlertComponent = /** @class */ (function () {
    function InTrayAlertComponent(projectService, actorService, acumenConfiguration) {
        this.projectService = projectService;
        this.actorService = actorService;
        this.acumenConfiguration = acumenConfiguration;
        this.onSelected = new EventEmitter();
        this.blink = false;
        if (acumenConfiguration.backendServiceUrl) {
            this.serviceUrl = acumenConfiguration.backendServiceUrl;
        }
        else {
            this.serviceUrl = "http://www.healthacumen.co.za/insight/";
        }
    }
    /**
     * @return {?}
     */
    InTrayAlertComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.username) {
            this.actorService.getSystemUser(this.username).then((/**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                if (result && result.actor) {
                    _this.assigneeId = result.actor.id;
                    _this.startPolling();
                }
            }));
        }
        else {
            this.startPolling();
        }
    };
    /**
     * @return {?}
     */
    InTrayAlertComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    };
    /**
     * @return {?}
     */
    InTrayAlertComponent.prototype.startPolling = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var interval = this.checkInterval;
        if (!interval || interval < 5000) {
            interval = 5000;
        }
        /** @type {?} */
        var timer = Observable.timer(0, interval);
        this.subscription = timer.subscribe((/**
         * @param {?} t
         * @return {?}
         */
        function (t) {
            _this.projectService.getLatestStepAssignmentDate(_this.assigneeId).then((/**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                // console.log(result);
                if (result) {
                    /** @type {?} */
                    var t_1 = new Date().getTime() - new Date(result).getTime();
                    _this.blink = t_1 < _this.alertTime;
                }
                else {
                    _this.blink = false;
                }
            }));
        }));
    };
    /**
     * @return {?}
     */
    InTrayAlertComponent.prototype.selected = /**
     * @return {?}
     */
    function () {
        this.onSelected.emit();
    };
    InTrayAlertComponent.decorators = [
        { type: Component, args: [{
                    selector: 'acn-in-tray-alert',
                    template: "<div class=\"in-tray-alert-component\" (click)=\"selected()\">\n  <div class=\"in-tray-image\">\n    <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"8\" height=\"8\" viewBox=\"0 0 8 8\" fill=\"#fff\">\n      <path d=\"M.19 0c-.11 0-.19.08-.19.19v7.63c0 .11.08.19.19.19h7.63c.11 0 .19-.08.19-.19v-7.63c0-.11-.08-.19-.19-.19h-7.63zm.81 2h6v3h-1l-1 1h-2l-1-1h-1v-3z\"/>\n    </svg>\n  </div>\n  <div *ngIf=\"blink\" class=\"warning-image\">\n    <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"8\" height=\"8\" viewBox=\"0 0 8 8\" fill=\"red\">\n      <path d=\"M3.09 0c-.06 0-.1.04-.13.09l-2.94 6.81c-.02.05-.03.13-.03.19v.81c0 .05.04.09.09.09h6.81c.05 0 .09-.04.09-.09v-.81c0-.05-.01-.14-.03-.19l-2.94-6.81c-.02-.05-.07-.09-.13-.09h-.81zm-.09 3h1v2h-1v-2zm0 3h1v1h-1v-1z\" />\n    </svg>\n  </div>\n</div>\n",
                    styles: [".in-tray-alert-component{width:100%;height:100%;position:relative;cursor:pointer}.in-tray-alert-component .in-tray-image,.in-tray-alert-component .in-tray-image svg{width:100%;height:100%}.in-tray-alert-component .warning-image{position:absolute;width:60%;height:60%;left:30%;top:0;animation:1s steps(5,start) infinite blink-animation;-webkit-animation:1s steps(5,start) infinite blink-animation}.in-tray-alert-component .warning-image svg{width:100%;height:100%}@keyframes blink-animation{to{opacity:0}}@-webkit-keyframes blink-animation{to{opacity:0}}"]
                }] }
    ];
    /** @nocollapse */
    InTrayAlertComponent.ctorParameters = function () { return [
        { type: ProjectService },
        { type: ActorService },
        { type: AcumenConfiguration }
    ]; };
    InTrayAlertComponent.propDecorators = {
        assigneeId: [{ type: Input }],
        username: [{ type: Input }],
        checkInterval: [{ type: Input }],
        alertTime: [{ type: Input }],
        onSelected: [{ type: Output }]
    };
    return InTrayAlertComponent;
}());
export { InTrayAlertComponent };
if (false) {
    /** @type {?} */
    InTrayAlertComponent.prototype.assigneeId;
    /** @type {?} */
    InTrayAlertComponent.prototype.username;
    /** @type {?} */
    InTrayAlertComponent.prototype.checkInterval;
    /** @type {?} */
    InTrayAlertComponent.prototype.alertTime;
    /** @type {?} */
    InTrayAlertComponent.prototype.onSelected;
    /**
     * @type {?}
     * @private
     */
    InTrayAlertComponent.prototype.subscription;
    /**
     * @type {?}
     * @private
     */
    InTrayAlertComponent.prototype.serviceUrl;
    /** @type {?} */
    InTrayAlertComponent.prototype.blink;
    /**
     * @type {?}
     * @private
     */
    InTrayAlertComponent.prototype.projectService;
    /**
     * @type {?}
     * @private
     */
    InTrayAlertComponent.prototype.actorService;
    /**
     * @type {?}
     * @private
     */
    InTrayAlertComponent.prototype.acumenConfiguration;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW4tdHJheS1hbGVydC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL2luLXRyYXktYWxlcnQvaW4tdHJheS1hbGVydC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQWEsTUFBTSxlQUFlLENBQUM7QUFFMUYsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUVyQyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDN0QsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBR3pELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBRXZFO0lBZ0JFLDhCQUFvQixjQUE4QixFQUFVLFlBQTBCLEVBQzFFLG1CQUF3QztRQURoQyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFBVSxpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxRSx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBUDFDLGVBQVUsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBSTFDLFVBQUssR0FBWSxLQUFLLENBQUM7UUFJckIsSUFBSSxtQkFBbUIsQ0FBQyxpQkFBaUIsRUFBRTtZQUN6QyxJQUFJLENBQUMsVUFBVSxHQUFHLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDO1NBQ3pEO2FBQU07WUFDTCxJQUFJLENBQUMsVUFBVSxHQUFHLHdDQUF3QyxDQUFDO1NBQzVEO0lBQ0gsQ0FBQzs7OztJQUVELHVDQUFROzs7SUFBUjtRQUFBLGlCQVdDO1FBVkMsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2pCLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJOzs7O1lBQUMsVUFBQSxNQUFNO2dCQUN4RCxJQUFJLE1BQU0sSUFBSSxNQUFNLENBQUMsS0FBSyxFQUFFO29CQUMxQixLQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDO29CQUNsQyxLQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7aUJBQ3JCO1lBQ0gsQ0FBQyxFQUFDLENBQUM7U0FDSjthQUFNO1lBQ0wsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1NBQ3JCO0lBQ0gsQ0FBQzs7OztJQUVNLDBDQUFXOzs7SUFBbEI7UUFDRSxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDckIsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNqQztJQUNILENBQUM7Ozs7SUFFRCwyQ0FBWTs7O0lBQVo7UUFBQSxpQkFrQkM7O1lBakJLLFFBQVEsR0FBRyxJQUFJLENBQUMsYUFBYTtRQUNqQyxJQUFJLENBQUMsUUFBUSxJQUFJLFFBQVEsR0FBRyxJQUFJLEVBQUU7WUFDaEMsUUFBUSxHQUFHLElBQUksQ0FBQztTQUNqQjs7WUFFRyxLQUFLLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsUUFBUSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDLFNBQVM7Ozs7UUFBQyxVQUFBLENBQUM7WUFDbkMsS0FBSSxDQUFDLGNBQWMsQ0FBQywyQkFBMkIsQ0FBQyxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSTs7OztZQUFDLFVBQUEsTUFBTTtnQkFDMUUsdUJBQXVCO2dCQUN2QixJQUFJLE1BQU0sRUFBRTs7d0JBQ04sR0FBQyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLEdBQUcsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsT0FBTyxFQUFFO29CQUN6RCxLQUFJLENBQUMsS0FBSyxHQUFHLEdBQUMsR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDO2lCQUNqQztxQkFBTTtvQkFDTCxLQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztpQkFDcEI7WUFDSCxDQUFDLEVBQUMsQ0FBQztRQUNMLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVELHVDQUFROzs7SUFBUjtRQUNFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDekIsQ0FBQzs7Z0JBbEVGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsbUJBQW1CO29CQUM3QixzekJBQTZDOztpQkFFOUM7Ozs7Z0JBVlEsY0FBYztnQkFDZCxZQUFZO2dCQUdaLG1CQUFtQjs7OzZCQVF6QixLQUFLOzJCQUNMLEtBQUs7Z0NBQ0wsS0FBSzs0QkFDTCxLQUFLOzZCQUNMLE1BQU07O0lBeURULDJCQUFDO0NBQUEsQUFuRUQsSUFtRUM7U0E5RFksb0JBQW9COzs7SUFDL0IsMENBQTRCOztJQUM1Qix3Q0FBMEI7O0lBQzFCLDZDQUErQjs7SUFDL0IseUNBQTJCOztJQUMzQiwwQ0FBMEM7Ozs7O0lBQzFDLDRDQUFtQzs7Ozs7SUFFbkMsMENBQW1COztJQUNuQixxQ0FBdUI7Ozs7O0lBRVgsOENBQXNDOzs7OztJQUFFLDRDQUFrQzs7Ozs7SUFDbEYsbURBQWdEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBFdmVudEVtaXR0ZXIsIE91dHB1dCwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzL1J4JztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzL1J4JztcblxuaW1wb3J0IHsgUHJvamVjdFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9wcm9qZWN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgQWN0b3JTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvYWN0b3Iuc2VydmljZSc7XG5pbXBvcnQgeyBTdGVwSW5zdGFuY2UgfSBmcm9tICcuLi9kb21haW4vc3RlcC1pbnN0YW5jZSc7XG5pbXBvcnQgeyBTeXN0ZW1Vc2VyIH0gZnJvbSAnLi4vZG9tYWluL3N5c3RlbS11c2VyJztcbmltcG9ydCB7IEFjdW1lbkNvbmZpZ3VyYXRpb24gfSBmcm9tICcuLi9zZXJ2aWNlcy9hY3VtZW4tY29uZmlndXJhdGlvbic7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2Fjbi1pbi10cmF5LWFsZXJ0JyxcbiAgdGVtcGxhdGVVcmw6ICcuL2luLXRyYXktYWxlcnQuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9pbi10cmF5LWFsZXJ0LmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgSW5UcmF5QWxlcnRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIGFzc2lnbmVlSWQ6IG51bWJlcjtcbiAgQElucHV0KCkgdXNlcm5hbWU6IHN0cmluZztcbiAgQElucHV0KCkgY2hlY2tJbnRlcnZhbDogbnVtYmVyO1xuICBASW5wdXQoKSBhbGVydFRpbWU6IG51bWJlcjtcbiAgQE91dHB1dCgpIG9uU2VsZWN0ZWQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIHByaXZhdGUgc3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XG4gIC8vIHNlcnZlclVybCA9ICdodHRwOi8vd3d3LmhlYWx0aGFjdW1lbi5jby56YS9pbnNpZ2h0Lyc7XG4gIHByaXZhdGUgc2VydmljZVVybDtcbiAgYmxpbms6IGJvb2xlYW4gPSBmYWxzZTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHByb2plY3RTZXJ2aWNlOiBQcm9qZWN0U2VydmljZSwgcHJpdmF0ZSBhY3RvclNlcnZpY2U6IEFjdG9yU2VydmljZSxcbiAgICAgIHByaXZhdGUgYWN1bWVuQ29uZmlndXJhdGlvbjogQWN1bWVuQ29uZmlndXJhdGlvbikge1xuICAgIGlmIChhY3VtZW5Db25maWd1cmF0aW9uLmJhY2tlbmRTZXJ2aWNlVXJsKSB7XG4gICAgICB0aGlzLnNlcnZpY2VVcmwgPSBhY3VtZW5Db25maWd1cmF0aW9uLmJhY2tlbmRTZXJ2aWNlVXJsO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNlcnZpY2VVcmwgPSBcImh0dHA6Ly93d3cuaGVhbHRoYWN1bWVuLmNvLnphL2luc2lnaHQvXCI7XG4gICAgfVxuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgaWYgKHRoaXMudXNlcm5hbWUpIHtcbiAgICAgIHRoaXMuYWN0b3JTZXJ2aWNlLmdldFN5c3RlbVVzZXIodGhpcy51c2VybmFtZSkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICBpZiAocmVzdWx0ICYmIHJlc3VsdC5hY3Rvcikge1xuICAgICAgICAgIHRoaXMuYXNzaWduZWVJZCA9IHJlc3VsdC5hY3Rvci5pZDtcbiAgICAgICAgICB0aGlzLnN0YXJ0UG9sbGluZygpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zdGFydFBvbGxpbmcoKTtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgaWYgKHRoaXMuc3Vic2NyaXB0aW9uKSB7XG4gICAgICB0aGlzLnN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xuICAgIH1cbiAgfVxuXG4gIHN0YXJ0UG9sbGluZygpIHtcbiAgICBsZXQgaW50ZXJ2YWwgPSB0aGlzLmNoZWNrSW50ZXJ2YWw7XG4gICAgaWYgKCFpbnRlcnZhbCB8fCBpbnRlcnZhbCA8IDUwMDApIHtcbiAgICAgIGludGVydmFsID0gNTAwMDtcbiAgICB9XG5cbiAgICBsZXQgdGltZXIgPSBPYnNlcnZhYmxlLnRpbWVyKDAsIGludGVydmFsKTtcbiAgICB0aGlzLnN1YnNjcmlwdGlvbiA9IHRpbWVyLnN1YnNjcmliZSh0ID0+IHtcbiAgICAgIHRoaXMucHJvamVjdFNlcnZpY2UuZ2V0TGF0ZXN0U3RlcEFzc2lnbm1lbnREYXRlKHRoaXMuYXNzaWduZWVJZCkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICAvLyBjb25zb2xlLmxvZyhyZXN1bHQpO1xuICAgICAgICBpZiAocmVzdWx0KSB7XG4gICAgICAgICAgbGV0IHQgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKSAtIG5ldyBEYXRlKHJlc3VsdCkuZ2V0VGltZSgpO1xuICAgICAgICAgIHRoaXMuYmxpbmsgPSB0IDwgdGhpcy5hbGVydFRpbWU7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGhpcy5ibGluayA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIHNlbGVjdGVkKCkge1xuICAgIHRoaXMub25TZWxlY3RlZC5lbWl0KCk7XG4gIH1cbn1cbiJdfQ==