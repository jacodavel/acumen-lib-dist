/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { ProjectService } from '../services/project.service';
import { ActorService } from '../services/actor.service';
import { AcumenConfiguration } from '../services/acumen-configuration';
var InTrayComponent = /** @class */ (function () {
    function InTrayComponent(projectService, actorService, acumenConfiguration) {
        this.projectService = projectService;
        this.actorService = actorService;
        this.acumenConfiguration = acumenConfiguration;
        this.onStepInstanceSelected = new EventEmitter();
        this.stepInstances = [];
        this.loading = false;
        this.selectedStepInstance = null;
        if (acumenConfiguration.backendServiceUrl) {
            this.serverUrl = acumenConfiguration.backendServiceUrl;
        }
        else {
            this.serverUrl = "http://www.healthacumen.co.za/insight/";
        }
    }
    Object.defineProperty(InTrayComponent.prototype, "latestStepCompletionDate", {
        set: /**
         * @param {?} d
         * @return {?}
         */
        function (d) {
            // alert("Dates: " + d + " -> " + this._latestStepCompletionDate);
            if (new Date(d).getTime() > new Date(this._latestStepCompletionDate).getTime()) {
                // alert("refresh");
                this.refreshInTray();
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    InTrayComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.username) {
            this.actorService.getSystemUser(this.username).then((/**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                _this.systemUser = result;
                if (result && result.actor) {
                    // this.userId = result.id;
                    _this.userId = result.actor.id;
                    _this.assigneeId = result.actor.id;
                    _this.refreshInTray();
                    _this.startPolling();
                }
            }));
        }
        else {
            this.refreshInTray();
            this.startPolling();
        }
    };
    /**
     * @return {?}
     */
    InTrayComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    };
    /**
     * @private
     * @return {?}
     */
    InTrayComponent.prototype.startPolling = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var timer = Observable.timer(0, 10000);
        this.subscription = timer.subscribe((/**
         * @param {?} t
         * @return {?}
         */
        function (t) {
            // this.projectService.getLatestStepAssignmentDate(this.assigneeId).then(result => {
            //   if (result) {
            //     if (this._latestStepCompletionDate) {
            //       let t = new Date(this._latestStepCompletionDate).getTime() - new Date(result).getTime();
            //       if (t < 0) {
            _this.refreshInTray();
            //       }
            //     }
            //   }
            // });
        }));
    };
    /**
     * @return {?}
     */
    InTrayComponent.prototype.refreshInTray = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this._latestStepCompletionDate = new Date();
        this.loading = true;
        this.selectedStepInstance = null;
        this.projectService.getStepInstances2(this.projectGuid, this.userId, this.assigneeId, this.participantId).then((/**
         * @param {?} result
         * @return {?}
         */
        function (result) {
            var e_1, _a;
            /** @type {?} */
            var stepInstances = [];
            try {
                for (var result_1 = tslib_1.__values(result), result_1_1 = result_1.next(); !result_1_1.done; result_1_1 = result_1.next()) {
                    var s = result_1_1.value;
                    if (s.stepInstanceStatus === 'WAITING' && s.step.stepType !== "CANCEL_WIZARD") {
                        stepInstances.push(s);
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (result_1_1 && !result_1_1.done && (_a = result_1.return)) _a.call(result_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
            stepInstances.sort((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            function (a, b) {
                return new Date(b.activationDate).getTime() - new Date(a.activationDate).getTime();
            }));
            _this.stepInstances = stepInstances;
            _this.loading = false;
        }));
    };
    /**
     * @param {?} stepInstance
     * @return {?}
     */
    InTrayComponent.prototype.stepInstanceSelected = /**
     * @param {?} stepInstance
     * @return {?}
     */
    function (stepInstance) {
        this.selectedStepInstance = stepInstance;
        if (this.selectedStepInstance) {
            this.selectedStepInstance.systemUser = this.systemUser;
        }
        this.onStepInstanceSelected.emit(stepInstance);
    };
    /**
     * @param {?} stepInstance
     * @return {?}
     */
    InTrayComponent.prototype.blinkRow = /**
     * @param {?} stepInstance
     * @return {?}
     */
    function (stepInstance) {
        return new Date().getTime() - new Date(stepInstance.activationDate).getTime() < 1000 * 60 * 3;
    };
    InTrayComponent.decorators = [
        { type: Component, args: [{
                    selector: 'acn-in-tray',
                    template: "<div class=\"in-tray-component\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n\n  <div class=\"step-list\">\n    <div *ngFor=\"let stepInstance of stepInstances; odd as isOdd\">\n      <div class=\"in-tray-row\" [ngClass]=\"{ 'oddRow': isOdd, 'selectedRow': selectedStepInstance && selectedStepInstance.id === stepInstance.id }\"\n          (click)=\"stepInstanceSelected(stepInstance)\">\n        <div class=\"project-image\">\n          <img src=\"{{ serverUrl }}spring/projectIcon/image.png?id={{ stepInstance.businessProcessInstance.businessProcess.project.id }}\">\n        </div>\n        <div class=\"description\" [ngClass]=\"{ 'blink': blinkRow(stepInstance) }\">\n          <div class=\"project\">\n            {{ stepInstance.businessProcessInstance.actor.firstname }} {{ stepInstance.businessProcessInstance.actor.surname }},\n            Id: {{ stepInstance.businessProcessInstance.actor.idNr }}\n          </div>\n          <div class=\"step-details\">\n            <div class=\"step\">\n              {{ stepInstance.businessProcessInstance.businessProcess.project.projectName }}: {{ stepInstance.step.stepType }}\n            </div>\n            <div class=\"activation-date\">\n              ({{ stepInstance.activationDate | date: 'yyyy-MM-dd HH:mm:ss' }})\n            </div>\n          </div>\n        </div>\n        <div class=\"cursor-image\">\n          <img src=\"data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAUVJREFUeJzt2EtOw0AURNELbCw7o9hZdgaTtBigBLv9+vM+JfUscnyPZw220+OknIDvx9HSN1kw8RufDkH8jU+DIJ7Hh0cQ/8eHRRDH48MhiPPxYRBEf7x7BHE93i2CsIt3hyDs47dBeF/8/58sRvg48Js78AbcBr3D7fH8+6Dnv9wRAAiMcBQAgiKcAYCACGcBIBhCDwAEQugFgCAIVwAgAMJVAHCOYAEAjhGsAMApgiUAOESwBgBnCCMAwBHCKABwgjASABwgjAaAzRFmAMDGCLMAYFOE1XeCqSbG3S5vccP8aqLiK77ijY9mhfRMVHzFV7zx0ayQnomKr/iKNz6aFdIzUfEVX/      HGR7NCeiYqPmb86jvBLzb/+m0i2JfvmUgc3yYSx7eJxPFtInF8m0gc3yYSx7eJxPFtInF8m0gc3yacxf8AAP323bWbSe8AAAAASUVORK5CYII=\">\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n",
                    styles: [".in-tray-component{width:100%;height:100%;position:relative}.in-tray-component .step-list{width:100%;height:100%;overflow-y:scroll}.in-tray-component .step-list .in-tray-row{display:table;width:100%;padding:5px;cursor:pointer}.in-tray-component .step-list .in-tray-row .project-image{display:table-cell;width:32px;text-align:center;vertical-align:middle}.in-tray-component .step-list .in-tray-row .project-image img{width:24px;height:24px}.in-tray-component .step-list .in-tray-row .description{display:table-cell;width:calc(100% - 64px);padding-left:10px;vertical-align:middle}.in-tray-component .step-list .in-tray-row .description .project{font-size:14px}.in-tray-component .step-list .in-tray-row .description .step-details{line-height:15px}.in-tray-component .step-list .in-tray-row .description .step-details .step{font-size:15px}.in-tray-component .step-list .in-tray-row .description .step-details .activation-date{font-size:10px}.in-tray-component .step-list .in-tray-row .blink{animation:1s steps(5,start) infinite blink-animation;-webkit-animation:1s steps(5,start) infinite blink-animation}@keyframes blink-animation{to{color:red}}@-webkit-keyframes blink-animation{to{color:red}}.in-tray-component .step-list .in-tray-row .cursor-image{display:table-cell;width:32px;text-align:center;vertical-align:middle}.in-tray-component .step-list .in-tray-row .cursor-image img{width:16px;height:16px}.in-tray-component .step-list .oddRow{background-color:#f2f2f2}.in-tray-component .step-list .selectedRow{background-color:#abb9d3}"]
                }] }
    ];
    /** @nocollapse */
    InTrayComponent.ctorParameters = function () { return [
        { type: ProjectService },
        { type: ActorService },
        { type: AcumenConfiguration }
    ]; };
    InTrayComponent.propDecorators = {
        projectGuid: [{ type: Input }],
        userId: [{ type: Input }],
        assigneeId: [{ type: Input }],
        participantId: [{ type: Input }],
        username: [{ type: Input }],
        onStepInstanceSelected: [{ type: Output }],
        latestStepCompletionDate: [{ type: Input }]
    };
    return InTrayComponent;
}());
export { InTrayComponent };
if (false) {
    /** @type {?} */
    InTrayComponent.prototype.projectGuid;
    /** @type {?} */
    InTrayComponent.prototype.userId;
    /** @type {?} */
    InTrayComponent.prototype.assigneeId;
    /** @type {?} */
    InTrayComponent.prototype.participantId;
    /** @type {?} */
    InTrayComponent.prototype.username;
    /** @type {?} */
    InTrayComponent.prototype.onStepInstanceSelected;
    /** @type {?} */
    InTrayComponent.prototype.stepInstances;
    /** @type {?} */
    InTrayComponent.prototype.serverUrl;
    /** @type {?} */
    InTrayComponent.prototype.loading;
    /** @type {?} */
    InTrayComponent.prototype.selectedStepInstance;
    /**
     * @type {?}
     * @private
     */
    InTrayComponent.prototype._latestStepCompletionDate;
    /**
     * @type {?}
     * @private
     */
    InTrayComponent.prototype.systemUser;
    /**
     * @type {?}
     * @private
     */
    InTrayComponent.prototype.subscription;
    /**
     * @type {?}
     * @private
     */
    InTrayComponent.prototype.projectService;
    /**
     * @type {?}
     * @private
     */
    InTrayComponent.prototype.actorService;
    /**
     * @type {?}
     * @private
     */
    InTrayComponent.prototype.acumenConfiguration;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW4tdHJheS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL2luLXRyYXkvaW4tdHJheS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUFhLE1BQU0sZUFBZSxDQUFDO0FBRTFGLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFFckMsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQzdELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUd6RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUV2RTtJQXFCRSx5QkFBb0IsY0FBOEIsRUFBVSxZQUEwQixFQUMxRSxtQkFBd0M7UUFEaEMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQVUsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDMUUsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQVgxQywyQkFBc0IsR0FBRyxJQUFJLFlBQVksRUFBZ0IsQ0FBQztRQUNwRSxrQkFBYSxHQUF3QixFQUFFLENBQUM7UUFHeEMsWUFBTyxHQUFZLEtBQUssQ0FBQztRQUN6Qix5QkFBb0IsR0FBaUIsSUFBSSxDQUFDO1FBT3hDLElBQUksbUJBQW1CLENBQUMsaUJBQWlCLEVBQUU7WUFDekMsSUFBSSxDQUFDLFNBQVMsR0FBRyxtQkFBbUIsQ0FBQyxpQkFBaUIsQ0FBQztTQUN4RDthQUFNO1lBQ0wsSUFBSSxDQUFDLFNBQVMsR0FBRyx3Q0FBd0MsQ0FBQztTQUMzRDtJQUNILENBQUM7SUFFRCxzQkFDSSxxREFBd0I7Ozs7O1FBRDVCLFVBQzZCLENBQU87WUFDbEMsa0VBQWtFO1lBQ2xFLElBQUksSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFLEdBQUcsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLENBQUMsT0FBTyxFQUFFLEVBQUU7Z0JBQzlFLG9CQUFvQjtnQkFDcEIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO2FBQ3RCO1FBQ0gsQ0FBQzs7O09BQUE7Ozs7SUFFRCxrQ0FBUTs7O0lBQVI7UUFBQSxpQkFnQkM7UUFmQyxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakIsSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUk7Ozs7WUFBQyxVQUFBLE1BQU07Z0JBQ3hELEtBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDO2dCQUN6QixJQUFJLE1BQU0sSUFBSSxNQUFNLENBQUMsS0FBSyxFQUFFO29CQUMxQiwyQkFBMkI7b0JBQzNCLEtBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUM7b0JBQzlCLEtBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUM7b0JBQ2xDLEtBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztvQkFDckIsS0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDO2lCQUNyQjtZQUNILENBQUMsRUFBQyxDQUFDO1NBQ0o7YUFBTTtZQUNMLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztZQUNyQixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7U0FDckI7SUFDSCxDQUFDOzs7O0lBRU0scUNBQVc7OztJQUFsQjtRQUNFLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNyQixJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ2pDO0lBQ0gsQ0FBQzs7Ozs7SUFFTyxzQ0FBWTs7OztJQUFwQjtRQUFBLGlCQWNDOztZQWJLLEtBQUssR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUM7UUFDdEMsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUMsU0FBUzs7OztRQUFDLFVBQUEsQ0FBQztZQUNuQyxvRkFBb0Y7WUFDcEYsa0JBQWtCO1lBQ2xCLDRDQUE0QztZQUM1QyxpR0FBaUc7WUFDakcscUJBQXFCO1lBQ2IsS0FBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQzdCLFVBQVU7WUFDVixRQUFRO1lBQ1IsTUFBTTtZQUNOLE1BQU07UUFDUixDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7SUFFRCx1Q0FBYTs7O0lBQWI7UUFBQSxpQkFtQkM7UUFsQkMsSUFBSSxDQUFDLHlCQUF5QixHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7UUFDNUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQztRQUNqQyxJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxJQUFJOzs7O1FBQUMsVUFBQSxNQUFNOzs7Z0JBQy9HLGFBQWEsR0FBd0IsRUFBRTs7Z0JBQzNDLEtBQWMsSUFBQSxXQUFBLGlCQUFBLE1BQU0sQ0FBQSw4QkFBQSxrREFBRTtvQkFBakIsSUFBSSxDQUFDLG1CQUFBO29CQUNSLElBQUksQ0FBQyxDQUFDLGtCQUFrQixLQUFLLFNBQVMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsS0FBSyxlQUFlLEVBQUU7d0JBQzdFLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7cUJBQ3ZCO2lCQUNGOzs7Ozs7Ozs7WUFFRCxhQUFhLENBQUMsSUFBSTs7Ozs7WUFBQyxVQUFDLENBQUMsRUFBRSxDQUFDO2dCQUN0QixPQUFPLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxPQUFPLEVBQUUsR0FBRyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDckYsQ0FBQyxFQUFDLENBQUM7WUFFSCxLQUFJLENBQUMsYUFBYSxHQUFHLGFBQWEsQ0FBQztZQUNuQyxLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUN2QixDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsOENBQW9COzs7O0lBQXBCLFVBQXFCLFlBQTBCO1FBQzdDLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxZQUFZLENBQUM7UUFDekMsSUFBSSxJQUFJLENBQUMsb0JBQW9CLEVBQUU7WUFDN0IsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1NBQ3hEO1FBRUQsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUNqRCxDQUFDOzs7OztJQUVELGtDQUFROzs7O0lBQVIsVUFBUyxZQUEwQjtRQUNqQyxPQUFPLElBQUksSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLEdBQUcsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxHQUFHLElBQUksR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ2hHLENBQUM7O2dCQS9HRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGFBQWE7b0JBQ3ZCLHloRUFBdUM7O2lCQUV4Qzs7OztnQkFWUSxjQUFjO2dCQUNkLFlBQVk7Z0JBR1osbUJBQW1COzs7OEJBUXpCLEtBQUs7eUJBQ0wsS0FBSzs2QkFDTCxLQUFLO2dDQUNMLEtBQUs7MkJBQ0wsS0FBSzt5Q0FDTCxNQUFNOzJDQW1CTixLQUFLOztJQWtGUixzQkFBQztDQUFBLEFBaEhELElBZ0hDO1NBM0dZLGVBQWU7OztJQUMxQixzQ0FBNkI7O0lBQzdCLGlDQUF3Qjs7SUFDeEIscUNBQTRCOztJQUM1Qix3Q0FBK0I7O0lBQy9CLG1DQUEwQjs7SUFDMUIsaURBQW9FOztJQUNwRSx3Q0FBd0M7O0lBRXhDLG9DQUFVOztJQUNWLGtDQUF5Qjs7SUFDekIsK0NBQTBDOzs7OztJQUMxQyxvREFBd0M7Ozs7O0lBQ3hDLHFDQUErQjs7Ozs7SUFDL0IsdUNBQW1DOzs7OztJQUV2Qix5Q0FBc0M7Ozs7O0lBQUUsdUNBQWtDOzs7OztJQUNsRiw4Q0FBZ0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIEV2ZW50RW1pdHRlciwgT3V0cHV0LCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMvUngnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMvUngnO1xuXG5pbXBvcnQgeyBQcm9qZWN0U2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL3Byb2plY3Quc2VydmljZSc7XG5pbXBvcnQgeyBBY3RvclNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9hY3Rvci5zZXJ2aWNlJztcbmltcG9ydCB7IFN0ZXBJbnN0YW5jZSB9IGZyb20gJy4uL2RvbWFpbi9zdGVwLWluc3RhbmNlJztcbmltcG9ydCB7IFN5c3RlbVVzZXIgfSBmcm9tICcuLi9kb21haW4vc3lzdGVtLXVzZXInO1xuaW1wb3J0IHsgQWN1bWVuQ29uZmlndXJhdGlvbiB9IGZyb20gJy4uL3NlcnZpY2VzL2FjdW1lbi1jb25maWd1cmF0aW9uJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYWNuLWluLXRyYXknLFxuICB0ZW1wbGF0ZVVybDogJy4vaW4tdHJheS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2luLXRyYXkuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBJblRyYXlDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIHByb2plY3RHdWlkOiBzdHJpbmc7XG4gIEBJbnB1dCgpIHVzZXJJZDogbnVtYmVyO1xuICBASW5wdXQoKSBhc3NpZ25lZUlkOiBudW1iZXI7XG4gIEBJbnB1dCgpIHBhcnRpY2lwYW50SWQ6IG51bWJlcjtcbiAgQElucHV0KCkgdXNlcm5hbWU6IHN0cmluZztcbiAgQE91dHB1dCgpIG9uU3RlcEluc3RhbmNlU2VsZWN0ZWQgPSBuZXcgRXZlbnRFbWl0dGVyPFN0ZXBJbnN0YW5jZT4oKTtcbiAgc3RlcEluc3RhbmNlczogQXJyYXk8U3RlcEluc3RhbmNlPiA9IFtdO1xuICAvLyBzZXJ2ZXJVcmwgPSAnaHR0cDovL3d3dy5oZWFsdGhhY3VtZW4uY28uemEvaW5zaWdodC8nO1xuICBzZXJ2ZXJVcmw7XG4gIGxvYWRpbmc6IGJvb2xlYW4gPSBmYWxzZTtcbiAgc2VsZWN0ZWRTdGVwSW5zdGFuY2U6IFN0ZXBJbnN0YW5jZSA9IG51bGw7XG4gIHByaXZhdGUgX2xhdGVzdFN0ZXBDb21wbGV0aW9uRGF0ZTogRGF0ZTtcbiAgcHJpdmF0ZSBzeXN0ZW1Vc2VyOiBTeXN0ZW1Vc2VyO1xuICBwcml2YXRlIHN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcHJvamVjdFNlcnZpY2U6IFByb2plY3RTZXJ2aWNlLCBwcml2YXRlIGFjdG9yU2VydmljZTogQWN0b3JTZXJ2aWNlLFxuICAgICAgcHJpdmF0ZSBhY3VtZW5Db25maWd1cmF0aW9uOiBBY3VtZW5Db25maWd1cmF0aW9uKSB7XG4gICAgaWYgKGFjdW1lbkNvbmZpZ3VyYXRpb24uYmFja2VuZFNlcnZpY2VVcmwpIHtcbiAgICAgIHRoaXMuc2VydmVyVXJsID0gYWN1bWVuQ29uZmlndXJhdGlvbi5iYWNrZW5kU2VydmljZVVybDtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zZXJ2ZXJVcmwgPSBcImh0dHA6Ly93d3cuaGVhbHRoYWN1bWVuLmNvLnphL2luc2lnaHQvXCI7XG4gICAgfVxuICB9XG5cbiAgQElucHV0KClcbiAgc2V0IGxhdGVzdFN0ZXBDb21wbGV0aW9uRGF0ZShkOiBEYXRlKSB7XG4gICAgLy8gYWxlcnQoXCJEYXRlczogXCIgKyBkICsgXCIgLT4gXCIgKyB0aGlzLl9sYXRlc3RTdGVwQ29tcGxldGlvbkRhdGUpO1xuICAgIGlmIChuZXcgRGF0ZShkKS5nZXRUaW1lKCkgPiBuZXcgRGF0ZSh0aGlzLl9sYXRlc3RTdGVwQ29tcGxldGlvbkRhdGUpLmdldFRpbWUoKSkge1xuICAgICAgLy8gYWxlcnQoXCJyZWZyZXNoXCIpO1xuICAgICAgdGhpcy5yZWZyZXNoSW5UcmF5KCk7XG4gICAgfVxuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgaWYgKHRoaXMudXNlcm5hbWUpIHtcbiAgICAgIHRoaXMuYWN0b3JTZXJ2aWNlLmdldFN5c3RlbVVzZXIodGhpcy51c2VybmFtZSkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICB0aGlzLnN5c3RlbVVzZXIgPSByZXN1bHQ7XG4gICAgICAgIGlmIChyZXN1bHQgJiYgcmVzdWx0LmFjdG9yKSB7XG4gICAgICAgICAgLy8gdGhpcy51c2VySWQgPSByZXN1bHQuaWQ7XG4gICAgICAgICAgdGhpcy51c2VySWQgPSByZXN1bHQuYWN0b3IuaWQ7XG4gICAgICAgICAgdGhpcy5hc3NpZ25lZUlkID0gcmVzdWx0LmFjdG9yLmlkO1xuICAgICAgICAgIHRoaXMucmVmcmVzaEluVHJheSgpO1xuICAgICAgICAgIHRoaXMuc3RhcnRQb2xsaW5nKCk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnJlZnJlc2hJblRyYXkoKTtcbiAgICAgIHRoaXMuc3RhcnRQb2xsaW5nKCk7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIG5nT25EZXN0cm95KCk6IHZvaWQge1xuICAgIGlmICh0aGlzLnN1YnNjcmlwdGlvbikge1xuICAgICAgdGhpcy5zdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIHN0YXJ0UG9sbGluZygpIHtcbiAgICBsZXQgdGltZXIgPSBPYnNlcnZhYmxlLnRpbWVyKDAsIDEwMDAwKTtcbiAgICB0aGlzLnN1YnNjcmlwdGlvbiA9IHRpbWVyLnN1YnNjcmliZSh0ID0+IHtcbiAgICAgIC8vIHRoaXMucHJvamVjdFNlcnZpY2UuZ2V0TGF0ZXN0U3RlcEFzc2lnbm1lbnREYXRlKHRoaXMuYXNzaWduZWVJZCkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgLy8gICBpZiAocmVzdWx0KSB7XG4gICAgICAvLyAgICAgaWYgKHRoaXMuX2xhdGVzdFN0ZXBDb21wbGV0aW9uRGF0ZSkge1xuICAgICAgLy8gICAgICAgbGV0IHQgPSBuZXcgRGF0ZSh0aGlzLl9sYXRlc3RTdGVwQ29tcGxldGlvbkRhdGUpLmdldFRpbWUoKSAtIG5ldyBEYXRlKHJlc3VsdCkuZ2V0VGltZSgpO1xuICAgICAgLy8gICAgICAgaWYgKHQgPCAwKSB7XG4gICAgICAgICAgICAgIHRoaXMucmVmcmVzaEluVHJheSgpO1xuICAgICAgLy8gICAgICAgfVxuICAgICAgLy8gICAgIH1cbiAgICAgIC8vICAgfVxuICAgICAgLy8gfSk7XG4gICAgfSk7XG4gIH1cblxuICByZWZyZXNoSW5UcmF5KCkge1xuICAgIHRoaXMuX2xhdGVzdFN0ZXBDb21wbGV0aW9uRGF0ZSA9IG5ldyBEYXRlKCk7XG4gICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcbiAgICB0aGlzLnNlbGVjdGVkU3RlcEluc3RhbmNlID0gbnVsbDtcbiAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmdldFN0ZXBJbnN0YW5jZXMyKHRoaXMucHJvamVjdEd1aWQsIHRoaXMudXNlcklkLCB0aGlzLmFzc2lnbmVlSWQsIHRoaXMucGFydGljaXBhbnRJZCkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgbGV0IHN0ZXBJbnN0YW5jZXM6IEFycmF5PFN0ZXBJbnN0YW5jZT4gPSBbXTtcbiAgICAgIGZvciAobGV0IHMgb2YgcmVzdWx0KSB7XG4gICAgICAgIGlmIChzLnN0ZXBJbnN0YW5jZVN0YXR1cyA9PT0gJ1dBSVRJTkcnICYmIHMuc3RlcC5zdGVwVHlwZSAhPT0gXCJDQU5DRUxfV0laQVJEXCIpIHtcbiAgICAgICAgICBzdGVwSW5zdGFuY2VzLnB1c2gocyk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgc3RlcEluc3RhbmNlcy5zb3J0KChhLCBiKSA9PiB7XG4gICAgICAgIHJldHVybiBuZXcgRGF0ZShiLmFjdGl2YXRpb25EYXRlKS5nZXRUaW1lKCkgLSBuZXcgRGF0ZShhLmFjdGl2YXRpb25EYXRlKS5nZXRUaW1lKCk7XG4gICAgICB9KTtcblxuICAgICAgdGhpcy5zdGVwSW5zdGFuY2VzID0gc3RlcEluc3RhbmNlcztcbiAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgIH0pO1xuICB9XG5cbiAgc3RlcEluc3RhbmNlU2VsZWN0ZWQoc3RlcEluc3RhbmNlOiBTdGVwSW5zdGFuY2UpIHtcbiAgICB0aGlzLnNlbGVjdGVkU3RlcEluc3RhbmNlID0gc3RlcEluc3RhbmNlO1xuICAgIGlmICh0aGlzLnNlbGVjdGVkU3RlcEluc3RhbmNlKSB7XG4gICAgICB0aGlzLnNlbGVjdGVkU3RlcEluc3RhbmNlLnN5c3RlbVVzZXIgPSB0aGlzLnN5c3RlbVVzZXI7XG4gICAgfVxuXG4gICAgdGhpcy5vblN0ZXBJbnN0YW5jZVNlbGVjdGVkLmVtaXQoc3RlcEluc3RhbmNlKTtcbiAgfVxuXG4gIGJsaW5rUm93KHN0ZXBJbnN0YW5jZTogU3RlcEluc3RhbmNlKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIG5ldyBEYXRlKCkuZ2V0VGltZSgpIC0gbmV3IERhdGUoc3RlcEluc3RhbmNlLmFjdGl2YXRpb25EYXRlKS5nZXRUaW1lKCkgPCAxMDAwICogNjAgKiAzO1xuICB9XG59XG4iXX0=