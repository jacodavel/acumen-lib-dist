/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { ActorService } from '../services/actor.service';
import { ProjectService } from '../services/project.service';
import { AnalysisService } from '../services/analysis.service';
import { Project } from '../domain/project';
var ProcessHistoryComponent = /** @class */ (function () {
    function ProcessHistoryComponent(actorService, projectService, analysisService) {
        this.actorService = actorService;
        this.projectService = projectService;
        this.analysisService = analysisService;
        this.stepInstances = [];
        this.loading = false;
    }
    /**
     * @return {?}
     */
    ProcessHistoryComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        // this.loading = true;
        if (this.externalActorIdNr) {
            this.actorService.getActorFromExternalId(this.externalActorIdNr).then((/**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                _this.actor = result;
                _this.subscription = Observable.timer(1000, 5000).subscribe((/**
                 * @param {?} t
                 * @return {?}
                 */
                function (t) {
                    _this.loadStepInstances();
                }));
            }));
        }
    };
    /**
     * @return {?}
     */
    ProcessHistoryComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    };
    /**
     * @private
     * @return {?}
     */
    ProcessHistoryComponent.prototype.loadStepInstances = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var projectGuid;
        if (this.project) {
            projectGuid = this.project.guid;
        }
        else {
            projectGuid = this.projectGuid;
        }
        // this.loading = true;
        this.projectService.getProcessHistory(projectGuid, this.actor.id).then((/**
         * @param {?} result
         * @return {?}
         */
        function (result) {
            var e_1, _a;
            if (result) {
                /** @type {?} */
                var stepInstances = [];
                try {
                    for (var result_1 = tslib_1.__values(result), result_1_1 = result_1.next(); !result_1_1.done; result_1_1 = result_1.next()) {
                        var stepInstance = result_1_1.value;
                        if (stepInstance.step.stepType === "PROGRESS_NOTES") {
                            stepInstances.push(stepInstance);
                        }
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (result_1_1 && !result_1_1.done && (_a = result_1.return)) _a.call(result_1);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
                _this.stepInstances = stepInstances;
            }
            // this.loading = false;
        }), (/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            // this.loading = false;
        }));
    };
    ProcessHistoryComponent.decorators = [
        { type: Component, args: [{
                    selector: 'acn-process-history',
                    template: "<div class=\"process-history-component\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n\n  <div class=\"step-list\">\n    <div *ngFor=\"let stepInstance of stepInstances; odd as isOdd\" class=\"step-row\" [ngClass]=\"{ 'oddRow': isOdd }\">\n      <div class=\"description\">\n        <!-- {{ stepInstance.step.stepType }} -->\n        {{ stepInstance.step.parameters[\"0\"].parameterValue }}\n      </div>\n      <div class=\"step-date\">\n        Completed: {{ stepInstance.activationDate | date: 'yyyy-MM-dd HH:mm:ss' }}\n      </div>\n    </div>\n  </div>\n</div>\n",
                    styles: [".process-history-component{width:100%;height:100%;position:relative}.process-history-component .step-list{width:100%;height:100%;overflow-y:scroll}.process-history-component .step-list .step-row{display:table;width:100%;padding:5px;font-size:14px}.process-history-component .step-list .step-row .description{display:table-cell;width:70%;padding-left:10px}.process-history-component .step-list .step-row .step-date{display:table-cell;width:30%;padding-left:10px}.process-history-component .step-list .oddRow{background-color:#f2f2f2}.process-history-component .step-list .selectedRow{background-color:#abb9d3}"]
                }] }
    ];
    /** @nocollapse */
    ProcessHistoryComponent.ctorParameters = function () { return [
        { type: ActorService },
        { type: ProjectService },
        { type: AnalysisService }
    ]; };
    ProcessHistoryComponent.propDecorators = {
        project: [{ type: Input }],
        projectGuid: [{ type: Input }],
        externalActorIdNr: [{ type: Input }]
    };
    return ProcessHistoryComponent;
}());
export { ProcessHistoryComponent };
if (false) {
    /** @type {?} */
    ProcessHistoryComponent.prototype.project;
    /** @type {?} */
    ProcessHistoryComponent.prototype.projectGuid;
    /** @type {?} */
    ProcessHistoryComponent.prototype.externalActorIdNr;
    /** @type {?} */
    ProcessHistoryComponent.prototype.actor;
    /** @type {?} */
    ProcessHistoryComponent.prototype.stepInstances;
    /**
     * @type {?}
     * @private
     */
    ProcessHistoryComponent.prototype.subscription;
    /** @type {?} */
    ProcessHistoryComponent.prototype.loading;
    /**
     * @type {?}
     * @private
     */
    ProcessHistoryComponent.prototype.actorService;
    /**
     * @type {?}
     * @private
     */
    ProcessHistoryComponent.prototype.projectService;
    /**
     * @type {?}
     * @private
     */
    ProcessHistoryComponent.prototype.analysisService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvY2Vzcy1oaXN0b3J5LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvcHJvY2Vzcy1oaXN0b3J5L3Byb2Nlc3MtaGlzdG9yeS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFxQixLQUFLLEVBQXdCLE1BQU0sZUFBZSxDQUFDO0FBQzFGLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFHckMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ3pELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFFL0QsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBTTVDO0lBY0UsaUNBQW9CLFlBQTBCLEVBQVUsY0FBOEIsRUFDMUUsZUFBZ0M7UUFEeEIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFBVSxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDMUUsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBTDVDLGtCQUFhLEdBQXdCLEVBQUUsQ0FBQztRQUV4QyxZQUFPLEdBQVksS0FBSyxDQUFDO0lBSXpCLENBQUM7Ozs7SUFFRCwwQ0FBUTs7O0lBQVI7UUFBQSxpQkFVQztRQVRDLHVCQUF1QjtRQUN2QixJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtZQUMxQixJQUFJLENBQUMsWUFBWSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUk7Ozs7WUFBQyxVQUFBLE1BQU07Z0JBQzFFLEtBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDO2dCQUNwQixLQUFJLENBQUMsWUFBWSxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDLFNBQVM7Ozs7Z0JBQUMsVUFBQSxDQUFDO29CQUMxRCxLQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztnQkFDM0IsQ0FBQyxFQUFDLENBQUM7WUFDTCxDQUFDLEVBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7OztJQUVELDZDQUFXOzs7SUFBWDtRQUNFLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNyQixJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ2pDO0lBQ0gsQ0FBQzs7Ozs7SUFFTyxtREFBaUI7Ozs7SUFBekI7UUFBQSxpQkF3QkM7O1lBdkJLLFdBQW1CO1FBQ3ZCLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNoQixXQUFXLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7U0FDakM7YUFBTTtZQUNMLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1NBQ2hDO1FBRUQsdUJBQXVCO1FBQ3ZCLElBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSTs7OztRQUFDLFVBQUEsTUFBTTs7WUFDM0UsSUFBSSxNQUFNLEVBQUU7O29CQUNOLGFBQWEsR0FBd0IsRUFBRTs7b0JBQzNDLEtBQXlCLElBQUEsV0FBQSxpQkFBQSxNQUFNLENBQUEsOEJBQUEsa0RBQUU7d0JBQTVCLElBQUksWUFBWSxtQkFBQTt3QkFDbkIsSUFBSSxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsS0FBSyxnQkFBZ0IsRUFBRTs0QkFDbkQsYUFBYSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQzt5QkFDbEM7cUJBQ0Y7Ozs7Ozs7OztnQkFFRCxLQUFJLENBQUMsYUFBYSxHQUFHLGFBQWEsQ0FBQzthQUNwQztZQUNELHdCQUF3QjtRQUMxQixDQUFDOzs7O1FBQUUsVUFBQSxLQUFLO1lBQ04sd0JBQXdCO1FBQzFCLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Z0JBNURGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUscUJBQXFCO29CQUMvQixzbEJBQStDOztpQkFFaEQ7Ozs7Z0JBZFEsWUFBWTtnQkFDWixjQUFjO2dCQUNkLGVBQWU7OzswQkFjckIsS0FBSzs4QkFDTCxLQUFLO29DQUNMLEtBQUs7O0lBcURSLDhCQUFDO0NBQUEsQUE3REQsSUE2REM7U0F4RFksdUJBQXVCOzs7SUFDbEMsMENBQTBCOztJQUMxQiw4Q0FBNkI7O0lBQzdCLG9EQUFtQzs7SUFDbkMsd0NBQWE7O0lBQ2IsZ0RBQXdDOzs7OztJQUN4QywrQ0FBbUM7O0lBQ25DLDBDQUF5Qjs7Ozs7SUFFYiwrQ0FBa0M7Ozs7O0lBQUUsaURBQXNDOzs7OztJQUNsRixrREFBd0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgT25EZXN0cm95LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzL1J4JztcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMvUngnO1xuXG5pbXBvcnQgeyBBY3RvclNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9hY3Rvci5zZXJ2aWNlJztcbmltcG9ydCB7IFByb2plY3RTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvcHJvamVjdC5zZXJ2aWNlJztcbmltcG9ydCB7IEFuYWx5c2lzU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2FuYWx5c2lzLnNlcnZpY2UnO1xuaW1wb3J0IHsgU3RlcEluc3RhbmNlIH0gZnJvbSAnLi4vZG9tYWluL3N0ZXAtaW5zdGFuY2UnO1xuaW1wb3J0IHsgUHJvamVjdCB9IGZyb20gJy4uL2RvbWFpbi9wcm9qZWN0JztcbmltcG9ydCB7IEFjdG9yIH0gZnJvbSAnLi4vZG9tYWluL2FjdG9yJztcbmltcG9ydCB7IEJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlIH0gZnJvbSAnLi4vZG9tYWluL2J1c2luZXNzLXByb2Nlc3MtaW5zdGFuY2UnO1xuaW1wb3J0IHsgUXVlc3Rpb25MaW5lIH0gZnJvbSAnLi4vZG9tYWluL3F1ZXN0aW9uLWxpbmUnO1xuaW1wb3J0IHsgSW1hZ2VDb250YWluZXIgfSBmcm9tICcuLi9kb21haW4vaW1hZ2UtY29udGFpbmVyJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYWNuLXByb2Nlc3MtaGlzdG9yeScsXG4gIHRlbXBsYXRlVXJsOiAnLi9wcm9jZXNzLWhpc3RvcnkuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9wcm9jZXNzLWhpc3RvcnkuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBQcm9jZXNzSGlzdG9yeUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgQElucHV0KCkgcHJvamVjdDogUHJvamVjdDtcbiAgQElucHV0KCkgcHJvamVjdEd1aWQ6IHN0cmluZztcbiAgQElucHV0KCkgZXh0ZXJuYWxBY3RvcklkTnI6IHN0cmluZztcbiAgYWN0b3I6IEFjdG9yO1xuICBzdGVwSW5zdGFuY2VzOiBBcnJheTxTdGVwSW5zdGFuY2U+ID0gW107XG4gIHByaXZhdGUgc3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XG4gIGxvYWRpbmc6IGJvb2xlYW4gPSBmYWxzZTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGFjdG9yU2VydmljZTogQWN0b3JTZXJ2aWNlLCBwcml2YXRlIHByb2plY3RTZXJ2aWNlOiBQcm9qZWN0U2VydmljZSxcbiAgICAgIHByaXZhdGUgYW5hbHlzaXNTZXJ2aWNlOiBBbmFseXNpc1NlcnZpY2UpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIC8vIHRoaXMubG9hZGluZyA9IHRydWU7XG4gICAgaWYgKHRoaXMuZXh0ZXJuYWxBY3RvcklkTnIpIHtcbiAgICAgIHRoaXMuYWN0b3JTZXJ2aWNlLmdldEFjdG9yRnJvbUV4dGVybmFsSWQodGhpcy5leHRlcm5hbEFjdG9ySWROcikudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICB0aGlzLmFjdG9yID0gcmVzdWx0O1xuICAgICAgICB0aGlzLnN1YnNjcmlwdGlvbiA9IE9ic2VydmFibGUudGltZXIoMTAwMCwgNTAwMCkuc3Vic2NyaWJlKHQgPT4ge1xuICAgICAgICAgIHRoaXMubG9hZFN0ZXBJbnN0YW5jZXMoKTtcbiAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICBuZ09uRGVzdHJveSgpIHtcbiAgICBpZiAodGhpcy5zdWJzY3JpcHRpb24pIHtcbiAgICAgIHRoaXMuc3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBsb2FkU3RlcEluc3RhbmNlcygpIHtcbiAgICBsZXQgcHJvamVjdEd1aWQ6IHN0cmluZztcbiAgICBpZiAodGhpcy5wcm9qZWN0KSB7XG4gICAgICBwcm9qZWN0R3VpZCA9IHRoaXMucHJvamVjdC5ndWlkO1xuICAgIH0gZWxzZSB7XG4gICAgICBwcm9qZWN0R3VpZCA9IHRoaXMucHJvamVjdEd1aWQ7XG4gICAgfVxuXG4gICAgLy8gdGhpcy5sb2FkaW5nID0gdHJ1ZTtcbiAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmdldFByb2Nlc3NIaXN0b3J5KHByb2plY3RHdWlkLCB0aGlzLmFjdG9yLmlkKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICBpZiAocmVzdWx0KSB7XG4gICAgICAgIGxldCBzdGVwSW5zdGFuY2VzOiBBcnJheTxTdGVwSW5zdGFuY2U+ID0gW107XG4gICAgICAgIGZvciAobGV0IHN0ZXBJbnN0YW5jZSBvZiByZXN1bHQpIHtcbiAgICAgICAgICBpZiAoc3RlcEluc3RhbmNlLnN0ZXAuc3RlcFR5cGUgPT09IFwiUFJPR1JFU1NfTk9URVNcIikge1xuICAgICAgICAgICAgc3RlcEluc3RhbmNlcy5wdXNoKHN0ZXBJbnN0YW5jZSk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5zdGVwSW5zdGFuY2VzID0gc3RlcEluc3RhbmNlcztcbiAgICAgIH1cbiAgICAgIC8vIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgIH0sIGVycm9yID0+IHtcbiAgICAgIC8vIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgIH0pO1xuICB9XG59XG4iXX0=