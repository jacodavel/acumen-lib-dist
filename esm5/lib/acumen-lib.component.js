/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var AcumenLibComponent = /** @class */ (function () {
    function AcumenLibComponent() {
    }
    /**
     * @return {?}
     */
    AcumenLibComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    AcumenLibComponent.decorators = [
        { type: Component, args: [{
                    selector: 'acn-acumen-lib',
                    template: "\n    <p>\n      acumen-lib works!\n    </p>\n  "
                }] }
    ];
    /** @nocollapse */
    AcumenLibComponent.ctorParameters = function () { return []; };
    return AcumenLibComponent;
}());
export { AcumenLibComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWN1bWVuLWxpYi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL2FjdW1lbi1saWIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBRWxEO0lBV0U7SUFBZ0IsQ0FBQzs7OztJQUVqQixxQ0FBUTs7O0lBQVI7SUFDQSxDQUFDOztnQkFkRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGdCQUFnQjtvQkFDMUIsUUFBUSxFQUFFLGtEQUlUO2lCQUVGOzs7O0lBUUQseUJBQUM7Q0FBQSxBQWhCRCxJQWdCQztTQVBZLGtCQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2Fjbi1hY3VtZW4tbGliJyxcbiAgdGVtcGxhdGU6IGBcbiAgICA8cD5cbiAgICAgIGFjdW1lbi1saWIgd29ya3MhXG4gICAgPC9wPlxuICBgLFxuICBzdHlsZXM6IFtdXG59KVxuZXhwb3J0IGNsYXNzIEFjdW1lbkxpYkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG59XG4iXX0=