/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { ActorService } from '../../services/actor.service';
import { Project } from '../../domain/project';
var ParticipantSearch = /** @class */ (function () {
    function ParticipantSearch(projectService, actorService) {
        this.projectService = projectService;
        this.actorService = actorService;
        this.onParticipantSelected = new EventEmitter();
        this.participants = [];
    }
    /**
     * @return {?}
     */
    ParticipantSearch.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    ParticipantSearch.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    ParticipantSearch.prototype.search = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.projectService.getProjectRoles(this.project.id).then((/**
         * @param {?} projectRoles
         * @return {?}
         */
        function (projectRoles) {
            var e_1, _a;
            try {
                for (var projectRoles_1 = tslib_1.__values(projectRoles), projectRoles_1_1 = projectRoles_1.next(); !projectRoles_1_1.done; projectRoles_1_1 = projectRoles_1.next()) {
                    var projectRole = projectRoles_1_1.value;
                    if (projectRole.projectFunction === 'PARTICIPANT') {
                        _this.projectService.getProjectActors3(0, 50, projectRole.id, _this.searchField).then((/**
                         * @param {?} result
                         * @return {?}
                         */
                        function (result) {
                            _this.participants = result.list;
                        }));
                        break;
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (projectRoles_1_1 && !projectRoles_1_1.done && (_a = projectRoles_1.return)) _a.call(projectRoles_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }));
    };
    /**
     * @param {?} participant
     * @return {?}
     */
    ParticipantSearch.prototype.participantSelected = /**
     * @param {?} participant
     * @return {?}
     */
    function (participant) {
        var _this = this;
        if (participant) {
            this.actorService.getProjectActor(participant.id, this.project.id).then((/**
             * @param {?} projectActor
             * @return {?}
             */
            function (projectActor) {
                _this.onParticipantSelected.emit(projectActor);
            }));
        }
    };
    ParticipantSearch.decorators = [
        { type: Component, args: [{
                    selector: 'participant-search',
                    template: "<div *ngIf=\"show\" class=\"paticipant-search\">\n  <div class=\"heading\">\n    Find participant\n  </div>\n  <div class=\"search-row\">\n    <div class=\"input-col\">\n      <input [(ngModel)]=\"searchField\" type=\"text\" id=\"searchField\">\n    </div>\n    <div class=\"button-col\" (click)=\"search()\">\n      <img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAY5JREFUSImt1LtrFGEUBfCfURsFHwEr29UNkS3MFklrQK0EIYUk/5IQ0FSmCCKW1mpAommToCKoK+lsLUKeSFbXFLuT3B13Hjt64INvOPeec+fOnUs2mpjHBrbRwQE+YQFTObm5qGMZf0qct7gxjPgM9kqKJ+cAs2XFf4fEX3iOe7iKsxjFHTxFO8R2ikzqqcq/oVFQUANfUm8ynhUce97qVVoGo/gaclcGBTVDQDuvigw09Lfrr+maD+TSkOIJngWNx2lyI5C3KxrcDRof0+R2IC9XNLgSNPbTZDKa7YricFr/v3EqIUZ0xxPO4FxFg0vhnoz7scFmICcqGjTDvRWJEayG57mKBg/C/U2anHDSu5+oDSlex6GTlTE2KOhVMPmACyXFL+qOZZL7Xf/3OMY17KZMrheI13px6e26nmVyX3eDxnYt4lav0qTiaTzp8VkrPNdkNyOpkyM4lEkNL0uK/CjgXw8ySHATD7GGLd0/fgfv8QiTOI93BSb/jCKT/4Isk1ZOTiWTF0H8M8aPANvFyARlADGFAAAAAElFTkSuQmCC\">\n    </div>\n  </div>\n\n  <div class=\"participant-list\">\n    <div *ngFor=\"let participant of participants; odd as isOdd\" class=\"participant-row\" [ngClass]=\"{ 'oddRow': isOdd }\" (click)=\"participantSelected(participant)\">\n      <div *ngIf=\"!participant.actorFieldValues || participant.actorFieldValues.length === 0\" class=\"description\">\n        <span>{{ participant.firstname }}</span>\n        <span *ngIf=\"participant.firstname !== participant.surname\">&nbsp;{{ participant.surname }}</span>\n        <span *ngIf=\"participant.surname !== participant.idNr\">&nbsp;{{ participant.idNr }}</span>\n      </div>\n      <div *ngIf=\"participant.actorFieldValues && participant.actorFieldValues.length > 0\" class=\"description\">\n        <span *ngFor=\"let actorFieldValue of participant.actorFieldValues; let i = index\">\n          <ng-container *ngIf=\"i === 0\">{{ actorFieldValue.fieldValue }}</ng-container><ng-container *ngIf=\"i > 0\">, {{ actorFieldValue.fieldValue }}</ng-container>\n        </span>\n      </div>\n      <div class=\"cursor-image\">\n        <img src=\"data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAUVJREFUeJzt2EtOw0AURNELbCw7o9hZdgaTtBigBLv9+vM+JfUscnyPZw220+OknIDvx9HSN1kw8RufDkH8jU+DIJ7Hh0cQ/8eHRRDH48MhiPPxYRBEf7x7BHE93i2CsIt3hyDs47dBeF/8/58sRvg48Js78AbcBr3D7fH8+6Dnv9wRAAiMcBQAgiKcAYCACGcBIBhCDwAEQugFgCAIVwAgAMJVAHCOYAEAjhGsAMApgiUAOESwBgBnCCMAwBHCKABwgjASABwgjAaAzRFmAMDGCLMAYFOE1XeCqSbG3S5vccP8aqLiK77ijY9mhfRMVHzFV7zx0ayQnomKr/iKNz6aFdIzUfEVX/      HGR7NCeiYqPmb86jvBLzb/+m0i2JfvmUgc3yYSx7eJxPFtInF8m0gc3yYSx7eJxPFtInF8m0gc3yacxf8AAP323bWbSe8AAAAASUVORK5CYII=\">\n      </div>\n    </div>\n  </div>\n\n  <div class=\"button-section\">\n    <div class=\"button button-outline\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                    styles: [".paticipant-search{width:100%;height:100%;position:relative}.paticipant-search .heading{position:absolute;top:0;width:100%;height:32px;font-size:16px;padding:8px}.paticipant-search .top-padding{padding-top:8px}.paticipant-search .search-row{display:table;width:100%;position:absolute;top:32px;height:40px;left:0;padding:8px;box-sizing:border-box;background-color:#f9f9f9;font-size:15px}.paticipant-search .search-row .input-col{display:table-cell;width:90%;font-size:15px;padding-top:7px;vertical-align:top}.paticipant-search .search-row .input-col input{width:100%;font-size:13px;border:1px solid #58666c;background-color:#fffcf6;box-sizing:border-box;padding:3px 5px;border-radius:5px;height:30px}.paticipant-search .search-row .button-col{display:table-cell;width:32px;padding-top:7px;text-align:right;cursor:pointer;box-sizing:border-box}.paticipant-search .search-row .button-col img{border:1px solid #58666c;padding:5px;border-radius:5px;box-sizing:border-box;width:30px;height:30px}.paticipant-search .participant-list{position:absolute;top:90px;bottom:50px;width:100%;overflow-y:scroll}.paticipant-search .participant-list .participant-row{display:table;box-sizing:border-box;width:100%;padding:5px;cursor:pointer}.paticipant-search .participant-list .participant-row .description{display:table-cell;width:calc(100% - 64px);padding-left:2px;padding-top:3px;font-size:14px;vertical-align:middle}.paticipant-search .participant-list .participant-row .cursor-image{display:table-cell;width:32px;text-align:center;vertical-align:middle}.paticipant-search .participant-list .participant-row .cursor-image img{width:16px;height:16px}.paticipant-search .oddRow{background-color:#f2f2f2}.paticipant-search .error-text{font-style:italic;font-size:11px;color:#f04141}.paticipant-search .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.paticipant-search .button-section .button{width:120px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.paticipant-search .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.paticipant-search .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.paticipant-search .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b;cursor:default}.paticipant-search .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b;cursor:default}"]
                }] }
    ];
    /** @nocollapse */
    ParticipantSearch.ctorParameters = function () { return [
        { type: ProjectService },
        { type: ActorService }
    ]; };
    ParticipantSearch.propDecorators = {
        show: [{ type: Input }],
        project: [{ type: Input }],
        onParticipantSelected: [{ type: Output }]
    };
    return ParticipantSearch;
}());
export { ParticipantSearch };
if (false) {
    /** @type {?} */
    ParticipantSearch.prototype.show;
    /** @type {?} */
    ParticipantSearch.prototype.project;
    /** @type {?} */
    ParticipantSearch.prototype.onParticipantSelected;
    /** @type {?} */
    ParticipantSearch.prototype.searchField;
    /** @type {?} */
    ParticipantSearch.prototype.participants;
    /**
     * @type {?}
     * @private
     */
    ParticipantSearch.prototype.projectService;
    /**
     * @type {?}
     * @private
     */
    ParticipantSearch.prototype.actorService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFydGljaXBhbnQtc2VhcmNoLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWN1bWVuLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL3BhcnRpY2lwYW50LXNlYXJjaC9wYXJ0aWNpcGFudC1zZWFyY2gudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBcUIsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUxRixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDaEUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQzVELE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQU0vQztJQVlFLDJCQUFvQixjQUE4QixFQUFVLFlBQTBCO1FBQWxFLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUFVLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBSjVFLDBCQUFxQixHQUFHLElBQUksWUFBWSxFQUFnQixDQUFDO1FBRW5FLGlCQUFZLEdBQTBCLEVBQUUsQ0FBQztJQUd6QyxDQUFDOzs7O0lBRUQsb0NBQVE7OztJQUFSO0lBQ0EsQ0FBQzs7OztJQUVELHVDQUFXOzs7SUFBWDtJQUNBLENBQUM7Ozs7SUFFRCxrQ0FBTTs7O0lBQU47UUFBQSxpQkFZQztRQVhDLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSTs7OztRQUFDLFVBQUMsWUFBMkI7OztnQkFDcEYsS0FBd0IsSUFBQSxpQkFBQSxpQkFBQSxZQUFZLENBQUEsMENBQUEsb0VBQUU7b0JBQWpDLElBQUksV0FBVyx5QkFBQTtvQkFDbEIsSUFBSSxXQUFXLENBQUMsZUFBZSxLQUFLLGFBQWEsRUFBRTt3QkFDakQsS0FBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsRUFBRSxFQUFFLFdBQVcsQ0FBQyxFQUFFLEVBQUUsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUk7Ozs7d0JBQUMsVUFBQyxNQUFxQjs0QkFDeEcsS0FBSSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDO3dCQUNsQyxDQUFDLEVBQUMsQ0FBQzt3QkFFSCxNQUFNO3FCQUNQO2lCQUNGOzs7Ozs7Ozs7UUFDSCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsK0NBQW1COzs7O0lBQW5CLFVBQW9CLFdBQTJCO1FBQS9DLGlCQU1DO1FBTEMsSUFBSSxXQUFXLEVBQUU7WUFDZixJQUFJLENBQUMsWUFBWSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSTs7OztZQUFDLFVBQUMsWUFBMEI7Z0JBQ2pHLEtBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDaEQsQ0FBQyxFQUFDLENBQUM7U0FDSjtJQUNILENBQUM7O2dCQXpDRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtvQkFDOUIsdzFGQUF3Qzs7aUJBRXpDOzs7O2dCQVpRLGNBQWM7Z0JBQ2QsWUFBWTs7O3VCQWFsQixLQUFLOzBCQUNMLEtBQUs7d0NBQ0wsTUFBTTs7SUFrQ1Qsd0JBQUM7Q0FBQSxBQTFDRCxJQTBDQztTQXJDWSxpQkFBaUI7OztJQUM1QixpQ0FBdUI7O0lBQ3ZCLG9DQUEwQjs7SUFDMUIsa0RBQW1FOztJQUNuRSx3Q0FBb0I7O0lBQ3BCLHlDQUF5Qzs7Ozs7SUFFN0IsMkNBQXNDOzs7OztJQUFFLHlDQUFrQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCwgT25EZXN0cm95LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBQcm9qZWN0U2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL3Byb2plY3Quc2VydmljZSc7XG5pbXBvcnQgeyBBY3RvclNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9hY3Rvci5zZXJ2aWNlJztcbmltcG9ydCB7IFByb2plY3QgfSBmcm9tICcuLi8uLi9kb21haW4vcHJvamVjdCc7XG5pbXBvcnQgeyBQcm9qZWN0Um9sZSB9IGZyb20gJy4uLy4uL2RvbWFpbi9wcm9qZWN0LXJvbGUnO1xuaW1wb3J0IHsgUHJvamVjdEFjdG9yIH0gZnJvbSAnLi4vLi4vZG9tYWluL3Byb2plY3QtYWN0b3InO1xuaW1wb3J0IHsgQWN0b3JTZWFyY2hEdG8gfSBmcm9tICcuLi8uLi9kb21haW4vYWN0b3Itc2VhcmNoLWR0byc7XG5pbXBvcnQgeyBUYWJsZURhdGFQYWdlIH0gZnJvbSAnLi4vLi4vZG9tYWluL3RhYmxlLWRhdGEtcGFnZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3BhcnRpY2lwYW50LXNlYXJjaCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9wYXJ0aWNpcGFudC1zZWFyY2guaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL3BhcnRpY2lwYW50LXNlYXJjaC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgUGFydGljaXBhbnRTZWFyY2ggaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIHNob3c6IGJvb2xlYW47XG4gIEBJbnB1dCgpIHByb2plY3Q6IFByb2plY3Q7XG4gIEBPdXRwdXQoKSBvblBhcnRpY2lwYW50U2VsZWN0ZWQgPSBuZXcgRXZlbnRFbWl0dGVyPFByb2plY3RBY3Rvcj4oKTtcbiAgc2VhcmNoRmllbGQ6IHN0cmluZztcbiAgcGFydGljaXBhbnRzOiBBcnJheTxBY3RvclNlYXJjaER0bz4gPSBbXTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHByb2plY3RTZXJ2aWNlOiBQcm9qZWN0U2VydmljZSwgcHJpdmF0ZSBhY3RvclNlcnZpY2U6IEFjdG9yU2VydmljZSkge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpIHtcbiAgfVxuXG4gIHNlYXJjaCgpIHtcbiAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmdldFByb2plY3RSb2xlcyh0aGlzLnByb2plY3QuaWQpLnRoZW4oKHByb2plY3RSb2xlczogUHJvamVjdFJvbGVbXSkgPT4ge1xuICAgICAgZm9yIChsZXQgcHJvamVjdFJvbGUgb2YgcHJvamVjdFJvbGVzKSB7XG4gICAgICAgIGlmIChwcm9qZWN0Um9sZS5wcm9qZWN0RnVuY3Rpb24gPT09ICdQQVJUSUNJUEFOVCcpIHtcbiAgICAgICAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmdldFByb2plY3RBY3RvcnMzKDAsIDUwLCBwcm9qZWN0Um9sZS5pZCwgdGhpcy5zZWFyY2hGaWVsZCkudGhlbigocmVzdWx0OiBUYWJsZURhdGFQYWdlKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnBhcnRpY2lwYW50cyA9IHJlc3VsdC5saXN0O1xuICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHBhcnRpY2lwYW50U2VsZWN0ZWQocGFydGljaXBhbnQ6IEFjdG9yU2VhcmNoRHRvKSB7XG4gICAgaWYgKHBhcnRpY2lwYW50KSB7XG4gICAgICB0aGlzLmFjdG9yU2VydmljZS5nZXRQcm9qZWN0QWN0b3IocGFydGljaXBhbnQuaWQsIHRoaXMucHJvamVjdC5pZCkudGhlbigocHJvamVjdEFjdG9yOiBQcm9qZWN0QWN0b3IpID0+IHtcbiAgICAgICAgdGhpcy5vblBhcnRpY2lwYW50U2VsZWN0ZWQuZW1pdChwcm9qZWN0QWN0b3IpO1xuICAgICAgfSk7XG4gICAgfVxuICB9XG59XG4iXX0=