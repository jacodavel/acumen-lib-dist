/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { DomSanitizer } from '@angular/platform-browser';
import { ActorService } from '../services/actor.service';
import { ProjectService } from '../services/project.service';
import { AnalysisService } from '../services/analysis.service';
var StepInstanceActiveComponent = /** @class */ (function () {
    function StepInstanceActiveComponent(actorService, projectService, analysisService, sanitizer) {
        this.actorService = actorService;
        this.projectService = projectService;
        this.analysisService = analysisService;
        this.sanitizer = sanitizer;
        this.isActive = false;
    }
    /**
     * @return {?}
     */
    StepInstanceActiveComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.subscription = Observable.timer(1000, 10000).subscribe((/**
         * @param {?} t
         * @return {?}
         */
        function (t) {
            _this.loadStepInstances();
        }));
    };
    /**
     * @return {?}
     */
    StepInstanceActiveComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    };
    /**
     * @private
     * @return {?}
     */
    StepInstanceActiveComponent.prototype.loadStepInstances = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        this.projectService.isStepInstanceActive(this.stepId, this.externalId).then((/**
         * @param {?} result
         * @return {?}
         */
        function (result) {
            _this.isActive = "true" === result.response;
        }));
    };
    StepInstanceActiveComponent.decorators = [
        { type: Component, args: [{
                    selector: 'acn-step-instance-active',
                    template: "<div class=\"step-instance-active\">\n  <div *ngIf=\"isActive\">\n    <ng-content>\n    </ng-content>\n  </div>\n</div>\n",
                    styles: [".step-instance-active{width:100%;height:100%;position:relative}"]
                }] }
    ];
    /** @nocollapse */
    StepInstanceActiveComponent.ctorParameters = function () { return [
        { type: ActorService },
        { type: ProjectService },
        { type: AnalysisService },
        { type: DomSanitizer }
    ]; };
    StepInstanceActiveComponent.propDecorators = {
        stepId: [{ type: Input }],
        externalId: [{ type: Input }]
    };
    return StepInstanceActiveComponent;
}());
export { StepInstanceActiveComponent };
if (false) {
    /** @type {?} */
    StepInstanceActiveComponent.prototype.stepId;
    /** @type {?} */
    StepInstanceActiveComponent.prototype.externalId;
    /**
     * @type {?}
     * @private
     */
    StepInstanceActiveComponent.prototype.subscription;
    /** @type {?} */
    StepInstanceActiveComponent.prototype.isActive;
    /**
     * @type {?}
     * @private
     */
    StepInstanceActiveComponent.prototype.actorService;
    /**
     * @type {?}
     * @private
     */
    StepInstanceActiveComponent.prototype.projectService;
    /**
     * @type {?}
     * @private
     */
    StepInstanceActiveComponent.prototype.analysisService;
    /**
     * @type {?}
     * @private
     */
    StepInstanceActiveComponent.prototype.sanitizer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RlcC1pbnN0YW5jZS1hY3RpdmUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWN1bWVuLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9zdGVwLWluc3RhbmNlLWFjdGl2ZS9zdGVwLWluc3RhbmNlLWFjdGl2ZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQXFCLEtBQUssRUFBd0IsTUFBTSxlQUFlLENBQUM7QUFDMUYsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUVyQyxPQUFPLEVBQUUsWUFBWSxFQUFtQixNQUFNLDJCQUEyQixDQUFDO0FBRTFFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUN6RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDN0QsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBUS9EO0lBV0UscUNBQW9CLFlBQTBCLEVBQVUsY0FBOEIsRUFDMUUsZUFBZ0MsRUFBVSxTQUF1QjtRQUR6RCxpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUFVLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUMxRSxvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFBVSxjQUFTLEdBQVQsU0FBUyxDQUFjO1FBSDdFLGFBQVEsR0FBWSxLQUFLLENBQUM7SUFJMUIsQ0FBQzs7OztJQUVELDhDQUFROzs7SUFBUjtRQUFBLGlCQUlDO1FBSEMsSUFBSSxDQUFDLFlBQVksR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQSxDQUFDO1lBQzNELEtBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBQzNCLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVELGlEQUFXOzs7SUFBWDtRQUNFLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNyQixJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ2pDO0lBQ0gsQ0FBQzs7Ozs7SUFFTyx1REFBaUI7Ozs7SUFBekI7UUFBQSxpQkFJQztRQUhDLElBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSTs7OztRQUFDLFVBQUEsTUFBTTtZQUNoRixLQUFJLENBQUMsUUFBUSxHQUFHLE1BQU0sS0FBSyxNQUFNLENBQUMsUUFBUSxDQUFDO1FBQzdDLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Z0JBL0JGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsMEJBQTBCO29CQUNwQyxxSUFBb0Q7O2lCQUVyRDs7OztnQkFkUSxZQUFZO2dCQUNaLGNBQWM7Z0JBQ2QsZUFBZTtnQkFKZixZQUFZOzs7eUJBa0JsQixLQUFLOzZCQUNMLEtBQUs7O0lBeUJSLGtDQUFDO0NBQUEsQUFoQ0QsSUFnQ0M7U0EzQlksMkJBQTJCOzs7SUFDdEMsNkNBQXdCOztJQUN4QixpREFBNEI7Ozs7O0lBQzVCLG1EQUFtQzs7SUFDbkMsK0NBQTBCOzs7OztJQUVkLG1EQUFrQzs7Ozs7SUFBRSxxREFBc0M7Ozs7O0lBQ2xGLHNEQUF3Qzs7Ozs7SUFBRSxnREFBK0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgT25EZXN0cm95LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzL1J4JztcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMvUngnO1xuaW1wb3J0IHsgRG9tU2FuaXRpemVyLCBTYWZlUmVzb3VyY2VVcmwgfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyJztcblxuaW1wb3J0IHsgQWN0b3JTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvYWN0b3Iuc2VydmljZSc7XG5pbXBvcnQgeyBQcm9qZWN0U2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL3Byb2plY3Quc2VydmljZSc7XG5pbXBvcnQgeyBBbmFseXNpc1NlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9hbmFseXNpcy5zZXJ2aWNlJztcbmltcG9ydCB7IFN0ZXBJbnN0YW5jZSB9IGZyb20gJy4uL2RvbWFpbi9zdGVwLWluc3RhbmNlJztcbmltcG9ydCB7IFByb2plY3QgfSBmcm9tICcuLi9kb21haW4vcHJvamVjdCc7XG5pbXBvcnQgeyBBY3RvciB9IGZyb20gJy4uL2RvbWFpbi9hY3Rvcic7XG5pbXBvcnQgeyBCdXNpbmVzc1Byb2Nlc3NJbnN0YW5jZSB9IGZyb20gJy4uL2RvbWFpbi9idXNpbmVzcy1wcm9jZXNzLWluc3RhbmNlJztcbmltcG9ydCB7IFF1ZXN0aW9uTGluZSB9IGZyb20gJy4uL2RvbWFpbi9xdWVzdGlvbi1saW5lJztcbmltcG9ydCB7IEltYWdlQ29udGFpbmVyIH0gZnJvbSAnLi4vZG9tYWluL2ltYWdlLWNvbnRhaW5lcic7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2Fjbi1zdGVwLWluc3RhbmNlLWFjdGl2ZScsXG4gIHRlbXBsYXRlVXJsOiAnLi9zdGVwLWluc3RhbmNlLWFjdGl2ZS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL3N0ZXAtaW5zdGFuY2UtYWN0aXZlLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgU3RlcEluc3RhbmNlQWN0aXZlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuICBASW5wdXQoKSBzdGVwSWQ6IG51bWJlcjtcbiAgQElucHV0KCkgZXh0ZXJuYWxJZDogc3RyaW5nO1xuICBwcml2YXRlIHN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xuICBpc0FjdGl2ZTogYm9vbGVhbiA9IGZhbHNlO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgYWN0b3JTZXJ2aWNlOiBBY3RvclNlcnZpY2UsIHByaXZhdGUgcHJvamVjdFNlcnZpY2U6IFByb2plY3RTZXJ2aWNlLFxuICAgICAgcHJpdmF0ZSBhbmFseXNpc1NlcnZpY2U6IEFuYWx5c2lzU2VydmljZSwgcHJpdmF0ZSBzYW5pdGl6ZXI6IERvbVNhbml0aXplcikge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5zdWJzY3JpcHRpb24gPSBPYnNlcnZhYmxlLnRpbWVyKDEwMDAsIDEwMDAwKS5zdWJzY3JpYmUodCA9PiB7XG4gICAgICB0aGlzLmxvYWRTdGVwSW5zdGFuY2VzKCk7XG4gICAgfSk7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpIHtcbiAgICBpZiAodGhpcy5zdWJzY3JpcHRpb24pIHtcbiAgICAgIHRoaXMuc3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBsb2FkU3RlcEluc3RhbmNlcygpIHtcbiAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmlzU3RlcEluc3RhbmNlQWN0aXZlKHRoaXMuc3RlcElkLCB0aGlzLmV4dGVybmFsSWQpLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgIHRoaXMuaXNBY3RpdmUgPSBcInRydWVcIiA9PT0gcmVzdWx0LnJlc3BvbnNlO1xuICAgIH0pO1xuICB9XG59XG4iXX0=