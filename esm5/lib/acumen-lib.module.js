/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
// import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AcumenLibComponent } from './acumen-lib.component';
import { InTrayComponent } from './in-tray/in-tray.component';
import { InTrayAlertComponent } from './in-tray-alert/in-tray-alert.component';
import { WizardComponent } from './wizard/wizard.component';
import { UserProjectsComponent } from './user-projects/user-projects.component';
import { UserProjects2Component } from './user-projects2/user-projects2.component';
import { StepInstanceComponent } from './step-instance/step-instance.component';
import { StepInstanceListComponent } from './step-instance-list/step-instance-list.component';
import { StepInstanceActiveComponent } from './step-instance-active/step-instance-active.component';
import { ProcessHistoryComponent } from './process-history/process-history.component';
import { LoadingIndicator } from './components/loading-indicator/loading-indicator';
import { ParticipantSearch } from './components/participant-search/participant-search';
import { MessageComponent } from './steps/message/message.component';
import { CreateSystemUserComponent } from './steps/create-system-user/create-system-user.component';
import { IdNrLookupComponent } from './steps/id-nr-lookup/id-nr-lookup.component';
import { RegisterComponent } from './steps/register/register.component';
import { UpdateDetailsComponent } from './steps/update-details/update-details.component';
import { TextQuestionComponent } from './steps/questionnaire/text-question.component';
import { CheckboxComponent } from './steps/questionnaire/checkbox-component';
import { RadioButtonComponent } from './steps/questionnaire/radio-button-component';
import { QuestionLineComponent } from './steps/questionnaire/question-line.component';
import { QuestionnaireComponent } from './steps/questionnaire/questionnaire.component';
import { SelectRole2Component } from './steps/select-role2/select-role2.component';
import { WaitComponent } from './steps/wait/wait.component';
import { FacialAuthenticationRefComponent } from './steps/facial-authentication-ref/facial-authentication-ref.component';
import { FacialAuthenticationComponent } from './steps/facial-authentication/facial-authentication.component';
import { CodeReaderComponent } from './steps/code-reader/code-reader.component';
import { ProjectService } from './services/project.service';
import { MaintenanceService } from './services/maintenance.service';
import { ActorService } from './services/actor.service';
import { AnalysisService } from './services/analysis.service';
import { AcumenConfiguration } from './services/acumen-configuration';
var AcumenLibModule = /** @class */ (function () {
    function AcumenLibModule() {
    }
    /**
     * @param {?} acumenConfiguration
     * @return {?}
     */
    AcumenLibModule.forRoot = /**
     * @param {?} acumenConfiguration
     * @return {?}
     */
    function (acumenConfiguration) {
        return {
            ngModule: AcumenLibModule,
            providers: [
                {
                    provide: AcumenConfiguration,
                    useValue: acumenConfiguration,
                },
            ],
        };
    };
    AcumenLibModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [
                        AcumenLibComponent,
                        InTrayComponent,
                        InTrayAlertComponent,
                        WizardComponent,
                        UserProjectsComponent,
                        UserProjects2Component,
                        StepInstanceComponent,
                        StepInstanceListComponent,
                        StepInstanceActiveComponent,
                        ProcessHistoryComponent,
                        LoadingIndicator,
                        ParticipantSearch,
                        MessageComponent,
                        CreateSystemUserComponent,
                        IdNrLookupComponent,
                        RegisterComponent,
                        UpdateDetailsComponent,
                        TextQuestionComponent,
                        CheckboxComponent,
                        RadioButtonComponent,
                        QuestionLineComponent,
                        QuestionnaireComponent,
                        SelectRole2Component,
                        WaitComponent,
                        FacialAuthenticationRefComponent,
                        FacialAuthenticationComponent,
                        CodeReaderComponent
                    ],
                    imports: [
                        FormsModule,
                        // BrowserModule,
                        CommonModule,
                        HttpClientModule
                    ],
                    exports: [
                        AcumenLibComponent,
                        InTrayComponent,
                        InTrayAlertComponent,
                        WizardComponent,
                        UserProjectsComponent,
                        UserProjects2Component,
                        StepInstanceComponent,
                        StepInstanceListComponent,
                        StepInstanceActiveComponent,
                        ProcessHistoryComponent
                    ],
                    providers: [
                        ProjectService,
                        MaintenanceService,
                        ActorService,
                        AnalysisService
                    ]
                },] }
    ];
    return AcumenLibModule;
}());
export { AcumenLibModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWN1bWVuLWxpYi5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL2FjdW1lbi1saWIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUF1QixNQUFNLGVBQWUsQ0FBQztBQUM5RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7O0FBRS9DLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUU3QyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDOUQsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDL0UsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQzVELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBQ25GLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLG1EQUFtRCxDQUFDO0FBQzlGLE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLHVEQUF1RCxDQUFDO0FBQ3BHLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBRXRGLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGtEQUFrRCxDQUFDO0FBQ3BGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLG9EQUFvRCxDQUFDO0FBRXZGLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLHlEQUF5RCxDQUFDO0FBQ3BHLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQ2xGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLGlEQUFpRCxDQUFDO0FBQ3pGLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQ3RGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQ3BGLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQ3RGLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQ3ZGLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQ25GLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUM1RCxPQUFPLEVBQUUsZ0NBQWdDLEVBQUUsTUFBTSx1RUFBdUUsQ0FBQztBQUN6SCxPQUFPLEVBQUUsNkJBQTZCLEVBQUUsTUFBTSwrREFBK0QsQ0FBQztBQUM5RyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUVoRixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDNUQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDcEUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUV0RTtJQUFBO0lBbUVBLENBQUM7Ozs7O0lBWFEsdUJBQU87Ozs7SUFBZCxVQUFlLG1CQUF3QztRQUNyRCxPQUFPO1lBQ0wsUUFBUSxFQUFFLGVBQWU7WUFDekIsU0FBUyxFQUFFO2dCQUNUO29CQUNFLE9BQU8sRUFBRSxtQkFBbUI7b0JBQzVCLFFBQVEsRUFBRSxtQkFBbUI7aUJBQzlCO2FBQ0Y7U0FDRixDQUFDO0lBQ0osQ0FBQzs7Z0JBbEVGLFFBQVEsU0FBQztvQkFDUixZQUFZLEVBQUU7d0JBQ1osa0JBQWtCO3dCQUNsQixlQUFlO3dCQUNmLG9CQUFvQjt3QkFDcEIsZUFBZTt3QkFDZixxQkFBcUI7d0JBQ3JCLHNCQUFzQjt3QkFDdEIscUJBQXFCO3dCQUNyQix5QkFBeUI7d0JBQ3pCLDJCQUEyQjt3QkFDM0IsdUJBQXVCO3dCQUN2QixnQkFBZ0I7d0JBQ2hCLGlCQUFpQjt3QkFDakIsZ0JBQWdCO3dCQUNoQix5QkFBeUI7d0JBQ3pCLG1CQUFtQjt3QkFDbkIsaUJBQWlCO3dCQUNqQixzQkFBc0I7d0JBQ3RCLHFCQUFxQjt3QkFDckIsaUJBQWlCO3dCQUNqQixvQkFBb0I7d0JBQ3BCLHFCQUFxQjt3QkFDckIsc0JBQXNCO3dCQUN0QixvQkFBb0I7d0JBQ3BCLGFBQWE7d0JBQ2IsZ0NBQWdDO3dCQUNoQyw2QkFBNkI7d0JBQzdCLG1CQUFtQjtxQkFDcEI7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLFdBQVc7d0JBQ1gsaUJBQWlCO3dCQUNqQixZQUFZO3dCQUNaLGdCQUFnQjtxQkFDakI7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLGtCQUFrQjt3QkFDbEIsZUFBZTt3QkFDZixvQkFBb0I7d0JBQ3BCLGVBQWU7d0JBQ2YscUJBQXFCO3dCQUNyQixzQkFBc0I7d0JBQ3RCLHFCQUFxQjt3QkFDckIseUJBQXlCO3dCQUN6QiwyQkFBMkI7d0JBQzNCLHVCQUF1QjtxQkFDeEI7b0JBQ0QsU0FBUyxFQUFFO3dCQUNULGNBQWM7d0JBQ2Qsa0JBQWtCO3dCQUNsQixZQUFZO3dCQUNaLGVBQWU7cUJBQ2hCO2lCQUNGOztJQWFELHNCQUFDO0NBQUEsQUFuRUQsSUFtRUM7U0FaWSxlQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUsIE1vZHVsZVdpdGhQcm92aWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBDbGllbnRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuLy8gaW1wb3J0IHsgQnJvd3Nlck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXInO1xuaW1wb3J0IHsgRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbmltcG9ydCB7IEFjdW1lbkxpYkNvbXBvbmVudCB9IGZyb20gJy4vYWN1bWVuLWxpYi5jb21wb25lbnQnO1xuaW1wb3J0IHsgSW5UcmF5Q29tcG9uZW50IH0gZnJvbSAnLi9pbi10cmF5L2luLXRyYXkuY29tcG9uZW50JztcbmltcG9ydCB7IEluVHJheUFsZXJ0Q29tcG9uZW50IH0gZnJvbSAnLi9pbi10cmF5LWFsZXJ0L2luLXRyYXktYWxlcnQuY29tcG9uZW50JztcbmltcG9ydCB7IFdpemFyZENvbXBvbmVudCB9IGZyb20gJy4vd2l6YXJkL3dpemFyZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgVXNlclByb2plY3RzQ29tcG9uZW50IH0gZnJvbSAnLi91c2VyLXByb2plY3RzL3VzZXItcHJvamVjdHMuY29tcG9uZW50JztcbmltcG9ydCB7IFVzZXJQcm9qZWN0czJDb21wb25lbnQgfSBmcm9tICcuL3VzZXItcHJvamVjdHMyL3VzZXItcHJvamVjdHMyLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBTdGVwSW5zdGFuY2VDb21wb25lbnQgfSBmcm9tICcuL3N0ZXAtaW5zdGFuY2Uvc3RlcC1pbnN0YW5jZS5jb21wb25lbnQnO1xuaW1wb3J0IHsgU3RlcEluc3RhbmNlTGlzdENvbXBvbmVudCB9IGZyb20gJy4vc3RlcC1pbnN0YW5jZS1saXN0L3N0ZXAtaW5zdGFuY2UtbGlzdC5jb21wb25lbnQnO1xuaW1wb3J0IHsgU3RlcEluc3RhbmNlQWN0aXZlQ29tcG9uZW50IH0gZnJvbSAnLi9zdGVwLWluc3RhbmNlLWFjdGl2ZS9zdGVwLWluc3RhbmNlLWFjdGl2ZS5jb21wb25lbnQnO1xuaW1wb3J0IHsgUHJvY2Vzc0hpc3RvcnlDb21wb25lbnQgfSBmcm9tICcuL3Byb2Nlc3MtaGlzdG9yeS9wcm9jZXNzLWhpc3RvcnkuY29tcG9uZW50JztcblxuaW1wb3J0IHsgTG9hZGluZ0luZGljYXRvciB9IGZyb20gJy4vY29tcG9uZW50cy9sb2FkaW5nLWluZGljYXRvci9sb2FkaW5nLWluZGljYXRvcic7XG5pbXBvcnQgeyBQYXJ0aWNpcGFudFNlYXJjaCB9IGZyb20gJy4vY29tcG9uZW50cy9wYXJ0aWNpcGFudC1zZWFyY2gvcGFydGljaXBhbnQtc2VhcmNoJztcblxuaW1wb3J0IHsgTWVzc2FnZUNvbXBvbmVudCB9IGZyb20gJy4vc3RlcHMvbWVzc2FnZS9tZXNzYWdlLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDcmVhdGVTeXN0ZW1Vc2VyQ29tcG9uZW50IH0gZnJvbSAnLi9zdGVwcy9jcmVhdGUtc3lzdGVtLXVzZXIvY3JlYXRlLXN5c3RlbS11c2VyLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBJZE5yTG9va3VwQ29tcG9uZW50IH0gZnJvbSAnLi9zdGVwcy9pZC1uci1sb29rdXAvaWQtbnItbG9va3VwLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBSZWdpc3RlckNvbXBvbmVudCB9IGZyb20gJy4vc3RlcHMvcmVnaXN0ZXIvcmVnaXN0ZXIuY29tcG9uZW50JztcbmltcG9ydCB7IFVwZGF0ZURldGFpbHNDb21wb25lbnQgfSBmcm9tICcuL3N0ZXBzL3VwZGF0ZS1kZXRhaWxzL3VwZGF0ZS1kZXRhaWxzLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBUZXh0UXVlc3Rpb25Db21wb25lbnQgfSBmcm9tICcuL3N0ZXBzL3F1ZXN0aW9ubmFpcmUvdGV4dC1xdWVzdGlvbi5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ2hlY2tib3hDb21wb25lbnQgfSBmcm9tICcuL3N0ZXBzL3F1ZXN0aW9ubmFpcmUvY2hlY2tib3gtY29tcG9uZW50JztcbmltcG9ydCB7IFJhZGlvQnV0dG9uQ29tcG9uZW50IH0gZnJvbSAnLi9zdGVwcy9xdWVzdGlvbm5haXJlL3JhZGlvLWJ1dHRvbi1jb21wb25lbnQnO1xuaW1wb3J0IHsgUXVlc3Rpb25MaW5lQ29tcG9uZW50IH0gZnJvbSAnLi9zdGVwcy9xdWVzdGlvbm5haXJlL3F1ZXN0aW9uLWxpbmUuY29tcG9uZW50JztcbmltcG9ydCB7IFF1ZXN0aW9ubmFpcmVDb21wb25lbnQgfSBmcm9tICcuL3N0ZXBzL3F1ZXN0aW9ubmFpcmUvcXVlc3Rpb25uYWlyZS5jb21wb25lbnQnO1xuaW1wb3J0IHsgU2VsZWN0Um9sZTJDb21wb25lbnQgfSBmcm9tICcuL3N0ZXBzL3NlbGVjdC1yb2xlMi9zZWxlY3Qtcm9sZTIuY29tcG9uZW50JztcbmltcG9ydCB7IFdhaXRDb21wb25lbnQgfSBmcm9tICcuL3N0ZXBzL3dhaXQvd2FpdC5jb21wb25lbnQnO1xuaW1wb3J0IHsgRmFjaWFsQXV0aGVudGljYXRpb25SZWZDb21wb25lbnQgfSBmcm9tICcuL3N0ZXBzL2ZhY2lhbC1hdXRoZW50aWNhdGlvbi1yZWYvZmFjaWFsLWF1dGhlbnRpY2F0aW9uLXJlZi5jb21wb25lbnQnO1xuaW1wb3J0IHsgRmFjaWFsQXV0aGVudGljYXRpb25Db21wb25lbnQgfSBmcm9tICcuL3N0ZXBzL2ZhY2lhbC1hdXRoZW50aWNhdGlvbi9mYWNpYWwtYXV0aGVudGljYXRpb24uY29tcG9uZW50JztcbmltcG9ydCB7IENvZGVSZWFkZXJDb21wb25lbnQgfSBmcm9tICcuL3N0ZXBzL2NvZGUtcmVhZGVyL2NvZGUtcmVhZGVyLmNvbXBvbmVudCc7XG5cbmltcG9ydCB7IFByb2plY3RTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9wcm9qZWN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgTWFpbnRlbmFuY2VTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9tYWludGVuYW5jZS5zZXJ2aWNlJztcbmltcG9ydCB7IEFjdG9yU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvYWN0b3Iuc2VydmljZSc7XG5pbXBvcnQgeyBBbmFseXNpc1NlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL2FuYWx5c2lzLnNlcnZpY2UnO1xuaW1wb3J0IHsgQWN1bWVuQ29uZmlndXJhdGlvbiB9IGZyb20gJy4vc2VydmljZXMvYWN1bWVuLWNvbmZpZ3VyYXRpb24nO1xuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBBY3VtZW5MaWJDb21wb25lbnQsXG4gICAgSW5UcmF5Q29tcG9uZW50LFxuICAgIEluVHJheUFsZXJ0Q29tcG9uZW50LFxuICAgIFdpemFyZENvbXBvbmVudCxcbiAgICBVc2VyUHJvamVjdHNDb21wb25lbnQsXG4gICAgVXNlclByb2plY3RzMkNvbXBvbmVudCxcbiAgICBTdGVwSW5zdGFuY2VDb21wb25lbnQsXG4gICAgU3RlcEluc3RhbmNlTGlzdENvbXBvbmVudCxcbiAgICBTdGVwSW5zdGFuY2VBY3RpdmVDb21wb25lbnQsXG4gICAgUHJvY2Vzc0hpc3RvcnlDb21wb25lbnQsXG4gICAgTG9hZGluZ0luZGljYXRvcixcbiAgICBQYXJ0aWNpcGFudFNlYXJjaCxcbiAgICBNZXNzYWdlQ29tcG9uZW50LFxuICAgIENyZWF0ZVN5c3RlbVVzZXJDb21wb25lbnQsXG4gICAgSWROckxvb2t1cENvbXBvbmVudCxcbiAgICBSZWdpc3RlckNvbXBvbmVudCxcbiAgICBVcGRhdGVEZXRhaWxzQ29tcG9uZW50LFxuICAgIFRleHRRdWVzdGlvbkNvbXBvbmVudCxcbiAgICBDaGVja2JveENvbXBvbmVudCxcbiAgICBSYWRpb0J1dHRvbkNvbXBvbmVudCxcbiAgICBRdWVzdGlvbkxpbmVDb21wb25lbnQsXG4gICAgUXVlc3Rpb25uYWlyZUNvbXBvbmVudCxcbiAgICBTZWxlY3RSb2xlMkNvbXBvbmVudCxcbiAgICBXYWl0Q29tcG9uZW50LFxuICAgIEZhY2lhbEF1dGhlbnRpY2F0aW9uUmVmQ29tcG9uZW50LFxuICAgIEZhY2lhbEF1dGhlbnRpY2F0aW9uQ29tcG9uZW50LFxuICAgIENvZGVSZWFkZXJDb21wb25lbnRcbiAgXSxcbiAgaW1wb3J0czogW1xuICAgIEZvcm1zTW9kdWxlLFxuICAgIC8vIEJyb3dzZXJNb2R1bGUsXG4gICAgQ29tbW9uTW9kdWxlLFxuICAgIEh0dHBDbGllbnRNb2R1bGVcbiAgXSxcbiAgZXhwb3J0czogW1xuICAgIEFjdW1lbkxpYkNvbXBvbmVudCxcbiAgICBJblRyYXlDb21wb25lbnQsXG4gICAgSW5UcmF5QWxlcnRDb21wb25lbnQsXG4gICAgV2l6YXJkQ29tcG9uZW50LFxuICAgIFVzZXJQcm9qZWN0c0NvbXBvbmVudCxcbiAgICBVc2VyUHJvamVjdHMyQ29tcG9uZW50LFxuICAgIFN0ZXBJbnN0YW5jZUNvbXBvbmVudCxcbiAgICBTdGVwSW5zdGFuY2VMaXN0Q29tcG9uZW50LFxuICAgIFN0ZXBJbnN0YW5jZUFjdGl2ZUNvbXBvbmVudCxcbiAgICBQcm9jZXNzSGlzdG9yeUNvbXBvbmVudFxuICBdLFxuICBwcm92aWRlcnM6IFtcbiAgICBQcm9qZWN0U2VydmljZSxcbiAgICBNYWludGVuYW5jZVNlcnZpY2UsXG4gICAgQWN0b3JTZXJ2aWNlLFxuICAgIEFuYWx5c2lzU2VydmljZVxuICBdXG59KVxuZXhwb3J0IGNsYXNzIEFjdW1lbkxpYk1vZHVsZSB7XG4gIHN0YXRpYyBmb3JSb290KGFjdW1lbkNvbmZpZ3VyYXRpb246IEFjdW1lbkNvbmZpZ3VyYXRpb24pOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcbiAgICByZXR1cm4ge1xuICAgICAgbmdNb2R1bGU6IEFjdW1lbkxpYk1vZHVsZSxcbiAgICAgIHByb3ZpZGVyczogW1xuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQWN1bWVuQ29uZmlndXJhdGlvbixcbiAgICAgICAgICB1c2VWYWx1ZTogYWN1bWVuQ29uZmlndXJhdGlvbixcbiAgICAgICAgfSxcbiAgICAgIF0sXG4gICAgfTtcbiAgfVxufVxuIl19