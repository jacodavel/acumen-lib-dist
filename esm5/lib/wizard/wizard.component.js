/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { ActorService } from '../services/actor.service';
import { ProjectService } from '../services/project.service';
import { AnalysisService } from '../services/analysis.service';
import { Project } from '../domain/project';
var WizardComponent = /** @class */ (function () {
    function WizardComponent(actorService, projectService, analysisService) {
        this.actorService = actorService;
        this.projectService = projectService;
        this.analysisService = analysisService;
        this.onWizardCancelled = new EventEmitter();
        this.onWizardCompleted = new EventEmitter();
        this.onOpenCameraListener = new EventEmitter();
        this.onQuestionAnswered = new EventEmitter();
        this.onReadCode = new EventEmitter();
        this.processError = false;
        this.processExecutionError = false;
        this.cancelled = false;
        this.showCompletedMessage = false;
        this.showParticipantSearch = false;
        this.loading = false;
    }
    /**
     * @return {?}
     */
    WizardComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.loading = true;
        this.showWaitIndicator = true;
        this.processError = false;
        this.processExecutionError = false;
        this.actor = null;
        this.businessProcessInstance = null;
        this.stepInstance = null;
        this.showParticipantSearch = false;
        if (this.project) {
            this.initialise();
        }
        else if (this.projectGuid) {
            this.projectService.getProject(this.projectGuid).then((/**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                _this.project = result;
                _this.initialise();
            }));
        }
    };
    /**
     * @return {?}
     */
    WizardComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @private
     * @return {?}
     */
    WizardComponent.prototype.initialise = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        this.actorService.getSystemUser(this.username).then((/**
         * @param {?} result
         * @return {?}
         */
        function (result) {
            if (result) {
                _this.userId = result.id;
                if (_this.externalActorIdNr) {
                    _this.actorService.getProjectActorFromExternalId(_this.externalActorIdNr, _this.project.id).then((/**
                     * @param {?} result2
                     * @return {?}
                     */
                    function (result2) {
                        _this.actor = result2;
                        if (_this.project.wizardProject) {
                            _this.startWizard(_this.project.guid, null);
                        }
                        else {
                            // if (this.project.actorProjectFunction === "PARTICIPANT") {
                            if (_this.actor) {
                                _this.startWizard(_this.project.guid, _this.actor.id);
                            }
                            else {
                                _this.showParticipantSearch = true;
                                _this.loading = false;
                            }
                        }
                    }));
                }
                else {
                    if (_this.project.wizardProject) {
                        _this.startWizard(_this.project.guid, null);
                    }
                    else {
                        if (_this.project.actorProjectFunction === "PARTICIPANT" && result.actor) {
                            _this.actor = result.actor;
                            _this.startWizard(_this.project.guid, _this.actor.id);
                        }
                        else {
                            _this.showParticipantSearch = true;
                            _this.loading = false;
                        }
                    }
                }
            }
            else {
                // Ongeldige user, wys boodskap
            }
        }));
    };
    /**
     * @param {?} participant
     * @return {?}
     */
    WizardComponent.prototype.onParticipantSelected = /**
     * @param {?} participant
     * @return {?}
     */
    function (participant) {
        this.actor = participant.actor;
        this.showParticipantSearch = false;
        this.loading = true;
        this.startWizard(this.project.guid, this.actor.id);
    };
    /**
     * @private
     * @param {?} projectGuid
     * @param {?} actorId
     * @return {?}
     */
    WizardComponent.prototype.startWizard = /**
     * @private
     * @param {?} projectGuid
     * @param {?} actorId
     * @return {?}
     */
    function (projectGuid, actorId) {
        var _this = this;
        this.processExecutionError = false;
        this.projectService.startWizardProject(projectGuid, actorId).then((/**
         * @param {?} businessProcessInstance
         * @return {?}
         */
        function (businessProcessInstance) {
            _this.businessProcessInstance = businessProcessInstance;
            _this.projectName = businessProcessInstance.businessProcess.project.projectName;
            _this.getNextStepDelayed();
            _this.loading = false;
        }), (/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            _this.processError = true;
            _this.loading = false;
        }));
    };
    /**
     * @param {?} event
     * @return {?}
     */
    WizardComponent.prototype.stepCompletedListener = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (this.stepInstance && this.latitude && this.longitude) {
            this.projectService.updateStepLocation(this.stepInstance.id, this.latitude, this.longitude).then((/**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                // hoef nie iets te doen nie
            }));
        }
        this.getNextStep();
    };
    /**
     * @private
     * @return {?}
     */
    WizardComponent.prototype.getNextStepDelayed = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        if (!this.cancelled) {
            /** @type {?} */
            var timer = Observable.timer(1000);
            timer.subscribe((/**
             * @param {?} t
             * @return {?}
             */
            function (t) {
                _this.getNextStep();
            }));
        }
    };
    /**
     * @private
     * @return {?}
     */
    WizardComponent.prototype.getNextStep = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        console.log("------------------------------------------------");
        this.projectService.getBusinessProcessState(this.businessProcessInstance.id).then((/**
         * @param {?} result
         * @return {?}
         */
        function (result) {
            _this.businessProcessInstance = result.businessProcessInstance;
            if (result.stepInstance == null) {
                console.log("stepInstance == null ");
                _this.stepInstance = result.stepInstance;
                if (result.businessProcessInstance.processStatus === 'COMPLETED') {
                    console.log("COMPLETED");
                    _this.processCompleted();
                    // this.analysisService.getProjectCompletionCount(this.project.guid).then(count => {
                    //     this.completionCount = count;
                    // });
                }
                else if (result.businessProcessInstance.processStatus === 'FAILED') {
                    _this.processExecutionError = true;
                }
                else {
                    console.log("getNextStepDelayed 1");
                    _this.getNextStepDelayed();
                }
            }
            else if (_this.stepInstance && _this.stepInstance.id === result.stepInstance.id) {
                _this.stepInstance = result.stepInstance;
                if (_this.stepInstance.step.stepType === 'CANCEL_WIZARD' ||
                    (_this.stepInstance.step.stepType === 'NONE_END_EVENT' && !_this.stepInstance.step.parentStep)) {
                    console.log("this.stepInstance.id === result.stepInstance.id 1");
                    _this.processCompleted();
                }
                else {
                    console.log("this.stepInstance.id === result.stepInstance.id 2");
                    // this.getNextStepDelayed();
                    _this.showStepPanel(result.stepInstance);
                }
            }
            else {
                console.log("this.stepInstance.id === result.stepInstance.id 3");
                _this.stepInstance = result.stepInstance;
                if (_this.stepInstance.step.stepType === 'CANCEL_WIZARD' ||
                    (_this.stepInstance.step.stepType === 'NONE_END_EVENT' && !_this.stepInstance.step.parentStep)) {
                    _this.processCompleted();
                }
                else {
                    if (_this.stepInstance.actor) {
                        console.log("this.stepInstance.actor");
                        if (_this.stepInstance.actor.id === _this.businessProcessInstance.actor.id) { // || stepInstance.getActor().getId().equals(evaluatorId) {
                            console.log("showStepPanel: " + _this.stepInstance.step.stepType);
                            _this.showStepPanel(result.stepInstance);
                        }
                        else {
                            console.log("showInProgressPanel 1");
                            _this.showInProgressPanel();
                        }
                    }
                    else {
                        console.log("showInProgressPanel 2");
                        _this.showInProgressPanel();
                    }
                }
            }
        }));
    };
    /**
     * @private
     * @param {?} stepInstance
     * @return {?}
     */
    WizardComponent.prototype.showStepPanel = /**
     * @private
     * @param {?} stepInstance
     * @return {?}
     */
    function (stepInstance) {
        if (stepInstance.step.stepType === 'COMPLETE_QUESTIONNAIRE' || stepInstance.step.stepType === 'MESSAGE' ||
            stepInstance.step.stepType === 'ID_NR_LOOKUP' || stepInstance.step.stepType === 'UPDATE_PERSONAL_DETAILS' ||
            stepInstance.step.stepType === 'REGISTER' || stepInstance.step.stepType === 'CREATE_SYSTEM_USER' ||
            stepInstance.step.stepType === 'SELECT_PROVIDER' || stepInstance.step.stepType === 'SELECT_PROVIDER_2' ||
            stepInstance.step.stepType === 'FACIAL_AUTHENTICATION_REF' || stepInstance.step.stepType === 'FACIAL_AUTHENTICATION' ||
            stepInstance.step.stepType === 'WAIT' || stepInstance.step.stepType === 'REGISTER' ||
            stepInstance.step.stepType === 'CREATE_SYSTEM_USER') {
            this.showWaitIndicator = false;
        }
        else {
            this.getNextStepDelayed();
        }
    };
    /**
     * @private
     * @return {?}
     */
    WizardComponent.prototype.processCompleted = /**
     * @private
     * @return {?}
     */
    function () {
        this.showCompletedMessage = true;
        this.stepInstance = null;
        this.onWizardCompleted.emit();
    };
    /**
     * @private
     * @return {?}
     */
    WizardComponent.prototype.showInProgressPanel = /**
     * @private
     * @return {?}
     */
    function () {
        this.getNextStepDelayed();
    };
    /**
     * @param {?} event
     * @return {?}
     */
    WizardComponent.prototype.cancelWizard = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.stepInstance = null;
        this.cancelled = true;
        this.onWizardCancelled.emit();
    };
    /**
     * @param {?} imageContainer
     * @return {?}
     */
    WizardComponent.prototype.openCamera = /**
     * @param {?} imageContainer
     * @return {?}
     */
    function (imageContainer) {
        this.onOpenCameraListener.emit(imageContainer);
    };
    /**
     * @param {?} questionLine
     * @return {?}
     */
    WizardComponent.prototype.questionAnswered = /**
     * @param {?} questionLine
     * @return {?}
     */
    function (questionLine) {
        this.onQuestionAnswered.emit(questionLine);
    };
    /**
     * @param {?} codeReadListener
     * @return {?}
     */
    WizardComponent.prototype.readCode = /**
     * @param {?} codeReadListener
     * @return {?}
     */
    function (codeReadListener) {
        this.onReadCode.emit(codeReadListener);
    };
    WizardComponent.decorators = [
        { type: Component, args: [{
                    selector: 'acn-wizard',
                    template: "<div class=\"wizard-component\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <participant-search [show]=\"showParticipantSearch\" [project]=\"project\" (onParticipantSelected)=\"onParticipantSelected($event)\"></participant-search>\n\n  <!-- <div class=\"step-content\">\n    username: {{ username }}, externalActorIdNr: {{ externalActorIdNr }}\n  </div> -->\n\n  <div *ngIf=\"stepInstance && !showCompletedMessage && stepInstance && stepInstance.step.stepType === 'MESSAGE'\" class=\"step-content\">\n    <message [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelWizard($event)\"></message>\n  </div>\n\n  <div *ngIf=\"stepInstance && !showCompletedMessage && stepInstance && stepInstance.step.stepType === 'COMPLETE_QUESTIONNAIRE'\" class=\"step-content\">\n    <questionnaire [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelWizard($event)\" (onTakePicture)=\"openCamera($event)\" (onQuestionAnswered)=\"questionAnswered($event)\"></questionnaire>\n  </div>\n\n  <div *ngIf=\"stepInstance && !showCompletedMessage && stepInstance.step.stepType === 'WAIT'\" class=\"step-content\">\n    <wait [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelWizard($event)\"></wait>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType === 'FACIAL_AUTHENTICATION_REF'\" class=\"step-content\">\n    <facial-authentication-ref [stepInstance]=\"stepInstance\" [userId]=\"userId\" (onOpenCameraListener)=\"openCamera($event)\"\n        (stepCompletedListener)=\"stepCompletedListener($event)\" (cancelListener)=\"cancelWizard($event)\"></facial-authentication-ref>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType === 'FACIAL_AUTHENTICATION'\" class=\"step-content\">\n    <facial-authentication [stepInstance]=\"stepInstance\" [userId]=\"userId\" (onOpenCameraListener)=\"openCamera($event)\"\n        (stepCompletedListener)=\"stepCompletedListener($event)\" (cancelListener)=\"cancelWizard($event)\"></facial-authentication>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType === 'CODE_READER'\" class=\"step-content\">\n    <code-reader [stepInstance]=\"stepInstance\" [userId]=\"userId\" (onReadCode)=\"readCode($event)\"\n        (stepCompletedListener)=\"stepCompletedListener($event)\" (cancelListener)=\"cancelWizard($event)\"></code-reader>\n  </div>\n\n  <div *ngIf=\"stepInstance && !showCompletedMessage && stepInstance.step.stepType === 'ID_NR_LOOKUP'\" class=\"step-content\">\n      <id-nr-lookup [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n          (cancelListener)=\"cancelWizard($event)\"></id-nr-lookup>\n  </div>\n\n  <div *ngIf=\"stepInstance && !showCompletedMessage && stepInstance && stepInstance.step.stepType === 'UPDATE_PERSONAL_DETAILS'\" class=\"step-content\">\n    <update-details [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelWizard($event)\"></update-details>\n  </div>\n\n  <div *ngIf=\"stepInstance && !showCompletedMessage && stepInstance && stepInstance.step.stepType === 'REGISTER'\" class=\"step-content\">\n    <register [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelWizard($event)\"></register>\n  </div>\n\n  <div *ngIf=\"stepInstance && !showCompletedMessage && stepInstance && stepInstance.step.stepType === 'CREATE_SYSTEM_USER'\" class=\"step-content\">\n    <create-system-user [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelWizard($event)\"></create-system-user>\n  </div>\n\n  <div *ngIf=\"stepInstance && !showCompletedMessage && stepInstance && stepInstance.step.stepType === 'SELECT_PROVIDER_2'\" class=\"step-content\">\n    <select-role2 [makeBooking]=\"true\" [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelWizard($event)\"></select-role2>\n  </div>\n\n  <div *ngIf=\"processError\" class=\"step-info\">\n    <div class=\"info-message\">\n        Error starting process...\n    </div>\n    <div class=\"button-section\">\n      <div class=\"button\" (click)=\"cancelWizard(null)\">\n        <div class=\"button-text\">\n          Cancel\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!processError && !processExecutionError && !showParticipantSearch && !showCompletedMessage && !cancelled && (!stepInstance ||\n          (stepInstance.step.stepType !== 'COMPLETE_QUESTIONNAIRE' && stepInstance.step.stepType !== 'MESSAGE' &&\n           stepInstance.step.stepType !== 'ID_NR_LOOKUP' && stepInstance.step.stepType !== 'UPDATE_PERSONAL_DETAILS' &&\n           stepInstance.step.stepType !== 'SELECT_PROVIDER_2' && stepInstance.step.stepType !== 'WAIT' &&\n           stepInstance.step.stepType !== 'FACIAL_AUTHENTICATION_REF' && stepInstance.step.stepType !== 'FACIAL_AUTHENTICATION' &&\n           stepInstance.step.stepType !== 'CODE_READER' &&\n           stepInstance.step.stepType !== 'REGISTER' && stepInstance.step.stepType !== 'CREATE_SYSTEM_USER'))\" class=\"step-info\">\n    <div class=\"info-message\">\n        Processing, please wait...\n    </div>\n    <div class=\"button-section\">\n      <div class=\"button\" (click)=\"cancelWizard(null)\">\n        <div class=\"button-text\">\n          Cancel\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div *ngIf=\"processExecutionError\" class=\"step-info\">\n    <div class=\"info-message\">\n        Process execution error\n    </div>\n    <div class=\"button-section\">\n      <div class=\"button\" (click)=\"cancelWizard(null)\">\n        <div class=\"button-text\">\n          Cancel\n        </div>\n      </div>\n    </div>\n  </div>\n\n\n  <div *ngIf=\"showCompletedMessage\" class=\"step-info\">\n    <div class=\"info-message\">\n        Process completed\n    </div>\n    <div class=\"button-section\">\n      <div class=\"button\" (click)=\"cancelWizard(null)\">\n        <div class=\"button-text\">\n          Continue\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n",
                    styles: [".wizard-component{width:100%;height:100%;max-height:100%;position:relative}.wizard-component .step-content{width:100%;height:100%;position:relative}.wizard-component .step-info{height:100%;font-size:14px;position:relative}.wizard-component .step-info .info-message{position:absolute;top:0;left:0;bottom:50px;box-sizing:border-box;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px}.wizard-component .step-info .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.wizard-component .step-info .button-section .button{width:120px;background-color:#2b4054;height:36px;border-radius:5px;float:right;margin-top:8px;margin-right:8px;display:table;cursor:pointer}.wizard-component .step-info .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px;color:#fff}"]
                }] }
    ];
    /** @nocollapse */
    WizardComponent.ctorParameters = function () { return [
        { type: ActorService },
        { type: ProjectService },
        { type: AnalysisService }
    ]; };
    WizardComponent.propDecorators = {
        project: [{ type: Input }],
        projectGuid: [{ type: Input }],
        username: [{ type: Input }],
        externalActorIdNr: [{ type: Input }],
        latitude: [{ type: Input }],
        longitude: [{ type: Input }],
        onWizardCancelled: [{ type: Output }],
        onWizardCompleted: [{ type: Output }],
        onOpenCameraListener: [{ type: Output }],
        onQuestionAnswered: [{ type: Output }],
        onReadCode: [{ type: Output }]
    };
    return WizardComponent;
}());
export { WizardComponent };
if (false) {
    /** @type {?} */
    WizardComponent.prototype.project;
    /** @type {?} */
    WizardComponent.prototype.projectGuid;
    /** @type {?} */
    WizardComponent.prototype.username;
    /** @type {?} */
    WizardComponent.prototype.externalActorIdNr;
    /** @type {?} */
    WizardComponent.prototype.latitude;
    /** @type {?} */
    WizardComponent.prototype.longitude;
    /** @type {?} */
    WizardComponent.prototype.onWizardCancelled;
    /** @type {?} */
    WizardComponent.prototype.onWizardCompleted;
    /** @type {?} */
    WizardComponent.prototype.onOpenCameraListener;
    /** @type {?} */
    WizardComponent.prototype.onQuestionAnswered;
    /** @type {?} */
    WizardComponent.prototype.onReadCode;
    /**
     * @type {?}
     * @private
     */
    WizardComponent.prototype.actor;
    /** @type {?} */
    WizardComponent.prototype.userId;
    /** @type {?} */
    WizardComponent.prototype.processError;
    /** @type {?} */
    WizardComponent.prototype.processExecutionError;
    /** @type {?} */
    WizardComponent.prototype.cancelled;
    /** @type {?} */
    WizardComponent.prototype.businessProcessInstance;
    /** @type {?} */
    WizardComponent.prototype.stepInstance;
    /** @type {?} */
    WizardComponent.prototype.projectName;
    /** @type {?} */
    WizardComponent.prototype.showWaitIndicator;
    /** @type {?} */
    WizardComponent.prototype.showCompletedMessage;
    /** @type {?} */
    WizardComponent.prototype.completionCount;
    /** @type {?} */
    WizardComponent.prototype.showParticipantSearch;
    /** @type {?} */
    WizardComponent.prototype.loading;
    /**
     * @type {?}
     * @private
     */
    WizardComponent.prototype.actorService;
    /**
     * @type {?}
     * @private
     */
    WizardComponent.prototype.projectService;
    /**
     * @type {?}
     * @private
     */
    WizardComponent.prototype.analysisService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2l6YXJkLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvd2l6YXJkL3dpemFyZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQXFCLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzFGLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFHckMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ3pELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFFL0QsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBUTVDO0lBZ0NFLHlCQUFvQixZQUEwQixFQUFVLGNBQThCLEVBQzFFLGVBQWdDO1FBRHhCLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQVUsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzFFLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQXJCbEMsc0JBQWlCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN2QyxzQkFBaUIsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3ZDLHlCQUFvQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDMUMsdUJBQWtCLEdBQUcsSUFBSSxZQUFZLEVBQWdCLENBQUM7UUFDdEQsZUFBVSxHQUFHLElBQUksWUFBWSxFQUFvQixDQUFDO1FBSTVELGlCQUFZLEdBQVksS0FBSyxDQUFDO1FBQzlCLDBCQUFxQixHQUFZLEtBQUssQ0FBQztRQUN2QyxjQUFTLEdBQVksS0FBSyxDQUFDO1FBSzNCLHlCQUFvQixHQUFZLEtBQUssQ0FBQztRQUV0QywwQkFBcUIsR0FBWSxLQUFLLENBQUM7UUFDdkMsWUFBTyxHQUFZLEtBQUssQ0FBQztJQUl6QixDQUFDOzs7O0lBRUQsa0NBQVE7OztJQUFSO1FBQUEsaUJBa0JDO1FBakJDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7UUFDOUIsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDMUIsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztRQUVuQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztRQUNsQixJQUFJLENBQUMsdUJBQXVCLEdBQUcsSUFBSSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxLQUFLLENBQUM7UUFDbkMsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztTQUNuQjthQUFNLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUMzQixJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSTs7OztZQUFDLFVBQUEsTUFBTTtnQkFDMUQsS0FBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7Z0JBQ3RCLEtBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUNwQixDQUFDLEVBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7OztJQUVELHFDQUFXOzs7SUFBWDtJQUNBLENBQUM7Ozs7O0lBRU8sb0NBQVU7Ozs7SUFBbEI7UUFBQSxpQkFvQ0M7UUFuQ0MsSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxVQUFBLE1BQU07WUFDeEQsSUFBSSxNQUFNLEVBQUU7Z0JBQ1YsS0FBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDO2dCQUN4QixJQUFJLEtBQUksQ0FBQyxpQkFBaUIsRUFBRTtvQkFDMUIsS0FBSSxDQUFDLFlBQVksQ0FBQyw2QkFBNkIsQ0FBQyxLQUFJLENBQUMsaUJBQWlCLEVBQUUsS0FBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJOzs7O29CQUFDLFVBQUEsT0FBTzt3QkFDbkcsS0FBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUM7d0JBQ3JCLElBQUksS0FBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUU7NEJBQzlCLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7eUJBQzNDOzZCQUFNOzRCQUNMLDZEQUE2RDs0QkFDN0QsSUFBSSxLQUFJLENBQUMsS0FBSyxFQUFFO2dDQUNkLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsS0FBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQzs2QkFDcEQ7aUNBQU07Z0NBQ0wsS0FBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQztnQ0FDbEMsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7NkJBQ3RCO3lCQUNGO29CQUNILENBQUMsRUFBQyxDQUFDO2lCQUNKO3FCQUFNO29CQUNMLElBQUksS0FBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUU7d0JBQzlCLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7cUJBQzNDO3lCQUFNO3dCQUNMLElBQUksS0FBSSxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsS0FBSyxhQUFhLElBQUksTUFBTSxDQUFDLEtBQUssRUFBRTs0QkFDdkUsS0FBSSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDOzRCQUMxQixLQUFJLENBQUMsV0FBVyxDQUFDLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEtBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7eUJBQ3BEOzZCQUFNOzRCQUNMLEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUM7NEJBQ2xDLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO3lCQUN0QjtxQkFDRjtpQkFDRjthQUNGO2lCQUFNO2dCQUNMLCtCQUErQjthQUNoQztRQUNILENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCwrQ0FBcUI7Ozs7SUFBckIsVUFBc0IsV0FBeUI7UUFDN0MsSUFBSSxDQUFDLEtBQUssR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDO1FBQy9CLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxLQUFLLENBQUM7UUFDbkMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3JELENBQUM7Ozs7Ozs7SUFFTyxxQ0FBVzs7Ozs7O0lBQW5CLFVBQW9CLFdBQW1CLEVBQUUsT0FBZTtRQUF4RCxpQkFXQztRQVZDLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxLQUFLLENBQUM7UUFDbkMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsT0FBTyxDQUFDLENBQUMsSUFBSTs7OztRQUFDLFVBQUEsdUJBQXVCO1lBQ3ZGLEtBQUksQ0FBQyx1QkFBdUIsR0FBRyx1QkFBdUIsQ0FBQztZQUN2RCxLQUFJLENBQUMsV0FBVyxHQUFHLHVCQUF1QixDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDO1lBQy9FLEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1lBQzFCLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLENBQUM7Ozs7UUFBRSxVQUFBLEtBQUs7WUFDTixLQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztZQUN6QixLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUN2QixDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsK0NBQXFCOzs7O0lBQXJCLFVBQXNCLEtBQUs7UUFDekIsSUFBSSxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUN4RCxJQUFJLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUk7Ozs7WUFBQyxVQUFBLE1BQU07Z0JBQ3JHLDRCQUE0QjtZQUM5QixDQUFDLEVBQUMsQ0FBQztTQUNKO1FBRUQsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ3JCLENBQUM7Ozs7O0lBRU8sNENBQWtCOzs7O0lBQTFCO1FBQUEsaUJBT0M7UUFOQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRTs7Z0JBQ2YsS0FBSyxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO1lBQ2xDLEtBQUssQ0FBQyxTQUFTOzs7O1lBQUMsVUFBQSxDQUFDO2dCQUNmLEtBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUNyQixDQUFDLEVBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7Ozs7SUFFTyxxQ0FBVzs7OztJQUFuQjtRQUFBLGlCQXFEQztRQXBEQyxPQUFPLENBQUMsR0FBRyxDQUFDLGtEQUFrRCxDQUFDLENBQUM7UUFDaEUsSUFBSSxDQUFDLGNBQWMsQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSTs7OztRQUFDLFVBQUEsTUFBTTtZQUN0RixLQUFJLENBQUMsdUJBQXVCLEdBQUcsTUFBTSxDQUFDLHVCQUF1QixDQUFDO1lBQzlELElBQUksTUFBTSxDQUFDLFlBQVksSUFBSSxJQUFJLEVBQUU7Z0JBQy9CLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsQ0FBQztnQkFDckMsS0FBSSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDO2dCQUN4QyxJQUFJLE1BQU0sQ0FBQyx1QkFBdUIsQ0FBQyxhQUFhLEtBQUssV0FBVyxFQUFFO29CQUNoRSxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUN6QixLQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztvQkFDeEIsb0ZBQW9GO29CQUNwRixvQ0FBb0M7b0JBQ3BDLE1BQU07aUJBQ1A7cUJBQU0sSUFBSSxNQUFNLENBQUMsdUJBQXVCLENBQUMsYUFBYSxLQUFLLFFBQVEsRUFBRTtvQkFDcEUsS0FBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQztpQkFDbkM7cUJBQU07b0JBQ0wsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO29CQUNwQyxLQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztpQkFDM0I7YUFDRjtpQkFBTSxJQUFJLEtBQUksQ0FBQyxZQUFZLElBQUksS0FBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLEtBQUssTUFBTSxDQUFDLFlBQVksQ0FBQyxFQUFFLEVBQUU7Z0JBQy9FLEtBQUksQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQztnQkFDeEMsSUFBSSxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEtBQUssZUFBZTtvQkFDbkQsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEtBQUssZ0JBQWdCLElBQUksQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRTtvQkFDaEcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtREFBbUQsQ0FBQyxDQUFDO29CQUNqRSxLQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztpQkFDekI7cUJBQU07b0JBQ0wsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtREFBbUQsQ0FBQyxDQUFDO29CQUNqRSw2QkFBNkI7b0JBQzdCLEtBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDO2lCQUN6QzthQUNGO2lCQUFNO2dCQUNMLE9BQU8sQ0FBQyxHQUFHLENBQUMsbURBQW1ELENBQUMsQ0FBQztnQkFDakUsS0FBSSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDO2dCQUN4QyxJQUFJLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsS0FBSyxlQUFlO29CQUNuRCxDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsS0FBSyxnQkFBZ0IsSUFBSSxDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFO29CQUNoRyxLQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztpQkFDekI7cUJBQU07b0JBQ0wsSUFBSSxLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRTt3QkFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO3dCQUN2QyxJQUFJLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEVBQUUsS0FBSyxLQUFJLENBQUMsdUJBQXVCLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxFQUFDLDJEQUEyRDs0QkFDcEksT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzs0QkFDakUsS0FBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUM7eUJBQ3pDOzZCQUFNOzRCQUNMLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsQ0FBQzs0QkFDckMsS0FBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7eUJBQzVCO3FCQUNGO3lCQUFNO3dCQUNMLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsQ0FBQzt3QkFDckMsS0FBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7cUJBQzVCO2lCQUNGO2FBQ0Y7UUFDSCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7OztJQUVPLHVDQUFhOzs7OztJQUFyQixVQUFzQixZQUEwQjtRQUM5QyxJQUFJLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxLQUFLLHdCQUF3QixJQUFJLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxLQUFLLFNBQVM7WUFDbkcsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEtBQUssY0FBYyxJQUFJLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxLQUFLLHlCQUF5QjtZQUN6RyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsS0FBSyxVQUFVLElBQUksWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEtBQUssb0JBQW9CO1lBQ2hHLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxLQUFLLGlCQUFpQixJQUFJLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxLQUFLLG1CQUFtQjtZQUN0RyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsS0FBSywyQkFBMkIsSUFBSSxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsS0FBSyx1QkFBdUI7WUFDcEgsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEtBQUssTUFBTSxJQUFJLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxLQUFLLFVBQVU7WUFDbEYsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEtBQUssb0JBQW9CLEVBQUU7WUFDdkQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztTQUNoQzthQUFNO1lBQ0wsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7U0FDM0I7SUFDSCxDQUFDOzs7OztJQUVPLDBDQUFnQjs7OztJQUF4QjtRQUNFLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUM7UUFDakMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7UUFDekIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2hDLENBQUM7Ozs7O0lBRU8sNkNBQW1COzs7O0lBQTNCO1FBQ0UsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7SUFDNUIsQ0FBQzs7Ozs7SUFFRCxzQ0FBWTs7OztJQUFaLFVBQWEsS0FBVTtRQUNyQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDaEMsQ0FBQzs7Ozs7SUFFRCxvQ0FBVTs7OztJQUFWLFVBQVcsY0FBOEI7UUFDdkMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUNqRCxDQUFDOzs7OztJQUVELDBDQUFnQjs7OztJQUFoQixVQUFpQixZQUEwQjtRQUN6QyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQzdDLENBQUM7Ozs7O0lBRUQsa0NBQVE7Ozs7SUFBUixVQUFTLGdCQUFrQztRQUN6QyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO0lBQ3pDLENBQUM7O2dCQXZPRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLFlBQVk7b0JBQ3RCLGc2TUFBc0M7O2lCQUV2Qzs7OztnQkFoQlEsWUFBWTtnQkFDWixjQUFjO2dCQUNkLGVBQWU7OzswQkFnQnJCLEtBQUs7OEJBQ0wsS0FBSzsyQkFDTCxLQUFLO29DQUNMLEtBQUs7MkJBQ0wsS0FBSzs0QkFDTCxLQUFLO29DQUNMLE1BQU07b0NBQ04sTUFBTTt1Q0FDTixNQUFNO3FDQUNOLE1BQU07NkJBQ04sTUFBTTs7SUF3TlQsc0JBQUM7Q0FBQSxBQXhPRCxJQXdPQztTQW5PWSxlQUFlOzs7SUFDMUIsa0NBQTBCOztJQUMxQixzQ0FBNkI7O0lBQzdCLG1DQUEwQjs7SUFDMUIsNENBQW1DOztJQUNuQyxtQ0FBMEI7O0lBQzFCLG9DQUEyQjs7SUFDM0IsNENBQWlEOztJQUNqRCw0Q0FBaUQ7O0lBQ2pELCtDQUFvRDs7SUFDcEQsNkNBQWdFOztJQUNoRSxxQ0FBNEQ7Ozs7O0lBRTVELGdDQUFxQjs7SUFDckIsaUNBQWU7O0lBQ2YsdUNBQThCOztJQUM5QixnREFBdUM7O0lBQ3ZDLG9DQUEyQjs7SUFDM0Isa0RBQWlEOztJQUNqRCx1Q0FBMkI7O0lBQzNCLHNDQUFvQjs7SUFDcEIsNENBQTJCOztJQUMzQiwrQ0FBc0M7O0lBQ3RDLDBDQUF3Qjs7SUFDeEIsZ0RBQXVDOztJQUN2QyxrQ0FBeUI7Ozs7O0lBRWIsdUNBQWtDOzs7OztJQUFFLHlDQUFzQzs7Ozs7SUFDbEYsMENBQXdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIE9uRGVzdHJveSwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcy9SeCc7XG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzL1J4JztcblxuaW1wb3J0IHsgQWN0b3JTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvYWN0b3Iuc2VydmljZSc7XG5pbXBvcnQgeyBQcm9qZWN0U2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL3Byb2plY3Quc2VydmljZSc7XG5pbXBvcnQgeyBBbmFseXNpc1NlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9hbmFseXNpcy5zZXJ2aWNlJztcbmltcG9ydCB7IFN0ZXBJbnN0YW5jZSB9IGZyb20gJy4uL2RvbWFpbi9zdGVwLWluc3RhbmNlJztcbmltcG9ydCB7IFByb2plY3QgfSBmcm9tICcuLi9kb21haW4vcHJvamVjdCc7XG5pbXBvcnQgeyBBY3RvciB9IGZyb20gJy4uL2RvbWFpbi9hY3Rvcic7XG5pbXBvcnQgeyBCdXNpbmVzc1Byb2Nlc3NJbnN0YW5jZSB9IGZyb20gJy4uL2RvbWFpbi9idXNpbmVzcy1wcm9jZXNzLWluc3RhbmNlJztcbmltcG9ydCB7IFF1ZXN0aW9uTGluZSB9IGZyb20gJy4uL2RvbWFpbi9xdWVzdGlvbi1saW5lJztcbmltcG9ydCB7IEltYWdlQ29udGFpbmVyIH0gZnJvbSAnLi4vZG9tYWluL2ltYWdlLWNvbnRhaW5lcic7XG5pbXBvcnQgeyBQcm9qZWN0QWN0b3IgfSBmcm9tICcuLi9kb21haW4vcHJvamVjdC1hY3Rvcic7XG5pbXBvcnQgeyBDb2RlUmVhZExpc3RlbmVyIH0gZnJvbSAnLi4vc3RlcHMvY29kZS1yZWFkZXIvY29kZS1yZWFkLWxpc3RlbmVyJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYWNuLXdpemFyZCcsXG4gIHRlbXBsYXRlVXJsOiAnLi93aXphcmQuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi93aXphcmQuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBXaXphcmRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIHByb2plY3Q6IFByb2plY3Q7XG4gIEBJbnB1dCgpIHByb2plY3RHdWlkOiBzdHJpbmc7XG4gIEBJbnB1dCgpIHVzZXJuYW1lOiBzdHJpbmc7XG4gIEBJbnB1dCgpIGV4dGVybmFsQWN0b3JJZE5yOiBzdHJpbmc7XG4gIEBJbnB1dCgpIGxhdGl0dWRlOiBudW1iZXI7XG4gIEBJbnB1dCgpIGxvbmdpdHVkZTogbnVtYmVyO1xuICBAT3V0cHV0KCkgb25XaXphcmRDYW5jZWxsZWQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBvbldpemFyZENvbXBsZXRlZCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIG9uT3BlbkNhbWVyYUxpc3RlbmVyID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgb25RdWVzdGlvbkFuc3dlcmVkID0gbmV3IEV2ZW50RW1pdHRlcjxRdWVzdGlvbkxpbmU+KCk7XG4gIEBPdXRwdXQoKSBvblJlYWRDb2RlID0gbmV3IEV2ZW50RW1pdHRlcjxDb2RlUmVhZExpc3RlbmVyPigpO1xuXG4gIHByaXZhdGUgYWN0b3I6IEFjdG9yO1xuICB1c2VySWQ6IG51bWJlcjtcbiAgcHJvY2Vzc0Vycm9yOiBib29sZWFuID0gZmFsc2U7XG4gIHByb2Nlc3NFeGVjdXRpb25FcnJvcjogYm9vbGVhbiA9IGZhbHNlO1xuICBjYW5jZWxsZWQ6IGJvb2xlYW4gPSBmYWxzZTtcbiAgYnVzaW5lc3NQcm9jZXNzSW5zdGFuY2U6IEJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlO1xuICBzdGVwSW5zdGFuY2U6IFN0ZXBJbnN0YW5jZTtcbiAgcHJvamVjdE5hbWU6IHN0cmluZztcbiAgc2hvd1dhaXRJbmRpY2F0b3I6IGJvb2xlYW47XG4gIHNob3dDb21wbGV0ZWRNZXNzYWdlOiBib29sZWFuID0gZmFsc2U7XG4gIGNvbXBsZXRpb25Db3VudDogc3RyaW5nO1xuICBzaG93UGFydGljaXBhbnRTZWFyY2g6IGJvb2xlYW4gPSBmYWxzZTtcbiAgbG9hZGluZzogYm9vbGVhbiA9IGZhbHNlO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgYWN0b3JTZXJ2aWNlOiBBY3RvclNlcnZpY2UsIHByaXZhdGUgcHJvamVjdFNlcnZpY2U6IFByb2plY3RTZXJ2aWNlLFxuICAgICAgcHJpdmF0ZSBhbmFseXNpc1NlcnZpY2U6IEFuYWx5c2lzU2VydmljZSkge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcbiAgICB0aGlzLnNob3dXYWl0SW5kaWNhdG9yID0gdHJ1ZTtcbiAgICB0aGlzLnByb2Nlc3NFcnJvciA9IGZhbHNlO1xuICAgIHRoaXMucHJvY2Vzc0V4ZWN1dGlvbkVycm9yID0gZmFsc2U7XG5cbiAgICB0aGlzLmFjdG9yID0gbnVsbDtcbiAgICB0aGlzLmJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlID0gbnVsbDtcbiAgICB0aGlzLnN0ZXBJbnN0YW5jZSA9IG51bGw7XG4gICAgdGhpcy5zaG93UGFydGljaXBhbnRTZWFyY2ggPSBmYWxzZTtcbiAgICBpZiAodGhpcy5wcm9qZWN0KSB7XG4gICAgICB0aGlzLmluaXRpYWxpc2UoKTtcbiAgICB9IGVsc2UgaWYgKHRoaXMucHJvamVjdEd1aWQpIHtcbiAgICAgIHRoaXMucHJvamVjdFNlcnZpY2UuZ2V0UHJvamVjdCh0aGlzLnByb2plY3RHdWlkKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICAgIHRoaXMucHJvamVjdCA9IHJlc3VsdDtcbiAgICAgICAgdGhpcy5pbml0aWFsaXNlKCk7XG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICBuZ09uRGVzdHJveSgpIHtcbiAgfVxuXG4gIHByaXZhdGUgaW5pdGlhbGlzZSgpIHtcbiAgICB0aGlzLmFjdG9yU2VydmljZS5nZXRTeXN0ZW1Vc2VyKHRoaXMudXNlcm5hbWUpLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgIGlmIChyZXN1bHQpIHtcbiAgICAgICAgdGhpcy51c2VySWQgPSByZXN1bHQuaWQ7XG4gICAgICAgIGlmICh0aGlzLmV4dGVybmFsQWN0b3JJZE5yKSB7XG4gICAgICAgICAgdGhpcy5hY3RvclNlcnZpY2UuZ2V0UHJvamVjdEFjdG9yRnJvbUV4dGVybmFsSWQodGhpcy5leHRlcm5hbEFjdG9ySWROciwgdGhpcy5wcm9qZWN0LmlkKS50aGVuKHJlc3VsdDIgPT4ge1xuICAgICAgICAgICAgdGhpcy5hY3RvciA9IHJlc3VsdDI7XG4gICAgICAgICAgICBpZiAodGhpcy5wcm9qZWN0LndpemFyZFByb2plY3QpIHtcbiAgICAgICAgICAgICAgdGhpcy5zdGFydFdpemFyZCh0aGlzLnByb2plY3QuZ3VpZCwgbnVsbCk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAvLyBpZiAodGhpcy5wcm9qZWN0LmFjdG9yUHJvamVjdEZ1bmN0aW9uID09PSBcIlBBUlRJQ0lQQU5UXCIpIHtcbiAgICAgICAgICAgICAgaWYgKHRoaXMuYWN0b3IpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnN0YXJ0V2l6YXJkKHRoaXMucHJvamVjdC5ndWlkLCB0aGlzLmFjdG9yLmlkKTtcbiAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLnNob3dQYXJ0aWNpcGFudFNlYXJjaCA9IHRydWU7XG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBpZiAodGhpcy5wcm9qZWN0LndpemFyZFByb2plY3QpIHtcbiAgICAgICAgICAgIHRoaXMuc3RhcnRXaXphcmQodGhpcy5wcm9qZWN0Lmd1aWQsIG51bGwpO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBpZiAodGhpcy5wcm9qZWN0LmFjdG9yUHJvamVjdEZ1bmN0aW9uID09PSBcIlBBUlRJQ0lQQU5UXCIgJiYgcmVzdWx0LmFjdG9yKSB7XG4gICAgICAgICAgICAgIHRoaXMuYWN0b3IgPSByZXN1bHQuYWN0b3I7XG4gICAgICAgICAgICAgIHRoaXMuc3RhcnRXaXphcmQodGhpcy5wcm9qZWN0Lmd1aWQsIHRoaXMuYWN0b3IuaWQpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgdGhpcy5zaG93UGFydGljaXBhbnRTZWFyY2ggPSB0cnVlO1xuICAgICAgICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIC8vIE9uZ2VsZGlnZSB1c2VyLCB3eXMgYm9vZHNrYXBcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIG9uUGFydGljaXBhbnRTZWxlY3RlZChwYXJ0aWNpcGFudDogUHJvamVjdEFjdG9yKSB7XG4gICAgdGhpcy5hY3RvciA9IHBhcnRpY2lwYW50LmFjdG9yO1xuICAgIHRoaXMuc2hvd1BhcnRpY2lwYW50U2VhcmNoID0gZmFsc2U7XG4gICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcbiAgICB0aGlzLnN0YXJ0V2l6YXJkKHRoaXMucHJvamVjdC5ndWlkLCB0aGlzLmFjdG9yLmlkKTtcbiAgfVxuXG4gIHByaXZhdGUgc3RhcnRXaXphcmQocHJvamVjdEd1aWQ6IHN0cmluZywgYWN0b3JJZDogbnVtYmVyKSB7XG4gICAgdGhpcy5wcm9jZXNzRXhlY3V0aW9uRXJyb3IgPSBmYWxzZTtcbiAgICB0aGlzLnByb2plY3RTZXJ2aWNlLnN0YXJ0V2l6YXJkUHJvamVjdChwcm9qZWN0R3VpZCwgYWN0b3JJZCkudGhlbihidXNpbmVzc1Byb2Nlc3NJbnN0YW5jZSA9PiB7XG4gICAgICB0aGlzLmJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlID0gYnVzaW5lc3NQcm9jZXNzSW5zdGFuY2U7XG4gICAgICB0aGlzLnByb2plY3ROYW1lID0gYnVzaW5lc3NQcm9jZXNzSW5zdGFuY2UuYnVzaW5lc3NQcm9jZXNzLnByb2plY3QucHJvamVjdE5hbWU7XG4gICAgICB0aGlzLmdldE5leHRTdGVwRGVsYXllZCgpO1xuICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgfSwgZXJyb3IgPT4ge1xuICAgICAgdGhpcy5wcm9jZXNzRXJyb3IgPSB0cnVlO1xuICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgfSk7XG4gIH1cblxuICBzdGVwQ29tcGxldGVkTGlzdGVuZXIoZXZlbnQpIHtcbiAgICBpZiAodGhpcy5zdGVwSW5zdGFuY2UgJiYgdGhpcy5sYXRpdHVkZSAmJiB0aGlzLmxvbmdpdHVkZSkge1xuICAgICAgdGhpcy5wcm9qZWN0U2VydmljZS51cGRhdGVTdGVwTG9jYXRpb24odGhpcy5zdGVwSW5zdGFuY2UuaWQsIHRoaXMubGF0aXR1ZGUsIHRoaXMubG9uZ2l0dWRlKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICAgIC8vIGhvZWYgbmllIGlldHMgdGUgZG9lbiBuaWVcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIHRoaXMuZ2V0TmV4dFN0ZXAoKTtcbiAgfVxuXG4gIHByaXZhdGUgZ2V0TmV4dFN0ZXBEZWxheWVkKCkge1xuICAgIGlmICghdGhpcy5jYW5jZWxsZWQpIHtcbiAgICAgIGxldCB0aW1lciA9IE9ic2VydmFibGUudGltZXIoMTAwMCk7XG4gICAgICB0aW1lci5zdWJzY3JpYmUodCA9PiB7XG4gICAgICAgIHRoaXMuZ2V0TmV4dFN0ZXAoKTtcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgZ2V0TmV4dFN0ZXAoKSB7XG4gICAgY29uc29sZS5sb2coXCItLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cIik7XG4gICAgdGhpcy5wcm9qZWN0U2VydmljZS5nZXRCdXNpbmVzc1Byb2Nlc3NTdGF0ZSh0aGlzLmJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlLmlkKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICB0aGlzLmJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlID0gcmVzdWx0LmJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlO1xuICAgICAgaWYgKHJlc3VsdC5zdGVwSW5zdGFuY2UgPT0gbnVsbCkge1xuICAgICAgICBjb25zb2xlLmxvZyhcInN0ZXBJbnN0YW5jZSA9PSBudWxsIFwiKTtcbiAgICAgICAgdGhpcy5zdGVwSW5zdGFuY2UgPSByZXN1bHQuc3RlcEluc3RhbmNlO1xuICAgICAgICBpZiAocmVzdWx0LmJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlLnByb2Nlc3NTdGF0dXMgPT09ICdDT01QTEVURUQnKSB7XG4gICAgICAgICAgY29uc29sZS5sb2coXCJDT01QTEVURURcIik7XG4gICAgICAgICAgdGhpcy5wcm9jZXNzQ29tcGxldGVkKCk7XG4gICAgICAgICAgLy8gdGhpcy5hbmFseXNpc1NlcnZpY2UuZ2V0UHJvamVjdENvbXBsZXRpb25Db3VudCh0aGlzLnByb2plY3QuZ3VpZCkudGhlbihjb3VudCA9PiB7XG4gICAgICAgICAgLy8gICAgIHRoaXMuY29tcGxldGlvbkNvdW50ID0gY291bnQ7XG4gICAgICAgICAgLy8gfSk7XG4gICAgICAgIH0gZWxzZSBpZiAocmVzdWx0LmJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlLnByb2Nlc3NTdGF0dXMgPT09ICdGQUlMRUQnKSB7XG4gICAgICAgICAgdGhpcy5wcm9jZXNzRXhlY3V0aW9uRXJyb3IgPSB0cnVlO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGNvbnNvbGUubG9nKFwiZ2V0TmV4dFN0ZXBEZWxheWVkIDFcIik7XG4gICAgICAgICAgdGhpcy5nZXROZXh0U3RlcERlbGF5ZWQoKTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIGlmICh0aGlzLnN0ZXBJbnN0YW5jZSAmJiB0aGlzLnN0ZXBJbnN0YW5jZS5pZCA9PT0gcmVzdWx0LnN0ZXBJbnN0YW5jZS5pZCkge1xuICAgICAgICB0aGlzLnN0ZXBJbnN0YW5jZSA9IHJlc3VsdC5zdGVwSW5zdGFuY2U7XG4gICAgICAgIGlmICh0aGlzLnN0ZXBJbnN0YW5jZS5zdGVwLnN0ZXBUeXBlID09PSAnQ0FOQ0VMX1dJWkFSRCcgfHxcbiAgICAgICAgICAgICh0aGlzLnN0ZXBJbnN0YW5jZS5zdGVwLnN0ZXBUeXBlID09PSAnTk9ORV9FTkRfRVZFTlQnICYmICF0aGlzLnN0ZXBJbnN0YW5jZS5zdGVwLnBhcmVudFN0ZXApKSB7XG4gICAgICAgICAgY29uc29sZS5sb2coXCJ0aGlzLnN0ZXBJbnN0YW5jZS5pZCA9PT0gcmVzdWx0LnN0ZXBJbnN0YW5jZS5pZCAxXCIpO1xuICAgICAgICAgIHRoaXMucHJvY2Vzc0NvbXBsZXRlZCgpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGNvbnNvbGUubG9nKFwidGhpcy5zdGVwSW5zdGFuY2UuaWQgPT09IHJlc3VsdC5zdGVwSW5zdGFuY2UuaWQgMlwiKTtcbiAgICAgICAgICAvLyB0aGlzLmdldE5leHRTdGVwRGVsYXllZCgpO1xuICAgICAgICAgIHRoaXMuc2hvd1N0ZXBQYW5lbChyZXN1bHQuc3RlcEluc3RhbmNlKTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY29uc29sZS5sb2coXCJ0aGlzLnN0ZXBJbnN0YW5jZS5pZCA9PT0gcmVzdWx0LnN0ZXBJbnN0YW5jZS5pZCAzXCIpO1xuICAgICAgICB0aGlzLnN0ZXBJbnN0YW5jZSA9IHJlc3VsdC5zdGVwSW5zdGFuY2U7XG4gICAgICAgIGlmICh0aGlzLnN0ZXBJbnN0YW5jZS5zdGVwLnN0ZXBUeXBlID09PSAnQ0FOQ0VMX1dJWkFSRCcgfHxcbiAgICAgICAgICAgICh0aGlzLnN0ZXBJbnN0YW5jZS5zdGVwLnN0ZXBUeXBlID09PSAnTk9ORV9FTkRfRVZFTlQnICYmICF0aGlzLnN0ZXBJbnN0YW5jZS5zdGVwLnBhcmVudFN0ZXApKSB7XG4gICAgICAgICAgdGhpcy5wcm9jZXNzQ29tcGxldGVkKCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgaWYgKHRoaXMuc3RlcEluc3RhbmNlLmFjdG9yKSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcInRoaXMuc3RlcEluc3RhbmNlLmFjdG9yXCIpO1xuICAgICAgICAgICAgaWYgKHRoaXMuc3RlcEluc3RhbmNlLmFjdG9yLmlkID09PSB0aGlzLmJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlLmFjdG9yLmlkKSB7Ly8gfHwgc3RlcEluc3RhbmNlLmdldEFjdG9yKCkuZ2V0SWQoKS5lcXVhbHMoZXZhbHVhdG9ySWQpIHtcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJzaG93U3RlcFBhbmVsOiBcIiArIHRoaXMuc3RlcEluc3RhbmNlLnN0ZXAuc3RlcFR5cGUpO1xuICAgICAgICAgICAgICB0aGlzLnNob3dTdGVwUGFuZWwocmVzdWx0LnN0ZXBJbnN0YW5jZSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcInNob3dJblByb2dyZXNzUGFuZWwgMVwiKTtcbiAgICAgICAgICAgICAgdGhpcy5zaG93SW5Qcm9ncmVzc1BhbmVsKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwic2hvd0luUHJvZ3Jlc3NQYW5lbCAyXCIpO1xuICAgICAgICAgICAgdGhpcy5zaG93SW5Qcm9ncmVzc1BhbmVsKCk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIHNob3dTdGVwUGFuZWwoc3RlcEluc3RhbmNlOiBTdGVwSW5zdGFuY2UpIHtcbiAgICBpZiAoc3RlcEluc3RhbmNlLnN0ZXAuc3RlcFR5cGUgPT09ICdDT01QTEVURV9RVUVTVElPTk5BSVJFJyB8fCBzdGVwSW5zdGFuY2Uuc3RlcC5zdGVwVHlwZSA9PT0gJ01FU1NBR0UnIHx8XG4gICAgICAgIHN0ZXBJbnN0YW5jZS5zdGVwLnN0ZXBUeXBlID09PSAnSURfTlJfTE9PS1VQJyB8fCBzdGVwSW5zdGFuY2Uuc3RlcC5zdGVwVHlwZSA9PT0gJ1VQREFURV9QRVJTT05BTF9ERVRBSUxTJyB8fFxuICAgICAgICBzdGVwSW5zdGFuY2Uuc3RlcC5zdGVwVHlwZSA9PT0gJ1JFR0lTVEVSJyB8fCBzdGVwSW5zdGFuY2Uuc3RlcC5zdGVwVHlwZSA9PT0gJ0NSRUFURV9TWVNURU1fVVNFUicgfHxcbiAgICAgICAgc3RlcEluc3RhbmNlLnN0ZXAuc3RlcFR5cGUgPT09ICdTRUxFQ1RfUFJPVklERVInIHx8IHN0ZXBJbnN0YW5jZS5zdGVwLnN0ZXBUeXBlID09PSAnU0VMRUNUX1BST1ZJREVSXzInIHx8XG4gICAgICAgIHN0ZXBJbnN0YW5jZS5zdGVwLnN0ZXBUeXBlID09PSAnRkFDSUFMX0FVVEhFTlRJQ0FUSU9OX1JFRicgfHwgc3RlcEluc3RhbmNlLnN0ZXAuc3RlcFR5cGUgPT09ICdGQUNJQUxfQVVUSEVOVElDQVRJT04nIHx8XG4gICAgICAgIHN0ZXBJbnN0YW5jZS5zdGVwLnN0ZXBUeXBlID09PSAnV0FJVCcgfHwgc3RlcEluc3RhbmNlLnN0ZXAuc3RlcFR5cGUgPT09ICdSRUdJU1RFUicgfHxcbiAgICAgICAgc3RlcEluc3RhbmNlLnN0ZXAuc3RlcFR5cGUgPT09ICdDUkVBVEVfU1lTVEVNX1VTRVInKSB7XG4gICAgICB0aGlzLnNob3dXYWl0SW5kaWNhdG9yID0gZmFsc2U7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuZ2V0TmV4dFN0ZXBEZWxheWVkKCk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBwcm9jZXNzQ29tcGxldGVkKCkge1xuICAgIHRoaXMuc2hvd0NvbXBsZXRlZE1lc3NhZ2UgPSB0cnVlO1xuICAgIHRoaXMuc3RlcEluc3RhbmNlID0gbnVsbDtcbiAgICB0aGlzLm9uV2l6YXJkQ29tcGxldGVkLmVtaXQoKTtcbiAgfVxuXG4gIHByaXZhdGUgc2hvd0luUHJvZ3Jlc3NQYW5lbCgpIHtcbiAgICB0aGlzLmdldE5leHRTdGVwRGVsYXllZCgpO1xuICB9XG5cbiAgY2FuY2VsV2l6YXJkKGV2ZW50OiBhbnkpIHtcbiAgICB0aGlzLnN0ZXBJbnN0YW5jZSA9IG51bGw7XG4gICAgdGhpcy5jYW5jZWxsZWQgPSB0cnVlO1xuICAgIHRoaXMub25XaXphcmRDYW5jZWxsZWQuZW1pdCgpO1xuICB9XG5cbiAgb3BlbkNhbWVyYShpbWFnZUNvbnRhaW5lcjogSW1hZ2VDb250YWluZXIpIHtcbiAgICB0aGlzLm9uT3BlbkNhbWVyYUxpc3RlbmVyLmVtaXQoaW1hZ2VDb250YWluZXIpO1xuICB9XG5cbiAgcXVlc3Rpb25BbnN3ZXJlZChxdWVzdGlvbkxpbmU6IFF1ZXN0aW9uTGluZSkge1xuICAgIHRoaXMub25RdWVzdGlvbkFuc3dlcmVkLmVtaXQocXVlc3Rpb25MaW5lKTtcbiAgfVxuXG4gIHJlYWRDb2RlKGNvZGVSZWFkTGlzdGVuZXI6IENvZGVSZWFkTGlzdGVuZXIpIHtcbiAgICB0aGlzLm9uUmVhZENvZGUuZW1pdChjb2RlUmVhZExpc3RlbmVyKTtcbiAgfVxufVxuIl19