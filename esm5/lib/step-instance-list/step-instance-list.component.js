/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { DomSanitizer } from '@angular/platform-browser';
import { ActorService } from '../services/actor.service';
import { ProjectService } from '../services/project.service';
import { AnalysisService } from '../services/analysis.service';
var StepInstanceListComponent = /** @class */ (function () {
    function StepInstanceListComponent(actorService, projectService, analysisService, sanitizer) {
        this.actorService = actorService;
        this.projectService = projectService;
        this.analysisService = analysisService;
        this.sanitizer = sanitizer;
        this.onStepInstanceSelected = new EventEmitter();
        this.stepInstances = [];
        this.selectedStepInstance = null;
        this.loading = false;
        this.descriptions = {};
    }
    /**
     * @return {?}
     */
    StepInstanceListComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.subscription = Observable.timer(1000, 10000).subscribe((/**
         * @param {?} t
         * @return {?}
         */
        function (t) {
            _this.loadStepInstances();
        }));
    };
    /**
     * @return {?}
     */
    StepInstanceListComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    };
    /**
     * @private
     * @return {?}
     */
    StepInstanceListComponent.prototype.loadStepInstances = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        // this.loading = true;
        // this.descriptions = {};
        this.projectService.getActiveStepInstances(this.stepIds).then((/**
         * @param {?} result
         * @return {?}
         */
        function (result) {
            var e_1, _a;
            if (result) {
                _this.stepInstances = result;
                var _loop_1 = function (stepInstance) {
                    if (stepInstance.step.stepType === "MESSAGE") {
                        // if (this.descriptions[stepInstance.id]) {
                        _this.projectService.getMessageStepHtml(stepInstance.id).then((/**
                         * @param {?} html
                         * @return {?}
                         */
                        function (html) {
                            if (html["text"]) {
                                _this.descriptions[stepInstance.id] = _this.sanitizer.bypassSecurityTrustHtml(html["text"]);
                            }
                            else {
                                _this.descriptions[stepInstance.id] = "<div></div>";
                            }
                        }));
                        // } else {
                        //   this.descriptions[stepInstance.id] = "<div></div>";
                        // }
                    }
                    else if (stepInstance.step.stepType === "PROGRESS_NOTES") {
                        _this.descriptions[stepInstance.id] = "<div>" + stepInstance.step.parameters["0"].parameterValue + "</div>";
                    }
                    else {
                        _this.descriptions[stepInstance.id] = "<div>" + stepInstance.step.stepType + "</div>";
                    }
                };
                try {
                    for (var _b = tslib_1.__values(_this.stepInstances), _c = _b.next(); !_c.done; _c = _b.next()) {
                        var stepInstance = _c.value;
                        _loop_1(stepInstance);
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
            }
            // this.loading = false;
        }), (/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            // this.loading = false;
        }));
    };
    /**
     * @param {?} stepInstance
     * @return {?}
     */
    StepInstanceListComponent.prototype.stepInstanceSelected = /**
     * @param {?} stepInstance
     * @return {?}
     */
    function (stepInstance) {
        this.selectedStepInstance = stepInstance;
        this.onStepInstanceSelected.emit(stepInstance);
    };
    StepInstanceListComponent.decorators = [
        { type: Component, args: [{
                    selector: 'acn-step-instance-list',
                    template: "<div class=\"step-instance-list\">\n  <!-- <loading-indicator [show]=\"loading\"></loading-indicator> -->\n\n  <div class=\"step-list\">\n    <div *ngFor=\"let stepInstance of stepInstances; odd as isOdd\">\n      <div class=\"in-tray-row\" [ngClass]=\"{ 'oddRow': isOdd, 'selectedRow': selectedStepInstance && selectedStepInstance.id === stepInstance.id }\"\n          (click)=\"stepInstanceSelected(stepInstance)\">\n        <div class=\"description\">\n          <div class=\"project\">\n            <div *ngIf=\"descriptions[stepInstance.id]\" class=\"message-content\" [innerHTML]=\"descriptions[stepInstance.id]\">\n            </div>\n          </div>\n          <div class=\"step-details\">\n            <div class=\"activation-date\">\n              Activated: {{ stepInstance.activationDate | date: 'yyyy-MM-dd HH:mm:ss' }}\n            </div>\n          </div>\n        </div>\n        <div class=\"cursor-image\">\n          <img src=\"data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAUVJREFUeJzt2EtOw0AURNELbCw7o9hZdgaTtBigBLv9+vM+JfUscnyPZw220+OknIDvx9HSN1kw8RufDkH8jU+DIJ7Hh0cQ/8eHRRDH48MhiPPxYRBEf7x7BHE93i2CsIt3hyDs47dBeF/8/58sRvg48Js78AbcBr3D7fH8+6Dnv9wRAAiMcBQAgiKcAYCACGcBIBhCDwAEQugFgCAIVwAgAMJVAHCOYAEAjhGsAMApgiUAOESwBgBnCCMAwBHCKABwgjASABwgjAaAzRFmAMDGCLMAYFOE1XeCqSbG3S5vccP8aqLiK77ijY9mhfRMVHzFV7zx0ayQnomKr/iKNz6aFdIzUfEVX/      HGR7NCeiYqPmb86jvBLzb/+m0i2JfvmUgc3yYSx7eJxPFtInF8m0gc3yYSx7eJxPFtInF8m0gc3yacxf8AAP323bWbSe8AAAAASUVORK5CYII=\">\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n",
                    styles: [".step-instance-list{width:100%;height:100%;position:relative}.step-instance-list .step-list{width:100%;height:100%;overflow-y:scroll}.step-instance-list .step-list .in-tray-row{display:table;width:100%;padding:5px;cursor:pointer}.step-instance-list .step-list .in-tray-row .description{display:table-cell;width:calc(100% - 32px);padding-left:10px;vertical-align:middle}.step-instance-list .step-list .in-tray-row .description .project{font-size:14px}.step-instance-list .step-list .in-tray-row .description .step-details{line-height:15px}.step-instance-list .step-list .in-tray-row .description .step-details .step{font-size:15px}.step-instance-list .step-list .in-tray-row .description .step-details .activation-date{font-size:10px}.step-instance-list .step-list .in-tray-row .cursor-image{display:table-cell;width:32px;text-align:center;vertical-align:middle}.step-instance-list .step-list .in-tray-row .cursor-image img{width:16px;height:16px}.step-instance-list .step-list .oddRow{background-color:#f2f2f2}.step-instance-list .step-list .selectedRow{background-color:#abb9d3}"]
                }] }
    ];
    /** @nocollapse */
    StepInstanceListComponent.ctorParameters = function () { return [
        { type: ActorService },
        { type: ProjectService },
        { type: AnalysisService },
        { type: DomSanitizer }
    ]; };
    StepInstanceListComponent.propDecorators = {
        stepIds: [{ type: Input }],
        onStepInstanceSelected: [{ type: Output }]
    };
    return StepInstanceListComponent;
}());
export { StepInstanceListComponent };
if (false) {
    /** @type {?} */
    StepInstanceListComponent.prototype.stepIds;
    /** @type {?} */
    StepInstanceListComponent.prototype.onStepInstanceSelected;
    /** @type {?} */
    StepInstanceListComponent.prototype.stepInstances;
    /** @type {?} */
    StepInstanceListComponent.prototype.selectedStepInstance;
    /**
     * @type {?}
     * @private
     */
    StepInstanceListComponent.prototype.subscription;
    /** @type {?} */
    StepInstanceListComponent.prototype.loading;
    /** @type {?} */
    StepInstanceListComponent.prototype.descriptions;
    /**
     * @type {?}
     * @private
     */
    StepInstanceListComponent.prototype.actorService;
    /**
     * @type {?}
     * @private
     */
    StepInstanceListComponent.prototype.projectService;
    /**
     * @type {?}
     * @private
     */
    StepInstanceListComponent.prototype.analysisService;
    /**
     * @type {?}
     * @private
     */
    StepInstanceListComponent.prototype.sanitizer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RlcC1pbnN0YW5jZS1saXN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvc3RlcC1pbnN0YW5jZS1saXN0L3N0ZXAtaW5zdGFuY2UtbGlzdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFxQixLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMxRixPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBRXJDLE9BQU8sRUFBRSxZQUFZLEVBQW1CLE1BQU0sMkJBQTJCLENBQUM7QUFFMUUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ3pELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFRL0Q7SUFjRSxtQ0FBb0IsWUFBMEIsRUFBVSxjQUE4QixFQUMxRSxlQUFnQyxFQUFVLFNBQXVCO1FBRHpELGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQVUsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzFFLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUFVLGNBQVMsR0FBVCxTQUFTLENBQWM7UUFSbkUsMkJBQXNCLEdBQUcsSUFBSSxZQUFZLEVBQWdCLENBQUM7UUFDcEUsa0JBQWEsR0FBd0IsRUFBRSxDQUFDO1FBQ3hDLHlCQUFvQixHQUFpQixJQUFJLENBQUM7UUFFMUMsWUFBTyxHQUFZLEtBQUssQ0FBQztRQUN6QixpQkFBWSxHQUFHLEVBQUUsQ0FBQztJQUlsQixDQUFDOzs7O0lBRUQsNENBQVE7OztJQUFSO1FBQUEsaUJBSUM7UUFIQyxJQUFJLENBQUMsWUFBWSxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDLFNBQVM7Ozs7UUFBQyxVQUFBLENBQUM7WUFDM0QsS0FBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7UUFDM0IsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7O0lBRUQsK0NBQVc7OztJQUFYO1FBQ0UsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3JCLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDakM7SUFDSCxDQUFDOzs7OztJQUVPLHFEQUFpQjs7OztJQUF6QjtRQUFBLGlCQThCQztRQTdCQyx1QkFBdUI7UUFDdkIsMEJBQTBCO1FBQzFCLElBQUksQ0FBQyxjQUFjLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxVQUFBLE1BQU07O1lBQ2xFLElBQUksTUFBTSxFQUFFO2dCQUNWLEtBQUksQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDO3dDQUNuQixZQUFZO29CQUNuQixJQUFJLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxLQUFLLFNBQVMsRUFBRTt3QkFDNUMsNENBQTRDO3dCQUMxQyxLQUFJLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJOzs7O3dCQUFDLFVBQUMsSUFBSTs0QkFDaEUsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0NBQ2hCLEtBQUksQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7NkJBQzNGO2lDQUFNO2dDQUNMLEtBQUksQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxHQUFHLGFBQWEsQ0FBQzs2QkFDcEQ7d0JBQ0gsQ0FBQyxFQUFDLENBQUM7d0JBQ0wsV0FBVzt3QkFDWCx3REFBd0Q7d0JBQ3hELElBQUk7cUJBQ0w7eUJBQU0sSUFBSSxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsS0FBSyxnQkFBZ0IsRUFBRTt3QkFDeEQsS0FBSSxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxHQUFHLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLGNBQWMsR0FBRyxRQUFRLENBQUM7cUJBQzlHO3lCQUFNO3dCQUNMLEtBQUksQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7cUJBQ3RGOzs7b0JBakJILEtBQXlCLElBQUEsS0FBQSxpQkFBQSxLQUFJLENBQUMsYUFBYSxDQUFBLGdCQUFBO3dCQUF0QyxJQUFJLFlBQVksV0FBQTtnQ0FBWixZQUFZO3FCQWtCcEI7Ozs7Ozs7OzthQUNGO1lBQ0Qsd0JBQXdCO1FBQzFCLENBQUM7Ozs7UUFBRSxVQUFBLEtBQUs7WUFDTix3QkFBd0I7UUFDMUIsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUVELHdEQUFvQjs7OztJQUFwQixVQUFxQixZQUEwQjtRQUM3QyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsWUFBWSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDakQsQ0FBQzs7Z0JBakVGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsd0JBQXdCO29CQUNsQyw2a0RBQWtEOztpQkFFbkQ7Ozs7Z0JBZFEsWUFBWTtnQkFDWixjQUFjO2dCQUNkLGVBQWU7Z0JBSmYsWUFBWTs7OzBCQWtCbEIsS0FBSzt5Q0FDTCxNQUFNOztJQTJEVCxnQ0FBQztDQUFBLEFBbEVELElBa0VDO1NBN0RZLHlCQUF5Qjs7O0lBQ3BDLDRDQUFnQzs7SUFDaEMsMkRBQW9FOztJQUNwRSxrREFBd0M7O0lBQ3hDLHlEQUEwQzs7Ozs7SUFDMUMsaURBQW1DOztJQUNuQyw0Q0FBeUI7O0lBQ3pCLGlEQUFrQjs7Ozs7SUFFTixpREFBa0M7Ozs7O0lBQUUsbURBQXNDOzs7OztJQUNsRixvREFBd0M7Ozs7O0lBQUUsOENBQStCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIE9uRGVzdHJveSwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcy9SeCc7XG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzL1J4JztcbmltcG9ydCB7IERvbVNhbml0aXplciwgU2FmZVJlc291cmNlVXJsIH0gZnJvbSAnQGFuZ3VsYXIvcGxhdGZvcm0tYnJvd3Nlcic7XG5cbmltcG9ydCB7IEFjdG9yU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2FjdG9yLnNlcnZpY2UnO1xuaW1wb3J0IHsgUHJvamVjdFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9wcm9qZWN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgQW5hbHlzaXNTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvYW5hbHlzaXMuc2VydmljZSc7XG5pbXBvcnQgeyBTdGVwSW5zdGFuY2UgfSBmcm9tICcuLi9kb21haW4vc3RlcC1pbnN0YW5jZSc7XG5pbXBvcnQgeyBQcm9qZWN0IH0gZnJvbSAnLi4vZG9tYWluL3Byb2plY3QnO1xuaW1wb3J0IHsgQWN0b3IgfSBmcm9tICcuLi9kb21haW4vYWN0b3InO1xuaW1wb3J0IHsgQnVzaW5lc3NQcm9jZXNzSW5zdGFuY2UgfSBmcm9tICcuLi9kb21haW4vYnVzaW5lc3MtcHJvY2Vzcy1pbnN0YW5jZSc7XG5pbXBvcnQgeyBRdWVzdGlvbkxpbmUgfSBmcm9tICcuLi9kb21haW4vcXVlc3Rpb24tbGluZSc7XG5pbXBvcnQgeyBJbWFnZUNvbnRhaW5lciB9IGZyb20gJy4uL2RvbWFpbi9pbWFnZS1jb250YWluZXInO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhY24tc3RlcC1pbnN0YW5jZS1saXN0JyxcbiAgdGVtcGxhdGVVcmw6ICcuL3N0ZXAtaW5zdGFuY2UtbGlzdC5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL3N0ZXAtaW5zdGFuY2UtbGlzdC5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIFN0ZXBJbnN0YW5jZUxpc3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIHN0ZXBJZHM6IEFycmF5PG51bWJlcj47XG4gIEBPdXRwdXQoKSBvblN0ZXBJbnN0YW5jZVNlbGVjdGVkID0gbmV3IEV2ZW50RW1pdHRlcjxTdGVwSW5zdGFuY2U+KCk7XG4gIHN0ZXBJbnN0YW5jZXM6IEFycmF5PFN0ZXBJbnN0YW5jZT4gPSBbXTtcbiAgc2VsZWN0ZWRTdGVwSW5zdGFuY2U6IFN0ZXBJbnN0YW5jZSA9IG51bGw7XG4gIHByaXZhdGUgc3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XG4gIGxvYWRpbmc6IGJvb2xlYW4gPSBmYWxzZTtcbiAgZGVzY3JpcHRpb25zID0ge307XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBhY3RvclNlcnZpY2U6IEFjdG9yU2VydmljZSwgcHJpdmF0ZSBwcm9qZWN0U2VydmljZTogUHJvamVjdFNlcnZpY2UsXG4gICAgICBwcml2YXRlIGFuYWx5c2lzU2VydmljZTogQW5hbHlzaXNTZXJ2aWNlLCBwcml2YXRlIHNhbml0aXplcjogRG9tU2FuaXRpemVyKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLnN1YnNjcmlwdGlvbiA9IE9ic2VydmFibGUudGltZXIoMTAwMCwgMTAwMDApLnN1YnNjcmliZSh0ID0+IHtcbiAgICAgIHRoaXMubG9hZFN0ZXBJbnN0YW5jZXMoKTtcbiAgICB9KTtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCkge1xuICAgIGlmICh0aGlzLnN1YnNjcmlwdGlvbikge1xuICAgICAgdGhpcy5zdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGxvYWRTdGVwSW5zdGFuY2VzKCkge1xuICAgIC8vIHRoaXMubG9hZGluZyA9IHRydWU7XG4gICAgLy8gdGhpcy5kZXNjcmlwdGlvbnMgPSB7fTtcbiAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmdldEFjdGl2ZVN0ZXBJbnN0YW5jZXModGhpcy5zdGVwSWRzKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICBpZiAocmVzdWx0KSB7XG4gICAgICAgIHRoaXMuc3RlcEluc3RhbmNlcyA9IHJlc3VsdDtcbiAgICAgICAgZm9yIChsZXQgc3RlcEluc3RhbmNlIG9mIHRoaXMuc3RlcEluc3RhbmNlcykge1xuICAgICAgICAgIGlmIChzdGVwSW5zdGFuY2Uuc3RlcC5zdGVwVHlwZSA9PT0gXCJNRVNTQUdFXCIpIHtcbiAgICAgICAgICAgIC8vIGlmICh0aGlzLmRlc2NyaXB0aW9uc1tzdGVwSW5zdGFuY2UuaWRdKSB7XG4gICAgICAgICAgICAgIHRoaXMucHJvamVjdFNlcnZpY2UuZ2V0TWVzc2FnZVN0ZXBIdG1sKHN0ZXBJbnN0YW5jZS5pZCkudGhlbigoaHRtbCkgPT4ge1xuICAgICAgICAgICAgICAgIGlmIChodG1sW1widGV4dFwiXSkge1xuICAgICAgICAgICAgICAgICAgdGhpcy5kZXNjcmlwdGlvbnNbc3RlcEluc3RhbmNlLmlkXSA9IHRoaXMuc2FuaXRpemVyLmJ5cGFzc1NlY3VyaXR5VHJ1c3RIdG1sKGh0bWxbXCJ0ZXh0XCJdKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgdGhpcy5kZXNjcmlwdGlvbnNbc3RlcEluc3RhbmNlLmlkXSA9IFwiPGRpdj48L2Rpdj5cIjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgLy8gfSBlbHNlIHtcbiAgICAgICAgICAgIC8vICAgdGhpcy5kZXNjcmlwdGlvbnNbc3RlcEluc3RhbmNlLmlkXSA9IFwiPGRpdj48L2Rpdj5cIjtcbiAgICAgICAgICAgIC8vIH1cbiAgICAgICAgICB9IGVsc2UgaWYgKHN0ZXBJbnN0YW5jZS5zdGVwLnN0ZXBUeXBlID09PSBcIlBST0dSRVNTX05PVEVTXCIpIHtcbiAgICAgICAgICAgICAgdGhpcy5kZXNjcmlwdGlvbnNbc3RlcEluc3RhbmNlLmlkXSA9IFwiPGRpdj5cIiArIHN0ZXBJbnN0YW5jZS5zdGVwLnBhcmFtZXRlcnNbXCIwXCJdLnBhcmFtZXRlclZhbHVlICsgXCI8L2Rpdj5cIjtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5kZXNjcmlwdGlvbnNbc3RlcEluc3RhbmNlLmlkXSA9IFwiPGRpdj5cIiArIHN0ZXBJbnN0YW5jZS5zdGVwLnN0ZXBUeXBlICsgXCI8L2Rpdj5cIjtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIC8vIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgIH0sIGVycm9yID0+IHtcbiAgICAgIC8vIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgIH0pO1xuICB9XG5cbiAgc3RlcEluc3RhbmNlU2VsZWN0ZWQoc3RlcEluc3RhbmNlOiBTdGVwSW5zdGFuY2UpIHtcbiAgICB0aGlzLnNlbGVjdGVkU3RlcEluc3RhbmNlID0gc3RlcEluc3RhbmNlO1xuICAgIHRoaXMub25TdGVwSW5zdGFuY2VTZWxlY3RlZC5lbWl0KHN0ZXBJbnN0YW5jZSk7XG4gIH1cbn1cbiJdfQ==