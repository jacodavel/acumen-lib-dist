/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ProjectService } from '../../services/project.service';
import { StepInstance } from '../../domain/step-instance';
var IdNrLookupComponent = /** @class */ (function () {
    function IdNrLookupComponent(sanitizer, projectService) {
        this.sanitizer = sanitizer;
        this.projectService = projectService;
        this.stepCompletedListener = new EventEmitter();
    }
    /**
     * @return {?}
     */
    IdNrLookupComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.noIdNr = false;
        this.idNrNotFound = false;
        this.invalidIdNr = false;
        this.hideNoIdNrWarning = this.stepInstance.step.parameters["0"] && this.stepInstance.step.parameters["0"].parameterValue;
        this.hideNoIdNrOption = this.stepInstance.step.parameters["1"] && this.stepInstance.step.parameters["1"].parameterValue;
        if (this.stepInstance.step.parameters["2"]) {
            this.idNrLabel = this.stepInstance.step.parameters["2"].parameterValue;
        }
        if (!this.idNrLabel) {
            this.idNrLabel = "Id number";
        }
        this.loading = true;
        this.projectService.getMessageStepHtml(this.stepInstance.id).then((/**
         * @param {?} html
         * @return {?}
         */
        function (html) {
            _this.loading = false;
            if (html["text"]) {
                _this.html = _this.sanitizer.bypassSecurityTrustHtml(html["text"]);
            }
        }));
    };
    /**
     * @return {?}
     */
    IdNrLookupComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    IdNrLookupComponent.prototype.continueWithIdNr = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.noIdNr = false;
        this.idNrNotFound = false;
        this.invalidIdNr = false;
        if (!this.idNr) {
            this.noIdNr = true;
            return;
        }
        // if (this.idNr.length !== 13 || !(/^\d+$/.test(this.idNr))) {
        //     this.invalidIdNr = true;
        //     return;
        // }
        this.projectService.verifyActorIdNr(this.stepInstance.businessProcessInstance.id, this.idNr).then((/**
         * @param {?} result
         * @return {?}
         */
        function (result) {
            if (result.response === "true") {
                _this.completeStep();
            }
            else {
                if (!_this.hideNoIdNrWarning) {
                    _this.idNrNotFound = true;
                }
                else {
                    _this.completeStep();
                }
            }
        }));
    };
    /**
     * @return {?}
     */
    IdNrLookupComponent.prototype.continueNoIdNr = /**
     * @return {?}
     */
    function () {
        this.completeStep();
    };
    /**
     * @return {?}
     */
    IdNrLookupComponent.prototype.completeStep = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.projectService.completeStep(this.stepInstance.id, this.userId).then((/**
         * @param {?} result
         * @return {?}
         */
        function (result) {
            _this.stepCompletedListener.emit({
                value: _this.stepInstance,
                useBusinessProcessId: true
            });
        }));
    };
    IdNrLookupComponent.decorators = [
        { type: Component, args: [{
                    selector: 'id-nr-lookup',
                    template: "<div class=\"id-nr-lookup\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div class=\"heading\">\n    Registration\n  </div>\n\n  <div *ngIf=\"!this.loading && hideNoIdNrOption\" class=\"content\">\n    <div *ngIf=\"!html\" class=\"wording\">\n      Please provide a valid {{ idNrLabel }}.\n    </div>\n    <div *ngIf=\"!html\" class=\"wording\">\n      This number is encrypted immediately and is not shared with any other party.\n    </div>\n    <div *ngIf=\"!html\" class=\"wording\">\n      It is only used as a unique identifier for this application. If you register\n      for future projects, it will also be used to link your projects.\n    </div>\n    <div *ngIf=\"!html\" class=\"wording\">\n      Schemes will notify us when you are hospitalised or registered for a disease so\n      that the relevant surveys can be sent to you.\n    </div>\n    <div *ngIf=\"html\" class=\"wording\" [innerHTML]=\"html\">\n    </div>\n\n    <div>\n      <input [(ngModel)]=\"idNr\" type=\"text\" id=\"idNrInput\" placeholder=\"{{ idNrLabel }}\">\n    </div>\n\n    <div *ngIf=\"idNrNotFound && !hideNoIdNrWarning\" class=\"error-text\">\n      No details were found on the system for the specified {{ idNrLabel }}.  Are you sure it is correct?\n    </div>\n    <div *ngIf=\"noIdNr\" class=\"error-text\">\n      Please provide your {{ idNrLabel }}\n    </div>\n    <div *ngIf=\"invalidIdNr && hideNoIdNrWarning\" class=\"error-text\">\n      Please provide a valid {{ idNrLabel }}\n    </div>\n  </div>\n  <div *ngIf=\"hideNoIdNrOption\" class=\"button-section\">\n    <div *ngIf=\"!idNrNotFound\" class=\"button\" (click)=\"continueWithIdNr()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div *ngIf=\"idNrNotFound\" class=\"button button-outline\" (click)=\"completeStep()\">\n      <div class=\"button-text\">{{ idNrLabel }} is correct</div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!this.loading && !hideNoIdNrOption\" class=\"content\">\n    <div *ngIf=\"!html\" class=\"wording\">\n      To continue with the registration process, please enter your {{ idNrLabel }}:\n    </div>\n    <div *ngIf=\"html\" class=\"wording\" [innerHTML]=\"html\">\n    </div>\n    <div>\n      <input class=\"form-control\" [(ngModel)]=\"idNr\" type=\"text\" id=\"idNrInput\">\n    </div>\n\n    <div *ngIf=\"idNrNotFound && !hideNoIdNrWarning\" class=\"error-text\">\n      No details were found on the system for the specified {{ idNrLabel }}.  Are you sure it is correct?\n    </div>\n    <div *ngIf=\"noIdNr\" class=\"error-text\">\n      Please provide your {{ idNrLabel }}\n    </div>\n    <div *ngIf=\"invalidIdNr && hideNoIdNrWarning\" class=\"error-text\">\n      Please provide a valid {{ idNrLabel }}\n    </div>\n    <div class=\"wording top-padding\">\n      Alternatively, if no {{ idNrLabel }} is avalaible, please click on \"No {{ idNrLabel }}\" below to continue.\n    </div>\n  </div>\n  <div *ngIf=\"!hideNoIdNrOption\" class=\"button-section\">\n    <div *ngIf=\"!idNrNotFound\" class=\"button\" (click)=\"continueWithIdNr()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div *ngIf=\"idNrNotFound\" class=\"button\" (click)=\"completeStep()\">\n      <div class=\"button-text\">{{ idNrLabel }} is correct</div>\n    </div>\n    <div class=\"button button-outline\" (click)=\"continueNoIdNr()\">\n      <div class=\"button-text\">No {{ idNrLabel }}</div>\n    </div>\n  </div>\n</div>\n",
                    styles: [".id-nr-lookup{width:100%;height:100%;position:relative}.id-nr-lookup .heading{position:absolute;top:0;width:100%;height:32px;font-size:16px;padding:8px}.id-nr-lookup .top-padding{padding-top:8px}.id-nr-lookup .content{position:absolute;top:32px;left:0;bottom:50px;width:100%;overflow-y:scroll;padding:8px;box-sizing:border-box;background-color:#f9f9f9;font-size:15px}.id-nr-lookup .content .wording{width:100%;font-size:15px;padding-bottom:5px}.id-nr-lookup .content input{width:200;font-size:13px;border:1px solid #58666c;background-color:#fffcf6;box-sizing:border-box;padding:3px 5px;margin-bottom:5px;height:30px;border-radius:5px}.id-nr-lookup .error-text{font-style:italic;font-size:11px;color:#f04141}.id-nr-lookup .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.id-nr-lookup .button-section .button{width:120px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.id-nr-lookup .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.id-nr-lookup .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.id-nr-lookup .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b;cursor:default}.id-nr-lookup .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b;cursor:default}"]
                }] }
    ];
    /** @nocollapse */
    IdNrLookupComponent.ctorParameters = function () { return [
        { type: DomSanitizer },
        { type: ProjectService }
    ]; };
    IdNrLookupComponent.propDecorators = {
        stepInstance: [{ type: Input }],
        userId: [{ type: Input }],
        stepCompletedListener: [{ type: Output }]
    };
    return IdNrLookupComponent;
}());
export { IdNrLookupComponent };
if (false) {
    /** @type {?} */
    IdNrLookupComponent.prototype.stepInstance;
    /** @type {?} */
    IdNrLookupComponent.prototype.userId;
    /** @type {?} */
    IdNrLookupComponent.prototype.stepCompletedListener;
    /** @type {?} */
    IdNrLookupComponent.prototype.idNr;
    /** @type {?} */
    IdNrLookupComponent.prototype.noIdNr;
    /** @type {?} */
    IdNrLookupComponent.prototype.idNrNotFound;
    /** @type {?} */
    IdNrLookupComponent.prototype.invalidIdNr;
    /** @type {?} */
    IdNrLookupComponent.prototype.hideNoIdNrWarning;
    /** @type {?} */
    IdNrLookupComponent.prototype.hideNoIdNrOption;
    /** @type {?} */
    IdNrLookupComponent.prototype.idNrLabel;
    /** @type {?} */
    IdNrLookupComponent.prototype.html;
    /** @type {?} */
    IdNrLookupComponent.prototype.loading;
    /**
     * @type {?}
     * @private
     */
    IdNrLookupComponent.prototype.sanitizer;
    /**
     * @type {?}
     * @private
     */
    IdNrLookupComponent.prototype.projectService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWQtbnItbG9va3VwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvc3RlcHMvaWQtbnItbG9va3VwL2lkLW5yLWxvb2t1cC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBcUIsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzFGLE9BQU8sRUFBRSxZQUFZLEVBQW1CLE1BQU0sMkJBQTJCLENBQUM7QUFFMUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQ2hFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUUxRDtJQW1CRSw2QkFBb0IsU0FBdUIsRUFBVSxjQUE4QjtRQUEvRCxjQUFTLEdBQVQsU0FBUyxDQUFjO1FBQVUsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBWHpFLDBCQUFxQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7SUFZckQsQ0FBQzs7OztJQUVELHNDQUFROzs7SUFBUjtRQUFBLGlCQXNCQztRQXJCQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNwQixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUV6QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxjQUFjLENBQUM7UUFDekgsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsY0FBYyxDQUFDO1FBQ3hILElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQzFDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLGNBQWMsQ0FBQztTQUN4RTtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ25CLElBQUksQ0FBQyxTQUFTLEdBQUcsV0FBVyxDQUFDO1NBQzlCO1FBRUQsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxVQUFDLElBQUk7WUFDckUsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFDckIsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQ2hCLEtBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSSxDQUFDLFNBQVMsQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQzthQUNsRTtRQUNILENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVELHlDQUFXOzs7SUFBWDtJQUNBLENBQUM7Ozs7SUFFRCw4Q0FBZ0I7OztJQUFoQjtRQUFBLGlCQXlCQztRQXhCQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNwQixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN6QixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNkLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQ25CLE9BQU87U0FDUjtRQUVELCtEQUErRDtRQUMvRCwrQkFBK0I7UUFDL0IsY0FBYztRQUNkLElBQUk7UUFFSixJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLHVCQUF1QixDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSTs7OztRQUFDLFVBQUEsTUFBTTtZQUN0RyxJQUFJLE1BQU0sQ0FBQyxRQUFRLEtBQUssTUFBTSxFQUFFO2dCQUM5QixLQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7YUFDckI7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLEtBQUksQ0FBQyxpQkFBaUIsRUFBRTtvQkFDM0IsS0FBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7aUJBQzFCO3FCQUFNO29CQUNMLEtBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztpQkFDckI7YUFDRjtRQUNILENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVELDRDQUFjOzs7SUFBZDtRQUNJLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUN4QixDQUFDOzs7O0lBRUQsMENBQVk7OztJQUFaO1FBQUEsaUJBT0M7UUFOQyxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSTs7OztRQUFDLFVBQUEsTUFBTTtZQUM3RSxLQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDO2dCQUM5QixLQUFLLEVBQUUsS0FBSSxDQUFDLFlBQVk7Z0JBQ3hCLG9CQUFvQixFQUFFLElBQUk7YUFDM0IsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOztnQkF2RkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxjQUFjO29CQUN4Qix3NEdBQTRDOztpQkFFN0M7Ozs7Z0JBVFEsWUFBWTtnQkFFWixjQUFjOzs7K0JBU3BCLEtBQUs7eUJBQ0wsS0FBSzt3Q0FDTCxNQUFNOztJQWdGVCwwQkFBQztDQUFBLEFBeEZELElBd0ZDO1NBbkZZLG1CQUFtQjs7O0lBQzlCLDJDQUFvQzs7SUFDcEMscUNBQXdCOztJQUN4QixvREFBcUQ7O0lBQ3JELG1DQUFhOztJQUNiLHFDQUFnQjs7SUFDaEIsMkNBQXNCOztJQUN0QiwwQ0FBcUI7O0lBQ3JCLGdEQUEyQjs7SUFDM0IsK0NBQTBCOztJQUMxQix3Q0FBa0I7O0lBQ2xCLG1DQUFVOztJQUNWLHNDQUFpQjs7Ozs7SUFFTCx3Q0FBK0I7Ozs7O0lBQUUsNkNBQXNDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPbkluaXQsIE9uRGVzdHJveSwgT3V0cHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBEb21TYW5pdGl6ZXIsIFNhZmVSZXNvdXJjZVVybCB9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXInO1xuXG5pbXBvcnQgeyBQcm9qZWN0U2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL3Byb2plY3Quc2VydmljZSc7XG5pbXBvcnQgeyBTdGVwSW5zdGFuY2UgfSBmcm9tICcuLi8uLi9kb21haW4vc3RlcC1pbnN0YW5jZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2lkLW5yLWxvb2t1cCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9pZC1uci1sb29rdXAuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9pZC1uci1sb29rdXAuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBJZE5yTG9va3VwQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuICBASW5wdXQoKSBzdGVwSW5zdGFuY2U6IFN0ZXBJbnN0YW5jZTtcbiAgQElucHV0KCkgdXNlcklkOiBudW1iZXI7XG4gIEBPdXRwdXQoKSBzdGVwQ29tcGxldGVkTGlzdGVuZXIgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIGlkTnI6IHN0cmluZztcbiAgbm9JZE5yOiBib29sZWFuO1xuICBpZE5yTm90Rm91bmQ6IGJvb2xlYW47XG4gIGludmFsaWRJZE5yOiBib29sZWFuO1xuICBoaWRlTm9JZE5yV2FybmluZzogYm9vbGVhbjtcbiAgaGlkZU5vSWROck9wdGlvbjogYm9vbGVhbjtcbiAgaWROckxhYmVsOiBzdHJpbmc7XG4gIGh0bWw6IGFueTtcbiAgbG9hZGluZzogYm9vbGVhbjtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHNhbml0aXplcjogRG9tU2FuaXRpemVyLCBwcml2YXRlIHByb2plY3RTZXJ2aWNlOiBQcm9qZWN0U2VydmljZSkge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5ub0lkTnIgPSBmYWxzZTtcbiAgICB0aGlzLmlkTnJOb3RGb3VuZCA9IGZhbHNlO1xuICAgIHRoaXMuaW52YWxpZElkTnIgPSBmYWxzZTtcblxuICAgIHRoaXMuaGlkZU5vSWROcldhcm5pbmcgPSB0aGlzLnN0ZXBJbnN0YW5jZS5zdGVwLnBhcmFtZXRlcnNbXCIwXCJdICYmIHRoaXMuc3RlcEluc3RhbmNlLnN0ZXAucGFyYW1ldGVyc1tcIjBcIl0ucGFyYW1ldGVyVmFsdWU7XG4gICAgdGhpcy5oaWRlTm9JZE5yT3B0aW9uID0gdGhpcy5zdGVwSW5zdGFuY2Uuc3RlcC5wYXJhbWV0ZXJzW1wiMVwiXSAmJiB0aGlzLnN0ZXBJbnN0YW5jZS5zdGVwLnBhcmFtZXRlcnNbXCIxXCJdLnBhcmFtZXRlclZhbHVlO1xuICAgIGlmICh0aGlzLnN0ZXBJbnN0YW5jZS5zdGVwLnBhcmFtZXRlcnNbXCIyXCJdKSB7XG4gICAgICB0aGlzLmlkTnJMYWJlbCA9IHRoaXMuc3RlcEluc3RhbmNlLnN0ZXAucGFyYW1ldGVyc1tcIjJcIl0ucGFyYW1ldGVyVmFsdWU7XG4gICAgfVxuXG4gICAgaWYgKCF0aGlzLmlkTnJMYWJlbCkge1xuICAgICAgdGhpcy5pZE5yTGFiZWwgPSBcIklkIG51bWJlclwiO1xuICAgIH1cblxuICAgIHRoaXMubG9hZGluZyA9IHRydWU7XG4gICAgdGhpcy5wcm9qZWN0U2VydmljZS5nZXRNZXNzYWdlU3RlcEh0bWwodGhpcy5zdGVwSW5zdGFuY2UuaWQpLnRoZW4oKGh0bWwpID0+IHtcbiAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgICAgaWYgKGh0bWxbXCJ0ZXh0XCJdKSB7XG4gICAgICAgIHRoaXMuaHRtbCA9IHRoaXMuc2FuaXRpemVyLmJ5cGFzc1NlY3VyaXR5VHJ1c3RIdG1sKGh0bWxbXCJ0ZXh0XCJdKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCkge1xuICB9XG5cbiAgY29udGludWVXaXRoSWROcigpIHtcbiAgICB0aGlzLm5vSWROciA9IGZhbHNlO1xuICAgIHRoaXMuaWROck5vdEZvdW5kID0gZmFsc2U7XG4gICAgdGhpcy5pbnZhbGlkSWROciA9IGZhbHNlO1xuICAgIGlmICghdGhpcy5pZE5yKSB7XG4gICAgICB0aGlzLm5vSWROciA9IHRydWU7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgLy8gaWYgKHRoaXMuaWROci5sZW5ndGggIT09IDEzIHx8ICEoL15cXGQrJC8udGVzdCh0aGlzLmlkTnIpKSkge1xuICAgIC8vICAgICB0aGlzLmludmFsaWRJZE5yID0gdHJ1ZTtcbiAgICAvLyAgICAgcmV0dXJuO1xuICAgIC8vIH1cblxuICAgIHRoaXMucHJvamVjdFNlcnZpY2UudmVyaWZ5QWN0b3JJZE5yKHRoaXMuc3RlcEluc3RhbmNlLmJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlLmlkLCB0aGlzLmlkTnIpLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgIGlmIChyZXN1bHQucmVzcG9uc2UgPT09IFwidHJ1ZVwiKSB7XG4gICAgICAgIHRoaXMuY29tcGxldGVTdGVwKCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpZiAoIXRoaXMuaGlkZU5vSWROcldhcm5pbmcpIHtcbiAgICAgICAgICB0aGlzLmlkTnJOb3RGb3VuZCA9IHRydWU7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGhpcy5jb21wbGV0ZVN0ZXAoKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgY29udGludWVOb0lkTnIoKSB7XG4gICAgICB0aGlzLmNvbXBsZXRlU3RlcCgpO1xuICB9XG5cbiAgY29tcGxldGVTdGVwKCkge1xuICAgIHRoaXMucHJvamVjdFNlcnZpY2UuY29tcGxldGVTdGVwKHRoaXMuc3RlcEluc3RhbmNlLmlkLCB0aGlzLnVzZXJJZCkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgdGhpcy5zdGVwQ29tcGxldGVkTGlzdGVuZXIuZW1pdCh7XG4gICAgICAgIHZhbHVlOiB0aGlzLnN0ZXBJbnN0YW5jZSxcbiAgICAgICAgdXNlQnVzaW5lc3NQcm9jZXNzSWQ6IHRydWVcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG59XG4iXX0=