/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
var CheckboxComponent = /** @class */ (function () {
    function CheckboxComponent() {
        this.selectionListener = new EventEmitter();
        this.selected = false;
    }
    /**
     * @return {?}
     */
    CheckboxComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        // if (this.description) {
        //     if (this.description.toLowerCase() === "yes" || this.description.toLowerCase() === "no") {
        //         this.description = "";
        //     }
        // }
    };
    /**
     * @return {?}
     */
    CheckboxComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    CheckboxComponent.prototype.clicked = /**
     * @return {?}
     */
    function () {
        this.selected = !this.selected;
        this.selectionListener.emit({
            value: this.selected
        });
    };
    CheckboxComponent.decorators = [
        { type: Component, args: [{
                    selector: 'checkbox-component',
                    template: "<div class=\"checkbox-component\" (click)=\"clicked()\">\n  <div class=\"value\">\n    <div class=\"checkbox\">\n      <div *ngIf=\"selected\" class=\"checkbox-selected\">\n      </div>\n    </div>\n  </div>\n  <div class=\"description\">\n    {{ description }}\n  </div>\n</div>\n",
                    styles: [".checkbox-component{display:table;margin:.5em 0;width:100%;cursor:pointer}.checkbox-component .value{display:table-cell;vertical-align:middle;text-align:left;width:32px;padding:3px}.checkbox-component .value .checkbox{border:1px solid #58666c;width:14px;height:14px;padding:2px;box-sizing:border-box}.checkbox-component .value .checkbox .checkbox-selected{background-color:#58666c;width:8px;height:8px;box-sizing:border-box}.checkbox-component .description{display:table-cell;vertical-align:middle;text-align:left;width:100%;font-size:15px;padding-left:5px;padding-top:3px;padding-right:5px;line-height:15px}"]
                }] }
    ];
    /** @nocollapse */
    CheckboxComponent.ctorParameters = function () { return []; };
    CheckboxComponent.propDecorators = {
        description: [{ type: Input }],
        selected: [{ type: Input }],
        selectionListener: [{ type: Output }]
    };
    return CheckboxComponent;
}());
export { CheckboxComponent };
if (false) {
    /** @type {?} */
    CheckboxComponent.prototype.description;
    /** @type {?} */
    CheckboxComponent.prototype.selected;
    /** @type {?} */
    CheckboxComponent.prototype.selectionListener;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hlY2tib3gtY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWN1bWVuLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9zdGVwcy9xdWVzdGlvbm5haXJlL2NoZWNrYm94LWNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFxQixNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFMUY7SUFVRTtRQUZVLHNCQUFpQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFHL0MsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7SUFDeEIsQ0FBQzs7OztJQUVELG9DQUFROzs7SUFBUjtRQUNFLDBCQUEwQjtRQUMxQixpR0FBaUc7UUFDakcsaUNBQWlDO1FBQ2pDLFFBQVE7UUFDUixJQUFJO0lBQ04sQ0FBQzs7OztJQUVELHVDQUFXOzs7SUFBWDtJQUNBLENBQUM7Ozs7SUFFRCxtQ0FBTzs7O0lBQVA7UUFDRSxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUMvQixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDO1lBQ3hCLEtBQUssRUFBRSxJQUFJLENBQUMsUUFBUTtTQUN2QixDQUFDLENBQUM7SUFDTCxDQUFDOztnQkE5QkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxvQkFBb0I7b0JBQzlCLHFTQUF3Qzs7aUJBRXpDOzs7Ozs4QkFFRSxLQUFLOzJCQUNMLEtBQUs7b0NBQ0wsTUFBTTs7SUF1QlQsd0JBQUM7Q0FBQSxBQS9CRCxJQStCQztTQTFCWSxpQkFBaUI7OztJQUM1Qix3Q0FBNkI7O0lBQzdCLHFDQUEyQjs7SUFDM0IsOENBQWlEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPbkluaXQsIE9uRGVzdHJveSwgT3V0cHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2NoZWNrYm94LWNvbXBvbmVudCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9jaGVja2JveC1jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2NoZWNrYm94LWNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgQ2hlY2tib3hDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIGRlc2NyaXB0aW9uOiBzdHJpbmc7XG4gIEBJbnB1dCgpIHNlbGVjdGVkOiBib29sZWFuO1xuICBAT3V0cHV0KCkgc2VsZWN0aW9uTGlzdGVuZXIgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5zZWxlY3RlZCA9IGZhbHNlO1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgLy8gaWYgKHRoaXMuZGVzY3JpcHRpb24pIHtcbiAgICAvLyAgICAgaWYgKHRoaXMuZGVzY3JpcHRpb24udG9Mb3dlckNhc2UoKSA9PT0gXCJ5ZXNcIiB8fCB0aGlzLmRlc2NyaXB0aW9uLnRvTG93ZXJDYXNlKCkgPT09IFwibm9cIikge1xuICAgIC8vICAgICAgICAgdGhpcy5kZXNjcmlwdGlvbiA9IFwiXCI7XG4gICAgLy8gICAgIH1cbiAgICAvLyB9XG4gIH1cblxuICBuZ09uRGVzdHJveSgpIHtcbiAgfVxuXG4gIGNsaWNrZWQoKSB7XG4gICAgdGhpcy5zZWxlY3RlZCA9ICF0aGlzLnNlbGVjdGVkO1xuICAgIHRoaXMuc2VsZWN0aW9uTGlzdGVuZXIuZW1pdCh7XG4gICAgICAgIHZhbHVlOiB0aGlzLnNlbGVjdGVkXG4gICAgfSk7XG4gIH1cbn1cbiJdfQ==