/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { QuestionResponse } from '../../domain/question-response';
import { StepInstance } from '../../domain/step-instance';
var QuestionnaireComponent = /** @class */ (function () {
    function QuestionnaireComponent(projectService) {
        this.projectService = projectService;
        this.stepCompletedListener = new EventEmitter();
        this.cancelListener = new EventEmitter();
        this.onTakePicture = new EventEmitter();
        this.onQuestionAnswered = new EventEmitter();
        this.pageIndex = -1;
        this.loading = false;
    }
    /**
     * @return {?}
     */
    QuestionnaireComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.loading = true;
        this.projectService.getQuestionnaire(this.stepInstance.id).then((/**
         * @param {?} questionnaire
         * @return {?}
         */
        function (questionnaire) {
            _this.questionnaire = questionnaire;
            _this.pageIndex = 0;
            _this.loading = false;
        }));
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    QuestionnaireComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        var _this = this;
        if (changes.stepInstance && changes.stepInstance.previousValue && changes.stepInstance.previousValue.id &&
            changes.stepInstance.currentValue.id !== changes.stepInstance.previousValue.id) {
            this.pageIndex = -1;
            this.loading = true;
            this.projectService.getQuestionnaire(this.stepInstance.id).then((/**
             * @param {?} questionnaire
             * @return {?}
             */
            function (questionnaire) {
                _this.questionnaire = questionnaire;
                _this.pageIndex = 0;
                _this.loading = false;
            }));
        }
    };
    /**
     * @return {?}
     */
    QuestionnaireComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    QuestionnaireComponent.prototype.previous = /**
     * @return {?}
     */
    function () {
        if (this.pageIndex > 0) {
            //            let questionLine = this.questionnaire.questionLines[this.pageIndex];
            try {
                //                this.setAllValidationMessages(questionLine);
                //                this.validateQuestionLine(questionLine);
                this.pageIndex--;
                document.body.scrollTop = 0;
            }
            catch (error) {
                // console.log(error);
            }
        }
    };
    /**
     * @return {?}
     */
    QuestionnaireComponent.prototype.next = /**
     * @return {?}
     */
    function () {
        if (this.pageIndex < this.questionnaire.questionLines.length - 1) {
            /** @type {?} */
            var questionLine = this.questionnaire.questionLines[this.pageIndex];
            try {
                this.setAllValidationMessages(questionLine);
                this.validateQuestionLine(questionLine);
                this.pageIndex++;
                document.body.scrollTop = 0;
            }
            catch (error) {
                // console.log(error);
            }
        }
    };
    /**
     * @param {?} submit
     * @return {?}
     */
    QuestionnaireComponent.prototype.save = /**
     * @param {?} submit
     * @return {?}
     */
    function (submit) {
        var _this = this;
        if (!this.loading) {
            this.loading = true;
            /** @type {?} */
            var responses = new Array();
            for (var i = 0; i < this.questionnaire.questionLines.length; i++) {
                this.addResponses(this.questionnaire.questionLines[i], responses);
            }
            this.projectService.updateQuestionnaire(this.stepInstance.id, responses, submit, this.userId).then((/**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                if (submit) {
                    _this.pageIndex = 0;
                }
                _this.stepCompletedListener.emit({
                    value: _this.stepInstance
                });
                _this.loading = false;
            }));
        }
    };
    /**
     * @return {?}
     */
    QuestionnaireComponent.prototype.cancel = /**
     * @return {?}
     */
    function () {
        this.cancelListener.emit({});
    };
    /**
     * @private
     * @param {?} questionLine
     * @param {?} responses
     * @return {?}
     */
    QuestionnaireComponent.prototype.addResponses = /**
     * @private
     * @param {?} questionLine
     * @param {?} responses
     * @return {?}
     */
    function (questionLine, responses) {
        if (!questionLine)
            return;
        if (questionLine.answerType === 'CHECK_BOX' || questionLine.answerType === 'RADIO_BUTTON') {
            /** @type {?} */
            var questionResponse = new QuestionResponse();
            questionResponse.questionLineId = questionLine.id;
            questionResponse.weight = questionLine.weight;
            questionResponse.selected = questionLine.questionResponse.selected;
            responses.push(questionResponse);
        }
        else if (questionLine.answerType === 'TEXT' || questionLine.answerType === 'TEXT_AREA' ||
            questionLine.answerType === 'NUMBER' || questionLine.answerType === 'IMAGE') {
            /** @type {?} */
            var questionResponse = new QuestionResponse();
            questionResponse.questionLineId = questionLine.id;
            questionResponse.weight = questionLine.weight;
            questionResponse.response = questionLine.questionResponse.response;
            responses.push(questionResponse);
        }
        else if (questionLine.answerType === 'SELECT') {
            /** @type {?} */
            var questionResponse = new QuestionResponse();
            questionResponse.questionLineId = questionLine.id;
            questionResponse.weight = questionLine.questionResponse.weight;
            if (questionLine.questionResponse.weight) {
                questionResponse.selected = true;
                questionResponse.response = questionLine.questionResponse.response;
            }
            responses.push(questionResponse);
        }
        else if ((questionLine.lineType === 'SINGLE_SELECT' || questionLine.lineType === 'MULTI_SELECT') &&
            (questionLine.allowSupportingDescription || questionLine.allowSupportingImage)) {
            /** @type {?} */
            var questionResponse = new QuestionResponse();
            questionResponse.questionLineId = questionLine.id;
            questionResponse.supportingDescription = questionLine.questionResponse.supportingDescription;
            questionResponse.supportingImage = questionLine.questionResponse.supportingImage;
            responses.push(questionResponse);
        }
        for (var i = 0; i < questionLine.children.length; i++) {
            this.addResponses(questionLine.children[i], responses);
        }
    };
    ;
    /**
     * @private
     * @param {?} questionLine
     * @return {?}
     */
    QuestionnaireComponent.prototype.validateQuestionLine = /**
     * @private
     * @param {?} questionLine
     * @return {?}
     */
    function (questionLine) {
        if (!this.hasValidAnswer(questionLine))
            throw "Invalid response";
        if (questionLine.children) {
            for (var i = 0; i < questionLine.children.length; i++) {
                /** @type {?} */
                var child = questionLine.children[i];
                if (child.answerType === "CHECK_BOX" || child.answerType === "RADIO_BUTTON") {
                    /** @type {?} */
                    var childResponse = questionLine.questionResponse;
                    if (childResponse && childResponse.selected) {
                        this.validateQuestionLine(child);
                    }
                }
                else {
                    this.validateQuestionLine(child);
                }
            }
        }
    };
    /**
     * @private
     * @param {?} questionLine
     * @return {?}
     */
    QuestionnaireComponent.prototype.hasValidAnswer = /**
     * @private
     * @param {?} questionLine
     * @return {?}
     */
    function (questionLine) {
        if (!questionLine.lineType || !questionLine.answerType)
            return true;
        if (questionLine.lineType === "MULTI_SELECT" || questionLine.lineType === "SINGLE_SELECT") {
            if (questionLine.required && questionLine.children) {
                for (var i = 0; i < questionLine.children.length; i++) {
                    if (questionLine.children[i].questionResponse && questionLine.children[i].questionResponse.selected)
                        return true;
                }
                return false;
            }
            else {
                return true;
            }
        }
        else if (questionLine.answerType === "NUMBER" || questionLine.answerType === "SELECT" ||
            questionLine.answerType === "TEXT" || questionLine.answerType === "TEXT_AREA" || questionLine.answerType === "IMAGE") {
            if (questionLine.required && (!questionLine.questionResponse || !questionLine.questionResponse.response))
                return false;
            if (questionLine.answerType === "NUMBER") {
                if ((questionLine.required && isNaN(parseInt(questionLine.questionResponse.response))) ||
                    (!questionLine.required && questionLine.questionResponse.response && isNaN(parseInt(questionLine.questionResponse.response)))) {
                    return false;
                }
                try {
                    /** @type {?} */
                    var d = parseFloat(questionLine.questionResponse.response);
                    /** @type {?} */
                    var p = questionLine.parameters.split("|");
                    if (p.length >= 4) {
                        /** @type {?} */
                        var hasLimits = "true" === p[1];
                        /** @type {?} */
                        var max = parseFloat(p[2]);
                        /** @type {?} */
                        var min = parseFloat(p[3]);
                        if (hasLimits && (d < min || d > max) && !questionLine.errorAccepted)
                            return false;
                    }
                }
                catch (error) {
                    return false;
                }
            }
        }
        return true;
    };
    /**
     * @private
     * @param {?} questionLine
     * @return {?}
     */
    QuestionnaireComponent.prototype.setAllValidationMessages = /**
     * @private
     * @param {?} questionLine
     * @return {?}
     */
    function (questionLine) {
        this.setValidationMessage(questionLine);
        if (questionLine.children) {
            for (var i = 0; i < questionLine.children.length; i++) {
                /** @type {?} */
                var child = questionLine.children[i];
                if (child.answerType === "CHECK_BOX" || child.answerType === "RADIO_BUTTON") {
                    /** @type {?} */
                    var childResponse = child.questionResponse;
                    if (childResponse && childResponse.selected) {
                        this.setAllValidationMessages(child);
                    }
                }
                else {
                    this.setAllValidationMessages(child);
                }
            }
        }
    };
    /**
     * @private
     * @param {?} questionLine
     * @return {?}
     */
    QuestionnaireComponent.prototype.setValidationMessage = /**
     * @private
     * @param {?} questionLine
     * @return {?}
     */
    function (questionLine) {
        if (!questionLine.lineType || !questionLine.answerType) {
            questionLine.hasError = false;
            questionLine.errorMessage = null;
            questionLine.showErrorConfirmation = false;
            return;
        }
        if (questionLine.lineType === "MULTI_SELECT" || questionLine.lineType === "SINGLE_SELECT") {
            if (questionLine.required && questionLine.children) {
                /** @type {?} */
                var selected = false;
                for (var i = 0; i < questionLine.children.length; i++) {
                    if (questionLine.children[i].questionResponse && questionLine.children[i].questionResponse.selected) {
                        selected = true;
                        break;
                    }
                }
                if (!selected) {
                    questionLine.hasError = true;
                    questionLine.showErrorConfirmation = false;
                    questionLine.errorMessage = "An answer must be selected";
                    return;
                }
            }
        }
        else if (questionLine.answerType === "NUMBER" || questionLine.answerType === "SELECT" ||
            questionLine.answerType === "TEXT" || questionLine.answerType === "TEXT_AREA" ||
            questionLine.answerType === "IMAGE") {
            /** @type {?} */
            var questionResponse = questionLine.questionResponse;
            if (questionLine.required && (!questionResponse || !questionResponse.response)) {
                questionLine.hasError = true;
                questionLine.showErrorConfirmation = false;
                questionLine.errorMessage = "An answer must be provided";
                return;
            }
            if (questionLine.answerType === "NUMBER") {
                if ((questionLine.required && isNaN(parseInt(questionResponse.response))) ||
                    (!questionLine.required && questionResponse.response && isNaN(parseInt(questionResponse.response)))) {
                    questionLine.hasError = true;
                    questionLine.showErrorConfirmation = false;
                    questionLine.errorMessage = "An invalid number was entered";
                    return;
                }
                try {
                    /** @type {?} */
                    var d = parseFloat(questionResponse.response);
                    /** @type {?} */
                    var p = questionLine.parameters.split("|");
                    if (p.length >= 4) {
                        /** @type {?} */
                        var hasLimits = "true" === p[1];
                        /** @type {?} */
                        var max = parseFloat(p[2]);
                        /** @type {?} */
                        var min = parseFloat(p[3]);
                        if (hasLimits) {
                            if (d < min) {
                                questionLine.hasError = true;
                                questionLine.showErrorConfirmation = true;
                                questionLine.errorMessage = "The value provided is less than the minimum value of " + min + ".  Are you sure this is correct?";
                                return;
                            }
                            else if (d > max) {
                                questionLine.hasError = true;
                                questionLine.showErrorConfirmation = true;
                                questionLine.errorMessage = "The value provided exceeds the maximum value of " + max + ".  Are you sure this is correct?";
                                return;
                            }
                        }
                    }
                }
                catch (error) {
                    questionLine.hasError = true;
                    questionLine.showErrorConfirmation = false;
                    questionLine.errorMessage = "An invalid number was supplied";
                    return;
                }
            }
        }
        questionLine.hasError = false;
        questionLine.showErrorConfirmation = false;
        questionLine.errorMessage = "";
    };
    /**
     * @param {?} questionLine
     * @return {?}
     */
    QuestionnaireComponent.prototype.takePictureQuestionnaire = /**
     * @param {?} questionLine
     * @return {?}
     */
    function (questionLine) {
        /** @type {?} */
        var imageContainer = {
            setImage: /**
             * @param {?} image
             * @return {?}
             */
            function (image) {
                questionLine.questionResponse.response = "data:image/jpeg;base64," + image;
            }
        };
        this.onTakePicture.emit(imageContainer);
    };
    /**
     * @param {?} questionLine
     * @return {?}
     */
    QuestionnaireComponent.prototype.takeSupportingPictureQuestionnaire = /**
     * @param {?} questionLine
     * @return {?}
     */
    function (questionLine) {
        /** @type {?} */
        var imageContainer = {
            setImage: /**
             * @param {?} image
             * @return {?}
             */
            function (image) {
                questionLine.questionResponse.supportingImage = "data:image/jpeg;base64," + image;
            }
        };
        this.onTakePicture.emit(imageContainer);
    };
    /**
     * @param {?} questionLine
     * @return {?}
     */
    QuestionnaireComponent.prototype.questionAnswered = /**
     * @param {?} questionLine
     * @return {?}
     */
    function (questionLine) {
        console.log("questionnaire.questionAnswered: " + questionLine.wording + " " + questionLine.integrationIndicator + " " + questionLine.id);
        this.onQuestionAnswered.emit(questionLine);
    };
    QuestionnaireComponent.decorators = [
        { type: Component, args: [{
                    selector: 'questionnaire',
                    template: "<div class=\"questionnaire\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div *ngIf=\"questionnaire\" class=\"heading\">\n    {{ questionnaire.description }}\n    <span class=\"sub-heading\">\n      (Page {{ (pageIndex + 1) }} of {{ this.questionnaire.questionLines.length }})\n    </span>\n  </div>\n  <!-- <loading-indicator [show]=\"loading\"></loading-indicator> -->\n  <!-- <div *ngIf=\"questionnaire\">\n    <div class=\"row\">\n      <div class=\"col-md-12\">\n        <h2 *ngIf=\"wizard\">{{questionnaire.description}}</h2>\n        <h3 *ngIf=\"!wizard\">{{questionnaire.description}}</h3>\n      </div>\n    </div> -->\n\n  <div *ngIf=\"questionnaire && questionnaire.questionLines\" class=\"questionnaire-content\">\n    <question-line [questionLine]=\"questionnaire.questionLines[pageIndex]\" (onTakePictureQuestionLine)=\"takePictureQuestionnaire($event)\"\n        (onQuestionAnswered)=\"questionAnswered($event)\" (onTakeSupportingPictureQuestionLine)=\"takeSupportingPictureQuestionnaire($event)\"></question-line>\n  </div>\n\n  <div class=\"button-section\">\n    <div *ngIf=\"questionnaire && questionnaire.questionLines && pageIndex >= questionnaire.questionLines.length - 1\"\n        class=\"button\" [ngClass]=\"{ 'button-disabled': loading }\" (click)=\"save(true)\">\n      <div class=\"button-text\">Submit</div>\n    </div>\n    <div *ngIf=\"questionnaire && questionnaire.questionLines && pageIndex < questionnaire.questionLines.length - 1\"\n        class=\"button button-outline\" [ngClass]=\"{ 'button-outline-disabled': loading }\" (click)=\"save(false)\">\n      <div class=\"button-text\">Save</div>\n    </div>\n\n    <div *ngIf=\"questionnaire && questionnaire.questionLines && questionnaire.questionLines.length > 1\"\n        class=\"button image-button\" [ngClass]=\"{ 'button-disabled': pageIndex >= questionnaire.questionLines.length - 1 || loading }\" (click)=\"next()\">\n      <div class=\"button-text\">\n          <img src=\"data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAUVJREFUeJzt2EtOw0AURNELbCw7o9hZdgaTtBigBLv9+vM+JfUscnyPZw220+OknIDvx9HSN1kw8RufDkH8jU+DIJ7Hh0cQ/8eHRRDH48MhiPPxYRBEf7x7BHE93i2CsIt3hyDs47dBeF/8/58sRvg48Js78AbcBr3D7fH8+6Dnv9wRAAiMcBQAgiKcAYCACGcBIBhCDwAEQugFgCAIVwAgAMJVAHCOYAEAjhGsAMApgiUAOESwBgBnCCMAwBHCKABwgjASABwgjAaAzRFmAMDGCLMAYFOE1XeCqSbG3S5vccP8aqLiK77ijY9mhfRMVHzFV7zx0ayQnomKr/iKNz6aFdIzUfEVX/      HGR7NCeiYqPmb86jvBLzb/+m0i2JfvmUgc3yYSx7eJxPFtInF8m0gc3yYSx7eJxPFtInF8m0gc3yacxf8AAP323bWbSe8AAAAASUVORK5CYII=\">\n      </div>\n    </div>\n    <div *ngIf=\"questionnaire && questionnaire.questionLines && questionnaire.questionLines.length > 1\"\n        class=\"button image-button\" (click)=\"previous()\">\n      <div class=\"button-text\">\n          <img src=\"data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAUlJREFUeJzt2k1uwjAUReFTujF2xu3O2Fk7icUAtU2cZ8fv50qeoSTnQ0wiYO1pOykn4Hs7uvRJLph4xadDEO/xaRDE7/HhEcT/8WERxP74cAjieHwYBNEf7x5BnI93iyDs4t0hCPv4boTbiZCeCXhMvucyE+O++eV/AqLiK77ijY9mhfRMVHzFV7zx0ayQnomKr/iKNz6aFdIzUfEVX/HGR7NCeiYqPmb87HeCy+1zx2eewAdwH/QM9+36z0HX/3N7ACAwwl4ACIpwBAACIhwFgGAIPQAQCKEXAIIgnAGAAAhnAcA5ggUAOEawAgCnCJYA4BDBGgCcIYwAAEcIowDACcJIAHCAMBoAFkeYAQALI8wCgEURrvif4Nfkey45Eeztcs9E4vg2kTi+TSSObxOJ49tE4vg2kTi+TSSObxOJ49tE4vg2MTj+B2+f9t1j5qkYAAAAAElFTkSuQmCC\">\n      </div>\n    </div>\n\n    <div class=\"button button-outline\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                    styles: [".questionnaire{width:100%;height:100%;max-height:100%;position:relative}.questionnaire .heading{position:absolute;top:0;width:100%;height:32px;font-size:16px;padding:8px}.questionnaire .heading .sub-heading{font-size:12px;font-style:italic}.questionnaire .questionnaire-content{position:absolute;top:32px;left:0;bottom:50px;width:100%;overflow-y:scroll;padding:8px;box-sizing:border-box;background-color:#f9f9f9}.questionnaire .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.questionnaire .button-section .button{width:70px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.questionnaire .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.questionnaire .button-section .button .button-text img{padding-left:4px;padding-top:4px;width:16px;height:16px}.questionnaire .button-section .image-button{width:36px;border:1px solid #2b4054;background-color:#fff;color:#2b4054}.questionnaire .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.questionnaire .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b}.questionnaire .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b}"]
                }] }
    ];
    /** @nocollapse */
    QuestionnaireComponent.ctorParameters = function () { return [
        { type: ProjectService }
    ]; };
    QuestionnaireComponent.propDecorators = {
        stepInstance: [{ type: Input }],
        userId: [{ type: Input }],
        wizard: [{ type: Input }],
        newPagePerSubquestion: [{ type: Input }],
        stepCompletedListener: [{ type: Output }],
        cancelListener: [{ type: Output }],
        onTakePicture: [{ type: Output }],
        onQuestionAnswered: [{ type: Output }]
    };
    return QuestionnaireComponent;
}());
export { QuestionnaireComponent };
if (false) {
    /** @type {?} */
    QuestionnaireComponent.prototype.stepInstance;
    /** @type {?} */
    QuestionnaireComponent.prototype.userId;
    /** @type {?} */
    QuestionnaireComponent.prototype.wizard;
    /** @type {?} */
    QuestionnaireComponent.prototype.newPagePerSubquestion;
    /** @type {?} */
    QuestionnaireComponent.prototype.questionnaire;
    /** @type {?} */
    QuestionnaireComponent.prototype.pageIndex;
    /** @type {?} */
    QuestionnaireComponent.prototype.stepCompletedListener;
    /** @type {?} */
    QuestionnaireComponent.prototype.cancelListener;
    /** @type {?} */
    QuestionnaireComponent.prototype.onTakePicture;
    /** @type {?} */
    QuestionnaireComponent.prototype.onQuestionAnswered;
    /** @type {?} */
    QuestionnaireComponent.prototype.error;
    /** @type {?} */
    QuestionnaireComponent.prototype.loading;
    /**
     * @type {?}
     * @private
     */
    QuestionnaireComponent.prototype.projectService;
    /* Skipping unhandled member: ;*/
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicXVlc3Rpb25uYWlyZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL3N0ZXBzL3F1ZXN0aW9ubmFpcmUvcXVlc3Rpb25uYWlyZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBcUIsTUFBTSxFQUFhLE1BQU0sZUFBZSxDQUFDO0FBRXJHLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUdoRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNsRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFHMUQ7SUFvQkUsZ0NBQW9CLGNBQThCO1FBQTlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQVB4QywwQkFBcUIsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQzNDLG1CQUFjLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNwQyxrQkFBYSxHQUFHLElBQUksWUFBWSxFQUFrQixDQUFDO1FBQ25ELHVCQUFrQixHQUFHLElBQUksWUFBWSxFQUFnQixDQUFDO1FBSzlELElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDcEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7SUFDdkIsQ0FBQzs7OztJQUVELHlDQUFROzs7SUFBUjtRQUFBLGlCQU9DO1FBTkMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxVQUFDLGFBQWE7WUFDNUUsS0FBSSxDQUFDLGFBQWEsR0FBRyxhQUFhLENBQUM7WUFDbkMsS0FBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7WUFDbkIsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDdkIsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUVELDRDQUFXOzs7O0lBQVgsVUFBWSxPQUFPO1FBQW5CLGlCQVdDO1FBVkMsSUFBSSxPQUFPLENBQUMsWUFBWSxJQUFJLE9BQU8sQ0FBQyxZQUFZLENBQUMsYUFBYSxJQUFJLE9BQU8sQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLEVBQUU7WUFDakcsT0FBTyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsRUFBRSxLQUFLLE9BQU8sQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLEVBQUUsRUFBRTtZQUNwRixJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1lBQ3BCLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJOzs7O1lBQUMsVUFBQyxhQUFhO2dCQUM1RSxLQUFJLENBQUMsYUFBYSxHQUFHLGFBQWEsQ0FBQztnQkFDbkMsS0FBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7Z0JBQ25CLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQ3ZCLENBQUMsRUFBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDOzs7O0lBRUQsNENBQVc7OztJQUFYO0lBQ0EsQ0FBQzs7OztJQUVELHlDQUFROzs7SUFBUjtRQUNFLElBQUksSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLEVBQUU7WUFDNUIsa0ZBQWtGO1lBQzVFLElBQUk7Z0JBQ1YsOERBQThEO2dCQUM5RCwwREFBMEQ7Z0JBQ2xELElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztnQkFDakIsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDO2FBQzdCO1lBQUMsT0FBTyxLQUFLLEVBQUU7Z0JBQ2Qsc0JBQXNCO2FBQ3ZCO1NBQ0Y7SUFDSCxDQUFDOzs7O0lBRUQscUNBQUk7OztJQUFKO1FBQ0UsSUFBSSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7O2dCQUM1RCxZQUFZLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUNuRSxJQUFJO2dCQUNGLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDNUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUN4QyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7Z0JBQ2pCLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQzthQUM3QjtZQUFDLE9BQU8sS0FBSyxFQUFFO2dCQUNkLHNCQUFzQjthQUN2QjtTQUNGO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxxQ0FBSTs7OztJQUFKLFVBQUssTUFBZTtRQUFwQixpQkFvQkM7UUFuQkMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDakIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7O2dCQUNoQixTQUFTLEdBQUcsSUFBSSxLQUFLLEVBQW9CO1lBQzdDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ2hFLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLEVBQUUsU0FBUyxDQUFDLENBQUM7YUFDbkU7WUFFRCxJQUFJLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUk7Ozs7WUFBQyxVQUFBLE1BQU07Z0JBQ3ZHLElBQUksTUFBTSxFQUFFO29CQUNWLEtBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDO2lCQUNwQjtnQkFFRCxLQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDO29CQUM5QixLQUFLLEVBQUUsS0FBSSxDQUFDLFlBQVk7aUJBQ3pCLENBQUMsQ0FBQztnQkFFSCxLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztZQUN2QixDQUFDLEVBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7OztJQUVELHVDQUFNOzs7SUFBTjtRQUNFLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQ3hCLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7Ozs7SUFFTyw2Q0FBWTs7Ozs7O0lBQXBCLFVBQXFCLFlBQTBCLEVBQUUsU0FBNkI7UUFDNUUsSUFBSSxDQUFDLFlBQVk7WUFDZixPQUFPO1FBRVQsSUFBSSxZQUFZLENBQUMsVUFBVSxLQUFLLFdBQVcsSUFBSSxZQUFZLENBQUMsVUFBVSxLQUFLLGNBQWMsRUFBRTs7Z0JBQ3JGLGdCQUFnQixHQUFHLElBQUksZ0JBQWdCLEVBQUU7WUFDN0MsZ0JBQWdCLENBQUMsY0FBYyxHQUFHLFlBQVksQ0FBQyxFQUFFLENBQUM7WUFDbEQsZ0JBQWdCLENBQUMsTUFBTSxHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUM7WUFDOUMsZ0JBQWdCLENBQUMsUUFBUSxHQUFHLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUM7WUFDbkUsU0FBUyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1NBQ2xDO2FBQU0sSUFBSSxZQUFZLENBQUMsVUFBVSxLQUFLLE1BQU0sSUFBSSxZQUFZLENBQUMsVUFBVSxLQUFLLFdBQVc7WUFDcEYsWUFBWSxDQUFDLFVBQVUsS0FBSyxRQUFRLElBQUksWUFBWSxDQUFDLFVBQVUsS0FBSyxPQUFPLEVBQUU7O2dCQUMzRSxnQkFBZ0IsR0FBRyxJQUFJLGdCQUFnQixFQUFFO1lBQzdDLGdCQUFnQixDQUFDLGNBQWMsR0FBRyxZQUFZLENBQUMsRUFBRSxDQUFDO1lBQ2xELGdCQUFnQixDQUFDLE1BQU0sR0FBRyxZQUFZLENBQUMsTUFBTSxDQUFDO1lBQzlDLGdCQUFnQixDQUFDLFFBQVEsR0FBRyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDO1lBQ25FLFNBQVMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztTQUNsQzthQUFNLElBQUksWUFBWSxDQUFDLFVBQVUsS0FBSyxRQUFRLEVBQUU7O2dCQUMzQyxnQkFBZ0IsR0FBRyxJQUFJLGdCQUFnQixFQUFFO1lBQzdDLGdCQUFnQixDQUFDLGNBQWMsR0FBRyxZQUFZLENBQUMsRUFBRSxDQUFDO1lBQ2xELGdCQUFnQixDQUFDLE1BQU0sR0FBRyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDO1lBQy9ELElBQUksWUFBWSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRTtnQkFDeEMsZ0JBQWdCLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztnQkFDakMsZ0JBQWdCLENBQUMsUUFBUSxHQUFHLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUM7YUFDcEU7WUFFRCxTQUFTLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7U0FDbEM7YUFBTSxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsS0FBSyxlQUFlLElBQUksWUFBWSxDQUFDLFFBQVEsS0FBSyxjQUFjLENBQUM7WUFDOUYsQ0FBQyxZQUFZLENBQUMsMEJBQTBCLElBQUksWUFBWSxDQUFDLG9CQUFvQixDQUFDLEVBQUU7O2dCQUM5RSxnQkFBZ0IsR0FBRyxJQUFJLGdCQUFnQixFQUFFO1lBQzdDLGdCQUFnQixDQUFDLGNBQWMsR0FBRyxZQUFZLENBQUMsRUFBRSxDQUFDO1lBQ2xELGdCQUFnQixDQUFDLHFCQUFxQixHQUFHLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FBQztZQUM3RixnQkFBZ0IsQ0FBQyxlQUFlLEdBQUcsWUFBWSxDQUFDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQztZQUNqRixTQUFTLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7U0FDbEM7UUFFRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsWUFBWSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDckQsSUFBSSxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxDQUFDO1NBQ3hEO0lBQ0gsQ0FBQztJQUFBLENBQUM7Ozs7OztJQUVNLHFEQUFvQjs7Ozs7SUFBNUIsVUFBNkIsWUFBMEI7UUFDckQsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDO1lBQ3BDLE1BQU0sa0JBQWtCLENBQUM7UUFFM0IsSUFBSSxZQUFZLENBQUMsUUFBUSxFQUFFO1lBQ3pCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxZQUFZLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTs7b0JBQ2pELEtBQUssR0FBRyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDcEMsSUFBSSxLQUFLLENBQUMsVUFBVSxLQUFLLFdBQVcsSUFBSSxLQUFLLENBQUMsVUFBVSxLQUFLLGNBQWMsRUFBRTs7d0JBQ3ZFLGFBQWEsR0FBRyxZQUFZLENBQUMsZ0JBQWdCO29CQUNqRCxJQUFJLGFBQWEsSUFBSSxhQUFhLENBQUMsUUFBUSxFQUFFO3dCQUMzQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLENBQUM7cUJBQ2xDO2lCQUNGO3FCQUFNO29CQUNMLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDbEM7YUFDRjtTQUNGO0lBQ0gsQ0FBQzs7Ozs7O0lBRU8sK0NBQWM7Ozs7O0lBQXRCLFVBQXVCLFlBQTBCO1FBQy9DLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVU7WUFDcEQsT0FBTyxJQUFJLENBQUM7UUFFZCxJQUFJLFlBQVksQ0FBQyxRQUFRLEtBQUssY0FBYyxJQUFJLFlBQVksQ0FBQyxRQUFRLEtBQUssZUFBZSxFQUFFO1lBQ3pGLElBQUksWUFBWSxDQUFDLFFBQVEsSUFBSSxZQUFZLENBQUMsUUFBUSxFQUFFO2dCQUNsRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsWUFBWSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ3JELElBQUksWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsSUFBSSxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLFFBQVE7d0JBQ2pHLE9BQU8sSUFBSSxDQUFDO2lCQUNmO2dCQUVELE9BQU8sS0FBSyxDQUFDO2FBQ2Q7aUJBQU07Z0JBQ0wsT0FBTyxJQUFJLENBQUM7YUFDYjtTQUNGO2FBQU0sSUFBSSxZQUFZLENBQUMsVUFBVSxLQUFLLFFBQVEsSUFBSSxZQUFZLENBQUMsVUFBVSxLQUFLLFFBQVE7WUFDL0UsWUFBWSxDQUFDLFVBQVUsS0FBSyxNQUFNLElBQUksWUFBWSxDQUFDLFVBQVUsS0FBSyxXQUFXLElBQUksWUFBWSxDQUFDLFVBQVUsS0FBSyxPQUFPLEVBQUU7WUFDNUgsSUFBSSxZQUFZLENBQUMsUUFBUSxJQUFJLENBQUMsQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLElBQUksQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDO2dCQUN0RyxPQUFPLEtBQUssQ0FBQztZQUVmLElBQUksWUFBWSxDQUFDLFVBQVUsS0FBSyxRQUFRLEVBQUU7Z0JBQ3hDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQzlFLENBQUMsQ0FBQyxZQUFZLENBQUMsUUFBUSxJQUFJLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLElBQUksS0FBSyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFO29CQUNySSxPQUFPLEtBQUssQ0FBQztpQkFDZDtnQkFFRCxJQUFJOzt3QkFDRSxDQUFDLEdBQUcsVUFBVSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUM7O3dCQUN0RCxDQUFDLEdBQUcsWUFBWSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO29CQUMxQyxJQUFJLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFOzs0QkFDYixTQUFTLEdBQUcsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7OzRCQUMzQixHQUFHLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzs7NEJBQ3RCLEdBQUcsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUMxQixJQUFJLFNBQVMsSUFBSSxDQUFDLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLGFBQWE7NEJBQ2xFLE9BQU8sS0FBSyxDQUFDO3FCQUNoQjtpQkFDRjtnQkFBQyxPQUFPLEtBQUssRUFBRTtvQkFDZCxPQUFPLEtBQUssQ0FBQztpQkFDZDthQUNGO1NBQ0Y7UUFFRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7Ozs7OztJQUVPLHlEQUF3Qjs7Ozs7SUFBaEMsVUFBaUMsWUFBMEI7UUFDekQsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3hDLElBQUksWUFBWSxDQUFDLFFBQVEsRUFBRTtZQUN6QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsWUFBWSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7O29CQUNqRCxLQUFLLEdBQUcsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BDLElBQUksS0FBSyxDQUFDLFVBQVUsS0FBSyxXQUFXLElBQUksS0FBSyxDQUFDLFVBQVUsS0FBSyxjQUFjLEVBQUU7O3dCQUN2RSxhQUFhLEdBQUcsS0FBSyxDQUFDLGdCQUFnQjtvQkFDMUMsSUFBSSxhQUFhLElBQUksYUFBYSxDQUFDLFFBQVEsRUFBRTt3QkFDM0MsSUFBSSxDQUFDLHdCQUF3QixDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUN0QztpQkFDRjtxQkFBTTtvQkFDTCxJQUFJLENBQUMsd0JBQXdCLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3RDO2FBQ0Y7U0FDRjtJQUNILENBQUM7Ozs7OztJQUVPLHFEQUFvQjs7Ozs7SUFBNUIsVUFBNkIsWUFBMEI7UUFDckQsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxFQUFFO1lBQ3RELFlBQVksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1lBQzlCLFlBQVksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1lBQ2pDLFlBQVksQ0FBQyxxQkFBcUIsR0FBRyxLQUFLLENBQUM7WUFDM0MsT0FBTztTQUNSO1FBRUQsSUFBSSxZQUFZLENBQUMsUUFBUSxLQUFLLGNBQWMsSUFBSSxZQUFZLENBQUMsUUFBUSxLQUFLLGVBQWUsRUFBRTtZQUN6RixJQUFJLFlBQVksQ0FBQyxRQUFRLElBQUksWUFBWSxDQUFDLFFBQVEsRUFBRTs7b0JBQzlDLFFBQVEsR0FBRyxLQUFLO2dCQUNwQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsWUFBWSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ3JELElBQUksWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsSUFBSSxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRTt3QkFDbkcsUUFBUSxHQUFHLElBQUksQ0FBQzt3QkFDaEIsTUFBTTtxQkFDUDtpQkFDRjtnQkFFRCxJQUFJLENBQUMsUUFBUSxFQUFFO29CQUNiLFlBQVksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO29CQUM3QixZQUFZLENBQUMscUJBQXFCLEdBQUcsS0FBSyxDQUFDO29CQUMzQyxZQUFZLENBQUMsWUFBWSxHQUFHLDRCQUE0QixDQUFDO29CQUN6RCxPQUFPO2lCQUNSO2FBQ0Y7U0FDRjthQUFNLElBQUksWUFBWSxDQUFDLFVBQVUsS0FBSyxRQUFRLElBQUksWUFBWSxDQUFDLFVBQVUsS0FBSyxRQUFRO1lBQy9FLFlBQVksQ0FBQyxVQUFVLEtBQUssTUFBTSxJQUFJLFlBQVksQ0FBQyxVQUFVLEtBQUssV0FBVztZQUM3RSxZQUFZLENBQUMsVUFBVSxLQUFLLE9BQU8sRUFBRTs7Z0JBQ3ZDLGdCQUFnQixHQUFHLFlBQVksQ0FBQyxnQkFBZ0I7WUFDcEQsSUFBSSxZQUFZLENBQUMsUUFBUSxJQUFJLENBQUMsQ0FBQyxnQkFBZ0IsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUM5RSxZQUFZLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztnQkFDN0IsWUFBWSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztnQkFDM0MsWUFBWSxDQUFDLFlBQVksR0FBRyw0QkFBNEIsQ0FBQztnQkFDekQsT0FBTzthQUNSO1lBRUQsSUFBSSxZQUFZLENBQUMsVUFBVSxLQUFLLFFBQVEsRUFBRTtnQkFDeEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLElBQUksS0FBSyxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO29CQUNqRSxDQUFDLENBQUMsWUFBWSxDQUFDLFFBQVEsSUFBSSxnQkFBZ0IsQ0FBQyxRQUFRLElBQUksS0FBSyxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0JBQzNHLFlBQVksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO29CQUM3QixZQUFZLENBQUMscUJBQXFCLEdBQUcsS0FBSyxDQUFDO29CQUMzQyxZQUFZLENBQUMsWUFBWSxHQUFHLCtCQUErQixDQUFDO29CQUM1RCxPQUFPO2lCQUNSO2dCQUVELElBQUk7O3dCQUNFLENBQUMsR0FBRyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDOzt3QkFDekMsQ0FBQyxHQUFHLFlBQVksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztvQkFDMUMsSUFBSSxDQUFDLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTs7NEJBQ2IsU0FBUyxHQUFHLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDOzs0QkFDM0IsR0FBRyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7OzRCQUN0QixHQUFHLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDMUIsSUFBSSxTQUFTLEVBQUU7NEJBQ2IsSUFBSSxDQUFDLEdBQUcsR0FBRyxFQUFFO2dDQUNYLFlBQVksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO2dDQUM3QixZQUFZLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDO2dDQUMxQyxZQUFZLENBQUMsWUFBWSxHQUFHLHVEQUF1RCxHQUFHLEdBQUcsR0FBRyxrQ0FBa0MsQ0FBQztnQ0FDL0gsT0FBTzs2QkFDUjtpQ0FBTSxJQUFJLENBQUMsR0FBRyxHQUFHLEVBQUU7Z0NBQ2xCLFlBQVksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO2dDQUM3QixZQUFZLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDO2dDQUMxQyxZQUFZLENBQUMsWUFBWSxHQUFHLGtEQUFrRCxHQUFHLEdBQUcsR0FBRyxrQ0FBa0MsQ0FBQztnQ0FDMUgsT0FBTzs2QkFDUjt5QkFDRjtxQkFDRjtpQkFDRjtnQkFBQyxPQUFPLEtBQUssRUFBRTtvQkFDZCxZQUFZLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztvQkFDN0IsWUFBWSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztvQkFDM0MsWUFBWSxDQUFDLFlBQVksR0FBRyxnQ0FBZ0MsQ0FBQztvQkFDN0QsT0FBTztpQkFDUjthQUNGO1NBQ0Y7UUFFRCxZQUFZLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUM5QixZQUFZLENBQUMscUJBQXFCLEdBQUcsS0FBSyxDQUFDO1FBQzNDLFlBQVksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO0lBQ2pDLENBQUM7Ozs7O0lBRUQseURBQXdCOzs7O0lBQXhCLFVBQXlCLFlBQTBCOztZQUM3QyxjQUFjLEdBQW1CO1lBQ25DLFFBQVE7Ozs7c0JBQUMsS0FBYTtnQkFDcEIsWUFBWSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsR0FBRyx5QkFBeUIsR0FBRyxLQUFLLENBQUM7WUFDN0UsQ0FBQztTQUNGO1FBQ0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDMUMsQ0FBQzs7Ozs7SUFFRCxtRUFBa0M7Ozs7SUFBbEMsVUFBbUMsWUFBMEI7O1lBQ3ZELGNBQWMsR0FBbUI7WUFDbkMsUUFBUTs7OztzQkFBQyxLQUFhO2dCQUNwQixZQUFZLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxHQUFHLHlCQUF5QixHQUFHLEtBQUssQ0FBQztZQUNwRixDQUFDO1NBQ0Y7UUFDRCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUMxQyxDQUFDOzs7OztJQUVELGlEQUFnQjs7OztJQUFoQixVQUFpQixZQUEwQjtRQUN6QyxPQUFPLENBQUMsR0FBRyxDQUFDLGtDQUFrQyxHQUFHLFlBQVksQ0FBQyxPQUFPLEdBQUcsR0FBRyxHQUFHLFlBQVksQ0FBQyxvQkFBb0IsR0FBRyxHQUFHLEdBQUcsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3pJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDN0MsQ0FBQzs7Z0JBeFVGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsZUFBZTtvQkFDekIseWhIQUE2Qzs7aUJBRzlDOzs7O2dCQVpRLGNBQWM7OzsrQkFjcEIsS0FBSzt5QkFDTCxLQUFLO3lCQUNMLEtBQUs7d0NBQ0wsS0FBSzt3Q0FHTCxNQUFNO2lDQUNOLE1BQU07Z0NBQ04sTUFBTTtxQ0FDTixNQUFNOztJQXlUVCw2QkFBQztDQUFBLEFBelVELElBeVVDO1NBblVZLHNCQUFzQjs7O0lBQ2pDLDhDQUFvQzs7SUFDcEMsd0NBQXdCOztJQUN4Qix3Q0FBeUI7O0lBQ3pCLHVEQUF3Qzs7SUFDeEMsK0NBQTZCOztJQUM3QiwyQ0FBa0I7O0lBQ2xCLHVEQUFxRDs7SUFDckQsZ0RBQThDOztJQUM5QywrQ0FBNkQ7O0lBQzdELG9EQUFnRTs7SUFDaEUsdUNBQVc7O0lBQ1gseUNBQWlCOzs7OztJQUVMLGdEQUFzQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPbkRlc3Ryb3ksIE91dHB1dCwgT25DaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IFByb2plY3RTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvcHJvamVjdC5zZXJ2aWNlJztcbmltcG9ydCB7IFF1ZXN0aW9ubmFpcmUgfSBmcm9tICcuLi8uLi9kb21haW4vcXVlc3Rpb25uYWlyZSc7XG5pbXBvcnQgeyBRdWVzdGlvbkxpbmUgfSBmcm9tICcuLi8uLi9kb21haW4vcXVlc3Rpb24tbGluZSc7XG5pbXBvcnQgeyBRdWVzdGlvblJlc3BvbnNlIH0gZnJvbSAnLi4vLi4vZG9tYWluL3F1ZXN0aW9uLXJlc3BvbnNlJztcbmltcG9ydCB7IFN0ZXBJbnN0YW5jZSB9IGZyb20gJy4uLy4uL2RvbWFpbi9zdGVwLWluc3RhbmNlJztcbmltcG9ydCB7IEltYWdlQ29udGFpbmVyIH0gZnJvbSAnLi4vLi4vZG9tYWluL2ltYWdlLWNvbnRhaW5lcic7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3F1ZXN0aW9ubmFpcmUnLFxuICB0ZW1wbGF0ZVVybDogJy4vcXVlc3Rpb25uYWlyZS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL3F1ZXN0aW9ubmFpcmUuY29tcG9uZW50LnNjc3MnXVxuICAvLyBkaXJlY3RpdmVzOiBbUXVlc3Rpb25MaW5lQ29tcG9uZW50LCBUZXh0UXVlc3Rpb25Db21wb25lbnQsIExvYWRpbmdJbmRpY2F0b3JdXG59KVxuZXhwb3J0IGNsYXNzIFF1ZXN0aW9ubmFpcmVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSwgT25DaGFuZ2VzIHtcbiAgQElucHV0KCkgc3RlcEluc3RhbmNlOiBTdGVwSW5zdGFuY2U7XG4gIEBJbnB1dCgpIHVzZXJJZDogbnVtYmVyO1xuICBASW5wdXQoKSB3aXphcmQ6IGJvb2xlYW47XG4gIEBJbnB1dCgpIG5ld1BhZ2VQZXJTdWJxdWVzdGlvbjogYm9vbGVhbjtcbiAgcXVlc3Rpb25uYWlyZTogUXVlc3Rpb25uYWlyZTtcbiAgcGFnZUluZGV4OiBudW1iZXI7XG4gIEBPdXRwdXQoKSBzdGVwQ29tcGxldGVkTGlzdGVuZXIgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBjYW5jZWxMaXN0ZW5lciA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIG9uVGFrZVBpY3R1cmUgPSBuZXcgRXZlbnRFbWl0dGVyPEltYWdlQ29udGFpbmVyPigpO1xuICBAT3V0cHV0KCkgb25RdWVzdGlvbkFuc3dlcmVkID0gbmV3IEV2ZW50RW1pdHRlcjxRdWVzdGlvbkxpbmU+KCk7XG4gIGVycm9yOiBhbnk7XG4gIGxvYWRpbmc6IGJvb2xlYW47XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBwcm9qZWN0U2VydmljZTogUHJvamVjdFNlcnZpY2UpIHtcbiAgICB0aGlzLnBhZ2VJbmRleCA9IC0xO1xuICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcbiAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmdldFF1ZXN0aW9ubmFpcmUodGhpcy5zdGVwSW5zdGFuY2UuaWQpLnRoZW4oKHF1ZXN0aW9ubmFpcmUpID0+IHtcbiAgICAgIHRoaXMucXVlc3Rpb25uYWlyZSA9IHF1ZXN0aW9ubmFpcmU7XG4gICAgICB0aGlzLnBhZ2VJbmRleCA9IDA7XG4gICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICB9KTtcbiAgfVxuXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXMpIHtcbiAgICBpZiAoY2hhbmdlcy5zdGVwSW5zdGFuY2UgJiYgY2hhbmdlcy5zdGVwSW5zdGFuY2UucHJldmlvdXNWYWx1ZSAmJiBjaGFuZ2VzLnN0ZXBJbnN0YW5jZS5wcmV2aW91c1ZhbHVlLmlkICYmXG4gICAgICAgICAgY2hhbmdlcy5zdGVwSW5zdGFuY2UuY3VycmVudFZhbHVlLmlkICE9PSBjaGFuZ2VzLnN0ZXBJbnN0YW5jZS5wcmV2aW91c1ZhbHVlLmlkKSB7XG4gICAgICB0aGlzLnBhZ2VJbmRleCA9IC0xO1xuICAgICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcbiAgICAgIHRoaXMucHJvamVjdFNlcnZpY2UuZ2V0UXVlc3Rpb25uYWlyZSh0aGlzLnN0ZXBJbnN0YW5jZS5pZCkudGhlbigocXVlc3Rpb25uYWlyZSkgPT4ge1xuICAgICAgICB0aGlzLnF1ZXN0aW9ubmFpcmUgPSBxdWVzdGlvbm5haXJlO1xuICAgICAgICB0aGlzLnBhZ2VJbmRleCA9IDA7XG4gICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgbmdPbkRlc3Ryb3koKSB7XG4gIH1cblxuICBwcmV2aW91cygpIHtcbiAgICBpZiAodGhpcy5wYWdlSW5kZXggPiAwKSB7XG4vLyAgICAgICAgICAgIGxldCBxdWVzdGlvbkxpbmUgPSB0aGlzLnF1ZXN0aW9ubmFpcmUucXVlc3Rpb25MaW5lc1t0aGlzLnBhZ2VJbmRleF07XG4gICAgICB0cnkge1xuLy8gICAgICAgICAgICAgICAgdGhpcy5zZXRBbGxWYWxpZGF0aW9uTWVzc2FnZXMocXVlc3Rpb25MaW5lKTtcbi8vICAgICAgICAgICAgICAgIHRoaXMudmFsaWRhdGVRdWVzdGlvbkxpbmUocXVlc3Rpb25MaW5lKTtcbiAgICAgICAgdGhpcy5wYWdlSW5kZXgtLTtcbiAgICAgICAgZG9jdW1lbnQuYm9keS5zY3JvbGxUb3AgPSAwO1xuICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgLy8gY29uc29sZS5sb2coZXJyb3IpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIG5leHQoKSB7XG4gICAgaWYgKHRoaXMucGFnZUluZGV4IDwgdGhpcy5xdWVzdGlvbm5haXJlLnF1ZXN0aW9uTGluZXMubGVuZ3RoIC0gMSkge1xuICAgICAgbGV0IHF1ZXN0aW9uTGluZSA9IHRoaXMucXVlc3Rpb25uYWlyZS5xdWVzdGlvbkxpbmVzW3RoaXMucGFnZUluZGV4XTtcbiAgICAgIHRyeSB7XG4gICAgICAgIHRoaXMuc2V0QWxsVmFsaWRhdGlvbk1lc3NhZ2VzKHF1ZXN0aW9uTGluZSk7XG4gICAgICAgIHRoaXMudmFsaWRhdGVRdWVzdGlvbkxpbmUocXVlc3Rpb25MaW5lKTtcbiAgICAgICAgdGhpcy5wYWdlSW5kZXgrKztcbiAgICAgICAgZG9jdW1lbnQuYm9keS5zY3JvbGxUb3AgPSAwO1xuICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgLy8gY29uc29sZS5sb2coZXJyb3IpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHNhdmUoc3VibWl0OiBib29sZWFuKSB7XG4gICAgaWYgKCF0aGlzLmxvYWRpbmcpIHtcbiAgICAgIHRoaXMubG9hZGluZyA9IHRydWU7XG4gICAgICBsZXQgcmVzcG9uc2VzID0gbmV3IEFycmF5PFF1ZXN0aW9uUmVzcG9uc2U+KCk7XG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMucXVlc3Rpb25uYWlyZS5xdWVzdGlvbkxpbmVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIHRoaXMuYWRkUmVzcG9uc2VzKHRoaXMucXVlc3Rpb25uYWlyZS5xdWVzdGlvbkxpbmVzW2ldLCByZXNwb25zZXMpO1xuICAgICAgfVxuXG4gICAgICB0aGlzLnByb2plY3RTZXJ2aWNlLnVwZGF0ZVF1ZXN0aW9ubmFpcmUodGhpcy5zdGVwSW5zdGFuY2UuaWQsIHJlc3BvbnNlcywgc3VibWl0LCB0aGlzLnVzZXJJZCkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICBpZiAoc3VibWl0KSB7XG4gICAgICAgICAgdGhpcy5wYWdlSW5kZXggPSAwO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5zdGVwQ29tcGxldGVkTGlzdGVuZXIuZW1pdCh7XG4gICAgICAgICAgdmFsdWU6IHRoaXMuc3RlcEluc3RhbmNlXG4gICAgICAgIH0pO1xuXG4gICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgY2FuY2VsKCkge1xuICAgIHRoaXMuY2FuY2VsTGlzdGVuZXIuZW1pdCh7XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIGFkZFJlc3BvbnNlcyhxdWVzdGlvbkxpbmU6IFF1ZXN0aW9uTGluZSwgcmVzcG9uc2VzOiBRdWVzdGlvblJlc3BvbnNlW10pIHtcbiAgICBpZiAoIXF1ZXN0aW9uTGluZSlcbiAgICAgIHJldHVybjtcblxuICAgIGlmIChxdWVzdGlvbkxpbmUuYW5zd2VyVHlwZSA9PT0gJ0NIRUNLX0JPWCcgfHwgcXVlc3Rpb25MaW5lLmFuc3dlclR5cGUgPT09ICdSQURJT19CVVRUT04nKSB7XG4gICAgICBsZXQgcXVlc3Rpb25SZXNwb25zZSA9IG5ldyBRdWVzdGlvblJlc3BvbnNlKCk7XG4gICAgICBxdWVzdGlvblJlc3BvbnNlLnF1ZXN0aW9uTGluZUlkID0gcXVlc3Rpb25MaW5lLmlkO1xuICAgICAgcXVlc3Rpb25SZXNwb25zZS53ZWlnaHQgPSBxdWVzdGlvbkxpbmUud2VpZ2h0O1xuICAgICAgcXVlc3Rpb25SZXNwb25zZS5zZWxlY3RlZCA9IHF1ZXN0aW9uTGluZS5xdWVzdGlvblJlc3BvbnNlLnNlbGVjdGVkO1xuICAgICAgcmVzcG9uc2VzLnB1c2gocXVlc3Rpb25SZXNwb25zZSk7XG4gICAgfSBlbHNlIGlmIChxdWVzdGlvbkxpbmUuYW5zd2VyVHlwZSA9PT0gJ1RFWFQnIHx8IHF1ZXN0aW9uTGluZS5hbnN3ZXJUeXBlID09PSAnVEVYVF9BUkVBJyB8fFxuICAgICAgICBxdWVzdGlvbkxpbmUuYW5zd2VyVHlwZSA9PT0gJ05VTUJFUicgfHwgcXVlc3Rpb25MaW5lLmFuc3dlclR5cGUgPT09ICdJTUFHRScpIHtcbiAgICAgIGxldCBxdWVzdGlvblJlc3BvbnNlID0gbmV3IFF1ZXN0aW9uUmVzcG9uc2UoKTtcbiAgICAgIHF1ZXN0aW9uUmVzcG9uc2UucXVlc3Rpb25MaW5lSWQgPSBxdWVzdGlvbkxpbmUuaWQ7XG4gICAgICBxdWVzdGlvblJlc3BvbnNlLndlaWdodCA9IHF1ZXN0aW9uTGluZS53ZWlnaHQ7XG4gICAgICBxdWVzdGlvblJlc3BvbnNlLnJlc3BvbnNlID0gcXVlc3Rpb25MaW5lLnF1ZXN0aW9uUmVzcG9uc2UucmVzcG9uc2U7XG4gICAgICByZXNwb25zZXMucHVzaChxdWVzdGlvblJlc3BvbnNlKTtcbiAgICB9IGVsc2UgaWYgKHF1ZXN0aW9uTGluZS5hbnN3ZXJUeXBlID09PSAnU0VMRUNUJykge1xuICAgICAgbGV0IHF1ZXN0aW9uUmVzcG9uc2UgPSBuZXcgUXVlc3Rpb25SZXNwb25zZSgpO1xuICAgICAgcXVlc3Rpb25SZXNwb25zZS5xdWVzdGlvbkxpbmVJZCA9IHF1ZXN0aW9uTGluZS5pZDtcbiAgICAgIHF1ZXN0aW9uUmVzcG9uc2Uud2VpZ2h0ID0gcXVlc3Rpb25MaW5lLnF1ZXN0aW9uUmVzcG9uc2Uud2VpZ2h0O1xuICAgICAgaWYgKHF1ZXN0aW9uTGluZS5xdWVzdGlvblJlc3BvbnNlLndlaWdodCkge1xuICAgICAgICBxdWVzdGlvblJlc3BvbnNlLnNlbGVjdGVkID0gdHJ1ZTtcbiAgICAgICAgcXVlc3Rpb25SZXNwb25zZS5yZXNwb25zZSA9IHF1ZXN0aW9uTGluZS5xdWVzdGlvblJlc3BvbnNlLnJlc3BvbnNlO1xuICAgICAgfVxuXG4gICAgICByZXNwb25zZXMucHVzaChxdWVzdGlvblJlc3BvbnNlKTtcbiAgICB9IGVsc2UgaWYgKChxdWVzdGlvbkxpbmUubGluZVR5cGUgPT09ICdTSU5HTEVfU0VMRUNUJyB8fCBxdWVzdGlvbkxpbmUubGluZVR5cGUgPT09ICdNVUxUSV9TRUxFQ1QnKSAmJlxuICAgICAgICAocXVlc3Rpb25MaW5lLmFsbG93U3VwcG9ydGluZ0Rlc2NyaXB0aW9uIHx8IHF1ZXN0aW9uTGluZS5hbGxvd1N1cHBvcnRpbmdJbWFnZSkpIHtcbiAgICAgIGxldCBxdWVzdGlvblJlc3BvbnNlID0gbmV3IFF1ZXN0aW9uUmVzcG9uc2UoKTtcbiAgICAgIHF1ZXN0aW9uUmVzcG9uc2UucXVlc3Rpb25MaW5lSWQgPSBxdWVzdGlvbkxpbmUuaWQ7XG4gICAgICBxdWVzdGlvblJlc3BvbnNlLnN1cHBvcnRpbmdEZXNjcmlwdGlvbiA9IHF1ZXN0aW9uTGluZS5xdWVzdGlvblJlc3BvbnNlLnN1cHBvcnRpbmdEZXNjcmlwdGlvbjtcbiAgICAgIHF1ZXN0aW9uUmVzcG9uc2Uuc3VwcG9ydGluZ0ltYWdlID0gcXVlc3Rpb25MaW5lLnF1ZXN0aW9uUmVzcG9uc2Uuc3VwcG9ydGluZ0ltYWdlO1xuICAgICAgcmVzcG9uc2VzLnB1c2gocXVlc3Rpb25SZXNwb25zZSk7XG4gICAgfVxuXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBxdWVzdGlvbkxpbmUuY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcbiAgICAgIHRoaXMuYWRkUmVzcG9uc2VzKHF1ZXN0aW9uTGluZS5jaGlsZHJlbltpXSwgcmVzcG9uc2VzKTtcbiAgICB9XG4gIH07XG5cbiAgcHJpdmF0ZSB2YWxpZGF0ZVF1ZXN0aW9uTGluZShxdWVzdGlvbkxpbmU6IFF1ZXN0aW9uTGluZSkge1xuICAgIGlmICghdGhpcy5oYXNWYWxpZEFuc3dlcihxdWVzdGlvbkxpbmUpKVxuICAgICAgdGhyb3cgXCJJbnZhbGlkIHJlc3BvbnNlXCI7XG5cbiAgICBpZiAocXVlc3Rpb25MaW5lLmNoaWxkcmVuKSB7XG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHF1ZXN0aW9uTGluZS5jaGlsZHJlbi5sZW5ndGg7IGkrKykge1xuICAgICAgICB2YXIgY2hpbGQgPSBxdWVzdGlvbkxpbmUuY2hpbGRyZW5baV07XG4gICAgICAgIGlmIChjaGlsZC5hbnN3ZXJUeXBlID09PSBcIkNIRUNLX0JPWFwiIHx8IGNoaWxkLmFuc3dlclR5cGUgPT09IFwiUkFESU9fQlVUVE9OXCIpIHtcbiAgICAgICAgICB2YXIgY2hpbGRSZXNwb25zZSA9IHF1ZXN0aW9uTGluZS5xdWVzdGlvblJlc3BvbnNlO1xuICAgICAgICAgIGlmIChjaGlsZFJlc3BvbnNlICYmIGNoaWxkUmVzcG9uc2Uuc2VsZWN0ZWQpIHtcbiAgICAgICAgICAgIHRoaXMudmFsaWRhdGVRdWVzdGlvbkxpbmUoY2hpbGQpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aGlzLnZhbGlkYXRlUXVlc3Rpb25MaW5lKGNoaWxkKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgaGFzVmFsaWRBbnN3ZXIocXVlc3Rpb25MaW5lOiBRdWVzdGlvbkxpbmUpIHtcbiAgICBpZiAoIXF1ZXN0aW9uTGluZS5saW5lVHlwZSB8fCAhcXVlc3Rpb25MaW5lLmFuc3dlclR5cGUpXG4gICAgICByZXR1cm4gdHJ1ZTtcblxuICAgIGlmIChxdWVzdGlvbkxpbmUubGluZVR5cGUgPT09IFwiTVVMVElfU0VMRUNUXCIgfHwgcXVlc3Rpb25MaW5lLmxpbmVUeXBlID09PSBcIlNJTkdMRV9TRUxFQ1RcIikge1xuICAgICAgaWYgKHF1ZXN0aW9uTGluZS5yZXF1aXJlZCAmJiBxdWVzdGlvbkxpbmUuY2hpbGRyZW4pIHtcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBxdWVzdGlvbkxpbmUuY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICBpZiAocXVlc3Rpb25MaW5lLmNoaWxkcmVuW2ldLnF1ZXN0aW9uUmVzcG9uc2UgJiYgcXVlc3Rpb25MaW5lLmNoaWxkcmVuW2ldLnF1ZXN0aW9uUmVzcG9uc2Uuc2VsZWN0ZWQpXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgfVxuICAgIH0gZWxzZSBpZiAocXVlc3Rpb25MaW5lLmFuc3dlclR5cGUgPT09IFwiTlVNQkVSXCIgfHwgcXVlc3Rpb25MaW5lLmFuc3dlclR5cGUgPT09IFwiU0VMRUNUXCIgfHxcbiAgICAgICAgICAgIHF1ZXN0aW9uTGluZS5hbnN3ZXJUeXBlID09PSBcIlRFWFRcIiB8fCBxdWVzdGlvbkxpbmUuYW5zd2VyVHlwZSA9PT0gXCJURVhUX0FSRUFcIiB8fCBxdWVzdGlvbkxpbmUuYW5zd2VyVHlwZSA9PT0gXCJJTUFHRVwiKSB7XG4gICAgICBpZiAocXVlc3Rpb25MaW5lLnJlcXVpcmVkICYmICghcXVlc3Rpb25MaW5lLnF1ZXN0aW9uUmVzcG9uc2UgfHwgIXF1ZXN0aW9uTGluZS5xdWVzdGlvblJlc3BvbnNlLnJlc3BvbnNlKSlcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuXG4gICAgICBpZiAocXVlc3Rpb25MaW5lLmFuc3dlclR5cGUgPT09IFwiTlVNQkVSXCIpIHtcbiAgICAgICAgaWYgKChxdWVzdGlvbkxpbmUucmVxdWlyZWQgJiYgaXNOYU4ocGFyc2VJbnQocXVlc3Rpb25MaW5lLnF1ZXN0aW9uUmVzcG9uc2UucmVzcG9uc2UpKSkgfHxcbiAgICAgICAgICAgICAgICAoIXF1ZXN0aW9uTGluZS5yZXF1aXJlZCAmJiBxdWVzdGlvbkxpbmUucXVlc3Rpb25SZXNwb25zZS5yZXNwb25zZSAmJiBpc05hTihwYXJzZUludChxdWVzdGlvbkxpbmUucXVlc3Rpb25SZXNwb25zZS5yZXNwb25zZSkpKSkge1xuICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgdmFyIGQgPSBwYXJzZUZsb2F0KHF1ZXN0aW9uTGluZS5xdWVzdGlvblJlc3BvbnNlLnJlc3BvbnNlKTtcbiAgICAgICAgICB2YXIgcCA9IHF1ZXN0aW9uTGluZS5wYXJhbWV0ZXJzLnNwbGl0KFwifFwiKTtcbiAgICAgICAgICBpZiAocC5sZW5ndGggPj0gNCkge1xuICAgICAgICAgICAgdmFyIGhhc0xpbWl0cyA9IFwidHJ1ZVwiID09PSBwWzFdO1xuICAgICAgICAgICAgdmFyIG1heCA9IHBhcnNlRmxvYXQocFsyXSk7XG4gICAgICAgICAgICB2YXIgbWluID0gcGFyc2VGbG9hdChwWzNdKTtcbiAgICAgICAgICAgIGlmIChoYXNMaW1pdHMgJiYgKGQgPCBtaW4gfHwgZCA+IG1heCkgJiYgIXF1ZXN0aW9uTGluZS5lcnJvckFjY2VwdGVkKVxuICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgfVxuICAgICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiB0cnVlO1xuICB9XG5cbiAgcHJpdmF0ZSBzZXRBbGxWYWxpZGF0aW9uTWVzc2FnZXMocXVlc3Rpb25MaW5lOiBRdWVzdGlvbkxpbmUpIHtcbiAgICB0aGlzLnNldFZhbGlkYXRpb25NZXNzYWdlKHF1ZXN0aW9uTGluZSk7XG4gICAgaWYgKHF1ZXN0aW9uTGluZS5jaGlsZHJlbikge1xuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBxdWVzdGlvbkxpbmUuY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgdmFyIGNoaWxkID0gcXVlc3Rpb25MaW5lLmNoaWxkcmVuW2ldO1xuICAgICAgICBpZiAoY2hpbGQuYW5zd2VyVHlwZSA9PT0gXCJDSEVDS19CT1hcIiB8fCBjaGlsZC5hbnN3ZXJUeXBlID09PSBcIlJBRElPX0JVVFRPTlwiKSB7XG4gICAgICAgICAgdmFyIGNoaWxkUmVzcG9uc2UgPSBjaGlsZC5xdWVzdGlvblJlc3BvbnNlO1xuICAgICAgICAgIGlmIChjaGlsZFJlc3BvbnNlICYmIGNoaWxkUmVzcG9uc2Uuc2VsZWN0ZWQpIHtcbiAgICAgICAgICAgIHRoaXMuc2V0QWxsVmFsaWRhdGlvbk1lc3NhZ2VzKGNoaWxkKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGhpcy5zZXRBbGxWYWxpZGF0aW9uTWVzc2FnZXMoY2hpbGQpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBzZXRWYWxpZGF0aW9uTWVzc2FnZShxdWVzdGlvbkxpbmU6IFF1ZXN0aW9uTGluZSkge1xuICAgIGlmICghcXVlc3Rpb25MaW5lLmxpbmVUeXBlIHx8ICFxdWVzdGlvbkxpbmUuYW5zd2VyVHlwZSkge1xuICAgICAgcXVlc3Rpb25MaW5lLmhhc0Vycm9yID0gZmFsc2U7XG4gICAgICBxdWVzdGlvbkxpbmUuZXJyb3JNZXNzYWdlID0gbnVsbDtcbiAgICAgIHF1ZXN0aW9uTGluZS5zaG93RXJyb3JDb25maXJtYXRpb24gPSBmYWxzZTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBpZiAocXVlc3Rpb25MaW5lLmxpbmVUeXBlID09PSBcIk1VTFRJX1NFTEVDVFwiIHx8IHF1ZXN0aW9uTGluZS5saW5lVHlwZSA9PT0gXCJTSU5HTEVfU0VMRUNUXCIpIHtcbiAgICAgIGlmIChxdWVzdGlvbkxpbmUucmVxdWlyZWQgJiYgcXVlc3Rpb25MaW5lLmNoaWxkcmVuKSB7XG4gICAgICAgIHZhciBzZWxlY3RlZCA9IGZhbHNlO1xuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHF1ZXN0aW9uTGluZS5jaGlsZHJlbi5sZW5ndGg7IGkrKykge1xuICAgICAgICAgIGlmIChxdWVzdGlvbkxpbmUuY2hpbGRyZW5baV0ucXVlc3Rpb25SZXNwb25zZSAmJiBxdWVzdGlvbkxpbmUuY2hpbGRyZW5baV0ucXVlc3Rpb25SZXNwb25zZS5zZWxlY3RlZCkge1xuICAgICAgICAgICAgc2VsZWN0ZWQgPSB0cnVlO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCFzZWxlY3RlZCkge1xuICAgICAgICAgIHF1ZXN0aW9uTGluZS5oYXNFcnJvciA9IHRydWU7XG4gICAgICAgICAgcXVlc3Rpb25MaW5lLnNob3dFcnJvckNvbmZpcm1hdGlvbiA9IGZhbHNlO1xuICAgICAgICAgIHF1ZXN0aW9uTGluZS5lcnJvck1lc3NhZ2UgPSBcIkFuIGFuc3dlciBtdXN0IGJlIHNlbGVjdGVkXCI7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSBlbHNlIGlmIChxdWVzdGlvbkxpbmUuYW5zd2VyVHlwZSA9PT0gXCJOVU1CRVJcIiB8fCBxdWVzdGlvbkxpbmUuYW5zd2VyVHlwZSA9PT0gXCJTRUxFQ1RcIiB8fFxuICAgICAgICAgICAgcXVlc3Rpb25MaW5lLmFuc3dlclR5cGUgPT09IFwiVEVYVFwiIHx8IHF1ZXN0aW9uTGluZS5hbnN3ZXJUeXBlID09PSBcIlRFWFRfQVJFQVwiIHx8XG4gICAgICAgICAgICBxdWVzdGlvbkxpbmUuYW5zd2VyVHlwZSA9PT0gXCJJTUFHRVwiKSB7XG4gICAgICB2YXIgcXVlc3Rpb25SZXNwb25zZSA9IHF1ZXN0aW9uTGluZS5xdWVzdGlvblJlc3BvbnNlO1xuICAgICAgaWYgKHF1ZXN0aW9uTGluZS5yZXF1aXJlZCAmJiAoIXF1ZXN0aW9uUmVzcG9uc2UgfHwgIXF1ZXN0aW9uUmVzcG9uc2UucmVzcG9uc2UpKSB7XG4gICAgICAgIHF1ZXN0aW9uTGluZS5oYXNFcnJvciA9IHRydWU7XG4gICAgICAgIHF1ZXN0aW9uTGluZS5zaG93RXJyb3JDb25maXJtYXRpb24gPSBmYWxzZTtcbiAgICAgICAgcXVlc3Rpb25MaW5lLmVycm9yTWVzc2FnZSA9IFwiQW4gYW5zd2VyIG11c3QgYmUgcHJvdmlkZWRcIjtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBpZiAocXVlc3Rpb25MaW5lLmFuc3dlclR5cGUgPT09IFwiTlVNQkVSXCIpIHtcbiAgICAgICAgaWYgKChxdWVzdGlvbkxpbmUucmVxdWlyZWQgJiYgaXNOYU4ocGFyc2VJbnQocXVlc3Rpb25SZXNwb25zZS5yZXNwb25zZSkpKSB8fFxuICAgICAgICAgICAgICAgICghcXVlc3Rpb25MaW5lLnJlcXVpcmVkICYmIHF1ZXN0aW9uUmVzcG9uc2UucmVzcG9uc2UgJiYgaXNOYU4ocGFyc2VJbnQocXVlc3Rpb25SZXNwb25zZS5yZXNwb25zZSkpKSkge1xuICAgICAgICAgIHF1ZXN0aW9uTGluZS5oYXNFcnJvciA9IHRydWU7XG4gICAgICAgICAgcXVlc3Rpb25MaW5lLnNob3dFcnJvckNvbmZpcm1hdGlvbiA9IGZhbHNlO1xuICAgICAgICAgIHF1ZXN0aW9uTGluZS5lcnJvck1lc3NhZ2UgPSBcIkFuIGludmFsaWQgbnVtYmVyIHdhcyBlbnRlcmVkXCI7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICB2YXIgZCA9IHBhcnNlRmxvYXQocXVlc3Rpb25SZXNwb25zZS5yZXNwb25zZSk7XG4gICAgICAgICAgdmFyIHAgPSBxdWVzdGlvbkxpbmUucGFyYW1ldGVycy5zcGxpdChcInxcIik7XG4gICAgICAgICAgaWYgKHAubGVuZ3RoID49IDQpIHtcbiAgICAgICAgICAgIHZhciBoYXNMaW1pdHMgPSBcInRydWVcIiA9PT0gcFsxXTtcbiAgICAgICAgICAgIHZhciBtYXggPSBwYXJzZUZsb2F0KHBbMl0pO1xuICAgICAgICAgICAgdmFyIG1pbiA9IHBhcnNlRmxvYXQocFszXSk7XG4gICAgICAgICAgICBpZiAoaGFzTGltaXRzKSB7XG4gICAgICAgICAgICAgIGlmIChkIDwgbWluKSB7XG4gICAgICAgICAgICAgICAgcXVlc3Rpb25MaW5lLmhhc0Vycm9yID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICBxdWVzdGlvbkxpbmUuc2hvd0Vycm9yQ29uZmlybWF0aW9uID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICBxdWVzdGlvbkxpbmUuZXJyb3JNZXNzYWdlID0gXCJUaGUgdmFsdWUgcHJvdmlkZWQgaXMgbGVzcyB0aGFuIHRoZSBtaW5pbXVtIHZhbHVlIG9mIFwiICsgbWluICsgXCIuICBBcmUgeW91IHN1cmUgdGhpcyBpcyBjb3JyZWN0P1wiO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgfSBlbHNlIGlmIChkID4gbWF4KSB7XG4gICAgICAgICAgICAgICAgcXVlc3Rpb25MaW5lLmhhc0Vycm9yID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICBxdWVzdGlvbkxpbmUuc2hvd0Vycm9yQ29uZmlybWF0aW9uID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICBxdWVzdGlvbkxpbmUuZXJyb3JNZXNzYWdlID0gXCJUaGUgdmFsdWUgcHJvdmlkZWQgZXhjZWVkcyB0aGUgbWF4aW11bSB2YWx1ZSBvZiBcIiArIG1heCArIFwiLiAgQXJlIHlvdSBzdXJlIHRoaXMgaXMgY29ycmVjdD9cIjtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgICAgcXVlc3Rpb25MaW5lLmhhc0Vycm9yID0gdHJ1ZTtcbiAgICAgICAgICBxdWVzdGlvbkxpbmUuc2hvd0Vycm9yQ29uZmlybWF0aW9uID0gZmFsc2U7XG4gICAgICAgICAgcXVlc3Rpb25MaW5lLmVycm9yTWVzc2FnZSA9IFwiQW4gaW52YWxpZCBudW1iZXIgd2FzIHN1cHBsaWVkXCI7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgcXVlc3Rpb25MaW5lLmhhc0Vycm9yID0gZmFsc2U7XG4gICAgcXVlc3Rpb25MaW5lLnNob3dFcnJvckNvbmZpcm1hdGlvbiA9IGZhbHNlO1xuICAgIHF1ZXN0aW9uTGluZS5lcnJvck1lc3NhZ2UgPSBcIlwiO1xuICB9XG5cbiAgdGFrZVBpY3R1cmVRdWVzdGlvbm5haXJlKHF1ZXN0aW9uTGluZTogUXVlc3Rpb25MaW5lKSB7XG4gICAgbGV0IGltYWdlQ29udGFpbmVyOiBJbWFnZUNvbnRhaW5lciA9IHtcbiAgICAgIHNldEltYWdlKGltYWdlOiBzdHJpbmcpIHtcbiAgICAgICAgcXVlc3Rpb25MaW5lLnF1ZXN0aW9uUmVzcG9uc2UucmVzcG9uc2UgPSBcImRhdGE6aW1hZ2UvanBlZztiYXNlNjQsXCIgKyBpbWFnZTtcbiAgICAgIH1cbiAgICB9O1xuICAgIHRoaXMub25UYWtlUGljdHVyZS5lbWl0KGltYWdlQ29udGFpbmVyKTtcbiAgfVxuXG4gIHRha2VTdXBwb3J0aW5nUGljdHVyZVF1ZXN0aW9ubmFpcmUocXVlc3Rpb25MaW5lOiBRdWVzdGlvbkxpbmUpIHtcbiAgICBsZXQgaW1hZ2VDb250YWluZXI6IEltYWdlQ29udGFpbmVyID0ge1xuICAgICAgc2V0SW1hZ2UoaW1hZ2U6IHN0cmluZykge1xuICAgICAgICBxdWVzdGlvbkxpbmUucXVlc3Rpb25SZXNwb25zZS5zdXBwb3J0aW5nSW1hZ2UgPSBcImRhdGE6aW1hZ2UvanBlZztiYXNlNjQsXCIgKyBpbWFnZTtcbiAgICAgIH1cbiAgICB9O1xuICAgIHRoaXMub25UYWtlUGljdHVyZS5lbWl0KGltYWdlQ29udGFpbmVyKTtcbiAgfVxuXG4gIHF1ZXN0aW9uQW5zd2VyZWQocXVlc3Rpb25MaW5lOiBRdWVzdGlvbkxpbmUpIHtcbiAgICBjb25zb2xlLmxvZyhcInF1ZXN0aW9ubmFpcmUucXVlc3Rpb25BbnN3ZXJlZDogXCIgKyBxdWVzdGlvbkxpbmUud29yZGluZyArIFwiIFwiICsgcXVlc3Rpb25MaW5lLmludGVncmF0aW9uSW5kaWNhdG9yICsgXCIgXCIgKyBxdWVzdGlvbkxpbmUuaWQpO1xuICAgIHRoaXMub25RdWVzdGlvbkFuc3dlcmVkLmVtaXQocXVlc3Rpb25MaW5lKTtcbiAgfVxufVxuIl19