/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Output } from '@angular/core';
var TextQuestionComponent = /** @class */ (function () {
    function TextQuestionComponent() {
        this.close = new EventEmitter();
    }
    /**
     * @return {?}
     */
    TextQuestionComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    TextQuestionComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
    };
    TextQuestionComponent.decorators = [
        { type: Component, args: [{
                    selector: 'text-question',
                    template: "<!--<form class=\"form-horizontal\">\n    <div class=\"form-group\">\n        <label for=\"textQuestion\" class=\"col-sm-6 text-left\">Text question text</label>\n        <div class=\"col-sm-6\">\n            <input type=\"text\" class=\"form-control\" id=\"textQuestion\" placeholder=\"\">\n        </div>\n    </div>\n</form>-->\n\n<div class=\"row\">\n    <div class=\"col-md-6\"><label class=\"text-muted\">&#9679;&nbsp;&nbsp;Question text</label></div>\n    <div class=\"col-md-6\"><input type=\"text\" class=\"form-control\" id=\"exampleInputEmail1\" placeholder=\"\"></div>\n</div>\n"
                }] }
    ];
    /** @nocollapse */
    TextQuestionComponent.ctorParameters = function () { return []; };
    TextQuestionComponent.propDecorators = {
        close: [{ type: Output }]
    };
    return TextQuestionComponent;
}());
export { TextQuestionComponent };
if (false) {
    /** @type {?} */
    TextQuestionComponent.prototype.close;
    /** @type {?} */
    TextQuestionComponent.prototype.error;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dC1xdWVzdGlvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL3N0ZXBzL3F1ZXN0aW9ubmFpcmUvdGV4dC1xdWVzdGlvbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUE0QixNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFMUY7SUFRRTtRQUhVLFVBQUssR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO0lBSXJDLENBQUM7Ozs7SUFFRCx3Q0FBUTs7O0lBQVI7SUFDQSxDQUFDOzs7O0lBRUQsMkNBQVc7OztJQUFYO0lBQ0EsQ0FBQzs7Z0JBZkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxlQUFlO29CQUN6QiwwbEJBQTZDO2lCQUM5Qzs7Ozs7d0JBRUUsTUFBTTs7SUFXVCw0QkFBQztDQUFBLEFBaEJELElBZ0JDO1NBWlkscUJBQXFCOzs7SUFDaEMsc0NBQXFDOztJQUNyQyxzQ0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPbkRlc3Ryb3ksIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd0ZXh0LXF1ZXN0aW9uJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3RleHQtcXVlc3Rpb24uY29tcG9uZW50Lmh0bWwnXG59KVxuZXhwb3J0IGNsYXNzIFRleHRRdWVzdGlvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgQE91dHB1dCgpIGNsb3NlID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBlcnJvcjogYW55O1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpIHtcbiAgfVxufVxuIl19