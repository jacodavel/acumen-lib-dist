/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, EventEmitter, Output } from '@angular/core';
import { QuestionLine } from '../../domain/question-line';
import { SelectQuestionOption } from '../../domain/select-question-option';
var QuestionLineComponent = /** @class */ (function () {
    function QuestionLineComponent() {
        this.onTakePictureQuestionLine = new EventEmitter();
        this.onTakeSupportingPictureQuestionLine = new EventEmitter();
        this.onQuestionAnswered = new EventEmitter();
    }
    /**
     * @return {?}
     */
    QuestionLineComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    QuestionLineComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (this.questionLine && this.questionLine.children) {
            for (var i = 0; i < this.questionLine.children.length; i++) {
                this.questionLine.children[i].parent = this.questionLine;
            }
            this.selectQuestionOptions = new Array();
            if (this.questionLine.parameters) {
                /** @type {?} */
                var rows = this.questionLine.parameters.split("|");
                /** @type {?} */
                var index = 0;
                for (var i = 0; i < rows.length; i++) {
                    /** @type {?} */
                    var s = rows[i].split("=");
                    if (s.length >= 2) {
                        /** @type {?} */
                        var selectQuestionOption = new SelectQuestionOption();
                        selectQuestionOption.description = s[1];
                        selectQuestionOption.weight = Number(s[2]);
                        selectQuestionOption.index = index;
                        this.selectQuestionOptions.push(selectQuestionOption);
                        index++;
                    }
                }
            }
        }
    };
    /**
     * @return {?}
     */
    QuestionLineComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @param {?} $selected
     * @return {?}
     */
    QuestionLineComponent.prototype.radioButtonChanged = /**
     * @param {?} $selected
     * @return {?}
     */
    function ($selected) {
        if (this.questionLine) {
            for (var i = 0; i < this.questionLine.parent.children.length; i++) {
                if (this.questionLine.parent.children[i].id !== this.questionLine.id) {
                    this.questionLine.parent.children[i].questionResponse.selected = false;
                }
            }
            if (!$selected) {
                $selected = {
                    value: false,
                };
            }
            this.questionLine.questionResponse.selected = $selected.value;
            this.questionLine.questionResponse.weight = this.questionLine.weight;
            if (this.questionLine.questionResponse.selected) {
                this.onQuestionAnswered.emit(this.questionLine);
            }
        }
    };
    /**
     * @param {?} $selected
     * @return {?}
     */
    QuestionLineComponent.prototype.checkboxChanged = /**
     * @param {?} $selected
     * @return {?}
     */
    function ($selected) {
        if (!$selected) {
            $selected = {
                value: false,
            };
        }
        if (this.questionLine) {
            this.questionLine.questionResponse.selected = $selected.value;
            this.questionLine.questionResponse.weight = this.questionLine.weight;
            if (this.questionLine.questionResponse.selected) {
                this.onQuestionAnswered.emit(this.questionLine);
            }
        }
    };
    /**
     * @param {?} description
     * @return {?}
     */
    QuestionLineComponent.prototype.selectChanged = /**
     * @param {?} description
     * @return {?}
     */
    function (description) {
        if (this.questionLine) {
            /** @type {?} */
            var selectQuestionOption = null;
            for (var i = 0; i < this.selectQuestionOptions.length; i++) {
                if (this.selectQuestionOptions[i].description === description) {
                    selectQuestionOption = this.selectQuestionOptions[i];
                    break;
                }
            }
            // let selectQuestionOption = this.selectQuestionOptions[index];
            if (selectQuestionOption) {
                this.questionLine.questionResponse.selected = true;
                this.questionLine.questionResponse.weight = selectQuestionOption.weight;
                this.questionLine.questionResponse.response = selectQuestionOption.description;
                this.onQuestionAnswered.emit(this.questionLine);
            }
        }
    };
    /**
     * @param {?} questionLine
     * @return {?}
     */
    QuestionLineComponent.prototype.takePicture = /**
     * @param {?} questionLine
     * @return {?}
     */
    function (questionLine) {
        this.onTakePictureQuestionLine.emit(questionLine);
    };
    /**
     * @param {?} questionLine
     * @return {?}
     */
    QuestionLineComponent.prototype.takeSupportingPicture = /**
     * @param {?} questionLine
     * @return {?}
     */
    function (questionLine) {
        this.onTakeSupportingPictureQuestionLine.emit(questionLine);
    };
    /**
     * @param {?} questionLine
     * @return {?}
     */
    QuestionLineComponent.prototype.questionAnswered = /**
     * @param {?} questionLine
     * @return {?}
     */
    function (questionLine) {
        console.log("questionLine.questionAnswered: " + questionLine.wording + " " + questionLine.integrationIndicator + " " + questionLine.id);
        this.onQuestionAnswered.emit(questionLine);
    };
    QuestionLineComponent.decorators = [
        { type: Component, args: [{
                    selector: 'question-line',
                    template: "<div class=\"question-line\">\n  <div *ngIf=\"questionLine && questionLine.answerType === 'NONE'\" class=\"statement\">\n    <div class=\"wording\" [ngClass]=\"{ 'heading': questionLine.lineType === 'HEADING' }\">\n      {{ questionLine.wording }}\n    </div>\n    <div [hidden]=\"!questionLine.hasError\" class=\"error-text\">\n      This question must be answered\n    </div>\n    <div *ngFor=\"let child of questionLine.children\" class=\"answer-question-lines\">\n      <question-line [questionLine]=\"child\" (onTakePictureQuestionLine)=\"takePicture(child)\" (onQuestionAnswered)=\"questionAnswered($event)\"\n          (onTakeSupportingPictureQuestionLine)=\"takeSupportingPicture(child)\"></question-line>\n    </div>\n\n    <div *ngIf=\"questionLine && questionLine.allowSupportingDescription\" class=\"control-question-line supporting-question-line\">\n      <div class=\"wording\">\n        Please provide motivation for your choice\n      </div>\n      <textarea *ngIf=\"questionLine.allowSupportingDescription\" rows=\"5\" [(ngModel)]=\"questionLine.questionResponse.supportingDescription\" placeholder=\"Type here\">\n      </textarea>\n    </div>\n\n    <div *ngIf=\"questionLine && questionLine.allowSupportingImage\" class=\"image-question-line supporting-question-line\">\n      <div class=\"row\">\n        <div class=\"wording\">\n          Please take a picture to support your choice\n        </div>\n        <div class=\"button\" (click)=\"takeSupportingPicture(questionLine)\">\n            <img src=\"data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAABBBJREFUeJzt289rXUUUB/BP0qZJTdriomjophWXamKh7lyoCyGtWQiKrlxYa924caFS+ie40UbrX+APxF1F0OIriFttGiu1JG0XQkEQbCyJUnNdzAuWmPdm7rv3zX3EfOGQx5t7zznf7503c2bmZkizmMY5jPZ4/7d4Fmu1ZZQZX6OoaC9mz7omPKE6+QI/Y2fm3GvBd+oRoMDLmXOvjKPqI1/ght7HkewYwvfqFaDA6zlJVMFz6idf4CbuycijJ+zAT/ojQIG38lHpDS/pH/kCv2FfmYR2VCRUBiP4DPf2McZu3ME3fYzRM17T36e/bsvYn4lTMnbjF3kEKPBOHlrpeEM+8gVWcCALswTswa/yClDgbA5yKTgtP/kCf+GBWHJDJYiMYG/7byoO4kslp6YacU5YLa50aL/TTYBhPI3n8TgOtb/bSvikU8OM/lZsg2KtjevoEbyLkwnqbQncLcAIPsexhnJpBHf/ps/4n5HnXwFmcaLJRJrCsLAgGrjSMReGhaf/YNOJNIVhvJBw3SVhH2+8bcfwY825LONjvILHhBXdaNv2t787Iczdf9QZ+Kbuc+U8Jja5bw8WIvem2FUcV247a1wQY7Fi7JaEi2a6JPJMheAreFO50nojduFtrPZTgG5PZrzHwIuYqkB8Ix7FtR7yaEm4qJsAEz0Encf91Tn/B5PCWFW7AEe7BJ0tGXAR99XBtgMmlesJLQkXLQgD3kbsxeUSwVbU2+074bD0MaEl8cIFYcAbF7r9rHLkC2HAy4VTiTm1lCBQxa5KH+2n8AGu4M+2XcH7eCTRxy4sDZIAxxOSHsOHwssOnfysYU7aQeirgyLALfEiZ0w4zEj1eb59TzdMCBVj4wJ8FEmU8OTL+p1L8PvpIAgQ6/5Tunf7TraGhyO+Yz+DVo5NzouR9pPK7U6vY0ggWCV2ll3ea5H2Jyv4firSvhRzkEOAW5H2gxV8H4q0/x5zsNX2+UsjhwB7I+3XK/iOdfHoiVQOAWLd9HwF37F7o2eDOQSYjrSfFaakslgT6ocqsbMIEBup54XavyzmhEValdjofyG0LKwiu2FU6M6pPr8SFjzdMIHbET+tHAIU0g5dxoSn2q0q/Fs4u4yRJ+2dpGwCLCYmTShvzwin06ttu4z38FCij1FhdhkYAQph9zYXUt9KaSlBoKqtCru3/cYRYRNl4AQohHXBZL+YC2+G3SiRT0ufCW9ml/RHhAPCcV2ZXBoRYL0nHK6R/BHlnnzjAhTCmHBK+uywGUaFAS/1Nz9QAqzbkrCxsdkBbCdMCPP89YqxW0PtD4OA2/hCqAgvCsKsr+f3CQubaaG8nVHPP0dcGCQBmsCF7Q2RphNoGtsCNJ1A09gWoOkEmsZOYa1epgjZSvjhH9khIZLIyIO8AAAAAElFTkSuQmCC\">\n        </div>\n      </div>\n      <div class=\"image-answer\">\n        <img src=\"{{ questionLine.questionResponse.supportingImage }}\">\n      </div>\n    </div>\n  </div>\n\n  <div *ngIf=\"questionLine && questionLine.answerType === 'RADIO_BUTTON'\" class=\"select-question-line\">\n    <radio-button-component [description]=\"questionLine.wording\" [selected]=\"questionLine.questionResponse.selected\"\n        (selectionListener)=\"radioButtonChanged($event)\"></radio-button-component>\n    <ng-container *ngIf=\"questionLine.questionResponse.selected && questionLine.children.length > 0\">\n      <div *ngFor=\"let child of questionLine.children\" class=\"sub-question-lines\">\n        <question-line [questionLine]=\"child\" (onQuestionAnswered)=\"questionAnswered($event)\"></question-line>\n      </div>\n    </ng-container>\n  </div>\n\n  <div *ngIf=\"questionLine && questionLine.answerType === 'CHECK_BOX'\" class=\"select-question-line\">\n    <checkbox-component [description]=\"questionLine.wording\" [selected]=\"questionLine.questionResponse.selected\"\n        (selectionListener)=\"checkboxChanged($event)\"></checkbox-component>\n    <ng-container *ngIf=\"questionLine.questionResponse.selected && questionLine.children.length > 0\">\n      <div *ngFor=\"let child of questionLine.children\" class=\"sub-question-lines\">\n        <question-line [questionLine]=\"child\"></question-line>\n      </div>\n    </ng-container>\n  </div>\n\n  <div *ngIf=\"questionLine && questionLine.answerType === 'SELECT'\" class=\"control-question-line\">\n    <div class=\"wording\">\n      {{ questionLine.wording }}\n    </div>\n    <div [hidden]=\"!questionLine.hasError\" class=\"error-text\">\n      This question must be answered\n    </div>\n    <select [(ngModel)]=\"questionLine.questionResponse.response\" (change)=\"selectChanged($event.target.value)\">\n      <option *ngFor=\"let selectQuestionOption of selectQuestionOptions\" [value]=\"selectQuestionOption.description\">\n        {{ selectQuestionOption.description }}\n      </option>\n    </select>\n  </div>\n\n  <div *ngIf=\"questionLine && questionLine.answerType === 'TEXT'\" class=\"control-question-line\">\n    <div class=\"wording\">\n      {{ questionLine.wording }}\n    </div>\n    <div [hidden]=\"!questionLine.hasError\" class=\"error-text\">\n      This question must be answered\n    </div>\n    <input type=\"text\" [(ngModel)]=\"questionLine.questionResponse.response\" placeholder=\"Type here\">\n  </div>\n\n  <div *ngIf=\"questionLine && questionLine.answerType === 'TEXT_AREA'\" class=\"control-question-line\">\n    <div class=\"wording\">\n      {{ questionLine.wording }}\n    </div>\n    <div [hidden]=\"!questionLine.hasError\" class=\"error-text\">\n      This question must be answered\n    </div>\n    <textarea rows=\"5\" [(ngModel)]=\"questionLine.questionResponse.response\" placeholder=\"Type here\">\n    </textarea>\n  </div>\n\n  <div *ngIf=\"questionLine && questionLine.answerType === 'NUMBER'\" class=\"control-question-line\">\n    <div class=\"wording\">\n      {{ questionLine.wording }}\n    </div>\n    <div [hidden]=\"!questionLine.hasError\" class=\"error-text\">\n      This question must be answered\n    </div>\n    <input type=\"number\" [(ngModel)]=\"questionLine.questionResponse.response\" placeholder=\"Type here\">\n  </div>\n\n  <div *ngIf=\"questionLine && questionLine.answerType === 'IMAGE'\" class=\"image-question-line\">\n    <div class=\"row\">\n      <div class=\"wording\">\n        {{ questionLine.wording }}\n      </div>\n      <div class=\"button\" (click)=\"takePicture(questionLine)\">\n          <img src=\"data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAABBBJREFUeJzt289rXUUUB/BP0qZJTdriomjophWXamKh7lyoCyGtWQiKrlxYa924caFS+ie40UbrX+APxF1F0OIriFttGiu1JG0XQkEQbCyJUnNdzAuWmPdm7rv3zX3EfOGQx5t7zznf7503c2bmZkizmMY5jPZ4/7d4Fmu1ZZQZX6OoaC9mz7omPKE6+QI/Y2fm3GvBd+oRoMDLmXOvjKPqI1/ght7HkewYwvfqFaDA6zlJVMFz6idf4CbuycijJ+zAT/ojQIG38lHpDS/pH/kCv2FfmYR2VCRUBiP4DPf2McZu3ME3fYzRM17T36e/bsvYn4lTMnbjF3kEKPBOHlrpeEM+8gVWcCALswTswa/yClDgbA5yKTgtP/kCf+GBWHJDJYiMYG/7byoO4kslp6YacU5YLa50aL/TTYBhPI3n8TgOtb/bSvikU8OM/lZsg2KtjevoEbyLkwnqbQncLcAIPsexhnJpBHf/ps/4n5HnXwFmcaLJRJrCsLAgGrjSMReGhaf/YNOJNIVhvJBw3SVhH2+8bcfwY825LONjvILHhBXdaNv2t787Iczdf9QZ+Kbuc+U8Jja5bw8WIvem2FUcV247a1wQY7Fi7JaEi2a6JPJMheAreFO50nojduFtrPZTgG5PZrzHwIuYqkB8Ix7FtR7yaEm4qJsAEz0Encf91Tn/B5PCWFW7AEe7BJ0tGXAR99XBtgMmlesJLQkXLQgD3kbsxeUSwVbU2+074bD0MaEl8cIFYcAbF7r9rHLkC2HAy4VTiTm1lCBQxa5KH+2n8AGu4M+2XcH7eCTRxy4sDZIAxxOSHsOHwssOnfysYU7aQeirgyLALfEiZ0w4zEj1eb59TzdMCBVj4wJ8FEmU8OTL+p1L8PvpIAgQ6/5Tunf7TraGhyO+Yz+DVo5NzouR9pPK7U6vY0ggWCV2ll3ea5H2Jyv4firSvhRzkEOAW5H2gxV8H4q0/x5zsNX2+UsjhwB7I+3XK/iOdfHoiVQOAWLd9HwF37F7o2eDOQSYjrSfFaakslgT6ocqsbMIEBup54XavyzmhEValdjofyG0LKwiu2FU6M6pPr8SFjzdMIHbET+tHAIU0g5dxoSn2q0q/Fs4u4yRJ+2dpGwCLCYmTShvzwin06ttu4z38FCij1FhdhkYAQph9zYXUt9KaSlBoKqtCru3/cYRYRNl4AQohHXBZL+YC2+G3SiRT0ufCW9ml/RHhAPCcV2ZXBoRYL0nHK6R/BHlnnzjAhTCmHBK+uywGUaFAS/1Nz9QAqzbkrCxsdkBbCdMCPP89YqxW0PtD4OA2/hCqAgvCsKsr+f3CQubaaG8nVHPP0dcGCQBmsCF7Q2RphNoGtsCNJ1A09gWoOkEmsZOYa1epgjZSvjhH9khIZLIyIO8AAAAAElFTkSuQmCC\">\n      </div>\n    </div>\n\n    <div class=\"image-answer\">\n      <img src=\"{{ questionLine.questionResponse.response }}\">\n    </div>\n  </div>\n</div>\n",
                    styles: [".question-line .statement{width:100%}.question-line .statement .wording{font-size:15px}.question-line .statement .heading{font-size:15px;font-weight:700}.question-line .statement .error-text{font-style:italic;font-size:11px;color:#f04141}.question-line .statement .answer-question-lines{width:100%}.question-line .sub-question-lines{padding-left:32px}.question-line .control-question-line{width:100%}.question-line .control-question-line .wording{width:100%;font-size:15px;padding-bottom:5px}.question-line .control-question-line .error-text{font-style:italic;font-size:11px;color:#f04141;padding-bottom:5px}.question-line .control-question-line input,.question-line .control-question-line select{width:100%;font-size:13px;border:1px solid #58666c;background-color:#fffcf6;box-sizing:border-box;padding:3px 5px;border-radius:5px;height:30px}.question-line .control-question-line textarea{width:100%;font-size:13px;border:1px solid #58666c;background-color:#fffcf6;padding:3px 5px;resize:none;box-sizing:border-box;border-radius:5px}.question-line .supporting-question-line{padding-bottom:20px}.question-line .image-question-line{width:100%}.question-line .image-question-line .row{display:table;width:100%}.question-line .image-question-line .row .wording{display:table-cell;width:90%;font-size:15px;padding-top:7px;vertical-align:top}.question-line .image-question-line .row .button{display:table-cell;width:32px;padding-top:7px;text-align:right;cursor:pointer;box-sizing:border-box}.question-line .image-question-line .row .button img{border:1px solid #58666c;padding:5px;border-radius:5px;box-sizing:border-box;width:24px;height:24px}.question-line .image-question-line .image-answer{width:90%;height:90%;padding:0;box-sizing:border-box;text-align:center;margin-left:auto;margin-right:auto;margin-bottom:8px}.question-line .image-question-line .image-answer img{max-width:100%;max-height:100%}"]
                }] }
    ];
    /** @nocollapse */
    QuestionLineComponent.ctorParameters = function () { return []; };
    QuestionLineComponent.propDecorators = {
        questionLine: [{ type: Input }],
        onTakePictureQuestionLine: [{ type: Output }],
        onTakeSupportingPictureQuestionLine: [{ type: Output }],
        onQuestionAnswered: [{ type: Output }]
    };
    return QuestionLineComponent;
}());
export { QuestionLineComponent };
if (false) {
    /** @type {?} */
    QuestionLineComponent.prototype.questionLine;
    /** @type {?} */
    QuestionLineComponent.prototype.onTakePictureQuestionLine;
    /** @type {?} */
    QuestionLineComponent.prototype.onTakeSupportingPictureQuestionLine;
    /** @type {?} */
    QuestionLineComponent.prototype.onQuestionAnswered;
    /** @type {?} */
    QuestionLineComponent.prototype.selectQuestionOptions;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicXVlc3Rpb24tbGluZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL3N0ZXBzL3F1ZXN0aW9ubmFpcmUvcXVlc3Rpb24tbGluZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQWdDLEtBQUssRUFBaUIsWUFBWSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUVwSCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDMUQsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFFM0U7SUFZRTtRQUxVLDhCQUF5QixHQUFHLElBQUksWUFBWSxFQUFnQixDQUFDO1FBQzdELHdDQUFtQyxHQUFHLElBQUksWUFBWSxFQUFnQixDQUFDO1FBQ3ZFLHVCQUFrQixHQUFHLElBQUksWUFBWSxFQUFnQixDQUFDO0lBSWhFLENBQUM7Ozs7SUFFRCx3Q0FBUTs7O0lBQVI7SUFDQSxDQUFDOzs7OztJQUVELDJDQUFXOzs7O0lBQVgsVUFBWSxPQUFzQjtRQUNoQyxJQUFJLElBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUU7WUFDbkQsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDMUQsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7YUFDMUQ7WUFFRCxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxLQUFLLEVBQXdCLENBQUM7WUFDL0QsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsRUFBRTs7b0JBQzVCLElBQUksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDOztvQkFDOUMsS0FBSyxHQUFHLENBQUM7Z0JBQ2IsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7O3dCQUNoQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7b0JBQzFCLElBQUksQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7OzRCQUNiLG9CQUFvQixHQUFHLElBQUksb0JBQW9CLEVBQUU7d0JBQ3JELG9CQUFvQixDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ3hDLG9CQUFvQixDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQzNDLG9CQUFvQixDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7d0JBQ25DLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQzt3QkFDdEQsS0FBSyxFQUFFLENBQUM7cUJBQ1Q7aUJBQ0Y7YUFDRjtTQUNGO0lBQ0gsQ0FBQzs7OztJQUVELDJDQUFXOzs7SUFBWDtJQUNBLENBQUM7Ozs7O0lBRUQsa0RBQWtCOzs7O0lBQWxCLFVBQW1CLFNBQVM7UUFDMUIsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3JCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNqRSxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLEVBQUU7b0JBQ3BFLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO2lCQUN4RTthQUNGO1lBRUQsSUFBSSxDQUFDLFNBQVMsRUFBRTtnQkFDZCxTQUFTLEdBQUc7b0JBQ1YsS0FBSyxFQUFFLEtBQUs7aUJBQ2IsQ0FBQzthQUNIO1lBRUQsSUFBSSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEdBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQztZQUM5RCxJQUFJLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQztZQUVyRSxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFO2dCQUMvQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQzthQUNqRDtTQUNGO0lBQ0gsQ0FBQzs7Ozs7SUFFRCwrQ0FBZTs7OztJQUFmLFVBQWdCLFNBQVM7UUFDdkIsSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNkLFNBQVMsR0FBRztnQkFDVixLQUFLLEVBQUUsS0FBSzthQUNiLENBQUM7U0FDSDtRQUVELElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNyQixJQUFJLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDO1lBQzlELElBQUksQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDO1lBRXJFLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUU7Z0JBQy9DLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO2FBQ2pEO1NBQ0Y7SUFDSCxDQUFDOzs7OztJQUVELDZDQUFhOzs7O0lBQWIsVUFBYyxXQUFXO1FBQ3ZCLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTs7Z0JBQ2pCLG9CQUFvQixHQUFHLElBQUk7WUFDL0IsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQzFELElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsS0FBSyxXQUFXLEVBQUU7b0JBQzdELG9CQUFvQixHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDckQsTUFBTTtpQkFDUDthQUNGO1lBRUQsZ0VBQWdFO1lBQ2hFLElBQUksb0JBQW9CLEVBQUU7Z0JBQ3hCLElBQUksQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztnQkFDbkQsSUFBSSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsb0JBQW9CLENBQUMsTUFBTSxDQUFDO2dCQUN4RSxJQUFJLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsR0FBRyxvQkFBb0IsQ0FBQyxXQUFXLENBQUM7Z0JBQy9FLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO2FBQ2pEO1NBQ0Y7SUFDSCxDQUFDOzs7OztJQUVELDJDQUFXOzs7O0lBQVgsVUFBWSxZQUEwQjtRQUNwQyxJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ3BELENBQUM7Ozs7O0lBRUQscURBQXFCOzs7O0lBQXJCLFVBQXNCLFlBQVk7UUFDaEMsSUFBSSxDQUFDLG1DQUFtQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUM5RCxDQUFDOzs7OztJQUVELGdEQUFnQjs7OztJQUFoQixVQUFpQixZQUEwQjtRQUN6QyxPQUFPLENBQUMsR0FBRyxDQUFDLGlDQUFpQyxHQUFHLFlBQVksQ0FBQyxPQUFPLEdBQUcsR0FBRyxHQUFHLFlBQVksQ0FBQyxvQkFBb0IsR0FBRyxHQUFHLEdBQUcsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3hJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDN0MsQ0FBQzs7Z0JBckhGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsZUFBZTtvQkFDekIsOHVRQUE2Qzs7aUJBRTlDOzs7OzsrQkFFRSxLQUFLOzRDQUNMLE1BQU07c0RBQ04sTUFBTTtxQ0FDTixNQUFNOztJQTZHVCw0QkFBQztDQUFBLEFBdEhELElBc0hDO1NBakhZLHFCQUFxQjs7O0lBQ2hDLDZDQUFvQzs7SUFDcEMsMERBQXVFOztJQUN2RSxvRUFBaUY7O0lBQ2pGLG1EQUFnRTs7SUFDaEUsc0RBQThDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIE9uQ2hhbmdlcywgT25EZXN0cm95LCBJbnB1dCwgU2ltcGxlQ2hhbmdlcywgRXZlbnRFbWl0dGVyLCBPdXRwdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgUXVlc3Rpb25MaW5lIH0gZnJvbSAnLi4vLi4vZG9tYWluL3F1ZXN0aW9uLWxpbmUnO1xuaW1wb3J0IHsgU2VsZWN0UXVlc3Rpb25PcHRpb24gfSBmcm9tICcuLi8uLi9kb21haW4vc2VsZWN0LXF1ZXN0aW9uLW9wdGlvbic7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3F1ZXN0aW9uLWxpbmUnLFxuICB0ZW1wbGF0ZVVybDogJy4vcXVlc3Rpb24tbGluZS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL3F1ZXN0aW9uLWxpbmUuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBRdWVzdGlvbkxpbmVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSwgT25DaGFuZ2VzIHtcbiAgQElucHV0KCkgcXVlc3Rpb25MaW5lOiBRdWVzdGlvbkxpbmU7XG4gIEBPdXRwdXQoKSBvblRha2VQaWN0dXJlUXVlc3Rpb25MaW5lID0gbmV3IEV2ZW50RW1pdHRlcjxRdWVzdGlvbkxpbmU+KCk7XG4gIEBPdXRwdXQoKSBvblRha2VTdXBwb3J0aW5nUGljdHVyZVF1ZXN0aW9uTGluZSA9IG5ldyBFdmVudEVtaXR0ZXI8UXVlc3Rpb25MaW5lPigpO1xuICBAT3V0cHV0KCkgb25RdWVzdGlvbkFuc3dlcmVkID0gbmV3IEV2ZW50RW1pdHRlcjxRdWVzdGlvbkxpbmU+KCk7XG4gIHNlbGVjdFF1ZXN0aW9uT3B0aW9uczogU2VsZWN0UXVlc3Rpb25PcHRpb25bXTtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xuICAgIGlmICh0aGlzLnF1ZXN0aW9uTGluZSAmJiB0aGlzLnF1ZXN0aW9uTGluZS5jaGlsZHJlbikge1xuICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLnF1ZXN0aW9uTGluZS5jaGlsZHJlbi5sZW5ndGg7IGkrKykge1xuICAgICAgICB0aGlzLnF1ZXN0aW9uTGluZS5jaGlsZHJlbltpXS5wYXJlbnQgPSB0aGlzLnF1ZXN0aW9uTGluZTtcbiAgICAgIH1cblxuICAgICAgdGhpcy5zZWxlY3RRdWVzdGlvbk9wdGlvbnMgPSBuZXcgQXJyYXk8U2VsZWN0UXVlc3Rpb25PcHRpb24+KCk7XG4gICAgICBpZiAodGhpcy5xdWVzdGlvbkxpbmUucGFyYW1ldGVycykge1xuICAgICAgICBsZXQgcm93cyA9IHRoaXMucXVlc3Rpb25MaW5lLnBhcmFtZXRlcnMuc3BsaXQoXCJ8XCIpO1xuICAgICAgICBsZXQgaW5kZXggPSAwO1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHJvd3MubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICBsZXQgcyA9IHJvd3NbaV0uc3BsaXQoXCI9XCIpO1xuICAgICAgICAgIGlmIChzLmxlbmd0aCA+PSAyKSB7XG4gICAgICAgICAgICBsZXQgc2VsZWN0UXVlc3Rpb25PcHRpb24gPSBuZXcgU2VsZWN0UXVlc3Rpb25PcHRpb24oKTtcbiAgICAgICAgICAgIHNlbGVjdFF1ZXN0aW9uT3B0aW9uLmRlc2NyaXB0aW9uID0gc1sxXTtcbiAgICAgICAgICAgIHNlbGVjdFF1ZXN0aW9uT3B0aW9uLndlaWdodCA9IE51bWJlcihzWzJdKTtcbiAgICAgICAgICAgIHNlbGVjdFF1ZXN0aW9uT3B0aW9uLmluZGV4ID0gaW5kZXg7XG4gICAgICAgICAgICB0aGlzLnNlbGVjdFF1ZXN0aW9uT3B0aW9ucy5wdXNoKHNlbGVjdFF1ZXN0aW9uT3B0aW9uKTtcbiAgICAgICAgICAgIGluZGV4Kys7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgbmdPbkRlc3Ryb3koKSB7XG4gIH1cblxuICByYWRpb0J1dHRvbkNoYW5nZWQoJHNlbGVjdGVkKSB7XG4gICAgaWYgKHRoaXMucXVlc3Rpb25MaW5lKSB7XG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMucXVlc3Rpb25MaW5lLnBhcmVudC5jaGlsZHJlbi5sZW5ndGg7IGkrKykge1xuICAgICAgICBpZiAodGhpcy5xdWVzdGlvbkxpbmUucGFyZW50LmNoaWxkcmVuW2ldLmlkICE9PSB0aGlzLnF1ZXN0aW9uTGluZS5pZCkge1xuICAgICAgICAgIHRoaXMucXVlc3Rpb25MaW5lLnBhcmVudC5jaGlsZHJlbltpXS5xdWVzdGlvblJlc3BvbnNlLnNlbGVjdGVkID0gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKCEkc2VsZWN0ZWQpIHtcbiAgICAgICAgJHNlbGVjdGVkID0ge1xuICAgICAgICAgIHZhbHVlOiBmYWxzZSxcbiAgICAgICAgfTtcbiAgICAgIH1cblxuICAgICAgdGhpcy5xdWVzdGlvbkxpbmUucXVlc3Rpb25SZXNwb25zZS5zZWxlY3RlZCA9ICRzZWxlY3RlZC52YWx1ZTtcbiAgICAgIHRoaXMucXVlc3Rpb25MaW5lLnF1ZXN0aW9uUmVzcG9uc2Uud2VpZ2h0ID0gdGhpcy5xdWVzdGlvbkxpbmUud2VpZ2h0O1xuXG4gICAgICBpZiAodGhpcy5xdWVzdGlvbkxpbmUucXVlc3Rpb25SZXNwb25zZS5zZWxlY3RlZCkge1xuICAgICAgICB0aGlzLm9uUXVlc3Rpb25BbnN3ZXJlZC5lbWl0KHRoaXMucXVlc3Rpb25MaW5lKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBjaGVja2JveENoYW5nZWQoJHNlbGVjdGVkKSB7XG4gICAgaWYgKCEkc2VsZWN0ZWQpIHtcbiAgICAgICRzZWxlY3RlZCA9IHtcbiAgICAgICAgdmFsdWU6IGZhbHNlLFxuICAgICAgfTtcbiAgICB9XG5cbiAgICBpZiAodGhpcy5xdWVzdGlvbkxpbmUpIHtcbiAgICAgIHRoaXMucXVlc3Rpb25MaW5lLnF1ZXN0aW9uUmVzcG9uc2Uuc2VsZWN0ZWQgPSAkc2VsZWN0ZWQudmFsdWU7XG4gICAgICB0aGlzLnF1ZXN0aW9uTGluZS5xdWVzdGlvblJlc3BvbnNlLndlaWdodCA9IHRoaXMucXVlc3Rpb25MaW5lLndlaWdodDtcblxuICAgICAgaWYgKHRoaXMucXVlc3Rpb25MaW5lLnF1ZXN0aW9uUmVzcG9uc2Uuc2VsZWN0ZWQpIHtcbiAgICAgICAgdGhpcy5vblF1ZXN0aW9uQW5zd2VyZWQuZW1pdCh0aGlzLnF1ZXN0aW9uTGluZSk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgc2VsZWN0Q2hhbmdlZChkZXNjcmlwdGlvbikge1xuICAgIGlmICh0aGlzLnF1ZXN0aW9uTGluZSkge1xuICAgICAgbGV0IHNlbGVjdFF1ZXN0aW9uT3B0aW9uID0gbnVsbDtcbiAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5zZWxlY3RRdWVzdGlvbk9wdGlvbnMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgaWYgKHRoaXMuc2VsZWN0UXVlc3Rpb25PcHRpb25zW2ldLmRlc2NyaXB0aW9uID09PSBkZXNjcmlwdGlvbikge1xuICAgICAgICAgIHNlbGVjdFF1ZXN0aW9uT3B0aW9uID0gdGhpcy5zZWxlY3RRdWVzdGlvbk9wdGlvbnNbaV07XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgLy8gbGV0IHNlbGVjdFF1ZXN0aW9uT3B0aW9uID0gdGhpcy5zZWxlY3RRdWVzdGlvbk9wdGlvbnNbaW5kZXhdO1xuICAgICAgaWYgKHNlbGVjdFF1ZXN0aW9uT3B0aW9uKSB7XG4gICAgICAgIHRoaXMucXVlc3Rpb25MaW5lLnF1ZXN0aW9uUmVzcG9uc2Uuc2VsZWN0ZWQgPSB0cnVlO1xuICAgICAgICB0aGlzLnF1ZXN0aW9uTGluZS5xdWVzdGlvblJlc3BvbnNlLndlaWdodCA9IHNlbGVjdFF1ZXN0aW9uT3B0aW9uLndlaWdodDtcbiAgICAgICAgdGhpcy5xdWVzdGlvbkxpbmUucXVlc3Rpb25SZXNwb25zZS5yZXNwb25zZSA9IHNlbGVjdFF1ZXN0aW9uT3B0aW9uLmRlc2NyaXB0aW9uO1xuICAgICAgICB0aGlzLm9uUXVlc3Rpb25BbnN3ZXJlZC5lbWl0KHRoaXMucXVlc3Rpb25MaW5lKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICB0YWtlUGljdHVyZShxdWVzdGlvbkxpbmU6IFF1ZXN0aW9uTGluZSkge1xuICAgIHRoaXMub25UYWtlUGljdHVyZVF1ZXN0aW9uTGluZS5lbWl0KHF1ZXN0aW9uTGluZSk7XG4gIH1cblxuICB0YWtlU3VwcG9ydGluZ1BpY3R1cmUocXVlc3Rpb25MaW5lKSB7XG4gICAgdGhpcy5vblRha2VTdXBwb3J0aW5nUGljdHVyZVF1ZXN0aW9uTGluZS5lbWl0KHF1ZXN0aW9uTGluZSk7XG4gIH1cblxuICBxdWVzdGlvbkFuc3dlcmVkKHF1ZXN0aW9uTGluZTogUXVlc3Rpb25MaW5lKSB7XG4gICAgY29uc29sZS5sb2coXCJxdWVzdGlvbkxpbmUucXVlc3Rpb25BbnN3ZXJlZDogXCIgKyBxdWVzdGlvbkxpbmUud29yZGluZyArIFwiIFwiICsgcXVlc3Rpb25MaW5lLmludGVncmF0aW9uSW5kaWNhdG9yICsgXCIgXCIgKyBxdWVzdGlvbkxpbmUuaWQpO1xuICAgIHRoaXMub25RdWVzdGlvbkFuc3dlcmVkLmVtaXQocXVlc3Rpb25MaW5lKTtcbiAgfVxufVxuIl19