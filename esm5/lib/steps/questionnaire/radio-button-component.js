/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
var RadioButtonComponent = /** @class */ (function () {
    function RadioButtonComponent() {
        this.selectionListener = new EventEmitter();
        this.selected = false;
    }
    /**
     * @return {?}
     */
    RadioButtonComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    RadioButtonComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    RadioButtonComponent.prototype.clicked = /**
     * @return {?}
     */
    function () {
        this.selected = !this.selected;
        this.selectionListener.emit({
            value: this.selected
        });
    };
    RadioButtonComponent.decorators = [
        { type: Component, args: [{
                    selector: 'radio-button-component',
                    template: "<div class=\"radio-button-component\" (click)=\"clicked()\">\n  <div class=\"value\">\n    <div class=\"radio-button\">\n      <div *ngIf=\"selected\" class=\"radio-button-selected\">\n      </div>\n    </div>\n  </div>\n  <div class=\"description\">\n    {{ description }}\n  </div>\n</div>\n",
                    styles: [".radio-button-component{display:table;margin:.5em 0;width:100%;cursor:pointer}.radio-button-component .value{display:table-cell;vertical-align:top;text-align:left;width:32px;padding:3px}.radio-button-component .value .radio-button{border:1px solid #58666c;width:14px;height:14px;padding:2px;border-radius:7px;box-sizing:border-box}.radio-button-component .value .radio-button .radio-button-selected{background-color:#58666c;width:8px;height:8px;border-radius:4px;box-sizing:border-box}.radio-button-component .description{display:table-cell;vertical-align:top;text-align:left;width:100%;font-size:15px;padding-left:5px;padding-top:3px;padding-right:5px;line-height:15px}"]
                }] }
    ];
    /** @nocollapse */
    RadioButtonComponent.ctorParameters = function () { return []; };
    RadioButtonComponent.propDecorators = {
        description: [{ type: Input }],
        selected: [{ type: Input }],
        selectionListener: [{ type: Output }]
    };
    return RadioButtonComponent;
}());
export { RadioButtonComponent };
if (false) {
    /** @type {?} */
    RadioButtonComponent.prototype.description;
    /** @type {?} */
    RadioButtonComponent.prototype.selected;
    /** @type {?} */
    RadioButtonComponent.prototype.selectionListener;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmFkaW8tYnV0dG9uLWNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvc3RlcHMvcXVlc3Rpb25uYWlyZS9yYWRpby1idXR0b24tY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQXFCLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUxRjtJQVVFO1FBRlUsc0JBQWlCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUcvQyxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztJQUN4QixDQUFDOzs7O0lBRUQsdUNBQVE7OztJQUFSO0lBQ0EsQ0FBQzs7OztJQUVELDBDQUFXOzs7SUFBWDtJQUNBLENBQUM7Ozs7SUFFRCxzQ0FBTzs7O0lBQVA7UUFDRSxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUMvQixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDO1lBQzFCLEtBQUssRUFBRSxJQUFJLENBQUMsUUFBUTtTQUNyQixDQUFDLENBQUM7SUFDTCxDQUFDOztnQkF6QkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSx3QkFBd0I7b0JBQ2xDLGlUQUE0Qzs7aUJBRTdDOzs7Ozs4QkFFRSxLQUFLOzJCQUNMLEtBQUs7b0NBQ0wsTUFBTTs7SUFrQlQsMkJBQUM7Q0FBQSxBQTFCRCxJQTBCQztTQXJCWSxvQkFBb0I7OztJQUMvQiwyQ0FBNkI7O0lBQzdCLHdDQUEyQjs7SUFDM0IsaURBQWlEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPbkluaXQsIE9uRGVzdHJveSwgT3V0cHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3JhZGlvLWJ1dHRvbi1jb21wb25lbnQnLFxuICB0ZW1wbGF0ZVVybDogJy4vcmFkaW8tYnV0dG9uLWNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vcmFkaW8tYnV0dG9uLWNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgUmFkaW9CdXR0b25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIGRlc2NyaXB0aW9uOiBzdHJpbmc7XG4gIEBJbnB1dCgpIHNlbGVjdGVkOiBib29sZWFuO1xuICBAT3V0cHV0KCkgc2VsZWN0aW9uTGlzdGVuZXIgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5zZWxlY3RlZCA9IGZhbHNlO1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpIHtcbiAgfVxuXG4gIGNsaWNrZWQoKSB7XG4gICAgdGhpcy5zZWxlY3RlZCA9ICF0aGlzLnNlbGVjdGVkO1xuICAgIHRoaXMuc2VsZWN0aW9uTGlzdGVuZXIuZW1pdCh7XG4gICAgICB2YWx1ZTogdGhpcy5zZWxlY3RlZFxuICAgIH0pO1xuICB9XG59XG4iXX0=