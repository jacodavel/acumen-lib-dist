/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { ActorService } from '../../services/actor.service';
import { MaintenanceService } from '../../services/maintenance.service';
import { StepInstance } from '../../domain/step-instance';
import { Actor } from '../../domain/actor';
var UpdateDetailsComponent = /** @class */ (function () {
    function UpdateDetailsComponent(projectService, maintenanceService, actorService) {
        this.projectService = projectService;
        this.maintenanceService = maintenanceService;
        this.actorService = actorService;
        this.stepCompletedListener = new EventEmitter();
        this.cancelListener = new EventEmitter();
        this.actorFieldList = [];
        this.actorFields = [];
        this.actorFieldOptionsMap = {};
        this.actorFieldValues = {};
        this.actorFieldErrors = {};
        this.updateError = false;
        this.firstnameError = false;
        this.surnameError = false;
        this.idNrError = false;
        this.celNrError = false;
        this.emailError = false;
        this.emailConfirmationError = false;
        this.actor = new Actor();
        this.loading = false;
    }
    /**
     * @return {?}
     */
    UpdateDetailsComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.loading = true;
        this.projectService.getStepParameters(this.stepInstance.step.id).then((/**
         * @param {?} result
         * @return {?}
         */
        function (result) {
            // this.requiresEmail = result[0] && "true" === result[0];
            _this.requiresFirstname = result["6"] && "true" === result["6"];
            _this.requiresSurname = result["7"] && "true" === result["7"];
            _this.requiresCelNr = result["8"] && "true" === result["8"];
            _this.requiresEmail = result["0"] && "true" === result["0"];
            _this.requiresEmployer = result["1"] && "true" === result["1"];
            if (result["9"]) {
                _this.idNrLabel = result["9"];
            }
            else {
                _this.idNrLabel = "Id nr";
            }
            // alert(this.stepInstance.actor.idNr + " " + this.stepInstance.businessProcessInstance.actor.idNr);
            _this.actor = _this.stepInstance.businessProcessInstance.actor;
            if (_this.actor.email) {
                _this.emailConfirmation = _this.actor.email;
            }
            if (_this.actor.actorType) {
                _this.maintenanceService.getActorFields(_this.actor.actorType.id).then((/**
                 * @param {?} actorFields
                 * @return {?}
                 */
                function (actorFields) {
                    _this.actorFieldList = actorFields;
                    if (actorFields) {
                        var _loop_1 = function (i) {
                            console.log("trace 9 -> " + i + ", " + actorFields[i].id);
                            _this.actorFieldValues[actorFields[i].id] = "";
                            _this.actorFieldErrors[actorFields[i].id] = false;
                            if (i % 2 === 0) {
                                console.log("trace 10");
                                /** @type {?} */
                                var actorFieldRow = {};
                                actorFieldRow[0] = actorFields[i];
                                console.log("trace 11");
                                if (i < actorFields.length - 1) {
                                    console.log("trace 12");
                                    actorFieldRow[1] = actorFields[i + 1];
                                }
                                _this.actorFields.push(actorFieldRow);
                            }
                            if (actorFields[i].actorFieldType === "SELECT") {
                                console.log("trace 13");
                                _this.maintenanceService.getActorFieldOptions(actorFields[i].id).then((/**
                                 * @param {?} actorFieldOptions
                                 * @return {?}
                                 */
                                function (actorFieldOptions) {
                                    console.log("trace 14");
                                    if (actorFieldOptions) {
                                        console.log("trace 15");
                                        _this.actorFieldOptionsMap[actorFields[i].id] = actorFieldOptions;
                                    }
                                }));
                            }
                        };
                        for (var i = 0; i < actorFields.length; i++) {
                            _loop_1(i);
                        }
                    }
                }));
                _this.actorService.getActorFieldValues(_this.actor.id).then((/**
                 * @param {?} actorFieldValues
                 * @return {?}
                 */
                function (actorFieldValues) {
                    var e_1, _a;
                    try {
                        for (var actorFieldValues_1 = tslib_1.__values(actorFieldValues), actorFieldValues_1_1 = actorFieldValues_1.next(); !actorFieldValues_1_1.done; actorFieldValues_1_1 = actorFieldValues_1.next()) {
                            var actorFieldValue = actorFieldValues_1_1.value;
                            _this.actorFieldValues[actorFieldValue.actorFieldId] = actorFieldValue.fieldValue;
                        }
                    }
                    catch (e_1_1) { e_1 = { error: e_1_1 }; }
                    finally {
                        try {
                            if (actorFieldValues_1_1 && !actorFieldValues_1_1.done && (_a = actorFieldValues_1.return)) _a.call(actorFieldValues_1);
                        }
                        finally { if (e_1) throw e_1.error; }
                    }
                    _this.loading = false;
                }));
            }
            else {
                _this.loading = false;
            }
        }));
        // console.log("trace 6");
        // this.projectService.getProcessVariableValue(this.stepInstance.businessProcessInstance.id, "idNr").then(result => {
        //   console.log("trace 7");
        //   if (result) {
        //     console.log("trace 8");
        //     this.actor.idNr = result;
        //   }
        //
        //   this.loading = false;
        // });
    };
    /**
     * @return {?}
     */
    UpdateDetailsComponent.prototype.completeStep = /**
     * @return {?}
     */
    function () {
        var _this = this;
        var e_2, _a;
        this.firstnameError = false;
        this.surnameError = false;
        this.idNrError = false;
        this.celNrError = false;
        this.emailError = false;
        this.emailConfirmationError = false;
        // requiresFirstname: boolean;
        // requiresSurname: boolean;
        // requiresCelNr: boolean;
        // requiresEmployer: boolean;
        if (this.requiresFirstname) {
            if (!this.actor.firstname) {
                this.firstnameError = true;
                return;
            }
        }
        else {
            this.actor.firstname = this.actor.idNr;
        }
        if (this.requiresSurname) {
            if (!this.actor.surname) {
                this.surnameError = true;
                return;
            }
        }
        else {
            this.actor.surname = this.actor.idNr;
        }
        if (!this.actor.idNr) {
            this.idNrError = true;
            return;
        }
        if (this.requiresCelNr && !this.actor.celNr) {
            this.celNrError = true;
            return;
        }
        if (!this.actor.email) {
            this.emailError = true;
            return;
        }
        if (this.actor.email !== this.emailConfirmation) {
            this.emailConfirmationError = true;
            return;
        }
        this.actorFieldErrors = {};
        /** @type {?} */
        var errorFound = false;
        try {
            for (var _b = tslib_1.__values(this.actorFieldList), _c = _b.next(); !_c.done; _c = _b.next()) {
                var actorField = _c.value;
                /** @type {?} */
                var fieldValue = this.actorFieldValues[actorField.id];
                if (actorField.required === true && (!fieldValue || fieldValue === "")) {
                    this.actorFieldErrors[actorField.id] = true;
                    errorFound = true;
                }
                else {
                    this.actorFieldErrors[actorField.id] = false;
                }
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_2) throw e_2.error; }
        }
        if (errorFound) {
            return;
        }
        this.updateError = false;
        this.loading = true;
        this.projectService.updateActor(this.actor).then((/**
         * @param {?} updateResult
         * @return {?}
         */
        function (updateResult) {
            var e_3, _a;
            if (updateResult) {
                /** @type {?} */
                var actorFieldValues = [];
                try {
                    for (var _b = tslib_1.__values(Object.keys(_this.actorFieldValues)), _c = _b.next(); !_c.done; _c = _b.next()) {
                        var actorFieldId = _c.value;
                        actorFieldValues.push({
                            actorFieldId: Number(actorFieldId),
                            fieldValue: _this.actorFieldValues[actorFieldId]
                        });
                    }
                }
                catch (e_3_1) { e_3 = { error: e_3_1 }; }
                finally {
                    try {
                        if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                    }
                    finally { if (e_3) throw e_3.error; }
                }
                _this.actorService.updateActorFieldValues(_this.actor.id, actorFieldValues).then((/**
                 * @param {?} updateResult2
                 * @return {?}
                 */
                function (updateResult2) {
                    if (updateResult2) {
                        _this.projectService.completeStep(_this.stepInstance.id, _this.userId).then((/**
                         * @param {?} result
                         * @return {?}
                         */
                        function (result) {
                            _this.loading = false;
                            _this.stepCompletedListener.emit({
                                value: _this.stepInstance
                            });
                        }));
                    }
                    else {
                        _this.updateError = true;
                        _this.loading = false;
                    }
                }));
            }
            else {
                _this.updateError = true;
                _this.loading = false;
            }
        }));
    };
    /**
     * @return {?}
     */
    UpdateDetailsComponent.prototype.cancel = /**
     * @return {?}
     */
    function () {
        this.cancelListener.emit({});
    };
    UpdateDetailsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'update-details',
                    template: "<div class=\"update-details\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div class=\"heading\">\n    Update details\n  </div>\n\n  <div *ngIf=\"!loading && updateError\" class=\"content\">\n    <div class=\"info-message\">\n      Update failed\n    </div>\n  </div>\n\n  <div *ngIf=\"!loading && !updateError\" class=\"content\">\n    <div *ngIf=\"requiresFirstname || requiresSurname\" class=\"component-row\">\n      <div *ngIf=\"requiresFirstname\" class=\"component-col\">\n        <div class=\"component-label\">\n          Firstname\n          <div *ngIf=\"firstnameError\" class=\"error-message\">A firstname must be entered</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"text\" [(ngModel)]=\"actor.firstname\">\n        </div>\n      </div>\n      <div *ngIf=\"requiresSurname\" class=\"component-col\">\n        <div class=\"component-label\">\n          Surname\n          <div *ngIf=\"surnameError\" class=\"error-message\">A surname must be entered</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"text\" [(ngModel)]=\"actor.surname\">\n        </div>\n      </div>\n    </div>\n\n    <div class=\"component-row\">\n      <div class=\"component-col\">\n        <div class=\"component-label\">\n          {{ idNrLabel }}\n          <div *ngIf=\"idNrError\" class=\"error-message\">An {{ idNrLabel }} must be entered</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"text\" [(ngModel)]=\"actor.idNr\">\n        </div>\n      </div>\n      <div *ngIf=\"requiresCelNr\" class=\"component-col\">\n        <div class=\"component-label\">\n          Cel nr\n          <div *ngIf=\"celNrError\" class=\"error-message\">A cel nr must be entered</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"text\" [(ngModel)]=\"actor.celNr\">\n        </div>\n      </div>\n    </div>\n\n    <div class=\"component-row\">\n      <div class=\"component-col\">\n        <div class=\"component-label\">\n          E-mail address\n          <div *ngIf=\"emailError\" class=\"error-message\">An email address must be entered</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"email\" [(ngModel)]=\"actor.email\">\n        </div>\n      </div>\n      <div class=\"component-col\">\n        <div class=\"component-label\">\n          Confirm e-mail address\n          <div *ngIf=\"emailConfirmationError\" class=\"error-message\">The email addresses entered are not the same</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"email\" [(ngModel)]=\"emailConfirmation\">\n        </div>\n      </div>\n    </div>\n\n    <div *ngFor=\"let actorFieldRow of actorFields; let i = index\" class=\"component-row\">\n      <div class=\"component-col\">\n        <div class=\"component-label\">\n          {{ actorFieldRow[0].description }}\n          <div *ngIf=\"actorFieldErrors[actorFieldRow[0].id]\" class=\"error-message\">\n            A value must be entered for {{ actorFieldRow[0].description }}\n          </div>\n        </div>\n        <div class=\"component\">\n          <input *ngIf=\"actorFieldRow[0].actorFieldType === 'TEXT'\" type=\"text\" [(ngModel)]=\"actorFieldValues[actorFieldRow[0].id]\">\n          <select *ngIf=\"actorFieldRow[0].actorFieldType === 'SELECT'\" [(ngModel)]=\"actorFieldValues[actorFieldRow[0].id]\">\n            <option *ngFor=\"let actorFieldOption of actorFieldOptionsMap[actorFieldRow[0].id]\" [value]=\"actorFieldOption.fieldValue\">\n              {{ actorFieldOption.description }}\n            </option>\n          </select>\n        </div>\n      </div>\n      <div *ngIf=\"actorFieldRow[1]\" class=\"component-col\">\n        <div class=\"component-label\">\n          {{ actorFieldRow[1].description }}\n          <div *ngIf=\"actorFieldErrors[actorFieldRow[1].id]\" class=\"error-message\">\n            A value must be entered for {{ actorFieldRow[1].description }}\n          </div>\n        </div>\n        <div class=\"component\">\n          <input *ngIf=\"actorFieldRow[1].actorFieldType === 'TEXT'\" type=\"text\" [(ngModel)]=\"actorFieldValues[actorFieldRow[1].id]\">\n          <select *ngIf=\"actorFieldRow[1].actorFieldType === 'SELECT'\" [(ngModel)]=\"actorFieldValues[actorFieldRow[1].id]\">\n            <option *ngFor=\"let actorFieldOption of actorFieldOptionsMap[actorFieldRow[1].id]\" [value]=\"actorFieldOption.fieldValue\">\n              {{ actorFieldOption.description }}\n            </option>\n          </select>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!loading\" class=\"button-section\">\n    <div class=\"button\" (click)=\"completeStep()\">\n      <div class=\"button-text\">Submit</div>\n    </div>\n    <div class=\"button button-outline\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n  <div *ngIf=\"loading\" class=\"button-section\">\n    <div class=\"button button-disabled\">\n      <div class=\"button-text\">Submit</div>\n    </div>\n    <div class=\"button button-outline-disabled\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                    styles: [".update-details{width:100%;height:100%;position:relative}.update-details .heading{position:absolute;top:0;width:100%;height:32px;font-size:16px;padding:8px}.update-details .content{position:absolute;top:32px;left:0;bottom:50px;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px;box-sizing:border-box}.update-details .content .info-message{font-size:15px}.update-details .content .component-row{width:100%}.update-details .content .component-row .component-col{width:100%;display:inline-block;box-sizing:border-box;padding-top:10px}.update-details .content .component-row .component-col .component-label{width:100%;font-size:15px;padding-bottom:5px}.update-details .content .component-row .component-col .error-message{font-style:italic;font-size:11px;color:#f04141;padding-bottom:5px}.update-details .content .component-row .component-col .component{width:100%}.update-details .content .component-row .component-col .component input,.update-details .content .component-row .component-col .component select{width:100%;font-size:13px;border:1px solid #58666c;background-color:#fffcf6;box-sizing:border-box;padding:3px 5px;border-radius:5px;height:30px}.update-details .content .component-row:first-child .component-col:first-child{padding-top:0}@media (min-width:600px){.update-details .content .component-row .component-col{width:50%}.update-details .content .component-row .component-col:first-child{padding-right:16px}.update-details .content .component-row:first-child .component-col{padding-top:0}}.update-details .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.update-details .button-section .button{width:120px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.update-details .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.update-details .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.update-details .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b;cursor:default}.update-details .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b;cursor:default}"]
                }] }
    ];
    /** @nocollapse */
    UpdateDetailsComponent.ctorParameters = function () { return [
        { type: ProjectService },
        { type: MaintenanceService },
        { type: ActorService }
    ]; };
    UpdateDetailsComponent.propDecorators = {
        stepInstance: [{ type: Input }],
        userId: [{ type: Input }],
        stepCompletedListener: [{ type: Output }],
        cancelListener: [{ type: Output }]
    };
    return UpdateDetailsComponent;
}());
export { UpdateDetailsComponent };
if (false) {
    /** @type {?} */
    UpdateDetailsComponent.prototype.stepInstance;
    /** @type {?} */
    UpdateDetailsComponent.prototype.userId;
    /** @type {?} */
    UpdateDetailsComponent.prototype.stepCompletedListener;
    /** @type {?} */
    UpdateDetailsComponent.prototype.cancelListener;
    /** @type {?} */
    UpdateDetailsComponent.prototype.actor;
    /** @type {?} */
    UpdateDetailsComponent.prototype.actorFieldList;
    /** @type {?} */
    UpdateDetailsComponent.prototype.actorFields;
    /** @type {?} */
    UpdateDetailsComponent.prototype.actorFieldOptionsMap;
    /** @type {?} */
    UpdateDetailsComponent.prototype.actorFieldValues;
    /** @type {?} */
    UpdateDetailsComponent.prototype.actorFieldErrors;
    /** @type {?} */
    UpdateDetailsComponent.prototype.emailConfirmation;
    /** @type {?} */
    UpdateDetailsComponent.prototype.loading;
    /** @type {?} */
    UpdateDetailsComponent.prototype.requiresEmail;
    /** @type {?} */
    UpdateDetailsComponent.prototype.requiresFirstname;
    /** @type {?} */
    UpdateDetailsComponent.prototype.requiresSurname;
    /** @type {?} */
    UpdateDetailsComponent.prototype.requiresCelNr;
    /** @type {?} */
    UpdateDetailsComponent.prototype.requiresEmployer;
    /** @type {?} */
    UpdateDetailsComponent.prototype.idNrLabel;
    /** @type {?} */
    UpdateDetailsComponent.prototype.updateError;
    /** @type {?} */
    UpdateDetailsComponent.prototype.firstnameError;
    /** @type {?} */
    UpdateDetailsComponent.prototype.surnameError;
    /** @type {?} */
    UpdateDetailsComponent.prototype.idNrError;
    /** @type {?} */
    UpdateDetailsComponent.prototype.celNrError;
    /** @type {?} */
    UpdateDetailsComponent.prototype.emailError;
    /** @type {?} */
    UpdateDetailsComponent.prototype.emailConfirmationError;
    /**
     * @type {?}
     * @private
     */
    UpdateDetailsComponent.prototype.projectService;
    /**
     * @type {?}
     * @private
     */
    UpdateDetailsComponent.prototype.maintenanceService;
    /**
     * @type {?}
     * @private
     */
    UpdateDetailsComponent.prototype.actorService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBkYXRlLWRldGFpbHMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWN1bWVuLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9zdGVwcy91cGRhdGUtZGV0YWlscy91cGRhdGUtZGV0YWlscy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQVUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRS9FLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNoRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDNUQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDeEUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQzFELE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQU0zQztJQWtDRSxnQ0FBb0IsY0FBOEIsRUFBVSxrQkFBc0MsRUFDdEYsWUFBMEI7UUFEbEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQVUsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0RixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQTNCNUIsMEJBQXFCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUMzQyxtQkFBYyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFHOUMsbUJBQWMsR0FBc0IsRUFBRSxDQUFDO1FBQ3ZDLGdCQUFXLEdBQWUsRUFBRSxDQUFDO1FBQzdCLHlCQUFvQixHQUFHLEVBQUUsQ0FBQztRQUMxQixxQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFDdEIscUJBQWdCLEdBQUcsRUFBRSxDQUFDO1FBVXRCLGdCQUFXLEdBQVksS0FBSyxDQUFDO1FBQzdCLG1CQUFjLEdBQVksS0FBSyxDQUFDO1FBQ2hDLGlCQUFZLEdBQVksS0FBSyxDQUFDO1FBQzlCLGNBQVMsR0FBWSxLQUFLLENBQUM7UUFDM0IsZUFBVSxHQUFZLEtBQUssQ0FBQztRQUM1QixlQUFVLEdBQVksS0FBSyxDQUFDO1FBQzVCLDJCQUFzQixHQUFZLEtBQUssQ0FBQztRQUl0QyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksS0FBSyxFQUFFLENBQUM7UUFDekIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7SUFDdkIsQ0FBQzs7OztJQUVELHlDQUFROzs7SUFBUjtRQUFBLGlCQStFQztRQTlFQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNwQixJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxVQUFBLE1BQU07WUFDMUUsMERBQTBEO1lBQzFELEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUksTUFBTSxLQUFLLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUMvRCxLQUFJLENBQUMsZUFBZSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxNQUFNLEtBQUssTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzdELEtBQUksQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxJQUFJLE1BQU0sS0FBSyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDM0QsS0FBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUksTUFBTSxLQUFLLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUMzRCxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxJQUFJLE1BQU0sS0FBSyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFOUQsSUFBSSxNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQ2YsS0FBSSxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDOUI7aUJBQU07Z0JBQ0wsS0FBSSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUM7YUFDMUI7WUFFRCxvR0FBb0c7WUFDcEcsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLHVCQUF1QixDQUFDLEtBQUssQ0FBQztZQUM3RCxJQUFJLEtBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFO2dCQUNwQixLQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7YUFDM0M7WUFFRCxJQUFJLEtBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxFQUFFO2dCQUN4QixLQUFJLENBQUMsa0JBQWtCLENBQUMsY0FBYyxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUk7Ozs7Z0JBQUMsVUFBQSxXQUFXO29CQUM5RSxLQUFJLENBQUMsY0FBYyxHQUFHLFdBQVcsQ0FBQztvQkFDbEMsSUFBSSxXQUFXLEVBQUU7Z0RBQ04sQ0FBQzs0QkFDUixPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsR0FBRyxDQUFDLEdBQUcsSUFBSSxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQzs0QkFDMUQsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUM7NEJBQzlDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDOzRCQUNqRCxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxFQUFFO2dDQUNmLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7O29DQUNwQixhQUFhLEdBQUcsRUFBRTtnQ0FDdEIsYUFBYSxDQUFDLENBQUMsQ0FBQyxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQ0FDbEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQ0FDeEIsSUFBSSxDQUFDLEdBQUcsV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0NBQzlCLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7b0NBQ3hCLGFBQWEsQ0FBQyxDQUFDLENBQUMsR0FBRyxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2lDQUN2QztnQ0FFRCxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQzs2QkFDdEM7NEJBRUQsSUFBSSxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBYyxLQUFLLFFBQVEsRUFBRTtnQ0FDOUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQ0FDeEIsS0FBSSxDQUFDLGtCQUFrQixDQUFDLG9CQUFvQixDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJOzs7O2dDQUFDLFVBQUEsaUJBQWlCO29DQUNwRixPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO29DQUN4QixJQUFJLGlCQUFpQixFQUFFO3dDQUNyQixPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO3dDQUN4QixLQUFJLENBQUMsb0JBQW9CLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLGlCQUFpQixDQUFDO3FDQUNsRTtnQ0FDSCxDQUFDLEVBQUMsQ0FBQzs2QkFDSjs7d0JBMUJILEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRTtvQ0FBbEMsQ0FBQzt5QkEyQlQ7cUJBQ0Y7Z0JBQ0gsQ0FBQyxFQUFDLENBQUM7Z0JBRUgsS0FBSSxDQUFDLFlBQVksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUk7Ozs7Z0JBQUMsVUFBQSxnQkFBZ0I7Ozt3QkFDeEUsS0FBNEIsSUFBQSxxQkFBQSxpQkFBQSxnQkFBZ0IsQ0FBQSxrREFBQSxnRkFBRTs0QkFBekMsSUFBSSxlQUFlLDZCQUFBOzRCQUN0QixLQUFJLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxHQUFHLGVBQWUsQ0FBQyxVQUFVLENBQUM7eUJBQ2xGOzs7Ozs7Ozs7b0JBRUQsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7Z0JBQ3ZCLENBQUMsRUFBQyxDQUFDO2FBQ0o7aUJBQU07Z0JBQ0wsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7YUFDdEI7UUFDSCxDQUFDLEVBQUMsQ0FBQztRQUVILDBCQUEwQjtRQUMxQixxSEFBcUg7UUFDckgsNEJBQTRCO1FBQzVCLGtCQUFrQjtRQUNsQiw4QkFBOEI7UUFDOUIsZ0NBQWdDO1FBQ2hDLE1BQU07UUFDTixFQUFFO1FBQ0YsMEJBQTBCO1FBQzFCLE1BQU07SUFDUixDQUFDOzs7O0lBRUQsNkNBQVk7OztJQUFaO1FBQUEsaUJBaUdDOztRQWhHQyxJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztRQUM1QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUN2QixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztRQUN4QixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztRQUN4QixJQUFJLENBQUMsc0JBQXNCLEdBQUcsS0FBSyxDQUFDO1FBRXBDLDhCQUE4QjtRQUM5Qiw0QkFBNEI7UUFDNUIsMEJBQTBCO1FBQzFCLDZCQUE2QjtRQUU3QixJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtZQUMxQixJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUU7Z0JBQ3pCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO2dCQUMzQixPQUFPO2FBQ1I7U0FDRjthQUFNO1lBQ0wsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7U0FDeEM7UUFFRCxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDeEIsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFO2dCQUN2QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztnQkFDekIsT0FBTzthQUNSO1NBQ0Y7YUFBTTtZQUNMLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO1NBQ3RDO1FBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFO1lBQ3BCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1lBQ3RCLE9BQU87U0FDUjtRQUVELElBQUksSUFBSSxDQUFDLGFBQWEsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFO1lBQzNDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBQ3ZCLE9BQU87U0FDUjtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRTtZQUNyQixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztZQUN2QixPQUFPO1NBQ1I7UUFFRCxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtZQUMvQyxJQUFJLENBQUMsc0JBQXNCLEdBQUcsSUFBSSxDQUFDO1lBQ25DLE9BQU87U0FDUjtRQUVELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7O1lBQ3ZCLFVBQVUsR0FBRyxLQUFLOztZQUN0QixLQUF5QixJQUFBLEtBQUEsaUJBQUEsSUFBSSxDQUFDLGNBQWMsQ0FBQSxnQkFBQSw0QkFBRTtnQkFBekMsSUFBTSxVQUFVLFdBQUE7O29CQUNmLFVBQVUsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQztnQkFDckQsSUFBSSxVQUFVLENBQUMsUUFBUSxLQUFLLElBQUksSUFBSSxDQUFDLENBQUMsVUFBVSxJQUFJLFVBQVUsS0FBSyxFQUFFLENBQUMsRUFBRTtvQkFDdEUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUM7b0JBQzVDLFVBQVUsR0FBRyxJQUFJLENBQUM7aUJBQ25CO3FCQUFNO29CQUNMLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDO2lCQUM5QzthQUNGOzs7Ozs7Ozs7UUFFRCxJQUFJLFVBQVUsRUFBRTtZQUNkLE9BQU87U0FDUjtRQUVELElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJOzs7O1FBQUMsVUFBQSxZQUFZOztZQUMzRCxJQUFJLFlBQVksRUFBRTs7b0JBQ1osZ0JBQWdCLEdBQUcsRUFBRTs7b0JBQ3pCLEtBQTJCLElBQUEsS0FBQSxpQkFBQSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFBLGdCQUFBLDRCQUFFO3dCQUExRCxJQUFNLFlBQVksV0FBQTt3QkFDckIsZ0JBQWdCLENBQUMsSUFBSSxDQUFDOzRCQUNwQixZQUFZLEVBQUUsTUFBTSxDQUFDLFlBQVksQ0FBQzs0QkFDbEMsVUFBVSxFQUFFLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUM7eUJBQ2hELENBQUMsQ0FBQztxQkFDSjs7Ozs7Ozs7O2dCQUVELEtBQUksQ0FBQyxZQUFZLENBQUMsc0JBQXNCLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQyxJQUFJOzs7O2dCQUFDLFVBQUEsYUFBYTtvQkFDMUYsSUFBSSxhQUFhLEVBQUU7d0JBQ2pCLEtBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxFQUFFLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJOzs7O3dCQUFDLFVBQUEsTUFBTTs0QkFDN0UsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7NEJBQ3JCLEtBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUM7Z0NBQzlCLEtBQUssRUFBRSxLQUFJLENBQUMsWUFBWTs2QkFDekIsQ0FBQyxDQUFDO3dCQUNMLENBQUMsRUFBQyxDQUFDO3FCQUNKO3lCQUFNO3dCQUNMLEtBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO3dCQUN4QixLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztxQkFDdEI7Z0JBQ0gsQ0FBQyxFQUFDLENBQUM7YUFDSjtpQkFBTTtnQkFDTCxLQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztnQkFDeEIsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7YUFDdEI7UUFDSCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7SUFFRCx1Q0FBTTs7O0lBQU47UUFDRSxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxFQUN4QixDQUFDLENBQUM7SUFDTCxDQUFDOztnQkEvTkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxnQkFBZ0I7b0JBQzFCLHFuS0FBOEM7O2lCQUUvQzs7OztnQkFkUSxjQUFjO2dCQUVkLGtCQUFrQjtnQkFEbEIsWUFBWTs7OytCQWVsQixLQUFLO3lCQUNMLEtBQUs7d0NBQ0wsTUFBTTtpQ0FDTixNQUFNOztJQXVOVCw2QkFBQztDQUFBLEFBaE9ELElBZ09DO1NBM05ZLHNCQUFzQjs7O0lBQ2pDLDhDQUFvQzs7SUFDcEMsd0NBQXdCOztJQUN4Qix1REFBcUQ7O0lBQ3JELGdEQUE4Qzs7SUFFOUMsdUNBQWE7O0lBQ2IsZ0RBQXVDOztJQUN2Qyw2Q0FBNkI7O0lBQzdCLHNEQUEwQjs7SUFDMUIsa0RBQXNCOztJQUN0QixrREFBc0I7O0lBQ3RCLG1EQUEwQjs7SUFDMUIseUNBQWlCOztJQUNqQiwrQ0FBdUI7O0lBQ3ZCLG1EQUEyQjs7SUFDM0IsaURBQXlCOztJQUN6QiwrQ0FBdUI7O0lBQ3ZCLGtEQUEwQjs7SUFDMUIsMkNBQWtCOztJQUVsQiw2Q0FBNkI7O0lBQzdCLGdEQUFnQzs7SUFDaEMsOENBQThCOztJQUM5QiwyQ0FBMkI7O0lBQzNCLDRDQUE0Qjs7SUFDNUIsNENBQTRCOztJQUM1Qix3REFBd0M7Ozs7O0lBRTVCLGdEQUFzQzs7Ozs7SUFBRSxvREFBOEM7Ozs7O0lBQzlGLDhDQUFrQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPdXRwdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgUHJvamVjdFNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9wcm9qZWN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgQWN0b3JTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvYWN0b3Iuc2VydmljZSc7XG5pbXBvcnQgeyBNYWludGVuYW5jZVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9tYWludGVuYW5jZS5zZXJ2aWNlJztcbmltcG9ydCB7IFN0ZXBJbnN0YW5jZSB9IGZyb20gJy4uLy4uL2RvbWFpbi9zdGVwLWluc3RhbmNlJztcbmltcG9ydCB7IEFjdG9yIH0gZnJvbSAnLi4vLi4vZG9tYWluL2FjdG9yJztcbmltcG9ydCB7IFNlbGVjdGlvbk9wdGlvbiB9IGZyb20gJy4uLy4uL2RvbWFpbi9zZWxlY3Rpb24tb3B0aW9uJztcbmltcG9ydCB7IEFjdG9yRmllbGQgfSBmcm9tICcuLi8uLi9kb21haW4vYWN0b3ItZmllbGQnO1xuaW1wb3J0IHsgQWN0b3JGaWVsZE9wdGlvbiB9IGZyb20gJy4uLy4uL2RvbWFpbi9hY3Rvci1maWVsZC1vcHRpb24nO1xuaW1wb3J0IHsgQWN0b3JGaWVsZFZhbHVlIH0gZnJvbSAnLi4vLi4vZG9tYWluL2FjdG9yLWZpZWxkLXZhbHVlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAndXBkYXRlLWRldGFpbHMnLFxuICB0ZW1wbGF0ZVVybDogJy4vdXBkYXRlLWRldGFpbHMuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi91cGRhdGUtZGV0YWlscy5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIFVwZGF0ZURldGFpbHNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBASW5wdXQoKSBzdGVwSW5zdGFuY2U6IFN0ZXBJbnN0YW5jZTtcbiAgQElucHV0KCkgdXNlcklkOiBudW1iZXI7XG4gIEBPdXRwdXQoKSBzdGVwQ29tcGxldGVkTGlzdGVuZXIgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBjYW5jZWxMaXN0ZW5lciA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICBhY3RvcjogQWN0b3I7XG4gIGFjdG9yRmllbGRMaXN0OiBBcnJheTxBY3RvckZpZWxkPiA9IFtdO1xuICBhY3RvckZpZWxkczogQXJyYXk8YW55PiA9IFtdO1xuICBhY3RvckZpZWxkT3B0aW9uc01hcCA9IHt9O1xuICBhY3RvckZpZWxkVmFsdWVzID0ge307XG4gIGFjdG9yRmllbGRFcnJvcnMgPSB7fTtcbiAgZW1haWxDb25maXJtYXRpb246IHN0cmluZztcbiAgbG9hZGluZzogYm9vbGVhbjtcbiAgcmVxdWlyZXNFbWFpbDogYm9vbGVhbjtcbiAgcmVxdWlyZXNGaXJzdG5hbWU6IGJvb2xlYW47XG4gIHJlcXVpcmVzU3VybmFtZTogYm9vbGVhbjtcbiAgcmVxdWlyZXNDZWxOcjogYm9vbGVhbjtcbiAgcmVxdWlyZXNFbXBsb3llcjogYm9vbGVhbjtcbiAgaWROckxhYmVsOiBzdHJpbmc7XG5cbiAgdXBkYXRlRXJyb3I6IGJvb2xlYW4gPSBmYWxzZTtcbiAgZmlyc3RuYW1lRXJyb3I6IGJvb2xlYW4gPSBmYWxzZTtcbiAgc3VybmFtZUVycm9yOiBib29sZWFuID0gZmFsc2U7XG4gIGlkTnJFcnJvcjogYm9vbGVhbiA9IGZhbHNlO1xuICBjZWxOckVycm9yOiBib29sZWFuID0gZmFsc2U7XG4gIGVtYWlsRXJyb3I6IGJvb2xlYW4gPSBmYWxzZTtcbiAgZW1haWxDb25maXJtYXRpb25FcnJvcjogYm9vbGVhbiA9IGZhbHNlO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcHJvamVjdFNlcnZpY2U6IFByb2plY3RTZXJ2aWNlLCBwcml2YXRlIG1haW50ZW5hbmNlU2VydmljZTogTWFpbnRlbmFuY2VTZXJ2aWNlLFxuICAgICAgcHJpdmF0ZSBhY3RvclNlcnZpY2U6IEFjdG9yU2VydmljZSkge1xuICAgIHRoaXMuYWN0b3IgPSBuZXcgQWN0b3IoKTtcbiAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMubG9hZGluZyA9IHRydWU7XG4gICAgdGhpcy5wcm9qZWN0U2VydmljZS5nZXRTdGVwUGFyYW1ldGVycyh0aGlzLnN0ZXBJbnN0YW5jZS5zdGVwLmlkKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICAvLyB0aGlzLnJlcXVpcmVzRW1haWwgPSByZXN1bHRbMF0gJiYgXCJ0cnVlXCIgPT09IHJlc3VsdFswXTtcbiAgICAgIHRoaXMucmVxdWlyZXNGaXJzdG5hbWUgPSByZXN1bHRbXCI2XCJdICYmIFwidHJ1ZVwiID09PSByZXN1bHRbXCI2XCJdO1xuICAgICAgdGhpcy5yZXF1aXJlc1N1cm5hbWUgPSByZXN1bHRbXCI3XCJdICYmIFwidHJ1ZVwiID09PSByZXN1bHRbXCI3XCJdO1xuICAgICAgdGhpcy5yZXF1aXJlc0NlbE5yID0gcmVzdWx0W1wiOFwiXSAmJiBcInRydWVcIiA9PT0gcmVzdWx0W1wiOFwiXTtcbiAgICAgIHRoaXMucmVxdWlyZXNFbWFpbCA9IHJlc3VsdFtcIjBcIl0gJiYgXCJ0cnVlXCIgPT09IHJlc3VsdFtcIjBcIl07XG4gICAgICB0aGlzLnJlcXVpcmVzRW1wbG95ZXIgPSByZXN1bHRbXCIxXCJdICYmIFwidHJ1ZVwiID09PSByZXN1bHRbXCIxXCJdO1xuXG4gICAgICBpZiAocmVzdWx0W1wiOVwiXSkge1xuICAgICAgICB0aGlzLmlkTnJMYWJlbCA9IHJlc3VsdFtcIjlcIl07XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLmlkTnJMYWJlbCA9IFwiSWQgbnJcIjtcbiAgICAgIH1cblxuICAgICAgLy8gYWxlcnQodGhpcy5zdGVwSW5zdGFuY2UuYWN0b3IuaWROciArIFwiIFwiICsgdGhpcy5zdGVwSW5zdGFuY2UuYnVzaW5lc3NQcm9jZXNzSW5zdGFuY2UuYWN0b3IuaWROcik7XG4gICAgICB0aGlzLmFjdG9yID0gdGhpcy5zdGVwSW5zdGFuY2UuYnVzaW5lc3NQcm9jZXNzSW5zdGFuY2UuYWN0b3I7XG4gICAgICBpZiAodGhpcy5hY3Rvci5lbWFpbCkge1xuICAgICAgICB0aGlzLmVtYWlsQ29uZmlybWF0aW9uID0gdGhpcy5hY3Rvci5lbWFpbDtcbiAgICAgIH1cblxuICAgICAgaWYgKHRoaXMuYWN0b3IuYWN0b3JUeXBlKSB7XG4gICAgICAgIHRoaXMubWFpbnRlbmFuY2VTZXJ2aWNlLmdldEFjdG9yRmllbGRzKHRoaXMuYWN0b3IuYWN0b3JUeXBlLmlkKS50aGVuKGFjdG9yRmllbGRzID0+IHtcbiAgICAgICAgICB0aGlzLmFjdG9yRmllbGRMaXN0ID0gYWN0b3JGaWVsZHM7XG4gICAgICAgICAgaWYgKGFjdG9yRmllbGRzKSB7XG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGFjdG9yRmllbGRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwidHJhY2UgOSAtPiBcIiArIGkgKyBcIiwgXCIgKyBhY3RvckZpZWxkc1tpXS5pZCk7XG4gICAgICAgICAgICAgIHRoaXMuYWN0b3JGaWVsZFZhbHVlc1thY3RvckZpZWxkc1tpXS5pZF0gPSBcIlwiO1xuICAgICAgICAgICAgICB0aGlzLmFjdG9yRmllbGRFcnJvcnNbYWN0b3JGaWVsZHNbaV0uaWRdID0gZmFsc2U7XG4gICAgICAgICAgICAgIGlmIChpICUgMiA9PT0gMCkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwidHJhY2UgMTBcIik7XG4gICAgICAgICAgICAgICAgbGV0IGFjdG9yRmllbGRSb3cgPSB7fTtcbiAgICAgICAgICAgICAgICBhY3RvckZpZWxkUm93WzBdID0gYWN0b3JGaWVsZHNbaV07XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJ0cmFjZSAxMVwiKTtcbiAgICAgICAgICAgICAgICBpZiAoaSA8IGFjdG9yRmllbGRzLmxlbmd0aCAtIDEpIHtcbiAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwidHJhY2UgMTJcIik7XG4gICAgICAgICAgICAgICAgICBhY3RvckZpZWxkUm93WzFdID0gYWN0b3JGaWVsZHNbaSArIDFdO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHRoaXMuYWN0b3JGaWVsZHMucHVzaChhY3RvckZpZWxkUm93KTtcbiAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgIGlmIChhY3RvckZpZWxkc1tpXS5hY3RvckZpZWxkVHlwZSA9PT0gXCJTRUxFQ1RcIikge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwidHJhY2UgMTNcIik7XG4gICAgICAgICAgICAgICAgdGhpcy5tYWludGVuYW5jZVNlcnZpY2UuZ2V0QWN0b3JGaWVsZE9wdGlvbnMoYWN0b3JGaWVsZHNbaV0uaWQpLnRoZW4oYWN0b3JGaWVsZE9wdGlvbnMgPT4ge1xuICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJ0cmFjZSAxNFwiKTtcbiAgICAgICAgICAgICAgICAgIGlmIChhY3RvckZpZWxkT3B0aW9ucykge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcInRyYWNlIDE1XCIpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmFjdG9yRmllbGRPcHRpb25zTWFwW2FjdG9yRmllbGRzW2ldLmlkXSA9IGFjdG9yRmllbGRPcHRpb25zO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICB0aGlzLmFjdG9yU2VydmljZS5nZXRBY3RvckZpZWxkVmFsdWVzKHRoaXMuYWN0b3IuaWQpLnRoZW4oYWN0b3JGaWVsZFZhbHVlcyA9PiB7XG4gICAgICAgICAgZm9yIChsZXQgYWN0b3JGaWVsZFZhbHVlIG9mIGFjdG9yRmllbGRWYWx1ZXMpIHtcbiAgICAgICAgICAgIHRoaXMuYWN0b3JGaWVsZFZhbHVlc1thY3RvckZpZWxkVmFsdWUuYWN0b3JGaWVsZElkXSA9IGFjdG9yRmllbGRWYWx1ZS5maWVsZFZhbHVlO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICB9KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgICAgfVxuICAgIH0pO1xuXG4gICAgLy8gY29uc29sZS5sb2coXCJ0cmFjZSA2XCIpO1xuICAgIC8vIHRoaXMucHJvamVjdFNlcnZpY2UuZ2V0UHJvY2Vzc1ZhcmlhYmxlVmFsdWUodGhpcy5zdGVwSW5zdGFuY2UuYnVzaW5lc3NQcm9jZXNzSW5zdGFuY2UuaWQsIFwiaWROclwiKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgLy8gICBjb25zb2xlLmxvZyhcInRyYWNlIDdcIik7XG4gICAgLy8gICBpZiAocmVzdWx0KSB7XG4gICAgLy8gICAgIGNvbnNvbGUubG9nKFwidHJhY2UgOFwiKTtcbiAgICAvLyAgICAgdGhpcy5hY3Rvci5pZE5yID0gcmVzdWx0O1xuICAgIC8vICAgfVxuICAgIC8vXG4gICAgLy8gICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAvLyB9KTtcbiAgfVxuXG4gIGNvbXBsZXRlU3RlcCgpIHtcbiAgICB0aGlzLmZpcnN0bmFtZUVycm9yID0gZmFsc2U7XG4gICAgdGhpcy5zdXJuYW1lRXJyb3IgPSBmYWxzZTtcbiAgICB0aGlzLmlkTnJFcnJvciA9IGZhbHNlO1xuICAgIHRoaXMuY2VsTnJFcnJvciA9IGZhbHNlO1xuICAgIHRoaXMuZW1haWxFcnJvciA9IGZhbHNlO1xuICAgIHRoaXMuZW1haWxDb25maXJtYXRpb25FcnJvciA9IGZhbHNlO1xuXG4gICAgLy8gcmVxdWlyZXNGaXJzdG5hbWU6IGJvb2xlYW47XG4gICAgLy8gcmVxdWlyZXNTdXJuYW1lOiBib29sZWFuO1xuICAgIC8vIHJlcXVpcmVzQ2VsTnI6IGJvb2xlYW47XG4gICAgLy8gcmVxdWlyZXNFbXBsb3llcjogYm9vbGVhbjtcblxuICAgIGlmICh0aGlzLnJlcXVpcmVzRmlyc3RuYW1lKSB7XG4gICAgICBpZiAoIXRoaXMuYWN0b3IuZmlyc3RuYW1lKSB7XG4gICAgICAgIHRoaXMuZmlyc3RuYW1lRXJyb3IgPSB0cnVlO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuYWN0b3IuZmlyc3RuYW1lID0gdGhpcy5hY3Rvci5pZE5yO1xuICAgIH1cblxuICAgIGlmICh0aGlzLnJlcXVpcmVzU3VybmFtZSkge1xuICAgICAgaWYgKCF0aGlzLmFjdG9yLnN1cm5hbWUpIHtcbiAgICAgICAgdGhpcy5zdXJuYW1lRXJyb3IgPSB0cnVlO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuYWN0b3Iuc3VybmFtZSA9IHRoaXMuYWN0b3IuaWROcjtcbiAgICB9XG5cbiAgICBpZiAoIXRoaXMuYWN0b3IuaWROcikge1xuICAgICAgdGhpcy5pZE5yRXJyb3IgPSB0cnVlO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGlmICh0aGlzLnJlcXVpcmVzQ2VsTnIgJiYgIXRoaXMuYWN0b3IuY2VsTnIpIHtcbiAgICAgIHRoaXMuY2VsTnJFcnJvciA9IHRydWU7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKCF0aGlzLmFjdG9yLmVtYWlsKSB7XG4gICAgICB0aGlzLmVtYWlsRXJyb3IgPSB0cnVlO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGlmICh0aGlzLmFjdG9yLmVtYWlsICE9PSB0aGlzLmVtYWlsQ29uZmlybWF0aW9uKSB7XG4gICAgICB0aGlzLmVtYWlsQ29uZmlybWF0aW9uRXJyb3IgPSB0cnVlO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHRoaXMuYWN0b3JGaWVsZEVycm9ycyA9IHt9O1xuICAgIGxldCBlcnJvckZvdW5kID0gZmFsc2U7XG4gICAgZm9yIChjb25zdCBhY3RvckZpZWxkIG9mIHRoaXMuYWN0b3JGaWVsZExpc3QpIHtcbiAgICAgIGxldCBmaWVsZFZhbHVlID0gdGhpcy5hY3RvckZpZWxkVmFsdWVzW2FjdG9yRmllbGQuaWRdO1xuICAgICAgaWYgKGFjdG9yRmllbGQucmVxdWlyZWQgPT09IHRydWUgJiYgKCFmaWVsZFZhbHVlIHx8IGZpZWxkVmFsdWUgPT09IFwiXCIpKSB7XG4gICAgICAgIHRoaXMuYWN0b3JGaWVsZEVycm9yc1thY3RvckZpZWxkLmlkXSA9IHRydWU7XG4gICAgICAgIGVycm9yRm91bmQgPSB0cnVlO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5hY3RvckZpZWxkRXJyb3JzW2FjdG9yRmllbGQuaWRdID0gZmFsc2U7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKGVycm9yRm91bmQpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB0aGlzLnVwZGF0ZUVycm9yID0gZmFsc2U7XG4gICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcbiAgICB0aGlzLnByb2plY3RTZXJ2aWNlLnVwZGF0ZUFjdG9yKHRoaXMuYWN0b3IpLnRoZW4odXBkYXRlUmVzdWx0ID0+IHtcbiAgICAgIGlmICh1cGRhdGVSZXN1bHQpIHtcbiAgICAgICAgbGV0IGFjdG9yRmllbGRWYWx1ZXMgPSBbXTtcbiAgICAgICAgZm9yIChjb25zdCBhY3RvckZpZWxkSWQgb2YgT2JqZWN0LmtleXModGhpcy5hY3RvckZpZWxkVmFsdWVzKSkge1xuICAgICAgICAgIGFjdG9yRmllbGRWYWx1ZXMucHVzaCh7XG4gICAgICAgICAgICBhY3RvckZpZWxkSWQ6IE51bWJlcihhY3RvckZpZWxkSWQpLFxuICAgICAgICAgICAgZmllbGRWYWx1ZTogdGhpcy5hY3RvckZpZWxkVmFsdWVzW2FjdG9yRmllbGRJZF1cbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuYWN0b3JTZXJ2aWNlLnVwZGF0ZUFjdG9yRmllbGRWYWx1ZXModGhpcy5hY3Rvci5pZCwgYWN0b3JGaWVsZFZhbHVlcykudGhlbih1cGRhdGVSZXN1bHQyID0+IHtcbiAgICAgICAgICBpZiAodXBkYXRlUmVzdWx0Mikge1xuICAgICAgICAgICAgdGhpcy5wcm9qZWN0U2VydmljZS5jb21wbGV0ZVN0ZXAodGhpcy5zdGVwSW5zdGFuY2UuaWQsIHRoaXMudXNlcklkKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICAgICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICB0aGlzLnN0ZXBDb21wbGV0ZWRMaXN0ZW5lci5lbWl0KHtcbiAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5zdGVwSW5zdGFuY2VcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy51cGRhdGVFcnJvciA9IHRydWU7XG4gICAgICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy51cGRhdGVFcnJvciA9IHRydWU7XG4gICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgY2FuY2VsKCkge1xuICAgIHRoaXMuY2FuY2VsTGlzdGVuZXIuZW1pdCh7XG4gICAgfSk7XG4gIH1cbn1cbiJdfQ==