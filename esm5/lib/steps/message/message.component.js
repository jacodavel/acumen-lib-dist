/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ProjectService } from '../../services/project.service';
import { StepInstance } from '../../domain/step-instance';
var MessageComponent = /** @class */ (function () {
    function MessageComponent(projectService, sanitizer) {
        this.projectService = projectService;
        this.sanitizer = sanitizer;
        this.stepCompletedListener = new EventEmitter();
        this.cancelListener = new EventEmitter();
        this.loading = false;
    }
    Object.defineProperty(MessageComponent.prototype, "stepInstance", {
        set: /**
         * @param {?} s
         * @return {?}
         */
        function (s) {
            var _this = this;
            this._stepInstance = s;
            this.loading = true;
            this.projectService.getMessageStepHtml(this._stepInstance.id).then((/**
             * @param {?} html
             * @return {?}
             */
            function (html) {
                if (html["text"]) {
                    _this.html = _this.sanitizer.bypassSecurityTrustHtml(html["text"]);
                }
                else {
                    _this.html = "<div></div>";
                }
                _this.loading = false;
            }));
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    MessageComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    MessageComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    MessageComponent.prototype.completeStep = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.loading = true;
        this.projectService.completeStep(this._stepInstance.id, this.userId).then((/**
         * @param {?} result
         * @return {?}
         */
        function (result) {
            _this.loading = false;
            _this.stepCompletedListener.emit({
                value: _this._stepInstance
            });
        }));
    };
    /**
     * @return {?}
     */
    MessageComponent.prototype.cancel = /**
     * @return {?}
     */
    function () {
        this.cancelListener.emit({});
    };
    MessageComponent.decorators = [
        { type: Component, args: [{
                    selector: 'message',
                    template: "<div class=\"message-step\">\n  <div *ngIf=\"!html\" class=\"info-message\">\n    Loading information. Please wait...\n  </div>\n  <div *ngIf=\"html\" class=\"message-content\" [innerHTML]=\"html\">\n  </div>\n\n  <div *ngIf=\"!loading\" class=\"button-section\">\n    <div class=\"button\" (click)=\"completeStep()\">\n      <div class=\"button-text\">Complete</div>\n    </div>\n    <div class=\"button button-outline\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n  <div *ngIf=\"loading\" class=\"button-section\">\n    <div class=\"button button-disabled\" (click)=\"completeStep()\">\n      <div class=\"button-text\">Complete</div>\n    </div>\n    <div class=\"button button-outline-disabled\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                    styles: [".message-step{width:100%;height:100%;position:relative}.message-step .info-message{position:absolute;top:0;left:0;bottom:50px;width:100%;background-color:#f9f9f9;padding:8px;box-sizing:border-box}.message-step .message-content{position:absolute;top:0;left:0;bottom:50px;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px;box-sizing:border-box}.message-step .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.message-step .button-section .button{width:120px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.message-step .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.message-step .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.message-step .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b}.message-step .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b}"]
                }] }
    ];
    /** @nocollapse */
    MessageComponent.ctorParameters = function () { return [
        { type: ProjectService },
        { type: DomSanitizer }
    ]; };
    MessageComponent.propDecorators = {
        _stepInstance: [{ type: Input }],
        userId: [{ type: Input }],
        stepCompletedListener: [{ type: Output }],
        cancelListener: [{ type: Output }],
        stepInstance: [{ type: Input }]
    };
    return MessageComponent;
}());
export { MessageComponent };
if (false) {
    /** @type {?} */
    MessageComponent.prototype._stepInstance;
    /** @type {?} */
    MessageComponent.prototype.userId;
    /** @type {?} */
    MessageComponent.prototype.stepCompletedListener;
    /** @type {?} */
    MessageComponent.prototype.cancelListener;
    /** @type {?} */
    MessageComponent.prototype.error;
    /** @type {?} */
    MessageComponent.prototype.html;
    /** @type {?} */
    MessageComponent.prototype.loading;
    /**
     * @type {?}
     * @private
     */
    MessageComponent.prototype.projectService;
    /**
     * @type {?}
     * @private
     */
    MessageComponent.prototype.sanitizer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVzc2FnZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL3N0ZXBzL21lc3NhZ2UvbWVzc2FnZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBcUIsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzFGLE9BQU8sRUFBRSxZQUFZLEVBQW1CLE1BQU0sMkJBQTJCLENBQUM7QUFFMUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQ2hFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUUxRDtJQTZCRSwwQkFBb0IsY0FBOEIsRUFBVSxTQUF1QjtRQUEvRCxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFBVSxjQUFTLEdBQVQsU0FBUyxDQUFjO1FBckJ6RSwwQkFBcUIsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQzNDLG1CQUFjLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUc5QyxZQUFPLEdBQVksS0FBSyxDQUFDO0lBa0J6QixDQUFDO0lBaEJELHNCQUNJLDBDQUFZOzs7OztRQURoQixVQUNpQixDQUFlO1lBRGhDLGlCQWFDO1lBWEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUM7WUFDdkIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDcEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUk7Ozs7WUFBQyxVQUFDLElBQUk7Z0JBQ3RFLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFO29CQUNoQixLQUFJLENBQUMsSUFBSSxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7aUJBQ2xFO3FCQUFNO29CQUNMLEtBQUksQ0FBQyxJQUFJLEdBQUcsYUFBYSxDQUFBO2lCQUMxQjtnQkFFRCxLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztZQUN2QixDQUFDLEVBQUMsQ0FBQztRQUNMLENBQUM7OztPQUFBOzs7O0lBS0QsbUNBQVE7OztJQUFSO0lBQ0EsQ0FBQzs7OztJQUVELHNDQUFXOzs7SUFBWDtJQUNBLENBQUM7Ozs7SUFFRCx1Q0FBWTs7O0lBQVo7UUFBQSxpQkFRQztRQVBDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJOzs7O1FBQUMsVUFBQSxNQUFNO1lBQzlFLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUM7Z0JBQzlCLEtBQUssRUFBRSxLQUFJLENBQUMsYUFBYTthQUMxQixDQUFDLENBQUM7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7SUFFRCxpQ0FBTTs7O0lBQU47UUFDRSxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxFQUN4QixDQUFDLENBQUM7SUFDTCxDQUFDOztnQkFuREYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxTQUFTO29CQUNuQiw0MUJBQXVDOztpQkFFeEM7Ozs7Z0JBUFEsY0FBYztnQkFGZCxZQUFZOzs7Z0NBV2xCLEtBQUs7eUJBQ0wsS0FBSzt3Q0FDTCxNQUFNO2lDQUNOLE1BQU07K0JBS04sS0FBSzs7SUFzQ1IsdUJBQUM7Q0FBQSxBQXBERCxJQW9EQztTQS9DWSxnQkFBZ0I7OztJQUMzQix5Q0FBcUM7O0lBQ3JDLGtDQUF3Qjs7SUFDeEIsaURBQXFEOztJQUNyRCwwQ0FBOEM7O0lBQzlDLGlDQUFXOztJQUNYLGdDQUFVOztJQUNWLG1DQUF5Qjs7Ozs7SUFpQmIsMENBQXNDOzs7OztJQUFFLHFDQUErQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPbkRlc3Ryb3ksIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRG9tU2FuaXRpemVyLCBTYWZlUmVzb3VyY2VVcmwgfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyJztcblxuaW1wb3J0IHsgUHJvamVjdFNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9wcm9qZWN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgU3RlcEluc3RhbmNlIH0gZnJvbSAnLi4vLi4vZG9tYWluL3N0ZXAtaW5zdGFuY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdtZXNzYWdlJyxcbiAgdGVtcGxhdGVVcmw6ICcuL21lc3NhZ2UuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9tZXNzYWdlLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgTWVzc2FnZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgQElucHV0KCkgX3N0ZXBJbnN0YW5jZTogU3RlcEluc3RhbmNlO1xuICBASW5wdXQoKSB1c2VySWQ6IG51bWJlcjtcbiAgQE91dHB1dCgpIHN0ZXBDb21wbGV0ZWRMaXN0ZW5lciA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIGNhbmNlbExpc3RlbmVyID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBlcnJvcjogYW55O1xuICBodG1sOiBhbnk7XG4gIGxvYWRpbmc6IGJvb2xlYW4gPSBmYWxzZTtcblxuICBASW5wdXQoKVxuICBzZXQgc3RlcEluc3RhbmNlKHM6IFN0ZXBJbnN0YW5jZSkge1xuICAgIHRoaXMuX3N0ZXBJbnN0YW5jZSA9IHM7XG4gICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcbiAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmdldE1lc3NhZ2VTdGVwSHRtbCh0aGlzLl9zdGVwSW5zdGFuY2UuaWQpLnRoZW4oKGh0bWwpID0+IHtcbiAgICAgIGlmIChodG1sW1widGV4dFwiXSkge1xuICAgICAgICB0aGlzLmh0bWwgPSB0aGlzLnNhbml0aXplci5ieXBhc3NTZWN1cml0eVRydXN0SHRtbChodG1sW1widGV4dFwiXSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLmh0bWwgPSBcIjxkaXY+PC9kaXY+XCJcbiAgICAgIH1cblxuICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgfSk7XG4gIH1cblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHByb2plY3RTZXJ2aWNlOiBQcm9qZWN0U2VydmljZSwgcHJpdmF0ZSBzYW5pdGl6ZXI6IERvbVNhbml0aXplcikge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpIHtcbiAgfVxuXG4gIGNvbXBsZXRlU3RlcCgpIHtcbiAgICB0aGlzLmxvYWRpbmcgPSB0cnVlO1xuICAgIHRoaXMucHJvamVjdFNlcnZpY2UuY29tcGxldGVTdGVwKHRoaXMuX3N0ZXBJbnN0YW5jZS5pZCwgdGhpcy51c2VySWQpLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgICAgdGhpcy5zdGVwQ29tcGxldGVkTGlzdGVuZXIuZW1pdCh7XG4gICAgICAgIHZhbHVlOiB0aGlzLl9zdGVwSW5zdGFuY2VcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgY2FuY2VsKCkge1xuICAgIHRoaXMuY2FuY2VsTGlzdGVuZXIuZW1pdCh7XG4gICAgfSk7XG4gIH1cbn1cbiJdfQ==