/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { StepInstance } from '../../domain/step-instance';
var CodeReaderComponent = /** @class */ (function () {
    function CodeReaderComponent(projectService) {
        this.projectService = projectService;
        this.onReadCode = new EventEmitter();
        this.stepCompletedListener = new EventEmitter();
        this.cancelListener = new EventEmitter();
        this.loading = false;
        this.description = "Code";
        this.variableName = "variableName";
    }
    /**
     * @return {?}
     */
    CodeReaderComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.loading = true;
        this.projectService.getStepParameters(this.stepInstance.step.id).then((/**
         * @param {?} result
         * @return {?}
         */
        function (result) {
            _this.loading = false;
            _this.description = _this.stepInstance.step.parameters["0"] && _this.stepInstance.step.parameters["0"].parameterValue;
            _this.variableName = _this.stepInstance.step.parameters["1"] && _this.stepInstance.step.parameters["1"].parameterValue;
        }));
    };
    /**
     * @return {?}
     */
    CodeReaderComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    CodeReaderComponent.prototype.readCode = /**
     * @return {?}
     */
    function () {
        this.onReadCode.emit(this);
    };
    /**
     * @return {?}
     */
    CodeReaderComponent.prototype.completeStep = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.loading = true;
        this.projectService.setProcessVariableValue(this.stepInstance.businessProcessInstance.id, this.variableName, this.code).then((/**
         * @param {?} imageUpdateResult
         * @return {?}
         */
        function (imageUpdateResult) {
            _this.projectService.completeStep(_this.stepInstance.id, _this.userId).then((/**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                _this.loading = false;
                _this.stepCompletedListener.emit({
                    value: _this.stepInstance
                });
            })).catch((/**
             * @param {?} error
             * @return {?}
             */
            function (error) {
                _this.loading = false;
                _this.stepCompletedListener.emit({
                    value: _this.stepInstance
                });
            }));
        }));
    };
    /**
     * @return {?}
     */
    CodeReaderComponent.prototype.cancel = /**
     * @return {?}
     */
    function () {
        this.cancelListener.emit({});
    };
    /**
     * @param {?} value
     * @return {?}
     */
    CodeReaderComponent.prototype.setValue = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this.code = value;
    };
    CodeReaderComponent.decorators = [
        { type: Component, args: [{
                    selector: 'code-reader',
                    template: "<div class=\"code-reader-step\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div *ngIf=\"!loading\" class=\"content\">\n    <div class=\"component-row\">\n      <div class=\"component-col\">\n        <div *ngIf=\"!description\" class=\"component-label\">\n          Value\n        </div>\n        <div *ngIf=\"description\" class=\"component-label\">\n          {{ description }}\n        </div>\n        <div class=\"component\">\n          <input type=\"text\" [(ngModel)]=\"code\" readonly>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!loading\" class=\"button-section\">\n    <div class=\"button\" (click)=\"readCode()\">\n      <div class=\"button-text\">Scan</div>\n    </div>\n    <div class=\"button\" (click)=\"completeStep()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div class=\"button button-outline\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n  <div *ngIf=\"loading\" class=\"button-section\">\n    <div class=\"button button-disabled\">\n      <div class=\"button-text\">Scan</div>\n    </div>\n    <div class=\"button button-disabled\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div class=\"button button-outline-disabled\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                    styles: [".code-reader-step{width:100%;height:100%;position:relative}.code-reader-step .heading{position:absolute;top:0;width:100%;height:32px;font-size:16px;padding:8px}.code-reader-step .content{position:absolute;top:0;left:0;bottom:50px;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px;box-sizing:border-box}.code-reader-step .content .info-message{font-size:15px}.code-reader-step .content .component-row{width:100%}.code-reader-step .content .component-row .component-col{width:100%;display:inline-block;box-sizing:border-box;padding-top:10px}.code-reader-step .content .component-row .component-col .component-label{width:100%;font-size:15px;padding-bottom:5px}.code-reader-step .content .component-row .component-col .error-message{font-style:italic;font-size:11px;color:#f04141;padding-bottom:5px}.code-reader-step .content .component-row .component-col .component{width:100%}.code-reader-step .content .component-row .component-col .component input{width:100%;font-size:13px;border:1px solid #58666c;background-color:#fffcf6;box-sizing:border-box;padding:3px 5px;border-radius:5px;height:30px}.code-reader-step .content .component-row:first-child .component-col:first-child{padding-top:0}@media (min-width:600px){.code-reader-step .content .component-row .component-col{width:50%}.code-reader-step .content .component-row .component-col:first-child{padding-right:16px}.code-reader-step .content .component-row:first-child .component-col{padding-top:0}}.code-reader-step .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.code-reader-step .button-section .button{width:70px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.code-reader-step .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.code-reader-step .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.code-reader-step .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b}.code-reader-step .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b}"]
                }] }
    ];
    /** @nocollapse */
    CodeReaderComponent.ctorParameters = function () { return [
        { type: ProjectService }
    ]; };
    CodeReaderComponent.propDecorators = {
        stepInstance: [{ type: Input }],
        userId: [{ type: Input }],
        onReadCode: [{ type: Output }],
        stepCompletedListener: [{ type: Output }],
        cancelListener: [{ type: Output }]
    };
    return CodeReaderComponent;
}());
export { CodeReaderComponent };
if (false) {
    /** @type {?} */
    CodeReaderComponent.prototype.stepInstance;
    /** @type {?} */
    CodeReaderComponent.prototype.userId;
    /** @type {?} */
    CodeReaderComponent.prototype.onReadCode;
    /** @type {?} */
    CodeReaderComponent.prototype.stepCompletedListener;
    /** @type {?} */
    CodeReaderComponent.prototype.cancelListener;
    /** @type {?} */
    CodeReaderComponent.prototype._stepInstance;
    /** @type {?} */
    CodeReaderComponent.prototype.loading;
    /** @type {?} */
    CodeReaderComponent.prototype.code;
    /** @type {?} */
    CodeReaderComponent.prototype.description;
    /** @type {?} */
    CodeReaderComponent.prototype.variableName;
    /**
     * @type {?}
     * @private
     */
    CodeReaderComponent.prototype.projectService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29kZS1yZWFkZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWN1bWVuLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9zdGVwcy9jb2RlLXJlYWRlci9jb2RlLXJlYWRlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBcUIsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTFGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNoRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFHMUQ7SUFpQkUsNkJBQW9CLGNBQThCO1FBQTlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQVR4QyxlQUFVLEdBQUcsSUFBSSxZQUFZLEVBQW9CLENBQUM7UUFDbEQsMEJBQXFCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUMzQyxtQkFBYyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFFOUMsWUFBTyxHQUFZLEtBQUssQ0FBQztRQUV6QixnQkFBVyxHQUFXLE1BQU0sQ0FBQztRQUM3QixpQkFBWSxHQUFXLGNBQWMsQ0FBQztJQUd0QyxDQUFDOzs7O0lBRUQsc0NBQVE7OztJQUFSO1FBQUEsaUJBT0M7UUFOQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNwQixJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxVQUFBLE1BQU07WUFDMUUsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFDckIsS0FBSSxDQUFDLFdBQVcsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLElBQUksS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLGNBQWMsQ0FBQztZQUNuSCxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsSUFBSSxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsY0FBYyxDQUFDO1FBQ3RILENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVELHlDQUFXOzs7SUFBWDtJQUNBLENBQUM7Ozs7SUFFRCxzQ0FBUTs7O0lBQVI7UUFDRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM3QixDQUFDOzs7O0lBRUQsMENBQVk7OztJQUFaO1FBQUEsaUJBZUM7UUFkQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNwQixJQUFJLENBQUMsY0FBYyxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsdUJBQXVCLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxVQUFBLGlCQUFpQjtZQUM1SSxLQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsRUFBRSxLQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSTs7OztZQUFDLFVBQUEsTUFBTTtnQkFDN0UsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7Z0JBQ3JCLEtBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUM7b0JBQzlCLEtBQUssRUFBRSxLQUFJLENBQUMsWUFBWTtpQkFDekIsQ0FBQyxDQUFDO1lBQ0wsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztZQUFDLFVBQUEsS0FBSztnQkFDWixLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDckIsS0FBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQztvQkFDOUIsS0FBSyxFQUFFLEtBQUksQ0FBQyxZQUFZO2lCQUN6QixDQUFDLENBQUM7WUFDTCxDQUFDLEVBQUMsQ0FBQztRQUNMLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVELG9DQUFNOzs7SUFBTjtRQUNFLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQ3hCLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsc0NBQVE7Ozs7SUFBUixVQUFTLEtBQWE7UUFDcEIsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7SUFDcEIsQ0FBQzs7Z0JBNURGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsYUFBYTtvQkFDdkIsbzJDQUEyQzs7aUJBRTVDOzs7O2dCQVJRLGNBQWM7OzsrQkFVcEIsS0FBSzt5QkFDTCxLQUFLOzZCQUNMLE1BQU07d0NBQ04sTUFBTTtpQ0FDTixNQUFNOztJQW1EVCwwQkFBQztDQUFBLEFBN0RELElBNkRDO1NBeERZLG1CQUFtQjs7O0lBQzlCLDJDQUFvQzs7SUFDcEMscUNBQXdCOztJQUN4Qix5Q0FBNEQ7O0lBQzVELG9EQUFxRDs7SUFDckQsNkNBQThDOztJQUM5Qyw0Q0FBNEI7O0lBQzVCLHNDQUF5Qjs7SUFDekIsbUNBQWE7O0lBQ2IsMENBQTZCOztJQUM3QiwyQ0FBc0M7Ozs7O0lBRTFCLDZDQUFzQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPbkRlc3Ryb3ksIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBQcm9qZWN0U2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL3Byb2plY3Quc2VydmljZSc7XG5pbXBvcnQgeyBTdGVwSW5zdGFuY2UgfSBmcm9tICcuLi8uLi9kb21haW4vc3RlcC1pbnN0YW5jZSc7XG5pbXBvcnQgeyBDb2RlUmVhZExpc3RlbmVyIH0gZnJvbSAnLi9jb2RlLXJlYWQtbGlzdGVuZXInO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdjb2RlLXJlYWRlcicsXG4gIHRlbXBsYXRlVXJsOiAnLi9jb2RlLXJlYWRlci5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2NvZGUtcmVhZGVyLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgQ29kZVJlYWRlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95LCBDb2RlUmVhZExpc3RlbmVyIHtcbiAgQElucHV0KCkgc3RlcEluc3RhbmNlOiBTdGVwSW5zdGFuY2U7XG4gIEBJbnB1dCgpIHVzZXJJZDogbnVtYmVyO1xuICBAT3V0cHV0KCkgb25SZWFkQ29kZSA9IG5ldyBFdmVudEVtaXR0ZXI8Q29kZVJlYWRMaXN0ZW5lcj4oKTtcbiAgQE91dHB1dCgpIHN0ZXBDb21wbGV0ZWRMaXN0ZW5lciA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIGNhbmNlbExpc3RlbmVyID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBfc3RlcEluc3RhbmNlOiBTdGVwSW5zdGFuY2U7XG4gIGxvYWRpbmc6IGJvb2xlYW4gPSBmYWxzZTtcbiAgY29kZTogc3RyaW5nO1xuICBkZXNjcmlwdGlvbjogc3RyaW5nID0gXCJDb2RlXCI7XG4gIHZhcmlhYmxlTmFtZTogc3RyaW5nID0gXCJ2YXJpYWJsZU5hbWVcIjtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHByb2plY3RTZXJ2aWNlOiBQcm9qZWN0U2VydmljZSkge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcbiAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmdldFN0ZXBQYXJhbWV0ZXJzKHRoaXMuc3RlcEluc3RhbmNlLnN0ZXAuaWQpLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgICAgdGhpcy5kZXNjcmlwdGlvbiA9IHRoaXMuc3RlcEluc3RhbmNlLnN0ZXAucGFyYW1ldGVyc1tcIjBcIl0gJiYgdGhpcy5zdGVwSW5zdGFuY2Uuc3RlcC5wYXJhbWV0ZXJzW1wiMFwiXS5wYXJhbWV0ZXJWYWx1ZTtcbiAgICAgIHRoaXMudmFyaWFibGVOYW1lID0gdGhpcy5zdGVwSW5zdGFuY2Uuc3RlcC5wYXJhbWV0ZXJzW1wiMVwiXSAmJiB0aGlzLnN0ZXBJbnN0YW5jZS5zdGVwLnBhcmFtZXRlcnNbXCIxXCJdLnBhcmFtZXRlclZhbHVlO1xuICAgIH0pO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKSB7XG4gIH1cblxuICByZWFkQ29kZSgpIHtcbiAgICB0aGlzLm9uUmVhZENvZGUuZW1pdCh0aGlzKTtcbiAgfVxuXG4gIGNvbXBsZXRlU3RlcCgpIHtcbiAgICB0aGlzLmxvYWRpbmcgPSB0cnVlO1xuICAgIHRoaXMucHJvamVjdFNlcnZpY2Uuc2V0UHJvY2Vzc1ZhcmlhYmxlVmFsdWUodGhpcy5zdGVwSW5zdGFuY2UuYnVzaW5lc3NQcm9jZXNzSW5zdGFuY2UuaWQsIHRoaXMudmFyaWFibGVOYW1lLCB0aGlzLmNvZGUpLnRoZW4oaW1hZ2VVcGRhdGVSZXN1bHQgPT4ge1xuICAgICAgdGhpcy5wcm9qZWN0U2VydmljZS5jb21wbGV0ZVN0ZXAodGhpcy5zdGVwSW5zdGFuY2UuaWQsIHRoaXMudXNlcklkKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICB0aGlzLnN0ZXBDb21wbGV0ZWRMaXN0ZW5lci5lbWl0KHtcbiAgICAgICAgICB2YWx1ZTogdGhpcy5zdGVwSW5zdGFuY2VcbiAgICAgICAgfSk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICB0aGlzLnN0ZXBDb21wbGV0ZWRMaXN0ZW5lci5lbWl0KHtcbiAgICAgICAgICB2YWx1ZTogdGhpcy5zdGVwSW5zdGFuY2VcbiAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIGNhbmNlbCgpIHtcbiAgICB0aGlzLmNhbmNlbExpc3RlbmVyLmVtaXQoe1xuICAgIH0pO1xuICB9XG5cbiAgc2V0VmFsdWUodmFsdWU6IHN0cmluZykge1xuICAgIHRoaXMuY29kZSA9IHZhbHVlO1xuICB9XG59XG4iXX0=