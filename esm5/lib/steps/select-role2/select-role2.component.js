/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { StepInstance } from '../../domain/step-instance';
var SelectRole2Component = /** @class */ (function () {
    function SelectRole2Component(projectService) {
        this.projectService = projectService;
        this.stepCompletedListener = new EventEmitter();
        this.bookedTimeSlots = [];
        this.newTimeSlots = [];
        this.showCalendar = false;
        this.showSelectedProviderError = false;
    }
    /**
     * @return {?}
     */
    SelectRole2Component.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.stepInstance && this.stepInstance.step.parameters["0"]) {
            this.showWaitIndicator = true;
            /** @type {?} */
            var projectId = this.stepInstance.businessProcessInstance.businessProcess.project.id;
            /** @type {?} */
            var projectRoleId = this.stepInstance.step.parameters["0"].parameterValue;
            this.projectService.getProjectActors2(0, 100, projectId, projectRoleId).then((/**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                _this.tableDataPage = result;
                _this.projectService.getProcessVariableValue(_this.stepInstance.businessProcessInstance.id, "providerId").then((/**
                 * @param {?} providerId
                 * @return {?}
                 */
                function (providerId) {
                    if (providerId) {
                        for (var i = 0; i < _this.tableDataPage.list.length; i++) {
                            if (Number(providerId) === _this.tableDataPage.list[i].actor.id) {
                                _this.provider = _this.tableDataPage.list[i].actor;
                                break;
                            }
                        }
                    }
                    _this.showWaitIndicator = false;
                }));
            }));
        }
    };
    /**
     * @return {?}
     */
    SelectRole2Component.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @param {?} provider
     * @return {?}
     */
    SelectRole2Component.prototype.selectProvider = /**
     * @param {?} provider
     * @return {?}
     */
    function (provider) {
        if (provider) {
            this.provider = provider.actor;
        }
        else {
            this.provider = null;
        }
    };
    /**
     * @return {?}
     */
    SelectRole2Component.prototype.continueWithSelectedProvider = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.showSelectedProviderError = false;
        if (!this.provider) {
            this.showSelectedProviderError = true;
            return;
        }
        this.showWaitIndicator = true;
        this.projectService.setProcessVariableValue(this.stepInstance.businessProcessInstance.id, "providerId", this.provider.id.toString()).then((/**
         * @param {?} result
         * @return {?}
         */
        function (result) {
            if (_this.makeBooking) {
                _this.projectService.getTimeSlots(null, _this.provider.id, null).then((/**
                 * @param {?} result
                 * @return {?}
                 */
                function (result) {
                    _this.bookedTimeSlots = result;
                    _this.showCalendar = true;
                    _this.showWaitIndicator = false;
                }));
            }
            else {
                _this.completeStep();
            }
        }));
    };
    /**
     * @param {?} event
     * @return {?}
     */
    SelectRole2Component.prototype.timeSlotSelected = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.newTimeSlots = event.value;
    };
    /**
     * @return {?}
     */
    SelectRole2Component.prototype.updateTimeSlots = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.newTimeSlots) {
            this.showWaitIndicator = true;
            this.projectService.updateTimeSlots(this.newTimeSlots).then((/**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                _this.completeStep();
            }));
        }
    };
    /**
     * @private
     * @return {?}
     */
    SelectRole2Component.prototype.completeStep = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        this.showWaitIndicator = true;
        this.projectService.completeStep(this.stepInstance.id, this.userId).then((/**
         * @param {?} result
         * @return {?}
         */
        function (result) {
            _this.stepCompletedListener.emit({
                value: _this.stepInstance
            });
            _this.showWaitIndicator = false;
        }));
    };
    SelectRole2Component.decorators = [
        { type: Component, args: [{
                    selector: 'select-role2',
                    template: "<div class=\"select-role2\">\n  <loading-indicator [show]=\"showWaitIndicator\"></loading-indicator>\n  <div *ngIf=\"!showCalendar\"class=\"heading\">\n    Select Provider\n  </div>\n  <div *ngIf=\"showCalendar\"class=\"heading\">\n    Make Appointment with {{ provider.title.description }} {{ provider.firstname }} {{ provider.surname }}\n  </div>\n\n  <div *ngIf=\"!showCalendar\" class=\"content\">\n    <div class=\"col-md-12\">\n      <table class=\"provider-table\">\n        <tr>\n          <th>Title</th>\n          <th>Firstname</th>\n          <th>Surname</th>\n          <th>E-mail</th>\n          <th>Contact Nrs</th>\n          <th>Address</th>\n        </tr>\n        <tbody *ngIf=\"tableDataPage\">\n          <tr *ngFor=\"let projectActor of tableDataPage.list\" (click)=\"selectProvider(projectActor)\">\n            <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n              <span *ngIf=\"projectActor.actor.title\">{{ projectActor.actor.title[\"description\"] }}</span>\n            </td>\n            <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n              {{ projectActor.actor.firstname }}\n            </td>\n            <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n              {{ projectActor.actor.surname }}\n            </td>\n            <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n              {{ projectActor.actor.email }}\n            </td>\n            <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n              <div *ngIf=\"projectActor.actor.celNr\">Cel: {{ projectActor.actor.celNr }}</div>\n              <div *ngIf=\"projectActor.actor.homeTelNr\">Home: {{ projectActor.actor.homeTelNr }}</div>\n              <div *ngIf=\"projectActor.actor.workTelNr\">Work: {{ projectActor.actor.workTelNr }}</div>\n            </td>\n            <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n              <div *ngIf=\"projectActor.actor.physicalAddress1\">{{ projectActor.actor.physicalAddress1 }}</div>\n              <div *ngIf=\"projectActor.actor.physicalAddress2\">{{ projectActor.actor.physicalAddress2 }}</div>\n              <div *ngIf=\"projectActor.actor.physicalAddress3\">{{ projectActor.actor.physicalAddress3 }}</div>\n              <div *ngIf=\"projectActor.actor.physicalAddress4\">{{ projectActor.actor.physicalAddress4 }}</div>\n            </td>\n          </tr>\n        </tbody>\n      </table>\n    </div>\n  </div>\n  <div *ngIf=\"!showCalendar\" class=\"button-section\">\n    <div *ngIf=\"provider\" class=\"button\" (click)=\"continueWithSelectedProvider()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div *ngIf=\"!provider\" class=\"button button-disabled\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n  </div>\n\n  <div *ngIf=\"showCalendar\" class=\"content\">\n    Calendar\n  </div>\n  <div *ngIf=\"showCalendar\" class=\"button-section\">\n    <div *ngIf=\"provider\" class=\"button\" (click)=\"updateTimeSlots()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div *ngIf=\"!provider\" class=\"button button-disabled\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n  </div>\n</div>\n\n\n<!--\n<loading-indicator [show]=\"showWaitIndicator\"></loading-indicator>\n\n<div *ngIf=\"!showCalendar\" class=\"row\">\n    <div class=\"col-md-12\">\n        <h4 class=\"control-label\">\n            Select Provider\n        </h4>\n    </div>\n</div>\n\n<div *ngIf=\"!showCalendar\" class=\"row\">\n    <div class=\"col-md-12\">\n        <table class=\"table table-hover table-bordered\">\n            <tr>\n                <th>Title</th>\n                <th>Firstname</th>\n                <th>Surname</th>\n                <th>E-mail</th>\n                <th>Contact Nrs</th>\n                <th>Address</th>\n            </tr>\n            <tbody *ngIf=\"tableDataPage\">\n                <tr *ngFor=\"let projectActor of tableDataPage.list\" (click)=\"selectProvider(projectActor)\">\n                    <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n                        <span *ngIf=\"projectActor.actor.title\">{{ projectActor.actor.title[\"description\"] }}</span>\n                    </td>\n                    <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n                        {{ projectActor.actor.firstname }}\n                    </td>\n                    <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n                        {{ projectActor.actor.surname }}\n                    </td>\n                    <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n                        {{ projectActor.actor.email }}\n                    </td>\n                    <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n                        <div *ngIf=\"projectActor.actor.celNr\">Cel: {{ projectActor.actor.celNr }}</div>\n                        <div *ngIf=\"projectActor.actor.homeTelNr\">Home: {{ projectActor.actor.homeTelNr }}</div>\n                        <div *ngIf=\"projectActor.actor.workTelNr\">Work: {{ projectActor.actor.workTelNr }}</div>\n                    </td>\n                    <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n                        <div *ngIf=\"projectActor.actor.physicalAddress1\">{{ projectActor.actor.physicalAddress1 }}</div>\n                        <div *ngIf=\"projectActor.actor.physicalAddress2\">{{ projectActor.actor.physicalAddress2 }}</div>\n                        <div *ngIf=\"projectActor.actor.physicalAddress3\">{{ projectActor.actor.physicalAddress3 }}</div>\n                        <div *ngIf=\"projectActor.actor.physicalAddress4\">{{ projectActor.actor.physicalAddress4 }}</div>\n                    </td>\n                </tr>\n            </tbody>\n        </table>\n    </div>\n</div>\n\n<div *ngIf=\"!showCalendar && showSelectedProviderError\" class=\"row\">\n    <div class=\"col-md-12\">\n        <div class=\"alert alert-danger\" role=\"alert\">A provider must be selected</div>\n    </div>\n</div>\n\n<div *ngIf=\"!showCalendar\" class=\"row\">\n    <div class=\"col-md-12\">\n        <button type=\"button\" class=\"btn btn-default\" (click)=\"continueWithSelectedProvider()\"\n                [ngClass]=\"{ 'disabled': !provider }\">\n            Continue\n        </button>\n    </div>\n</div>\n\n<div *ngIf=\"showCalendar\" class=\"row\">\n    <div class=\"col-md-12\">\n        <h4 class=\"control-label\">\n            Make Appointment with {{ provider.title.description }} {{ provider.firstname }} {{ provider.surname }}\n        </h4>\n    </div>\n</div>\n\n<div *ngIf=\"provider && showCalendar\" class=\"row\">\n    <div class=\"col-md-12\">\n        <h4 class=\"control-label\">\n            <calendar-control [showAddButton]=\"true\" [provider]=\"provider\" [participant]=\"stepInstance.businessProcessInstance.actor\"\n                [appointments]=\"bookedTimeSlots\" (selectionListener)=\"timeSlotSelected($event)\"></calendar-control>\n        </h4>\n    </div>\n</div>\n\n<div *ngIf=\"provider && showCalendar\" class=\"row\">\n    <div class=\"col-md-12\">\n        <button type=\"button\" class=\"btn btn-default\" (click)=\"updateTimeSlots()\">\n            Continue\n        </button>\n    </div>\n</div> -->\n",
                    styles: [".select-role2{width:100%;height:100%;position:relative}.select-role2 .heading{position:absolute;top:0;width:100%;height:32px;font-size:16px;padding:8px}.select-role2 .top-padding{padding-top:8px}.select-role2 .content{position:absolute;top:32px;left:0;bottom:50px;width:100%;overflow-y:scroll;padding:8px;box-sizing:border-box;background-color:#f9f9f9;font-size:15px}.select-role2 .content .wording{width:100%;font-size:15px;padding-bottom:5px}.select-role2 .content input{width:200;font-size:12px;border:1px solid #58666c;background-color:#fffcf6;box-sizing:border-box;padding:3px 5px;margin-bottom:5px}.select-role2 .error-text{font-style:italic;font-size:11px;color:#f04141}.select-role2 .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.select-role2 .button-section .button{width:120px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.select-role2 .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.select-role2 .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.select-role2 .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b;cursor:default}.select-role2 .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b;cursor:default}"]
                }] }
    ];
    /** @nocollapse */
    SelectRole2Component.ctorParameters = function () { return [
        { type: ProjectService }
    ]; };
    SelectRole2Component.propDecorators = {
        makeBooking: [{ type: Input }],
        stepInstance: [{ type: Input }],
        userId: [{ type: Input }],
        stepCompletedListener: [{ type: Output }]
    };
    return SelectRole2Component;
}());
export { SelectRole2Component };
if (false) {
    /** @type {?} */
    SelectRole2Component.prototype.makeBooking;
    /** @type {?} */
    SelectRole2Component.prototype.stepInstance;
    /** @type {?} */
    SelectRole2Component.prototype.userId;
    /** @type {?} */
    SelectRole2Component.prototype.stepCompletedListener;
    /** @type {?} */
    SelectRole2Component.prototype.showWaitIndicator;
    /** @type {?} */
    SelectRole2Component.prototype.projectRoles;
    /** @type {?} */
    SelectRole2Component.prototype.tableDataPage;
    /** @type {?} */
    SelectRole2Component.prototype.provider;
    /** @type {?} */
    SelectRole2Component.prototype.bookedTimeSlots;
    /** @type {?} */
    SelectRole2Component.prototype.newTimeSlots;
    /** @type {?} */
    SelectRole2Component.prototype.showCalendar;
    /** @type {?} */
    SelectRole2Component.prototype.showSelectedProviderError;
    /**
     * @type {?}
     * @private
     */
    SelectRole2Component.prototype.projectService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0LXJvbGUyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvc3RlcHMvc2VsZWN0LXJvbGUyL3NlbGVjdC1yb2xlMi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBcUIsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTFGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNoRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFPMUQ7SUFtQkUsOEJBQW9CLGNBQThCO1FBQTlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQVZ4QywwQkFBcUIsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBS3JELG9CQUFlLEdBQWUsRUFBRSxDQUFDO1FBQ2pDLGlCQUFZLEdBQWUsRUFBRSxDQUFDO1FBQzlCLGlCQUFZLEdBQVksS0FBSyxDQUFDO1FBQzlCLDhCQUF5QixHQUFZLEtBQUssQ0FBQztJQUczQyxDQUFDOzs7O0lBRUQsdUNBQVE7OztJQUFSO1FBQUEsaUJBcUJDO1FBcEJDLElBQUksSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDL0QsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQzs7Z0JBQzFCLFNBQVMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLHVCQUF1QixDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsRUFBRTs7Z0JBQ2hGLGFBQWEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsY0FBYztZQUN6RSxJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxHQUFHLEVBQUUsU0FBUyxFQUFFLGFBQWEsQ0FBQyxDQUFDLElBQUk7Ozs7WUFBQyxVQUFBLE1BQU07Z0JBQ2pGLEtBQUksQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDO2dCQUM1QixLQUFJLENBQUMsY0FBYyxDQUFDLHVCQUF1QixDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsdUJBQXVCLENBQUMsRUFBRSxFQUFFLFlBQVksQ0FBQyxDQUFDLElBQUk7Ozs7Z0JBQUMsVUFBQSxVQUFVO29CQUNySCxJQUFJLFVBQVUsRUFBRTt3QkFDZCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFOzRCQUN2RCxJQUFJLE1BQU0sQ0FBQyxVQUFVLENBQUMsS0FBSyxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFO2dDQUM5RCxLQUFJLENBQUMsUUFBUSxHQUFHLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztnQ0FDakQsTUFBTTs2QkFDUDt5QkFDRjtxQkFDRjtvQkFFRCxLQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO2dCQUNqQyxDQUFDLEVBQUMsQ0FBQztZQUNMLENBQUMsRUFBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDOzs7O0lBRUQsMENBQVc7OztJQUFYO0lBQ0EsQ0FBQzs7Ozs7SUFFRCw2Q0FBYzs7OztJQUFkLFVBQWUsUUFBc0I7UUFDbkMsSUFBSSxRQUFRLEVBQUU7WUFDWixJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUM7U0FDaEM7YUFBTTtZQUNMLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1NBQ3RCO0lBQ0gsQ0FBQzs7OztJQUVELDJEQUE0Qjs7O0lBQTVCO1FBQUEsaUJBbUJDO1FBbEJDLElBQUksQ0FBQyx5QkFBeUIsR0FBRyxLQUFLLENBQUM7UUFDdkMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDbEIsSUFBSSxDQUFDLHlCQUF5QixHQUFHLElBQUksQ0FBQztZQUN0QyxPQUFPO1NBQ1I7UUFFRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1FBQzlCLElBQUksQ0FBQyxjQUFjLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLEVBQUUsWUFBWSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsSUFBSTs7OztRQUFDLFVBQUEsTUFBTTtZQUM5SSxJQUFJLEtBQUksQ0FBQyxXQUFXLEVBQUU7Z0JBQ3BCLEtBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxLQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQyxJQUFJOzs7O2dCQUFDLFVBQUEsTUFBTTtvQkFDeEUsS0FBSSxDQUFDLGVBQWUsR0FBRyxNQUFNLENBQUM7b0JBQzlCLEtBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO29CQUN6QixLQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO2dCQUNqQyxDQUFDLEVBQUMsQ0FBQzthQUNKO2lCQUFNO2dCQUNMLEtBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQzthQUNyQjtRQUNILENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCwrQ0FBZ0I7Ozs7SUFBaEIsVUFBaUIsS0FBSztRQUNwQixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7SUFDbEMsQ0FBQzs7OztJQUVELDhDQUFlOzs7SUFBZjtRQUFBLGlCQU9DO1FBTkMsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3JCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7WUFDOUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUk7Ozs7WUFBQyxVQUFBLE1BQU07Z0JBQ2hFLEtBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztZQUN0QixDQUFDLEVBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7Ozs7SUFFTywyQ0FBWTs7OztJQUFwQjtRQUFBLGlCQVNDO1FBUkMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztRQUM5QixJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSTs7OztRQUFDLFVBQUEsTUFBTTtZQUM3RSxLQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDO2dCQUM5QixLQUFLLEVBQUUsS0FBSSxDQUFDLFlBQVk7YUFDekIsQ0FBQyxDQUFDO1lBRUgsS0FBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztRQUNqQyxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7O2dCQW5HRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGNBQWM7b0JBQ3hCLHNnUEFBNEM7O2lCQUU3Qzs7OztnQkFaUSxjQUFjOzs7OEJBY3BCLEtBQUs7K0JBQ0wsS0FBSzt5QkFDTCxLQUFLO3dDQUNMLE1BQU07O0lBMkZULDJCQUFDO0NBQUEsQUFwR0QsSUFvR0M7U0EvRlksb0JBQW9COzs7SUFDL0IsMkNBQThCOztJQUM5Qiw0Q0FBb0M7O0lBQ3BDLHNDQUF3Qjs7SUFDeEIscURBQXFEOztJQUNyRCxpREFBMkI7O0lBQzNCLDRDQUE0Qjs7SUFDNUIsNkNBQTZCOztJQUM3Qix3Q0FBZ0I7O0lBQ2hCLCtDQUFpQzs7SUFDakMsNENBQThCOztJQUM5Qiw0Q0FBOEI7O0lBQzlCLHlEQUEyQzs7Ozs7SUFFL0IsOENBQXNDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPbkluaXQsIE9uRGVzdHJveSwgT3V0cHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IFByb2plY3RTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvcHJvamVjdC5zZXJ2aWNlJztcbmltcG9ydCB7IFN0ZXBJbnN0YW5jZSB9IGZyb20gJy4uLy4uL2RvbWFpbi9zdGVwLWluc3RhbmNlJztcbmltcG9ydCB7IFByb2plY3RSb2xlIH0gZnJvbSAnLi4vLi4vZG9tYWluL3Byb2plY3Qtcm9sZSc7XG5pbXBvcnQgeyBQcm9qZWN0QWN0b3IgfSBmcm9tICcuLi8uLi9kb21haW4vcHJvamVjdC1hY3Rvcic7XG5pbXBvcnQgeyBBY3RvciB9IGZyb20gJy4uLy4uL2RvbWFpbi9hY3Rvcic7XG5pbXBvcnQgeyBUYWJsZURhdGFQYWdlIH0gZnJvbSAnLi4vLi4vZG9tYWluL3RhYmxlLWRhdGEtcGFnZSc7XG5pbXBvcnQgeyBUaW1lU2xvdCB9IGZyb20gJy4uLy4uL2RvbWFpbi90aW1lLXNsb3QnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdzZWxlY3Qtcm9sZTInLFxuICB0ZW1wbGF0ZVVybDogJy4vc2VsZWN0LXJvbGUyLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vc2VsZWN0LXJvbGUyLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgU2VsZWN0Um9sZTJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIG1ha2VCb29raW5nOiBib29sZWFuO1xuICBASW5wdXQoKSBzdGVwSW5zdGFuY2U6IFN0ZXBJbnN0YW5jZTtcbiAgQElucHV0KCkgdXNlcklkOiBudW1iZXI7XG4gIEBPdXRwdXQoKSBzdGVwQ29tcGxldGVkTGlzdGVuZXIgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIHNob3dXYWl0SW5kaWNhdG9yOiBib29sZWFuO1xuICBwcm9qZWN0Um9sZXM6IFByb2plY3RSb2xlW107XG4gIHRhYmxlRGF0YVBhZ2U6IFRhYmxlRGF0YVBhZ2U7XG4gIHByb3ZpZGVyOiBBY3RvcjtcbiAgYm9va2VkVGltZVNsb3RzOiBUaW1lU2xvdFtdID0gW107XG4gIG5ld1RpbWVTbG90czogVGltZVNsb3RbXSA9IFtdO1xuICBzaG93Q2FsZW5kYXI6IGJvb2xlYW4gPSBmYWxzZTtcbiAgc2hvd1NlbGVjdGVkUHJvdmlkZXJFcnJvcjogYm9vbGVhbiA9IGZhbHNlO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcHJvamVjdFNlcnZpY2U6IFByb2plY3RTZXJ2aWNlKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICBpZiAodGhpcy5zdGVwSW5zdGFuY2UgJiYgdGhpcy5zdGVwSW5zdGFuY2Uuc3RlcC5wYXJhbWV0ZXJzW1wiMFwiXSkge1xuICAgICAgdGhpcy5zaG93V2FpdEluZGljYXRvciA9IHRydWU7XG4gICAgICBsZXQgcHJvamVjdElkID0gdGhpcy5zdGVwSW5zdGFuY2UuYnVzaW5lc3NQcm9jZXNzSW5zdGFuY2UuYnVzaW5lc3NQcm9jZXNzLnByb2plY3QuaWQ7XG4gICAgICBsZXQgcHJvamVjdFJvbGVJZCA9IHRoaXMuc3RlcEluc3RhbmNlLnN0ZXAucGFyYW1ldGVyc1tcIjBcIl0ucGFyYW1ldGVyVmFsdWU7XG4gICAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmdldFByb2plY3RBY3RvcnMyKDAsIDEwMCwgcHJvamVjdElkLCBwcm9qZWN0Um9sZUlkKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICAgIHRoaXMudGFibGVEYXRhUGFnZSA9IHJlc3VsdDtcbiAgICAgICAgdGhpcy5wcm9qZWN0U2VydmljZS5nZXRQcm9jZXNzVmFyaWFibGVWYWx1ZSh0aGlzLnN0ZXBJbnN0YW5jZS5idXNpbmVzc1Byb2Nlc3NJbnN0YW5jZS5pZCwgXCJwcm92aWRlcklkXCIpLnRoZW4ocHJvdmlkZXJJZCA9PiB7XG4gICAgICAgICAgaWYgKHByb3ZpZGVySWQpIHtcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy50YWJsZURhdGFQYWdlLmxpc3QubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgaWYgKE51bWJlcihwcm92aWRlcklkKSA9PT0gdGhpcy50YWJsZURhdGFQYWdlLmxpc3RbaV0uYWN0b3IuaWQpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnByb3ZpZGVyID0gdGhpcy50YWJsZURhdGFQYWdlLmxpc3RbaV0uYWN0b3I7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG5cbiAgICAgICAgICB0aGlzLnNob3dXYWl0SW5kaWNhdG9yID0gZmFsc2U7XG4gICAgICAgIH0pO1xuICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgbmdPbkRlc3Ryb3koKSB7XG4gIH1cblxuICBzZWxlY3RQcm92aWRlcihwcm92aWRlcjogUHJvamVjdEFjdG9yKSB7XG4gICAgaWYgKHByb3ZpZGVyKSB7XG4gICAgICB0aGlzLnByb3ZpZGVyID0gcHJvdmlkZXIuYWN0b3I7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMucHJvdmlkZXIgPSBudWxsO1xuICAgIH1cbiAgfVxuXG4gIGNvbnRpbnVlV2l0aFNlbGVjdGVkUHJvdmlkZXIoKSB7XG4gICAgdGhpcy5zaG93U2VsZWN0ZWRQcm92aWRlckVycm9yID0gZmFsc2U7XG4gICAgaWYgKCF0aGlzLnByb3ZpZGVyKSB7XG4gICAgICB0aGlzLnNob3dTZWxlY3RlZFByb3ZpZGVyRXJyb3IgPSB0cnVlO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHRoaXMuc2hvd1dhaXRJbmRpY2F0b3IgPSB0cnVlO1xuICAgIHRoaXMucHJvamVjdFNlcnZpY2Uuc2V0UHJvY2Vzc1ZhcmlhYmxlVmFsdWUodGhpcy5zdGVwSW5zdGFuY2UuYnVzaW5lc3NQcm9jZXNzSW5zdGFuY2UuaWQsIFwicHJvdmlkZXJJZFwiLCB0aGlzLnByb3ZpZGVyLmlkLnRvU3RyaW5nKCkpLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgIGlmICh0aGlzLm1ha2VCb29raW5nKSB7XG4gICAgICAgIHRoaXMucHJvamVjdFNlcnZpY2UuZ2V0VGltZVNsb3RzKG51bGwsIHRoaXMucHJvdmlkZXIuaWQsIG51bGwpLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgICAgICB0aGlzLmJvb2tlZFRpbWVTbG90cyA9IHJlc3VsdDtcbiAgICAgICAgICB0aGlzLnNob3dDYWxlbmRhciA9IHRydWU7XG4gICAgICAgICAgdGhpcy5zaG93V2FpdEluZGljYXRvciA9IGZhbHNlO1xuICAgICAgICB9KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuY29tcGxldGVTdGVwKCk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICB0aW1lU2xvdFNlbGVjdGVkKGV2ZW50KSB7XG4gICAgdGhpcy5uZXdUaW1lU2xvdHMgPSBldmVudC52YWx1ZTtcbiAgfVxuXG4gIHVwZGF0ZVRpbWVTbG90cygpIHtcbiAgICBpZiAodGhpcy5uZXdUaW1lU2xvdHMpIHtcbiAgICAgIHRoaXMuc2hvd1dhaXRJbmRpY2F0b3IgPSB0cnVlO1xuICAgICAgdGhpcy5wcm9qZWN0U2VydmljZS51cGRhdGVUaW1lU2xvdHModGhpcy5uZXdUaW1lU2xvdHMpLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgICAgdGhpcy5jb21wbGV0ZVN0ZXAoKTtcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgY29tcGxldGVTdGVwKCkge1xuICAgIHRoaXMuc2hvd1dhaXRJbmRpY2F0b3IgPSB0cnVlO1xuICAgIHRoaXMucHJvamVjdFNlcnZpY2UuY29tcGxldGVTdGVwKHRoaXMuc3RlcEluc3RhbmNlLmlkLCB0aGlzLnVzZXJJZCkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgdGhpcy5zdGVwQ29tcGxldGVkTGlzdGVuZXIuZW1pdCh7XG4gICAgICAgIHZhbHVlOiB0aGlzLnN0ZXBJbnN0YW5jZVxuICAgICAgfSk7XG5cbiAgICAgIHRoaXMuc2hvd1dhaXRJbmRpY2F0b3IgPSBmYWxzZTtcbiAgICB9KTtcbiAgfVxufVxuIl19