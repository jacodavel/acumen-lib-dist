/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import * as momentImported from 'moment';
import { ProjectService } from '../../services/project.service';
import { StepInstance } from '../../domain/step-instance';
/** @type {?} */
var moment = momentImported;
var WaitComponent = /** @class */ (function () {
    function WaitComponent(projectService) {
        this.projectService = projectService;
        this.stepCompletedListener = new EventEmitter();
        this.cancelListener = new EventEmitter();
        this.showOverride = false;
        this.loading = false;
    }
    Object.defineProperty(WaitComponent.prototype, "stepInstance", {
        set: /**
         * @param {?} s
         * @return {?}
         */
        function (s) {
            this._stepInstance = s;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    WaitComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.projectService.getStepParameters(this._stepInstance.step.id).then((/**
         * @param {?} result
         * @return {?}
         */
        function (result) {
            _this.showOverride = result[2] && "true" === result[2];
            /** @type {?} */
            var messageParameter = result[3];
            if (messageParameter) {
                if (_this._stepInstance.wakeUpDate && messageParameter.indexOf("${completionDate}") > 0) {
                    _this.message = messageParameter.replace("${completionDate}", moment(_this._stepInstance.wakeUpDate).format("YYYY-MM-DD HH:mm:ss"));
                }
                else {
                    _this.message = messageParameter;
                }
            }
            else {
                if (_this._stepInstance.wakeUpDate) {
                    _this.message = "The process is currently waiting.  It will continue on " + moment(_this._stepInstance.wakeUpDate).format("YYYY-MM-DD HH:mm:ss") + ".";
                }
                else {
                    _this.message = "The process is currently waiting.";
                }
            }
        }));
    };
    /**
     * @return {?}
     */
    WaitComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    WaitComponent.prototype.completeStep = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.loading = true;
        this.projectService.completeStep(this._stepInstance.id, this.userId).then((/**
         * @param {?} result
         * @return {?}
         */
        function (result) {
            _this.loading = false;
            _this.stepCompletedListener.emit({
                value: _this._stepInstance
            });
        }));
    };
    /**
     * @return {?}
     */
    WaitComponent.prototype.cancel = /**
     * @return {?}
     */
    function () {
        this.cancelListener.emit({});
    };
    WaitComponent.decorators = [
        { type: Component, args: [{
                    selector: 'wait',
                    template: "<div class=\"wait-step\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div class=\"message-content\">\n    {{ message }}\n  </div>\n\n  <div *ngIf=\"!loading\" class=\"button-section\">\n    <div *ngIf=\"showOverride\" class=\"button\" (click)=\"completeStep()\">\n      <div class=\"button-text\">Override</div>\n    </div>\n    <div class=\"button button-outline\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n  <div *ngIf=\"loading\" class=\"button-section\">\n    <div *ngIf=\"showOverride\" class=\"button button-disabled\">\n      <div class=\"button-text\">Override</div>\n    </div>\n    <div class=\"button button-outline-disabled\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                    styles: [".wait-step{width:100%;height:100%;position:relative}.wait-step .message-content{position:absolute;top:0;left:0;bottom:50px;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px;box-sizing:border-box}.wait-step .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.wait-step .button-section .button{width:120px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.wait-step .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.wait-step .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.wait-step .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b;cursor:default}.wait-step .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b;cursor:default}"]
                }] }
    ];
    /** @nocollapse */
    WaitComponent.ctorParameters = function () { return [
        { type: ProjectService }
    ]; };
    WaitComponent.propDecorators = {
        _stepInstance: [{ type: Input }],
        userId: [{ type: Input }],
        stepCompletedListener: [{ type: Output }],
        cancelListener: [{ type: Output }],
        stepInstance: [{ type: Input }]
    };
    return WaitComponent;
}());
export { WaitComponent };
if (false) {
    /** @type {?} */
    WaitComponent.prototype._stepInstance;
    /** @type {?} */
    WaitComponent.prototype.userId;
    /** @type {?} */
    WaitComponent.prototype.stepCompletedListener;
    /** @type {?} */
    WaitComponent.prototype.cancelListener;
    /** @type {?} */
    WaitComponent.prototype.message;
    /** @type {?} */
    WaitComponent.prototype.showOverride;
    /** @type {?} */
    WaitComponent.prototype.loading;
    /**
     * @type {?}
     * @private
     */
    WaitComponent.prototype.projectService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2FpdC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL3N0ZXBzL3dhaXQvd2FpdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBcUIsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzFGLE9BQU8sS0FBSyxjQUFjLE1BQU0sUUFBUSxDQUFDO0FBRXpDLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNoRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7O0lBRXBELE1BQU0sR0FBRyxjQUFjO0FBRTdCO0lBbUJFLHVCQUFvQixjQUE4QjtRQUE5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFYeEMsMEJBQXFCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUMzQyxtQkFBYyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFFOUMsaUJBQVksR0FBWSxLQUFLLENBQUM7UUFDOUIsWUFBTyxHQUFZLEtBQUssQ0FBQztJQVF6QixDQUFDO0lBTkQsc0JBQ0ksdUNBQVk7Ozs7O1FBRGhCLFVBQ2lCLENBQWU7WUFDOUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUM7UUFDekIsQ0FBQzs7O09BQUE7Ozs7SUFLRCxnQ0FBUTs7O0lBQVI7UUFBQSxpQkFrQkM7UUFqQkMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJOzs7O1FBQUMsVUFBQSxNQUFNO1lBQzNFLEtBQUksQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLE1BQU0sS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7O2dCQUNsRCxnQkFBZ0IsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ2hDLElBQUksZ0JBQWdCLEVBQUU7Z0JBQ3BCLElBQUksS0FBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLElBQUksZ0JBQWdCLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLEdBQUcsQ0FBQyxFQUFFO29CQUN0RixLQUFJLENBQUMsT0FBTyxHQUFHLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsRUFBRSxNQUFNLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxNQUFNLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDO2lCQUNuSTtxQkFBTTtvQkFDTCxLQUFJLENBQUMsT0FBTyxHQUFHLGdCQUFnQixDQUFDO2lCQUNqQzthQUNGO2lCQUFNO2dCQUNMLElBQUksS0FBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUU7b0JBQ2pDLEtBQUksQ0FBQyxPQUFPLEdBQUcseURBQXlELEdBQUcsTUFBTSxDQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUMsTUFBTSxDQUFDLHFCQUFxQixDQUFDLEdBQUcsR0FBRyxDQUFDO2lCQUN0SjtxQkFBTTtvQkFDTCxLQUFJLENBQUMsT0FBTyxHQUFHLG1DQUFtQyxDQUFDO2lCQUNwRDthQUNGO1FBQ0gsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7O0lBRUQsbUNBQVc7OztJQUFYO0lBQ0EsQ0FBQzs7OztJQUVELG9DQUFZOzs7SUFBWjtRQUFBLGlCQVFDO1FBUEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxVQUFBLE1BQU07WUFDOUUsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFDckIsS0FBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQztnQkFDOUIsS0FBSyxFQUFFLEtBQUksQ0FBQyxhQUFhO2FBQzFCLENBQUMsQ0FBQztRQUNMLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVELDhCQUFNOzs7SUFBTjtRQUNFLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQ3hCLENBQUMsQ0FBQztJQUNMLENBQUM7O2dCQTFERixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLE1BQU07b0JBQ2hCLGl5QkFBb0M7O2lCQUVyQzs7OztnQkFUUSxjQUFjOzs7Z0NBV3BCLEtBQUs7eUJBQ0wsS0FBSzt3Q0FDTCxNQUFNO2lDQUNOLE1BQU07K0JBS04sS0FBSzs7SUE2Q1Isb0JBQUM7Q0FBQSxBQTNERCxJQTJEQztTQXREWSxhQUFhOzs7SUFDeEIsc0NBQXFDOztJQUNyQywrQkFBd0I7O0lBQ3hCLDhDQUFxRDs7SUFDckQsdUNBQThDOztJQUM5QyxnQ0FBZ0I7O0lBQ2hCLHFDQUE4Qjs7SUFDOUIsZ0NBQXlCOzs7OztJQU9iLHVDQUFzQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPbkRlc3Ryb3ksIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0ICogYXMgbW9tZW50SW1wb3J0ZWQgZnJvbSAnbW9tZW50JztcblxuaW1wb3J0IHsgUHJvamVjdFNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9wcm9qZWN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgU3RlcEluc3RhbmNlIH0gZnJvbSAnLi4vLi4vZG9tYWluL3N0ZXAtaW5zdGFuY2UnO1xuXG5jb25zdCBtb21lbnQgPSBtb21lbnRJbXBvcnRlZDtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnd2FpdCcsXG4gIHRlbXBsYXRlVXJsOiAnLi93YWl0LmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vd2FpdC5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIFdhaXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIF9zdGVwSW5zdGFuY2U6IFN0ZXBJbnN0YW5jZTtcbiAgQElucHV0KCkgdXNlcklkOiBudW1iZXI7XG4gIEBPdXRwdXQoKSBzdGVwQ29tcGxldGVkTGlzdGVuZXIgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBjYW5jZWxMaXN0ZW5lciA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgbWVzc2FnZTogc3RyaW5nO1xuICBzaG93T3ZlcnJpZGU6IGJvb2xlYW4gPSBmYWxzZTtcbiAgbG9hZGluZzogYm9vbGVhbiA9IGZhbHNlO1xuXG4gIEBJbnB1dCgpXG4gIHNldCBzdGVwSW5zdGFuY2UoczogU3RlcEluc3RhbmNlKSB7XG4gICAgdGhpcy5fc3RlcEluc3RhbmNlID0gcztcbiAgfVxuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcHJvamVjdFNlcnZpY2U6IFByb2plY3RTZXJ2aWNlKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmdldFN0ZXBQYXJhbWV0ZXJzKHRoaXMuX3N0ZXBJbnN0YW5jZS5zdGVwLmlkKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICB0aGlzLnNob3dPdmVycmlkZSA9IHJlc3VsdFsyXSAmJiBcInRydWVcIiA9PT0gcmVzdWx0WzJdO1xuICAgICAgbGV0IG1lc3NhZ2VQYXJhbWV0ZXIgPSByZXN1bHRbM107XG4gICAgICBpZiAobWVzc2FnZVBhcmFtZXRlcikge1xuICAgICAgICBpZiAodGhpcy5fc3RlcEluc3RhbmNlLndha2VVcERhdGUgJiYgbWVzc2FnZVBhcmFtZXRlci5pbmRleE9mKFwiJHtjb21wbGV0aW9uRGF0ZX1cIikgPiAwKSB7XG4gICAgICAgICAgdGhpcy5tZXNzYWdlID0gbWVzc2FnZVBhcmFtZXRlci5yZXBsYWNlKFwiJHtjb21wbGV0aW9uRGF0ZX1cIiwgbW9tZW50KHRoaXMuX3N0ZXBJbnN0YW5jZS53YWtlVXBEYXRlKS5mb3JtYXQoXCJZWVlZLU1NLUREIEhIOm1tOnNzXCIpKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aGlzLm1lc3NhZ2UgPSBtZXNzYWdlUGFyYW1ldGVyO1xuICAgICAgICB9XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpZiAodGhpcy5fc3RlcEluc3RhbmNlLndha2VVcERhdGUpIHtcbiAgICAgICAgICB0aGlzLm1lc3NhZ2UgPSBcIlRoZSBwcm9jZXNzIGlzIGN1cnJlbnRseSB3YWl0aW5nLiAgSXQgd2lsbCBjb250aW51ZSBvbiBcIiArIG1vbWVudCh0aGlzLl9zdGVwSW5zdGFuY2Uud2FrZVVwRGF0ZSkuZm9ybWF0KFwiWVlZWS1NTS1ERCBISDptbTpzc1wiKSArIFwiLlwiO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRoaXMubWVzc2FnZSA9IFwiVGhlIHByb2Nlc3MgaXMgY3VycmVudGx5IHdhaXRpbmcuXCI7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCkge1xuICB9XG5cbiAgY29tcGxldGVTdGVwKCkge1xuICAgIHRoaXMubG9hZGluZyA9IHRydWU7XG4gICAgdGhpcy5wcm9qZWN0U2VydmljZS5jb21wbGV0ZVN0ZXAodGhpcy5fc3RlcEluc3RhbmNlLmlkLCB0aGlzLnVzZXJJZCkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgICB0aGlzLnN0ZXBDb21wbGV0ZWRMaXN0ZW5lci5lbWl0KHtcbiAgICAgICAgdmFsdWU6IHRoaXMuX3N0ZXBJbnN0YW5jZVxuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICBjYW5jZWwoKSB7XG4gICAgdGhpcy5jYW5jZWxMaXN0ZW5lci5lbWl0KHtcbiAgICB9KTtcbiAgfVxufVxuIl19