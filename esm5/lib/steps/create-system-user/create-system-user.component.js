/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { StepInstance } from '../../domain/step-instance';
var CreateSystemUserComponent = /** @class */ (function () {
    function CreateSystemUserComponent(projectService) {
        this.projectService = projectService;
        this.stepCompletedListener = new EventEmitter();
        this.cancelListener = new EventEmitter();
    }
    /**
     * @return {?}
     */
    CreateSystemUserComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.usernameError = false;
        this.usernameExistsError = false;
        this.password1Error = false;
        this.password2Error = false;
    };
    /**
     * @return {?}
     */
    CreateSystemUserComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    CreateSystemUserComponent.prototype.completeStep = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.usernameError = false;
        this.usernameExistsError = false;
        this.password1Error = false;
        this.password2Error = false;
        if (!this.username) {
            this.usernameError = true;
            return;
        }
        if (!this.password1) {
            this.password1Error = true;
            return;
        }
        if (this.password1 !== this.password2) {
            this.password2Error = true;
            return;
        }
        this.projectService.createSystemUser(this.username, this.password1, this.stepInstance.businessProcessInstance.actor.id).then((/**
         * @param {?} result
         * @return {?}
         */
        function (result) {
            if (result["response"] === "Success") {
                _this.projectService.completeStep(_this.stepInstance.id, _this.userId).then((/**
                 * @param {?} result
                 * @return {?}
                 */
                function (result) {
                    _this.stepCompletedListener.emit({
                        value: _this.stepInstance,
                        useBusinessProcessId: true
                    });
                }));
            }
            else {
                _this.usernameExistsError = true;
            }
        }));
    };
    /**
     * @return {?}
     */
    CreateSystemUserComponent.prototype.cancel = /**
     * @return {?}
     */
    function () {
        this.cancelListener.emit({});
    };
    CreateSystemUserComponent.decorators = [
        { type: Component, args: [{
                    selector: 'create-system-user',
                    template: "<div class=\"create-system-user\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div class=\"heading\">\n    Create System User\n  </div>\n\n  <div *ngIf=\"!loading\" class=\"content\">\n    <div class=\"component-row\">\n      <div class=\"component-col\">\n        <div class=\"component-label\">\n          Username\n          <div *ngIf=\"usernameError\" class=\"error-message\">A username must be entered</div>\n          <div *ngIf=\"usernameExistsError\" class=\"error-message\">The username already exists</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"text\" [(ngModel)]=\"username\">\n        </div>\n      </div>\n    </div>\n\n    <div class=\"component-row\">\n      <div class=\"component-col\">\n        <div class=\"component-label\">\n          Password\n          <div *ngIf=\"password1Error\" class=\"error-message\">A password be entered</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"password\" [(ngModel)]=\"password1\">\n        </div>\n      </div>\n      <div class=\"component-col\">\n        <div class=\"component-label\">\n          Re-type password\n          <div *ngIf=\"password2Error\" class=\"error-message\">The second password differs from the first password</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"password\" [(ngModel)]=\"password2\">\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!loading\" class=\"button-section\">\n    <div class=\"button\" (click)=\"completeStep()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div class=\"button button-outline\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n  <div *ngIf=\"loading\" class=\"button-section\">\n    <div class=\"button button-disabled\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div class=\"button button-outline-disabled\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                    styles: [".create-system-user{width:100%;height:100%;position:relative}.create-system-user .heading{position:absolute;top:0;width:100%;height:32px;font-size:16px;padding:8px}.create-system-user .content{position:absolute;top:32px;left:0;bottom:50px;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px;box-sizing:border-box}.create-system-user .content .info-message{font-size:15px}.create-system-user .content .component-row{width:100%}.create-system-user .content .component-row .component-col{width:100%;display:inline-block;box-sizing:border-box;padding-top:10px}.create-system-user .content .component-row .component-col .component-label{width:100%;font-size:15px;padding-bottom:5px}.create-system-user .content .component-row .component-col .error-message{font-style:italic;font-size:11px;color:#f04141;padding-bottom:5px}.create-system-user .content .component-row .component-col .component{width:100%}.create-system-user .content .component-row .component-col .component input{width:100%;font-size:13px;border:1px solid #58666c;background-color:#fffcf6;box-sizing:border-box;padding:3px 5px;border-radius:5px;height:30px}.create-system-user .content .component-row:first-child .component-col:first-child{padding-top:0}@media (min-width:600px){.create-system-user .content .component-row .component-col{width:50%}.create-system-user .content .component-row .component-col:first-child{padding-right:16px}.create-system-user .content .component-row:first-child .component-col{padding-top:0}}.create-system-user .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.create-system-user .button-section .button{width:70px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.create-system-user .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.create-system-user .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.create-system-user .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b}.create-system-user .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b}"]
                }] }
    ];
    /** @nocollapse */
    CreateSystemUserComponent.ctorParameters = function () { return [
        { type: ProjectService }
    ]; };
    CreateSystemUserComponent.propDecorators = {
        stepInstance: [{ type: Input }],
        userId: [{ type: Input }],
        stepCompletedListener: [{ type: Output }],
        cancelListener: [{ type: Output }]
    };
    return CreateSystemUserComponent;
}());
export { CreateSystemUserComponent };
if (false) {
    /** @type {?} */
    CreateSystemUserComponent.prototype.stepInstance;
    /** @type {?} */
    CreateSystemUserComponent.prototype.userId;
    /** @type {?} */
    CreateSystemUserComponent.prototype.stepCompletedListener;
    /** @type {?} */
    CreateSystemUserComponent.prototype.cancelListener;
    /** @type {?} */
    CreateSystemUserComponent.prototype.username;
    /** @type {?} */
    CreateSystemUserComponent.prototype.password1;
    /** @type {?} */
    CreateSystemUserComponent.prototype.password2;
    /** @type {?} */
    CreateSystemUserComponent.prototype.loading;
    /** @type {?} */
    CreateSystemUserComponent.prototype.usernameError;
    /** @type {?} */
    CreateSystemUserComponent.prototype.usernameExistsError;
    /** @type {?} */
    CreateSystemUserComponent.prototype.password1Error;
    /** @type {?} */
    CreateSystemUserComponent.prototype.password2Error;
    /**
     * @type {?}
     * @private
     */
    CreateSystemUserComponent.prototype.projectService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3JlYXRlLXN5c3RlbS11c2VyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvc3RlcHMvY3JlYXRlLXN5c3RlbS11c2VyL2NyZWF0ZS1zeXN0ZW0tdXNlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBcUIsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTFGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNoRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFFMUQ7SUFvQkUsbUNBQW9CLGNBQThCO1FBQTlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQVp4QywwQkFBcUIsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQzNDLG1CQUFjLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQVk5QyxDQUFDOzs7O0lBRUQsNENBQVE7OztJQUFSO1FBQ0UsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7UUFDM0IsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUssQ0FBQztRQUNqQyxJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztRQUM1QixJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztJQUM5QixDQUFDOzs7O0lBRUQsK0NBQVc7OztJQUFYO0lBQ0EsQ0FBQzs7OztJQUVELGdEQUFZOzs7SUFBWjtRQUFBLGlCQWlDQztRQWhDQyxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsbUJBQW1CLEdBQUcsS0FBSyxDQUFDO1FBQ2pDLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1FBQzVCLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1FBRTVCLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2xCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1lBQzFCLE9BQU87U0FDUjtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ25CLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1lBQzNCLE9BQU87U0FDUjtRQUVELElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ3JDLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1lBQzNCLE9BQU87U0FDUjtRQUVELElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsdUJBQXVCLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxVQUFBLE1BQU07WUFDakksSUFBSSxNQUFNLENBQUMsVUFBVSxDQUFDLEtBQUssU0FBUyxFQUFFO2dCQUNwQyxLQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsRUFBRSxLQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSTs7OztnQkFBQyxVQUFBLE1BQU07b0JBQzdFLEtBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUM7d0JBQzlCLEtBQUssRUFBRSxLQUFJLENBQUMsWUFBWTt3QkFDeEIsb0JBQW9CLEVBQUUsSUFBSTtxQkFDM0IsQ0FBQyxDQUFDO2dCQUNMLENBQUMsRUFBQyxDQUFDO2FBQ0o7aUJBQU07Z0JBQ0wsS0FBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQzthQUNqQztRQUNILENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVELDBDQUFNOzs7SUFBTjtRQUNFLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQ3hCLENBQUMsQ0FBQztJQUNMLENBQUM7O2dCQXZFRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtvQkFDOUIsNmdFQUFrRDs7aUJBRW5EOzs7O2dCQVBRLGNBQWM7OzsrQkFTcEIsS0FBSzt5QkFDTCxLQUFLO3dDQUNMLE1BQU07aUNBQ04sTUFBTTs7SUErRFQsZ0NBQUM7Q0FBQSxBQXhFRCxJQXdFQztTQW5FWSx5QkFBeUI7OztJQUNwQyxpREFBb0M7O0lBQ3BDLDJDQUF3Qjs7SUFDeEIsMERBQXFEOztJQUNyRCxtREFBOEM7O0lBQzlDLDZDQUFpQjs7SUFDakIsOENBQWtCOztJQUNsQiw4Q0FBa0I7O0lBRWxCLDRDQUFpQjs7SUFDakIsa0RBQXVCOztJQUN2Qix3REFBNkI7O0lBQzdCLG1EQUF3Qjs7SUFDeEIsbURBQXdCOzs7OztJQUVaLG1EQUFzQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPbkRlc3Ryb3ksIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBQcm9qZWN0U2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL3Byb2plY3Quc2VydmljZSc7XG5pbXBvcnQgeyBTdGVwSW5zdGFuY2UgfSBmcm9tICcuLi8uLi9kb21haW4vc3RlcC1pbnN0YW5jZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2NyZWF0ZS1zeXN0ZW0tdXNlcicsXG4gIHRlbXBsYXRlVXJsOiAnLi9jcmVhdGUtc3lzdGVtLXVzZXIuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9jcmVhdGUtc3lzdGVtLXVzZXIuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBDcmVhdGVTeXN0ZW1Vc2VyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuICBASW5wdXQoKSBzdGVwSW5zdGFuY2U6IFN0ZXBJbnN0YW5jZTtcbiAgQElucHV0KCkgdXNlcklkOiBudW1iZXI7XG4gIEBPdXRwdXQoKSBzdGVwQ29tcGxldGVkTGlzdGVuZXIgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBjYW5jZWxMaXN0ZW5lciA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgdXNlcm5hbWU6IHN0cmluZztcbiAgcGFzc3dvcmQxOiBzdHJpbmc7XG4gIHBhc3N3b3JkMjogc3RyaW5nO1xuXG4gIGxvYWRpbmc6IGJvb2xlYW47XG4gIHVzZXJuYW1lRXJyb3I6IGJvb2xlYW47XG4gIHVzZXJuYW1lRXhpc3RzRXJyb3I6IGJvb2xlYW47XG4gIHBhc3N3b3JkMUVycm9yOiBib29sZWFuO1xuICBwYXNzd29yZDJFcnJvcjogYm9vbGVhbjtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHByb2plY3RTZXJ2aWNlOiBQcm9qZWN0U2VydmljZSkge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy51c2VybmFtZUVycm9yID0gZmFsc2U7XG4gICAgdGhpcy51c2VybmFtZUV4aXN0c0Vycm9yID0gZmFsc2U7XG4gICAgdGhpcy5wYXNzd29yZDFFcnJvciA9IGZhbHNlO1xuICAgIHRoaXMucGFzc3dvcmQyRXJyb3IgPSBmYWxzZTtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCkge1xuICB9XG5cbiAgY29tcGxldGVTdGVwKCkge1xuICAgIHRoaXMudXNlcm5hbWVFcnJvciA9IGZhbHNlO1xuICAgIHRoaXMudXNlcm5hbWVFeGlzdHNFcnJvciA9IGZhbHNlO1xuICAgIHRoaXMucGFzc3dvcmQxRXJyb3IgPSBmYWxzZTtcbiAgICB0aGlzLnBhc3N3b3JkMkVycm9yID0gZmFsc2U7XG5cbiAgICBpZiAoIXRoaXMudXNlcm5hbWUpIHtcbiAgICAgIHRoaXMudXNlcm5hbWVFcnJvciA9IHRydWU7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKCF0aGlzLnBhc3N3b3JkMSkge1xuICAgICAgdGhpcy5wYXNzd29yZDFFcnJvciA9IHRydWU7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKHRoaXMucGFzc3dvcmQxICE9PSB0aGlzLnBhc3N3b3JkMikge1xuICAgICAgdGhpcy5wYXNzd29yZDJFcnJvciA9IHRydWU7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy5wcm9qZWN0U2VydmljZS5jcmVhdGVTeXN0ZW1Vc2VyKHRoaXMudXNlcm5hbWUsIHRoaXMucGFzc3dvcmQxLCB0aGlzLnN0ZXBJbnN0YW5jZS5idXNpbmVzc1Byb2Nlc3NJbnN0YW5jZS5hY3Rvci5pZCkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgaWYgKHJlc3VsdFtcInJlc3BvbnNlXCJdID09PSBcIlN1Y2Nlc3NcIikge1xuICAgICAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmNvbXBsZXRlU3RlcCh0aGlzLnN0ZXBJbnN0YW5jZS5pZCwgdGhpcy51c2VySWQpLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgICAgICB0aGlzLnN0ZXBDb21wbGV0ZWRMaXN0ZW5lci5lbWl0KHtcbiAgICAgICAgICAgIHZhbHVlOiB0aGlzLnN0ZXBJbnN0YW5jZSxcbiAgICAgICAgICAgIHVzZUJ1c2luZXNzUHJvY2Vzc0lkOiB0cnVlXG4gICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy51c2VybmFtZUV4aXN0c0Vycm9yID0gdHJ1ZTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIGNhbmNlbCgpIHtcbiAgICB0aGlzLmNhbmNlbExpc3RlbmVyLmVtaXQoe1xuICAgIH0pO1xuICB9XG59XG4iXX0=