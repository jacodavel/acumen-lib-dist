/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { StepInstance } from '../../domain/step-instance';
var FacialAuthenticationComponent = /** @class */ (function () {
    function FacialAuthenticationComponent(projectService) {
        this.projectService = projectService;
        this.onOpenCameraListener = new EventEmitter();
        this.stepCompletedListener = new EventEmitter();
        this.cancelListener = new EventEmitter();
        this.loading = false;
        this.imageContainer = {
            imageData: null,
            setImage: /**
             * @param {?} image
             * @return {?}
             */
            function (image) {
                this.imageData = image;
            }
        };
    }
    Object.defineProperty(FacialAuthenticationComponent.prototype, "stepInstance", {
        set: /**
         * @param {?} s
         * @return {?}
         */
        function (s) {
            this._stepInstance = s;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    FacialAuthenticationComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (!this.uploadText) {
            this.uploadText = "Open camera";
        }
    };
    /**
     * @return {?}
     */
    FacialAuthenticationComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    FacialAuthenticationComponent.prototype.openCamera = /**
     * @return {?}
     */
    function () {
        this.onOpenCameraListener.emit(this.imageContainer);
    };
    /**
     * @return {?}
     */
    FacialAuthenticationComponent.prototype.retake = /**
     * @return {?}
     */
    function () {
        this.imageContainer.imageData = null;
        this.onOpenCameraListener.emit(this.imageContainer);
    };
    /**
     * @return {?}
     */
    FacialAuthenticationComponent.prototype.completeStep = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.loading = true;
        this.projectService.facialAuthentication(this._stepInstance.id, this.imageContainer.imageData).then((/**
         * @param {?} imageUpdateResult
         * @return {?}
         */
        function (imageUpdateResult) {
            _this.projectService.completeStep(_this._stepInstance.id, _this.userId).then((/**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                _this.loading = false;
                _this.stepCompletedListener.emit({
                    value: _this._stepInstance
                });
            })).catch((/**
             * @param {?} error
             * @return {?}
             */
            function (error) {
                _this.loading = false;
                _this.stepCompletedListener.emit({
                    value: _this._stepInstance
                });
            }));
        }));
    };
    /**
     * @return {?}
     */
    FacialAuthenticationComponent.prototype.cancel = /**
     * @return {?}
     */
    function () {
        this.cancelListener.emit({});
    };
    FacialAuthenticationComponent.decorators = [
        { type: Component, args: [{
                    selector: 'facial-authentication',
                    template: "<div class=\"facial-authentication-step\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div *ngIf=\"!imageContainer.imageData\" class=\"message-content\">\n    Please provide an image that will be used for facial authentication.\n  </div>\n  <div *ngIf=\"imageContainer.imageData\" class=\"image-content\">\n    <img src=\"{{ 'data:image/jpeg;base64,' + imageContainer.imageData }}\">\n  </div>\n\n  <div *ngIf=\"!loading\" class=\"button-section\">\n    <div *ngIf=\"!imageContainer.imageData\" class=\"button\" (click)=\"openCamera()\">\n      <div class=\"button-text\">{{ uploadText }}</div>\n    </div>\n    <div *ngIf=\"imageContainer.imageData\" class=\"button\" (click)=\"completeStep()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div *ngIf=\"imageContainer.imageData\" class=\"button button-outline\" (click)=\"retake()\">\n      <div class=\"button-text\">Retake</div>\n    </div>\n    <div class=\"button button-outline\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n  <div *ngIf=\"loading\" class=\"button-section\">\n    <div *ngIf=\"!imageContainer.imageData\" class=\"button button-disabled\">\n      <div class=\"button-text\">{{ uploadText }}</div>\n    </div>\n    <div *ngIf=\"imageContainer.imageData\" class=\"button button-disabled\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div *ngIf=\"imageContainer.imageData\" class=\"button button-outline-disabled\">\n      <div class=\"button-text\">Retake</div>\n    </div>\n    <div class=\"button button-outline-disabled\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                    styles: [".facial-authentication-step{width:100%;height:100%;position:relative}.facial-authentication-step .message-content{position:absolute;top:0;left:0;bottom:50px;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px;box-sizing:border-box}.facial-authentication-step .image-content{position:absolute;top:0;left:0;bottom:50px;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px;box-sizing:border-box;text-align:center}.facial-authentication-step .image-content img{max-width:100%;max-height:100%}.facial-authentication-step .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.facial-authentication-step .button-section .button{width:100px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.facial-authentication-step .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.facial-authentication-step .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.facial-authentication-step .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b;cursor:default}.facial-authentication-step .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b;cursor:default}"]
                }] }
    ];
    /** @nocollapse */
    FacialAuthenticationComponent.ctorParameters = function () { return [
        { type: ProjectService }
    ]; };
    FacialAuthenticationComponent.propDecorators = {
        _stepInstance: [{ type: Input }],
        userId: [{ type: Input }],
        uploadText: [{ type: Input }],
        onOpenCameraListener: [{ type: Output }],
        stepCompletedListener: [{ type: Output }],
        cancelListener: [{ type: Output }],
        stepInstance: [{ type: Input }]
    };
    return FacialAuthenticationComponent;
}());
export { FacialAuthenticationComponent };
if (false) {
    /** @type {?} */
    FacialAuthenticationComponent.prototype._stepInstance;
    /** @type {?} */
    FacialAuthenticationComponent.prototype.userId;
    /** @type {?} */
    FacialAuthenticationComponent.prototype.uploadText;
    /** @type {?} */
    FacialAuthenticationComponent.prototype.onOpenCameraListener;
    /** @type {?} */
    FacialAuthenticationComponent.prototype.stepCompletedListener;
    /** @type {?} */
    FacialAuthenticationComponent.prototype.cancelListener;
    /** @type {?} */
    FacialAuthenticationComponent.prototype.loading;
    /** @type {?} */
    FacialAuthenticationComponent.prototype.imageContainer;
    /**
     * @type {?}
     * @private
     */
    FacialAuthenticationComponent.prototype.projectService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFjaWFsLWF1dGhlbnRpY2F0aW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvc3RlcHMvZmFjaWFsLWF1dGhlbnRpY2F0aW9uL2ZhY2lhbC1hdXRoZW50aWNhdGlvbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBcUIsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTFGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNoRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFHMUQ7SUEyQkUsdUNBQW9CLGNBQThCO1FBQTlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQWxCeEMseUJBQW9CLEdBQUcsSUFBSSxZQUFZLEVBQWtCLENBQUM7UUFDMUQsMEJBQXFCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUMzQyxtQkFBYyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDOUMsWUFBTyxHQUFZLEtBQUssQ0FBQztRQUV6QixtQkFBYyxHQUFtQjtZQUMvQixTQUFTLEVBQUUsSUFBSTtZQUVmLFFBQVE7Ozs7c0JBQUMsS0FBYTtnQkFDcEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7WUFDekIsQ0FBQztTQUNGLENBQUM7SUFRRixDQUFDO0lBTkQsc0JBQ0ksdURBQVk7Ozs7O1FBRGhCLFVBQ2lCLENBQWU7WUFDOUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUM7UUFDekIsQ0FBQzs7O09BQUE7Ozs7SUFLRCxnREFBUTs7O0lBQVI7UUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNwQixJQUFJLENBQUMsVUFBVSxHQUFHLGFBQWEsQ0FBQztTQUNqQztJQUNILENBQUM7Ozs7SUFFRCxtREFBVzs7O0lBQVg7SUFDQSxDQUFDOzs7O0lBRUQsa0RBQVU7OztJQUFWO1FBQ0UsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDdEQsQ0FBQzs7OztJQUVELDhDQUFNOzs7SUFBTjtRQUNFLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUNyQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUN0RCxDQUFDOzs7O0lBRUQsb0RBQVk7OztJQUFaO1FBQUEsaUJBZUM7UUFkQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNwQixJQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSTs7OztRQUFDLFVBQUEsaUJBQWlCO1lBQ25ILEtBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxFQUFFLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJOzs7O1lBQUMsVUFBQSxNQUFNO2dCQUM5RSxLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDckIsS0FBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQztvQkFDOUIsS0FBSyxFQUFFLEtBQUksQ0FBQyxhQUFhO2lCQUMxQixDQUFDLENBQUM7WUFDTCxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1lBQUMsVUFBQSxLQUFLO2dCQUNaLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO2dCQUNyQixLQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDO29CQUM5QixLQUFLLEVBQUUsS0FBSSxDQUFDLGFBQWE7aUJBQzFCLENBQUMsQ0FBQztZQUNMLENBQUMsRUFBQyxDQUFDO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7O0lBRUQsOENBQU07OztJQUFOO1FBQ0UsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFDeEIsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7Z0JBcEVGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsdUJBQXVCO29CQUNqQyxvcURBQXFEOztpQkFFdEQ7Ozs7Z0JBUlEsY0FBYzs7O2dDQVVwQixLQUFLO3lCQUNMLEtBQUs7NkJBQ0wsS0FBSzt1Q0FDTCxNQUFNO3dDQUNOLE1BQU07aUNBQ04sTUFBTTsrQkFXTixLQUFLOztJQStDUixvQ0FBQztDQUFBLEFBckVELElBcUVDO1NBaEVZLDZCQUE2Qjs7O0lBQ3hDLHNEQUFxQzs7SUFDckMsK0NBQXdCOztJQUN4QixtREFBNEI7O0lBQzVCLDZEQUFvRTs7SUFDcEUsOERBQXFEOztJQUNyRCx1REFBOEM7O0lBQzlDLGdEQUF5Qjs7SUFFekIsdURBTUU7Ozs7O0lBT1UsdURBQXNDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPbkluaXQsIE9uRGVzdHJveSwgT3V0cHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IFByb2plY3RTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvcHJvamVjdC5zZXJ2aWNlJztcbmltcG9ydCB7IFN0ZXBJbnN0YW5jZSB9IGZyb20gJy4uLy4uL2RvbWFpbi9zdGVwLWluc3RhbmNlJztcbmltcG9ydCB7IEltYWdlQ29udGFpbmVyIH0gZnJvbSAnLi4vLi4vZG9tYWluL2ltYWdlLWNvbnRhaW5lcic7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2ZhY2lhbC1hdXRoZW50aWNhdGlvbicsXG4gIHRlbXBsYXRlVXJsOiAnLi9mYWNpYWwtYXV0aGVudGljYXRpb24uY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9mYWNpYWwtYXV0aGVudGljYXRpb24uY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBGYWNpYWxBdXRoZW50aWNhdGlvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgQElucHV0KCkgX3N0ZXBJbnN0YW5jZTogU3RlcEluc3RhbmNlO1xuICBASW5wdXQoKSB1c2VySWQ6IG51bWJlcjtcbiAgQElucHV0KCkgdXBsb2FkVGV4dDogc3RyaW5nO1xuICBAT3V0cHV0KCkgb25PcGVuQ2FtZXJhTGlzdGVuZXIgPSBuZXcgRXZlbnRFbWl0dGVyPEltYWdlQ29udGFpbmVyPigpO1xuICBAT3V0cHV0KCkgc3RlcENvbXBsZXRlZExpc3RlbmVyID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgY2FuY2VsTGlzdGVuZXIgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIGxvYWRpbmc6IGJvb2xlYW4gPSBmYWxzZTtcblxuICBpbWFnZUNvbnRhaW5lcjogSW1hZ2VDb250YWluZXIgPSB7XG4gICAgaW1hZ2VEYXRhOiBudWxsLFxuXG4gICAgc2V0SW1hZ2UoaW1hZ2U6IHN0cmluZykge1xuICAgICAgdGhpcy5pbWFnZURhdGEgPSBpbWFnZTtcbiAgICB9XG4gIH07XG5cbiAgQElucHV0KClcbiAgc2V0IHN0ZXBJbnN0YW5jZShzOiBTdGVwSW5zdGFuY2UpIHtcbiAgICB0aGlzLl9zdGVwSW5zdGFuY2UgPSBzO1xuICB9XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBwcm9qZWN0U2VydmljZTogUHJvamVjdFNlcnZpY2UpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIGlmICghdGhpcy51cGxvYWRUZXh0KSB7XG4gICAgICB0aGlzLnVwbG9hZFRleHQgPSBcIk9wZW4gY2FtZXJhXCI7XG4gICAgfVxuICB9XG5cbiAgbmdPbkRlc3Ryb3koKSB7XG4gIH1cblxuICBvcGVuQ2FtZXJhKCkge1xuICAgIHRoaXMub25PcGVuQ2FtZXJhTGlzdGVuZXIuZW1pdCh0aGlzLmltYWdlQ29udGFpbmVyKTtcbiAgfVxuXG4gIHJldGFrZSgpIHtcbiAgICB0aGlzLmltYWdlQ29udGFpbmVyLmltYWdlRGF0YSA9IG51bGw7XG4gICAgdGhpcy5vbk9wZW5DYW1lcmFMaXN0ZW5lci5lbWl0KHRoaXMuaW1hZ2VDb250YWluZXIpO1xuICB9XG5cbiAgY29tcGxldGVTdGVwKCkge1xuICAgIHRoaXMubG9hZGluZyA9IHRydWU7XG4gICAgdGhpcy5wcm9qZWN0U2VydmljZS5mYWNpYWxBdXRoZW50aWNhdGlvbih0aGlzLl9zdGVwSW5zdGFuY2UuaWQsIHRoaXMuaW1hZ2VDb250YWluZXIuaW1hZ2VEYXRhKS50aGVuKGltYWdlVXBkYXRlUmVzdWx0ID0+IHtcbiAgICAgIHRoaXMucHJvamVjdFNlcnZpY2UuY29tcGxldGVTdGVwKHRoaXMuX3N0ZXBJbnN0YW5jZS5pZCwgdGhpcy51c2VySWQpLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgIHRoaXMuc3RlcENvbXBsZXRlZExpc3RlbmVyLmVtaXQoe1xuICAgICAgICAgIHZhbHVlOiB0aGlzLl9zdGVwSW5zdGFuY2VcbiAgICAgICAgfSk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICB0aGlzLnN0ZXBDb21wbGV0ZWRMaXN0ZW5lci5lbWl0KHtcbiAgICAgICAgICB2YWx1ZTogdGhpcy5fc3RlcEluc3RhbmNlXG4gICAgICAgIH0pO1xuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICBjYW5jZWwoKSB7XG4gICAgdGhpcy5jYW5jZWxMaXN0ZW5lci5lbWl0KHtcbiAgICB9KTtcbiAgfVxufVxuIl19