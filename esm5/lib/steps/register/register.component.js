/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { StepInstance } from '../../domain/step-instance';
var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(projectService) {
        this.projectService = projectService;
        this.stepCompletedListener = new EventEmitter();
        this.cancelListener = new EventEmitter();
        this.agreedError = false;
        this.loading = false;
        this.agreeWording = "I agree to the terms and conditions as stated above.";
    }
    /**
     * @return {?}
     */
    RegisterComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.agreed = false;
        this.agreedError = false;
        this.loading = true;
        this.projectService.getStepParameters(this.stepInstance.step.id).then((/**
         * @param {?} result
         * @return {?}
         */
        function (result) {
            _this.loading = false;
            _this.html = result["0"]; // && this.stepInstance.step.parameters["0"].parameterValue;
        }));
    };
    /**
     * @return {?}
     */
    RegisterComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    RegisterComponent.prototype.completeStep = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.agreedError = false;
        if (!this.agreed) {
            this.agreedError = true;
            return;
        }
        this.loading = true;
        this.projectService.completeStep(this.stepInstance.id, this.userId).then((/**
         * @param {?} result
         * @return {?}
         */
        function (result) {
            _this.loading = false;
            _this.stepCompletedListener.emit({
                value: _this.stepInstance,
                useBusinessProcessId: true
            });
        }));
    };
    /**
     * @param {?} $selected
     * @return {?}
     */
    RegisterComponent.prototype.radioButtonChanged = /**
     * @param {?} $selected
     * @return {?}
     */
    function ($selected) {
        this.agreed = $selected.value;
    };
    /**
     * @return {?}
     */
    RegisterComponent.prototype.cancel = /**
     * @return {?}
     */
    function () {
        this.cancelListener.emit({});
    };
    RegisterComponent.decorators = [
        { type: Component, args: [{
                    selector: 'register',
                    template: "<div class=\"register\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div class=\"heading\">\n    Register\n  </div>\n\n  <div class=\"content\">\n    <div *ngIf=\"html\" class=\"message\" [innerHTML]=\"html\">>\n    </div>\n    <div class=\"agree-checkbox\">\n      <checkbox-component [selected]=\"agreed\" (selectionListener)=\"radioButtonChanged($event);\"\n          [description]=\"agreeWording\"></checkbox-component>\n      <!-- <checkbox-component [selected]=\"agreed\" (selectionListener)=\"radioButtonChanged($event);\"\n        [description]=\"agreeWording\"></checkbox-component> -->\n    </div>\n    <div *ngIf=\"agreedError\" class=\"agree-error\">\n      You must agree to the terms and conditions to register.\n    </div>\n  </div>\n\n  <div *ngIf=\"!loading\" class=\"button-section\">\n    <div class=\"button\" (click)=\"completeStep()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div class=\"button button-outline\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n  <div *ngIf=\"loading\" class=\"button-section\">\n    <div class=\"button button-disabled\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div class=\"button button-outline-disabled\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                    styles: [".register{width:100%;height:100%;position:relative}.register .heading{position:absolute;top:0;width:100%;height:32px;font-size:16px;padding:8px}.register .content{position:absolute;top:32px;left:0;bottom:50px;width:100%;overflow-y:scroll;padding:8px;box-sizing:border-box;background-color:#f9f9f9}.register .content .agree-checkbox{width:100%;font-size:15px;padding-bottom:5px}.register .content .agree-error{font-style:italic;font-size:11px;color:#f04141;padding-bottom:5px}.register .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.register .button-section .button{width:70px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.register .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.register .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.register .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b}.register .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b}"]
                }] }
    ];
    /** @nocollapse */
    RegisterComponent.ctorParameters = function () { return [
        { type: ProjectService }
    ]; };
    RegisterComponent.propDecorators = {
        stepInstance: [{ type: Input }],
        userId: [{ type: Input }],
        stepCompletedListener: [{ type: Output }],
        cancelListener: [{ type: Output }]
    };
    return RegisterComponent;
}());
export { RegisterComponent };
if (false) {
    /** @type {?} */
    RegisterComponent.prototype.stepInstance;
    /** @type {?} */
    RegisterComponent.prototype.userId;
    /** @type {?} */
    RegisterComponent.prototype.stepCompletedListener;
    /** @type {?} */
    RegisterComponent.prototype.cancelListener;
    /** @type {?} */
    RegisterComponent.prototype.html;
    /** @type {?} */
    RegisterComponent.prototype.agreed;
    /** @type {?} */
    RegisterComponent.prototype.agreedError;
    /** @type {?} */
    RegisterComponent.prototype.agreeWording;
    /** @type {?} */
    RegisterComponent.prototype.loading;
    /**
     * @type {?}
     * @private
     */
    RegisterComponent.prototype.projectService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVnaXN0ZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWN1bWVuLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9zdGVwcy9yZWdpc3Rlci9yZWdpc3Rlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBcUIsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTFGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNoRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFFMUQ7SUFnQkUsMkJBQW9CLGNBQThCO1FBQTlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQVJ4QywwQkFBcUIsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQzNDLG1CQUFjLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUc5QyxnQkFBVyxHQUFZLEtBQUssQ0FBQztRQUU3QixZQUFPLEdBQVksS0FBSyxDQUFDO1FBR3ZCLElBQUksQ0FBQyxZQUFZLEdBQUcsc0RBQXNELENBQUM7SUFDN0UsQ0FBQzs7OztJQUVELG9DQUFROzs7SUFBUjtRQUFBLGlCQVFDO1FBUEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7UUFDekIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJOzs7O1FBQUMsVUFBQSxNQUFNO1lBQzFFLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUEsNERBQTREO1FBQ3RGLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVELHVDQUFXOzs7SUFBWDtJQUNBLENBQUM7Ozs7SUFFRCx3Q0FBWTs7O0lBQVo7UUFBQSxpQkFlQztRQWRDLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2hCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1lBQ3hCLE9BQU87U0FDUjtRQUVELElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJOzs7O1FBQUMsVUFBQSxNQUFNO1lBQzdFLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUM7Z0JBQzlCLEtBQUssRUFBRSxLQUFJLENBQUMsWUFBWTtnQkFDeEIsb0JBQW9CLEVBQUUsSUFBSTthQUMzQixDQUFDLENBQUM7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsOENBQWtCOzs7O0lBQWxCLFVBQW1CLFNBQVM7UUFDMUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDO0lBQ2hDLENBQUM7Ozs7SUFFRCxrQ0FBTTs7O0lBQU47UUFDRSxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxFQUN4QixDQUFDLENBQUM7SUFDTCxDQUFDOztnQkF6REYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxVQUFVO29CQUNwQiw2MUNBQXdDOztpQkFFekM7Ozs7Z0JBUFEsY0FBYzs7OytCQVNwQixLQUFLO3lCQUNMLEtBQUs7d0NBQ0wsTUFBTTtpQ0FDTixNQUFNOztJQWlEVCx3QkFBQztDQUFBLEFBMURELElBMERDO1NBckRZLGlCQUFpQjs7O0lBQzVCLHlDQUFvQzs7SUFDcEMsbUNBQXdCOztJQUN4QixrREFBcUQ7O0lBQ3JELDJDQUE4Qzs7SUFDOUMsaUNBQWE7O0lBQ2IsbUNBQWdCOztJQUNoQix3Q0FBNkI7O0lBQzdCLHlDQUFxQjs7SUFDckIsb0NBQXlCOzs7OztJQUViLDJDQUFzQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPbkRlc3Ryb3ksIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBQcm9qZWN0U2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL3Byb2plY3Quc2VydmljZSc7XG5pbXBvcnQgeyBTdGVwSW5zdGFuY2UgfSBmcm9tICcuLi8uLi9kb21haW4vc3RlcC1pbnN0YW5jZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3JlZ2lzdGVyJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3JlZ2lzdGVyLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vcmVnaXN0ZXIuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBSZWdpc3RlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgQElucHV0KCkgc3RlcEluc3RhbmNlOiBTdGVwSW5zdGFuY2U7XG4gIEBJbnB1dCgpIHVzZXJJZDogbnVtYmVyO1xuICBAT3V0cHV0KCkgc3RlcENvbXBsZXRlZExpc3RlbmVyID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgY2FuY2VsTGlzdGVuZXIgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIGh0bWw6IHN0cmluZztcbiAgYWdyZWVkOiBib29sZWFuO1xuICBhZ3JlZWRFcnJvcjogYm9vbGVhbiA9IGZhbHNlO1xuICBhZ3JlZVdvcmRpbmc6IHN0cmluZztcbiAgbG9hZGluZzogYm9vbGVhbiA9IGZhbHNlO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcHJvamVjdFNlcnZpY2U6IFByb2plY3RTZXJ2aWNlKSB7XG4gICAgdGhpcy5hZ3JlZVdvcmRpbmcgPSBcIkkgYWdyZWUgdG8gdGhlIHRlcm1zIGFuZCBjb25kaXRpb25zIGFzIHN0YXRlZCBhYm92ZS5cIjtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMuYWdyZWVkID0gZmFsc2U7XG4gICAgdGhpcy5hZ3JlZWRFcnJvciA9IGZhbHNlO1xuICAgIHRoaXMubG9hZGluZyA9IHRydWU7XG4gICAgdGhpcy5wcm9qZWN0U2VydmljZS5nZXRTdGVwUGFyYW1ldGVycyh0aGlzLnN0ZXBJbnN0YW5jZS5zdGVwLmlkKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgIHRoaXMuaHRtbCA9IHJlc3VsdFtcIjBcIl07Ly8gJiYgdGhpcy5zdGVwSW5zdGFuY2Uuc3RlcC5wYXJhbWV0ZXJzW1wiMFwiXS5wYXJhbWV0ZXJWYWx1ZTtcbiAgICB9KTtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCkge1xuICB9XG5cbiAgY29tcGxldGVTdGVwKCkge1xuICAgIHRoaXMuYWdyZWVkRXJyb3IgPSBmYWxzZTtcbiAgICBpZiAoIXRoaXMuYWdyZWVkKSB7XG4gICAgICB0aGlzLmFncmVlZEVycm9yID0gdHJ1ZTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB0aGlzLmxvYWRpbmcgPSB0cnVlO1xuICAgIHRoaXMucHJvamVjdFNlcnZpY2UuY29tcGxldGVTdGVwKHRoaXMuc3RlcEluc3RhbmNlLmlkLCB0aGlzLnVzZXJJZCkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgICB0aGlzLnN0ZXBDb21wbGV0ZWRMaXN0ZW5lci5lbWl0KHtcbiAgICAgICAgdmFsdWU6IHRoaXMuc3RlcEluc3RhbmNlLFxuICAgICAgICB1c2VCdXNpbmVzc1Byb2Nlc3NJZDogdHJ1ZVxuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICByYWRpb0J1dHRvbkNoYW5nZWQoJHNlbGVjdGVkKSB7XG4gICAgdGhpcy5hZ3JlZWQgPSAkc2VsZWN0ZWQudmFsdWU7XG4gIH1cblxuICBjYW5jZWwoKSB7XG4gICAgdGhpcy5jYW5jZWxMaXN0ZW5lci5lbWl0KHtcbiAgICB9KTtcbiAgfVxufVxuIl19