/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var TimeSlot = /** @class */ (function () {
    function TimeSlot() {
    }
    return TimeSlot;
}());
export { TimeSlot };
if (false) {
    /** @type {?} */
    TimeSlot.prototype.id;
    /** @type {?} */
    TimeSlot.prototype.timeSlotType;
    /** @type {?} */
    TimeSlot.prototype.description;
    /** @type {?} */
    TimeSlot.prototype.fromDate;
    /** @type {?} */
    TimeSlot.prototype.toDate;
    /** @type {?} */
    TimeSlot.prototype.actor;
    /** @type {?} */
    TimeSlot.prototype.participant;
    /** @type {?} */
    TimeSlot.prototype.date;
    /** @type {?} */
    TimeSlot.prototype.startTime;
    /** @type {?} */
    TimeSlot.prototype.endTime;
    /** @type {?} */
    TimeSlot.prototype.startIndex;
    /** @type {?} */
    TimeSlot.prototype.endIndex;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGltZS1zbG90LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWN1bWVuLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9kb21haW4vdGltZS1zbG90LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFFQTtJQUFBO0lBZUEsQ0FBQztJQUFELGVBQUM7QUFBRCxDQUFDLEFBZkQsSUFlQzs7OztJQWRHLHNCQUFXOztJQUNYLGdDQUFxQjs7SUFDckIsK0JBQW9COztJQUNwQiw0QkFBZTs7SUFDZiwwQkFBYTs7SUFDYix5QkFBYTs7SUFDYiwrQkFBbUI7O0lBRW5CLHdCQUFVOztJQUNWLDZCQUFrQjs7SUFDbEIsMkJBQWdCOztJQUVoQiw4QkFBbUI7O0lBQ25CLDRCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFjdG9yIH0gZnJvbSAnLi9hY3Rvcic7XG5cbmV4cG9ydCBjbGFzcyBUaW1lU2xvdCB7XG4gICAgaWQ6IG51bWJlcjtcbiAgICB0aW1lU2xvdFR5cGU6IHN0cmluZztcbiAgICBkZXNjcmlwdGlvbjogc3RyaW5nO1xuICAgIGZyb21EYXRlOiBEYXRlO1xuICAgIHRvRGF0ZTogRGF0ZTtcbiAgICBhY3RvcjogQWN0b3I7XG4gICAgcGFydGljaXBhbnQ6IEFjdG9yO1xuXG4gICAgZGF0ZTogYW55O1xuICAgIHN0YXJ0VGltZTogc3RyaW5nO1xuICAgIGVuZFRpbWU6IHN0cmluZztcblxuICAgIHN0YXJ0SW5kZXg6IG51bWJlcjtcbiAgICBlbmRJbmRleDogbnVtYmVyO1xufVxuIl19