/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var ActorFieldValue = /** @class */ (function () {
    function ActorFieldValue() {
    }
    return ActorFieldValue;
}());
export { ActorFieldValue };
if (false) {
    /** @type {?} */
    ActorFieldValue.prototype.actorFieldValueId;
    /** @type {?} */
    ActorFieldValue.prototype.actorFieldId;
    /** @type {?} */
    ActorFieldValue.prototype.fieldValue;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWN0b3ItZmllbGQtdmFsdWUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL2RvbWFpbi9hY3Rvci1maWVsZC12YWx1ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQ0E7SUFBQTtJQUlBLENBQUM7SUFBRCxzQkFBQztBQUFELENBQUMsQUFKRCxJQUlDOzs7O0lBSEMsNENBQTBCOztJQUMxQix1Q0FBcUI7O0lBQ3JCLHFDQUFtQiIsInNvdXJjZXNDb250ZW50IjpbIlxuZXhwb3J0IGNsYXNzIEFjdG9yRmllbGRWYWx1ZSB7XG4gIGFjdG9yRmllbGRWYWx1ZUlkOiBudW1iZXI7XG4gIGFjdG9yRmllbGRJZDogbnVtYmVyO1xuICBmaWVsZFZhbHVlOiBzdHJpbmc7XG59XG4iXX0=