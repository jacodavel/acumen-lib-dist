/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var ActorFieldOption = /** @class */ (function () {
    function ActorFieldOption() {
    }
    return ActorFieldOption;
}());
export { ActorFieldOption };
if (false) {
    /** @type {?} */
    ActorFieldOption.prototype.id;
    /** @type {?} */
    ActorFieldOption.prototype.fieldValue;
    /** @type {?} */
    ActorFieldOption.prototype.description;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWN0b3ItZmllbGQtb3B0aW9uLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWN1bWVuLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9kb21haW4vYWN0b3ItZmllbGQtb3B0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFDQTtJQUFBO0lBSUEsQ0FBQztJQUFELHVCQUFDO0FBQUQsQ0FBQyxBQUpELElBSUM7Ozs7SUFIQyw4QkFBVzs7SUFDWCxzQ0FBbUI7O0lBQ25CLHVDQUFvQiIsInNvdXJjZXNDb250ZW50IjpbIlxuZXhwb3J0IGNsYXNzIEFjdG9yRmllbGRPcHRpb24ge1xuICBpZDogbnVtYmVyO1xuICBmaWVsZFZhbHVlOiBzdHJpbmc7XG4gIGRlc2NyaXB0aW9uOiBzdHJpbmc7XG59XG4iXX0=