/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var Step = /** @class */ (function () {
    function Step() {
    }
    return Step;
}());
export { Step };
if (false) {
    /** @type {?} */
    Step.prototype.id;
    /** @type {?} */
    Step.prototype.stepType;
    /** @type {?} */
    Step.prototype.parameters;
    /** @type {?} */
    Step.prototype.parentStep;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RlcC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvZG9tYWluL3N0ZXAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBO0lBQUE7SUFLQSxDQUFDO0lBQUQsV0FBQztBQUFELENBQUMsQUFMRCxJQUtDOzs7O0lBSkcsa0JBQVc7O0lBQ1gsd0JBQWlCOztJQUNqQiwwQkFBZ0I7O0lBQ2hCLDBCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjbGFzcyBTdGVwIHtcbiAgICBpZDogbnVtYmVyO1xuICAgIHN0ZXBUeXBlOiBTdHJpbmc7XG4gICAgcGFyYW1ldGVyczogYW55O1xuICAgIHBhcmVudFN0ZXA6IFN0ZXA7XG59XG4iXX0=