/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var ProjectActor = /** @class */ (function () {
    function ProjectActor() {
    }
    return ProjectActor;
}());
export { ProjectActor };
if (false) {
    /** @type {?} */
    ProjectActor.prototype.id;
    /** @type {?} */
    ProjectActor.prototype.actor;
    /** @type {?} */
    ProjectActor.prototype.projectRole;
    /** @type {?} */
    ProjectActor.prototype.canChangeProcessVariables;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC1hY3Rvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvZG9tYWluL3Byb2plY3QtYWN0b3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUdBO0lBQUE7SUFLQSxDQUFDO0lBQUQsbUJBQUM7QUFBRCxDQUFDLEFBTEQsSUFLQzs7OztJQUpHLDBCQUFXOztJQUNYLDZCQUFhOztJQUNiLG1DQUF5Qjs7SUFDekIsaURBQW1DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQWN0b3IgfSBmcm9tICcuL2FjdG9yJztcbmltcG9ydCB7IFByb2plY3RSb2xlIH0gZnJvbSAnLi9wcm9qZWN0LXJvbGUnO1xuXG5leHBvcnQgY2xhc3MgUHJvamVjdEFjdG9yIHtcbiAgICBpZDogbnVtYmVyO1xuICAgIGFjdG9yOiBBY3RvcjtcbiAgICBwcm9qZWN0Um9sZTogUHJvamVjdFJvbGU7XG4gICAgY2FuQ2hhbmdlUHJvY2Vzc1ZhcmlhYmxlczogYm9vbGVhbjtcbn1cbiJdfQ==