/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var ActorSearchDto = /** @class */ (function () {
    function ActorSearchDto() {
    }
    return ActorSearchDto;
}());
export { ActorSearchDto };
if (false) {
    /** @type {?} */
    ActorSearchDto.prototype.id;
    /** @type {?} */
    ActorSearchDto.prototype.firstname;
    /** @type {?} */
    ActorSearchDto.prototype.surname;
    /** @type {?} */
    ActorSearchDto.prototype.idNr;
    /** @type {?} */
    ActorSearchDto.prototype.actorFieldValues;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWN0b3Itc2VhcmNoLWR0by5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvZG9tYWluL2FjdG9yLXNlYXJjaC1kdG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUVBO0lBQUE7SUFNQSxDQUFDO0lBQUQscUJBQUM7QUFBRCxDQUFDLEFBTkQsSUFNQzs7OztJQUxDLDRCQUFXOztJQUNYLG1DQUFrQjs7SUFDbEIsaUNBQWdCOztJQUNoQiw4QkFBYTs7SUFDYiwwQ0FBdUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBY3RvckZpZWxkRHRvIH0gZnJvbSAnLi9hY3Rvci1maWVsZC1kdG8nO1xuXG5leHBvcnQgY2xhc3MgQWN0b3JTZWFyY2hEdG8ge1xuICBpZDogbnVtYmVyO1xuICBmaXJzdG5hbWU6IHN0cmluZztcbiAgc3VybmFtZTogc3RyaW5nO1xuICBpZE5yOiBzdHJpbmc7XG4gIGFjdG9yRmllbGRWYWx1ZXM6IEFycmF5PEFjdG9yRmllbGREdG8+O1xufVxuIl19