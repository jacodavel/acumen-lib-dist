/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var TableDataPage = /** @class */ (function () {
    function TableDataPage() {
        this.list = [];
        this.recordCount = 0;
    }
    return TableDataPage;
}());
export { TableDataPage };
if (false) {
    /** @type {?} */
    TableDataPage.prototype.list;
    /** @type {?} */
    TableDataPage.prototype.recordCount;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUtZGF0YS1wYWdlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWN1bWVuLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9kb21haW4vdGFibGUtZGF0YS1wYWdlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFDQTtJQUlJO1FBQ0ksSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7UUFDZixJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztJQUN6QixDQUFDO0lBQ0wsb0JBQUM7QUFBRCxDQUFDLEFBUkQsSUFRQzs7OztJQVBHLDZCQUFZOztJQUNaLG9DQUFvQiIsInNvdXJjZXNDb250ZW50IjpbIlxuZXhwb3J0IGNsYXNzIFRhYmxlRGF0YVBhZ2Uge1xuICAgIGxpc3Q6IGFueVtdO1xuICAgIHJlY29yZENvdW50OiBudW1iZXI7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgdGhpcy5saXN0ID0gW107XG4gICAgICAgIHRoaXMucmVjb3JkQ291bnQgPSAwO1xuICAgIH1cbn1cbiJdfQ==