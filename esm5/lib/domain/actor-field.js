/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var ActorField = /** @class */ (function () {
    function ActorField() {
    }
    return ActorField;
}());
export { ActorField };
if (false) {
    /** @type {?} */
    ActorField.prototype.id;
    /** @type {?} */
    ActorField.prototype.description;
    /** @type {?} */
    ActorField.prototype.actorFieldType;
    /** @type {?} */
    ActorField.prototype.sequenceNr;
    /** @type {?} */
    ActorField.prototype.required;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWN0b3ItZmllbGQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL2RvbWFpbi9hY3Rvci1maWVsZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQ0E7SUFBQTtJQU1BLENBQUM7SUFBRCxpQkFBQztBQUFELENBQUMsQUFORCxJQU1DOzs7O0lBTEMsd0JBQVc7O0lBQ1gsaUNBQW9COztJQUNwQixvQ0FBdUI7O0lBQ3ZCLGdDQUFtQjs7SUFDbkIsOEJBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiXG5leHBvcnQgY2xhc3MgQWN0b3JGaWVsZCB7XG4gIGlkOiBudW1iZXI7XG4gIGRlc2NyaXB0aW9uOiBzdHJpbmc7XG4gIGFjdG9yRmllbGRUeXBlOiBzdHJpbmc7XG4gIHNlcXVlbmNlTnI6IG51bWJlcjtcbiAgcmVxdWlyZWQ6IGJvb2xlYW47XG59XG4iXX0=