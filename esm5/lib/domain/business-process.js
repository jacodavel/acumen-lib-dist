/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var BusinessProcess = /** @class */ (function () {
    function BusinessProcess() {
    }
    return BusinessProcess;
}());
export { BusinessProcess };
if (false) {
    /** @type {?} */
    BusinessProcess.prototype.id;
    /** @type {?} */
    BusinessProcess.prototype.name;
    /** @type {?} */
    BusinessProcess.prototype.description;
    /** @type {?} */
    BusinessProcess.prototype.project;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnVzaW5lc3MtcHJvY2Vzcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvZG9tYWluL2J1c2luZXNzLXByb2Nlc3MudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUVBO0lBQUE7SUFLQSxDQUFDO0lBQUQsc0JBQUM7QUFBRCxDQUFDLEFBTEQsSUFLQzs7OztJQUpHLDZCQUFXOztJQUNYLCtCQUFhOztJQUNiLHNDQUFvQjs7SUFDcEIsa0NBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUHJvamVjdCB9IGZyb20gJy4vcHJvamVjdCc7XG5cbmV4cG9ydCBjbGFzcyBCdXNpbmVzc1Byb2Nlc3Mge1xuICAgIGlkOiBudW1iZXI7XG4gICAgbmFtZTogc3RyaW5nO1xuICAgIGRlc2NyaXB0aW9uOiBzdHJpbmc7XG4gICAgcHJvamVjdDogUHJvamVjdDtcbn0iXX0=