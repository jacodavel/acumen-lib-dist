/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var SelectQuestionOption = /** @class */ (function () {
    function SelectQuestionOption() {
    }
    return SelectQuestionOption;
}());
export { SelectQuestionOption };
if (false) {
    /** @type {?} */
    SelectQuestionOption.prototype.index;
    /** @type {?} */
    SelectQuestionOption.prototype.description;
    /** @type {?} */
    SelectQuestionOption.prototype.weight;
    /** @type {?} */
    SelectQuestionOption.prototype.selected;
    /** @type {?} */
    SelectQuestionOption.prototype.response;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0LXF1ZXN0aW9uLW9wdGlvbi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvZG9tYWluL3NlbGVjdC1xdWVzdGlvbi1vcHRpb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUNBO0lBQUE7SUFNQSxDQUFDO0lBQUQsMkJBQUM7QUFBRCxDQUFDLEFBTkQsSUFNQzs7OztJQUxHLHFDQUFjOztJQUNkLDJDQUFvQjs7SUFDcEIsc0NBQWU7O0lBQ2Ysd0NBQWtCOztJQUNsQix3Q0FBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJcbmV4cG9ydCBjbGFzcyBTZWxlY3RRdWVzdGlvbk9wdGlvbiB7XG4gICAgaW5kZXg6IG51bWJlcjtcbiAgICBkZXNjcmlwdGlvbjogc3RyaW5nO1xuICAgIHdlaWdodDogbnVtYmVyO1xuICAgIHNlbGVjdGVkOiBib29sZWFuO1xuICAgIHJlc3BvbnNlOiBzdHJpbmc7XG59XG4iXX0=