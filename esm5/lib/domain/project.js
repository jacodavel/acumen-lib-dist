/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var Project = /** @class */ (function () {
    function Project() {
    }
    return Project;
}());
export { Project };
if (false) {
    /** @type {?} */
    Project.prototype.id;
    /** @type {?} */
    Project.prototype.projectName;
    /** @type {?} */
    Project.prototype.description;
    /** @type {?} */
    Project.prototype.guid;
    /** @type {?} */
    Project.prototype.wizardProject;
    /** @type {?} */
    Project.prototype.registrationProject;
    /** @type {?} */
    Project.prototype.actorProjectFunction;
    /** @type {?} */
    Project.prototype.iconUrl;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvZG9tYWluL3Byb2plY3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUNBO0lBQUE7SUFTQSxDQUFDO0lBQUQsY0FBQztBQUFELENBQUMsQUFURCxJQVNDOzs7O0lBUkcscUJBQVc7O0lBQ1gsOEJBQW9COztJQUNwQiw4QkFBb0I7O0lBQ3BCLHVCQUFhOztJQUNiLGdDQUF1Qjs7SUFDdkIsc0NBQTZCOztJQUM3Qix1Q0FBNkI7O0lBQzdCLDBCQUFnQiIsInNvdXJjZXNDb250ZW50IjpbIlxuZXhwb3J0IGNsYXNzIFByb2plY3Qge1xuICAgIGlkOiBudW1iZXI7XG4gICAgcHJvamVjdE5hbWU6IHN0cmluZztcbiAgICBkZXNjcmlwdGlvbjogc3RyaW5nO1xuICAgIGd1aWQ6IHN0cmluZztcbiAgICB3aXphcmRQcm9qZWN0OiBib29sZWFuO1xuICAgIHJlZ2lzdHJhdGlvblByb2plY3Q6IGJvb2xlYW47XG4gICAgYWN0b3JQcm9qZWN0RnVuY3Rpb246IHN0cmluZztcbiAgICBpY29uVXJsOiBzdHJpbmc7XG59XG4iXX0=