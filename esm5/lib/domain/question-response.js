/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var QuestionResponse = /** @class */ (function () {
    function QuestionResponse() {
    }
    return QuestionResponse;
}());
export { QuestionResponse };
if (false) {
    /** @type {?} */
    QuestionResponse.prototype.id;
    /** @type {?} */
    QuestionResponse.prototype.response;
    /** @type {?} */
    QuestionResponse.prototype.comments;
    /** @type {?} */
    QuestionResponse.prototype.weight;
    /** @type {?} */
    QuestionResponse.prototype.selected;
    /** @type {?} */
    QuestionResponse.prototype.questionLineId;
    /** @type {?} */
    QuestionResponse.prototype.supportingDescription;
    /** @type {?} */
    QuestionResponse.prototype.supportingImage;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicXVlc3Rpb24tcmVzcG9uc2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL2RvbWFpbi9xdWVzdGlvbi1yZXNwb25zZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQ0E7SUFBQTtJQVNBLENBQUM7SUFBRCx1QkFBQztBQUFELENBQUMsQUFURCxJQVNDOzs7O0lBUkMsOEJBQVc7O0lBQ1gsb0NBQWlCOztJQUNqQixvQ0FBaUI7O0lBQ2pCLGtDQUFlOztJQUNmLG9DQUFrQjs7SUFDbEIsMENBQXVCOztJQUN2QixpREFBOEI7O0lBQzlCLDJDQUF3QiIsInNvdXJjZXNDb250ZW50IjpbIlxuZXhwb3J0IGNsYXNzIFF1ZXN0aW9uUmVzcG9uc2Uge1xuICBpZDogbnVtYmVyO1xuICByZXNwb25zZTogc3RyaW5nO1xuICBjb21tZW50czogc3RyaW5nO1xuICB3ZWlnaHQ6IG51bWJlcjtcbiAgc2VsZWN0ZWQ6IGJvb2xlYW47XG4gIHF1ZXN0aW9uTGluZUlkOiBudW1iZXI7XG4gIHN1cHBvcnRpbmdEZXNjcmlwdGlvbjogc3RyaW5nO1xuICBzdXBwb3J0aW5nSW1hZ2U6IHN0cmluZztcbn1cbiJdfQ==