/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var Actor = /** @class */ (function () {
    function Actor() {
    }
    return Actor;
}());
export { Actor };
if (false) {
    /** @type {?} */
    Actor.prototype.id;
    /** @type {?} */
    Actor.prototype.firstname;
    /** @type {?} */
    Actor.prototype.surname;
    /** @type {?} */
    Actor.prototype.idNr;
    /** @type {?} */
    Actor.prototype.email;
    /** @type {?} */
    Actor.prototype.celNr;
    /** @type {?} */
    Actor.prototype.title;
    /** @type {?} */
    Actor.prototype.actorType;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWN0b3IuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL2RvbWFpbi9hY3Rvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBRUE7SUFBQTtJQVVBLENBQUM7SUFBRCxZQUFDO0FBQUQsQ0FBQyxBQVZELElBVUM7Ozs7SUFURyxtQkFBVzs7SUFDWCwwQkFBa0I7O0lBQ2xCLHdCQUFnQjs7SUFDaEIscUJBQWE7O0lBQ2Isc0JBQWM7O0lBQ2Qsc0JBQWM7O0lBQ2Qsc0JBQVc7O0lBRVgsMEJBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQWN0b3JUeXBlIH0gZnJvbSBcIi4vYWN0b3ItdHlwZVwiO1xuXG5leHBvcnQgY2xhc3MgQWN0b3Ige1xuICAgIGlkOiBudW1iZXI7XG4gICAgZmlyc3RuYW1lOiBzdHJpbmc7XG4gICAgc3VybmFtZTogc3RyaW5nO1xuICAgIGlkTnI6IHN0cmluZztcbiAgICBlbWFpbDogc3RyaW5nO1xuICAgIGNlbE5yOiBzdHJpbmc7XG4gICAgdGl0bGU6IGFueTtcblxuICAgIGFjdG9yVHlwZTogQWN0b3JUeXBlO1xufVxuIl19