/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var StepInstance = /** @class */ (function () {
    function StepInstance() {
    }
    return StepInstance;
}());
export { StepInstance };
if (false) {
    /** @type {?} */
    StepInstance.prototype.id;
    /** @type {?} */
    StepInstance.prototype.businessProcessInstance;
    /** @type {?} */
    StepInstance.prototype.actor;
    /** @type {?} */
    StepInstance.prototype.step;
    /** @type {?} */
    StepInstance.prototype.assignedProjectFunction;
    /** @type {?} */
    StepInstance.prototype.stepInstanceStatus;
    /** @type {?} */
    StepInstance.prototype.activationDate;
    /** @type {?} */
    StepInstance.prototype.systemUser;
    /** @type {?} */
    StepInstance.prototype.wakeUpDate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RlcC1pbnN0YW5jZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvZG9tYWluL3N0ZXAtaW5zdGFuY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUtBO0lBQUE7SUFVQSxDQUFDO0lBQUQsbUJBQUM7QUFBRCxDQUFDLEFBVkQsSUFVQzs7OztJQVRDLDBCQUFXOztJQUNYLCtDQUFpRDs7SUFDakQsNkJBQWE7O0lBQ2IsNEJBQVc7O0lBQ1gsK0NBQWdDOztJQUNoQywwQ0FBMkI7O0lBQzNCLHNDQUFxQjs7SUFDckIsa0NBQXVCOztJQUN2QixrQ0FBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBCdXNpbmVzc1Byb2Nlc3NJbnN0YW5jZSB9IGZyb20gJy4vYnVzaW5lc3MtcHJvY2Vzcy1pbnN0YW5jZSc7XG5pbXBvcnQgeyBBY3RvciB9IGZyb20gJy4vYWN0b3InO1xuaW1wb3J0IHsgU3RlcCB9IGZyb20gJy4vc3RlcCc7XG5pbXBvcnQgeyBTeXN0ZW1Vc2VyIH0gZnJvbSAnLi9zeXN0ZW0tdXNlcic7XG5cbmV4cG9ydCBjbGFzcyBTdGVwSW5zdGFuY2Uge1xuICBpZDogbnVtYmVyO1xuICBidXNpbmVzc1Byb2Nlc3NJbnN0YW5jZTogQnVzaW5lc3NQcm9jZXNzSW5zdGFuY2U7XG4gIGFjdG9yOiBBY3RvcjtcbiAgc3RlcDogU3RlcDtcbiAgYXNzaWduZWRQcm9qZWN0RnVuY3Rpb246IHN0cmluZztcbiAgc3RlcEluc3RhbmNlU3RhdHVzOiBzdHJpbmc7XG4gIGFjdGl2YXRpb25EYXRlOiBEYXRlO1xuICBzeXN0ZW1Vc2VyOiBTeXN0ZW1Vc2VyO1xuICB3YWtlVXBEYXRlOiBEYXRlO1xufVxuIl19