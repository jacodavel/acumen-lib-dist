/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var QuestionLine = /** @class */ (function () {
    function QuestionLine() {
    }
    return QuestionLine;
}());
export { QuestionLine };
if (false) {
    /** @type {?} */
    QuestionLine.prototype.id;
    /** @type {?} */
    QuestionLine.prototype.wording;
    /** @type {?} */
    QuestionLine.prototype.children;
    /** @type {?} */
    QuestionLine.prototype.parent;
    /** @type {?} */
    QuestionLine.prototype.lineType;
    /** @type {?} */
    QuestionLine.prototype.answerType;
    /** @type {?} */
    QuestionLine.prototype.parameters;
    /** @type {?} */
    QuestionLine.prototype.required;
    /** @type {?} */
    QuestionLine.prototype.questionResponse;
    /** @type {?} */
    QuestionLine.prototype.hasError;
    /** @type {?} */
    QuestionLine.prototype.errorAccepted;
    /** @type {?} */
    QuestionLine.prototype.errorMessage;
    /** @type {?} */
    QuestionLine.prototype.showErrorConfirmation;
    /** @type {?} */
    QuestionLine.prototype.weight;
    /** @type {?} */
    QuestionLine.prototype.integrationIndicator;
    /** @type {?} */
    QuestionLine.prototype.allowSupportingDescription;
    /** @type {?} */
    QuestionLine.prototype.allowSupportingImage;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicXVlc3Rpb24tbGluZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvZG9tYWluL3F1ZXN0aW9uLWxpbmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUVBO0lBQUE7SUFrQkEsQ0FBQztJQUFELG1CQUFDO0FBQUQsQ0FBQyxBQWxCRCxJQWtCQzs7OztJQWpCQywwQkFBVzs7SUFDWCwrQkFBZ0I7O0lBQ2hCLGdDQUF5Qjs7SUFDekIsOEJBQXFCOztJQUNyQixnQ0FBaUI7O0lBQ2pCLGtDQUFtQjs7SUFDbkIsa0NBQW1COztJQUNuQixnQ0FBa0I7O0lBQ2xCLHdDQUFtQzs7SUFDbkMsZ0NBQWtCOztJQUNsQixxQ0FBdUI7O0lBQ3ZCLG9DQUFxQjs7SUFDckIsNkNBQStCOztJQUMvQiw4QkFBZTs7SUFDZiw0Q0FBNkI7O0lBQzdCLGtEQUFvQzs7SUFDcEMsNENBQThCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUXVlc3Rpb25SZXNwb25zZSB9IGZyb20gJy4vcXVlc3Rpb24tcmVzcG9uc2UnO1xuXG5leHBvcnQgY2xhc3MgUXVlc3Rpb25MaW5lIHtcbiAgaWQ6IG51bWJlcjtcbiAgd29yZGluZzogc3RyaW5nO1xuICBjaGlsZHJlbjogW1F1ZXN0aW9uTGluZV07XG4gIHBhcmVudDogUXVlc3Rpb25MaW5lO1xuICBsaW5lVHlwZTogc3RyaW5nO1xuICBhbnN3ZXJUeXBlOiBzdHJpbmc7XG4gIHBhcmFtZXRlcnM6IHN0cmluZztcbiAgcmVxdWlyZWQ6IGJvb2xlYW47XG4gIHF1ZXN0aW9uUmVzcG9uc2U6IFF1ZXN0aW9uUmVzcG9uc2U7XG4gIGhhc0Vycm9yOiBib29sZWFuO1xuICBlcnJvckFjY2VwdGVkOiBib29sZWFuO1xuICBlcnJvck1lc3NhZ2U6IHN0cmluZztcbiAgc2hvd0Vycm9yQ29uZmlybWF0aW9uOiBib29sZWFuO1xuICB3ZWlnaHQ6IG51bWJlcjtcbiAgaW50ZWdyYXRpb25JbmRpY2F0b3I6IHN0cmluZztcbiAgYWxsb3dTdXBwb3J0aW5nRGVzY3JpcHRpb246IGJvb2xlYW47XG4gIGFsbG93U3VwcG9ydGluZ0ltYWdlOiBib29sZWFuO1xufVxuIl19