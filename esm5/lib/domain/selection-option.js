/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var SelectionOption = /** @class */ (function () {
    function SelectionOption() {
    }
    return SelectionOption;
}());
export { SelectionOption };
if (false) {
    /** @type {?} */
    SelectionOption.prototype.id;
    /** @type {?} */
    SelectionOption.prototype.description;
    /** @type {?} */
    SelectionOption.prototype.defaultOption;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0aW9uLW9wdGlvbi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvZG9tYWluL3NlbGVjdGlvbi1vcHRpb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUNBO0lBQUE7SUFJQSxDQUFDO0lBQUQsc0JBQUM7QUFBRCxDQUFDLEFBSkQsSUFJQzs7OztJQUhHLDZCQUFXOztJQUNYLHNDQUFvQjs7SUFDcEIsd0NBQXVCIiwic291cmNlc0NvbnRlbnQiOlsiXG5leHBvcnQgY2xhc3MgU2VsZWN0aW9uT3B0aW9uIHtcbiAgICBpZDogbnVtYmVyO1xuICAgIGRlc2NyaXB0aW9uOiBzdHJpbmc7XG4gICAgZGVmYXVsdE9wdGlvbjogYm9vbGVhbjtcbn1cbiJdfQ==