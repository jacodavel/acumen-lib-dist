/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var SystemUser = /** @class */ (function () {
    function SystemUser() {
    }
    return SystemUser;
}());
export { SystemUser };
if (false) {
    /** @type {?} */
    SystemUser.prototype.id;
    /** @type {?} */
    SystemUser.prototype.username;
    /** @type {?} */
    SystemUser.prototype.systemRole;
    /** @type {?} */
    SystemUser.prototype.guid;
    /** @type {?} */
    SystemUser.prototype.actor;
    /** @type {?} */
    SystemUser.prototype.evaluatorProjects;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3lzdGVtLXVzZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL2RvbWFpbi9zeXN0ZW0tdXNlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBRUE7SUFBQTtJQVFBLENBQUM7SUFBRCxpQkFBQztBQUFELENBQUMsQUFSRCxJQVFDOzs7O0lBUEcsd0JBQVc7O0lBQ1gsOEJBQWlCOztJQUNqQixnQ0FBbUI7O0lBQ25CLDBCQUFhOztJQUNiLDJCQUFhOztJQUViLHVDQUEyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFjdG9yIH0gZnJvbSAnLi9hY3Rvcic7XG5cbmV4cG9ydCBjbGFzcyBTeXN0ZW1Vc2VyIHtcbiAgICBpZDogbnVtYmVyO1xuICAgIHVzZXJuYW1lOiBzdHJpbmc7XG4gICAgc3lzdGVtUm9sZTogc3RyaW5nO1xuICAgIGd1aWQ6IHN0cmluZztcbiAgICBhY3RvcjogQWN0b3I7XG5cbiAgICBldmFsdWF0b3JQcm9qZWN0czogbnVtYmVyW11cbn1cbiJdfQ==