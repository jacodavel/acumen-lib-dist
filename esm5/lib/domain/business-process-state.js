/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var BusinessProcessState = /** @class */ (function () {
    function BusinessProcessState() {
    }
    return BusinessProcessState;
}());
export { BusinessProcessState };
if (false) {
    /** @type {?} */
    BusinessProcessState.prototype.businessProcessInstance;
    /** @type {?} */
    BusinessProcessState.prototype.stepInstance;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnVzaW5lc3MtcHJvY2Vzcy1zdGF0ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvZG9tYWluL2J1c2luZXNzLXByb2Nlc3Mtc3RhdGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUdBO0lBQUE7SUFHQSxDQUFDO0lBQUQsMkJBQUM7QUFBRCxDQUFDLEFBSEQsSUFHQzs7OztJQUZHLHVEQUFpRDs7SUFDakQsNENBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQnVzaW5lc3NQcm9jZXNzSW5zdGFuY2UgfSBmcm9tICcuL2J1c2luZXNzLXByb2Nlc3MtaW5zdGFuY2UnO1xuaW1wb3J0IHsgU3RlcEluc3RhbmNlIH0gZnJvbSAnLi9zdGVwLWluc3RhbmNlJztcblxuZXhwb3J0IGNsYXNzIEJ1c2luZXNzUHJvY2Vzc1N0YXRlIHtcbiAgICBidXNpbmVzc1Byb2Nlc3NJbnN0YW5jZTogQnVzaW5lc3NQcm9jZXNzSW5zdGFuY2U7XG4gICAgc3RlcEluc3RhbmNlOiBTdGVwSW5zdGFuY2U7XG59Il19