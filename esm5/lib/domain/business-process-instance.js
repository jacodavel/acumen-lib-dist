/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var BusinessProcessInstance = /** @class */ (function () {
    function BusinessProcessInstance() {
    }
    return BusinessProcessInstance;
}());
export { BusinessProcessInstance };
if (false) {
    /** @type {?} */
    BusinessProcessInstance.prototype.id;
    /** @type {?} */
    BusinessProcessInstance.prototype.wording;
    /** @type {?} */
    BusinessProcessInstance.prototype.businessProcess;
    /** @type {?} */
    BusinessProcessInstance.prototype.actor;
    /** @type {?} */
    BusinessProcessInstance.prototype.processStatus;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnVzaW5lc3MtcHJvY2Vzcy1pbnN0YW5jZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvZG9tYWluL2J1c2luZXNzLXByb2Nlc3MtaW5zdGFuY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUdBO0lBQUE7SUFNQSxDQUFDO0lBQUQsOEJBQUM7QUFBRCxDQUFDLEFBTkQsSUFNQzs7OztJQUxHLHFDQUFXOztJQUNYLDBDQUFnQjs7SUFDaEIsa0RBQWlDOztJQUNqQyx3Q0FBYTs7SUFDYixnREFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBCdXNpbmVzc1Byb2Nlc3MgfSBmcm9tICcuL2J1c2luZXNzLXByb2Nlc3MnO1xuaW1wb3J0IHsgQWN0b3IgfSBmcm9tICcuL2FjdG9yJztcblxuZXhwb3J0IGNsYXNzIEJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlIHtcbiAgICBpZDogbnVtYmVyO1xuICAgIHdvcmRpbmc6IHN0cmluZztcbiAgICBidXNpbmVzc1Byb2Nlc3M6IEJ1c2luZXNzUHJvY2VzcztcbiAgICBhY3RvcjogQWN0b3I7XG4gICAgcHJvY2Vzc1N0YXR1czogc3RyaW5nO1xufSJdfQ==