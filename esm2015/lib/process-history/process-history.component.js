/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { ActorService } from '../services/actor.service';
import { ProjectService } from '../services/project.service';
import { AnalysisService } from '../services/analysis.service';
import { Project } from '../domain/project';
export class ProcessHistoryComponent {
    /**
     * @param {?} actorService
     * @param {?} projectService
     * @param {?} analysisService
     */
    constructor(actorService, projectService, analysisService) {
        this.actorService = actorService;
        this.projectService = projectService;
        this.analysisService = analysisService;
        this.stepInstances = [];
        this.loading = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        // this.loading = true;
        if (this.externalActorIdNr) {
            this.actorService.getActorFromExternalId(this.externalActorIdNr).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                this.actor = result;
                this.subscription = Observable.timer(1000, 5000).subscribe((/**
                 * @param {?} t
                 * @return {?}
                 */
                t => {
                    this.loadStepInstances();
                }));
            }));
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
    /**
     * @private
     * @return {?}
     */
    loadStepInstances() {
        /** @type {?} */
        let projectGuid;
        if (this.project) {
            projectGuid = this.project.guid;
        }
        else {
            projectGuid = this.projectGuid;
        }
        // this.loading = true;
        this.projectService.getProcessHistory(projectGuid, this.actor.id).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            if (result) {
                /** @type {?} */
                let stepInstances = [];
                for (let stepInstance of result) {
                    if (stepInstance.step.stepType === "PROGRESS_NOTES") {
                        stepInstances.push(stepInstance);
                    }
                }
                this.stepInstances = stepInstances;
            }
            // this.loading = false;
        }), (/**
         * @param {?} error
         * @return {?}
         */
        error => {
            // this.loading = false;
        }));
    }
}
ProcessHistoryComponent.decorators = [
    { type: Component, args: [{
                selector: 'acn-process-history',
                template: "<div class=\"process-history-component\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n\n  <div class=\"step-list\">\n    <div *ngFor=\"let stepInstance of stepInstances; odd as isOdd\" class=\"step-row\" [ngClass]=\"{ 'oddRow': isOdd }\">\n      <div class=\"description\">\n        <!-- {{ stepInstance.step.stepType }} -->\n        {{ stepInstance.step.parameters[\"0\"].parameterValue }}\n      </div>\n      <div class=\"step-date\">\n        Completed: {{ stepInstance.activationDate | date: 'yyyy-MM-dd HH:mm:ss' }}\n      </div>\n    </div>\n  </div>\n</div>\n",
                styles: [".process-history-component{width:100%;height:100%;position:relative}.process-history-component .step-list{width:100%;height:100%;overflow-y:scroll}.process-history-component .step-list .step-row{display:table;width:100%;padding:5px;font-size:14px}.process-history-component .step-list .step-row .description{display:table-cell;width:70%;padding-left:10px}.process-history-component .step-list .step-row .step-date{display:table-cell;width:30%;padding-left:10px}.process-history-component .step-list .oddRow{background-color:#f2f2f2}.process-history-component .step-list .selectedRow{background-color:#abb9d3}"]
            }] }
];
/** @nocollapse */
ProcessHistoryComponent.ctorParameters = () => [
    { type: ActorService },
    { type: ProjectService },
    { type: AnalysisService }
];
ProcessHistoryComponent.propDecorators = {
    project: [{ type: Input }],
    projectGuid: [{ type: Input }],
    externalActorIdNr: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    ProcessHistoryComponent.prototype.project;
    /** @type {?} */
    ProcessHistoryComponent.prototype.projectGuid;
    /** @type {?} */
    ProcessHistoryComponent.prototype.externalActorIdNr;
    /** @type {?} */
    ProcessHistoryComponent.prototype.actor;
    /** @type {?} */
    ProcessHistoryComponent.prototype.stepInstances;
    /**
     * @type {?}
     * @private
     */
    ProcessHistoryComponent.prototype.subscription;
    /** @type {?} */
    ProcessHistoryComponent.prototype.loading;
    /**
     * @type {?}
     * @private
     */
    ProcessHistoryComponent.prototype.actorService;
    /**
     * @type {?}
     * @private
     */
    ProcessHistoryComponent.prototype.projectService;
    /**
     * @type {?}
     * @private
     */
    ProcessHistoryComponent.prototype.analysisService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvY2Vzcy1oaXN0b3J5LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvcHJvY2Vzcy1oaXN0b3J5L3Byb2Nlc3MtaGlzdG9yeS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQXFCLEtBQUssRUFBd0IsTUFBTSxlQUFlLENBQUM7QUFDMUYsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUdyQyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDekQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQzdELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUUvRCxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFXNUMsTUFBTSxPQUFPLHVCQUF1Qjs7Ozs7O0lBU2xDLFlBQW9CLFlBQTBCLEVBQVUsY0FBOEIsRUFDMUUsZUFBZ0M7UUFEeEIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFBVSxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDMUUsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBTDVDLGtCQUFhLEdBQXdCLEVBQUUsQ0FBQztRQUV4QyxZQUFPLEdBQVksS0FBSyxDQUFDO0lBSXpCLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ04sdUJBQXVCO1FBQ3ZCLElBQUksSUFBSSxDQUFDLGlCQUFpQixFQUFFO1lBQzFCLElBQUksQ0FBQyxZQUFZLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSTs7OztZQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUM3RSxJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQztnQkFDcEIsSUFBSSxDQUFDLFlBQVksR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxTQUFTOzs7O2dCQUFDLENBQUMsQ0FBQyxFQUFFO29CQUM3RCxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztnQkFDM0IsQ0FBQyxFQUFDLENBQUM7WUFDTCxDQUFDLEVBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7OztJQUVELFdBQVc7UUFDVCxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDckIsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNqQztJQUNILENBQUM7Ozs7O0lBRU8saUJBQWlCOztZQUNuQixXQUFtQjtRQUN2QixJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDaEIsV0FBVyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1NBQ2pDO2FBQU07WUFDTCxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztTQUNoQztRQUVELHVCQUF1QjtRQUN2QixJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxNQUFNLENBQUMsRUFBRTtZQUM5RSxJQUFJLE1BQU0sRUFBRTs7b0JBQ04sYUFBYSxHQUF3QixFQUFFO2dCQUMzQyxLQUFLLElBQUksWUFBWSxJQUFJLE1BQU0sRUFBRTtvQkFDL0IsSUFBSSxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsS0FBSyxnQkFBZ0IsRUFBRTt3QkFDbkQsYUFBYSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztxQkFDbEM7aUJBQ0Y7Z0JBRUQsSUFBSSxDQUFDLGFBQWEsR0FBRyxhQUFhLENBQUM7YUFDcEM7WUFDRCx3QkFBd0I7UUFDMUIsQ0FBQzs7OztRQUFFLEtBQUssQ0FBQyxFQUFFO1lBQ1Qsd0JBQXdCO1FBQzFCLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7O1lBNURGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUscUJBQXFCO2dCQUMvQixzbEJBQStDOzthQUVoRDs7OztZQWRRLFlBQVk7WUFDWixjQUFjO1lBQ2QsZUFBZTs7O3NCQWNyQixLQUFLOzBCQUNMLEtBQUs7Z0NBQ0wsS0FBSzs7OztJQUZOLDBDQUEwQjs7SUFDMUIsOENBQTZCOztJQUM3QixvREFBbUM7O0lBQ25DLHdDQUFhOztJQUNiLGdEQUF3Qzs7Ozs7SUFDeEMsK0NBQW1DOztJQUNuQywwQ0FBeUI7Ozs7O0lBRWIsK0NBQWtDOzs7OztJQUFFLGlEQUFzQzs7Ozs7SUFDbEYsa0RBQXdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIE9uRGVzdHJveSwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcy9SeCc7XG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzL1J4JztcblxuaW1wb3J0IHsgQWN0b3JTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvYWN0b3Iuc2VydmljZSc7XG5pbXBvcnQgeyBQcm9qZWN0U2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL3Byb2plY3Quc2VydmljZSc7XG5pbXBvcnQgeyBBbmFseXNpc1NlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9hbmFseXNpcy5zZXJ2aWNlJztcbmltcG9ydCB7IFN0ZXBJbnN0YW5jZSB9IGZyb20gJy4uL2RvbWFpbi9zdGVwLWluc3RhbmNlJztcbmltcG9ydCB7IFByb2plY3QgfSBmcm9tICcuLi9kb21haW4vcHJvamVjdCc7XG5pbXBvcnQgeyBBY3RvciB9IGZyb20gJy4uL2RvbWFpbi9hY3Rvcic7XG5pbXBvcnQgeyBCdXNpbmVzc1Byb2Nlc3NJbnN0YW5jZSB9IGZyb20gJy4uL2RvbWFpbi9idXNpbmVzcy1wcm9jZXNzLWluc3RhbmNlJztcbmltcG9ydCB7IFF1ZXN0aW9uTGluZSB9IGZyb20gJy4uL2RvbWFpbi9xdWVzdGlvbi1saW5lJztcbmltcG9ydCB7IEltYWdlQ29udGFpbmVyIH0gZnJvbSAnLi4vZG9tYWluL2ltYWdlLWNvbnRhaW5lcic7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2Fjbi1wcm9jZXNzLWhpc3RvcnknLFxuICB0ZW1wbGF0ZVVybDogJy4vcHJvY2Vzcy1oaXN0b3J5LmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vcHJvY2Vzcy1oaXN0b3J5LmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgUHJvY2Vzc0hpc3RvcnlDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIHByb2plY3Q6IFByb2plY3Q7XG4gIEBJbnB1dCgpIHByb2plY3RHdWlkOiBzdHJpbmc7XG4gIEBJbnB1dCgpIGV4dGVybmFsQWN0b3JJZE5yOiBzdHJpbmc7XG4gIGFjdG9yOiBBY3RvcjtcbiAgc3RlcEluc3RhbmNlczogQXJyYXk8U3RlcEluc3RhbmNlPiA9IFtdO1xuICBwcml2YXRlIHN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xuICBsb2FkaW5nOiBib29sZWFuID0gZmFsc2U7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBhY3RvclNlcnZpY2U6IEFjdG9yU2VydmljZSwgcHJpdmF0ZSBwcm9qZWN0U2VydmljZTogUHJvamVjdFNlcnZpY2UsXG4gICAgICBwcml2YXRlIGFuYWx5c2lzU2VydmljZTogQW5hbHlzaXNTZXJ2aWNlKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICAvLyB0aGlzLmxvYWRpbmcgPSB0cnVlO1xuICAgIGlmICh0aGlzLmV4dGVybmFsQWN0b3JJZE5yKSB7XG4gICAgICB0aGlzLmFjdG9yU2VydmljZS5nZXRBY3RvckZyb21FeHRlcm5hbElkKHRoaXMuZXh0ZXJuYWxBY3RvcklkTnIpLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgICAgdGhpcy5hY3RvciA9IHJlc3VsdDtcbiAgICAgICAgdGhpcy5zdWJzY3JpcHRpb24gPSBPYnNlcnZhYmxlLnRpbWVyKDEwMDAsIDUwMDApLnN1YnNjcmliZSh0ID0+IHtcbiAgICAgICAgICB0aGlzLmxvYWRTdGVwSW5zdGFuY2VzKCk7XG4gICAgICAgIH0pO1xuICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgbmdPbkRlc3Ryb3koKSB7XG4gICAgaWYgKHRoaXMuc3Vic2NyaXB0aW9uKSB7XG4gICAgICB0aGlzLnN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgbG9hZFN0ZXBJbnN0YW5jZXMoKSB7XG4gICAgbGV0IHByb2plY3RHdWlkOiBzdHJpbmc7XG4gICAgaWYgKHRoaXMucHJvamVjdCkge1xuICAgICAgcHJvamVjdEd1aWQgPSB0aGlzLnByb2plY3QuZ3VpZDtcbiAgICB9IGVsc2Uge1xuICAgICAgcHJvamVjdEd1aWQgPSB0aGlzLnByb2plY3RHdWlkO1xuICAgIH1cblxuICAgIC8vIHRoaXMubG9hZGluZyA9IHRydWU7XG4gICAgdGhpcy5wcm9qZWN0U2VydmljZS5nZXRQcm9jZXNzSGlzdG9yeShwcm9qZWN0R3VpZCwgdGhpcy5hY3Rvci5pZCkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgaWYgKHJlc3VsdCkge1xuICAgICAgICBsZXQgc3RlcEluc3RhbmNlczogQXJyYXk8U3RlcEluc3RhbmNlPiA9IFtdO1xuICAgICAgICBmb3IgKGxldCBzdGVwSW5zdGFuY2Ugb2YgcmVzdWx0KSB7XG4gICAgICAgICAgaWYgKHN0ZXBJbnN0YW5jZS5zdGVwLnN0ZXBUeXBlID09PSBcIlBST0dSRVNTX05PVEVTXCIpIHtcbiAgICAgICAgICAgIHN0ZXBJbnN0YW5jZXMucHVzaChzdGVwSW5zdGFuY2UpO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuc3RlcEluc3RhbmNlcyA9IHN0ZXBJbnN0YW5jZXM7XG4gICAgICB9XG4gICAgICAvLyB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICB9LCBlcnJvciA9PiB7XG4gICAgICAvLyB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICB9KTtcbiAgfVxufVxuIl19