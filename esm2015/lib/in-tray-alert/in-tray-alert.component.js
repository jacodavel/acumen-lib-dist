/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { ProjectService } from '../services/project.service';
import { ActorService } from '../services/actor.service';
import { AcumenConfiguration } from '../services/acumen-configuration';
export class InTrayAlertComponent {
    /**
     * @param {?} projectService
     * @param {?} actorService
     * @param {?} acumenConfiguration
     */
    constructor(projectService, actorService, acumenConfiguration) {
        this.projectService = projectService;
        this.actorService = actorService;
        this.acumenConfiguration = acumenConfiguration;
        this.onSelected = new EventEmitter();
        this.blink = false;
        if (acumenConfiguration.backendServiceUrl) {
            this.serviceUrl = acumenConfiguration.backendServiceUrl;
        }
        else {
            this.serviceUrl = "http://www.healthacumen.co.za/insight/";
        }
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.username) {
            this.actorService.getSystemUser(this.username).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                if (result && result.actor) {
                    this.assigneeId = result.actor.id;
                    this.startPolling();
                }
            }));
        }
        else {
            this.startPolling();
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
    /**
     * @return {?}
     */
    startPolling() {
        /** @type {?} */
        let interval = this.checkInterval;
        if (!interval || interval < 5000) {
            interval = 5000;
        }
        /** @type {?} */
        let timer = Observable.timer(0, interval);
        this.subscription = timer.subscribe((/**
         * @param {?} t
         * @return {?}
         */
        t => {
            this.projectService.getLatestStepAssignmentDate(this.assigneeId).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                // console.log(result);
                if (result) {
                    /** @type {?} */
                    let t = new Date().getTime() - new Date(result).getTime();
                    this.blink = t < this.alertTime;
                }
                else {
                    this.blink = false;
                }
            }));
        }));
    }
    /**
     * @return {?}
     */
    selected() {
        this.onSelected.emit();
    }
}
InTrayAlertComponent.decorators = [
    { type: Component, args: [{
                selector: 'acn-in-tray-alert',
                template: "<div class=\"in-tray-alert-component\" (click)=\"selected()\">\n  <div class=\"in-tray-image\">\n    <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"8\" height=\"8\" viewBox=\"0 0 8 8\" fill=\"#fff\">\n      <path d=\"M.19 0c-.11 0-.19.08-.19.19v7.63c0 .11.08.19.19.19h7.63c.11 0 .19-.08.19-.19v-7.63c0-.11-.08-.19-.19-.19h-7.63zm.81 2h6v3h-1l-1 1h-2l-1-1h-1v-3z\"/>\n    </svg>\n  </div>\n  <div *ngIf=\"blink\" class=\"warning-image\">\n    <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"8\" height=\"8\" viewBox=\"0 0 8 8\" fill=\"red\">\n      <path d=\"M3.09 0c-.06 0-.1.04-.13.09l-2.94 6.81c-.02.05-.03.13-.03.19v.81c0 .05.04.09.09.09h6.81c.05 0 .09-.04.09-.09v-.81c0-.05-.01-.14-.03-.19l-2.94-6.81c-.02-.05-.07-.09-.13-.09h-.81zm-.09 3h1v2h-1v-2zm0 3h1v1h-1v-1z\" />\n    </svg>\n  </div>\n</div>\n",
                styles: [".in-tray-alert-component{width:100%;height:100%;position:relative;cursor:pointer}.in-tray-alert-component .in-tray-image,.in-tray-alert-component .in-tray-image svg{width:100%;height:100%}.in-tray-alert-component .warning-image{position:absolute;width:60%;height:60%;left:30%;top:0;animation:1s steps(5,start) infinite blink-animation;-webkit-animation:1s steps(5,start) infinite blink-animation}.in-tray-alert-component .warning-image svg{width:100%;height:100%}@keyframes blink-animation{to{opacity:0}}@-webkit-keyframes blink-animation{to{opacity:0}}"]
            }] }
];
/** @nocollapse */
InTrayAlertComponent.ctorParameters = () => [
    { type: ProjectService },
    { type: ActorService },
    { type: AcumenConfiguration }
];
InTrayAlertComponent.propDecorators = {
    assigneeId: [{ type: Input }],
    username: [{ type: Input }],
    checkInterval: [{ type: Input }],
    alertTime: [{ type: Input }],
    onSelected: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    InTrayAlertComponent.prototype.assigneeId;
    /** @type {?} */
    InTrayAlertComponent.prototype.username;
    /** @type {?} */
    InTrayAlertComponent.prototype.checkInterval;
    /** @type {?} */
    InTrayAlertComponent.prototype.alertTime;
    /** @type {?} */
    InTrayAlertComponent.prototype.onSelected;
    /**
     * @type {?}
     * @private
     */
    InTrayAlertComponent.prototype.subscription;
    /**
     * @type {?}
     * @private
     */
    InTrayAlertComponent.prototype.serviceUrl;
    /** @type {?} */
    InTrayAlertComponent.prototype.blink;
    /**
     * @type {?}
     * @private
     */
    InTrayAlertComponent.prototype.projectService;
    /**
     * @type {?}
     * @private
     */
    InTrayAlertComponent.prototype.actorService;
    /**
     * @type {?}
     * @private
     */
    InTrayAlertComponent.prototype.acumenConfiguration;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW4tdHJheS1hbGVydC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL2luLXRyYXktYWxlcnQvaW4tdHJheS1hbGVydC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQWEsTUFBTSxlQUFlLENBQUM7QUFFMUYsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUVyQyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDN0QsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBR3pELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBT3ZFLE1BQU0sT0FBTyxvQkFBb0I7Ozs7OztJQVcvQixZQUFvQixjQUE4QixFQUFVLFlBQTBCLEVBQzFFLG1CQUF3QztRQURoQyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFBVSxpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxRSx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBUDFDLGVBQVUsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBSTFDLFVBQUssR0FBWSxLQUFLLENBQUM7UUFJckIsSUFBSSxtQkFBbUIsQ0FBQyxpQkFBaUIsRUFBRTtZQUN6QyxJQUFJLENBQUMsVUFBVSxHQUFHLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDO1NBQ3pEO2FBQU07WUFDTCxJQUFJLENBQUMsVUFBVSxHQUFHLHdDQUF3QyxDQUFDO1NBQzVEO0lBQ0gsQ0FBQzs7OztJQUVELFFBQVE7UUFDTixJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakIsSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUk7Ozs7WUFBQyxNQUFNLENBQUMsRUFBRTtnQkFDM0QsSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLEtBQUssRUFBRTtvQkFDMUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQztvQkFDbEMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO2lCQUNyQjtZQUNILENBQUMsRUFBQyxDQUFDO1NBQ0o7YUFBTTtZQUNMLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztTQUNyQjtJQUNILENBQUM7Ozs7SUFFTSxXQUFXO1FBQ2hCLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNyQixJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ2pDO0lBQ0gsQ0FBQzs7OztJQUVELFlBQVk7O1lBQ04sUUFBUSxHQUFHLElBQUksQ0FBQyxhQUFhO1FBQ2pDLElBQUksQ0FBQyxRQUFRLElBQUksUUFBUSxHQUFHLElBQUksRUFBRTtZQUNoQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1NBQ2pCOztZQUVHLEtBQUssR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUM7UUFDekMsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUMsU0FBUzs7OztRQUFDLENBQUMsQ0FBQyxFQUFFO1lBQ3RDLElBQUksQ0FBQyxjQUFjLENBQUMsMkJBQTJCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUk7Ozs7WUFBQyxNQUFNLENBQUMsRUFBRTtnQkFDN0UsdUJBQXVCO2dCQUN2QixJQUFJLE1BQU0sRUFBRTs7d0JBQ04sQ0FBQyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLEdBQUcsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsT0FBTyxFQUFFO29CQUN6RCxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO2lCQUNqQztxQkFBTTtvQkFDTCxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztpQkFDcEI7WUFDSCxDQUFDLEVBQUMsQ0FBQztRQUNMLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3pCLENBQUM7OztZQWxFRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLG1CQUFtQjtnQkFDN0Isc3pCQUE2Qzs7YUFFOUM7Ozs7WUFWUSxjQUFjO1lBQ2QsWUFBWTtZQUdaLG1CQUFtQjs7O3lCQVF6QixLQUFLO3VCQUNMLEtBQUs7NEJBQ0wsS0FBSzt3QkFDTCxLQUFLO3lCQUNMLE1BQU07Ozs7SUFKUCwwQ0FBNEI7O0lBQzVCLHdDQUEwQjs7SUFDMUIsNkNBQStCOztJQUMvQix5Q0FBMkI7O0lBQzNCLDBDQUEwQzs7Ozs7SUFDMUMsNENBQW1DOzs7OztJQUVuQywwQ0FBbUI7O0lBQ25CLHFDQUF1Qjs7Ozs7SUFFWCw4Q0FBc0M7Ozs7O0lBQUUsNENBQWtDOzs7OztJQUNsRixtREFBZ0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIEV2ZW50RW1pdHRlciwgT3V0cHV0LCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMvUngnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMvUngnO1xuXG5pbXBvcnQgeyBQcm9qZWN0U2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL3Byb2plY3Quc2VydmljZSc7XG5pbXBvcnQgeyBBY3RvclNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9hY3Rvci5zZXJ2aWNlJztcbmltcG9ydCB7IFN0ZXBJbnN0YW5jZSB9IGZyb20gJy4uL2RvbWFpbi9zdGVwLWluc3RhbmNlJztcbmltcG9ydCB7IFN5c3RlbVVzZXIgfSBmcm9tICcuLi9kb21haW4vc3lzdGVtLXVzZXInO1xuaW1wb3J0IHsgQWN1bWVuQ29uZmlndXJhdGlvbiB9IGZyb20gJy4uL3NlcnZpY2VzL2FjdW1lbi1jb25maWd1cmF0aW9uJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYWNuLWluLXRyYXktYWxlcnQnLFxuICB0ZW1wbGF0ZVVybDogJy4vaW4tdHJheS1hbGVydC5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2luLXRyYXktYWxlcnQuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBJblRyYXlBbGVydENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgQElucHV0KCkgYXNzaWduZWVJZDogbnVtYmVyO1xuICBASW5wdXQoKSB1c2VybmFtZTogc3RyaW5nO1xuICBASW5wdXQoKSBjaGVja0ludGVydmFsOiBudW1iZXI7XG4gIEBJbnB1dCgpIGFsZXJ0VGltZTogbnVtYmVyO1xuICBAT3V0cHV0KCkgb25TZWxlY3RlZCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgcHJpdmF0ZSBzdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcbiAgLy8gc2VydmVyVXJsID0gJ2h0dHA6Ly93d3cuaGVhbHRoYWN1bWVuLmNvLnphL2luc2lnaHQvJztcbiAgcHJpdmF0ZSBzZXJ2aWNlVXJsO1xuICBibGluazogYm9vbGVhbiA9IGZhbHNlO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcHJvamVjdFNlcnZpY2U6IFByb2plY3RTZXJ2aWNlLCBwcml2YXRlIGFjdG9yU2VydmljZTogQWN0b3JTZXJ2aWNlLFxuICAgICAgcHJpdmF0ZSBhY3VtZW5Db25maWd1cmF0aW9uOiBBY3VtZW5Db25maWd1cmF0aW9uKSB7XG4gICAgaWYgKGFjdW1lbkNvbmZpZ3VyYXRpb24uYmFja2VuZFNlcnZpY2VVcmwpIHtcbiAgICAgIHRoaXMuc2VydmljZVVybCA9IGFjdW1lbkNvbmZpZ3VyYXRpb24uYmFja2VuZFNlcnZpY2VVcmw7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuc2VydmljZVVybCA9IFwiaHR0cDovL3d3dy5oZWFsdGhhY3VtZW4uY28uemEvaW5zaWdodC9cIjtcbiAgICB9XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICBpZiAodGhpcy51c2VybmFtZSkge1xuICAgICAgdGhpcy5hY3RvclNlcnZpY2UuZ2V0U3lzdGVtVXNlcih0aGlzLnVzZXJuYW1lKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICAgIGlmIChyZXN1bHQgJiYgcmVzdWx0LmFjdG9yKSB7XG4gICAgICAgICAgdGhpcy5hc3NpZ25lZUlkID0gcmVzdWx0LmFjdG9yLmlkO1xuICAgICAgICAgIHRoaXMuc3RhcnRQb2xsaW5nKCk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnN0YXJ0UG9sbGluZygpO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5zdWJzY3JpcHRpb24pIHtcbiAgICAgIHRoaXMuc3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XG4gICAgfVxuICB9XG5cbiAgc3RhcnRQb2xsaW5nKCkge1xuICAgIGxldCBpbnRlcnZhbCA9IHRoaXMuY2hlY2tJbnRlcnZhbDtcbiAgICBpZiAoIWludGVydmFsIHx8IGludGVydmFsIDwgNTAwMCkge1xuICAgICAgaW50ZXJ2YWwgPSA1MDAwO1xuICAgIH1cblxuICAgIGxldCB0aW1lciA9IE9ic2VydmFibGUudGltZXIoMCwgaW50ZXJ2YWwpO1xuICAgIHRoaXMuc3Vic2NyaXB0aW9uID0gdGltZXIuc3Vic2NyaWJlKHQgPT4ge1xuICAgICAgdGhpcy5wcm9qZWN0U2VydmljZS5nZXRMYXRlc3RTdGVwQXNzaWdubWVudERhdGUodGhpcy5hc3NpZ25lZUlkKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKHJlc3VsdCk7XG4gICAgICAgIGlmIChyZXN1bHQpIHtcbiAgICAgICAgICBsZXQgdCA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpIC0gbmV3IERhdGUocmVzdWx0KS5nZXRUaW1lKCk7XG4gICAgICAgICAgdGhpcy5ibGluayA9IHQgPCB0aGlzLmFsZXJ0VGltZTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aGlzLmJsaW5rID0gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgc2VsZWN0ZWQoKSB7XG4gICAgdGhpcy5vblNlbGVjdGVkLmVtaXQoKTtcbiAgfVxufVxuIl19