/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { ActorService } from '../services/actor.service';
import { ProjectService } from '../services/project.service';
import { AnalysisService } from '../services/analysis.service';
import { Project } from '../domain/project';
export class WizardComponent {
    /**
     * @param {?} actorService
     * @param {?} projectService
     * @param {?} analysisService
     */
    constructor(actorService, projectService, analysisService) {
        this.actorService = actorService;
        this.projectService = projectService;
        this.analysisService = analysisService;
        this.onWizardCancelled = new EventEmitter();
        this.onWizardCompleted = new EventEmitter();
        this.onOpenCameraListener = new EventEmitter();
        this.onQuestionAnswered = new EventEmitter();
        this.onReadCode = new EventEmitter();
        this.processError = false;
        this.processExecutionError = false;
        this.cancelled = false;
        this.showCompletedMessage = false;
        this.showParticipantSearch = false;
        this.loading = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.loading = true;
        this.showWaitIndicator = true;
        this.processError = false;
        this.processExecutionError = false;
        this.actor = null;
        this.businessProcessInstance = null;
        this.stepInstance = null;
        this.showParticipantSearch = false;
        if (this.project) {
            this.initialise();
        }
        else if (this.projectGuid) {
            this.projectService.getProject(this.projectGuid).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                this.project = result;
                this.initialise();
            }));
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @private
     * @return {?}
     */
    initialise() {
        this.actorService.getSystemUser(this.username).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            if (result) {
                this.userId = result.id;
                if (this.externalActorIdNr) {
                    this.actorService.getProjectActorFromExternalId(this.externalActorIdNr, this.project.id).then((/**
                     * @param {?} result2
                     * @return {?}
                     */
                    result2 => {
                        this.actor = result2;
                        if (this.project.wizardProject) {
                            this.startWizard(this.project.guid, null);
                        }
                        else {
                            // if (this.project.actorProjectFunction === "PARTICIPANT") {
                            if (this.actor) {
                                this.startWizard(this.project.guid, this.actor.id);
                            }
                            else {
                                this.showParticipantSearch = true;
                                this.loading = false;
                            }
                        }
                    }));
                }
                else {
                    if (this.project.wizardProject) {
                        this.startWizard(this.project.guid, null);
                    }
                    else {
                        if (this.project.actorProjectFunction === "PARTICIPANT" && result.actor) {
                            this.actor = result.actor;
                            this.startWizard(this.project.guid, this.actor.id);
                        }
                        else {
                            this.showParticipantSearch = true;
                            this.loading = false;
                        }
                    }
                }
            }
            else {
                // Ongeldige user, wys boodskap
            }
        }));
    }
    /**
     * @param {?} participant
     * @return {?}
     */
    onParticipantSelected(participant) {
        this.actor = participant.actor;
        this.showParticipantSearch = false;
        this.loading = true;
        this.startWizard(this.project.guid, this.actor.id);
    }
    /**
     * @private
     * @param {?} projectGuid
     * @param {?} actorId
     * @return {?}
     */
    startWizard(projectGuid, actorId) {
        this.processExecutionError = false;
        this.projectService.startWizardProject(projectGuid, actorId).then((/**
         * @param {?} businessProcessInstance
         * @return {?}
         */
        businessProcessInstance => {
            this.businessProcessInstance = businessProcessInstance;
            this.projectName = businessProcessInstance.businessProcess.project.projectName;
            this.getNextStepDelayed();
            this.loading = false;
        }), (/**
         * @param {?} error
         * @return {?}
         */
        error => {
            this.processError = true;
            this.loading = false;
        }));
    }
    /**
     * @param {?} event
     * @return {?}
     */
    stepCompletedListener(event) {
        if (this.stepInstance && this.latitude && this.longitude) {
            this.projectService.updateStepLocation(this.stepInstance.id, this.latitude, this.longitude).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                // hoef nie iets te doen nie
            }));
        }
        this.getNextStep();
    }
    /**
     * @private
     * @return {?}
     */
    getNextStepDelayed() {
        if (!this.cancelled) {
            /** @type {?} */
            let timer = Observable.timer(1000);
            timer.subscribe((/**
             * @param {?} t
             * @return {?}
             */
            t => {
                this.getNextStep();
            }));
        }
    }
    /**
     * @private
     * @return {?}
     */
    getNextStep() {
        console.log("------------------------------------------------");
        this.projectService.getBusinessProcessState(this.businessProcessInstance.id).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            this.businessProcessInstance = result.businessProcessInstance;
            if (result.stepInstance == null) {
                console.log("stepInstance == null ");
                this.stepInstance = result.stepInstance;
                if (result.businessProcessInstance.processStatus === 'COMPLETED') {
                    console.log("COMPLETED");
                    this.processCompleted();
                    // this.analysisService.getProjectCompletionCount(this.project.guid).then(count => {
                    //     this.completionCount = count;
                    // });
                }
                else if (result.businessProcessInstance.processStatus === 'FAILED') {
                    this.processExecutionError = true;
                }
                else {
                    console.log("getNextStepDelayed 1");
                    this.getNextStepDelayed();
                }
            }
            else if (this.stepInstance && this.stepInstance.id === result.stepInstance.id) {
                this.stepInstance = result.stepInstance;
                if (this.stepInstance.step.stepType === 'CANCEL_WIZARD' ||
                    (this.stepInstance.step.stepType === 'NONE_END_EVENT' && !this.stepInstance.step.parentStep)) {
                    console.log("this.stepInstance.id === result.stepInstance.id 1");
                    this.processCompleted();
                }
                else {
                    console.log("this.stepInstance.id === result.stepInstance.id 2");
                    // this.getNextStepDelayed();
                    this.showStepPanel(result.stepInstance);
                }
            }
            else {
                console.log("this.stepInstance.id === result.stepInstance.id 3");
                this.stepInstance = result.stepInstance;
                if (this.stepInstance.step.stepType === 'CANCEL_WIZARD' ||
                    (this.stepInstance.step.stepType === 'NONE_END_EVENT' && !this.stepInstance.step.parentStep)) {
                    this.processCompleted();
                }
                else {
                    if (this.stepInstance.actor) {
                        console.log("this.stepInstance.actor");
                        if (this.stepInstance.actor.id === this.businessProcessInstance.actor.id) { // || stepInstance.getActor().getId().equals(evaluatorId) {
                            console.log("showStepPanel: " + this.stepInstance.step.stepType);
                            this.showStepPanel(result.stepInstance);
                        }
                        else {
                            console.log("showInProgressPanel 1");
                            this.showInProgressPanel();
                        }
                    }
                    else {
                        console.log("showInProgressPanel 2");
                        this.showInProgressPanel();
                    }
                }
            }
        }));
    }
    /**
     * @private
     * @param {?} stepInstance
     * @return {?}
     */
    showStepPanel(stepInstance) {
        if (stepInstance.step.stepType === 'COMPLETE_QUESTIONNAIRE' || stepInstance.step.stepType === 'MESSAGE' ||
            stepInstance.step.stepType === 'ID_NR_LOOKUP' || stepInstance.step.stepType === 'UPDATE_PERSONAL_DETAILS' ||
            stepInstance.step.stepType === 'REGISTER' || stepInstance.step.stepType === 'CREATE_SYSTEM_USER' ||
            stepInstance.step.stepType === 'SELECT_PROVIDER' || stepInstance.step.stepType === 'SELECT_PROVIDER_2' ||
            stepInstance.step.stepType === 'FACIAL_AUTHENTICATION_REF' || stepInstance.step.stepType === 'FACIAL_AUTHENTICATION' ||
            stepInstance.step.stepType === 'WAIT' || stepInstance.step.stepType === 'REGISTER' ||
            stepInstance.step.stepType === 'CREATE_SYSTEM_USER') {
            this.showWaitIndicator = false;
        }
        else {
            this.getNextStepDelayed();
        }
    }
    /**
     * @private
     * @return {?}
     */
    processCompleted() {
        this.showCompletedMessage = true;
        this.stepInstance = null;
        this.onWizardCompleted.emit();
    }
    /**
     * @private
     * @return {?}
     */
    showInProgressPanel() {
        this.getNextStepDelayed();
    }
    /**
     * @param {?} event
     * @return {?}
     */
    cancelWizard(event) {
        this.stepInstance = null;
        this.cancelled = true;
        this.onWizardCancelled.emit();
    }
    /**
     * @param {?} imageContainer
     * @return {?}
     */
    openCamera(imageContainer) {
        this.onOpenCameraListener.emit(imageContainer);
    }
    /**
     * @param {?} questionLine
     * @return {?}
     */
    questionAnswered(questionLine) {
        this.onQuestionAnswered.emit(questionLine);
    }
    /**
     * @param {?} codeReadListener
     * @return {?}
     */
    readCode(codeReadListener) {
        this.onReadCode.emit(codeReadListener);
    }
}
WizardComponent.decorators = [
    { type: Component, args: [{
                selector: 'acn-wizard',
                template: "<div class=\"wizard-component\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <participant-search [show]=\"showParticipantSearch\" [project]=\"project\" (onParticipantSelected)=\"onParticipantSelected($event)\"></participant-search>\n\n  <!-- <div class=\"step-content\">\n    username: {{ username }}, externalActorIdNr: {{ externalActorIdNr }}\n  </div> -->\n\n  <div *ngIf=\"stepInstance && !showCompletedMessage && stepInstance && stepInstance.step.stepType === 'MESSAGE'\" class=\"step-content\">\n    <message [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelWizard($event)\"></message>\n  </div>\n\n  <div *ngIf=\"stepInstance && !showCompletedMessage && stepInstance && stepInstance.step.stepType === 'COMPLETE_QUESTIONNAIRE'\" class=\"step-content\">\n    <questionnaire [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelWizard($event)\" (onTakePicture)=\"openCamera($event)\" (onQuestionAnswered)=\"questionAnswered($event)\"></questionnaire>\n  </div>\n\n  <div *ngIf=\"stepInstance && !showCompletedMessage && stepInstance.step.stepType === 'WAIT'\" class=\"step-content\">\n    <wait [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelWizard($event)\"></wait>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType === 'FACIAL_AUTHENTICATION_REF'\" class=\"step-content\">\n    <facial-authentication-ref [stepInstance]=\"stepInstance\" [userId]=\"userId\" (onOpenCameraListener)=\"openCamera($event)\"\n        (stepCompletedListener)=\"stepCompletedListener($event)\" (cancelListener)=\"cancelWizard($event)\"></facial-authentication-ref>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType === 'FACIAL_AUTHENTICATION'\" class=\"step-content\">\n    <facial-authentication [stepInstance]=\"stepInstance\" [userId]=\"userId\" (onOpenCameraListener)=\"openCamera($event)\"\n        (stepCompletedListener)=\"stepCompletedListener($event)\" (cancelListener)=\"cancelWizard($event)\"></facial-authentication>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType === 'CODE_READER'\" class=\"step-content\">\n    <code-reader [stepInstance]=\"stepInstance\" [userId]=\"userId\" (onReadCode)=\"readCode($event)\"\n        (stepCompletedListener)=\"stepCompletedListener($event)\" (cancelListener)=\"cancelWizard($event)\"></code-reader>\n  </div>\n\n  <div *ngIf=\"stepInstance && !showCompletedMessage && stepInstance.step.stepType === 'ID_NR_LOOKUP'\" class=\"step-content\">\n      <id-nr-lookup [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n          (cancelListener)=\"cancelWizard($event)\"></id-nr-lookup>\n  </div>\n\n  <div *ngIf=\"stepInstance && !showCompletedMessage && stepInstance && stepInstance.step.stepType === 'UPDATE_PERSONAL_DETAILS'\" class=\"step-content\">\n    <update-details [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelWizard($event)\"></update-details>\n  </div>\n\n  <div *ngIf=\"stepInstance && !showCompletedMessage && stepInstance && stepInstance.step.stepType === 'REGISTER'\" class=\"step-content\">\n    <register [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelWizard($event)\"></register>\n  </div>\n\n  <div *ngIf=\"stepInstance && !showCompletedMessage && stepInstance && stepInstance.step.stepType === 'CREATE_SYSTEM_USER'\" class=\"step-content\">\n    <create-system-user [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelWizard($event)\"></create-system-user>\n  </div>\n\n  <div *ngIf=\"stepInstance && !showCompletedMessage && stepInstance && stepInstance.step.stepType === 'SELECT_PROVIDER_2'\" class=\"step-content\">\n    <select-role2 [makeBooking]=\"true\" [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelWizard($event)\"></select-role2>\n  </div>\n\n  <div *ngIf=\"processError\" class=\"step-info\">\n    <div class=\"info-message\">\n        Error starting process...\n    </div>\n    <div class=\"button-section\">\n      <div class=\"button\" (click)=\"cancelWizard(null)\">\n        <div class=\"button-text\">\n          Cancel\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!processError && !processExecutionError && !showParticipantSearch && !showCompletedMessage && !cancelled && (!stepInstance ||\n          (stepInstance.step.stepType !== 'COMPLETE_QUESTIONNAIRE' && stepInstance.step.stepType !== 'MESSAGE' &&\n           stepInstance.step.stepType !== 'ID_NR_LOOKUP' && stepInstance.step.stepType !== 'UPDATE_PERSONAL_DETAILS' &&\n           stepInstance.step.stepType !== 'SELECT_PROVIDER_2' && stepInstance.step.stepType !== 'WAIT' &&\n           stepInstance.step.stepType !== 'FACIAL_AUTHENTICATION_REF' && stepInstance.step.stepType !== 'FACIAL_AUTHENTICATION' &&\n           stepInstance.step.stepType !== 'CODE_READER' &&\n           stepInstance.step.stepType !== 'REGISTER' && stepInstance.step.stepType !== 'CREATE_SYSTEM_USER'))\" class=\"step-info\">\n    <div class=\"info-message\">\n        Processing, please wait...\n    </div>\n    <div class=\"button-section\">\n      <div class=\"button\" (click)=\"cancelWizard(null)\">\n        <div class=\"button-text\">\n          Cancel\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div *ngIf=\"processExecutionError\" class=\"step-info\">\n    <div class=\"info-message\">\n        Process execution error\n    </div>\n    <div class=\"button-section\">\n      <div class=\"button\" (click)=\"cancelWizard(null)\">\n        <div class=\"button-text\">\n          Cancel\n        </div>\n      </div>\n    </div>\n  </div>\n\n\n  <div *ngIf=\"showCompletedMessage\" class=\"step-info\">\n    <div class=\"info-message\">\n        Process completed\n    </div>\n    <div class=\"button-section\">\n      <div class=\"button\" (click)=\"cancelWizard(null)\">\n        <div class=\"button-text\">\n          Continue\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n",
                styles: [".wizard-component{width:100%;height:100%;max-height:100%;position:relative}.wizard-component .step-content{width:100%;height:100%;position:relative}.wizard-component .step-info{height:100%;font-size:14px;position:relative}.wizard-component .step-info .info-message{position:absolute;top:0;left:0;bottom:50px;box-sizing:border-box;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px}.wizard-component .step-info .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.wizard-component .step-info .button-section .button{width:120px;background-color:#2b4054;height:36px;border-radius:5px;float:right;margin-top:8px;margin-right:8px;display:table;cursor:pointer}.wizard-component .step-info .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px;color:#fff}"]
            }] }
];
/** @nocollapse */
WizardComponent.ctorParameters = () => [
    { type: ActorService },
    { type: ProjectService },
    { type: AnalysisService }
];
WizardComponent.propDecorators = {
    project: [{ type: Input }],
    projectGuid: [{ type: Input }],
    username: [{ type: Input }],
    externalActorIdNr: [{ type: Input }],
    latitude: [{ type: Input }],
    longitude: [{ type: Input }],
    onWizardCancelled: [{ type: Output }],
    onWizardCompleted: [{ type: Output }],
    onOpenCameraListener: [{ type: Output }],
    onQuestionAnswered: [{ type: Output }],
    onReadCode: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    WizardComponent.prototype.project;
    /** @type {?} */
    WizardComponent.prototype.projectGuid;
    /** @type {?} */
    WizardComponent.prototype.username;
    /** @type {?} */
    WizardComponent.prototype.externalActorIdNr;
    /** @type {?} */
    WizardComponent.prototype.latitude;
    /** @type {?} */
    WizardComponent.prototype.longitude;
    /** @type {?} */
    WizardComponent.prototype.onWizardCancelled;
    /** @type {?} */
    WizardComponent.prototype.onWizardCompleted;
    /** @type {?} */
    WizardComponent.prototype.onOpenCameraListener;
    /** @type {?} */
    WizardComponent.prototype.onQuestionAnswered;
    /** @type {?} */
    WizardComponent.prototype.onReadCode;
    /**
     * @type {?}
     * @private
     */
    WizardComponent.prototype.actor;
    /** @type {?} */
    WizardComponent.prototype.userId;
    /** @type {?} */
    WizardComponent.prototype.processError;
    /** @type {?} */
    WizardComponent.prototype.processExecutionError;
    /** @type {?} */
    WizardComponent.prototype.cancelled;
    /** @type {?} */
    WizardComponent.prototype.businessProcessInstance;
    /** @type {?} */
    WizardComponent.prototype.stepInstance;
    /** @type {?} */
    WizardComponent.prototype.projectName;
    /** @type {?} */
    WizardComponent.prototype.showWaitIndicator;
    /** @type {?} */
    WizardComponent.prototype.showCompletedMessage;
    /** @type {?} */
    WizardComponent.prototype.completionCount;
    /** @type {?} */
    WizardComponent.prototype.showParticipantSearch;
    /** @type {?} */
    WizardComponent.prototype.loading;
    /**
     * @type {?}
     * @private
     */
    WizardComponent.prototype.actorService;
    /**
     * @type {?}
     * @private
     */
    WizardComponent.prototype.projectService;
    /**
     * @type {?}
     * @private
     */
    WizardComponent.prototype.analysisService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2l6YXJkLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvd2l6YXJkL3dpemFyZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQXFCLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzFGLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFHckMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ3pELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFFL0QsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBYTVDLE1BQU0sT0FBTyxlQUFlOzs7Ozs7SUEyQjFCLFlBQW9CLFlBQTBCLEVBQVUsY0FBOEIsRUFDMUUsZUFBZ0M7UUFEeEIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFBVSxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDMUUsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBckJsQyxzQkFBaUIsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3ZDLHNCQUFpQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDdkMseUJBQW9CLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUMxQyx1QkFBa0IsR0FBRyxJQUFJLFlBQVksRUFBZ0IsQ0FBQztRQUN0RCxlQUFVLEdBQUcsSUFBSSxZQUFZLEVBQW9CLENBQUM7UUFJNUQsaUJBQVksR0FBWSxLQUFLLENBQUM7UUFDOUIsMEJBQXFCLEdBQVksS0FBSyxDQUFDO1FBQ3ZDLGNBQVMsR0FBWSxLQUFLLENBQUM7UUFLM0IseUJBQW9CLEdBQVksS0FBSyxDQUFDO1FBRXRDLDBCQUFxQixHQUFZLEtBQUssQ0FBQztRQUN2QyxZQUFPLEdBQVksS0FBSyxDQUFDO0lBSXpCLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztRQUM5QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMscUJBQXFCLEdBQUcsS0FBSyxDQUFDO1FBRW5DLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUM7UUFDcEMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7UUFDekIsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztRQUNuQyxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDaEIsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1NBQ25CO2FBQU0sSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQzNCLElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJOzs7O1lBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQzdELElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO2dCQUN0QixJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDcEIsQ0FBQyxFQUFDLENBQUM7U0FDSjtJQUNILENBQUM7Ozs7SUFFRCxXQUFXO0lBQ1gsQ0FBQzs7Ozs7SUFFTyxVQUFVO1FBQ2hCLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJOzs7O1FBQUMsTUFBTSxDQUFDLEVBQUU7WUFDM0QsSUFBSSxNQUFNLEVBQUU7Z0JBQ1YsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDO2dCQUN4QixJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtvQkFDMUIsSUFBSSxDQUFDLFlBQVksQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJOzs7O29CQUFDLE9BQU8sQ0FBQyxFQUFFO3dCQUN0RyxJQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQzt3QkFDckIsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRTs0QkFDOUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQzt5QkFDM0M7NkJBQU07NEJBQ0wsNkRBQTZEOzRCQUM3RCxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7Z0NBQ2QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDOzZCQUNwRDtpQ0FBTTtnQ0FDTCxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDO2dDQUNsQyxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQzs2QkFDdEI7eUJBQ0Y7b0JBQ0gsQ0FBQyxFQUFDLENBQUM7aUJBQ0o7cUJBQU07b0JBQ0wsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRTt3QkFDOUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztxQkFDM0M7eUJBQU07d0JBQ0wsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLG9CQUFvQixLQUFLLGFBQWEsSUFBSSxNQUFNLENBQUMsS0FBSyxFQUFFOzRCQUN2RSxJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUM7NEJBQzFCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQzt5QkFDcEQ7NkJBQU07NEJBQ0wsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQzs0QkFDbEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7eUJBQ3RCO3FCQUNGO2lCQUNGO2FBQ0Y7aUJBQU07Z0JBQ0wsK0JBQStCO2FBQ2hDO1FBQ0gsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUVELHFCQUFxQixDQUFDLFdBQXlCO1FBQzdDLElBQUksQ0FBQyxLQUFLLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQztRQUMvQixJQUFJLENBQUMscUJBQXFCLEdBQUcsS0FBSyxDQUFDO1FBQ25DLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUNyRCxDQUFDOzs7Ozs7O0lBRU8sV0FBVyxDQUFDLFdBQW1CLEVBQUUsT0FBZTtRQUN0RCxJQUFJLENBQUMscUJBQXFCLEdBQUcsS0FBSyxDQUFDO1FBQ25DLElBQUksQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsV0FBVyxFQUFFLE9BQU8sQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyx1QkFBdUIsQ0FBQyxFQUFFO1lBQzFGLElBQUksQ0FBQyx1QkFBdUIsR0FBRyx1QkFBdUIsQ0FBQztZQUN2RCxJQUFJLENBQUMsV0FBVyxHQUFHLHVCQUF1QixDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDO1lBQy9FLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1lBQzFCLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLENBQUM7Ozs7UUFBRSxLQUFLLENBQUMsRUFBRTtZQUNULElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxxQkFBcUIsQ0FBQyxLQUFLO1FBQ3pCLElBQUksSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDeEQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJOzs7O1lBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQ3hHLDRCQUE0QjtZQUM5QixDQUFDLEVBQUMsQ0FBQztTQUNKO1FBRUQsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ3JCLENBQUM7Ozs7O0lBRU8sa0JBQWtCO1FBQ3hCLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFOztnQkFDZixLQUFLLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7WUFDbEMsS0FBSyxDQUFDLFNBQVM7Ozs7WUFBQyxDQUFDLENBQUMsRUFBRTtnQkFDbEIsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3JCLENBQUMsRUFBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDOzs7OztJQUVPLFdBQVc7UUFDakIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrREFBa0QsQ0FBQyxDQUFDO1FBQ2hFLElBQUksQ0FBQyxjQUFjLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxNQUFNLENBQUMsRUFBRTtZQUN6RixJQUFJLENBQUMsdUJBQXVCLEdBQUcsTUFBTSxDQUFDLHVCQUF1QixDQUFDO1lBQzlELElBQUksTUFBTSxDQUFDLFlBQVksSUFBSSxJQUFJLEVBQUU7Z0JBQy9CLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsQ0FBQztnQkFDckMsSUFBSSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDO2dCQUN4QyxJQUFJLE1BQU0sQ0FBQyx1QkFBdUIsQ0FBQyxhQUFhLEtBQUssV0FBVyxFQUFFO29CQUNoRSxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUN6QixJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztvQkFDeEIsb0ZBQW9GO29CQUNwRixvQ0FBb0M7b0JBQ3BDLE1BQU07aUJBQ1A7cUJBQU0sSUFBSSxNQUFNLENBQUMsdUJBQXVCLENBQUMsYUFBYSxLQUFLLFFBQVEsRUFBRTtvQkFDcEUsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQztpQkFDbkM7cUJBQU07b0JBQ0wsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO29CQUNwQyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztpQkFDM0I7YUFDRjtpQkFBTSxJQUFJLElBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLEtBQUssTUFBTSxDQUFDLFlBQVksQ0FBQyxFQUFFLEVBQUU7Z0JBQy9FLElBQUksQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQztnQkFDeEMsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEtBQUssZUFBZTtvQkFDbkQsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEtBQUssZ0JBQWdCLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRTtvQkFDaEcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtREFBbUQsQ0FBQyxDQUFDO29CQUNqRSxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztpQkFDekI7cUJBQU07b0JBQ0wsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtREFBbUQsQ0FBQyxDQUFDO29CQUNqRSw2QkFBNkI7b0JBQzdCLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDO2lCQUN6QzthQUNGO2lCQUFNO2dCQUNMLE9BQU8sQ0FBQyxHQUFHLENBQUMsbURBQW1ELENBQUMsQ0FBQztnQkFDakUsSUFBSSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDO2dCQUN4QyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsS0FBSyxlQUFlO29CQUNuRCxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsS0FBSyxnQkFBZ0IsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFO29CQUNoRyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztpQkFDekI7cUJBQU07b0JBQ0wsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRTt3QkFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO3dCQUN2QyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEVBQUUsS0FBSyxJQUFJLENBQUMsdUJBQXVCLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxFQUFDLDJEQUEyRDs0QkFDcEksT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzs0QkFDakUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUM7eUJBQ3pDOzZCQUFNOzRCQUNMLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsQ0FBQzs0QkFDckMsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7eUJBQzVCO3FCQUNGO3lCQUFNO3dCQUNMLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsQ0FBQzt3QkFDckMsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7cUJBQzVCO2lCQUNGO2FBQ0Y7UUFDSCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7OztJQUVPLGFBQWEsQ0FBQyxZQUEwQjtRQUM5QyxJQUFJLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxLQUFLLHdCQUF3QixJQUFJLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxLQUFLLFNBQVM7WUFDbkcsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEtBQUssY0FBYyxJQUFJLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxLQUFLLHlCQUF5QjtZQUN6RyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsS0FBSyxVQUFVLElBQUksWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEtBQUssb0JBQW9CO1lBQ2hHLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxLQUFLLGlCQUFpQixJQUFJLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxLQUFLLG1CQUFtQjtZQUN0RyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsS0FBSywyQkFBMkIsSUFBSSxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsS0FBSyx1QkFBdUI7WUFDcEgsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEtBQUssTUFBTSxJQUFJLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxLQUFLLFVBQVU7WUFDbEYsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEtBQUssb0JBQW9CLEVBQUU7WUFDdkQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztTQUNoQzthQUFNO1lBQ0wsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7U0FDM0I7SUFDSCxDQUFDOzs7OztJQUVPLGdCQUFnQjtRQUN0QixJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDO1FBQ2pDLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNoQyxDQUFDOzs7OztJQUVPLG1CQUFtQjtRQUN6QixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztJQUM1QixDQUFDOzs7OztJQUVELFlBQVksQ0FBQyxLQUFVO1FBQ3JCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNoQyxDQUFDOzs7OztJQUVELFVBQVUsQ0FBQyxjQUE4QjtRQUN2QyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ2pELENBQUM7Ozs7O0lBRUQsZ0JBQWdCLENBQUMsWUFBMEI7UUFDekMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUM3QyxDQUFDOzs7OztJQUVELFFBQVEsQ0FBQyxnQkFBa0M7UUFDekMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztJQUN6QyxDQUFDOzs7WUF2T0YsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxZQUFZO2dCQUN0QixnNk1BQXNDOzthQUV2Qzs7OztZQWhCUSxZQUFZO1lBQ1osY0FBYztZQUNkLGVBQWU7OztzQkFnQnJCLEtBQUs7MEJBQ0wsS0FBSzt1QkFDTCxLQUFLO2dDQUNMLEtBQUs7dUJBQ0wsS0FBSzt3QkFDTCxLQUFLO2dDQUNMLE1BQU07Z0NBQ04sTUFBTTttQ0FDTixNQUFNO2lDQUNOLE1BQU07eUJBQ04sTUFBTTs7OztJQVZQLGtDQUEwQjs7SUFDMUIsc0NBQTZCOztJQUM3QixtQ0FBMEI7O0lBQzFCLDRDQUFtQzs7SUFDbkMsbUNBQTBCOztJQUMxQixvQ0FBMkI7O0lBQzNCLDRDQUFpRDs7SUFDakQsNENBQWlEOztJQUNqRCwrQ0FBb0Q7O0lBQ3BELDZDQUFnRTs7SUFDaEUscUNBQTREOzs7OztJQUU1RCxnQ0FBcUI7O0lBQ3JCLGlDQUFlOztJQUNmLHVDQUE4Qjs7SUFDOUIsZ0RBQXVDOztJQUN2QyxvQ0FBMkI7O0lBQzNCLGtEQUFpRDs7SUFDakQsdUNBQTJCOztJQUMzQixzQ0FBb0I7O0lBQ3BCLDRDQUEyQjs7SUFDM0IsK0NBQXNDOztJQUN0QywwQ0FBd0I7O0lBQ3hCLGdEQUF1Qzs7SUFDdkMsa0NBQXlCOzs7OztJQUViLHVDQUFrQzs7Ozs7SUFBRSx5Q0FBc0M7Ozs7O0lBQ2xGLDBDQUF3QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBPbkRlc3Ryb3ksIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMvUngnO1xuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcy9SeCc7XG5cbmltcG9ydCB7IEFjdG9yU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2FjdG9yLnNlcnZpY2UnO1xuaW1wb3J0IHsgUHJvamVjdFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9wcm9qZWN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgQW5hbHlzaXNTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvYW5hbHlzaXMuc2VydmljZSc7XG5pbXBvcnQgeyBTdGVwSW5zdGFuY2UgfSBmcm9tICcuLi9kb21haW4vc3RlcC1pbnN0YW5jZSc7XG5pbXBvcnQgeyBQcm9qZWN0IH0gZnJvbSAnLi4vZG9tYWluL3Byb2plY3QnO1xuaW1wb3J0IHsgQWN0b3IgfSBmcm9tICcuLi9kb21haW4vYWN0b3InO1xuaW1wb3J0IHsgQnVzaW5lc3NQcm9jZXNzSW5zdGFuY2UgfSBmcm9tICcuLi9kb21haW4vYnVzaW5lc3MtcHJvY2Vzcy1pbnN0YW5jZSc7XG5pbXBvcnQgeyBRdWVzdGlvbkxpbmUgfSBmcm9tICcuLi9kb21haW4vcXVlc3Rpb24tbGluZSc7XG5pbXBvcnQgeyBJbWFnZUNvbnRhaW5lciB9IGZyb20gJy4uL2RvbWFpbi9pbWFnZS1jb250YWluZXInO1xuaW1wb3J0IHsgUHJvamVjdEFjdG9yIH0gZnJvbSAnLi4vZG9tYWluL3Byb2plY3QtYWN0b3InO1xuaW1wb3J0IHsgQ29kZVJlYWRMaXN0ZW5lciB9IGZyb20gJy4uL3N0ZXBzL2NvZGUtcmVhZGVyL2NvZGUtcmVhZC1saXN0ZW5lcic7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2Fjbi13aXphcmQnLFxuICB0ZW1wbGF0ZVVybDogJy4vd2l6YXJkLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vd2l6YXJkLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgV2l6YXJkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuICBASW5wdXQoKSBwcm9qZWN0OiBQcm9qZWN0O1xuICBASW5wdXQoKSBwcm9qZWN0R3VpZDogc3RyaW5nO1xuICBASW5wdXQoKSB1c2VybmFtZTogc3RyaW5nO1xuICBASW5wdXQoKSBleHRlcm5hbEFjdG9ySWROcjogc3RyaW5nO1xuICBASW5wdXQoKSBsYXRpdHVkZTogbnVtYmVyO1xuICBASW5wdXQoKSBsb25naXR1ZGU6IG51bWJlcjtcbiAgQE91dHB1dCgpIG9uV2l6YXJkQ2FuY2VsbGVkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgb25XaXphcmRDb21wbGV0ZWQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBvbk9wZW5DYW1lcmFMaXN0ZW5lciA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIG9uUXVlc3Rpb25BbnN3ZXJlZCA9IG5ldyBFdmVudEVtaXR0ZXI8UXVlc3Rpb25MaW5lPigpO1xuICBAT3V0cHV0KCkgb25SZWFkQ29kZSA9IG5ldyBFdmVudEVtaXR0ZXI8Q29kZVJlYWRMaXN0ZW5lcj4oKTtcblxuICBwcml2YXRlIGFjdG9yOiBBY3RvcjtcbiAgdXNlcklkOiBudW1iZXI7XG4gIHByb2Nlc3NFcnJvcjogYm9vbGVhbiA9IGZhbHNlO1xuICBwcm9jZXNzRXhlY3V0aW9uRXJyb3I6IGJvb2xlYW4gPSBmYWxzZTtcbiAgY2FuY2VsbGVkOiBib29sZWFuID0gZmFsc2U7XG4gIGJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlOiBCdXNpbmVzc1Byb2Nlc3NJbnN0YW5jZTtcbiAgc3RlcEluc3RhbmNlOiBTdGVwSW5zdGFuY2U7XG4gIHByb2plY3ROYW1lOiBzdHJpbmc7XG4gIHNob3dXYWl0SW5kaWNhdG9yOiBib29sZWFuO1xuICBzaG93Q29tcGxldGVkTWVzc2FnZTogYm9vbGVhbiA9IGZhbHNlO1xuICBjb21wbGV0aW9uQ291bnQ6IHN0cmluZztcbiAgc2hvd1BhcnRpY2lwYW50U2VhcmNoOiBib29sZWFuID0gZmFsc2U7XG4gIGxvYWRpbmc6IGJvb2xlYW4gPSBmYWxzZTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGFjdG9yU2VydmljZTogQWN0b3JTZXJ2aWNlLCBwcml2YXRlIHByb2plY3RTZXJ2aWNlOiBQcm9qZWN0U2VydmljZSxcbiAgICAgIHByaXZhdGUgYW5hbHlzaXNTZXJ2aWNlOiBBbmFseXNpc1NlcnZpY2UpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMubG9hZGluZyA9IHRydWU7XG4gICAgdGhpcy5zaG93V2FpdEluZGljYXRvciA9IHRydWU7XG4gICAgdGhpcy5wcm9jZXNzRXJyb3IgPSBmYWxzZTtcbiAgICB0aGlzLnByb2Nlc3NFeGVjdXRpb25FcnJvciA9IGZhbHNlO1xuXG4gICAgdGhpcy5hY3RvciA9IG51bGw7XG4gICAgdGhpcy5idXNpbmVzc1Byb2Nlc3NJbnN0YW5jZSA9IG51bGw7XG4gICAgdGhpcy5zdGVwSW5zdGFuY2UgPSBudWxsO1xuICAgIHRoaXMuc2hvd1BhcnRpY2lwYW50U2VhcmNoID0gZmFsc2U7XG4gICAgaWYgKHRoaXMucHJvamVjdCkge1xuICAgICAgdGhpcy5pbml0aWFsaXNlKCk7XG4gICAgfSBlbHNlIGlmICh0aGlzLnByb2plY3RHdWlkKSB7XG4gICAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmdldFByb2plY3QodGhpcy5wcm9qZWN0R3VpZCkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICB0aGlzLnByb2plY3QgPSByZXN1bHQ7XG4gICAgICAgIHRoaXMuaW5pdGlhbGlzZSgpO1xuICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgbmdPbkRlc3Ryb3koKSB7XG4gIH1cblxuICBwcml2YXRlIGluaXRpYWxpc2UoKSB7XG4gICAgdGhpcy5hY3RvclNlcnZpY2UuZ2V0U3lzdGVtVXNlcih0aGlzLnVzZXJuYW1lKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICBpZiAocmVzdWx0KSB7XG4gICAgICAgIHRoaXMudXNlcklkID0gcmVzdWx0LmlkO1xuICAgICAgICBpZiAodGhpcy5leHRlcm5hbEFjdG9ySWROcikge1xuICAgICAgICAgIHRoaXMuYWN0b3JTZXJ2aWNlLmdldFByb2plY3RBY3RvckZyb21FeHRlcm5hbElkKHRoaXMuZXh0ZXJuYWxBY3RvcklkTnIsIHRoaXMucHJvamVjdC5pZCkudGhlbihyZXN1bHQyID0+IHtcbiAgICAgICAgICAgIHRoaXMuYWN0b3IgPSByZXN1bHQyO1xuICAgICAgICAgICAgaWYgKHRoaXMucHJvamVjdC53aXphcmRQcm9qZWN0KSB7XG4gICAgICAgICAgICAgIHRoaXMuc3RhcnRXaXphcmQodGhpcy5wcm9qZWN0Lmd1aWQsIG51bGwpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgLy8gaWYgKHRoaXMucHJvamVjdC5hY3RvclByb2plY3RGdW5jdGlvbiA9PT0gXCJQQVJUSUNJUEFOVFwiKSB7XG4gICAgICAgICAgICAgIGlmICh0aGlzLmFjdG9yKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zdGFydFdpemFyZCh0aGlzLnByb2plY3QuZ3VpZCwgdGhpcy5hY3Rvci5pZCk7XG4gICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zaG93UGFydGljaXBhbnRTZWFyY2ggPSB0cnVlO1xuICAgICAgICAgICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgaWYgKHRoaXMucHJvamVjdC53aXphcmRQcm9qZWN0KSB7XG4gICAgICAgICAgICB0aGlzLnN0YXJ0V2l6YXJkKHRoaXMucHJvamVjdC5ndWlkLCBudWxsKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaWYgKHRoaXMucHJvamVjdC5hY3RvclByb2plY3RGdW5jdGlvbiA9PT0gXCJQQVJUSUNJUEFOVFwiICYmIHJlc3VsdC5hY3Rvcikge1xuICAgICAgICAgICAgICB0aGlzLmFjdG9yID0gcmVzdWx0LmFjdG9yO1xuICAgICAgICAgICAgICB0aGlzLnN0YXJ0V2l6YXJkKHRoaXMucHJvamVjdC5ndWlkLCB0aGlzLmFjdG9yLmlkKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgIHRoaXMuc2hvd1BhcnRpY2lwYW50U2VhcmNoID0gdHJ1ZTtcbiAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9IGVsc2Uge1xuICAgICAgICAvLyBPbmdlbGRpZ2UgdXNlciwgd3lzIGJvb2Rza2FwXG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBvblBhcnRpY2lwYW50U2VsZWN0ZWQocGFydGljaXBhbnQ6IFByb2plY3RBY3Rvcikge1xuICAgIHRoaXMuYWN0b3IgPSBwYXJ0aWNpcGFudC5hY3RvcjtcbiAgICB0aGlzLnNob3dQYXJ0aWNpcGFudFNlYXJjaCA9IGZhbHNlO1xuICAgIHRoaXMubG9hZGluZyA9IHRydWU7XG4gICAgdGhpcy5zdGFydFdpemFyZCh0aGlzLnByb2plY3QuZ3VpZCwgdGhpcy5hY3Rvci5pZCk7XG4gIH1cblxuICBwcml2YXRlIHN0YXJ0V2l6YXJkKHByb2plY3RHdWlkOiBzdHJpbmcsIGFjdG9ySWQ6IG51bWJlcikge1xuICAgIHRoaXMucHJvY2Vzc0V4ZWN1dGlvbkVycm9yID0gZmFsc2U7XG4gICAgdGhpcy5wcm9qZWN0U2VydmljZS5zdGFydFdpemFyZFByb2plY3QocHJvamVjdEd1aWQsIGFjdG9ySWQpLnRoZW4oYnVzaW5lc3NQcm9jZXNzSW5zdGFuY2UgPT4ge1xuICAgICAgdGhpcy5idXNpbmVzc1Byb2Nlc3NJbnN0YW5jZSA9IGJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlO1xuICAgICAgdGhpcy5wcm9qZWN0TmFtZSA9IGJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlLmJ1c2luZXNzUHJvY2Vzcy5wcm9qZWN0LnByb2plY3ROYW1lO1xuICAgICAgdGhpcy5nZXROZXh0U3RlcERlbGF5ZWQoKTtcbiAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgIH0sIGVycm9yID0+IHtcbiAgICAgIHRoaXMucHJvY2Vzc0Vycm9yID0gdHJ1ZTtcbiAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgIH0pO1xuICB9XG5cbiAgc3RlcENvbXBsZXRlZExpc3RlbmVyKGV2ZW50KSB7XG4gICAgaWYgKHRoaXMuc3RlcEluc3RhbmNlICYmIHRoaXMubGF0aXR1ZGUgJiYgdGhpcy5sb25naXR1ZGUpIHtcbiAgICAgIHRoaXMucHJvamVjdFNlcnZpY2UudXBkYXRlU3RlcExvY2F0aW9uKHRoaXMuc3RlcEluc3RhbmNlLmlkLCB0aGlzLmxhdGl0dWRlLCB0aGlzLmxvbmdpdHVkZSkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICAvLyBob2VmIG5pZSBpZXRzIHRlIGRvZW4gbmllXG4gICAgICB9KTtcbiAgICB9XG5cbiAgICB0aGlzLmdldE5leHRTdGVwKCk7XG4gIH1cblxuICBwcml2YXRlIGdldE5leHRTdGVwRGVsYXllZCgpIHtcbiAgICBpZiAoIXRoaXMuY2FuY2VsbGVkKSB7XG4gICAgICBsZXQgdGltZXIgPSBPYnNlcnZhYmxlLnRpbWVyKDEwMDApO1xuICAgICAgdGltZXIuc3Vic2NyaWJlKHQgPT4ge1xuICAgICAgICB0aGlzLmdldE5leHRTdGVwKCk7XG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGdldE5leHRTdGVwKCkge1xuICAgIGNvbnNvbGUubG9nKFwiLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXCIpO1xuICAgIHRoaXMucHJvamVjdFNlcnZpY2UuZ2V0QnVzaW5lc3NQcm9jZXNzU3RhdGUodGhpcy5idXNpbmVzc1Byb2Nlc3NJbnN0YW5jZS5pZCkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgdGhpcy5idXNpbmVzc1Byb2Nlc3NJbnN0YW5jZSA9IHJlc3VsdC5idXNpbmVzc1Byb2Nlc3NJbnN0YW5jZTtcbiAgICAgIGlmIChyZXN1bHQuc3RlcEluc3RhbmNlID09IG51bGwpIHtcbiAgICAgICAgY29uc29sZS5sb2coXCJzdGVwSW5zdGFuY2UgPT0gbnVsbCBcIik7XG4gICAgICAgIHRoaXMuc3RlcEluc3RhbmNlID0gcmVzdWx0LnN0ZXBJbnN0YW5jZTtcbiAgICAgICAgaWYgKHJlc3VsdC5idXNpbmVzc1Byb2Nlc3NJbnN0YW5jZS5wcm9jZXNzU3RhdHVzID09PSAnQ09NUExFVEVEJykge1xuICAgICAgICAgIGNvbnNvbGUubG9nKFwiQ09NUExFVEVEXCIpO1xuICAgICAgICAgIHRoaXMucHJvY2Vzc0NvbXBsZXRlZCgpO1xuICAgICAgICAgIC8vIHRoaXMuYW5hbHlzaXNTZXJ2aWNlLmdldFByb2plY3RDb21wbGV0aW9uQ291bnQodGhpcy5wcm9qZWN0Lmd1aWQpLnRoZW4oY291bnQgPT4ge1xuICAgICAgICAgIC8vICAgICB0aGlzLmNvbXBsZXRpb25Db3VudCA9IGNvdW50O1xuICAgICAgICAgIC8vIH0pO1xuICAgICAgICB9IGVsc2UgaWYgKHJlc3VsdC5idXNpbmVzc1Byb2Nlc3NJbnN0YW5jZS5wcm9jZXNzU3RhdHVzID09PSAnRkFJTEVEJykge1xuICAgICAgICAgIHRoaXMucHJvY2Vzc0V4ZWN1dGlvbkVycm9yID0gdHJ1ZTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBjb25zb2xlLmxvZyhcImdldE5leHRTdGVwRGVsYXllZCAxXCIpO1xuICAgICAgICAgIHRoaXMuZ2V0TmV4dFN0ZXBEZWxheWVkKCk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGVwSW5zdGFuY2UgJiYgdGhpcy5zdGVwSW5zdGFuY2UuaWQgPT09IHJlc3VsdC5zdGVwSW5zdGFuY2UuaWQpIHtcbiAgICAgICAgdGhpcy5zdGVwSW5zdGFuY2UgPSByZXN1bHQuc3RlcEluc3RhbmNlO1xuICAgICAgICBpZiAodGhpcy5zdGVwSW5zdGFuY2Uuc3RlcC5zdGVwVHlwZSA9PT0gJ0NBTkNFTF9XSVpBUkQnIHx8XG4gICAgICAgICAgICAodGhpcy5zdGVwSW5zdGFuY2Uuc3RlcC5zdGVwVHlwZSA9PT0gJ05PTkVfRU5EX0VWRU5UJyAmJiAhdGhpcy5zdGVwSW5zdGFuY2Uuc3RlcC5wYXJlbnRTdGVwKSkge1xuICAgICAgICAgIGNvbnNvbGUubG9nKFwidGhpcy5zdGVwSW5zdGFuY2UuaWQgPT09IHJlc3VsdC5zdGVwSW5zdGFuY2UuaWQgMVwiKTtcbiAgICAgICAgICB0aGlzLnByb2Nlc3NDb21wbGV0ZWQoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBjb25zb2xlLmxvZyhcInRoaXMuc3RlcEluc3RhbmNlLmlkID09PSByZXN1bHQuc3RlcEluc3RhbmNlLmlkIDJcIik7XG4gICAgICAgICAgLy8gdGhpcy5nZXROZXh0U3RlcERlbGF5ZWQoKTtcbiAgICAgICAgICB0aGlzLnNob3dTdGVwUGFuZWwocmVzdWx0LnN0ZXBJbnN0YW5jZSk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwidGhpcy5zdGVwSW5zdGFuY2UuaWQgPT09IHJlc3VsdC5zdGVwSW5zdGFuY2UuaWQgM1wiKTtcbiAgICAgICAgdGhpcy5zdGVwSW5zdGFuY2UgPSByZXN1bHQuc3RlcEluc3RhbmNlO1xuICAgICAgICBpZiAodGhpcy5zdGVwSW5zdGFuY2Uuc3RlcC5zdGVwVHlwZSA9PT0gJ0NBTkNFTF9XSVpBUkQnIHx8XG4gICAgICAgICAgICAodGhpcy5zdGVwSW5zdGFuY2Uuc3RlcC5zdGVwVHlwZSA9PT0gJ05PTkVfRU5EX0VWRU5UJyAmJiAhdGhpcy5zdGVwSW5zdGFuY2Uuc3RlcC5wYXJlbnRTdGVwKSkge1xuICAgICAgICAgIHRoaXMucHJvY2Vzc0NvbXBsZXRlZCgpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGlmICh0aGlzLnN0ZXBJbnN0YW5jZS5hY3Rvcikge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJ0aGlzLnN0ZXBJbnN0YW5jZS5hY3RvclwiKTtcbiAgICAgICAgICAgIGlmICh0aGlzLnN0ZXBJbnN0YW5jZS5hY3Rvci5pZCA9PT0gdGhpcy5idXNpbmVzc1Byb2Nlc3NJbnN0YW5jZS5hY3Rvci5pZCkgey8vIHx8IHN0ZXBJbnN0YW5jZS5nZXRBY3RvcigpLmdldElkKCkuZXF1YWxzKGV2YWx1YXRvcklkKSB7XG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwic2hvd1N0ZXBQYW5lbDogXCIgKyB0aGlzLnN0ZXBJbnN0YW5jZS5zdGVwLnN0ZXBUeXBlKTtcbiAgICAgICAgICAgICAgdGhpcy5zaG93U3RlcFBhbmVsKHJlc3VsdC5zdGVwSW5zdGFuY2UpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJzaG93SW5Qcm9ncmVzc1BhbmVsIDFcIik7XG4gICAgICAgICAgICAgIHRoaXMuc2hvd0luUHJvZ3Jlc3NQYW5lbCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcInNob3dJblByb2dyZXNzUGFuZWwgMlwiKTtcbiAgICAgICAgICAgIHRoaXMuc2hvd0luUHJvZ3Jlc3NQYW5lbCgpO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBzaG93U3RlcFBhbmVsKHN0ZXBJbnN0YW5jZTogU3RlcEluc3RhbmNlKSB7XG4gICAgaWYgKHN0ZXBJbnN0YW5jZS5zdGVwLnN0ZXBUeXBlID09PSAnQ09NUExFVEVfUVVFU1RJT05OQUlSRScgfHwgc3RlcEluc3RhbmNlLnN0ZXAuc3RlcFR5cGUgPT09ICdNRVNTQUdFJyB8fFxuICAgICAgICBzdGVwSW5zdGFuY2Uuc3RlcC5zdGVwVHlwZSA9PT0gJ0lEX05SX0xPT0tVUCcgfHwgc3RlcEluc3RhbmNlLnN0ZXAuc3RlcFR5cGUgPT09ICdVUERBVEVfUEVSU09OQUxfREVUQUlMUycgfHxcbiAgICAgICAgc3RlcEluc3RhbmNlLnN0ZXAuc3RlcFR5cGUgPT09ICdSRUdJU1RFUicgfHwgc3RlcEluc3RhbmNlLnN0ZXAuc3RlcFR5cGUgPT09ICdDUkVBVEVfU1lTVEVNX1VTRVInIHx8XG4gICAgICAgIHN0ZXBJbnN0YW5jZS5zdGVwLnN0ZXBUeXBlID09PSAnU0VMRUNUX1BST1ZJREVSJyB8fCBzdGVwSW5zdGFuY2Uuc3RlcC5zdGVwVHlwZSA9PT0gJ1NFTEVDVF9QUk9WSURFUl8yJyB8fFxuICAgICAgICBzdGVwSW5zdGFuY2Uuc3RlcC5zdGVwVHlwZSA9PT0gJ0ZBQ0lBTF9BVVRIRU5USUNBVElPTl9SRUYnIHx8IHN0ZXBJbnN0YW5jZS5zdGVwLnN0ZXBUeXBlID09PSAnRkFDSUFMX0FVVEhFTlRJQ0FUSU9OJyB8fFxuICAgICAgICBzdGVwSW5zdGFuY2Uuc3RlcC5zdGVwVHlwZSA9PT0gJ1dBSVQnIHx8IHN0ZXBJbnN0YW5jZS5zdGVwLnN0ZXBUeXBlID09PSAnUkVHSVNURVInIHx8XG4gICAgICAgIHN0ZXBJbnN0YW5jZS5zdGVwLnN0ZXBUeXBlID09PSAnQ1JFQVRFX1NZU1RFTV9VU0VSJykge1xuICAgICAgdGhpcy5zaG93V2FpdEluZGljYXRvciA9IGZhbHNlO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmdldE5leHRTdGVwRGVsYXllZCgpO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgcHJvY2Vzc0NvbXBsZXRlZCgpIHtcbiAgICB0aGlzLnNob3dDb21wbGV0ZWRNZXNzYWdlID0gdHJ1ZTtcbiAgICB0aGlzLnN0ZXBJbnN0YW5jZSA9IG51bGw7XG4gICAgdGhpcy5vbldpemFyZENvbXBsZXRlZC5lbWl0KCk7XG4gIH1cblxuICBwcml2YXRlIHNob3dJblByb2dyZXNzUGFuZWwoKSB7XG4gICAgdGhpcy5nZXROZXh0U3RlcERlbGF5ZWQoKTtcbiAgfVxuXG4gIGNhbmNlbFdpemFyZChldmVudDogYW55KSB7XG4gICAgdGhpcy5zdGVwSW5zdGFuY2UgPSBudWxsO1xuICAgIHRoaXMuY2FuY2VsbGVkID0gdHJ1ZTtcbiAgICB0aGlzLm9uV2l6YXJkQ2FuY2VsbGVkLmVtaXQoKTtcbiAgfVxuXG4gIG9wZW5DYW1lcmEoaW1hZ2VDb250YWluZXI6IEltYWdlQ29udGFpbmVyKSB7XG4gICAgdGhpcy5vbk9wZW5DYW1lcmFMaXN0ZW5lci5lbWl0KGltYWdlQ29udGFpbmVyKTtcbiAgfVxuXG4gIHF1ZXN0aW9uQW5zd2VyZWQocXVlc3Rpb25MaW5lOiBRdWVzdGlvbkxpbmUpIHtcbiAgICB0aGlzLm9uUXVlc3Rpb25BbnN3ZXJlZC5lbWl0KHF1ZXN0aW9uTGluZSk7XG4gIH1cblxuICByZWFkQ29kZShjb2RlUmVhZExpc3RlbmVyOiBDb2RlUmVhZExpc3RlbmVyKSB7XG4gICAgdGhpcy5vblJlYWRDb2RlLmVtaXQoY29kZVJlYWRMaXN0ZW5lcik7XG4gIH1cbn1cbiJdfQ==