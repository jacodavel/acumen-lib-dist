/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, EventEmitter, Output } from '@angular/core';
import { StepInstance } from '../domain/step-instance';
import { ActorService } from '../services/actor.service';
import { ProjectService } from '../services/project.service';
export class StepInstanceComponent {
    /**
     * @param {?} actorService
     * @param {?} projectService
     */
    constructor(actorService, projectService) {
        this.actorService = actorService;
        this.projectService = projectService;
        this.onStepCompleted = new EventEmitter();
        this.onStepCancelled = new EventEmitter();
        // @Output() onTakePicture = new EventEmitter();
        this.onOpenCameraListener = new EventEmitter();
        this.onReadCode = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.username) {
            this.actorService.getSystemUser(this.username).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                if (result) {
                    this.userId = result.id;
                }
            }));
        }
        else if (!this.userId && this.stepInstance && this.stepInstance.systemUser) {
            this.userId = this.stepInstance.systemUser.id;
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    stepCompletedListener(event) {
        if (this.latitude && this.longitude) {
            this.projectService.updateStepLocation(this.stepInstance.id, this.latitude, this.longitude).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                // hoef nie iets te doen nie
            }));
        }
        this.onStepCompleted.emit();
        this.stepInstance = null;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    cancelListener(event) {
        this.onStepCancelled.emit();
        this.stepInstance = null;
    }
    /**
     * @param {?} imageContainer
     * @return {?}
     */
    openCamera(imageContainer) {
        this.onOpenCameraListener.emit(imageContainer);
    }
    /**
     * @param {?} codeReadListener
     * @return {?}
     */
    readCode(codeReadListener) {
        this.onReadCode.emit(codeReadListener);
    }
}
StepInstanceComponent.decorators = [
    { type: Component, args: [{
                selector: 'acn-step-instance',
                template: "<div class=\"step-instance-component\">\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType === 'MESSAGE'\" class=\"step-content\">\n    <message [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelListener($event)\"></message>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType === 'COMPLETE_QUESTIONNAIRE'\" class=\"step-content\">\n    <questionnaire [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelListener($event)\" (onTakePicture)=\"openCamera($event)\"></questionnaire>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType === 'WAIT'\" class=\"step-content\">\n    <wait [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelListener($event)\"></wait>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType === 'FACIAL_AUTHENTICATION_REF'\" class=\"step-content\">\n    <facial-authentication-ref [stepInstance]=\"stepInstance\" [userId]=\"userId\" (onOpenCameraListener)=\"openCamera($event)\"\n        (stepCompletedListener)=\"stepCompletedListener($event)\" (cancelListener)=\"cancelListener($event)\"></facial-authentication-ref>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType === 'FACIAL_AUTHENTICATION'\" class=\"step-content\">\n    <facial-authentication [stepInstance]=\"stepInstance\" [userId]=\"userId\" (onOpenCameraListener)=\"openCamera($event)\"\n        (stepCompletedListener)=\"stepCompletedListener($event)\" (cancelListener)=\"cancelListener($event)\"></facial-authentication>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType === 'CODE_READER'\" class=\"step-content\">\n    <code-reader [stepInstance]=\"stepInstance\" [userId]=\"userId\" (onReadCode)=\"readCode($event)\"\n        (stepCompletedListener)=\"stepCompletedListener($event)\" (cancelListener)=\"cancelListener($event)\"></code-reader>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType === 'ID_NR_LOOKUP'\" class=\"step-content\">\n      <id-nr-lookup [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n          (cancelListener)=\"cancelListener($event)\"></id-nr-lookup>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance && stepInstance.step.stepType === 'UPDATE_PERSONAL_DETAILS'\" class=\"step-content\">\n    <update-details [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelListener($event)\"></update-details>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance && stepInstance.step.stepType === 'REGISTER'\" class=\"step-content\">\n    <register [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelListener($event)\"></register>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance && stepInstance.step.stepType === 'CREATE_SYSTEM_USER'\" class=\"step-content\">\n    <create-system-user [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelListener($event)\"></create-system-user>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType !== 'MESSAGE' && stepInstance.step.stepType !== 'COMPLETE_QUESTIONNAIRE' &&\n      stepInstance.step.stepType !== 'WAIT' && stepInstance.step.stepType !== 'FACIAL_AUTHENTICATION_REF' &&\n      stepInstance.step.stepType !== 'FACIAL_AUTHENTICATION' && stepInstance.step.stepType !== 'UPDATE_PERSONAL_DETAILS' &&\n      stepInstance.step.stepType !== 'ID_NR_LOOKUP' && stepInstance.step.stepType !== 'UPDATE_PERSONAL_DETAILS' &&\n      stepInstance.step.stepType !== 'REGISTER' && stepInstance.step.stepType !== 'CREATE_SYSTEM_USER'\" class=\"step-info\">\n    <div class=\"info-message\">\n        No step implementation available for step type {{ stepInstance.step.stepType }}.\n    </div>\n\n    <div class=\"button-section\">\n      <div class=\"button\" (click)=\"cancelListener(null)\">\n        <div class=\"button-text\">\n          Cancel\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n",
                styles: [".step-instance-component,.step-instance-component .step-content{width:100%;height:100%;position:relative}.step-instance-component .step-info{height:100%;font-size:14px;position:relative}.step-instance-component .step-info .info-message{position:absolute;top:0;left:0;bottom:50px;box-sizing:border-box;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px}.step-instance-component .step-info .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.step-instance-component .step-info .button-section .button{width:120px;background-color:#2b4054;height:36px;border-radius:5px;float:right;margin-top:8px;margin-right:8px;display:table;cursor:pointer}.step-instance-component .step-info .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px;color:#fff}"]
            }] }
];
/** @nocollapse */
StepInstanceComponent.ctorParameters = () => [
    { type: ActorService },
    { type: ProjectService }
];
StepInstanceComponent.propDecorators = {
    stepInstance: [{ type: Input }],
    userId: [{ type: Input }],
    username: [{ type: Input }],
    latitude: [{ type: Input }],
    longitude: [{ type: Input }],
    onStepCompleted: [{ type: Output }],
    onStepCancelled: [{ type: Output }],
    onOpenCameraListener: [{ type: Output }],
    onReadCode: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    StepInstanceComponent.prototype.stepInstance;
    /** @type {?} */
    StepInstanceComponent.prototype.userId;
    /** @type {?} */
    StepInstanceComponent.prototype.username;
    /** @type {?} */
    StepInstanceComponent.prototype.latitude;
    /** @type {?} */
    StepInstanceComponent.prototype.longitude;
    /** @type {?} */
    StepInstanceComponent.prototype.onStepCompleted;
    /** @type {?} */
    StepInstanceComponent.prototype.onStepCancelled;
    /** @type {?} */
    StepInstanceComponent.prototype.onOpenCameraListener;
    /** @type {?} */
    StepInstanceComponent.prototype.onReadCode;
    /**
     * @type {?}
     * @private
     */
    StepInstanceComponent.prototype.actorService;
    /**
     * @type {?}
     * @private
     */
    StepInstanceComponent.prototype.projectService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RlcC1pbnN0YW5jZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL3N0ZXAtaW5zdGFuY2Uvc3RlcC1pbnN0YW5jZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFL0UsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBR3ZELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUN6RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFRN0QsTUFBTSxPQUFPLHFCQUFxQjs7Ozs7SUFZaEMsWUFBb0IsWUFBMEIsRUFBVSxjQUE4QjtRQUFsRSxpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUFVLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQU41RSxvQkFBZSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDckMsb0JBQWUsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDOztRQUVyQyx5QkFBb0IsR0FBRyxJQUFJLFlBQVksRUFBa0IsQ0FBQztRQUMxRCxlQUFVLEdBQUcsSUFBSSxZQUFZLEVBQW9CLENBQUM7SUFHNUQsQ0FBQzs7OztJQUVELFFBQVE7UUFDTixJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakIsSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUk7Ozs7WUFBQyxNQUFNLENBQUMsRUFBRTtnQkFDM0QsSUFBSSxNQUFNLEVBQUU7b0JBQ1YsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDO2lCQUN6QjtZQUNILENBQUMsRUFBQyxDQUFDO1NBQ0o7YUFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxFQUFFO1lBQzVFLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDO1NBQy9DO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxxQkFBcUIsQ0FBQyxLQUFVO1FBQzlCLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ25DLElBQUksQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSTs7OztZQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUN4Ryw0QkFBNEI7WUFDOUIsQ0FBQyxFQUFDLENBQUM7U0FDSjtRQUVELElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7SUFDM0IsQ0FBQzs7Ozs7SUFFRCxjQUFjLENBQUMsS0FBVTtRQUN2QixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzVCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO0lBQzNCLENBQUM7Ozs7O0lBRUQsVUFBVSxDQUFDLGNBQThCO1FBQ3ZDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDakQsQ0FBQzs7Ozs7SUFFRCxRQUFRLENBQUMsZ0JBQWtDO1FBQ3pDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDekMsQ0FBQzs7O1lBdERGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsbUJBQW1CO2dCQUM3QixnMUlBQTZDOzthQUU5Qzs7OztZQVJRLFlBQVk7WUFDWixjQUFjOzs7MkJBU3BCLEtBQUs7cUJBQ0wsS0FBSzt1QkFDTCxLQUFLO3VCQUNMLEtBQUs7d0JBQ0wsS0FBSzs4QkFDTCxNQUFNOzhCQUNOLE1BQU07bUNBRU4sTUFBTTt5QkFDTixNQUFNOzs7O0lBVFAsNkNBQW9DOztJQUNwQyx1Q0FBd0I7O0lBQ3hCLHlDQUEwQjs7SUFDMUIseUNBQTBCOztJQUMxQiwwQ0FBMkI7O0lBQzNCLGdEQUErQzs7SUFDL0MsZ0RBQStDOztJQUUvQyxxREFBb0U7O0lBQ3BFLDJDQUE0RDs7Ozs7SUFFaEQsNkNBQWtDOzs7OztJQUFFLCtDQUFzQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgRXZlbnRFbWl0dGVyLCBPdXRwdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgU3RlcEluc3RhbmNlIH0gZnJvbSAnLi4vZG9tYWluL3N0ZXAtaW5zdGFuY2UnO1xuaW1wb3J0IHsgUXVlc3Rpb25MaW5lIH0gZnJvbSAnLi4vZG9tYWluL3F1ZXN0aW9uLWxpbmUnO1xuaW1wb3J0IHsgSW1hZ2VDb250YWluZXIgfSBmcm9tICcuLi9kb21haW4vaW1hZ2UtY29udGFpbmVyJztcbmltcG9ydCB7IEFjdG9yU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2FjdG9yLnNlcnZpY2UnO1xuaW1wb3J0IHsgUHJvamVjdFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9wcm9qZWN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgQ29kZVJlYWRMaXN0ZW5lciB9IGZyb20gJy4uL3N0ZXBzL2NvZGUtcmVhZGVyL2NvZGUtcmVhZC1saXN0ZW5lcic7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2Fjbi1zdGVwLWluc3RhbmNlJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3N0ZXAtaW5zdGFuY2UuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9zdGVwLWluc3RhbmNlLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgU3RlcEluc3RhbmNlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgc3RlcEluc3RhbmNlOiBTdGVwSW5zdGFuY2U7XG4gIEBJbnB1dCgpIHVzZXJJZDogbnVtYmVyO1xuICBASW5wdXQoKSB1c2VybmFtZTogc3RyaW5nO1xuICBASW5wdXQoKSBsYXRpdHVkZTogbnVtYmVyO1xuICBASW5wdXQoKSBsb25naXR1ZGU6IG51bWJlcjtcbiAgQE91dHB1dCgpIG9uU3RlcENvbXBsZXRlZCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIG9uU3RlcENhbmNlbGxlZCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgLy8gQE91dHB1dCgpIG9uVGFrZVBpY3R1cmUgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBvbk9wZW5DYW1lcmFMaXN0ZW5lciA9IG5ldyBFdmVudEVtaXR0ZXI8SW1hZ2VDb250YWluZXI+KCk7XG4gIEBPdXRwdXQoKSBvblJlYWRDb2RlID0gbmV3IEV2ZW50RW1pdHRlcjxDb2RlUmVhZExpc3RlbmVyPigpO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgYWN0b3JTZXJ2aWNlOiBBY3RvclNlcnZpY2UsIHByaXZhdGUgcHJvamVjdFNlcnZpY2U6IFByb2plY3RTZXJ2aWNlKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICBpZiAodGhpcy51c2VybmFtZSkge1xuICAgICAgdGhpcy5hY3RvclNlcnZpY2UuZ2V0U3lzdGVtVXNlcih0aGlzLnVzZXJuYW1lKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICAgIGlmIChyZXN1bHQpIHtcbiAgICAgICAgICB0aGlzLnVzZXJJZCA9IHJlc3VsdC5pZDtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfSBlbHNlIGlmICghdGhpcy51c2VySWQgJiYgdGhpcy5zdGVwSW5zdGFuY2UgJiYgdGhpcy5zdGVwSW5zdGFuY2Uuc3lzdGVtVXNlcikge1xuICAgICAgdGhpcy51c2VySWQgPSB0aGlzLnN0ZXBJbnN0YW5jZS5zeXN0ZW1Vc2VyLmlkO1xuICAgIH1cbiAgfVxuXG4gIHN0ZXBDb21wbGV0ZWRMaXN0ZW5lcihldmVudDogYW55KSB7XG4gICAgaWYgKHRoaXMubGF0aXR1ZGUgJiYgdGhpcy5sb25naXR1ZGUpIHtcbiAgICAgIHRoaXMucHJvamVjdFNlcnZpY2UudXBkYXRlU3RlcExvY2F0aW9uKHRoaXMuc3RlcEluc3RhbmNlLmlkLCB0aGlzLmxhdGl0dWRlLCB0aGlzLmxvbmdpdHVkZSkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICAvLyBob2VmIG5pZSBpZXRzIHRlIGRvZW4gbmllXG4gICAgICB9KTtcbiAgICB9XG5cbiAgICB0aGlzLm9uU3RlcENvbXBsZXRlZC5lbWl0KCk7XG4gICAgdGhpcy5zdGVwSW5zdGFuY2UgPSBudWxsO1xuICB9XG5cbiAgY2FuY2VsTGlzdGVuZXIoZXZlbnQ6IGFueSkge1xuICAgIHRoaXMub25TdGVwQ2FuY2VsbGVkLmVtaXQoKTtcbiAgICB0aGlzLnN0ZXBJbnN0YW5jZSA9IG51bGw7XG4gIH1cblxuICBvcGVuQ2FtZXJhKGltYWdlQ29udGFpbmVyOiBJbWFnZUNvbnRhaW5lcikge1xuICAgIHRoaXMub25PcGVuQ2FtZXJhTGlzdGVuZXIuZW1pdChpbWFnZUNvbnRhaW5lcik7XG4gIH1cblxuICByZWFkQ29kZShjb2RlUmVhZExpc3RlbmVyOiBDb2RlUmVhZExpc3RlbmVyKSB7XG4gICAgdGhpcy5vblJlYWRDb2RlLmVtaXQoY29kZVJlYWRMaXN0ZW5lcik7XG4gIH1cbn1cbiJdfQ==