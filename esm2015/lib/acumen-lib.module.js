/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
// import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AcumenLibComponent } from './acumen-lib.component';
import { InTrayComponent } from './in-tray/in-tray.component';
import { InTrayAlertComponent } from './in-tray-alert/in-tray-alert.component';
import { WizardComponent } from './wizard/wizard.component';
import { UserProjectsComponent } from './user-projects/user-projects.component';
import { UserProjects2Component } from './user-projects2/user-projects2.component';
import { StepInstanceComponent } from './step-instance/step-instance.component';
import { StepInstanceListComponent } from './step-instance-list/step-instance-list.component';
import { StepInstanceActiveComponent } from './step-instance-active/step-instance-active.component';
import { ProcessHistoryComponent } from './process-history/process-history.component';
import { LoadingIndicator } from './components/loading-indicator/loading-indicator';
import { ParticipantSearch } from './components/participant-search/participant-search';
import { MessageComponent } from './steps/message/message.component';
import { CreateSystemUserComponent } from './steps/create-system-user/create-system-user.component';
import { IdNrLookupComponent } from './steps/id-nr-lookup/id-nr-lookup.component';
import { RegisterComponent } from './steps/register/register.component';
import { UpdateDetailsComponent } from './steps/update-details/update-details.component';
import { TextQuestionComponent } from './steps/questionnaire/text-question.component';
import { CheckboxComponent } from './steps/questionnaire/checkbox-component';
import { RadioButtonComponent } from './steps/questionnaire/radio-button-component';
import { QuestionLineComponent } from './steps/questionnaire/question-line.component';
import { QuestionnaireComponent } from './steps/questionnaire/questionnaire.component';
import { SelectRole2Component } from './steps/select-role2/select-role2.component';
import { WaitComponent } from './steps/wait/wait.component';
import { FacialAuthenticationRefComponent } from './steps/facial-authentication-ref/facial-authentication-ref.component';
import { FacialAuthenticationComponent } from './steps/facial-authentication/facial-authentication.component';
import { CodeReaderComponent } from './steps/code-reader/code-reader.component';
import { ProjectService } from './services/project.service';
import { MaintenanceService } from './services/maintenance.service';
import { ActorService } from './services/actor.service';
import { AnalysisService } from './services/analysis.service';
import { AcumenConfiguration } from './services/acumen-configuration';
export class AcumenLibModule {
    /**
     * @param {?} acumenConfiguration
     * @return {?}
     */
    static forRoot(acumenConfiguration) {
        return {
            ngModule: AcumenLibModule,
            providers: [
                {
                    provide: AcumenConfiguration,
                    useValue: acumenConfiguration,
                },
            ],
        };
    }
}
AcumenLibModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    AcumenLibComponent,
                    InTrayComponent,
                    InTrayAlertComponent,
                    WizardComponent,
                    UserProjectsComponent,
                    UserProjects2Component,
                    StepInstanceComponent,
                    StepInstanceListComponent,
                    StepInstanceActiveComponent,
                    ProcessHistoryComponent,
                    LoadingIndicator,
                    ParticipantSearch,
                    MessageComponent,
                    CreateSystemUserComponent,
                    IdNrLookupComponent,
                    RegisterComponent,
                    UpdateDetailsComponent,
                    TextQuestionComponent,
                    CheckboxComponent,
                    RadioButtonComponent,
                    QuestionLineComponent,
                    QuestionnaireComponent,
                    SelectRole2Component,
                    WaitComponent,
                    FacialAuthenticationRefComponent,
                    FacialAuthenticationComponent,
                    CodeReaderComponent
                ],
                imports: [
                    FormsModule,
                    // BrowserModule,
                    CommonModule,
                    HttpClientModule
                ],
                exports: [
                    AcumenLibComponent,
                    InTrayComponent,
                    InTrayAlertComponent,
                    WizardComponent,
                    UserProjectsComponent,
                    UserProjects2Component,
                    StepInstanceComponent,
                    StepInstanceListComponent,
                    StepInstanceActiveComponent,
                    ProcessHistoryComponent
                ],
                providers: [
                    ProjectService,
                    MaintenanceService,
                    ActorService,
                    AnalysisService
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWN1bWVuLWxpYi5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL2FjdW1lbi1saWIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUF1QixNQUFNLGVBQWUsQ0FBQztBQUM5RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7O0FBRS9DLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUU3QyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDOUQsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDL0UsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQzVELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBQ25GLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLG1EQUFtRCxDQUFDO0FBQzlGLE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLHVEQUF1RCxDQUFDO0FBQ3BHLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBRXRGLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGtEQUFrRCxDQUFDO0FBQ3BGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLG9EQUFvRCxDQUFDO0FBRXZGLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLHlEQUF5RCxDQUFDO0FBQ3BHLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQ2xGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLGlEQUFpRCxDQUFDO0FBQ3pGLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQ3RGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQ3BGLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQ3RGLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQ3ZGLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQ25GLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUM1RCxPQUFPLEVBQUUsZ0NBQWdDLEVBQUUsTUFBTSx1RUFBdUUsQ0FBQztBQUN6SCxPQUFPLEVBQUUsNkJBQTZCLEVBQUUsTUFBTSwrREFBK0QsQ0FBQztBQUM5RyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUVoRixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDNUQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDcEUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQXlEdEUsTUFBTSxPQUFPLGVBQWU7Ozs7O0lBQzFCLE1BQU0sQ0FBQyxPQUFPLENBQUMsbUJBQXdDO1FBQ3JELE9BQU87WUFDTCxRQUFRLEVBQUUsZUFBZTtZQUN6QixTQUFTLEVBQUU7Z0JBQ1Q7b0JBQ0UsT0FBTyxFQUFFLG1CQUFtQjtvQkFDNUIsUUFBUSxFQUFFLG1CQUFtQjtpQkFDOUI7YUFDRjtTQUNGLENBQUM7SUFDSixDQUFDOzs7WUFsRUYsUUFBUSxTQUFDO2dCQUNSLFlBQVksRUFBRTtvQkFDWixrQkFBa0I7b0JBQ2xCLGVBQWU7b0JBQ2Ysb0JBQW9CO29CQUNwQixlQUFlO29CQUNmLHFCQUFxQjtvQkFDckIsc0JBQXNCO29CQUN0QixxQkFBcUI7b0JBQ3JCLHlCQUF5QjtvQkFDekIsMkJBQTJCO29CQUMzQix1QkFBdUI7b0JBQ3ZCLGdCQUFnQjtvQkFDaEIsaUJBQWlCO29CQUNqQixnQkFBZ0I7b0JBQ2hCLHlCQUF5QjtvQkFDekIsbUJBQW1CO29CQUNuQixpQkFBaUI7b0JBQ2pCLHNCQUFzQjtvQkFDdEIscUJBQXFCO29CQUNyQixpQkFBaUI7b0JBQ2pCLG9CQUFvQjtvQkFDcEIscUJBQXFCO29CQUNyQixzQkFBc0I7b0JBQ3RCLG9CQUFvQjtvQkFDcEIsYUFBYTtvQkFDYixnQ0FBZ0M7b0JBQ2hDLDZCQUE2QjtvQkFDN0IsbUJBQW1CO2lCQUNwQjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsV0FBVztvQkFDWCxpQkFBaUI7b0JBQ2pCLFlBQVk7b0JBQ1osZ0JBQWdCO2lCQUNqQjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1Asa0JBQWtCO29CQUNsQixlQUFlO29CQUNmLG9CQUFvQjtvQkFDcEIsZUFBZTtvQkFDZixxQkFBcUI7b0JBQ3JCLHNCQUFzQjtvQkFDdEIscUJBQXFCO29CQUNyQix5QkFBeUI7b0JBQ3pCLDJCQUEyQjtvQkFDM0IsdUJBQXVCO2lCQUN4QjtnQkFDRCxTQUFTLEVBQUU7b0JBQ1QsY0FBYztvQkFDZCxrQkFBa0I7b0JBQ2xCLFlBQVk7b0JBQ1osZUFBZTtpQkFDaEI7YUFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBNb2R1bGVXaXRoUHJvdmlkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbi8vIGltcG9ydCB7IEJyb3dzZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyJztcbmltcG9ydCB7IEZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuXG5pbXBvcnQgeyBBY3VtZW5MaWJDb21wb25lbnQgfSBmcm9tICcuL2FjdW1lbi1saWIuY29tcG9uZW50JztcbmltcG9ydCB7IEluVHJheUNvbXBvbmVudCB9IGZyb20gJy4vaW4tdHJheS9pbi10cmF5LmNvbXBvbmVudCc7XG5pbXBvcnQgeyBJblRyYXlBbGVydENvbXBvbmVudCB9IGZyb20gJy4vaW4tdHJheS1hbGVydC9pbi10cmF5LWFsZXJ0LmNvbXBvbmVudCc7XG5pbXBvcnQgeyBXaXphcmRDb21wb25lbnQgfSBmcm9tICcuL3dpemFyZC93aXphcmQuY29tcG9uZW50JztcbmltcG9ydCB7IFVzZXJQcm9qZWN0c0NvbXBvbmVudCB9IGZyb20gJy4vdXNlci1wcm9qZWN0cy91c2VyLXByb2plY3RzLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBVc2VyUHJvamVjdHMyQ29tcG9uZW50IH0gZnJvbSAnLi91c2VyLXByb2plY3RzMi91c2VyLXByb2plY3RzMi5jb21wb25lbnQnO1xuaW1wb3J0IHsgU3RlcEluc3RhbmNlQ29tcG9uZW50IH0gZnJvbSAnLi9zdGVwLWluc3RhbmNlL3N0ZXAtaW5zdGFuY2UuY29tcG9uZW50JztcbmltcG9ydCB7IFN0ZXBJbnN0YW5jZUxpc3RDb21wb25lbnQgfSBmcm9tICcuL3N0ZXAtaW5zdGFuY2UtbGlzdC9zdGVwLWluc3RhbmNlLWxpc3QuY29tcG9uZW50JztcbmltcG9ydCB7IFN0ZXBJbnN0YW5jZUFjdGl2ZUNvbXBvbmVudCB9IGZyb20gJy4vc3RlcC1pbnN0YW5jZS1hY3RpdmUvc3RlcC1pbnN0YW5jZS1hY3RpdmUuY29tcG9uZW50JztcbmltcG9ydCB7IFByb2Nlc3NIaXN0b3J5Q29tcG9uZW50IH0gZnJvbSAnLi9wcm9jZXNzLWhpc3RvcnkvcHJvY2Vzcy1oaXN0b3J5LmNvbXBvbmVudCc7XG5cbmltcG9ydCB7IExvYWRpbmdJbmRpY2F0b3IgfSBmcm9tICcuL2NvbXBvbmVudHMvbG9hZGluZy1pbmRpY2F0b3IvbG9hZGluZy1pbmRpY2F0b3InO1xuaW1wb3J0IHsgUGFydGljaXBhbnRTZWFyY2ggfSBmcm9tICcuL2NvbXBvbmVudHMvcGFydGljaXBhbnQtc2VhcmNoL3BhcnRpY2lwYW50LXNlYXJjaCc7XG5cbmltcG9ydCB7IE1lc3NhZ2VDb21wb25lbnQgfSBmcm9tICcuL3N0ZXBzL21lc3NhZ2UvbWVzc2FnZS5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ3JlYXRlU3lzdGVtVXNlckNvbXBvbmVudCB9IGZyb20gJy4vc3RlcHMvY3JlYXRlLXN5c3RlbS11c2VyL2NyZWF0ZS1zeXN0ZW0tdXNlci5jb21wb25lbnQnO1xuaW1wb3J0IHsgSWROckxvb2t1cENvbXBvbmVudCB9IGZyb20gJy4vc3RlcHMvaWQtbnItbG9va3VwL2lkLW5yLWxvb2t1cC5jb21wb25lbnQnO1xuaW1wb3J0IHsgUmVnaXN0ZXJDb21wb25lbnQgfSBmcm9tICcuL3N0ZXBzL3JlZ2lzdGVyL3JlZ2lzdGVyLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBVcGRhdGVEZXRhaWxzQ29tcG9uZW50IH0gZnJvbSAnLi9zdGVwcy91cGRhdGUtZGV0YWlscy91cGRhdGUtZGV0YWlscy5jb21wb25lbnQnO1xuaW1wb3J0IHsgVGV4dFF1ZXN0aW9uQ29tcG9uZW50IH0gZnJvbSAnLi9zdGVwcy9xdWVzdGlvbm5haXJlL3RleHQtcXVlc3Rpb24uY29tcG9uZW50JztcbmltcG9ydCB7IENoZWNrYm94Q29tcG9uZW50IH0gZnJvbSAnLi9zdGVwcy9xdWVzdGlvbm5haXJlL2NoZWNrYm94LWNvbXBvbmVudCc7XG5pbXBvcnQgeyBSYWRpb0J1dHRvbkNvbXBvbmVudCB9IGZyb20gJy4vc3RlcHMvcXVlc3Rpb25uYWlyZS9yYWRpby1idXR0b24tY29tcG9uZW50JztcbmltcG9ydCB7IFF1ZXN0aW9uTGluZUNvbXBvbmVudCB9IGZyb20gJy4vc3RlcHMvcXVlc3Rpb25uYWlyZS9xdWVzdGlvbi1saW5lLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBRdWVzdGlvbm5haXJlQ29tcG9uZW50IH0gZnJvbSAnLi9zdGVwcy9xdWVzdGlvbm5haXJlL3F1ZXN0aW9ubmFpcmUuY29tcG9uZW50JztcbmltcG9ydCB7IFNlbGVjdFJvbGUyQ29tcG9uZW50IH0gZnJvbSAnLi9zdGVwcy9zZWxlY3Qtcm9sZTIvc2VsZWN0LXJvbGUyLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBXYWl0Q29tcG9uZW50IH0gZnJvbSAnLi9zdGVwcy93YWl0L3dhaXQuY29tcG9uZW50JztcbmltcG9ydCB7IEZhY2lhbEF1dGhlbnRpY2F0aW9uUmVmQ29tcG9uZW50IH0gZnJvbSAnLi9zdGVwcy9mYWNpYWwtYXV0aGVudGljYXRpb24tcmVmL2ZhY2lhbC1hdXRoZW50aWNhdGlvbi1yZWYuY29tcG9uZW50JztcbmltcG9ydCB7IEZhY2lhbEF1dGhlbnRpY2F0aW9uQ29tcG9uZW50IH0gZnJvbSAnLi9zdGVwcy9mYWNpYWwtYXV0aGVudGljYXRpb24vZmFjaWFsLWF1dGhlbnRpY2F0aW9uLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDb2RlUmVhZGVyQ29tcG9uZW50IH0gZnJvbSAnLi9zdGVwcy9jb2RlLXJlYWRlci9jb2RlLXJlYWRlci5jb21wb25lbnQnO1xuXG5pbXBvcnQgeyBQcm9qZWN0U2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvcHJvamVjdC5zZXJ2aWNlJztcbmltcG9ydCB7IE1haW50ZW5hbmNlU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvbWFpbnRlbmFuY2Uuc2VydmljZSc7XG5pbXBvcnQgeyBBY3RvclNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL2FjdG9yLnNlcnZpY2UnO1xuaW1wb3J0IHsgQW5hbHlzaXNTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9hbmFseXNpcy5zZXJ2aWNlJztcbmltcG9ydCB7IEFjdW1lbkNvbmZpZ3VyYXRpb24gfSBmcm9tICcuL3NlcnZpY2VzL2FjdW1lbi1jb25maWd1cmF0aW9uJztcblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgQWN1bWVuTGliQ29tcG9uZW50LFxuICAgIEluVHJheUNvbXBvbmVudCxcbiAgICBJblRyYXlBbGVydENvbXBvbmVudCxcbiAgICBXaXphcmRDb21wb25lbnQsXG4gICAgVXNlclByb2plY3RzQ29tcG9uZW50LFxuICAgIFVzZXJQcm9qZWN0czJDb21wb25lbnQsXG4gICAgU3RlcEluc3RhbmNlQ29tcG9uZW50LFxuICAgIFN0ZXBJbnN0YW5jZUxpc3RDb21wb25lbnQsXG4gICAgU3RlcEluc3RhbmNlQWN0aXZlQ29tcG9uZW50LFxuICAgIFByb2Nlc3NIaXN0b3J5Q29tcG9uZW50LFxuICAgIExvYWRpbmdJbmRpY2F0b3IsXG4gICAgUGFydGljaXBhbnRTZWFyY2gsXG4gICAgTWVzc2FnZUNvbXBvbmVudCxcbiAgICBDcmVhdGVTeXN0ZW1Vc2VyQ29tcG9uZW50LFxuICAgIElkTnJMb29rdXBDb21wb25lbnQsXG4gICAgUmVnaXN0ZXJDb21wb25lbnQsXG4gICAgVXBkYXRlRGV0YWlsc0NvbXBvbmVudCxcbiAgICBUZXh0UXVlc3Rpb25Db21wb25lbnQsXG4gICAgQ2hlY2tib3hDb21wb25lbnQsXG4gICAgUmFkaW9CdXR0b25Db21wb25lbnQsXG4gICAgUXVlc3Rpb25MaW5lQ29tcG9uZW50LFxuICAgIFF1ZXN0aW9ubmFpcmVDb21wb25lbnQsXG4gICAgU2VsZWN0Um9sZTJDb21wb25lbnQsXG4gICAgV2FpdENvbXBvbmVudCxcbiAgICBGYWNpYWxBdXRoZW50aWNhdGlvblJlZkNvbXBvbmVudCxcbiAgICBGYWNpYWxBdXRoZW50aWNhdGlvbkNvbXBvbmVudCxcbiAgICBDb2RlUmVhZGVyQ29tcG9uZW50XG4gIF0sXG4gIGltcG9ydHM6IFtcbiAgICBGb3Jtc01vZHVsZSxcbiAgICAvLyBCcm93c2VyTW9kdWxlLFxuICAgIENvbW1vbk1vZHVsZSxcbiAgICBIdHRwQ2xpZW50TW9kdWxlXG4gIF0sXG4gIGV4cG9ydHM6IFtcbiAgICBBY3VtZW5MaWJDb21wb25lbnQsXG4gICAgSW5UcmF5Q29tcG9uZW50LFxuICAgIEluVHJheUFsZXJ0Q29tcG9uZW50LFxuICAgIFdpemFyZENvbXBvbmVudCxcbiAgICBVc2VyUHJvamVjdHNDb21wb25lbnQsXG4gICAgVXNlclByb2plY3RzMkNvbXBvbmVudCxcbiAgICBTdGVwSW5zdGFuY2VDb21wb25lbnQsXG4gICAgU3RlcEluc3RhbmNlTGlzdENvbXBvbmVudCxcbiAgICBTdGVwSW5zdGFuY2VBY3RpdmVDb21wb25lbnQsXG4gICAgUHJvY2Vzc0hpc3RvcnlDb21wb25lbnRcbiAgXSxcbiAgcHJvdmlkZXJzOiBbXG4gICAgUHJvamVjdFNlcnZpY2UsXG4gICAgTWFpbnRlbmFuY2VTZXJ2aWNlLFxuICAgIEFjdG9yU2VydmljZSxcbiAgICBBbmFseXNpc1NlcnZpY2VcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBBY3VtZW5MaWJNb2R1bGUge1xuICBzdGF0aWMgZm9yUm9vdChhY3VtZW5Db25maWd1cmF0aW9uOiBBY3VtZW5Db25maWd1cmF0aW9uKTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIG5nTW9kdWxlOiBBY3VtZW5MaWJNb2R1bGUsXG4gICAgICBwcm92aWRlcnM6IFtcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEFjdW1lbkNvbmZpZ3VyYXRpb24sXG4gICAgICAgICAgdXNlVmFsdWU6IGFjdW1lbkNvbmZpZ3VyYXRpb24sXG4gICAgICAgIH0sXG4gICAgICBdLFxuICAgIH07XG4gIH1cbn1cbiJdfQ==