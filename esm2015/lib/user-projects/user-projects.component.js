/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ActorService } from '../services/actor.service';
import { ProjectService } from '../services/project.service';
import { AcumenConfiguration } from '../services/acumen-configuration';
export class UserProjectsComponent {
    /**
     * @param {?} actorService
     * @param {?} projectService
     * @param {?} acumenConfiguration
     */
    constructor(actorService, projectService, acumenConfiguration) {
        this.actorService = actorService;
        this.projectService = projectService;
        this.acumenConfiguration = acumenConfiguration;
        this.onProjectSelected = new EventEmitter();
        this.projects = [];
        this.loading = false;
        if (acumenConfiguration.backendServiceUrl) {
            this.serverUrl = acumenConfiguration.backendServiceUrl;
        }
        else {
            this.serverUrl = "http://www.healthacumen.co.za/insight/";
        }
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.loading = true;
        if (this.username) {
            this.actorService.getSystemUser(this.username).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                if (result) {
                    this.userId = result.id;
                    this.actorId = result.actor.id;
                }
                this.refreshProjects();
            })).catch((/**
             * @param {?} error
             * @return {?}
             */
            error => {
                this.loading = false;
            }));
        }
        else {
            this.refreshProjects();
        }
    }
    /**
     * @return {?}
     */
    refreshProjects() {
        // if (this.userId) {
        //   this.projectService.getUserProjects(this.userId).then(result => {
        //     this.projects = result;
        //     this.loading = false;
        //   }).catch(error => {
        //     this.loading = false;
        //   });
        // } else {
        //   this.loading = false;
        // }
        if (this.actorId) {
            this.projects = [];
            this.projectService.getProjects(this.actorId).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                /** @type {?} */
                let projectIds = {};
                for (let project of result) {
                    if (!projectIds[project.id]) {
                        this.projects.push(project);
                        projectIds[project.id] = project;
                    }
                }
                this.loading = false;
            })).catch((/**
             * @param {?} error
             * @return {?}
             */
            error => {
                this.loading = false;
            }));
        }
        else {
            this.loading = false;
        }
    }
    /**
     * @param {?} project
     * @return {?}
     */
    projectSelected(project) {
        this.onProjectSelected.emit(project);
    }
}
UserProjectsComponent.decorators = [
    { type: Component, args: [{
                selector: 'acn-user-projects',
                template: "<div class=\"user-projects-component\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n\n  <div *ngIf=\"projects\" class=\"project-list\">\n    <div *ngFor=\"let project of projects; odd as isOdd\" class=\"project-row\" [ngClass]=\"{ 'oddRow': isOdd }\" (click)=\"projectSelected(project)\">\n      <div class=\"project-image\">\n        <img src=\"{{ serverUrl }}spring/projectIcon/image.png?id={{ project.id }}\">\n      </div>\n      <div class=\"description\">\n        {{ project.projectName }}\n\n      </div>\n      <div class=\"cursor-image\">\n        <img src=\"data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAUVJREFUeJzt2EtOw0AURNELbCw7o9hZdgaTtBigBLv9+vM+JfUscnyPZw220+OknIDvx9HSN1kw8RufDkH8jU+DIJ7Hh0cQ/8eHRRDH48MhiPPxYRBEf7x7BHE93i2CsIt3hyDs47dBeF/8/58sRvg48Js78AbcBr3D7fH8+6Dnv9wRAAiMcBQAgiKcAYCACGcBIBhCDwAEQugFgCAIVwAgAMJVAHCOYAEAjhGsAMApgiUAOESwBgBnCCMAwBHCKABwgjASABwgjAaAzRFmAMDGCLMAYFOE1XeCqSbG3S5vccP8aqLiK77ijY9mhfRMVHzFV7zx0ayQnomKr/iKNz6aFdIzUfEVX/      HGR7NCeiYqPmb86jvBLzb/+m0i2JfvmUgc3yYSx7eJxPFtInF8m0gc3yYSx7eJxPFtInF8m0gc3yacxf8AAP323bWbSe8AAAAASUVORK5CYII=\">\n      </div>\n    </div>\n  </div>\n</div>\n",
                styles: [".user-projects-component{width:100%;height:100%;position:relative}.user-projects-component .project-list{width:100%;height:100%;overflow-y:scroll}.user-projects-component .project-list .project-row{display:table;width:100%;padding:5px;cursor:pointer}.user-projects-component .project-list .project-row .project-image{display:table-cell;width:32px;text-align:center;vertical-align:middle}.user-projects-component .project-list .project-row .project-image img{width:24px;height:24px}.user-projects-component .project-list .project-row .description{display:table-cell;width:calc(100% - 64px);padding-left:10px;padding-top:3px;font-size:14px;vertical-align:middle}.user-projects-component .project-list .project-row .cursor-image{display:table-cell;width:32px;text-align:center;vertical-align:middle}.user-projects-component .project-list .project-row .cursor-image img{width:16px;height:16px}.user-projects-component .project-list .oddRow{background-color:#f2f2f2}"]
            }] }
];
/** @nocollapse */
UserProjectsComponent.ctorParameters = () => [
    { type: ActorService },
    { type: ProjectService },
    { type: AcumenConfiguration }
];
UserProjectsComponent.propDecorators = {
    username: [{ type: Input }],
    actorId: [{ type: Input }],
    userId: [{ type: Input }],
    onProjectSelected: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    UserProjectsComponent.prototype.username;
    /** @type {?} */
    UserProjectsComponent.prototype.actorId;
    /** @type {?} */
    UserProjectsComponent.prototype.userId;
    /** @type {?} */
    UserProjectsComponent.prototype.onProjectSelected;
    /** @type {?} */
    UserProjectsComponent.prototype.serverUrl;
    /** @type {?} */
    UserProjectsComponent.prototype.projects;
    /** @type {?} */
    UserProjectsComponent.prototype.loading;
    /**
     * @type {?}
     * @private
     */
    UserProjectsComponent.prototype.actorService;
    /**
     * @type {?}
     * @private
     */
    UserProjectsComponent.prototype.projectService;
    /**
     * @type {?}
     * @private
     */
    UserProjectsComponent.prototype.acumenConfiguration;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci1wcm9qZWN0cy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL3VzZXItcHJvamVjdHMvdXNlci1wcm9qZWN0cy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFL0UsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ3pELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUU3RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQU92RSxNQUFNLE9BQU8scUJBQXFCOzs7Ozs7SUFVaEMsWUFBb0IsWUFBMEIsRUFBVSxjQUE4QixFQUMxRSxtQkFBd0M7UUFEaEMsaUJBQVksR0FBWixZQUFZLENBQWM7UUFBVSxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDMUUsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQVAxQyxzQkFBaUIsR0FBRyxJQUFJLFlBQVksRUFBVyxDQUFDO1FBRzFELGFBQVEsR0FBbUIsRUFBRSxDQUFDO1FBQzlCLFlBQU8sR0FBWSxLQUFLLENBQUM7UUFJdkIsSUFBSSxtQkFBbUIsQ0FBQyxpQkFBaUIsRUFBRTtZQUN6QyxJQUFJLENBQUMsU0FBUyxHQUFHLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDO1NBQ3hEO2FBQU07WUFDTCxJQUFJLENBQUMsU0FBUyxHQUFHLHdDQUF3QyxDQUFDO1NBQzNEO0lBQ0gsQ0FBQzs7OztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNwQixJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakIsSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUk7Ozs7WUFBQyxNQUFNLENBQUMsRUFBRTtnQkFDM0QsSUFBSSxNQUFNLEVBQUU7b0JBQ1YsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDO29CQUN4QixJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDO2lCQUNoQztnQkFFRCxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDekIsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztZQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUNmLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQ3ZCLENBQUMsRUFBQyxDQUFDO1NBQ0o7YUFBTTtZQUNMLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztTQUN4QjtJQUNILENBQUM7Ozs7SUFFRCxlQUFlO1FBQ2IscUJBQXFCO1FBQ3JCLHNFQUFzRTtRQUN0RSw4QkFBOEI7UUFDOUIsNEJBQTRCO1FBQzVCLHdCQUF3QjtRQUN4Qiw0QkFBNEI7UUFDNUIsUUFBUTtRQUNSLFdBQVc7UUFDWCwwQkFBMEI7UUFDMUIsSUFBSTtRQUNKLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNoQixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztZQUNuQixJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSTs7OztZQUFDLE1BQU0sQ0FBQyxFQUFFOztvQkFDdEQsVUFBVSxHQUFHLEVBQUU7Z0JBQ25CLEtBQUssSUFBSSxPQUFPLElBQUksTUFBTSxFQUFFO29CQUMxQixJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsRUFBRTt3QkFDM0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7d0JBQzVCLFVBQVUsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDO3FCQUNsQztpQkFDRjtnQkFDRCxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztZQUN2QixDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1lBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ2YsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFDdkIsQ0FBQyxFQUFDLENBQUM7U0FDSjthQUFNO1lBQ0wsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7U0FDdEI7SUFDSCxDQUFDOzs7OztJQUVELGVBQWUsQ0FBQyxPQUFnQjtRQUM5QixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7OztZQTFFRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLG1CQUFtQjtnQkFDN0IscXRDQUE2Qzs7YUFFOUM7Ozs7WUFUUSxZQUFZO1lBQ1osY0FBYztZQUVkLG1CQUFtQjs7O3VCQVF6QixLQUFLO3NCQUNMLEtBQUs7cUJBQ0wsS0FBSztnQ0FDTCxNQUFNOzs7O0lBSFAseUNBQTBCOztJQUMxQix3Q0FBeUI7O0lBQ3pCLHVDQUF3Qjs7SUFDeEIsa0RBQTBEOztJQUUxRCwwQ0FBVTs7SUFDVix5Q0FBOEI7O0lBQzlCLHdDQUF5Qjs7Ozs7SUFFYiw2Q0FBa0M7Ozs7O0lBQUUsK0NBQXNDOzs7OztJQUNsRixvREFBZ0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IEFjdG9yU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2FjdG9yLnNlcnZpY2UnO1xuaW1wb3J0IHsgUHJvamVjdFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9wcm9qZWN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgUHJvamVjdCB9IGZyb20gJy4uL2RvbWFpbi9wcm9qZWN0JztcbmltcG9ydCB7IEFjdW1lbkNvbmZpZ3VyYXRpb24gfSBmcm9tICcuLi9zZXJ2aWNlcy9hY3VtZW4tY29uZmlndXJhdGlvbic7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2Fjbi11c2VyLXByb2plY3RzJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3VzZXItcHJvamVjdHMuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi91c2VyLXByb2plY3RzLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgVXNlclByb2plY3RzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgdXNlcm5hbWU6IHN0cmluZztcbiAgQElucHV0KCkgYWN0b3JJZDogbnVtYmVyO1xuICBASW5wdXQoKSB1c2VySWQ6IG51bWJlcjtcbiAgQE91dHB1dCgpIG9uUHJvamVjdFNlbGVjdGVkID0gbmV3IEV2ZW50RW1pdHRlcjxQcm9qZWN0PigpO1xuICAvLyBzZXJ2ZXJVcmwgPSAnaHR0cDovL3d3dy5oZWFsdGhhY3VtZW4uY28uemEvaW5zaWdodC8nO1xuICBzZXJ2ZXJVcmw7XG4gIHByb2plY3RzOiBBcnJheTxQcm9qZWN0PiA9IFtdO1xuICBsb2FkaW5nOiBib29sZWFuID0gZmFsc2U7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBhY3RvclNlcnZpY2U6IEFjdG9yU2VydmljZSwgcHJpdmF0ZSBwcm9qZWN0U2VydmljZTogUHJvamVjdFNlcnZpY2UsXG4gICAgICBwcml2YXRlIGFjdW1lbkNvbmZpZ3VyYXRpb246IEFjdW1lbkNvbmZpZ3VyYXRpb24pIHtcbiAgICBpZiAoYWN1bWVuQ29uZmlndXJhdGlvbi5iYWNrZW5kU2VydmljZVVybCkge1xuICAgICAgdGhpcy5zZXJ2ZXJVcmwgPSBhY3VtZW5Db25maWd1cmF0aW9uLmJhY2tlbmRTZXJ2aWNlVXJsO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNlcnZlclVybCA9IFwiaHR0cDovL3d3dy5oZWFsdGhhY3VtZW4uY28uemEvaW5zaWdodC9cIjtcbiAgICB9XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLmxvYWRpbmcgPSB0cnVlO1xuICAgIGlmICh0aGlzLnVzZXJuYW1lKSB7XG4gICAgICB0aGlzLmFjdG9yU2VydmljZS5nZXRTeXN0ZW1Vc2VyKHRoaXMudXNlcm5hbWUpLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgICAgaWYgKHJlc3VsdCkge1xuICAgICAgICAgIHRoaXMudXNlcklkID0gcmVzdWx0LmlkO1xuICAgICAgICAgIHRoaXMuYWN0b3JJZCA9IHJlc3VsdC5hY3Rvci5pZDtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMucmVmcmVzaFByb2plY3RzKCk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMucmVmcmVzaFByb2plY3RzKCk7XG4gICAgfVxuICB9XG5cbiAgcmVmcmVzaFByb2plY3RzKCkge1xuICAgIC8vIGlmICh0aGlzLnVzZXJJZCkge1xuICAgIC8vICAgdGhpcy5wcm9qZWN0U2VydmljZS5nZXRVc2VyUHJvamVjdHModGhpcy51c2VySWQpLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAvLyAgICAgdGhpcy5wcm9qZWN0cyA9IHJlc3VsdDtcbiAgICAvLyAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgLy8gICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgLy8gICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgIC8vICAgfSk7XG4gICAgLy8gfSBlbHNlIHtcbiAgICAvLyAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgIC8vIH1cbiAgICBpZiAodGhpcy5hY3RvcklkKSB7XG4gICAgICB0aGlzLnByb2plY3RzID0gW107XG4gICAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmdldFByb2plY3RzKHRoaXMuYWN0b3JJZCkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICBsZXQgcHJvamVjdElkcyA9IHt9O1xuICAgICAgICBmb3IgKGxldCBwcm9qZWN0IG9mIHJlc3VsdCkge1xuICAgICAgICAgIGlmICghcHJvamVjdElkc1twcm9qZWN0LmlkXSkge1xuICAgICAgICAgICAgdGhpcy5wcm9qZWN0cy5wdXNoKHByb2plY3QpO1xuICAgICAgICAgICAgcHJvamVjdElkc1twcm9qZWN0LmlkXSA9IHByb2plY3Q7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICB9XG4gIH1cblxuICBwcm9qZWN0U2VsZWN0ZWQocHJvamVjdDogUHJvamVjdCkge1xuICAgIHRoaXMub25Qcm9qZWN0U2VsZWN0ZWQuZW1pdChwcm9qZWN0KTtcbiAgfVxufVxuIl19