/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { DomSanitizer } from '@angular/platform-browser';
import { ActorService } from '../services/actor.service';
import { ProjectService } from '../services/project.service';
import { AnalysisService } from '../services/analysis.service';
export class StepInstanceListComponent {
    /**
     * @param {?} actorService
     * @param {?} projectService
     * @param {?} analysisService
     * @param {?} sanitizer
     */
    constructor(actorService, projectService, analysisService, sanitizer) {
        this.actorService = actorService;
        this.projectService = projectService;
        this.analysisService = analysisService;
        this.sanitizer = sanitizer;
        this.onStepInstanceSelected = new EventEmitter();
        this.stepInstances = [];
        this.selectedStepInstance = null;
        this.loading = false;
        this.descriptions = {};
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.subscription = Observable.timer(1000, 10000).subscribe((/**
         * @param {?} t
         * @return {?}
         */
        t => {
            this.loadStepInstances();
        }));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
    /**
     * @private
     * @return {?}
     */
    loadStepInstances() {
        // this.loading = true;
        // this.descriptions = {};
        this.projectService.getActiveStepInstances(this.stepIds).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            if (result) {
                this.stepInstances = result;
                for (let stepInstance of this.stepInstances) {
                    if (stepInstance.step.stepType === "MESSAGE") {
                        // if (this.descriptions[stepInstance.id]) {
                        this.projectService.getMessageStepHtml(stepInstance.id).then((/**
                         * @param {?} html
                         * @return {?}
                         */
                        (html) => {
                            if (html["text"]) {
                                this.descriptions[stepInstance.id] = this.sanitizer.bypassSecurityTrustHtml(html["text"]);
                            }
                            else {
                                this.descriptions[stepInstance.id] = "<div></div>";
                            }
                        }));
                        // } else {
                        //   this.descriptions[stepInstance.id] = "<div></div>";
                        // }
                    }
                    else if (stepInstance.step.stepType === "PROGRESS_NOTES") {
                        this.descriptions[stepInstance.id] = "<div>" + stepInstance.step.parameters["0"].parameterValue + "</div>";
                    }
                    else {
                        this.descriptions[stepInstance.id] = "<div>" + stepInstance.step.stepType + "</div>";
                    }
                }
            }
            // this.loading = false;
        }), (/**
         * @param {?} error
         * @return {?}
         */
        error => {
            // this.loading = false;
        }));
    }
    /**
     * @param {?} stepInstance
     * @return {?}
     */
    stepInstanceSelected(stepInstance) {
        this.selectedStepInstance = stepInstance;
        this.onStepInstanceSelected.emit(stepInstance);
    }
}
StepInstanceListComponent.decorators = [
    { type: Component, args: [{
                selector: 'acn-step-instance-list',
                template: "<div class=\"step-instance-list\">\n  <!-- <loading-indicator [show]=\"loading\"></loading-indicator> -->\n\n  <div class=\"step-list\">\n    <div *ngFor=\"let stepInstance of stepInstances; odd as isOdd\">\n      <div class=\"in-tray-row\" [ngClass]=\"{ 'oddRow': isOdd, 'selectedRow': selectedStepInstance && selectedStepInstance.id === stepInstance.id }\"\n          (click)=\"stepInstanceSelected(stepInstance)\">\n        <div class=\"description\">\n          <div class=\"project\">\n            <div *ngIf=\"descriptions[stepInstance.id]\" class=\"message-content\" [innerHTML]=\"descriptions[stepInstance.id]\">\n            </div>\n          </div>\n          <div class=\"step-details\">\n            <div class=\"activation-date\">\n              Activated: {{ stepInstance.activationDate | date: 'yyyy-MM-dd HH:mm:ss' }}\n            </div>\n          </div>\n        </div>\n        <div class=\"cursor-image\">\n          <img src=\"data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAUVJREFUeJzt2EtOw0AURNELbCw7o9hZdgaTtBigBLv9+vM+JfUscnyPZw220+OknIDvx9HSN1kw8RufDkH8jU+DIJ7Hh0cQ/8eHRRDH48MhiPPxYRBEf7x7BHE93i2CsIt3hyDs47dBeF/8/58sRvg48Js78AbcBr3D7fH8+6Dnv9wRAAiMcBQAgiKcAYCACGcBIBhCDwAEQugFgCAIVwAgAMJVAHCOYAEAjhGsAMApgiUAOESwBgBnCCMAwBHCKABwgjASABwgjAaAzRFmAMDGCLMAYFOE1XeCqSbG3S5vccP8aqLiK77ijY9mhfRMVHzFV7zx0ayQnomKr/iKNz6aFdIzUfEVX/      HGR7NCeiYqPmb86jvBLzb/+m0i2JfvmUgc3yYSx7eJxPFtInF8m0gc3yYSx7eJxPFtInF8m0gc3yacxf8AAP323bWbSe8AAAAASUVORK5CYII=\">\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n",
                styles: [".step-instance-list{width:100%;height:100%;position:relative}.step-instance-list .step-list{width:100%;height:100%;overflow-y:scroll}.step-instance-list .step-list .in-tray-row{display:table;width:100%;padding:5px;cursor:pointer}.step-instance-list .step-list .in-tray-row .description{display:table-cell;width:calc(100% - 32px);padding-left:10px;vertical-align:middle}.step-instance-list .step-list .in-tray-row .description .project{font-size:14px}.step-instance-list .step-list .in-tray-row .description .step-details{line-height:15px}.step-instance-list .step-list .in-tray-row .description .step-details .step{font-size:15px}.step-instance-list .step-list .in-tray-row .description .step-details .activation-date{font-size:10px}.step-instance-list .step-list .in-tray-row .cursor-image{display:table-cell;width:32px;text-align:center;vertical-align:middle}.step-instance-list .step-list .in-tray-row .cursor-image img{width:16px;height:16px}.step-instance-list .step-list .oddRow{background-color:#f2f2f2}.step-instance-list .step-list .selectedRow{background-color:#abb9d3}"]
            }] }
];
/** @nocollapse */
StepInstanceListComponent.ctorParameters = () => [
    { type: ActorService },
    { type: ProjectService },
    { type: AnalysisService },
    { type: DomSanitizer }
];
StepInstanceListComponent.propDecorators = {
    stepIds: [{ type: Input }],
    onStepInstanceSelected: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    StepInstanceListComponent.prototype.stepIds;
    /** @type {?} */
    StepInstanceListComponent.prototype.onStepInstanceSelected;
    /** @type {?} */
    StepInstanceListComponent.prototype.stepInstances;
    /** @type {?} */
    StepInstanceListComponent.prototype.selectedStepInstance;
    /**
     * @type {?}
     * @private
     */
    StepInstanceListComponent.prototype.subscription;
    /** @type {?} */
    StepInstanceListComponent.prototype.loading;
    /** @type {?} */
    StepInstanceListComponent.prototype.descriptions;
    /**
     * @type {?}
     * @private
     */
    StepInstanceListComponent.prototype.actorService;
    /**
     * @type {?}
     * @private
     */
    StepInstanceListComponent.prototype.projectService;
    /**
     * @type {?}
     * @private
     */
    StepInstanceListComponent.prototype.analysisService;
    /**
     * @type {?}
     * @private
     */
    StepInstanceListComponent.prototype.sanitizer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RlcC1pbnN0YW5jZS1saXN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvc3RlcC1pbnN0YW5jZS1saXN0L3N0ZXAtaW5zdGFuY2UtbGlzdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQXFCLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzFGLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFFckMsT0FBTyxFQUFFLFlBQVksRUFBbUIsTUFBTSwyQkFBMkIsQ0FBQztBQUUxRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDekQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQzdELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQWEvRCxNQUFNLE9BQU8seUJBQXlCOzs7Ozs7O0lBU3BDLFlBQW9CLFlBQTBCLEVBQVUsY0FBOEIsRUFDMUUsZUFBZ0MsRUFBVSxTQUF1QjtRQUR6RCxpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUFVLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUMxRSxvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFBVSxjQUFTLEdBQVQsU0FBUyxDQUFjO1FBUm5FLDJCQUFzQixHQUFHLElBQUksWUFBWSxFQUFnQixDQUFDO1FBQ3BFLGtCQUFhLEdBQXdCLEVBQUUsQ0FBQztRQUN4Qyx5QkFBb0IsR0FBaUIsSUFBSSxDQUFDO1FBRTFDLFlBQU8sR0FBWSxLQUFLLENBQUM7UUFDekIsaUJBQVksR0FBRyxFQUFFLENBQUM7SUFJbEIsQ0FBQzs7OztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsWUFBWSxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDLFNBQVM7Ozs7UUFBQyxDQUFDLENBQUMsRUFBRTtZQUM5RCxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUMzQixDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1QsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3JCLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDakM7SUFDSCxDQUFDOzs7OztJQUVPLGlCQUFpQjtRQUN2Qix1QkFBdUI7UUFDdkIsMEJBQTBCO1FBQzFCLElBQUksQ0FBQyxjQUFjLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxNQUFNLENBQUMsRUFBRTtZQUNyRSxJQUFJLE1BQU0sRUFBRTtnQkFDVixJQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQztnQkFDNUIsS0FBSyxJQUFJLFlBQVksSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO29CQUMzQyxJQUFJLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxLQUFLLFNBQVMsRUFBRTt3QkFDNUMsNENBQTRDO3dCQUMxQyxJQUFJLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJOzs7O3dCQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7NEJBQ3BFLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFO2dDQUNoQixJQUFJLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDOzZCQUMzRjtpQ0FBTTtnQ0FDTCxJQUFJLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsR0FBRyxhQUFhLENBQUM7NkJBQ3BEO3dCQUNILENBQUMsRUFBQyxDQUFDO3dCQUNMLFdBQVc7d0JBQ1gsd0RBQXdEO3dCQUN4RCxJQUFJO3FCQUNMO3lCQUFNLElBQUksWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEtBQUssZ0JBQWdCLEVBQUU7d0JBQ3hELElBQUksQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxjQUFjLEdBQUcsUUFBUSxDQUFDO3FCQUM5Rzt5QkFBTTt3QkFDTCxJQUFJLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLEdBQUcsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO3FCQUN0RjtpQkFDRjthQUNGO1lBQ0Qsd0JBQXdCO1FBQzFCLENBQUM7Ozs7UUFBRSxLQUFLLENBQUMsRUFBRTtZQUNULHdCQUF3QjtRQUMxQixDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsb0JBQW9CLENBQUMsWUFBMEI7UUFDN0MsSUFBSSxDQUFDLG9CQUFvQixHQUFHLFlBQVksQ0FBQztRQUN6QyxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ2pELENBQUM7OztZQWpFRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLHdCQUF3QjtnQkFDbEMsNmtEQUFrRDs7YUFFbkQ7Ozs7WUFkUSxZQUFZO1lBQ1osY0FBYztZQUNkLGVBQWU7WUFKZixZQUFZOzs7c0JBa0JsQixLQUFLO3FDQUNMLE1BQU07Ozs7SUFEUCw0Q0FBZ0M7O0lBQ2hDLDJEQUFvRTs7SUFDcEUsa0RBQXdDOztJQUN4Qyx5REFBMEM7Ozs7O0lBQzFDLGlEQUFtQzs7SUFDbkMsNENBQXlCOztJQUN6QixpREFBa0I7Ozs7O0lBRU4saURBQWtDOzs7OztJQUFFLG1EQUFzQzs7Ozs7SUFDbEYsb0RBQXdDOzs7OztJQUFFLDhDQUErQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBPbkRlc3Ryb3ksIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMvUngnO1xuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcy9SeCc7XG5pbXBvcnQgeyBEb21TYW5pdGl6ZXIsIFNhZmVSZXNvdXJjZVVybCB9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXInO1xuXG5pbXBvcnQgeyBBY3RvclNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9hY3Rvci5zZXJ2aWNlJztcbmltcG9ydCB7IFByb2plY3RTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvcHJvamVjdC5zZXJ2aWNlJztcbmltcG9ydCB7IEFuYWx5c2lzU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2FuYWx5c2lzLnNlcnZpY2UnO1xuaW1wb3J0IHsgU3RlcEluc3RhbmNlIH0gZnJvbSAnLi4vZG9tYWluL3N0ZXAtaW5zdGFuY2UnO1xuaW1wb3J0IHsgUHJvamVjdCB9IGZyb20gJy4uL2RvbWFpbi9wcm9qZWN0JztcbmltcG9ydCB7IEFjdG9yIH0gZnJvbSAnLi4vZG9tYWluL2FjdG9yJztcbmltcG9ydCB7IEJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlIH0gZnJvbSAnLi4vZG9tYWluL2J1c2luZXNzLXByb2Nlc3MtaW5zdGFuY2UnO1xuaW1wb3J0IHsgUXVlc3Rpb25MaW5lIH0gZnJvbSAnLi4vZG9tYWluL3F1ZXN0aW9uLWxpbmUnO1xuaW1wb3J0IHsgSW1hZ2VDb250YWluZXIgfSBmcm9tICcuLi9kb21haW4vaW1hZ2UtY29udGFpbmVyJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYWNuLXN0ZXAtaW5zdGFuY2UtbGlzdCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9zdGVwLWluc3RhbmNlLWxpc3QuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9zdGVwLWluc3RhbmNlLWxpc3QuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBTdGVwSW5zdGFuY2VMaXN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuICBASW5wdXQoKSBzdGVwSWRzOiBBcnJheTxudW1iZXI+O1xuICBAT3V0cHV0KCkgb25TdGVwSW5zdGFuY2VTZWxlY3RlZCA9IG5ldyBFdmVudEVtaXR0ZXI8U3RlcEluc3RhbmNlPigpO1xuICBzdGVwSW5zdGFuY2VzOiBBcnJheTxTdGVwSW5zdGFuY2U+ID0gW107XG4gIHNlbGVjdGVkU3RlcEluc3RhbmNlOiBTdGVwSW5zdGFuY2UgPSBudWxsO1xuICBwcml2YXRlIHN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xuICBsb2FkaW5nOiBib29sZWFuID0gZmFsc2U7XG4gIGRlc2NyaXB0aW9ucyA9IHt9O1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgYWN0b3JTZXJ2aWNlOiBBY3RvclNlcnZpY2UsIHByaXZhdGUgcHJvamVjdFNlcnZpY2U6IFByb2plY3RTZXJ2aWNlLFxuICAgICAgcHJpdmF0ZSBhbmFseXNpc1NlcnZpY2U6IEFuYWx5c2lzU2VydmljZSwgcHJpdmF0ZSBzYW5pdGl6ZXI6IERvbVNhbml0aXplcikge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5zdWJzY3JpcHRpb24gPSBPYnNlcnZhYmxlLnRpbWVyKDEwMDAsIDEwMDAwKS5zdWJzY3JpYmUodCA9PiB7XG4gICAgICB0aGlzLmxvYWRTdGVwSW5zdGFuY2VzKCk7XG4gICAgfSk7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpIHtcbiAgICBpZiAodGhpcy5zdWJzY3JpcHRpb24pIHtcbiAgICAgIHRoaXMuc3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBsb2FkU3RlcEluc3RhbmNlcygpIHtcbiAgICAvLyB0aGlzLmxvYWRpbmcgPSB0cnVlO1xuICAgIC8vIHRoaXMuZGVzY3JpcHRpb25zID0ge307XG4gICAgdGhpcy5wcm9qZWN0U2VydmljZS5nZXRBY3RpdmVTdGVwSW5zdGFuY2VzKHRoaXMuc3RlcElkcykudGhlbihyZXN1bHQgPT4ge1xuICAgICAgaWYgKHJlc3VsdCkge1xuICAgICAgICB0aGlzLnN0ZXBJbnN0YW5jZXMgPSByZXN1bHQ7XG4gICAgICAgIGZvciAobGV0IHN0ZXBJbnN0YW5jZSBvZiB0aGlzLnN0ZXBJbnN0YW5jZXMpIHtcbiAgICAgICAgICBpZiAoc3RlcEluc3RhbmNlLnN0ZXAuc3RlcFR5cGUgPT09IFwiTUVTU0FHRVwiKSB7XG4gICAgICAgICAgICAvLyBpZiAodGhpcy5kZXNjcmlwdGlvbnNbc3RlcEluc3RhbmNlLmlkXSkge1xuICAgICAgICAgICAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmdldE1lc3NhZ2VTdGVwSHRtbChzdGVwSW5zdGFuY2UuaWQpLnRoZW4oKGh0bWwpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAoaHRtbFtcInRleHRcIl0pIHtcbiAgICAgICAgICAgICAgICAgIHRoaXMuZGVzY3JpcHRpb25zW3N0ZXBJbnN0YW5jZS5pZF0gPSB0aGlzLnNhbml0aXplci5ieXBhc3NTZWN1cml0eVRydXN0SHRtbChodG1sW1widGV4dFwiXSk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgIHRoaXMuZGVzY3JpcHRpb25zW3N0ZXBJbnN0YW5jZS5pZF0gPSBcIjxkaXY+PC9kaXY+XCI7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIC8vIH0gZWxzZSB7XG4gICAgICAgICAgICAvLyAgIHRoaXMuZGVzY3JpcHRpb25zW3N0ZXBJbnN0YW5jZS5pZF0gPSBcIjxkaXY+PC9kaXY+XCI7XG4gICAgICAgICAgICAvLyB9XG4gICAgICAgICAgfSBlbHNlIGlmIChzdGVwSW5zdGFuY2Uuc3RlcC5zdGVwVHlwZSA9PT0gXCJQUk9HUkVTU19OT1RFU1wiKSB7XG4gICAgICAgICAgICAgIHRoaXMuZGVzY3JpcHRpb25zW3N0ZXBJbnN0YW5jZS5pZF0gPSBcIjxkaXY+XCIgKyBzdGVwSW5zdGFuY2Uuc3RlcC5wYXJhbWV0ZXJzW1wiMFwiXS5wYXJhbWV0ZXJWYWx1ZSArIFwiPC9kaXY+XCI7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuZGVzY3JpcHRpb25zW3N0ZXBJbnN0YW5jZS5pZF0gPSBcIjxkaXY+XCIgKyBzdGVwSW5zdGFuY2Uuc3RlcC5zdGVwVHlwZSArIFwiPC9kaXY+XCI7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgICAvLyB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICB9LCBlcnJvciA9PiB7XG4gICAgICAvLyB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICB9KTtcbiAgfVxuXG4gIHN0ZXBJbnN0YW5jZVNlbGVjdGVkKHN0ZXBJbnN0YW5jZTogU3RlcEluc3RhbmNlKSB7XG4gICAgdGhpcy5zZWxlY3RlZFN0ZXBJbnN0YW5jZSA9IHN0ZXBJbnN0YW5jZTtcbiAgICB0aGlzLm9uU3RlcEluc3RhbmNlU2VsZWN0ZWQuZW1pdChzdGVwSW5zdGFuY2UpO1xuICB9XG59XG4iXX0=