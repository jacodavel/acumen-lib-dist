/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { AcumenConfiguration } from '../services/acumen-configuration';
export class AnalysisService {
    // private backendServiceUrl = 'http://www.healthacumen.co.za/insight/';
    // private backendServiceUrl = 'https://frank.rmsui.co.za/insight/';
    // private backendServiceUrl = 'http://localhost:8888/';
    /**
     * @param {?} httpClient
     * @param {?} acumenConfiguration
     */
    constructor(httpClient, acumenConfiguration) {
        this.httpClient = httpClient;
        this.acumenConfiguration = acumenConfiguration;
        if (acumenConfiguration.backendServiceUrl) {
            this.backendServiceUrl = acumenConfiguration.backendServiceUrl;
        }
        else {
            this.backendServiceUrl = "https://frank.rmsui.co.za/insight/";
        }
    }
    /**
     * @param {?} projectGuid
     * @return {?}
     */
    getProjectCompletionCount(projectGuid) {
        /** @type {?} */
        let url = this.backendServiceUrl + `countserv?pr=${projectGuid}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @return {?}
     */
    getWellnessData() {
        /** @type {?} */
        let url = this.backendServiceUrl + `welldashdplot?actor=359817&project=123`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
}
AnalysisService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
AnalysisService.ctorParameters = () => [
    { type: HttpClient },
    { type: AcumenConfiguration }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    AnalysisService.prototype.backendServiceUrl;
    /**
     * @type {?}
     * @private
     */
    AnalysisService.prototype.httpClient;
    /**
     * @type {?}
     * @private
     */
    AnalysisService.prototype.acumenConfiguration;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5hbHlzaXMuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvYW5hbHlzaXMuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFTLGVBQWUsQ0FBQztBQUM5QyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbEQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBRW5ELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBR3ZFLE1BQU0sT0FBTyxlQUFlOzs7Ozs7OztJQU0xQixZQUFvQixVQUFzQixFQUFVLG1CQUF3QztRQUF4RSxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQVUsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUMxRixJQUFJLG1CQUFtQixDQUFDLGlCQUFpQixFQUFFO1lBQ3pDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxtQkFBbUIsQ0FBQyxpQkFBaUIsQ0FBQztTQUNoRTthQUFNO1lBQ0wsSUFBSSxDQUFDLGlCQUFpQixHQUFHLG9DQUFvQyxDQUFDO1NBQy9EO0lBQ0gsQ0FBQzs7Ozs7SUFFRCx5QkFBeUIsQ0FBQyxXQUFtQjs7WUFDdkMsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxnQkFBZ0IsV0FBVyxFQUFFOztjQUMxRCxPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDOUIsUUFBUSxFQUFFLGtCQUFrQjtTQUM3QixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDNUQsSUFBSTs7OztRQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLG1CQUFBLFFBQVEsRUFBVSxDQUFDLENBQUM7UUFDN0MsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7OztJQUVELGVBQWU7O1lBQ1QsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyx3Q0FBd0M7O2NBQ3JFLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQztZQUM5QixRQUFRLEVBQUUsa0JBQWtCO1NBQzdCLENBQUM7UUFFRixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxFQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRTthQUM1RCxJQUFJOzs7O1FBQUMsUUFBUSxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFVLENBQUMsQ0FBQztRQUM3QyxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7WUF6Q0YsVUFBVTs7OztZQUxGLFVBQVU7WUFHVixtQkFBbUI7Ozs7Ozs7SUFJMUIsNENBQTBCOzs7OztJQUtkLHFDQUE4Qjs7Ozs7SUFBRSw4Q0FBZ0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gICAgZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgSHR0cEhlYWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5cbmltcG9ydCB7IEFjdW1lbkNvbmZpZ3VyYXRpb24gfSBmcm9tICcuLi9zZXJ2aWNlcy9hY3VtZW4tY29uZmlndXJhdGlvbic7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBBbmFseXNpc1NlcnZpY2Uge1xuICBwcml2YXRlIGJhY2tlbmRTZXJ2aWNlVXJsO1xuICAvLyBwcml2YXRlIGJhY2tlbmRTZXJ2aWNlVXJsID0gJ2h0dHA6Ly93d3cuaGVhbHRoYWN1bWVuLmNvLnphL2luc2lnaHQvJztcbiAgLy8gcHJpdmF0ZSBiYWNrZW5kU2VydmljZVVybCA9ICdodHRwczovL2ZyYW5rLnJtc3VpLmNvLnphL2luc2lnaHQvJztcbiAgLy8gcHJpdmF0ZSBiYWNrZW5kU2VydmljZVVybCA9ICdodHRwOi8vbG9jYWxob3N0Ojg4ODgvJztcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHBDbGllbnQ6IEh0dHBDbGllbnQsIHByaXZhdGUgYWN1bWVuQ29uZmlndXJhdGlvbjogQWN1bWVuQ29uZmlndXJhdGlvbikge1xuICAgIGlmIChhY3VtZW5Db25maWd1cmF0aW9uLmJhY2tlbmRTZXJ2aWNlVXJsKSB7XG4gICAgICB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsID0gYWN1bWVuQ29uZmlndXJhdGlvbi5iYWNrZW5kU2VydmljZVVybDtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5iYWNrZW5kU2VydmljZVVybCA9IFwiaHR0cHM6Ly9mcmFuay5ybXN1aS5jby56YS9pbnNpZ2h0L1wiO1xuICAgIH1cbiAgfVxuXG4gIGdldFByb2plY3RDb21wbGV0aW9uQ291bnQocHJvamVjdEd1aWQ6IHN0cmluZyk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBgY291bnRzZXJ2P3ByPSR7cHJvamVjdEd1aWR9YDtcbiAgICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldCh1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBzdHJpbmcpO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxuICBnZXRXZWxsbmVzc0RhdGEoKTogUHJvbWlzZTxhbnk+IHtcbiAgICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybCArIGB3ZWxsZGFzaGRwbG90P2FjdG9yPTM1OTgxNyZwcm9qZWN0PTEyM2A7XG4gICAgY29uc3QgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQodXJsLCB7aGVhZGVyczogaGVhZGVyc30pLnRvUHJvbWlzZSgpXG4gICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgc3RyaW5nKTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG59XG4iXX0=