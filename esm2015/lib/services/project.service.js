/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { AcumenConfiguration } from '../services/acumen-configuration';
export class ProjectService {
    // private backendServiceUrl = 'http://www.healthacumen.co.za/insight/';
    // private backendServiceUrl = 'https://frank.rmsui.co.za/insight/';
    // private backendServiceUrl = 'http://localhost:8888/';
    /**
     * @param {?} httpClient
     * @param {?} acumenConfiguration
     */
    constructor(httpClient, acumenConfiguration) {
        this.httpClient = httpClient;
        this.acumenConfiguration = acumenConfiguration;
        if (acumenConfiguration.backendServiceUrl) {
            this.backendServiceUrl = acumenConfiguration.backendServiceUrl;
        }
        else {
            this.backendServiceUrl = "https://frank.rmsui.co.za/insight/";
        }
    }
    /**
     * @param {?} projectGuid
     * @param {?} actorId
     * @return {?}
     */
    startWizardProject(projectGuid, actorId) {
        /** @type {?} */
        let url = this.backendServiceUrl;
        if (actorId) {
            url += `rest/project/projects/start2/${projectGuid}/${actorId}`;
        }
        else {
            url += `rest/project/projects/start/wizard/${projectGuid}`;
        }
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} businessProcessInstanceId
     * @return {?}
     */
    getBusinessProcessState(businessProcessInstanceId) {
        /** @type {?} */
        let url = this.backendServiceUrl + "rest/project/projects/state/" + businessProcessInstanceId + "?timestamp=" + new Date().getTime();
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} projectGuid
     * @return {?}
     */
    getProject(projectGuid) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/project/${projectGuid}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} actorId
     * @return {?}
     */
    getProjects(actorId) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/actorProjects/${actorId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} systemUserId
     * @return {?}
     */
    getUserProjects(systemUserId) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/projects/${systemUserId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} projectId
     * @return {?}
     */
    getProjectRoles(projectId) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/projectRoles/${projectId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} fromRecord
     * @param {?} recordCount
     * @param {?} projectId
     * @param {?} projectRoleId
     * @param {?} firstname
     * @param {?} surname
     * @param {?} idNr
     * @param {?} memberNr
     * @param {?} dependantCode
     * @param {?} assigneeId
     * @return {?}
     */
    getProjectActors(fromRecord, recordCount, projectId, projectRoleId, firstname, surname, idNr, memberNr, dependantCode, assigneeId) {
        if (!firstname)
            firstname = "null";
        if (!surname)
            surname = "null";
        if (!idNr)
            idNr = "null";
        if (!memberNr)
            memberNr = "null";
        if (!dependantCode)
            dependantCode = "null";
        // assigneeId = -1;
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/projectActors/${fromRecord}/${recordCount}/${projectId}/${projectRoleId}/${firstname}/${surname}/${idNr}/${memberNr}/${dependantCode}/${assigneeId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} fromRecord
     * @param {?} recordCount
     * @param {?} projectId
     * @param {?} projectRoleId
     * @return {?}
     */
    getProjectActors2(fromRecord, recordCount, projectId, projectRoleId) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/projectActors2/${fromRecord}/${recordCount}/${projectId}/${projectRoleId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} fromRecord
     * @param {?} recordCount
     * @param {?} projectRoleId
     * @param {?} searchField
     * @return {?}
     */
    getProjectActors3(fromRecord, recordCount, projectRoleId, searchField) {
        if (!searchField)
            searchField = "null";
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/projectActors3/${fromRecord}/${recordCount}/${projectRoleId}/${searchField}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} participantId
     * @return {?}
     */
    getStepInstances(participantId) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/stepInstances/${participantId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} projectGuid
     * @param {?} userId
     * @param {?} assigneeId
     * @param {?} participantId
     * @return {?}
     */
    getStepInstances2(projectGuid, userId, assigneeId, participantId) {
        if (!projectGuid) {
            projectGuid = "null";
        }
        if (!userId) {
            userId = -1;
        }
        if (!assigneeId) {
            assigneeId = -1;
        }
        if (!participantId) {
            participantId = -1;
        }
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/stepInstances/${projectGuid}/${userId}/${assigneeId}/${participantId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} stepId
     * @return {?}
     */
    getStepParameters(stepId) {
        /** @type {?} */
        let url = this.backendServiceUrl + "rest/project/stepInstances/parameters/" + stepId;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve(response);
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} actorId
     * @return {?}
     */
    getLatestStepAssignmentDate(actorId) {
        /** @type {?} */
        let url = this.backendServiceUrl + "rest/project/stepInstances/latestAssignment/" + actorId;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} stepInstanceId
     * @param {?} systemUserId
     * @return {?}
     */
    completeStep(stepInstanceId, systemUserId) {
        /** @type {?} */
        let url = this.backendServiceUrl;
        if (systemUserId)
            url += `rest/project/stepInstance/completeStep?stepInstanceId=${stepInstanceId}&systemUserId=${systemUserId}`;
        else
            url += `rest/project/stepInstance/completeStep2?stepInstanceId=${stepInstanceId}`;
        /** @type {?} */
        let headers = new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        return this.httpClient.post(url, JSON.stringify({})).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} stepInstanceId
     * @param {?} latitude
     * @param {?} longitude
     * @return {?}
     */
    updateStepLocation(stepInstanceId, latitude, longitude) {
        /** @type {?} */
        let url = this.backendServiceUrl;
        url += `rest/project/updateStepLocation?stepInstanceId=${stepInstanceId}&latitude=${latitude}&longitude=${longitude}`;
        /** @type {?} */
        let headers = new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        return this.httpClient.post(url, JSON.stringify({})).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} stepInstanceId
     * @return {?}
     */
    getQuestionnaire(stepInstanceId) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/stepInstance/questionnaire/${stepInstanceId}`;
        /** @type {?} */
        let headers = new HttpHeaders({
            'Accept': 'application/json'
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} stepInstanceId
     * @param {?} responses
     * @param {?} submit
     * @param {?} systemUserId
     * @return {?}
     */
    updateQuestionnaire(stepInstanceId, responses, submit, systemUserId) {
        /** @type {?} */
        let url = this.backendServiceUrl + 'rest/project/stepInstance/questionnaire/responses';
        /** @type {?} */
        let headers = new HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        /** @type {?} */
        let questionnaireResponse = {
            stepInstanceId: stepInstanceId,
            responses: responses,
            submit: submit,
            systemUserId: systemUserId
        };
        return this.httpClient.post(url, JSON.stringify(questionnaireResponse), { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} stepInstanceId
     * @return {?}
     */
    getMessageStepHtml(stepInstanceId) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/stepInstance/message/${stepInstanceId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} businessProcessInstanceId
     * @param {?} idNr
     * @return {?}
     */
    verifyActorIdNr(businessProcessInstanceId, idNr) {
        /** @type {?} */
        let url = this.backendServiceUrl + 'rest/project/verifyActorIdNr';
        /** @type {?} */
        let headers = new HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        /** @type {?} */
        let idNrLookUp = {
            businessProcessInstanceId: businessProcessInstanceId,
            idNr: idNr
        };
        return this.httpClient.post(url, idNrLookUp, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} businessProcessInstanceId
     * @param {?} key
     * @return {?}
     */
    getProcessVariableValue(businessProcessInstanceId, key) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/businessProcessInstance/${businessProcessInstanceId}/${key}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} businessProcessInstanceId
     * @param {?} key
     * @param {?} value
     * @return {?}
     */
    setProcessVariableValue(businessProcessInstanceId, key, value) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/setProcessVariableValue/${businessProcessInstanceId}/${key}/${value}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} actor
     * @return {?}
     */
    updateActor(actor) {
        /** @type {?} */
        let url = this.backendServiceUrl + 'rest/project/updateActor';
        /** @type {?} */
        let headers = new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        return this.httpClient.post(url, actor).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} participantId
     * @param {?} providerId
     * @param {?} timeSlotType
     * @return {?}
     */
    getTimeSlots(participantId, providerId, timeSlotType) {
        if (!participantId)
            participantId = -1;
        if (!providerId)
            providerId = -1;
        if (!timeSlotType)
            timeSlotType = null;
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/timeSlots/${participantId}/${providerId}/${timeSlotType}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} timeSlots
     * @return {?}
     */
    updateTimeSlots(timeSlots) {
        /** @type {?} */
        let url = this.backendServiceUrl + 'rest/project/updateTimeSlots';
        /** @type {?} */
        let headers = new HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        /** @type {?} */
        let params = {
            timeSlots: timeSlots
        };
        return this.httpClient.post(url, params, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} username
     * @param {?} password
     * @param {?} actorId
     * @return {?}
     */
    createSystemUser(username, password, actorId) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/login/createUser/${username}/${password}/${actorId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} stepInstanceId
     * @param {?} imageData
     * @return {?}
     */
    updateFacialAuthenticationReference(stepInstanceId, imageData) {
        /** @type {?} */
        let url = this.backendServiceUrl + 'rest/project/facialAuth/reference';
        /** @type {?} */
        let headers = new HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        /** @type {?} */
        let params = {
            stepInstanceId: stepInstanceId,
            imageData: imageData
        };
        return this.httpClient.post(url, params, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} stepInstanceId
     * @param {?} imageData
     * @return {?}
     */
    facialAuthentication(stepInstanceId, imageData) {
        /** @type {?} */
        let url = this.backendServiceUrl + 'rest/project/facialAuth/authenticate';
        /** @type {?} */
        let headers = new HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        /** @type {?} */
        let params = {
            stepInstanceId: stepInstanceId,
            imageData: imageData
        };
        return this.httpClient.post(url, params, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} projectGuid
     * @param {?} actorId
     * @return {?}
     */
    getProcessHistory(projectGuid, actorId) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/stepInstance/progress/${projectGuid}/${actorId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} stepIds
     * @return {?}
     */
    getActiveStepInstances(stepIds) {
        /** @type {?} */
        let url = this.backendServiceUrl + 'rest/project/stepInstance/active';
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        for (let i = 0; i < stepIds.length; i++) {
            if (i === 0) {
                url += "?stepIds=" + stepIds[i];
            }
            else {
                url += "&stepIds=" + stepIds[i];
            }
        }
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} stepId
     * @param {?} externalId
     * @return {?}
     */
    isStepInstanceActive(stepId, externalId) {
        /** @type {?} */
        let url = this.backendServiceUrl + 'rest/project/stepInstance/active/' + stepId + "/" + externalId;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve(response);
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
}
ProjectService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
ProjectService.ctorParameters = () => [
    { type: HttpClient },
    { type: AcumenConfiguration }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    ProjectService.prototype.backendServiceUrl;
    /**
     * @type {?}
     * @private
     */
    ProjectService.prototype.httpClient;
    /**
     * @type {?}
     * @private
     */
    ProjectService.prototype.acumenConfiguration;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWN1bWVuLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9zZXJ2aWNlcy9wcm9qZWN0LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQVluRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUd2RSxNQUFNLE9BQU8sY0FBYzs7Ozs7Ozs7SUFNekIsWUFBb0IsVUFBc0IsRUFBVSxtQkFBd0M7UUFBeEUsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUFVLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDMUYsSUFBSSxtQkFBbUIsQ0FBQyxpQkFBaUIsRUFBRTtZQUN6QyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsbUJBQW1CLENBQUMsaUJBQWlCLENBQUM7U0FDaEU7YUFBTTtZQUNMLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxvQ0FBb0MsQ0FBQztTQUMvRDtJQUNILENBQUM7Ozs7OztJQUVELGtCQUFrQixDQUFDLFdBQW1CLEVBQUUsT0FBZTs7WUFDakQsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUI7UUFDaEMsSUFBSSxPQUFPLEVBQUU7WUFDVCxHQUFHLElBQUksZ0NBQWdDLFdBQVcsSUFBSSxPQUFPLEVBQUUsQ0FBQztTQUNuRTthQUFNO1lBQ0gsR0FBRyxJQUFJLHNDQUFzQyxXQUFXLEVBQUUsQ0FBQztTQUM5RDs7Y0FFSyxPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDOUIsUUFBUSxFQUFFLGtCQUFrQjtTQUM3QixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBMEIsR0FBRyxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQ3JGLElBQUk7Ozs7UUFBQyxRQUFRLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQTJCLENBQUMsQ0FBQztRQUM5RCxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVELHVCQUF1QixDQUFDLHlCQUFpQzs7WUFDbkQsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyw4QkFBOEIsR0FBRyx5QkFBeUIsR0FBRyxhQUFhLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUU7O2NBQzlILE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQztZQUM5QixRQUFRLEVBQUUsa0JBQWtCO1NBQzdCLENBQUM7UUFFRixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUF1QixHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDbEYsSUFBSTs7OztRQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLG1CQUFBLFFBQVEsRUFBd0IsQ0FBQyxDQUFDO1FBQzNELENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxLQUFLLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBRUQsVUFBVSxDQUFDLFdBQW1COztZQUN4QixHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFHLHdCQUF3QixXQUFXLEVBQUU7O2NBQ2xFLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQztZQUM5QixRQUFRLEVBQUUsa0JBQWtCO1NBQzdCLENBQUM7UUFFRixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFVLEdBQUcsRUFBRSxFQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRTthQUNyRSxJQUFJOzs7O1FBQUMsUUFBUSxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFXLENBQUMsQ0FBQztRQUM5QyxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxPQUFlOztZQUNyQixHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFHLDhCQUE4QixPQUFPLEVBQUU7O2NBQ3BFLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQztZQUM5QixRQUFRLEVBQUUsa0JBQWtCO1NBQzdCLENBQUM7UUFFRixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFZLEdBQUcsRUFBRSxFQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRTthQUN2RSxJQUFJOzs7O1FBQUMsUUFBUSxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFhLENBQUMsQ0FBQztRQUNoRCxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVELGVBQWUsQ0FBQyxZQUFvQjs7WUFDOUIsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyx5QkFBeUIsWUFBWSxFQUFFOztjQUNwRSxPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDOUIsUUFBUSxFQUFFLGtCQUFrQjtTQUM3QixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBWSxHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDdkUsSUFBSTs7OztRQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLG1CQUFBLFFBQVEsRUFBYSxDQUFDLENBQUM7UUFDaEQsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCxlQUFlLENBQUMsU0FBaUI7O1lBQzNCLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsNkJBQTZCLFNBQVMsRUFBRTs7Y0FDckUsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzlCLFFBQVEsRUFBRSxrQkFBa0I7U0FDN0IsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQWdCLEdBQUcsRUFBRSxFQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRTthQUMzRSxJQUFJOzs7O1FBQUMsUUFBUSxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFpQixDQUFDLENBQUM7UUFDcEQsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7SUFFRCxnQkFBZ0IsQ0FBQyxVQUFrQixFQUFFLFdBQW1CLEVBQUUsU0FBaUIsRUFBRSxhQUFxQixFQUMxRixTQUFpQixFQUFFLE9BQWUsRUFBRSxJQUFZLEVBQUUsUUFBZ0IsRUFBRSxhQUFxQixFQUFFLFVBQWtCO1FBQ25ILElBQUksQ0FBQyxTQUFTO1lBQ1YsU0FBUyxHQUFHLE1BQU0sQ0FBQztRQUV2QixJQUFJLENBQUMsT0FBTztZQUNSLE9BQU8sR0FBRyxNQUFNLENBQUM7UUFFckIsSUFBSSxDQUFDLElBQUk7WUFDTCxJQUFJLEdBQUcsTUFBTSxDQUFDO1FBRWxCLElBQUksQ0FBQyxRQUFRO1lBQ1QsUUFBUSxHQUFHLE1BQU0sQ0FBQztRQUV0QixJQUFJLENBQUMsYUFBYTtZQUNkLGFBQWEsR0FBRyxNQUFNLENBQUM7OztZQUd2QixHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFHLDhCQUE4QixVQUFVLElBQUksV0FBVyxJQUFJLFNBQVMsSUFBSSxhQUFhLElBQUksU0FBUyxJQUFJLE9BQU8sSUFBSSxJQUFJLElBQUksUUFBUSxJQUFJLGFBQWEsSUFBSSxVQUFVLEVBQUU7O2NBQy9MLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQztZQUM5QixRQUFRLEVBQUUsa0JBQWtCO1NBQzdCLENBQUM7UUFFRixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFnQixHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDM0UsSUFBSTs7OztRQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLG1CQUFBLFFBQVEsRUFBaUIsQ0FBQyxDQUFDO1FBQ3BELENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxLQUFLLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7Ozs7O0lBRUQsaUJBQWlCLENBQUMsVUFBa0IsRUFBRSxXQUFtQixFQUFFLFNBQWlCLEVBQUUsYUFBcUI7O1lBQzdGLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsK0JBQStCLFVBQVUsSUFBSSxXQUFXLElBQUksU0FBUyxJQUFJLGFBQWEsRUFBRTs7Y0FDckgsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzlCLFFBQVEsRUFBRSxrQkFBa0I7U0FDN0IsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQWdCLEdBQUcsRUFBRSxFQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRTthQUMzRSxJQUFJOzs7O1FBQUMsUUFBUSxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFpQixDQUFDLENBQUM7UUFDcEQsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7Ozs7SUFFRCxpQkFBaUIsQ0FBQyxVQUFrQixFQUFFLFdBQW1CLEVBQUUsYUFBcUIsRUFBRSxXQUFtQjtRQUNuRyxJQUFJLENBQUMsV0FBVztZQUNaLFdBQVcsR0FBRyxNQUFNLENBQUM7O1lBRXJCLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsK0JBQStCLFVBQVUsSUFBSSxXQUFXLElBQUksYUFBYSxJQUFJLFdBQVcsRUFBRTs7Y0FDdkgsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzlCLFFBQVEsRUFBRSxrQkFBa0I7U0FDN0IsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQWdCLEdBQUcsRUFBRSxFQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRTthQUMzRSxJQUFJOzs7O1FBQUMsUUFBUSxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFpQixDQUFDLENBQUM7UUFDcEQsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCxnQkFBZ0IsQ0FBQyxhQUFxQjs7WUFDaEMsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyw4QkFBOEIsYUFBYSxFQUFFOztjQUMxRSxPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDOUIsUUFBUSxFQUFFLGtCQUFrQjtTQUM3QixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBaUIsR0FBRyxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQzVFLElBQUk7Ozs7UUFBQyxRQUFRLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQWtCLENBQUMsQ0FBQztRQUNyRCxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7OztJQUVELGlCQUFpQixDQUFDLFdBQW1CLEVBQUUsTUFBYyxFQUFFLFVBQWtCLEVBQUUsYUFBcUI7UUFDOUYsSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNoQixXQUFXLEdBQUcsTUFBTSxDQUFDO1NBQ3RCO1FBRUQsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNYLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztTQUNiO1FBRUQsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNmLFVBQVUsR0FBRyxDQUFDLENBQUMsQ0FBQztTQUNqQjtRQUVELElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDbEIsYUFBYSxHQUFHLENBQUMsQ0FBQyxDQUFDO1NBQ3BCOztZQUVHLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsOEJBQThCLFdBQVcsSUFBSSxNQUFNLElBQUksVUFBVSxJQUFJLGFBQWEsRUFBRTs7Y0FDakgsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzlCLFFBQVEsRUFBRSxrQkFBa0I7U0FDN0IsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQWlCLEdBQUcsRUFBRSxFQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRTthQUM1RSxJQUFJOzs7O1FBQUMsUUFBUSxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFrQixDQUFDLENBQUM7UUFDckQsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCxpQkFBaUIsQ0FBQyxNQUFjOztZQUMxQixHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFHLHdDQUF3QyxHQUFHLE1BQU07O2NBQzlFLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQztZQUM5QixRQUFRLEVBQUUsa0JBQWtCO1NBQzdCLENBQUM7UUFFRixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFNLEdBQUcsRUFBRSxFQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRTthQUNqRSxJQUFJOzs7O1FBQUMsUUFBUSxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDbkMsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCwyQkFBMkIsQ0FBQyxPQUFlOztZQUNyQyxHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFHLDhDQUE4QyxHQUFHLE9BQU87O2NBQ3JGLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQztZQUM5QixRQUFRLEVBQUUsa0JBQWtCO1NBQzdCLENBQUM7UUFFRixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFPLEdBQUcsRUFBRSxFQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRTthQUNsRSxJQUFJOzs7O1FBQUMsUUFBUSxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFRLENBQUMsQ0FBQztRQUMzQyxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7SUFFRCxZQUFZLENBQUMsY0FBc0IsRUFBRSxZQUFvQjs7WUFDbkQsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUI7UUFDaEMsSUFBSSxZQUFZO1lBQ1osR0FBRyxJQUFJLHlEQUF5RCxjQUFjLGlCQUFpQixZQUFZLEVBQUUsQ0FBQzs7WUFFOUcsR0FBRyxJQUFJLDBEQUEwRCxjQUFjLEVBQUUsQ0FBQzs7WUFFbEYsT0FBTyxHQUFHLElBQUksT0FBTyxDQUFDO1lBQ3hCLFFBQVEsRUFBRSxrQkFBa0I7WUFDNUIsY0FBYyxFQUFFLGtCQUFrQjtTQUNuQyxDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRTthQUM3RCxJQUFJOzs7O1FBQUMsUUFBUSxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFVLENBQUMsQ0FBQztRQUM3QyxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7O0lBRUQsa0JBQWtCLENBQUMsY0FBc0IsRUFBRSxRQUFnQixFQUFFLFNBQWlCOztZQUN4RSxHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQjtRQUNoQyxHQUFHLElBQUksa0RBQWtELGNBQWMsYUFBYSxRQUFRLGNBQWMsU0FBUyxFQUFFLENBQUM7O1lBRWxILE9BQU8sR0FBRyxJQUFJLE9BQU8sQ0FBQztZQUN4QixRQUFRLEVBQUUsa0JBQWtCO1lBQzVCLGNBQWMsRUFBRSxrQkFBa0I7U0FDbkMsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDN0QsSUFBSTs7OztRQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLG1CQUFBLFFBQVEsRUFBVSxDQUFDLENBQUM7UUFDN0MsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCxnQkFBZ0IsQ0FBQyxjQUFzQjs7WUFDakMsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRywyQ0FBMkMsY0FBYyxFQUFFOztZQUMxRixPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDNUIsUUFBUSxFQUFFLGtCQUFrQjtTQUM3QixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDNUQsSUFBSTs7OztRQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLG1CQUFBLFFBQVEsRUFBaUIsQ0FBQyxDQUFDO1FBQ3BELENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxLQUFLLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7Ozs7O0lBRUQsbUJBQW1CLENBQUMsY0FBc0IsRUFBRSxTQUE2QixFQUFFLE1BQWUsRUFBRSxZQUFvQjs7WUFDMUcsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxtREFBbUQ7O1lBQ2xGLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQztZQUM1QixRQUFRLEVBQUUsa0JBQWtCO1lBQzVCLGNBQWMsRUFBRSxrQkFBa0I7U0FDbkMsQ0FBQzs7WUFFRSxxQkFBcUIsR0FBRztZQUMxQixjQUFjLEVBQUUsY0FBYztZQUM5QixTQUFTLEVBQUUsU0FBUztZQUNwQixNQUFNLEVBQUUsTUFBTTtZQUNkLFlBQVksRUFBRSxZQUFZO1NBQzNCO1FBRUQsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQ3RHLElBQUk7Ozs7UUFBQyxRQUFRLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQVUsQ0FBQyxDQUFDO1FBQzdDLENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxLQUFLLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBRUQsa0JBQWtCLENBQUMsY0FBc0I7O1lBQ25DLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUcscUNBQXFDLGNBQWMsRUFBRTs7Y0FDbEYsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzlCLFFBQVEsRUFBRSxrQkFBa0I7U0FDN0IsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQzVELElBQUk7Ozs7UUFBQyxRQUFRLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQVUsQ0FBQyxDQUFDO1FBQzdDLENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxLQUFLLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7OztJQUVELGVBQWUsQ0FBQyx5QkFBaUMsRUFBRSxJQUFZOztZQUN6RCxHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFHLDhCQUE4Qjs7WUFDN0QsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzVCLFFBQVEsRUFBRSxrQkFBa0I7WUFDNUIsY0FBYyxFQUFFLGtCQUFrQjtTQUNuQyxDQUFDOztZQUVFLFVBQVUsR0FBRztZQUNmLHlCQUF5QixFQUFFLHlCQUF5QjtZQUNwRCxJQUFJLEVBQUUsSUFBSTtTQUNYO1FBRUQsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsVUFBVSxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQ3hFLElBQUk7Ozs7UUFBQyxRQUFRLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQVUsQ0FBQyxDQUFDO1FBQzdDLENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxLQUFLLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNSLENBQUM7Ozs7OztJQUVELHVCQUF1QixDQUFDLHlCQUFpQyxFQUFFLEdBQVc7O1lBQ2hFLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsd0NBQXdDLHlCQUF5QixJQUFJLEdBQUcsRUFBRTs7Y0FDdkcsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzlCLFFBQVEsRUFBRSxrQkFBa0I7U0FDN0IsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQzVELElBQUk7Ozs7UUFBQyxRQUFRLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQVUsQ0FBQyxDQUFDO1FBQzdDLENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxLQUFLLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7Ozs7SUFFRCx1QkFBdUIsQ0FBQyx5QkFBaUMsRUFBRSxHQUFXLEVBQUUsS0FBYTs7WUFDL0UsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyx3Q0FBd0MseUJBQXlCLElBQUksR0FBRyxJQUFJLEtBQUssRUFBRTs7Y0FDaEgsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzlCLFFBQVEsRUFBRSxrQkFBa0I7U0FDN0IsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQzVELElBQUk7Ozs7UUFBQyxRQUFRLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQVUsQ0FBQyxDQUFDO1FBQzdDLENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxLQUFLLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBRUQsV0FBVyxDQUFDLEtBQVk7O1lBQ2xCLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsMEJBQTBCOztZQUN6RCxPQUFPLEdBQUcsSUFBSSxPQUFPLENBQUM7WUFDeEIsUUFBUSxFQUFFLGtCQUFrQjtZQUM1QixjQUFjLEVBQUUsa0JBQWtCO1NBQ25DLENBQUM7UUFFRixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDaEQsSUFBSTs7OztRQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLG1CQUFBLFFBQVEsRUFBVSxDQUFDLENBQUM7UUFDN0MsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7OztJQUVELFlBQVksQ0FBQyxhQUFxQixFQUFFLFVBQWtCLEVBQUUsWUFBb0I7UUFDMUUsSUFBSSxDQUFDLGFBQWE7WUFDZCxhQUFhLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFFdkIsSUFBSSxDQUFDLFVBQVU7WUFDWCxVQUFVLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFFcEIsSUFBSSxDQUFDLFlBQVk7WUFDYixZQUFZLEdBQUcsSUFBSSxDQUFDOztZQUVwQixHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFHLDBCQUEwQixhQUFhLElBQUksVUFBVSxJQUFJLFlBQVksRUFBRTs7Y0FDcEcsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzlCLFFBQVEsRUFBRSxrQkFBa0I7U0FDN0IsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQzVELElBQUk7Ozs7UUFBQyxRQUFRLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQWMsQ0FBQyxDQUFDO1FBQ2pELENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxLQUFLLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBRUQsZUFBZSxDQUFDLFNBQXFCOztZQUMvQixHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFHLDhCQUE4Qjs7WUFDN0QsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzFCLFFBQVEsRUFBRSxrQkFBa0I7WUFDNUIsY0FBYyxFQUFFLGtCQUFrQjtTQUNyQyxDQUFDOztZQUVFLE1BQU0sR0FBRztZQUNULFNBQVMsRUFBRSxTQUFTO1NBQ3ZCO1FBRUQsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsTUFBTSxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQ3BFLElBQUk7Ozs7UUFBQyxRQUFRLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQWMsQ0FBQyxDQUFDO1FBQ2pELENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxLQUFLLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNSLENBQUM7Ozs7Ozs7SUFFRCxnQkFBZ0IsQ0FBQyxRQUFnQixFQUFFLFFBQWdCLEVBQUUsT0FBZTs7WUFDOUQsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyx5QkFBeUIsUUFBUSxJQUFJLFFBQVEsSUFBSSxPQUFPLEVBQUU7O2NBQ3ZGLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQztZQUM5QixRQUFRLEVBQUUsa0JBQWtCO1NBQzdCLENBQUM7UUFFRixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxFQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRTthQUM1RCxJQUFJOzs7O1FBQUMsUUFBUSxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFVLENBQUMsQ0FBQztRQUM3QyxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7SUFFRCxtQ0FBbUMsQ0FBQyxjQUFzQixFQUFFLFNBQWlCOztZQUN2RSxHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFHLG1DQUFtQzs7WUFDbEUsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzFCLFFBQVEsRUFBRSxrQkFBa0I7WUFDNUIsY0FBYyxFQUFFLGtCQUFrQjtTQUNyQyxDQUFDOztZQUVFLE1BQU0sR0FBRztZQUNULGNBQWMsRUFBRSxjQUFjO1lBQzlCLFNBQVMsRUFBRSxTQUFTO1NBQ3ZCO1FBRUQsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsTUFBTSxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQ3BFLElBQUk7Ozs7UUFBQyxRQUFRLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQWMsQ0FBQyxDQUFDO1FBQ2pELENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxLQUFLLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNSLENBQUM7Ozs7OztJQUVELG9CQUFvQixDQUFDLGNBQXNCLEVBQUUsU0FBaUI7O1lBQ3hELEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsc0NBQXNDOztZQUNyRSxPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDMUIsUUFBUSxFQUFFLGtCQUFrQjtZQUM1QixjQUFjLEVBQUUsa0JBQWtCO1NBQ3JDLENBQUM7O1lBRUUsTUFBTSxHQUFHO1lBQ1QsY0FBYyxFQUFFLGNBQWM7WUFDOUIsU0FBUyxFQUFFLFNBQVM7U0FDdkI7UUFFRCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDcEUsSUFBSTs7OztRQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLG1CQUFBLFFBQVEsRUFBYyxDQUFDLENBQUM7UUFDakQsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1IsQ0FBQzs7Ozs7O0lBRUQsaUJBQWlCLENBQUMsV0FBbUIsRUFBRSxPQUFlOztZQUNoRCxHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFHLHNDQUFzQyxXQUFXLElBQUksT0FBTyxFQUFFOztjQUMzRixPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDOUIsUUFBUSxFQUFFLGtCQUFrQjtTQUM3QixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDNUQsSUFBSTs7OztRQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLG1CQUFBLFFBQVEsRUFBdUIsQ0FBQyxDQUFDO1FBQzFELENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxLQUFLLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBRUQsc0JBQXNCLENBQUMsT0FBc0I7O1lBQ3ZDLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsa0NBQWtDOztjQUMvRCxPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDOUIsUUFBUSxFQUFFLGtCQUFrQjtTQUM3QixDQUFDO1FBRUYsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDdkMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUNYLEdBQUcsSUFBSSxXQUFXLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ2pDO2lCQUFNO2dCQUNMLEdBQUcsSUFBSSxXQUFXLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ2pDO1NBQ0Y7UUFFRCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxFQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRTthQUM1RCxJQUFJOzs7O1FBQUMsUUFBUSxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUF1QixDQUFDLENBQUM7UUFDMUQsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7O0lBRUQsb0JBQW9CLENBQUMsTUFBYyxFQUFFLFVBQWtCOztZQUNqRCxHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFHLG1DQUFtQyxHQUFHLE1BQU0sR0FBRyxHQUFHLEdBQUcsVUFBVTs7Y0FDNUYsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzlCLFFBQVEsRUFBRSxrQkFBa0I7U0FDN0IsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQzVELElBQUk7Ozs7UUFBQyxRQUFRLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNuQyxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7WUFyaEJGLFVBQVU7Ozs7WUFmRixVQUFVO1lBYVYsbUJBQW1COzs7Ozs7O0lBSTFCLDJDQUEwQjs7Ozs7SUFLZCxvQ0FBOEI7Ozs7O0lBQUUsNkNBQWdEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IEh0dHBIZWFkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuXG5pbXBvcnQgeyBCdXNpbmVzc1Byb2Nlc3NJbnN0YW5jZSB9IGZyb20gJy4uL2RvbWFpbi9idXNpbmVzcy1wcm9jZXNzLWluc3RhbmNlJztcbmltcG9ydCB7IFN0ZXBJbnN0YW5jZSB9IGZyb20gJy4uL2RvbWFpbi9zdGVwLWluc3RhbmNlJztcbmltcG9ydCB7IEJ1c2luZXNzUHJvY2Vzc1N0YXRlIH0gZnJvbSAnLi4vZG9tYWluL2J1c2luZXNzLXByb2Nlc3Mtc3RhdGUnO1xuaW1wb3J0IHsgUHJvamVjdCB9IGZyb20gJy4uL2RvbWFpbi9wcm9qZWN0JztcbmltcG9ydCB7IFByb2plY3RSb2xlIH0gZnJvbSAnLi4vZG9tYWluL3Byb2plY3Qtcm9sZSc7XG5pbXBvcnQgeyBUYWJsZURhdGFQYWdlIH0gZnJvbSAnLi4vZG9tYWluL3RhYmxlLWRhdGEtcGFnZSc7XG5pbXBvcnQgeyBBY3RvciB9IGZyb20gJy4uL2RvbWFpbi9hY3Rvcic7XG5pbXBvcnQgeyBRdWVzdGlvbm5haXJlIH0gZnJvbSAnLi4vZG9tYWluL3F1ZXN0aW9ubmFpcmUnO1xuaW1wb3J0IHsgUXVlc3Rpb25SZXNwb25zZSB9IGZyb20gJy4uL2RvbWFpbi9xdWVzdGlvbi1yZXNwb25zZSc7XG5pbXBvcnQgeyBUaW1lU2xvdCB9IGZyb20gJy4uL2RvbWFpbi90aW1lLXNsb3QnO1xuaW1wb3J0IHsgQWN1bWVuQ29uZmlndXJhdGlvbiB9IGZyb20gJy4uL3NlcnZpY2VzL2FjdW1lbi1jb25maWd1cmF0aW9uJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFByb2plY3RTZXJ2aWNlIHtcbiAgcHJpdmF0ZSBiYWNrZW5kU2VydmljZVVybDtcbiAgLy8gcHJpdmF0ZSBiYWNrZW5kU2VydmljZVVybCA9ICdodHRwOi8vd3d3LmhlYWx0aGFjdW1lbi5jby56YS9pbnNpZ2h0Lyc7XG4gIC8vIHByaXZhdGUgYmFja2VuZFNlcnZpY2VVcmwgPSAnaHR0cHM6Ly9mcmFuay5ybXN1aS5jby56YS9pbnNpZ2h0Lyc7XG4gIC8vIHByaXZhdGUgYmFja2VuZFNlcnZpY2VVcmwgPSAnaHR0cDovL2xvY2FsaG9zdDo4ODg4Lyc7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwQ2xpZW50OiBIdHRwQ2xpZW50LCBwcml2YXRlIGFjdW1lbkNvbmZpZ3VyYXRpb246IEFjdW1lbkNvbmZpZ3VyYXRpb24pIHtcbiAgICBpZiAoYWN1bWVuQ29uZmlndXJhdGlvbi5iYWNrZW5kU2VydmljZVVybCkge1xuICAgICAgdGhpcy5iYWNrZW5kU2VydmljZVVybCA9IGFjdW1lbkNvbmZpZ3VyYXRpb24uYmFja2VuZFNlcnZpY2VVcmw7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgPSBcImh0dHBzOi8vZnJhbmsucm1zdWkuY28uemEvaW5zaWdodC9cIjtcbiAgICB9XG4gIH1cblxuICBzdGFydFdpemFyZFByb2plY3QocHJvamVjdEd1aWQ6IHN0cmluZywgYWN0b3JJZDogbnVtYmVyKTogUHJvbWlzZTxCdXNpbmVzc1Byb2Nlc3NJbnN0YW5jZT4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsO1xuICAgIGlmIChhY3RvcklkKSB7XG4gICAgICAgIHVybCArPSBgcmVzdC9wcm9qZWN0L3Byb2plY3RzL3N0YXJ0Mi8ke3Byb2plY3RHdWlkfS8ke2FjdG9ySWR9YDtcbiAgICB9IGVsc2Uge1xuICAgICAgICB1cmwgKz0gYHJlc3QvcHJvamVjdC9wcm9qZWN0cy9zdGFydC93aXphcmQvJHtwcm9qZWN0R3VpZH1gO1xuICAgIH1cblxuICAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICB9KTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PEJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlPih1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBCdXNpbmVzc1Byb2Nlc3NJbnN0YW5jZSk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIGdldEJ1c2luZXNzUHJvY2Vzc1N0YXRlKGJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlSWQ6IG51bWJlcik6IFByb21pc2U8QnVzaW5lc3NQcm9jZXNzU3RhdGU+IHtcbiAgICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybCArIFwicmVzdC9wcm9qZWN0L3Byb2plY3RzL3N0YXRlL1wiICsgYnVzaW5lc3NQcm9jZXNzSW5zdGFuY2VJZCArIFwiP3RpbWVzdGFtcD1cIiArIG5ldyBEYXRlKCkuZ2V0VGltZSgpO1xuICAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICB9KTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PEJ1c2luZXNzUHJvY2Vzc1N0YXRlPih1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBCdXNpbmVzc1Byb2Nlc3NTdGF0ZSk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIGdldFByb2plY3QocHJvamVjdEd1aWQ6IHN0cmluZyk6IFByb21pc2U8UHJvamVjdD4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgYHJlc3QvcHJvamVjdC9wcm9qZWN0LyR7cHJvamVjdEd1aWR9YDtcbiAgICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldDxQcm9qZWN0Pih1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBQcm9qZWN0KTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgZ2V0UHJvamVjdHMoYWN0b3JJZDogbnVtYmVyKTogUHJvbWlzZTxQcm9qZWN0W10+IHtcbiAgICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybCArIGByZXN0L3Byb2plY3QvYWN0b3JQcm9qZWN0cy8ke2FjdG9ySWR9YDtcbiAgICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldDxQcm9qZWN0W10+KHVybCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIFByb2plY3RbXSk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIGdldFVzZXJQcm9qZWN0cyhzeXN0ZW1Vc2VySWQ6IG51bWJlcik6IFByb21pc2U8UHJvamVjdFtdPiB7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBgcmVzdC9wcm9qZWN0L3Byb2plY3RzLyR7c3lzdGVtVXNlcklkfWA7XG4gICAgY29uc3QgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8UHJvamVjdFtdPih1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBQcm9qZWN0W10pO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxuICBnZXRQcm9qZWN0Um9sZXMocHJvamVjdElkOiBudW1iZXIpOiBQcm9taXNlPFByb2plY3RSb2xlW10+IHtcbiAgICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybCArIGByZXN0L3Byb2plY3QvcHJvamVjdFJvbGVzLyR7cHJvamVjdElkfWA7XG4gICAgY29uc3QgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8UHJvamVjdFJvbGVbXT4odXJsLCB7aGVhZGVyczogaGVhZGVyc30pLnRvUHJvbWlzZSgpXG4gICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgUHJvamVjdFJvbGVbXSk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIGdldFByb2plY3RBY3RvcnMoZnJvbVJlY29yZDogbnVtYmVyLCByZWNvcmRDb3VudDogbnVtYmVyLCBwcm9qZWN0SWQ6IG51bWJlciwgcHJvamVjdFJvbGVJZDogbnVtYmVyLFxuICAgICAgICAgIGZpcnN0bmFtZTogc3RyaW5nLCBzdXJuYW1lOiBzdHJpbmcsIGlkTnI6IHN0cmluZywgbWVtYmVyTnI6IHN0cmluZywgZGVwZW5kYW50Q29kZTogc3RyaW5nLCBhc3NpZ25lZUlkOiBudW1iZXIpOiBQcm9taXNlPFRhYmxlRGF0YVBhZ2U+IHtcbiAgICBpZiAoIWZpcnN0bmFtZSlcbiAgICAgICAgZmlyc3RuYW1lID0gXCJudWxsXCI7XG5cbiAgICBpZiAoIXN1cm5hbWUpXG4gICAgICAgIHN1cm5hbWUgPSBcIm51bGxcIjtcblxuICAgIGlmICghaWROcilcbiAgICAgICAgaWROciA9IFwibnVsbFwiO1xuXG4gICAgaWYgKCFtZW1iZXJOcilcbiAgICAgICAgbWVtYmVyTnIgPSBcIm51bGxcIjtcblxuICAgIGlmICghZGVwZW5kYW50Q29kZSlcbiAgICAgICAgZGVwZW5kYW50Q29kZSA9IFwibnVsbFwiO1xuXG4gICAgICAvLyBhc3NpZ25lZUlkID0gLTE7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBgcmVzdC9wcm9qZWN0L3Byb2plY3RBY3RvcnMvJHtmcm9tUmVjb3JkfS8ke3JlY29yZENvdW50fS8ke3Byb2plY3RJZH0vJHtwcm9qZWN0Um9sZUlkfS8ke2ZpcnN0bmFtZX0vJHtzdXJuYW1lfS8ke2lkTnJ9LyR7bWVtYmVyTnJ9LyR7ZGVwZW5kYW50Q29kZX0vJHthc3NpZ25lZUlkfWA7XG4gICAgY29uc3QgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8VGFibGVEYXRhUGFnZT4odXJsLCB7aGVhZGVyczogaGVhZGVyc30pLnRvUHJvbWlzZSgpXG4gICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgVGFibGVEYXRhUGFnZSk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIGdldFByb2plY3RBY3RvcnMyKGZyb21SZWNvcmQ6IG51bWJlciwgcmVjb3JkQ291bnQ6IG51bWJlciwgcHJvamVjdElkOiBudW1iZXIsIHByb2plY3RSb2xlSWQ6IG51bWJlcik6IFByb21pc2U8VGFibGVEYXRhUGFnZT4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgYHJlc3QvcHJvamVjdC9wcm9qZWN0QWN0b3JzMi8ke2Zyb21SZWNvcmR9LyR7cmVjb3JkQ291bnR9LyR7cHJvamVjdElkfS8ke3Byb2plY3RSb2xlSWR9YDtcbiAgICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldDxUYWJsZURhdGFQYWdlPih1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBUYWJsZURhdGFQYWdlKTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgZ2V0UHJvamVjdEFjdG9yczMoZnJvbVJlY29yZDogbnVtYmVyLCByZWNvcmRDb3VudDogbnVtYmVyLCBwcm9qZWN0Um9sZUlkOiBudW1iZXIsIHNlYXJjaEZpZWxkOiBzdHJpbmcpOiBQcm9taXNlPFRhYmxlRGF0YVBhZ2U+IHtcbiAgICBpZiAoIXNlYXJjaEZpZWxkKVxuICAgICAgICBzZWFyY2hGaWVsZCA9IFwibnVsbFwiO1xuXG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBgcmVzdC9wcm9qZWN0L3Byb2plY3RBY3RvcnMzLyR7ZnJvbVJlY29yZH0vJHtyZWNvcmRDb3VudH0vJHtwcm9qZWN0Um9sZUlkfS8ke3NlYXJjaEZpZWxkfWA7XG4gICAgY29uc3QgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8VGFibGVEYXRhUGFnZT4odXJsLCB7aGVhZGVyczogaGVhZGVyc30pLnRvUHJvbWlzZSgpXG4gICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgVGFibGVEYXRhUGFnZSk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIGdldFN0ZXBJbnN0YW5jZXMocGFydGljaXBhbnRJZDogbnVtYmVyKTogUHJvbWlzZTxBcnJheTxTdGVwSW5zdGFuY2U+PiB7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBgcmVzdC9wcm9qZWN0L3N0ZXBJbnN0YW5jZXMvJHtwYXJ0aWNpcGFudElkfWA7XG4gICAgY29uc3QgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8U3RlcEluc3RhbmNlW10+KHVybCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIFN0ZXBJbnN0YW5jZVtdKTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgZ2V0U3RlcEluc3RhbmNlczIocHJvamVjdEd1aWQ6IHN0cmluZywgdXNlcklkOiBudW1iZXIsIGFzc2lnbmVlSWQ6IG51bWJlciwgcGFydGljaXBhbnRJZDogbnVtYmVyKTogUHJvbWlzZTxBcnJheTxTdGVwSW5zdGFuY2U+PiB7XG4gICAgaWYgKCFwcm9qZWN0R3VpZCkge1xuICAgICAgcHJvamVjdEd1aWQgPSBcIm51bGxcIjtcbiAgICB9XG5cbiAgICBpZiAoIXVzZXJJZCkge1xuICAgICAgdXNlcklkID0gLTE7XG4gICAgfVxuXG4gICAgaWYgKCFhc3NpZ25lZUlkKSB7XG4gICAgICBhc3NpZ25lZUlkID0gLTE7XG4gICAgfVxuXG4gICAgaWYgKCFwYXJ0aWNpcGFudElkKSB7XG4gICAgICBwYXJ0aWNpcGFudElkID0gLTE7XG4gICAgfVxuXG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBgcmVzdC9wcm9qZWN0L3N0ZXBJbnN0YW5jZXMvJHtwcm9qZWN0R3VpZH0vJHt1c2VySWR9LyR7YXNzaWduZWVJZH0vJHtwYXJ0aWNpcGFudElkfWA7XG4gICAgY29uc3QgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8U3RlcEluc3RhbmNlW10+KHVybCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIFN0ZXBJbnN0YW5jZVtdKTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgZ2V0U3RlcFBhcmFtZXRlcnMoc3RlcElkOiBudW1iZXIpOiBQcm9taXNlPGFueT4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgXCJyZXN0L3Byb2plY3Qvc3RlcEluc3RhbmNlcy9wYXJhbWV0ZXJzL1wiICsgc3RlcElkO1xuICAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICB9KTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PGFueT4odXJsLCB7aGVhZGVyczogaGVhZGVyc30pLnRvUHJvbWlzZSgpXG4gICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UpO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxuICBnZXRMYXRlc3RTdGVwQXNzaWdubWVudERhdGUoYWN0b3JJZDogbnVtYmVyKTogUHJvbWlzZTxEYXRlPiB7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBcInJlc3QvcHJvamVjdC9zdGVwSW5zdGFuY2VzL2xhdGVzdEFzc2lnbm1lbnQvXCIgKyBhY3RvcklkO1xuICAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICB9KTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PERhdGU+KHVybCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIERhdGUpO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxuICBjb21wbGV0ZVN0ZXAoc3RlcEluc3RhbmNlSWQ6IG51bWJlciwgc3lzdGVtVXNlcklkOiBudW1iZXIpOiBQcm9taXNlPGFueT4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsO1xuICAgIGlmIChzeXN0ZW1Vc2VySWQpXG4gICAgICAgIHVybCArPSBgcmVzdC9wcm9qZWN0L3N0ZXBJbnN0YW5jZS9jb21wbGV0ZVN0ZXA/c3RlcEluc3RhbmNlSWQ9JHtzdGVwSW5zdGFuY2VJZH0mc3lzdGVtVXNlcklkPSR7c3lzdGVtVXNlcklkfWA7XG4gICAgZWxzZVxuICAgICAgICB1cmwgKz0gYHJlc3QvcHJvamVjdC9zdGVwSW5zdGFuY2UvY29tcGxldGVTdGVwMj9zdGVwSW5zdGFuY2VJZD0ke3N0ZXBJbnN0YW5jZUlkfWA7XG5cbiAgICBsZXQgaGVhZGVycyA9IG5ldyBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LnBvc3QodXJsLCBKU09OLnN0cmluZ2lmeSh7fSkpLnRvUHJvbWlzZSgpXG4gICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgc3RyaW5nKTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgdXBkYXRlU3RlcExvY2F0aW9uKHN0ZXBJbnN0YW5jZUlkOiBudW1iZXIsIGxhdGl0dWRlOiBudW1iZXIsIGxvbmdpdHVkZTogbnVtYmVyKTogUHJvbWlzZTxhbnk+IHtcbiAgICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybDtcbiAgICB1cmwgKz0gYHJlc3QvcHJvamVjdC91cGRhdGVTdGVwTG9jYXRpb24/c3RlcEluc3RhbmNlSWQ9JHtzdGVwSW5zdGFuY2VJZH0mbGF0aXR1ZGU9JHtsYXRpdHVkZX0mbG9uZ2l0dWRlPSR7bG9uZ2l0dWRlfWA7XG5cbiAgICBsZXQgaGVhZGVycyA9IG5ldyBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LnBvc3QodXJsLCBKU09OLnN0cmluZ2lmeSh7fSkpLnRvUHJvbWlzZSgpXG4gICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgc3RyaW5nKTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgZ2V0UXVlc3Rpb25uYWlyZShzdGVwSW5zdGFuY2VJZDogbnVtYmVyKTogUHJvbWlzZTxRdWVzdGlvbm5haXJlPiB7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBgcmVzdC9wcm9qZWN0L3N0ZXBJbnN0YW5jZS9xdWVzdGlvbm5haXJlLyR7c3RlcEluc3RhbmNlSWR9YDtcbiAgICBsZXQgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldCh1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBRdWVzdGlvbm5haXJlKTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgdXBkYXRlUXVlc3Rpb25uYWlyZShzdGVwSW5zdGFuY2VJZDogbnVtYmVyLCByZXNwb25zZXM6IFF1ZXN0aW9uUmVzcG9uc2VbXSwgc3VibWl0OiBib29sZWFuLCBzeXN0ZW1Vc2VySWQ6IG51bWJlcikge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgJ3Jlc3QvcHJvamVjdC9zdGVwSW5zdGFuY2UvcXVlc3Rpb25uYWlyZS9yZXNwb25zZXMnO1xuICAgIGxldCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nXG4gICAgfSk7XG5cbiAgICBsZXQgcXVlc3Rpb25uYWlyZVJlc3BvbnNlID0ge1xuICAgICAgc3RlcEluc3RhbmNlSWQ6IHN0ZXBJbnN0YW5jZUlkLFxuICAgICAgcmVzcG9uc2VzOiByZXNwb25zZXMsXG4gICAgICBzdWJtaXQ6IHN1Ym1pdCxcbiAgICAgIHN5c3RlbVVzZXJJZDogc3lzdGVtVXNlcklkXG4gICAgfTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQucG9zdCh1cmwsIEpTT04uc3RyaW5naWZ5KHF1ZXN0aW9ubmFpcmVSZXNwb25zZSksIHsgaGVhZGVyczogaGVhZGVycyB9KS50b1Byb21pc2UoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIHN0cmluZyk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIGdldE1lc3NhZ2VTdGVwSHRtbChzdGVwSW5zdGFuY2VJZDogbnVtYmVyKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybCArIGByZXN0L3Byb2plY3Qvc3RlcEluc3RhbmNlL21lc3NhZ2UvJHtzdGVwSW5zdGFuY2VJZH1gO1xuICAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICB9KTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0KHVybCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIHN0cmluZyk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIHZlcmlmeUFjdG9ySWROcihidXNpbmVzc1Byb2Nlc3NJbnN0YW5jZUlkOiBudW1iZXIsIGlkTnI6IHN0cmluZyk6IFByb21pc2U8YW55PiB7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyAncmVzdC9wcm9qZWN0L3ZlcmlmeUFjdG9ySWROcic7XG4gICAgbGV0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbidcbiAgICB9KTtcblxuICAgIGxldCBpZE5yTG9va1VwID0ge1xuICAgICAgYnVzaW5lc3NQcm9jZXNzSW5zdGFuY2VJZDogYnVzaW5lc3NQcm9jZXNzSW5zdGFuY2VJZCxcbiAgICAgIGlkTnI6IGlkTnJcbiAgICB9O1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5wb3N0KHVybCwgaWROckxvb2tVcCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgc3RyaW5nKTtcbiAgICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgIH0pO1xuICB9XG5cbiAgZ2V0UHJvY2Vzc1ZhcmlhYmxlVmFsdWUoYnVzaW5lc3NQcm9jZXNzSW5zdGFuY2VJZDogbnVtYmVyLCBrZXk6IHN0cmluZyk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBgcmVzdC9wcm9qZWN0L2J1c2luZXNzUHJvY2Vzc0luc3RhbmNlLyR7YnVzaW5lc3NQcm9jZXNzSW5zdGFuY2VJZH0vJHtrZXl9YDtcbiAgICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldCh1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBzdHJpbmcpO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxuICBzZXRQcm9jZXNzVmFyaWFibGVWYWx1ZShidXNpbmVzc1Byb2Nlc3NJbnN0YW5jZUlkOiBudW1iZXIsIGtleTogc3RyaW5nLCB2YWx1ZTogc3RyaW5nKTogUHJvbWlzZTxzdHJpbmc+IHtcbiAgICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybCArIGByZXN0L3Byb2plY3Qvc2V0UHJvY2Vzc1ZhcmlhYmxlVmFsdWUvJHtidXNpbmVzc1Byb2Nlc3NJbnN0YW5jZUlkfS8ke2tleX0vJHt2YWx1ZX1gO1xuICAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICB9KTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0KHVybCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIHN0cmluZyk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIHVwZGF0ZUFjdG9yKGFjdG9yOiBBY3Rvcik6IFByb21pc2U8YW55PiB7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyAncmVzdC9wcm9qZWN0L3VwZGF0ZUFjdG9yJztcbiAgICBsZXQgaGVhZGVycyA9IG5ldyBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LnBvc3QodXJsLCBhY3RvcikudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBzdHJpbmcpO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxuICBnZXRUaW1lU2xvdHMocGFydGljaXBhbnRJZDogbnVtYmVyLCBwcm92aWRlcklkOiBudW1iZXIsIHRpbWVTbG90VHlwZTogc3RyaW5nKTogUHJvbWlzZTxUaW1lU2xvdFtdPiB7XG4gICAgaWYgKCFwYXJ0aWNpcGFudElkKVxuICAgICAgICBwYXJ0aWNpcGFudElkID0gLTE7XG5cbiAgICBpZiAoIXByb3ZpZGVySWQpXG4gICAgICAgIHByb3ZpZGVySWQgPSAtMTtcblxuICAgIGlmICghdGltZVNsb3RUeXBlKVxuICAgICAgICB0aW1lU2xvdFR5cGUgPSBudWxsO1xuXG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBgcmVzdC9wcm9qZWN0L3RpbWVTbG90cy8ke3BhcnRpY2lwYW50SWR9LyR7cHJvdmlkZXJJZH0vJHt0aW1lU2xvdFR5cGV9YDtcbiAgICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldCh1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBUaW1lU2xvdFtdKTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgdXBkYXRlVGltZVNsb3RzKHRpbWVTbG90czogVGltZVNsb3RbXSk6IFByb21pc2U8YW55PiB7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyAncmVzdC9wcm9qZWN0L3VwZGF0ZVRpbWVTbG90cyc7XG4gICAgbGV0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nXG4gICAgfSk7XG5cbiAgICBsZXQgcGFyYW1zID0ge1xuICAgICAgICB0aW1lU2xvdHM6IHRpbWVTbG90c1xuICAgIH07XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LnBvc3QodXJsLCBwYXJhbXMsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIFRpbWVTbG90W10pO1xuICAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICAgfSk7XG4gIH1cblxuICBjcmVhdGVTeXN0ZW1Vc2VyKHVzZXJuYW1lOiBzdHJpbmcsIHBhc3N3b3JkOiBzdHJpbmcsIGFjdG9ySWQ6IG51bWJlcik6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBgcmVzdC9sb2dpbi9jcmVhdGVVc2VyLyR7dXNlcm5hbWV9LyR7cGFzc3dvcmR9LyR7YWN0b3JJZH1gO1xuICAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICB9KTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0KHVybCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIHN0cmluZyk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIHVwZGF0ZUZhY2lhbEF1dGhlbnRpY2F0aW9uUmVmZXJlbmNlKHN0ZXBJbnN0YW5jZUlkOiBudW1iZXIsIGltYWdlRGF0YTogc3RyaW5nKTogUHJvbWlzZTxhbnk+IHtcbiAgICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybCArICdyZXN0L3Byb2plY3QvZmFjaWFsQXV0aC9yZWZlcmVuY2UnO1xuICAgIGxldCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJ1xuICAgIH0pO1xuXG4gICAgbGV0IHBhcmFtcyA9IHtcbiAgICAgICAgc3RlcEluc3RhbmNlSWQ6IHN0ZXBJbnN0YW5jZUlkLFxuICAgICAgICBpbWFnZURhdGE6IGltYWdlRGF0YVxuICAgIH07XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LnBvc3QodXJsLCBwYXJhbXMsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIFRpbWVTbG90W10pO1xuICAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICAgfSk7XG4gIH1cblxuICBmYWNpYWxBdXRoZW50aWNhdGlvbihzdGVwSW5zdGFuY2VJZDogbnVtYmVyLCBpbWFnZURhdGE6IHN0cmluZyk6IFByb21pc2U8YW55PiB7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyAncmVzdC9wcm9qZWN0L2ZhY2lhbEF1dGgvYXV0aGVudGljYXRlJztcbiAgICBsZXQgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbidcbiAgICB9KTtcblxuICAgIGxldCBwYXJhbXMgPSB7XG4gICAgICAgIHN0ZXBJbnN0YW5jZUlkOiBzdGVwSW5zdGFuY2VJZCxcbiAgICAgICAgaW1hZ2VEYXRhOiBpbWFnZURhdGFcbiAgICB9O1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5wb3N0KHVybCwgcGFyYW1zLCB7aGVhZGVyczogaGVhZGVyc30pLnRvUHJvbWlzZSgpXG4gICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBUaW1lU2xvdFtdKTtcbiAgICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgIH0pO1xuICB9XG5cbiAgZ2V0UHJvY2Vzc0hpc3RvcnkocHJvamVjdEd1aWQ6IHN0cmluZywgYWN0b3JJZDogbnVtYmVyKTogUHJvbWlzZTxBcnJheTxTdGVwSW5zdGFuY2U+PiB7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBgcmVzdC9wcm9qZWN0L3N0ZXBJbnN0YW5jZS9wcm9ncmVzcy8ke3Byb2plY3RHdWlkfS8ke2FjdG9ySWR9YDtcbiAgICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldCh1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBBcnJheTxTdGVwSW5zdGFuY2U+KTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgZ2V0QWN0aXZlU3RlcEluc3RhbmNlcyhzdGVwSWRzOiBBcnJheTxudW1iZXI+KTogUHJvbWlzZTxBcnJheTxTdGVwSW5zdGFuY2U+PiB7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyAncmVzdC9wcm9qZWN0L3N0ZXBJbnN0YW5jZS9hY3RpdmUnO1xuICAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICB9KTtcblxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgc3RlcElkcy5sZW5ndGg7IGkrKykge1xuICAgICAgaWYgKGkgPT09IDApIHtcbiAgICAgICAgdXJsICs9IFwiP3N0ZXBJZHM9XCIgKyBzdGVwSWRzW2ldO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdXJsICs9IFwiJnN0ZXBJZHM9XCIgKyBzdGVwSWRzW2ldO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0KHVybCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIEFycmF5PFN0ZXBJbnN0YW5jZT4pO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxuICBpc1N0ZXBJbnN0YW5jZUFjdGl2ZShzdGVwSWQ6IG51bWJlciwgZXh0ZXJuYWxJZDogc3RyaW5nKTogUHJvbWlzZTxhbnk+IHtcbiAgICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybCArICdyZXN0L3Byb2plY3Qvc3RlcEluc3RhbmNlL2FjdGl2ZS8nICsgc3RlcElkICsgXCIvXCIgKyBleHRlcm5hbElkO1xuICAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICB9KTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0KHVybCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlKTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG59XG4iXX0=