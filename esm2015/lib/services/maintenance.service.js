/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { AcumenConfiguration } from '../services/acumen-configuration';
export class MaintenanceService {
    // private backendServiceUrl = 'http://www.healthacumen.co.za/insight/';
    // private backendServiceUrl = 'https://frank.rmsui.co.za/insight/';
    // private backendServiceUrl = 'http://localhost:8888/';
    /**
     * @param {?} httpClient
     * @param {?} acumenConfiguration
     */
    constructor(httpClient, acumenConfiguration) {
        this.httpClient = httpClient;
        this.acumenConfiguration = acumenConfiguration;
        if (acumenConfiguration.backendServiceUrl) {
            this.backendServiceUrl = acumenConfiguration.backendServiceUrl;
        }
        else {
            this.backendServiceUrl = "https://frank.rmsui.co.za/insight/";
        }
    }
    // getTitles(): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/titles`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    //
    // getEmployers(): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/employers`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    //
    // getWorksites(): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/worksites`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       let workSites = response;
    //       for (let i = 0; i < workSites.length; i++) {
    //           workSites[i].defaultOption = workSites[i]["defaultWorkSite"];
    //       }
    //
    //       return workSites;
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    //   }
    //
    // getBuildings(employerId: number): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/buildings/${employerId}`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    //
    // getMedicalAids(): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/medicalaids`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    //
    // getSubSites(workSiteId: number): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/subSites/${workSiteId}`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    /**
     * @param {?} actorId
     * @return {?}
     */
    getActorFields(actorId) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/maintenance/actorType/actorFields/${actorId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} actorFieldId
     * @return {?}
     */
    getActorFieldOptions(actorFieldId) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/maintenance/actorType/actorFields/actorFieldOptions/${actorFieldId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
}
MaintenanceService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
MaintenanceService.ctorParameters = () => [
    { type: HttpClient },
    { type: AcumenConfiguration }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    MaintenanceService.prototype.backendServiceUrl;
    /**
     * @type {?}
     * @private
     */
    MaintenanceService.prototype.httpClient;
    /**
     * @type {?}
     * @private
     */
    MaintenanceService.prototype.acumenConfiguration;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbnRlbmFuY2Uuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvbWFpbnRlbmFuY2Uuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFTLGVBQWUsQ0FBQztBQUM5QyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbEQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBS25ELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBR3ZFLE1BQU0sT0FBTyxrQkFBa0I7Ozs7Ozs7O0lBTTdCLFlBQW9CLFVBQXNCLEVBQVUsbUJBQXdDO1FBQXhFLGVBQVUsR0FBVixVQUFVLENBQVk7UUFBVSx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQzFGLElBQUksbUJBQW1CLENBQUMsaUJBQWlCLEVBQUU7WUFDekMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDO1NBQ2hFO2FBQU07WUFDTCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsb0NBQW9DLENBQUM7U0FDL0Q7SUFDSCxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUEyRkQsY0FBYyxDQUFDLE9BQWU7O1lBQ3hCLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsMENBQTBDLE9BQU8sRUFBRTs7Y0FDaEYsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzlCLFFBQVEsRUFBRSxrQkFBa0I7U0FDN0IsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQWUsR0FBRyxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQzFFLElBQUk7Ozs7UUFBQyxRQUFRLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQWdCLENBQUMsQ0FBQztRQUNuRCxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVELG9CQUFvQixDQUFDLFlBQW9COztZQUNuQyxHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFHLDREQUE0RCxZQUFZLEVBQUU7O2NBQ3ZHLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQztZQUM5QixRQUFRLEVBQUUsa0JBQWtCO1NBQzdCLENBQUM7UUFFRixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFxQixHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDaEYsSUFBSTs7OztRQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLG1CQUFBLFFBQVEsRUFBc0IsQ0FBQyxDQUFDO1FBQ3pELENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxLQUFLLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7OztZQWxJRixVQUFVOzs7O1lBUkYsVUFBVTtZQU1WLG1CQUFtQjs7Ozs7OztJQUkxQiwrQ0FBMEI7Ozs7O0lBS2Qsd0NBQThCOzs7OztJQUFFLGlEQUFnRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSAgICBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBIdHRwSGVhZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcblxuLy8gaW1wb3J0IHsgU2VsZWN0aW9uT3B0aW9uIH0gZnJvbSAnLi4vZG9tYWluL3NlbGVjdGlvbi1vcHRpb24nO1xuaW1wb3J0IHsgQWN0b3JGaWVsZCB9IGZyb20gJy4uL2RvbWFpbi9hY3Rvci1maWVsZCc7XG5pbXBvcnQgeyBBY3RvckZpZWxkT3B0aW9uIH0gZnJvbSAnLi4vZG9tYWluL2FjdG9yLWZpZWxkLW9wdGlvbic7XG5pbXBvcnQgeyBBY3VtZW5Db25maWd1cmF0aW9uIH0gZnJvbSAnLi4vc2VydmljZXMvYWN1bWVuLWNvbmZpZ3VyYXRpb24nO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgTWFpbnRlbmFuY2VTZXJ2aWNlIHtcbiAgcHJpdmF0ZSBiYWNrZW5kU2VydmljZVVybDtcbiAgLy8gcHJpdmF0ZSBiYWNrZW5kU2VydmljZVVybCA9ICdodHRwOi8vd3d3LmhlYWx0aGFjdW1lbi5jby56YS9pbnNpZ2h0Lyc7XG4gIC8vIHByaXZhdGUgYmFja2VuZFNlcnZpY2VVcmwgPSAnaHR0cHM6Ly9mcmFuay5ybXN1aS5jby56YS9pbnNpZ2h0Lyc7XG4gIC8vIHByaXZhdGUgYmFja2VuZFNlcnZpY2VVcmwgPSAnaHR0cDovL2xvY2FsaG9zdDo4ODg4Lyc7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwQ2xpZW50OiBIdHRwQ2xpZW50LCBwcml2YXRlIGFjdW1lbkNvbmZpZ3VyYXRpb246IEFjdW1lbkNvbmZpZ3VyYXRpb24pIHtcbiAgICBpZiAoYWN1bWVuQ29uZmlndXJhdGlvbi5iYWNrZW5kU2VydmljZVVybCkge1xuICAgICAgdGhpcy5iYWNrZW5kU2VydmljZVVybCA9IGFjdW1lbkNvbmZpZ3VyYXRpb24uYmFja2VuZFNlcnZpY2VVcmw7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgPSBcImh0dHBzOi8vZnJhbmsucm1zdWkuY28uemEvaW5zaWdodC9cIjtcbiAgICB9XG4gIH1cblxuICAvLyBnZXRUaXRsZXMoKTogUHJvbWlzZTxTZWxlY3Rpb25PcHRpb25bXT4ge1xuICAvLyAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgYHJlc3QvbWFpbnRlbmFuY2UvdGl0bGVzYDtcbiAgLy8gICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgLy8gICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gIC8vICAgfSk7XG4gIC8vXG4gIC8vICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8U2VsZWN0aW9uT3B0aW9uW10+KHVybCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAvLyAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAvLyAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIFNlbGVjdGlvbk9wdGlvbltdKTtcbiAgLy8gICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgLy8gICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgLy8gICAgIH0pO1xuICAvLyB9XG4gIC8vXG4gIC8vIGdldEVtcGxveWVycygpOiBQcm9taXNlPFNlbGVjdGlvbk9wdGlvbltdPiB7XG4gIC8vICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBgcmVzdC9tYWludGVuYW5jZS9lbXBsb3llcnNgO1xuICAvLyAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAvLyAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgLy8gICB9KTtcbiAgLy9cbiAgLy8gICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldDxTZWxlY3Rpb25PcHRpb25bXT4odXJsLCB7aGVhZGVyczogaGVhZGVyc30pLnRvUHJvbWlzZSgpXG4gIC8vICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gIC8vICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgU2VsZWN0aW9uT3B0aW9uW10pO1xuICAvLyAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAvLyAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAvLyAgICAgfSk7XG4gIC8vIH1cbiAgLy9cbiAgLy8gZ2V0V29ya3NpdGVzKCk6IFByb21pc2U8U2VsZWN0aW9uT3B0aW9uW10+IHtcbiAgLy8gICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybCArIGByZXN0L21haW50ZW5hbmNlL3dvcmtzaXRlc2A7XG4gIC8vICAgY29uc3QgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gIC8vICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAvLyAgIH0pO1xuICAvL1xuICAvLyAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PFNlbGVjdGlvbk9wdGlvbltdPih1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgLy8gICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgLy8gICAgICAgbGV0IHdvcmtTaXRlcyA9IHJlc3BvbnNlO1xuICAvLyAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHdvcmtTaXRlcy5sZW5ndGg7IGkrKykge1xuICAvLyAgICAgICAgICAgd29ya1NpdGVzW2ldLmRlZmF1bHRPcHRpb24gPSB3b3JrU2l0ZXNbaV1bXCJkZWZhdWx0V29ya1NpdGVcIl07XG4gIC8vICAgICAgIH1cbiAgLy9cbiAgLy8gICAgICAgcmV0dXJuIHdvcmtTaXRlcztcbiAgLy8gICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgLy8gICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgLy8gICAgIH0pO1xuICAvLyAgIH1cbiAgLy9cbiAgLy8gZ2V0QnVpbGRpbmdzKGVtcGxveWVySWQ6IG51bWJlcik6IFByb21pc2U8U2VsZWN0aW9uT3B0aW9uW10+IHtcbiAgLy8gICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybCArIGByZXN0L21haW50ZW5hbmNlL2J1aWxkaW5ncy8ke2VtcGxveWVySWR9YDtcbiAgLy8gICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgLy8gICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gIC8vICAgfSk7XG4gIC8vXG4gIC8vICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8U2VsZWN0aW9uT3B0aW9uW10+KHVybCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAvLyAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAvLyAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIFNlbGVjdGlvbk9wdGlvbltdKTtcbiAgLy8gICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgLy8gICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgLy8gICAgIH0pO1xuICAvLyB9XG4gIC8vXG4gIC8vIGdldE1lZGljYWxBaWRzKCk6IFByb21pc2U8U2VsZWN0aW9uT3B0aW9uW10+IHtcbiAgLy8gICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybCArIGByZXN0L21haW50ZW5hbmNlL21lZGljYWxhaWRzYDtcbiAgLy8gICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgLy8gICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gIC8vICAgfSk7XG4gIC8vXG4gIC8vICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8U2VsZWN0aW9uT3B0aW9uW10+KHVybCwge2hlYWRlcnM6IGhlYWRlcnN9KS50b1Byb21pc2UoKVxuICAvLyAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAvLyAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIFNlbGVjdGlvbk9wdGlvbltdKTtcbiAgLy8gICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgLy8gICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgLy8gICAgIH0pO1xuICAvLyB9XG4gIC8vXG4gIC8vIGdldFN1YlNpdGVzKHdvcmtTaXRlSWQ6IG51bWJlcik6IFByb21pc2U8U2VsZWN0aW9uT3B0aW9uW10+IHtcbiAgLy8gICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybCArIGByZXN0L21haW50ZW5hbmNlL3N1YlNpdGVzLyR7d29ya1NpdGVJZH1gO1xuICAvLyAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAvLyAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgLy8gICB9KTtcbiAgLy9cbiAgLy8gICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldDxTZWxlY3Rpb25PcHRpb25bXT4odXJsLCB7aGVhZGVyczogaGVhZGVyc30pLnRvUHJvbWlzZSgpXG4gIC8vICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gIC8vICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgU2VsZWN0aW9uT3B0aW9uW10pO1xuICAvLyAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAvLyAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAvLyAgICAgfSk7XG4gIC8vIH1cblxuICBnZXRBY3RvckZpZWxkcyhhY3RvcklkOiBudW1iZXIpOiBQcm9taXNlPEFjdG9yRmllbGRbXT4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgYHJlc3QvbWFpbnRlbmFuY2UvYWN0b3JUeXBlL2FjdG9yRmllbGRzLyR7YWN0b3JJZH1gO1xuICAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICB9KTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PEFjdG9yRmllbGRbXT4odXJsLCB7aGVhZGVyczogaGVhZGVyc30pLnRvUHJvbWlzZSgpXG4gICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgQWN0b3JGaWVsZFtdKTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgZ2V0QWN0b3JGaWVsZE9wdGlvbnMoYWN0b3JGaWVsZElkOiBudW1iZXIpOiBQcm9taXNlPEFjdG9yRmllbGRPcHRpb25bXT4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgYHJlc3QvbWFpbnRlbmFuY2UvYWN0b3JUeXBlL2FjdG9yRmllbGRzL2FjdG9yRmllbGRPcHRpb25zLyR7YWN0b3JGaWVsZElkfWA7XG4gICAgY29uc3QgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8QWN0b3JGaWVsZE9wdGlvbltdPih1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBBY3RvckZpZWxkT3B0aW9uW10pO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cbn1cbiJdfQ==