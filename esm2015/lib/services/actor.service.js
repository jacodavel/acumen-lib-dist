/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { AcumenConfiguration } from '../services/acumen-configuration';
export class ActorService {
    // private backendServiceUrl = 'http://www.healthacumen.co.za/insight/';
    // private backendServiceUrl = 'https://frank.rmsui.co.za/insight/';
    // private backendServiceUrl = 'http://localhost:8888/';
    /**
     * @param {?} httpClient
     * @param {?} acumenConfiguration
     */
    constructor(httpClient, acumenConfiguration) {
        this.httpClient = httpClient;
        this.acumenConfiguration = acumenConfiguration;
        if (acumenConfiguration.backendServiceUrl) {
            this.backendServiceUrl = acumenConfiguration.backendServiceUrl;
        }
        else {
            this.backendServiceUrl = "https://frank.rmsui.co.za/insight/";
        }
    }
    /**
     * @param {?} username
     * @return {?}
     */
    getSystemUser(username) {
        /** @type {?} */
        let url = this.backendServiceUrl + "rest/actor/systemUser/" + username;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} externalActorId
     * @return {?}
     */
    getActorFromExternalId(externalActorId) {
        /** @type {?} */
        let url = this.backendServiceUrl + "rest/actor/externalId/" + externalActorId;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} externalActorId
     * @param {?} projectId
     * @return {?}
     */
    getProjectActorFromExternalId(externalActorId, projectId) {
        /** @type {?} */
        let url = this.backendServiceUrl + "rest/actor/projectParticipant/" + externalActorId + "/" + projectId;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} actorId
     * @param {?} projectId
     * @return {?}
     */
    getProjectActor(actorId, projectId) {
        /** @type {?} */
        let url = this.backendServiceUrl + "rest/project/projectActor/" + projectId + "/" + actorId;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} actorId
     * @return {?}
     */
    getActorFieldValues(actorId) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/actor/actorType/actorFields/actorFieldValues/${actorId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} actorId
     * @param {?} actorFieldValues
     * @return {?}
     */
    updateActorFieldValues(actorId, actorFieldValues) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/actor/actorType/actorFields/actorFieldValues/update`;
        /** @type {?} */
        let headers = new HttpHeaders({
            'Accept': 'application/json'
        });
        /** @type {?} */
        let parameters = {
            actorId: actorId,
            actorFieldValues: actorFieldValues
        };
        return this.httpClient.post(url, parameters, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
}
ActorService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
ActorService.ctorParameters = () => [
    { type: HttpClient },
    { type: AcumenConfiguration }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    ActorService.prototype.backendServiceUrl;
    /**
     * @type {?}
     * @private
     */
    ActorService.prototype.httpClient;
    /**
     * @type {?}
     * @private
     */
    ActorService.prototype.acumenConfiguration;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWN0b3Iuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvYWN0b3Iuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFTLGVBQWUsQ0FBQztBQUM5QyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbEQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBTW5ELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBR3ZFLE1BQU0sT0FBTyxZQUFZOzs7Ozs7OztJQU12QixZQUFvQixVQUFzQixFQUFVLG1CQUF3QztRQUF4RSxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQVUsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUMxRixJQUFJLG1CQUFtQixDQUFDLGlCQUFpQixFQUFFO1lBQ3pDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxtQkFBbUIsQ0FBQyxpQkFBaUIsQ0FBQztTQUNoRTthQUFNO1lBQ0wsSUFBSSxDQUFDLGlCQUFpQixHQUFHLG9DQUFvQyxDQUFDO1NBQy9EO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxhQUFhLENBQUMsUUFBZ0I7O1lBQ3hCLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsd0JBQXdCLEdBQUcsUUFBUTs7Y0FDaEUsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzlCLFFBQVEsRUFBRSxrQkFBa0I7U0FDN0IsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQWEsR0FBRyxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQ3hFLElBQUk7Ozs7UUFBQyxRQUFRLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQWMsQ0FBQyxDQUFDO1FBQ2pELENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxLQUFLLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBRUQsc0JBQXNCLENBQUMsZUFBdUI7O1lBQ3hDLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsd0JBQXdCLEdBQUcsZUFBZTs7Y0FDdkUsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQzlCLFFBQVEsRUFBRSxrQkFBa0I7U0FDN0IsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQVEsR0FBRyxFQUFFLEVBQUMsT0FBTyxFQUFFLE9BQU8sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2FBQ25FLElBQUk7Ozs7UUFBQyxRQUFRLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBQSxRQUFRLEVBQVMsQ0FBQyxDQUFDO1FBQzVDLENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxLQUFLLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7OztJQUVELDZCQUE2QixDQUFDLGVBQXVCLEVBQUUsU0FBaUI7O1lBQ2xFLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsZ0NBQWdDLEdBQUcsZUFBZSxHQUFHLEdBQUcsR0FBRyxTQUFTOztjQUNqRyxPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDOUIsUUFBUSxFQUFFLGtCQUFrQjtTQUM3QixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBUSxHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDbkUsSUFBSTs7OztRQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLG1CQUFBLFFBQVEsRUFBUyxDQUFDLENBQUM7UUFDNUMsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7O0lBRUQsZUFBZSxDQUFDLE9BQWUsRUFBRSxTQUFpQjs7WUFDNUMsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyw0QkFBNEIsR0FBRyxTQUFTLEdBQUcsR0FBRyxHQUFHLE9BQU87O2NBQ3JGLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQztZQUM5QixRQUFRLEVBQUUsa0JBQWtCO1NBQzdCLENBQUM7UUFFRixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFlLEdBQUcsRUFBRSxFQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRTthQUMxRSxJQUFJOzs7O1FBQUMsUUFBUSxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFnQixDQUFDLENBQUM7UUFDbkQsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCxtQkFBbUIsQ0FBQyxPQUFlOztZQUM3QixHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFHLHFEQUFxRCxPQUFPLEVBQUU7O2NBQzNGLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQztZQUM5QixRQUFRLEVBQUUsa0JBQWtCO1NBQzdCLENBQUM7UUFFRixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFvQixHQUFHLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7YUFDL0UsSUFBSTs7OztRQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ2YsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLG1CQUFBLFFBQVEsRUFBcUIsQ0FBQyxDQUFDO1FBQ3hELENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxLQUFLLENBQUMsRUFBRTtZQUNmLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7OztJQUVELHNCQUFzQixDQUFDLE9BQWUsRUFBRSxnQkFBd0M7O1lBQzFFLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsMERBQTBEOztZQUN6RixPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUM7WUFDNUIsUUFBUSxFQUFFLGtCQUFrQjtTQUM3QixDQUFDOztZQUVFLFVBQVUsR0FBRztZQUNmLE9BQU8sRUFBRSxPQUFPO1lBQ2hCLGdCQUFnQixFQUFFLGdCQUFnQjtTQUNuQztRQUVELE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLFVBQVUsRUFBRSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDLFNBQVMsRUFBRTthQUMzRSxJQUFJOzs7O1FBQUMsUUFBUSxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQUEsUUFBUSxFQUFVLENBQUMsQ0FBQztRQUM3QyxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUU7WUFDZixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7WUF0R0YsVUFBVTs7OztZQVRGLFVBQVU7WUFPVixtQkFBbUI7Ozs7Ozs7SUFJMUIseUNBQTBCOzs7OztJQUtkLGtDQUE4Qjs7Ozs7SUFBRSwyQ0FBZ0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gICAgZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgSHR0cEhlYWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5cbmltcG9ydCB7IFN5c3RlbVVzZXIgfSBmcm9tICcuLi9kb21haW4vc3lzdGVtLXVzZXInO1xuaW1wb3J0IHsgQWN0b3IgfSBmcm9tICcuLi9kb21haW4vYWN0b3InO1xuaW1wb3J0IHsgUHJvamVjdEFjdG9yIH0gZnJvbSAnLi4vZG9tYWluL3Byb2plY3QtYWN0b3InO1xuaW1wb3J0IHsgQWN0b3JGaWVsZFZhbHVlIH0gZnJvbSAnLi4vZG9tYWluL2FjdG9yLWZpZWxkLXZhbHVlJztcbmltcG9ydCB7IEFjdW1lbkNvbmZpZ3VyYXRpb24gfSBmcm9tICcuLi9zZXJ2aWNlcy9hY3VtZW4tY29uZmlndXJhdGlvbic7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBBY3RvclNlcnZpY2Uge1xuICBwcml2YXRlIGJhY2tlbmRTZXJ2aWNlVXJsO1xuICAvLyBwcml2YXRlIGJhY2tlbmRTZXJ2aWNlVXJsID0gJ2h0dHA6Ly93d3cuaGVhbHRoYWN1bWVuLmNvLnphL2luc2lnaHQvJztcbiAgLy8gcHJpdmF0ZSBiYWNrZW5kU2VydmljZVVybCA9ICdodHRwczovL2ZyYW5rLnJtc3VpLmNvLnphL2luc2lnaHQvJztcbiAgLy8gcHJpdmF0ZSBiYWNrZW5kU2VydmljZVVybCA9ICdodHRwOi8vbG9jYWxob3N0Ojg4ODgvJztcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHBDbGllbnQ6IEh0dHBDbGllbnQsIHByaXZhdGUgYWN1bWVuQ29uZmlndXJhdGlvbjogQWN1bWVuQ29uZmlndXJhdGlvbikge1xuICAgIGlmIChhY3VtZW5Db25maWd1cmF0aW9uLmJhY2tlbmRTZXJ2aWNlVXJsKSB7XG4gICAgICB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsID0gYWN1bWVuQ29uZmlndXJhdGlvbi5iYWNrZW5kU2VydmljZVVybDtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5iYWNrZW5kU2VydmljZVVybCA9IFwiaHR0cHM6Ly9mcmFuay5ybXN1aS5jby56YS9pbnNpZ2h0L1wiO1xuICAgIH1cbiAgfVxuXG4gIGdldFN5c3RlbVVzZXIodXNlcm5hbWU6IHN0cmluZyk6IFByb21pc2U8U3lzdGVtVXNlcj4ge1xuICAgIGxldCB1cmwgPSB0aGlzLmJhY2tlbmRTZXJ2aWNlVXJsICsgXCJyZXN0L2FjdG9yL3N5c3RlbVVzZXIvXCIgKyB1c2VybmFtZTtcbiAgICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldDxTeXN0ZW1Vc2VyPih1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBTeXN0ZW1Vc2VyKTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgZ2V0QWN0b3JGcm9tRXh0ZXJuYWxJZChleHRlcm5hbEFjdG9ySWQ6IHN0cmluZyk6IFByb21pc2U8QWN0b3I+IHtcbiAgICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybCArIFwicmVzdC9hY3Rvci9leHRlcm5hbElkL1wiICsgZXh0ZXJuYWxBY3RvcklkO1xuICAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICB9KTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PEFjdG9yPih1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBBY3Rvcik7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIGdldFByb2plY3RBY3RvckZyb21FeHRlcm5hbElkKGV4dGVybmFsQWN0b3JJZDogc3RyaW5nLCBwcm9qZWN0SWQ6IG51bWJlcik6IFByb21pc2U8QWN0b3I+IHtcbiAgICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybCArIFwicmVzdC9hY3Rvci9wcm9qZWN0UGFydGljaXBhbnQvXCIgKyBleHRlcm5hbEFjdG9ySWQgKyBcIi9cIiArIHByb2plY3RJZDtcbiAgICBjb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldDxBY3Rvcj4odXJsLCB7aGVhZGVyczogaGVhZGVyc30pLnRvUHJvbWlzZSgpXG4gICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocmVzcG9uc2UgYXMgQWN0b3IpO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxuICBnZXRQcm9qZWN0QWN0b3IoYWN0b3JJZDogbnVtYmVyLCBwcm9qZWN0SWQ6IG51bWJlcik6IFByb21pc2U8UHJvamVjdEFjdG9yPiB7XG4gICAgbGV0IHVybCA9IHRoaXMuYmFja2VuZFNlcnZpY2VVcmwgKyBcInJlc3QvcHJvamVjdC9wcm9qZWN0QWN0b3IvXCIgKyBwcm9qZWN0SWQgKyBcIi9cIiArIGFjdG9ySWQ7XG4gICAgY29uc3QgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8UHJvamVjdEFjdG9yPih1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBQcm9qZWN0QWN0b3IpO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxuICBnZXRBY3RvckZpZWxkVmFsdWVzKGFjdG9ySWQ6IG51bWJlcik6IFByb21pc2U8QWN0b3JGaWVsZFZhbHVlW10+IHtcbiAgICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybCArIGByZXN0L2FjdG9yL2FjdG9yVHlwZS9hY3RvckZpZWxkcy9hY3RvckZpZWxkVmFsdWVzLyR7YWN0b3JJZH1gO1xuICAgIGNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICB9KTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PEFjdG9yRmllbGRWYWx1ZVtdPih1cmwsIHtoZWFkZXJzOiBoZWFkZXJzfSkudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShyZXNwb25zZSBhcyBBY3RvckZpZWxkVmFsdWVbXSk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIHVwZGF0ZUFjdG9yRmllbGRWYWx1ZXMoYWN0b3JJZDogbnVtYmVyLCBhY3RvckZpZWxkVmFsdWVzOiBBcnJheTxBY3RvckZpZWxkVmFsdWU+KTogUHJvbWlzZTxhbnk+IHtcbiAgICBsZXQgdXJsID0gdGhpcy5iYWNrZW5kU2VydmljZVVybCArIGByZXN0L2FjdG9yL2FjdG9yVHlwZS9hY3RvckZpZWxkcy9hY3RvckZpZWxkVmFsdWVzL3VwZGF0ZWA7XG4gICAgbGV0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJ1xuICAgIH0pO1xuXG4gICAgbGV0IHBhcmFtZXRlcnMgPSB7XG4gICAgICBhY3RvcklkOiBhY3RvcklkLFxuICAgICAgYWN0b3JGaWVsZFZhbHVlczogYWN0b3JGaWVsZFZhbHVlc1xuICAgIH1cblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQucG9zdCh1cmwsIHBhcmFtZXRlcnMsIHsgaGVhZGVyczogaGVhZGVycyB9KS50b1Byb21pc2UoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlIGFzIHN0cmluZyk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnJvcik7XG4gICAgICB9KTtcbiAgfVxufVxuIl19