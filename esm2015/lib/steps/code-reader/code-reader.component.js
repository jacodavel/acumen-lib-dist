/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { StepInstance } from '../../domain/step-instance';
export class CodeReaderComponent {
    /**
     * @param {?} projectService
     */
    constructor(projectService) {
        this.projectService = projectService;
        this.onReadCode = new EventEmitter();
        this.stepCompletedListener = new EventEmitter();
        this.cancelListener = new EventEmitter();
        this.loading = false;
        this.description = "Code";
        this.variableName = "variableName";
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.loading = true;
        this.projectService.getStepParameters(this.stepInstance.step.id).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            this.loading = false;
            this.description = this.stepInstance.step.parameters["0"] && this.stepInstance.step.parameters["0"].parameterValue;
            this.variableName = this.stepInstance.step.parameters["1"] && this.stepInstance.step.parameters["1"].parameterValue;
        }));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @return {?}
     */
    readCode() {
        this.onReadCode.emit(this);
    }
    /**
     * @return {?}
     */
    completeStep() {
        this.loading = true;
        this.projectService.setProcessVariableValue(this.stepInstance.businessProcessInstance.id, this.variableName, this.code).then((/**
         * @param {?} imageUpdateResult
         * @return {?}
         */
        imageUpdateResult => {
            this.projectService.completeStep(this.stepInstance.id, this.userId).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                this.loading = false;
                this.stepCompletedListener.emit({
                    value: this.stepInstance
                });
            })).catch((/**
             * @param {?} error
             * @return {?}
             */
            error => {
                this.loading = false;
                this.stepCompletedListener.emit({
                    value: this.stepInstance
                });
            }));
        }));
    }
    /**
     * @return {?}
     */
    cancel() {
        this.cancelListener.emit({});
    }
    /**
     * @param {?} value
     * @return {?}
     */
    setValue(value) {
        this.code = value;
    }
}
CodeReaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'code-reader',
                template: "<div class=\"code-reader-step\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div *ngIf=\"!loading\" class=\"content\">\n    <div class=\"component-row\">\n      <div class=\"component-col\">\n        <div *ngIf=\"!description\" class=\"component-label\">\n          Value\n        </div>\n        <div *ngIf=\"description\" class=\"component-label\">\n          {{ description }}\n        </div>\n        <div class=\"component\">\n          <input type=\"text\" [(ngModel)]=\"code\" readonly>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!loading\" class=\"button-section\">\n    <div class=\"button\" (click)=\"readCode()\">\n      <div class=\"button-text\">Scan</div>\n    </div>\n    <div class=\"button\" (click)=\"completeStep()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div class=\"button button-outline\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n  <div *ngIf=\"loading\" class=\"button-section\">\n    <div class=\"button button-disabled\">\n      <div class=\"button-text\">Scan</div>\n    </div>\n    <div class=\"button button-disabled\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div class=\"button button-outline-disabled\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                styles: [".code-reader-step{width:100%;height:100%;position:relative}.code-reader-step .heading{position:absolute;top:0;width:100%;height:32px;font-size:16px;padding:8px}.code-reader-step .content{position:absolute;top:0;left:0;bottom:50px;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px;box-sizing:border-box}.code-reader-step .content .info-message{font-size:15px}.code-reader-step .content .component-row{width:100%}.code-reader-step .content .component-row .component-col{width:100%;display:inline-block;box-sizing:border-box;padding-top:10px}.code-reader-step .content .component-row .component-col .component-label{width:100%;font-size:15px;padding-bottom:5px}.code-reader-step .content .component-row .component-col .error-message{font-style:italic;font-size:11px;color:#f04141;padding-bottom:5px}.code-reader-step .content .component-row .component-col .component{width:100%}.code-reader-step .content .component-row .component-col .component input{width:100%;font-size:13px;border:1px solid #58666c;background-color:#fffcf6;box-sizing:border-box;padding:3px 5px;border-radius:5px;height:30px}.code-reader-step .content .component-row:first-child .component-col:first-child{padding-top:0}@media (min-width:600px){.code-reader-step .content .component-row .component-col{width:50%}.code-reader-step .content .component-row .component-col:first-child{padding-right:16px}.code-reader-step .content .component-row:first-child .component-col{padding-top:0}}.code-reader-step .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.code-reader-step .button-section .button{width:70px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.code-reader-step .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.code-reader-step .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.code-reader-step .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b}.code-reader-step .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b}"]
            }] }
];
/** @nocollapse */
CodeReaderComponent.ctorParameters = () => [
    { type: ProjectService }
];
CodeReaderComponent.propDecorators = {
    stepInstance: [{ type: Input }],
    userId: [{ type: Input }],
    onReadCode: [{ type: Output }],
    stepCompletedListener: [{ type: Output }],
    cancelListener: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    CodeReaderComponent.prototype.stepInstance;
    /** @type {?} */
    CodeReaderComponent.prototype.userId;
    /** @type {?} */
    CodeReaderComponent.prototype.onReadCode;
    /** @type {?} */
    CodeReaderComponent.prototype.stepCompletedListener;
    /** @type {?} */
    CodeReaderComponent.prototype.cancelListener;
    /** @type {?} */
    CodeReaderComponent.prototype._stepInstance;
    /** @type {?} */
    CodeReaderComponent.prototype.loading;
    /** @type {?} */
    CodeReaderComponent.prototype.code;
    /** @type {?} */
    CodeReaderComponent.prototype.description;
    /** @type {?} */
    CodeReaderComponent.prototype.variableName;
    /**
     * @type {?}
     * @private
     */
    CodeReaderComponent.prototype.projectService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29kZS1yZWFkZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWN1bWVuLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9zdGVwcy9jb2RlLXJlYWRlci9jb2RlLXJlYWRlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBcUIsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTFGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNoRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFRMUQsTUFBTSxPQUFPLG1CQUFtQjs7OztJQVk5QixZQUFvQixjQUE4QjtRQUE5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFUeEMsZUFBVSxHQUFHLElBQUksWUFBWSxFQUFvQixDQUFDO1FBQ2xELDBCQUFxQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDM0MsbUJBQWMsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBRTlDLFlBQU8sR0FBWSxLQUFLLENBQUM7UUFFekIsZ0JBQVcsR0FBVyxNQUFNLENBQUM7UUFDN0IsaUJBQVksR0FBVyxjQUFjLENBQUM7SUFHdEMsQ0FBQzs7OztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNwQixJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxNQUFNLENBQUMsRUFBRTtZQUM3RSxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztZQUNyQixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsY0FBYyxDQUFDO1lBQ25ILElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxjQUFjLENBQUM7UUFDdEgsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7O0lBRUQsV0FBVztJQUNYLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDN0IsQ0FBQzs7OztJQUVELFlBQVk7UUFDVixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNwQixJQUFJLENBQUMsY0FBYyxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsdUJBQXVCLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxpQkFBaUIsQ0FBQyxFQUFFO1lBQy9JLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJOzs7O1lBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQ2hGLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO2dCQUNyQixJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDO29CQUM5QixLQUFLLEVBQUUsSUFBSSxDQUFDLFlBQVk7aUJBQ3pCLENBQUMsQ0FBQztZQUNMLENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7WUFBQyxLQUFLLENBQUMsRUFBRTtnQkFDZixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDckIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQztvQkFDOUIsS0FBSyxFQUFFLElBQUksQ0FBQyxZQUFZO2lCQUN6QixDQUFDLENBQUM7WUFDTCxDQUFDLEVBQUMsQ0FBQztRQUNMLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVELE1BQU07UUFDSixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxFQUN4QixDQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUVELFFBQVEsQ0FBQyxLQUFhO1FBQ3BCLElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO0lBQ3BCLENBQUM7OztZQTVERixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGFBQWE7Z0JBQ3ZCLG8yQ0FBMkM7O2FBRTVDOzs7O1lBUlEsY0FBYzs7OzJCQVVwQixLQUFLO3FCQUNMLEtBQUs7eUJBQ0wsTUFBTTtvQ0FDTixNQUFNOzZCQUNOLE1BQU07Ozs7SUFKUCwyQ0FBb0M7O0lBQ3BDLHFDQUF3Qjs7SUFDeEIseUNBQTREOztJQUM1RCxvREFBcUQ7O0lBQ3JELDZDQUE4Qzs7SUFDOUMsNENBQTRCOztJQUM1QixzQ0FBeUI7O0lBQ3pCLG1DQUFhOztJQUNiLDBDQUE2Qjs7SUFDN0IsMkNBQXNDOzs7OztJQUUxQiw2Q0FBc0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE9uSW5pdCwgT25EZXN0cm95LCBPdXRwdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgUHJvamVjdFNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9wcm9qZWN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgU3RlcEluc3RhbmNlIH0gZnJvbSAnLi4vLi4vZG9tYWluL3N0ZXAtaW5zdGFuY2UnO1xuaW1wb3J0IHsgQ29kZVJlYWRMaXN0ZW5lciB9IGZyb20gJy4vY29kZS1yZWFkLWxpc3RlbmVyJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnY29kZS1yZWFkZXInLFxuICB0ZW1wbGF0ZVVybDogJy4vY29kZS1yZWFkZXIuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9jb2RlLXJlYWRlci5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIENvZGVSZWFkZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSwgQ29kZVJlYWRMaXN0ZW5lciB7XG4gIEBJbnB1dCgpIHN0ZXBJbnN0YW5jZTogU3RlcEluc3RhbmNlO1xuICBASW5wdXQoKSB1c2VySWQ6IG51bWJlcjtcbiAgQE91dHB1dCgpIG9uUmVhZENvZGUgPSBuZXcgRXZlbnRFbWl0dGVyPENvZGVSZWFkTGlzdGVuZXI+KCk7XG4gIEBPdXRwdXQoKSBzdGVwQ29tcGxldGVkTGlzdGVuZXIgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBjYW5jZWxMaXN0ZW5lciA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgX3N0ZXBJbnN0YW5jZTogU3RlcEluc3RhbmNlO1xuICBsb2FkaW5nOiBib29sZWFuID0gZmFsc2U7XG4gIGNvZGU6IHN0cmluZztcbiAgZGVzY3JpcHRpb246IHN0cmluZyA9IFwiQ29kZVwiO1xuICB2YXJpYWJsZU5hbWU6IHN0cmluZyA9IFwidmFyaWFibGVOYW1lXCI7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBwcm9qZWN0U2VydmljZTogUHJvamVjdFNlcnZpY2UpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMubG9hZGluZyA9IHRydWU7XG4gICAgdGhpcy5wcm9qZWN0U2VydmljZS5nZXRTdGVwUGFyYW1ldGVycyh0aGlzLnN0ZXBJbnN0YW5jZS5zdGVwLmlkKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgIHRoaXMuZGVzY3JpcHRpb24gPSB0aGlzLnN0ZXBJbnN0YW5jZS5zdGVwLnBhcmFtZXRlcnNbXCIwXCJdICYmIHRoaXMuc3RlcEluc3RhbmNlLnN0ZXAucGFyYW1ldGVyc1tcIjBcIl0ucGFyYW1ldGVyVmFsdWU7XG4gICAgICB0aGlzLnZhcmlhYmxlTmFtZSA9IHRoaXMuc3RlcEluc3RhbmNlLnN0ZXAucGFyYW1ldGVyc1tcIjFcIl0gJiYgdGhpcy5zdGVwSW5zdGFuY2Uuc3RlcC5wYXJhbWV0ZXJzW1wiMVwiXS5wYXJhbWV0ZXJWYWx1ZTtcbiAgICB9KTtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCkge1xuICB9XG5cbiAgcmVhZENvZGUoKSB7XG4gICAgdGhpcy5vblJlYWRDb2RlLmVtaXQodGhpcyk7XG4gIH1cblxuICBjb21wbGV0ZVN0ZXAoKSB7XG4gICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcbiAgICB0aGlzLnByb2plY3RTZXJ2aWNlLnNldFByb2Nlc3NWYXJpYWJsZVZhbHVlKHRoaXMuc3RlcEluc3RhbmNlLmJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlLmlkLCB0aGlzLnZhcmlhYmxlTmFtZSwgdGhpcy5jb2RlKS50aGVuKGltYWdlVXBkYXRlUmVzdWx0ID0+IHtcbiAgICAgIHRoaXMucHJvamVjdFNlcnZpY2UuY29tcGxldGVTdGVwKHRoaXMuc3RlcEluc3RhbmNlLmlkLCB0aGlzLnVzZXJJZCkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5zdGVwQ29tcGxldGVkTGlzdGVuZXIuZW1pdCh7XG4gICAgICAgICAgdmFsdWU6IHRoaXMuc3RlcEluc3RhbmNlXG4gICAgICAgIH0pO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5zdGVwQ29tcGxldGVkTGlzdGVuZXIuZW1pdCh7XG4gICAgICAgICAgdmFsdWU6IHRoaXMuc3RlcEluc3RhbmNlXG4gICAgICAgIH0pO1xuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICBjYW5jZWwoKSB7XG4gICAgdGhpcy5jYW5jZWxMaXN0ZW5lci5lbWl0KHtcbiAgICB9KTtcbiAgfVxuXG4gIHNldFZhbHVlKHZhbHVlOiBzdHJpbmcpIHtcbiAgICB0aGlzLmNvZGUgPSB2YWx1ZTtcbiAgfVxufVxuIl19