/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ProjectService } from '../../services/project.service';
import { StepInstance } from '../../domain/step-instance';
export class IdNrLookupComponent {
    /**
     * @param {?} sanitizer
     * @param {?} projectService
     */
    constructor(sanitizer, projectService) {
        this.sanitizer = sanitizer;
        this.projectService = projectService;
        this.stepCompletedListener = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.noIdNr = false;
        this.idNrNotFound = false;
        this.invalidIdNr = false;
        this.hideNoIdNrWarning = this.stepInstance.step.parameters["0"] && this.stepInstance.step.parameters["0"].parameterValue;
        this.hideNoIdNrOption = this.stepInstance.step.parameters["1"] && this.stepInstance.step.parameters["1"].parameterValue;
        if (this.stepInstance.step.parameters["2"]) {
            this.idNrLabel = this.stepInstance.step.parameters["2"].parameterValue;
        }
        if (!this.idNrLabel) {
            this.idNrLabel = "Id number";
        }
        this.loading = true;
        this.projectService.getMessageStepHtml(this.stepInstance.id).then((/**
         * @param {?} html
         * @return {?}
         */
        (html) => {
            this.loading = false;
            if (html["text"]) {
                this.html = this.sanitizer.bypassSecurityTrustHtml(html["text"]);
            }
        }));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @return {?}
     */
    continueWithIdNr() {
        this.noIdNr = false;
        this.idNrNotFound = false;
        this.invalidIdNr = false;
        if (!this.idNr) {
            this.noIdNr = true;
            return;
        }
        // if (this.idNr.length !== 13 || !(/^\d+$/.test(this.idNr))) {
        //     this.invalidIdNr = true;
        //     return;
        // }
        this.projectService.verifyActorIdNr(this.stepInstance.businessProcessInstance.id, this.idNr).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            if (result.response === "true") {
                this.completeStep();
            }
            else {
                if (!this.hideNoIdNrWarning) {
                    this.idNrNotFound = true;
                }
                else {
                    this.completeStep();
                }
            }
        }));
    }
    /**
     * @return {?}
     */
    continueNoIdNr() {
        this.completeStep();
    }
    /**
     * @return {?}
     */
    completeStep() {
        this.projectService.completeStep(this.stepInstance.id, this.userId).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            this.stepCompletedListener.emit({
                value: this.stepInstance,
                useBusinessProcessId: true
            });
        }));
    }
}
IdNrLookupComponent.decorators = [
    { type: Component, args: [{
                selector: 'id-nr-lookup',
                template: "<div class=\"id-nr-lookup\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div class=\"heading\">\n    Registration\n  </div>\n\n  <div *ngIf=\"!this.loading && hideNoIdNrOption\" class=\"content\">\n    <div *ngIf=\"!html\" class=\"wording\">\n      Please provide a valid {{ idNrLabel }}.\n    </div>\n    <div *ngIf=\"!html\" class=\"wording\">\n      This number is encrypted immediately and is not shared with any other party.\n    </div>\n    <div *ngIf=\"!html\" class=\"wording\">\n      It is only used as a unique identifier for this application. If you register\n      for future projects, it will also be used to link your projects.\n    </div>\n    <div *ngIf=\"!html\" class=\"wording\">\n      Schemes will notify us when you are hospitalised or registered for a disease so\n      that the relevant surveys can be sent to you.\n    </div>\n    <div *ngIf=\"html\" class=\"wording\" [innerHTML]=\"html\">\n    </div>\n\n    <div>\n      <input [(ngModel)]=\"idNr\" type=\"text\" id=\"idNrInput\" placeholder=\"{{ idNrLabel }}\">\n    </div>\n\n    <div *ngIf=\"idNrNotFound && !hideNoIdNrWarning\" class=\"error-text\">\n      No details were found on the system for the specified {{ idNrLabel }}.  Are you sure it is correct?\n    </div>\n    <div *ngIf=\"noIdNr\" class=\"error-text\">\n      Please provide your {{ idNrLabel }}\n    </div>\n    <div *ngIf=\"invalidIdNr && hideNoIdNrWarning\" class=\"error-text\">\n      Please provide a valid {{ idNrLabel }}\n    </div>\n  </div>\n  <div *ngIf=\"hideNoIdNrOption\" class=\"button-section\">\n    <div *ngIf=\"!idNrNotFound\" class=\"button\" (click)=\"continueWithIdNr()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div *ngIf=\"idNrNotFound\" class=\"button button-outline\" (click)=\"completeStep()\">\n      <div class=\"button-text\">{{ idNrLabel }} is correct</div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!this.loading && !hideNoIdNrOption\" class=\"content\">\n    <div *ngIf=\"!html\" class=\"wording\">\n      To continue with the registration process, please enter your {{ idNrLabel }}:\n    </div>\n    <div *ngIf=\"html\" class=\"wording\" [innerHTML]=\"html\">\n    </div>\n    <div>\n      <input class=\"form-control\" [(ngModel)]=\"idNr\" type=\"text\" id=\"idNrInput\">\n    </div>\n\n    <div *ngIf=\"idNrNotFound && !hideNoIdNrWarning\" class=\"error-text\">\n      No details were found on the system for the specified {{ idNrLabel }}.  Are you sure it is correct?\n    </div>\n    <div *ngIf=\"noIdNr\" class=\"error-text\">\n      Please provide your {{ idNrLabel }}\n    </div>\n    <div *ngIf=\"invalidIdNr && hideNoIdNrWarning\" class=\"error-text\">\n      Please provide a valid {{ idNrLabel }}\n    </div>\n    <div class=\"wording top-padding\">\n      Alternatively, if no {{ idNrLabel }} is avalaible, please click on \"No {{ idNrLabel }}\" below to continue.\n    </div>\n  </div>\n  <div *ngIf=\"!hideNoIdNrOption\" class=\"button-section\">\n    <div *ngIf=\"!idNrNotFound\" class=\"button\" (click)=\"continueWithIdNr()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div *ngIf=\"idNrNotFound\" class=\"button\" (click)=\"completeStep()\">\n      <div class=\"button-text\">{{ idNrLabel }} is correct</div>\n    </div>\n    <div class=\"button button-outline\" (click)=\"continueNoIdNr()\">\n      <div class=\"button-text\">No {{ idNrLabel }}</div>\n    </div>\n  </div>\n</div>\n",
                styles: [".id-nr-lookup{width:100%;height:100%;position:relative}.id-nr-lookup .heading{position:absolute;top:0;width:100%;height:32px;font-size:16px;padding:8px}.id-nr-lookup .top-padding{padding-top:8px}.id-nr-lookup .content{position:absolute;top:32px;left:0;bottom:50px;width:100%;overflow-y:scroll;padding:8px;box-sizing:border-box;background-color:#f9f9f9;font-size:15px}.id-nr-lookup .content .wording{width:100%;font-size:15px;padding-bottom:5px}.id-nr-lookup .content input{width:200;font-size:13px;border:1px solid #58666c;background-color:#fffcf6;box-sizing:border-box;padding:3px 5px;margin-bottom:5px;height:30px;border-radius:5px}.id-nr-lookup .error-text{font-style:italic;font-size:11px;color:#f04141}.id-nr-lookup .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.id-nr-lookup .button-section .button{width:120px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.id-nr-lookup .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.id-nr-lookup .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.id-nr-lookup .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b;cursor:default}.id-nr-lookup .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b;cursor:default}"]
            }] }
];
/** @nocollapse */
IdNrLookupComponent.ctorParameters = () => [
    { type: DomSanitizer },
    { type: ProjectService }
];
IdNrLookupComponent.propDecorators = {
    stepInstance: [{ type: Input }],
    userId: [{ type: Input }],
    stepCompletedListener: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    IdNrLookupComponent.prototype.stepInstance;
    /** @type {?} */
    IdNrLookupComponent.prototype.userId;
    /** @type {?} */
    IdNrLookupComponent.prototype.stepCompletedListener;
    /** @type {?} */
    IdNrLookupComponent.prototype.idNr;
    /** @type {?} */
    IdNrLookupComponent.prototype.noIdNr;
    /** @type {?} */
    IdNrLookupComponent.prototype.idNrNotFound;
    /** @type {?} */
    IdNrLookupComponent.prototype.invalidIdNr;
    /** @type {?} */
    IdNrLookupComponent.prototype.hideNoIdNrWarning;
    /** @type {?} */
    IdNrLookupComponent.prototype.hideNoIdNrOption;
    /** @type {?} */
    IdNrLookupComponent.prototype.idNrLabel;
    /** @type {?} */
    IdNrLookupComponent.prototype.html;
    /** @type {?} */
    IdNrLookupComponent.prototype.loading;
    /**
     * @type {?}
     * @private
     */
    IdNrLookupComponent.prototype.sanitizer;
    /**
     * @type {?}
     * @private
     */
    IdNrLookupComponent.prototype.projectService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWQtbnItbG9va3VwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvc3RlcHMvaWQtbnItbG9va3VwL2lkLW5yLWxvb2t1cC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBcUIsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzFGLE9BQU8sRUFBRSxZQUFZLEVBQW1CLE1BQU0sMkJBQTJCLENBQUM7QUFFMUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQ2hFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQU8xRCxNQUFNLE9BQU8sbUJBQW1COzs7OztJQWM5QixZQUFvQixTQUF1QixFQUFVLGNBQThCO1FBQS9ELGNBQVMsR0FBVCxTQUFTLENBQWM7UUFBVSxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFYekUsMEJBQXFCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQVlyRCxDQUFDOzs7O0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzFCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1FBRXpCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLGNBQWMsQ0FBQztRQUN6SCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxjQUFjLENBQUM7UUFDeEgsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDMUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsY0FBYyxDQUFDO1NBQ3hFO1FBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDbkIsSUFBSSxDQUFDLFNBQVMsR0FBRyxXQUFXLENBQUM7U0FDOUI7UUFFRCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNwQixJQUFJLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSTs7OztRQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7WUFDekUsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFDckIsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQ2hCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQzthQUNsRTtRQUNILENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVELFdBQVc7SUFDWCxDQUFDOzs7O0lBRUQsZ0JBQWdCO1FBQ2QsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDMUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7UUFDekIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDZCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztZQUNuQixPQUFPO1NBQ1I7UUFFRCwrREFBK0Q7UUFDL0QsK0JBQStCO1FBQy9CLGNBQWM7UUFDZCxJQUFJO1FBRUosSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxNQUFNLENBQUMsRUFBRTtZQUN6RyxJQUFJLE1BQU0sQ0FBQyxRQUFRLEtBQUssTUFBTSxFQUFFO2dCQUM5QixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7YUFDckI7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtvQkFDM0IsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7aUJBQzFCO3FCQUFNO29CQUNMLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztpQkFDckI7YUFDRjtRQUNILENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVELGNBQWM7UUFDVixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDeEIsQ0FBQzs7OztJQUVELFlBQVk7UUFDVixJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSTs7OztRQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQ2hGLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUM7Z0JBQzlCLEtBQUssRUFBRSxJQUFJLENBQUMsWUFBWTtnQkFDeEIsb0JBQW9CLEVBQUUsSUFBSTthQUMzQixDQUFDLENBQUM7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7OztZQXZGRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGNBQWM7Z0JBQ3hCLHc0R0FBNEM7O2FBRTdDOzs7O1lBVFEsWUFBWTtZQUVaLGNBQWM7OzsyQkFTcEIsS0FBSztxQkFDTCxLQUFLO29DQUNMLE1BQU07Ozs7SUFGUCwyQ0FBb0M7O0lBQ3BDLHFDQUF3Qjs7SUFDeEIsb0RBQXFEOztJQUNyRCxtQ0FBYTs7SUFDYixxQ0FBZ0I7O0lBQ2hCLDJDQUFzQjs7SUFDdEIsMENBQXFCOztJQUNyQixnREFBMkI7O0lBQzNCLCtDQUEwQjs7SUFDMUIsd0NBQWtCOztJQUNsQixtQ0FBVTs7SUFDVixzQ0FBaUI7Ozs7O0lBRUwsd0NBQStCOzs7OztJQUFFLDZDQUFzQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPbkRlc3Ryb3ksIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRG9tU2FuaXRpemVyLCBTYWZlUmVzb3VyY2VVcmwgfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyJztcblxuaW1wb3J0IHsgUHJvamVjdFNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9wcm9qZWN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgU3RlcEluc3RhbmNlIH0gZnJvbSAnLi4vLi4vZG9tYWluL3N0ZXAtaW5zdGFuY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdpZC1uci1sb29rdXAnLFxuICB0ZW1wbGF0ZVVybDogJy4vaWQtbnItbG9va3VwLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vaWQtbnItbG9va3VwLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgSWROckxvb2t1cENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgQElucHV0KCkgc3RlcEluc3RhbmNlOiBTdGVwSW5zdGFuY2U7XG4gIEBJbnB1dCgpIHVzZXJJZDogbnVtYmVyO1xuICBAT3V0cHV0KCkgc3RlcENvbXBsZXRlZExpc3RlbmVyID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBpZE5yOiBzdHJpbmc7XG4gIG5vSWROcjogYm9vbGVhbjtcbiAgaWROck5vdEZvdW5kOiBib29sZWFuO1xuICBpbnZhbGlkSWROcjogYm9vbGVhbjtcbiAgaGlkZU5vSWROcldhcm5pbmc6IGJvb2xlYW47XG4gIGhpZGVOb0lkTnJPcHRpb246IGJvb2xlYW47XG4gIGlkTnJMYWJlbDogc3RyaW5nO1xuICBodG1sOiBhbnk7XG4gIGxvYWRpbmc6IGJvb2xlYW47XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBzYW5pdGl6ZXI6IERvbVNhbml0aXplciwgcHJpdmF0ZSBwcm9qZWN0U2VydmljZTogUHJvamVjdFNlcnZpY2UpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMubm9JZE5yID0gZmFsc2U7XG4gICAgdGhpcy5pZE5yTm90Rm91bmQgPSBmYWxzZTtcbiAgICB0aGlzLmludmFsaWRJZE5yID0gZmFsc2U7XG5cbiAgICB0aGlzLmhpZGVOb0lkTnJXYXJuaW5nID0gdGhpcy5zdGVwSW5zdGFuY2Uuc3RlcC5wYXJhbWV0ZXJzW1wiMFwiXSAmJiB0aGlzLnN0ZXBJbnN0YW5jZS5zdGVwLnBhcmFtZXRlcnNbXCIwXCJdLnBhcmFtZXRlclZhbHVlO1xuICAgIHRoaXMuaGlkZU5vSWROck9wdGlvbiA9IHRoaXMuc3RlcEluc3RhbmNlLnN0ZXAucGFyYW1ldGVyc1tcIjFcIl0gJiYgdGhpcy5zdGVwSW5zdGFuY2Uuc3RlcC5wYXJhbWV0ZXJzW1wiMVwiXS5wYXJhbWV0ZXJWYWx1ZTtcbiAgICBpZiAodGhpcy5zdGVwSW5zdGFuY2Uuc3RlcC5wYXJhbWV0ZXJzW1wiMlwiXSkge1xuICAgICAgdGhpcy5pZE5yTGFiZWwgPSB0aGlzLnN0ZXBJbnN0YW5jZS5zdGVwLnBhcmFtZXRlcnNbXCIyXCJdLnBhcmFtZXRlclZhbHVlO1xuICAgIH1cblxuICAgIGlmICghdGhpcy5pZE5yTGFiZWwpIHtcbiAgICAgIHRoaXMuaWROckxhYmVsID0gXCJJZCBudW1iZXJcIjtcbiAgICB9XG5cbiAgICB0aGlzLmxvYWRpbmcgPSB0cnVlO1xuICAgIHRoaXMucHJvamVjdFNlcnZpY2UuZ2V0TWVzc2FnZVN0ZXBIdG1sKHRoaXMuc3RlcEluc3RhbmNlLmlkKS50aGVuKChodG1sKSA9PiB7XG4gICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgIGlmIChodG1sW1widGV4dFwiXSkge1xuICAgICAgICB0aGlzLmh0bWwgPSB0aGlzLnNhbml0aXplci5ieXBhc3NTZWN1cml0eVRydXN0SHRtbChodG1sW1widGV4dFwiXSk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpIHtcbiAgfVxuXG4gIGNvbnRpbnVlV2l0aElkTnIoKSB7XG4gICAgdGhpcy5ub0lkTnIgPSBmYWxzZTtcbiAgICB0aGlzLmlkTnJOb3RGb3VuZCA9IGZhbHNlO1xuICAgIHRoaXMuaW52YWxpZElkTnIgPSBmYWxzZTtcbiAgICBpZiAoIXRoaXMuaWROcikge1xuICAgICAgdGhpcy5ub0lkTnIgPSB0cnVlO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIC8vIGlmICh0aGlzLmlkTnIubGVuZ3RoICE9PSAxMyB8fCAhKC9eXFxkKyQvLnRlc3QodGhpcy5pZE5yKSkpIHtcbiAgICAvLyAgICAgdGhpcy5pbnZhbGlkSWROciA9IHRydWU7XG4gICAgLy8gICAgIHJldHVybjtcbiAgICAvLyB9XG5cbiAgICB0aGlzLnByb2plY3RTZXJ2aWNlLnZlcmlmeUFjdG9ySWROcih0aGlzLnN0ZXBJbnN0YW5jZS5idXNpbmVzc1Byb2Nlc3NJbnN0YW5jZS5pZCwgdGhpcy5pZE5yKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICBpZiAocmVzdWx0LnJlc3BvbnNlID09PSBcInRydWVcIikge1xuICAgICAgICB0aGlzLmNvbXBsZXRlU3RlcCgpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKCF0aGlzLmhpZGVOb0lkTnJXYXJuaW5nKSB7XG4gICAgICAgICAgdGhpcy5pZE5yTm90Rm91bmQgPSB0cnVlO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRoaXMuY29tcGxldGVTdGVwKCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIGNvbnRpbnVlTm9JZE5yKCkge1xuICAgICAgdGhpcy5jb21wbGV0ZVN0ZXAoKTtcbiAgfVxuXG4gIGNvbXBsZXRlU3RlcCgpIHtcbiAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmNvbXBsZXRlU3RlcCh0aGlzLnN0ZXBJbnN0YW5jZS5pZCwgdGhpcy51c2VySWQpLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgIHRoaXMuc3RlcENvbXBsZXRlZExpc3RlbmVyLmVtaXQoe1xuICAgICAgICB2YWx1ZTogdGhpcy5zdGVwSW5zdGFuY2UsXG4gICAgICAgIHVzZUJ1c2luZXNzUHJvY2Vzc0lkOiB0cnVlXG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxufVxuIl19