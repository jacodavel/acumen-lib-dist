/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { StepInstance } from '../../domain/step-instance';
export class RegisterComponent {
    /**
     * @param {?} projectService
     */
    constructor(projectService) {
        this.projectService = projectService;
        this.stepCompletedListener = new EventEmitter();
        this.cancelListener = new EventEmitter();
        this.agreedError = false;
        this.loading = false;
        this.agreeWording = "I agree to the terms and conditions as stated above.";
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.agreed = false;
        this.agreedError = false;
        this.loading = true;
        this.projectService.getStepParameters(this.stepInstance.step.id).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            this.loading = false;
            this.html = result["0"]; // && this.stepInstance.step.parameters["0"].parameterValue;
        }));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @return {?}
     */
    completeStep() {
        this.agreedError = false;
        if (!this.agreed) {
            this.agreedError = true;
            return;
        }
        this.loading = true;
        this.projectService.completeStep(this.stepInstance.id, this.userId).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            this.loading = false;
            this.stepCompletedListener.emit({
                value: this.stepInstance,
                useBusinessProcessId: true
            });
        }));
    }
    /**
     * @param {?} $selected
     * @return {?}
     */
    radioButtonChanged($selected) {
        this.agreed = $selected.value;
    }
    /**
     * @return {?}
     */
    cancel() {
        this.cancelListener.emit({});
    }
}
RegisterComponent.decorators = [
    { type: Component, args: [{
                selector: 'register',
                template: "<div class=\"register\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div class=\"heading\">\n    Register\n  </div>\n\n  <div class=\"content\">\n    <div *ngIf=\"html\" class=\"message\" [innerHTML]=\"html\">>\n    </div>\n    <div class=\"agree-checkbox\">\n      <checkbox-component [selected]=\"agreed\" (selectionListener)=\"radioButtonChanged($event);\"\n          [description]=\"agreeWording\"></checkbox-component>\n      <!-- <checkbox-component [selected]=\"agreed\" (selectionListener)=\"radioButtonChanged($event);\"\n        [description]=\"agreeWording\"></checkbox-component> -->\n    </div>\n    <div *ngIf=\"agreedError\" class=\"agree-error\">\n      You must agree to the terms and conditions to register.\n    </div>\n  </div>\n\n  <div *ngIf=\"!loading\" class=\"button-section\">\n    <div class=\"button\" (click)=\"completeStep()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div class=\"button button-outline\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n  <div *ngIf=\"loading\" class=\"button-section\">\n    <div class=\"button button-disabled\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div class=\"button button-outline-disabled\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                styles: [".register{width:100%;height:100%;position:relative}.register .heading{position:absolute;top:0;width:100%;height:32px;font-size:16px;padding:8px}.register .content{position:absolute;top:32px;left:0;bottom:50px;width:100%;overflow-y:scroll;padding:8px;box-sizing:border-box;background-color:#f9f9f9}.register .content .agree-checkbox{width:100%;font-size:15px;padding-bottom:5px}.register .content .agree-error{font-style:italic;font-size:11px;color:#f04141;padding-bottom:5px}.register .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.register .button-section .button{width:70px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.register .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.register .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.register .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b}.register .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b}"]
            }] }
];
/** @nocollapse */
RegisterComponent.ctorParameters = () => [
    { type: ProjectService }
];
RegisterComponent.propDecorators = {
    stepInstance: [{ type: Input }],
    userId: [{ type: Input }],
    stepCompletedListener: [{ type: Output }],
    cancelListener: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    RegisterComponent.prototype.stepInstance;
    /** @type {?} */
    RegisterComponent.prototype.userId;
    /** @type {?} */
    RegisterComponent.prototype.stepCompletedListener;
    /** @type {?} */
    RegisterComponent.prototype.cancelListener;
    /** @type {?} */
    RegisterComponent.prototype.html;
    /** @type {?} */
    RegisterComponent.prototype.agreed;
    /** @type {?} */
    RegisterComponent.prototype.agreedError;
    /** @type {?} */
    RegisterComponent.prototype.agreeWording;
    /** @type {?} */
    RegisterComponent.prototype.loading;
    /**
     * @type {?}
     * @private
     */
    RegisterComponent.prototype.projectService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVnaXN0ZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWN1bWVuLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9zdGVwcy9yZWdpc3Rlci9yZWdpc3Rlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBcUIsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTFGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNoRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFPMUQsTUFBTSxPQUFPLGlCQUFpQjs7OztJQVc1QixZQUFvQixjQUE4QjtRQUE5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFSeEMsMEJBQXFCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUMzQyxtQkFBYyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFHOUMsZ0JBQVcsR0FBWSxLQUFLLENBQUM7UUFFN0IsWUFBTyxHQUFZLEtBQUssQ0FBQztRQUd2QixJQUFJLENBQUMsWUFBWSxHQUFHLHNEQUFzRCxDQUFDO0lBQzdFLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7UUFDekIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJOzs7O1FBQUMsTUFBTSxDQUFDLEVBQUU7WUFDN0UsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFDckIsSUFBSSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQSw0REFBNEQ7UUFDdEYsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7O0lBRUQsV0FBVztJQUNYLENBQUM7Ozs7SUFFRCxZQUFZO1FBQ1YsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7UUFDekIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDaEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7WUFDeEIsT0FBTztTQUNSO1FBRUQsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxNQUFNLENBQUMsRUFBRTtZQUNoRixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztZQUNyQixJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDO2dCQUM5QixLQUFLLEVBQUUsSUFBSSxDQUFDLFlBQVk7Z0JBQ3hCLG9CQUFvQixFQUFFLElBQUk7YUFDM0IsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUVELGtCQUFrQixDQUFDLFNBQVM7UUFDMUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDO0lBQ2hDLENBQUM7Ozs7SUFFRCxNQUFNO1FBQ0osSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFDeEIsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7O1lBekRGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsVUFBVTtnQkFDcEIsNjFDQUF3Qzs7YUFFekM7Ozs7WUFQUSxjQUFjOzs7MkJBU3BCLEtBQUs7cUJBQ0wsS0FBSztvQ0FDTCxNQUFNOzZCQUNOLE1BQU07Ozs7SUFIUCx5Q0FBb0M7O0lBQ3BDLG1DQUF3Qjs7SUFDeEIsa0RBQXFEOztJQUNyRCwyQ0FBOEM7O0lBQzlDLGlDQUFhOztJQUNiLG1DQUFnQjs7SUFDaEIsd0NBQTZCOztJQUM3Qix5Q0FBcUI7O0lBQ3JCLG9DQUF5Qjs7Ozs7SUFFYiwyQ0FBc0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE9uSW5pdCwgT25EZXN0cm95LCBPdXRwdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgUHJvamVjdFNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9wcm9qZWN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgU3RlcEluc3RhbmNlIH0gZnJvbSAnLi4vLi4vZG9tYWluL3N0ZXAtaW5zdGFuY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdyZWdpc3RlcicsXG4gIHRlbXBsYXRlVXJsOiAnLi9yZWdpc3Rlci5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL3JlZ2lzdGVyLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgUmVnaXN0ZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIHN0ZXBJbnN0YW5jZTogU3RlcEluc3RhbmNlO1xuICBASW5wdXQoKSB1c2VySWQ6IG51bWJlcjtcbiAgQE91dHB1dCgpIHN0ZXBDb21wbGV0ZWRMaXN0ZW5lciA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIGNhbmNlbExpc3RlbmVyID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBodG1sOiBzdHJpbmc7XG4gIGFncmVlZDogYm9vbGVhbjtcbiAgYWdyZWVkRXJyb3I6IGJvb2xlYW4gPSBmYWxzZTtcbiAgYWdyZWVXb3JkaW5nOiBzdHJpbmc7XG4gIGxvYWRpbmc6IGJvb2xlYW4gPSBmYWxzZTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHByb2plY3RTZXJ2aWNlOiBQcm9qZWN0U2VydmljZSkge1xuICAgIHRoaXMuYWdyZWVXb3JkaW5nID0gXCJJIGFncmVlIHRvIHRoZSB0ZXJtcyBhbmQgY29uZGl0aW9ucyBhcyBzdGF0ZWQgYWJvdmUuXCI7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLmFncmVlZCA9IGZhbHNlO1xuICAgIHRoaXMuYWdyZWVkRXJyb3IgPSBmYWxzZTtcbiAgICB0aGlzLmxvYWRpbmcgPSB0cnVlO1xuICAgIHRoaXMucHJvamVjdFNlcnZpY2UuZ2V0U3RlcFBhcmFtZXRlcnModGhpcy5zdGVwSW5zdGFuY2Uuc3RlcC5pZCkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgICB0aGlzLmh0bWwgPSByZXN1bHRbXCIwXCJdOy8vICYmIHRoaXMuc3RlcEluc3RhbmNlLnN0ZXAucGFyYW1ldGVyc1tcIjBcIl0ucGFyYW1ldGVyVmFsdWU7XG4gICAgfSk7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpIHtcbiAgfVxuXG4gIGNvbXBsZXRlU3RlcCgpIHtcbiAgICB0aGlzLmFncmVlZEVycm9yID0gZmFsc2U7XG4gICAgaWYgKCF0aGlzLmFncmVlZCkge1xuICAgICAgdGhpcy5hZ3JlZWRFcnJvciA9IHRydWU7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcbiAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmNvbXBsZXRlU3RlcCh0aGlzLnN0ZXBJbnN0YW5jZS5pZCwgdGhpcy51c2VySWQpLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgICAgdGhpcy5zdGVwQ29tcGxldGVkTGlzdGVuZXIuZW1pdCh7XG4gICAgICAgIHZhbHVlOiB0aGlzLnN0ZXBJbnN0YW5jZSxcbiAgICAgICAgdXNlQnVzaW5lc3NQcm9jZXNzSWQ6IHRydWVcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgcmFkaW9CdXR0b25DaGFuZ2VkKCRzZWxlY3RlZCkge1xuICAgIHRoaXMuYWdyZWVkID0gJHNlbGVjdGVkLnZhbHVlO1xuICB9XG5cbiAgY2FuY2VsKCkge1xuICAgIHRoaXMuY2FuY2VsTGlzdGVuZXIuZW1pdCh7XG4gICAgfSk7XG4gIH1cbn1cbiJdfQ==