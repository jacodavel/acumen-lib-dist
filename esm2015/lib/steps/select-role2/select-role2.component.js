/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { StepInstance } from '../../domain/step-instance';
export class SelectRole2Component {
    /**
     * @param {?} projectService
     */
    constructor(projectService) {
        this.projectService = projectService;
        this.stepCompletedListener = new EventEmitter();
        this.bookedTimeSlots = [];
        this.newTimeSlots = [];
        this.showCalendar = false;
        this.showSelectedProviderError = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.stepInstance && this.stepInstance.step.parameters["0"]) {
            this.showWaitIndicator = true;
            /** @type {?} */
            let projectId = this.stepInstance.businessProcessInstance.businessProcess.project.id;
            /** @type {?} */
            let projectRoleId = this.stepInstance.step.parameters["0"].parameterValue;
            this.projectService.getProjectActors2(0, 100, projectId, projectRoleId).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                this.tableDataPage = result;
                this.projectService.getProcessVariableValue(this.stepInstance.businessProcessInstance.id, "providerId").then((/**
                 * @param {?} providerId
                 * @return {?}
                 */
                providerId => {
                    if (providerId) {
                        for (let i = 0; i < this.tableDataPage.list.length; i++) {
                            if (Number(providerId) === this.tableDataPage.list[i].actor.id) {
                                this.provider = this.tableDataPage.list[i].actor;
                                break;
                            }
                        }
                    }
                    this.showWaitIndicator = false;
                }));
            }));
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @param {?} provider
     * @return {?}
     */
    selectProvider(provider) {
        if (provider) {
            this.provider = provider.actor;
        }
        else {
            this.provider = null;
        }
    }
    /**
     * @return {?}
     */
    continueWithSelectedProvider() {
        this.showSelectedProviderError = false;
        if (!this.provider) {
            this.showSelectedProviderError = true;
            return;
        }
        this.showWaitIndicator = true;
        this.projectService.setProcessVariableValue(this.stepInstance.businessProcessInstance.id, "providerId", this.provider.id.toString()).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            if (this.makeBooking) {
                this.projectService.getTimeSlots(null, this.provider.id, null).then((/**
                 * @param {?} result
                 * @return {?}
                 */
                result => {
                    this.bookedTimeSlots = result;
                    this.showCalendar = true;
                    this.showWaitIndicator = false;
                }));
            }
            else {
                this.completeStep();
            }
        }));
    }
    /**
     * @param {?} event
     * @return {?}
     */
    timeSlotSelected(event) {
        this.newTimeSlots = event.value;
    }
    /**
     * @return {?}
     */
    updateTimeSlots() {
        if (this.newTimeSlots) {
            this.showWaitIndicator = true;
            this.projectService.updateTimeSlots(this.newTimeSlots).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                this.completeStep();
            }));
        }
    }
    /**
     * @private
     * @return {?}
     */
    completeStep() {
        this.showWaitIndicator = true;
        this.projectService.completeStep(this.stepInstance.id, this.userId).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            this.stepCompletedListener.emit({
                value: this.stepInstance
            });
            this.showWaitIndicator = false;
        }));
    }
}
SelectRole2Component.decorators = [
    { type: Component, args: [{
                selector: 'select-role2',
                template: "<div class=\"select-role2\">\n  <loading-indicator [show]=\"showWaitIndicator\"></loading-indicator>\n  <div *ngIf=\"!showCalendar\"class=\"heading\">\n    Select Provider\n  </div>\n  <div *ngIf=\"showCalendar\"class=\"heading\">\n    Make Appointment with {{ provider.title.description }} {{ provider.firstname }} {{ provider.surname }}\n  </div>\n\n  <div *ngIf=\"!showCalendar\" class=\"content\">\n    <div class=\"col-md-12\">\n      <table class=\"provider-table\">\n        <tr>\n          <th>Title</th>\n          <th>Firstname</th>\n          <th>Surname</th>\n          <th>E-mail</th>\n          <th>Contact Nrs</th>\n          <th>Address</th>\n        </tr>\n        <tbody *ngIf=\"tableDataPage\">\n          <tr *ngFor=\"let projectActor of tableDataPage.list\" (click)=\"selectProvider(projectActor)\">\n            <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n              <span *ngIf=\"projectActor.actor.title\">{{ projectActor.actor.title[\"description\"] }}</span>\n            </td>\n            <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n              {{ projectActor.actor.firstname }}\n            </td>\n            <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n              {{ projectActor.actor.surname }}\n            </td>\n            <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n              {{ projectActor.actor.email }}\n            </td>\n            <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n              <div *ngIf=\"projectActor.actor.celNr\">Cel: {{ projectActor.actor.celNr }}</div>\n              <div *ngIf=\"projectActor.actor.homeTelNr\">Home: {{ projectActor.actor.homeTelNr }}</div>\n              <div *ngIf=\"projectActor.actor.workTelNr\">Work: {{ projectActor.actor.workTelNr }}</div>\n            </td>\n            <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n              <div *ngIf=\"projectActor.actor.physicalAddress1\">{{ projectActor.actor.physicalAddress1 }}</div>\n              <div *ngIf=\"projectActor.actor.physicalAddress2\">{{ projectActor.actor.physicalAddress2 }}</div>\n              <div *ngIf=\"projectActor.actor.physicalAddress3\">{{ projectActor.actor.physicalAddress3 }}</div>\n              <div *ngIf=\"projectActor.actor.physicalAddress4\">{{ projectActor.actor.physicalAddress4 }}</div>\n            </td>\n          </tr>\n        </tbody>\n      </table>\n    </div>\n  </div>\n  <div *ngIf=\"!showCalendar\" class=\"button-section\">\n    <div *ngIf=\"provider\" class=\"button\" (click)=\"continueWithSelectedProvider()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div *ngIf=\"!provider\" class=\"button button-disabled\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n  </div>\n\n  <div *ngIf=\"showCalendar\" class=\"content\">\n    Calendar\n  </div>\n  <div *ngIf=\"showCalendar\" class=\"button-section\">\n    <div *ngIf=\"provider\" class=\"button\" (click)=\"updateTimeSlots()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div *ngIf=\"!provider\" class=\"button button-disabled\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n  </div>\n</div>\n\n\n<!--\n<loading-indicator [show]=\"showWaitIndicator\"></loading-indicator>\n\n<div *ngIf=\"!showCalendar\" class=\"row\">\n    <div class=\"col-md-12\">\n        <h4 class=\"control-label\">\n            Select Provider\n        </h4>\n    </div>\n</div>\n\n<div *ngIf=\"!showCalendar\" class=\"row\">\n    <div class=\"col-md-12\">\n        <table class=\"table table-hover table-bordered\">\n            <tr>\n                <th>Title</th>\n                <th>Firstname</th>\n                <th>Surname</th>\n                <th>E-mail</th>\n                <th>Contact Nrs</th>\n                <th>Address</th>\n            </tr>\n            <tbody *ngIf=\"tableDataPage\">\n                <tr *ngFor=\"let projectActor of tableDataPage.list\" (click)=\"selectProvider(projectActor)\">\n                    <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n                        <span *ngIf=\"projectActor.actor.title\">{{ projectActor.actor.title[\"description\"] }}</span>\n                    </td>\n                    <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n                        {{ projectActor.actor.firstname }}\n                    </td>\n                    <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n                        {{ projectActor.actor.surname }}\n                    </td>\n                    <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n                        {{ projectActor.actor.email }}\n                    </td>\n                    <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n                        <div *ngIf=\"projectActor.actor.celNr\">Cel: {{ projectActor.actor.celNr }}</div>\n                        <div *ngIf=\"projectActor.actor.homeTelNr\">Home: {{ projectActor.actor.homeTelNr }}</div>\n                        <div *ngIf=\"projectActor.actor.workTelNr\">Work: {{ projectActor.actor.workTelNr }}</div>\n                    </td>\n                    <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n                        <div *ngIf=\"projectActor.actor.physicalAddress1\">{{ projectActor.actor.physicalAddress1 }}</div>\n                        <div *ngIf=\"projectActor.actor.physicalAddress2\">{{ projectActor.actor.physicalAddress2 }}</div>\n                        <div *ngIf=\"projectActor.actor.physicalAddress3\">{{ projectActor.actor.physicalAddress3 }}</div>\n                        <div *ngIf=\"projectActor.actor.physicalAddress4\">{{ projectActor.actor.physicalAddress4 }}</div>\n                    </td>\n                </tr>\n            </tbody>\n        </table>\n    </div>\n</div>\n\n<div *ngIf=\"!showCalendar && showSelectedProviderError\" class=\"row\">\n    <div class=\"col-md-12\">\n        <div class=\"alert alert-danger\" role=\"alert\">A provider must be selected</div>\n    </div>\n</div>\n\n<div *ngIf=\"!showCalendar\" class=\"row\">\n    <div class=\"col-md-12\">\n        <button type=\"button\" class=\"btn btn-default\" (click)=\"continueWithSelectedProvider()\"\n                [ngClass]=\"{ 'disabled': !provider }\">\n            Continue\n        </button>\n    </div>\n</div>\n\n<div *ngIf=\"showCalendar\" class=\"row\">\n    <div class=\"col-md-12\">\n        <h4 class=\"control-label\">\n            Make Appointment with {{ provider.title.description }} {{ provider.firstname }} {{ provider.surname }}\n        </h4>\n    </div>\n</div>\n\n<div *ngIf=\"provider && showCalendar\" class=\"row\">\n    <div class=\"col-md-12\">\n        <h4 class=\"control-label\">\n            <calendar-control [showAddButton]=\"true\" [provider]=\"provider\" [participant]=\"stepInstance.businessProcessInstance.actor\"\n                [appointments]=\"bookedTimeSlots\" (selectionListener)=\"timeSlotSelected($event)\"></calendar-control>\n        </h4>\n    </div>\n</div>\n\n<div *ngIf=\"provider && showCalendar\" class=\"row\">\n    <div class=\"col-md-12\">\n        <button type=\"button\" class=\"btn btn-default\" (click)=\"updateTimeSlots()\">\n            Continue\n        </button>\n    </div>\n</div> -->\n",
                styles: [".select-role2{width:100%;height:100%;position:relative}.select-role2 .heading{position:absolute;top:0;width:100%;height:32px;font-size:16px;padding:8px}.select-role2 .top-padding{padding-top:8px}.select-role2 .content{position:absolute;top:32px;left:0;bottom:50px;width:100%;overflow-y:scroll;padding:8px;box-sizing:border-box;background-color:#f9f9f9;font-size:15px}.select-role2 .content .wording{width:100%;font-size:15px;padding-bottom:5px}.select-role2 .content input{width:200;font-size:12px;border:1px solid #58666c;background-color:#fffcf6;box-sizing:border-box;padding:3px 5px;margin-bottom:5px}.select-role2 .error-text{font-style:italic;font-size:11px;color:#f04141}.select-role2 .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.select-role2 .button-section .button{width:120px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.select-role2 .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.select-role2 .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.select-role2 .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b;cursor:default}.select-role2 .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b;cursor:default}"]
            }] }
];
/** @nocollapse */
SelectRole2Component.ctorParameters = () => [
    { type: ProjectService }
];
SelectRole2Component.propDecorators = {
    makeBooking: [{ type: Input }],
    stepInstance: [{ type: Input }],
    userId: [{ type: Input }],
    stepCompletedListener: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    SelectRole2Component.prototype.makeBooking;
    /** @type {?} */
    SelectRole2Component.prototype.stepInstance;
    /** @type {?} */
    SelectRole2Component.prototype.userId;
    /** @type {?} */
    SelectRole2Component.prototype.stepCompletedListener;
    /** @type {?} */
    SelectRole2Component.prototype.showWaitIndicator;
    /** @type {?} */
    SelectRole2Component.prototype.projectRoles;
    /** @type {?} */
    SelectRole2Component.prototype.tableDataPage;
    /** @type {?} */
    SelectRole2Component.prototype.provider;
    /** @type {?} */
    SelectRole2Component.prototype.bookedTimeSlots;
    /** @type {?} */
    SelectRole2Component.prototype.newTimeSlots;
    /** @type {?} */
    SelectRole2Component.prototype.showCalendar;
    /** @type {?} */
    SelectRole2Component.prototype.showSelectedProviderError;
    /**
     * @type {?}
     * @private
     */
    SelectRole2Component.prototype.projectService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0LXJvbGUyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvc3RlcHMvc2VsZWN0LXJvbGUyL3NlbGVjdC1yb2xlMi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBcUIsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTFGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNoRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFZMUQsTUFBTSxPQUFPLG9CQUFvQjs7OztJQWMvQixZQUFvQixjQUE4QjtRQUE5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFWeEMsMEJBQXFCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUtyRCxvQkFBZSxHQUFlLEVBQUUsQ0FBQztRQUNqQyxpQkFBWSxHQUFlLEVBQUUsQ0FBQztRQUM5QixpQkFBWSxHQUFZLEtBQUssQ0FBQztRQUM5Qiw4QkFBeUIsR0FBWSxLQUFLLENBQUM7SUFHM0MsQ0FBQzs7OztJQUVELFFBQVE7UUFDTixJQUFJLElBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQy9ELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7O2dCQUMxQixTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyx1QkFBdUIsQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLEVBQUU7O2dCQUNoRixhQUFhLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLGNBQWM7WUFDekUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsR0FBRyxFQUFFLFNBQVMsRUFBRSxhQUFhLENBQUMsQ0FBQyxJQUFJOzs7O1lBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQ3BGLElBQUksQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDO2dCQUM1QixJQUFJLENBQUMsY0FBYyxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsdUJBQXVCLENBQUMsRUFBRSxFQUFFLFlBQVksQ0FBQyxDQUFDLElBQUk7Ozs7Z0JBQUMsVUFBVSxDQUFDLEVBQUU7b0JBQ3hILElBQUksVUFBVSxFQUFFO3dCQUNkLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7NEJBQ3ZELElBQUksTUFBTSxDQUFDLFVBQVUsQ0FBQyxLQUFLLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUU7Z0NBQzlELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO2dDQUNqRCxNQUFNOzZCQUNQO3lCQUNGO3FCQUNGO29CQUVELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7Z0JBQ2pDLENBQUMsRUFBQyxDQUFDO1lBQ0wsQ0FBQyxFQUFDLENBQUM7U0FDSjtJQUNILENBQUM7Ozs7SUFFRCxXQUFXO0lBQ1gsQ0FBQzs7Ozs7SUFFRCxjQUFjLENBQUMsUUFBc0I7UUFDbkMsSUFBSSxRQUFRLEVBQUU7WUFDWixJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUM7U0FDaEM7YUFBTTtZQUNMLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1NBQ3RCO0lBQ0gsQ0FBQzs7OztJQUVELDRCQUE0QjtRQUMxQixJQUFJLENBQUMseUJBQXlCLEdBQUcsS0FBSyxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2xCLElBQUksQ0FBQyx5QkFBeUIsR0FBRyxJQUFJLENBQUM7WUFDdEMsT0FBTztTQUNSO1FBRUQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztRQUM5QixJQUFJLENBQUMsY0FBYyxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsdUJBQXVCLENBQUMsRUFBRSxFQUFFLFlBQVksRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxNQUFNLENBQUMsRUFBRTtZQUNqSixJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7Z0JBQ3BCLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQyxJQUFJOzs7O2dCQUFDLE1BQU0sQ0FBQyxFQUFFO29CQUMzRSxJQUFJLENBQUMsZUFBZSxHQUFHLE1BQU0sQ0FBQztvQkFDOUIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7b0JBQ3pCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7Z0JBQ2pDLENBQUMsRUFBQyxDQUFDO2FBQ0o7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO2FBQ3JCO1FBQ0gsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUVELGdCQUFnQixDQUFDLEtBQUs7UUFDcEIsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO0lBQ2xDLENBQUM7Ozs7SUFFRCxlQUFlO1FBQ2IsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3JCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7WUFDOUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUk7Ozs7WUFBQyxNQUFNLENBQUMsRUFBRTtnQkFDbkUsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1lBQ3RCLENBQUMsRUFBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDOzs7OztJQUVPLFlBQVk7UUFDbEIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztRQUM5QixJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSTs7OztRQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQ2hGLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUM7Z0JBQzlCLEtBQUssRUFBRSxJQUFJLENBQUMsWUFBWTthQUN6QixDQUFDLENBQUM7WUFFSCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQ2pDLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7O1lBbkdGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsY0FBYztnQkFDeEIsc2dQQUE0Qzs7YUFFN0M7Ozs7WUFaUSxjQUFjOzs7MEJBY3BCLEtBQUs7MkJBQ0wsS0FBSztxQkFDTCxLQUFLO29DQUNMLE1BQU07Ozs7SUFIUCwyQ0FBOEI7O0lBQzlCLDRDQUFvQzs7SUFDcEMsc0NBQXdCOztJQUN4QixxREFBcUQ7O0lBQ3JELGlEQUEyQjs7SUFDM0IsNENBQTRCOztJQUM1Qiw2Q0FBNkI7O0lBQzdCLHdDQUFnQjs7SUFDaEIsK0NBQWlDOztJQUNqQyw0Q0FBOEI7O0lBQzlCLDRDQUE4Qjs7SUFDOUIseURBQTJDOzs7OztJQUUvQiw4Q0FBc0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE9uSW5pdCwgT25EZXN0cm95LCBPdXRwdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgUHJvamVjdFNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9wcm9qZWN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgU3RlcEluc3RhbmNlIH0gZnJvbSAnLi4vLi4vZG9tYWluL3N0ZXAtaW5zdGFuY2UnO1xuaW1wb3J0IHsgUHJvamVjdFJvbGUgfSBmcm9tICcuLi8uLi9kb21haW4vcHJvamVjdC1yb2xlJztcbmltcG9ydCB7IFByb2plY3RBY3RvciB9IGZyb20gJy4uLy4uL2RvbWFpbi9wcm9qZWN0LWFjdG9yJztcbmltcG9ydCB7IEFjdG9yIH0gZnJvbSAnLi4vLi4vZG9tYWluL2FjdG9yJztcbmltcG9ydCB7IFRhYmxlRGF0YVBhZ2UgfSBmcm9tICcuLi8uLi9kb21haW4vdGFibGUtZGF0YS1wYWdlJztcbmltcG9ydCB7IFRpbWVTbG90IH0gZnJvbSAnLi4vLi4vZG9tYWluL3RpbWUtc2xvdCc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3NlbGVjdC1yb2xlMicsXG4gIHRlbXBsYXRlVXJsOiAnLi9zZWxlY3Qtcm9sZTIuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9zZWxlY3Qtcm9sZTIuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBTZWxlY3RSb2xlMkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgQElucHV0KCkgbWFrZUJvb2tpbmc6IGJvb2xlYW47XG4gIEBJbnB1dCgpIHN0ZXBJbnN0YW5jZTogU3RlcEluc3RhbmNlO1xuICBASW5wdXQoKSB1c2VySWQ6IG51bWJlcjtcbiAgQE91dHB1dCgpIHN0ZXBDb21wbGV0ZWRMaXN0ZW5lciA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgc2hvd1dhaXRJbmRpY2F0b3I6IGJvb2xlYW47XG4gIHByb2plY3RSb2xlczogUHJvamVjdFJvbGVbXTtcbiAgdGFibGVEYXRhUGFnZTogVGFibGVEYXRhUGFnZTtcbiAgcHJvdmlkZXI6IEFjdG9yO1xuICBib29rZWRUaW1lU2xvdHM6IFRpbWVTbG90W10gPSBbXTtcbiAgbmV3VGltZVNsb3RzOiBUaW1lU2xvdFtdID0gW107XG4gIHNob3dDYWxlbmRhcjogYm9vbGVhbiA9IGZhbHNlO1xuICBzaG93U2VsZWN0ZWRQcm92aWRlckVycm9yOiBib29sZWFuID0gZmFsc2U7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBwcm9qZWN0U2VydmljZTogUHJvamVjdFNlcnZpY2UpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIGlmICh0aGlzLnN0ZXBJbnN0YW5jZSAmJiB0aGlzLnN0ZXBJbnN0YW5jZS5zdGVwLnBhcmFtZXRlcnNbXCIwXCJdKSB7XG4gICAgICB0aGlzLnNob3dXYWl0SW5kaWNhdG9yID0gdHJ1ZTtcbiAgICAgIGxldCBwcm9qZWN0SWQgPSB0aGlzLnN0ZXBJbnN0YW5jZS5idXNpbmVzc1Byb2Nlc3NJbnN0YW5jZS5idXNpbmVzc1Byb2Nlc3MucHJvamVjdC5pZDtcbiAgICAgIGxldCBwcm9qZWN0Um9sZUlkID0gdGhpcy5zdGVwSW5zdGFuY2Uuc3RlcC5wYXJhbWV0ZXJzW1wiMFwiXS5wYXJhbWV0ZXJWYWx1ZTtcbiAgICAgIHRoaXMucHJvamVjdFNlcnZpY2UuZ2V0UHJvamVjdEFjdG9yczIoMCwgMTAwLCBwcm9qZWN0SWQsIHByb2plY3RSb2xlSWQpLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgICAgdGhpcy50YWJsZURhdGFQYWdlID0gcmVzdWx0O1xuICAgICAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmdldFByb2Nlc3NWYXJpYWJsZVZhbHVlKHRoaXMuc3RlcEluc3RhbmNlLmJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlLmlkLCBcInByb3ZpZGVySWRcIikudGhlbihwcm92aWRlcklkID0+IHtcbiAgICAgICAgICBpZiAocHJvdmlkZXJJZCkge1xuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLnRhYmxlRGF0YVBhZ2UubGlzdC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICBpZiAoTnVtYmVyKHByb3ZpZGVySWQpID09PSB0aGlzLnRhYmxlRGF0YVBhZ2UubGlzdFtpXS5hY3Rvci5pZCkge1xuICAgICAgICAgICAgICAgIHRoaXMucHJvdmlkZXIgPSB0aGlzLnRhYmxlRGF0YVBhZ2UubGlzdFtpXS5hY3RvcjtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cblxuICAgICAgICAgIHRoaXMuc2hvd1dhaXRJbmRpY2F0b3IgPSBmYWxzZTtcbiAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICBuZ09uRGVzdHJveSgpIHtcbiAgfVxuXG4gIHNlbGVjdFByb3ZpZGVyKHByb3ZpZGVyOiBQcm9qZWN0QWN0b3IpIHtcbiAgICBpZiAocHJvdmlkZXIpIHtcbiAgICAgIHRoaXMucHJvdmlkZXIgPSBwcm92aWRlci5hY3RvcjtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5wcm92aWRlciA9IG51bGw7XG4gICAgfVxuICB9XG5cbiAgY29udGludWVXaXRoU2VsZWN0ZWRQcm92aWRlcigpIHtcbiAgICB0aGlzLnNob3dTZWxlY3RlZFByb3ZpZGVyRXJyb3IgPSBmYWxzZTtcbiAgICBpZiAoIXRoaXMucHJvdmlkZXIpIHtcbiAgICAgIHRoaXMuc2hvd1NlbGVjdGVkUHJvdmlkZXJFcnJvciA9IHRydWU7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy5zaG93V2FpdEluZGljYXRvciA9IHRydWU7XG4gICAgdGhpcy5wcm9qZWN0U2VydmljZS5zZXRQcm9jZXNzVmFyaWFibGVWYWx1ZSh0aGlzLnN0ZXBJbnN0YW5jZS5idXNpbmVzc1Byb2Nlc3NJbnN0YW5jZS5pZCwgXCJwcm92aWRlcklkXCIsIHRoaXMucHJvdmlkZXIuaWQudG9TdHJpbmcoKSkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgaWYgKHRoaXMubWFrZUJvb2tpbmcpIHtcbiAgICAgICAgdGhpcy5wcm9qZWN0U2VydmljZS5nZXRUaW1lU2xvdHMobnVsbCwgdGhpcy5wcm92aWRlci5pZCwgbnVsbCkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICAgIHRoaXMuYm9va2VkVGltZVNsb3RzID0gcmVzdWx0O1xuICAgICAgICAgIHRoaXMuc2hvd0NhbGVuZGFyID0gdHJ1ZTtcbiAgICAgICAgICB0aGlzLnNob3dXYWl0SW5kaWNhdG9yID0gZmFsc2U7XG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5jb21wbGV0ZVN0ZXAoKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHRpbWVTbG90U2VsZWN0ZWQoZXZlbnQpIHtcbiAgICB0aGlzLm5ld1RpbWVTbG90cyA9IGV2ZW50LnZhbHVlO1xuICB9XG5cbiAgdXBkYXRlVGltZVNsb3RzKCkge1xuICAgIGlmICh0aGlzLm5ld1RpbWVTbG90cykge1xuICAgICAgdGhpcy5zaG93V2FpdEluZGljYXRvciA9IHRydWU7XG4gICAgICB0aGlzLnByb2plY3RTZXJ2aWNlLnVwZGF0ZVRpbWVTbG90cyh0aGlzLm5ld1RpbWVTbG90cykudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICB0aGlzLmNvbXBsZXRlU3RlcCgpO1xuICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBjb21wbGV0ZVN0ZXAoKSB7XG4gICAgdGhpcy5zaG93V2FpdEluZGljYXRvciA9IHRydWU7XG4gICAgdGhpcy5wcm9qZWN0U2VydmljZS5jb21wbGV0ZVN0ZXAodGhpcy5zdGVwSW5zdGFuY2UuaWQsIHRoaXMudXNlcklkKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICB0aGlzLnN0ZXBDb21wbGV0ZWRMaXN0ZW5lci5lbWl0KHtcbiAgICAgICAgdmFsdWU6IHRoaXMuc3RlcEluc3RhbmNlXG4gICAgICB9KTtcblxuICAgICAgdGhpcy5zaG93V2FpdEluZGljYXRvciA9IGZhbHNlO1xuICAgIH0pO1xuICB9XG59XG4iXX0=