/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { StepInstance } from '../../domain/step-instance';
export class FacialAuthenticationRefComponent {
    /**
     * @param {?} projectService
     */
    constructor(projectService) {
        this.projectService = projectService;
        this.onOpenCameraListener = new EventEmitter();
        this.stepCompletedListener = new EventEmitter();
        this.cancelListener = new EventEmitter();
        this.loading = false;
        this.imageContainer = {
            imageData: null,
            /**
             * @param {?} image
             * @return {?}
             */
            setImage(image) {
                console.log("facial-authentication-ref image received");
                this.imageData = image;
            }
        };
    }
    /**
     * @param {?} s
     * @return {?}
     */
    set stepInstance(s) {
        this._stepInstance = s;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (!this.uploadText) {
            this.uploadText = "Open camera";
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @return {?}
     */
    openCamera() {
        this.onOpenCameraListener.emit(this.imageContainer);
    }
    /**
     * @return {?}
     */
    retake() {
        this.imageContainer.imageData = null;
        this.onOpenCameraListener.emit(this.imageContainer);
    }
    /**
     * @return {?}
     */
    completeStep() {
        this.loading = true;
        console.log("completing facial-authentication-ref");
        this.projectService.updateFacialAuthenticationReference(this._stepInstance.id, this.imageContainer.imageData).then((/**
         * @param {?} imageUpdateResult
         * @return {?}
         */
        imageUpdateResult => {
            console.log("facial-authentication-ref update succeeded");
            this.projectService.completeStep(this._stepInstance.id, this.userId).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                console.log("facial-authentication-ref completion succeeded");
                this.loading = false;
                this.stepCompletedListener.emit({
                    value: this._stepInstance
                });
            })).catch((/**
             * @param {?} error
             * @return {?}
             */
            error => {
                console.log("facial-authentication-ref completion failed: " + error);
                this.loading = false;
                this.stepCompletedListener.emit({
                    value: this._stepInstance
                });
            }));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            console.log("facial-authentication-ref update failed: " + error);
        }));
    }
    /**
     * @return {?}
     */
    cancel() {
        this.cancelListener.emit({});
    }
}
FacialAuthenticationRefComponent.decorators = [
    { type: Component, args: [{
                selector: 'facial-authentication-ref',
                template: "<div class=\"facial-authentication-ref-step\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div *ngIf=\"!imageContainer.imageData\" class=\"message-content\">\n    Please provide a reference image that will be used for future facial authentication.\n  </div>\n  <div *ngIf=\"imageContainer.imageData\" class=\"image-content\">\n    <img src=\"{{ 'data:image/jpeg;base64,' + imageContainer.imageData }}\">\n  </div>\n\n  <div *ngIf=\"!loading\" class=\"button-section\">\n    <div *ngIf=\"!imageContainer.imageData\" class=\"button\" (click)=\"openCamera()\">\n      <div class=\"button-text\">{{ uploadText }}</div>\n    </div>\n    <div *ngIf=\"imageContainer.imageData\" class=\"button\" (click)=\"completeStep()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div *ngIf=\"imageContainer.imageData\" class=\"button button-outline\" (click)=\"retake()\">\n      <div class=\"button-text\">Retake</div>\n    </div>\n    <div class=\"button button-outline\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n  <div *ngIf=\"loading\" class=\"button-section\">\n    <div *ngIf=\"!imageContainer.imageData\" class=\"button button-disabled\">\n      <div class=\"button-text\">{{ uploadText }}</div>\n    </div>\n    <div *ngIf=\"imageContainer.imageData\" class=\"button button-disabled\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div *ngIf=\"imageContainer.imageData\" class=\"button button-outline-disabled\">\n      <div class=\"button-text\">Retake</div>\n    </div>\n    <div class=\"button button-outline-disabled\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                styles: [".facial-authentication-ref-step{width:100%;height:100%;position:relative}.facial-authentication-ref-step .message-content{position:absolute;top:0;left:0;bottom:50px;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px;box-sizing:border-box}.facial-authentication-ref-step .image-content{position:absolute;top:0;left:0;bottom:50px;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px;box-sizing:border-box;text-align:center}.facial-authentication-ref-step .image-content img{max-width:100%;max-height:100%}.facial-authentication-ref-step .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.facial-authentication-ref-step .button-section .button{width:100px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.facial-authentication-ref-step .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.facial-authentication-ref-step .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.facial-authentication-ref-step .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b;cursor:default}.facial-authentication-ref-step .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b;cursor:default}"]
            }] }
];
/** @nocollapse */
FacialAuthenticationRefComponent.ctorParameters = () => [
    { type: ProjectService }
];
FacialAuthenticationRefComponent.propDecorators = {
    _stepInstance: [{ type: Input }],
    userId: [{ type: Input }],
    uploadText: [{ type: Input }],
    onOpenCameraListener: [{ type: Output }],
    stepCompletedListener: [{ type: Output }],
    cancelListener: [{ type: Output }],
    stepInstance: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    FacialAuthenticationRefComponent.prototype._stepInstance;
    /** @type {?} */
    FacialAuthenticationRefComponent.prototype.userId;
    /** @type {?} */
    FacialAuthenticationRefComponent.prototype.uploadText;
    /** @type {?} */
    FacialAuthenticationRefComponent.prototype.onOpenCameraListener;
    /** @type {?} */
    FacialAuthenticationRefComponent.prototype.stepCompletedListener;
    /** @type {?} */
    FacialAuthenticationRefComponent.prototype.cancelListener;
    /** @type {?} */
    FacialAuthenticationRefComponent.prototype.loading;
    /** @type {?} */
    FacialAuthenticationRefComponent.prototype.imageContainer;
    /**
     * @type {?}
     * @private
     */
    FacialAuthenticationRefComponent.prototype.projectService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFjaWFsLWF1dGhlbnRpY2F0aW9uLXJlZi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL3N0ZXBzL2ZhY2lhbC1hdXRoZW50aWNhdGlvbi1yZWYvZmFjaWFsLWF1dGhlbnRpY2F0aW9uLXJlZi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBcUIsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTFGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNoRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFRMUQsTUFBTSxPQUFPLGdDQUFnQzs7OztJQXVCM0MsWUFBb0IsY0FBOEI7UUFBOUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBbkJ4Qyx5QkFBb0IsR0FBRyxJQUFJLFlBQVksRUFBa0IsQ0FBQztRQUMxRCwwQkFBcUIsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQzNDLG1CQUFjLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUM5QyxZQUFPLEdBQVksS0FBSyxDQUFDO1FBRXpCLG1CQUFjLEdBQW1CO1lBQy9CLFNBQVMsRUFBRSxJQUFJOzs7OztZQUVmLFFBQVEsQ0FBQyxLQUFhO2dCQUNwQixPQUFPLENBQUMsR0FBRyxDQUFDLDBDQUEwQyxDQUFDLENBQUM7Z0JBQ3hELElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1lBQ3pCLENBQUM7U0FDRixDQUFDO0lBUUYsQ0FBQzs7Ozs7SUFORCxJQUNJLFlBQVksQ0FBQyxDQUFlO1FBQzlCLElBQUksQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDO0lBQ3pCLENBQUM7Ozs7SUFLRCxRQUFRO1FBQ04sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDcEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxhQUFhLENBQUM7U0FDakM7SUFDSCxDQUFDOzs7O0lBRUQsV0FBVztJQUNYLENBQUM7Ozs7SUFFRCxVQUFVO1FBQ1IsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDdEQsQ0FBQzs7OztJQUVELE1BQU07UUFDSixJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDckMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDdEQsQ0FBQzs7OztJQUVELFlBQVk7UUFDVixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNwQixPQUFPLENBQUMsR0FBRyxDQUFDLHNDQUFzQyxDQUFDLENBQUM7UUFDcEQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxtQ0FBbUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxpQkFBaUIsQ0FBQyxFQUFFO1lBQ3JJLE9BQU8sQ0FBQyxHQUFHLENBQUMsNENBQTRDLENBQUMsQ0FBQztZQUMxRCxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSTs7OztZQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUNqRixPQUFPLENBQUMsR0FBRyxDQUFDLGdEQUFnRCxDQUFDLENBQUM7Z0JBQzlELElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO2dCQUNyQixJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDO29CQUM5QixLQUFLLEVBQUUsSUFBSSxDQUFDLGFBQWE7aUJBQzFCLENBQUMsQ0FBQztZQUNMLENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7WUFBQyxLQUFLLENBQUMsRUFBRTtnQkFDZixPQUFPLENBQUMsR0FBRyxDQUFDLCtDQUErQyxHQUFHLEtBQUssQ0FBQyxDQUFDO2dCQUNyRSxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDckIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQztvQkFDOUIsS0FBSyxFQUFFLElBQUksQ0FBQyxhQUFhO2lCQUMxQixDQUFDLENBQUM7WUFDTCxDQUFDLEVBQUMsQ0FBQztRQUNMLENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxLQUFLLENBQUMsRUFBRTtZQUNmLE9BQU8sQ0FBQyxHQUFHLENBQUMsMkNBQTJDLEdBQUcsS0FBSyxDQUFDLENBQUM7UUFDbkUsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7O0lBRUQsTUFBTTtRQUNKLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQ3hCLENBQUMsQ0FBQztJQUNMLENBQUM7OztZQTNFRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLDJCQUEyQjtnQkFDckMsd3JEQUF5RDs7YUFFMUQ7Ozs7WUFSUSxjQUFjOzs7NEJBVXBCLEtBQUs7cUJBQ0wsS0FBSzt5QkFDTCxLQUFLO21DQUNMLE1BQU07b0NBQ04sTUFBTTs2QkFDTixNQUFNOzJCQVlOLEtBQUs7Ozs7SUFqQk4seURBQXFDOztJQUNyQyxrREFBd0I7O0lBQ3hCLHNEQUE0Qjs7SUFDNUIsZ0VBQW9FOztJQUNwRSxpRUFBcUQ7O0lBQ3JELDBEQUE4Qzs7SUFDOUMsbURBQXlCOztJQUV6QiwwREFPRTs7Ozs7SUFPVSwwREFBc0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE9uSW5pdCwgT25EZXN0cm95LCBPdXRwdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgUHJvamVjdFNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9wcm9qZWN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgU3RlcEluc3RhbmNlIH0gZnJvbSAnLi4vLi4vZG9tYWluL3N0ZXAtaW5zdGFuY2UnO1xuaW1wb3J0IHsgSW1hZ2VDb250YWluZXIgfSBmcm9tICcuLi8uLi9kb21haW4vaW1hZ2UtY29udGFpbmVyJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZmFjaWFsLWF1dGhlbnRpY2F0aW9uLXJlZicsXG4gIHRlbXBsYXRlVXJsOiAnLi9mYWNpYWwtYXV0aGVudGljYXRpb24tcmVmLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vZmFjaWFsLWF1dGhlbnRpY2F0aW9uLXJlZi5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIEZhY2lhbEF1dGhlbnRpY2F0aW9uUmVmQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuICBASW5wdXQoKSBfc3RlcEluc3RhbmNlOiBTdGVwSW5zdGFuY2U7XG4gIEBJbnB1dCgpIHVzZXJJZDogbnVtYmVyO1xuICBASW5wdXQoKSB1cGxvYWRUZXh0OiBzdHJpbmc7XG4gIEBPdXRwdXQoKSBvbk9wZW5DYW1lcmFMaXN0ZW5lciA9IG5ldyBFdmVudEVtaXR0ZXI8SW1hZ2VDb250YWluZXI+KCk7XG4gIEBPdXRwdXQoKSBzdGVwQ29tcGxldGVkTGlzdGVuZXIgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBjYW5jZWxMaXN0ZW5lciA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgbG9hZGluZzogYm9vbGVhbiA9IGZhbHNlO1xuXG4gIGltYWdlQ29udGFpbmVyOiBJbWFnZUNvbnRhaW5lciA9IHtcbiAgICBpbWFnZURhdGE6IG51bGwsXG5cbiAgICBzZXRJbWFnZShpbWFnZTogc3RyaW5nKSB7XG4gICAgICBjb25zb2xlLmxvZyhcImZhY2lhbC1hdXRoZW50aWNhdGlvbi1yZWYgaW1hZ2UgcmVjZWl2ZWRcIik7XG4gICAgICB0aGlzLmltYWdlRGF0YSA9IGltYWdlO1xuICAgIH1cbiAgfTtcblxuICBASW5wdXQoKVxuICBzZXQgc3RlcEluc3RhbmNlKHM6IFN0ZXBJbnN0YW5jZSkge1xuICAgIHRoaXMuX3N0ZXBJbnN0YW5jZSA9IHM7XG4gIH1cblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHByb2plY3RTZXJ2aWNlOiBQcm9qZWN0U2VydmljZSkge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgaWYgKCF0aGlzLnVwbG9hZFRleHQpIHtcbiAgICAgIHRoaXMudXBsb2FkVGV4dCA9IFwiT3BlbiBjYW1lcmFcIjtcbiAgICB9XG4gIH1cblxuICBuZ09uRGVzdHJveSgpIHtcbiAgfVxuXG4gIG9wZW5DYW1lcmEoKSB7XG4gICAgdGhpcy5vbk9wZW5DYW1lcmFMaXN0ZW5lci5lbWl0KHRoaXMuaW1hZ2VDb250YWluZXIpO1xuICB9XG5cbiAgcmV0YWtlKCkge1xuICAgIHRoaXMuaW1hZ2VDb250YWluZXIuaW1hZ2VEYXRhID0gbnVsbDtcbiAgICB0aGlzLm9uT3BlbkNhbWVyYUxpc3RlbmVyLmVtaXQodGhpcy5pbWFnZUNvbnRhaW5lcik7XG4gIH1cblxuICBjb21wbGV0ZVN0ZXAoKSB7XG4gICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcbiAgICBjb25zb2xlLmxvZyhcImNvbXBsZXRpbmcgZmFjaWFsLWF1dGhlbnRpY2F0aW9uLXJlZlwiKTtcbiAgICB0aGlzLnByb2plY3RTZXJ2aWNlLnVwZGF0ZUZhY2lhbEF1dGhlbnRpY2F0aW9uUmVmZXJlbmNlKHRoaXMuX3N0ZXBJbnN0YW5jZS5pZCwgdGhpcy5pbWFnZUNvbnRhaW5lci5pbWFnZURhdGEpLnRoZW4oaW1hZ2VVcGRhdGVSZXN1bHQgPT4ge1xuICAgICAgY29uc29sZS5sb2coXCJmYWNpYWwtYXV0aGVudGljYXRpb24tcmVmIHVwZGF0ZSBzdWNjZWVkZWRcIik7XG4gICAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmNvbXBsZXRlU3RlcCh0aGlzLl9zdGVwSW5zdGFuY2UuaWQsIHRoaXMudXNlcklkKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiZmFjaWFsLWF1dGhlbnRpY2F0aW9uLXJlZiBjb21wbGV0aW9uIHN1Y2NlZWRlZFwiKTtcbiAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgIHRoaXMuc3RlcENvbXBsZXRlZExpc3RlbmVyLmVtaXQoe1xuICAgICAgICAgIHZhbHVlOiB0aGlzLl9zdGVwSW5zdGFuY2VcbiAgICAgICAgfSk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiZmFjaWFsLWF1dGhlbnRpY2F0aW9uLXJlZiBjb21wbGV0aW9uIGZhaWxlZDogXCIgKyBlcnJvcik7XG4gICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICB0aGlzLnN0ZXBDb21wbGV0ZWRMaXN0ZW5lci5lbWl0KHtcbiAgICAgICAgICB2YWx1ZTogdGhpcy5fc3RlcEluc3RhbmNlXG4gICAgICAgIH0pO1xuICAgICAgfSk7XG4gICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgY29uc29sZS5sb2coXCJmYWNpYWwtYXV0aGVudGljYXRpb24tcmVmIHVwZGF0ZSBmYWlsZWQ6IFwiICsgZXJyb3IpO1xuICAgIH0pO1xuICB9XG5cbiAgY2FuY2VsKCkge1xuICAgIHRoaXMuY2FuY2VsTGlzdGVuZXIuZW1pdCh7XG4gICAgfSk7XG4gIH1cbn1cbiJdfQ==