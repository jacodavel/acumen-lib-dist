/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, EventEmitter, Output } from '@angular/core';
import { QuestionLine } from '../../domain/question-line';
import { SelectQuestionOption } from '../../domain/select-question-option';
export class QuestionLineComponent {
    constructor() {
        this.onTakePictureQuestionLine = new EventEmitter();
        this.onTakeSupportingPictureQuestionLine = new EventEmitter();
        this.onQuestionAnswered = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (this.questionLine && this.questionLine.children) {
            for (let i = 0; i < this.questionLine.children.length; i++) {
                this.questionLine.children[i].parent = this.questionLine;
            }
            this.selectQuestionOptions = new Array();
            if (this.questionLine.parameters) {
                /** @type {?} */
                let rows = this.questionLine.parameters.split("|");
                /** @type {?} */
                let index = 0;
                for (let i = 0; i < rows.length; i++) {
                    /** @type {?} */
                    let s = rows[i].split("=");
                    if (s.length >= 2) {
                        /** @type {?} */
                        let selectQuestionOption = new SelectQuestionOption();
                        selectQuestionOption.description = s[1];
                        selectQuestionOption.weight = Number(s[2]);
                        selectQuestionOption.index = index;
                        this.selectQuestionOptions.push(selectQuestionOption);
                        index++;
                    }
                }
            }
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @param {?} $selected
     * @return {?}
     */
    radioButtonChanged($selected) {
        if (this.questionLine) {
            for (let i = 0; i < this.questionLine.parent.children.length; i++) {
                if (this.questionLine.parent.children[i].id !== this.questionLine.id) {
                    this.questionLine.parent.children[i].questionResponse.selected = false;
                }
            }
            if (!$selected) {
                $selected = {
                    value: false,
                };
            }
            this.questionLine.questionResponse.selected = $selected.value;
            this.questionLine.questionResponse.weight = this.questionLine.weight;
            if (this.questionLine.questionResponse.selected) {
                this.onQuestionAnswered.emit(this.questionLine);
            }
        }
    }
    /**
     * @param {?} $selected
     * @return {?}
     */
    checkboxChanged($selected) {
        if (!$selected) {
            $selected = {
                value: false,
            };
        }
        if (this.questionLine) {
            this.questionLine.questionResponse.selected = $selected.value;
            this.questionLine.questionResponse.weight = this.questionLine.weight;
            if (this.questionLine.questionResponse.selected) {
                this.onQuestionAnswered.emit(this.questionLine);
            }
        }
    }
    /**
     * @param {?} description
     * @return {?}
     */
    selectChanged(description) {
        if (this.questionLine) {
            /** @type {?} */
            let selectQuestionOption = null;
            for (let i = 0; i < this.selectQuestionOptions.length; i++) {
                if (this.selectQuestionOptions[i].description === description) {
                    selectQuestionOption = this.selectQuestionOptions[i];
                    break;
                }
            }
            // let selectQuestionOption = this.selectQuestionOptions[index];
            if (selectQuestionOption) {
                this.questionLine.questionResponse.selected = true;
                this.questionLine.questionResponse.weight = selectQuestionOption.weight;
                this.questionLine.questionResponse.response = selectQuestionOption.description;
                this.onQuestionAnswered.emit(this.questionLine);
            }
        }
    }
    /**
     * @param {?} questionLine
     * @return {?}
     */
    takePicture(questionLine) {
        this.onTakePictureQuestionLine.emit(questionLine);
    }
    /**
     * @param {?} questionLine
     * @return {?}
     */
    takeSupportingPicture(questionLine) {
        this.onTakeSupportingPictureQuestionLine.emit(questionLine);
    }
    /**
     * @param {?} questionLine
     * @return {?}
     */
    questionAnswered(questionLine) {
        console.log("questionLine.questionAnswered: " + questionLine.wording + " " + questionLine.integrationIndicator + " " + questionLine.id);
        this.onQuestionAnswered.emit(questionLine);
    }
}
QuestionLineComponent.decorators = [
    { type: Component, args: [{
                selector: 'question-line',
                template: "<div class=\"question-line\">\n  <div *ngIf=\"questionLine && questionLine.answerType === 'NONE'\" class=\"statement\">\n    <div class=\"wording\" [ngClass]=\"{ 'heading': questionLine.lineType === 'HEADING' }\">\n      {{ questionLine.wording }}\n    </div>\n    <div [hidden]=\"!questionLine.hasError\" class=\"error-text\">\n      This question must be answered\n    </div>\n    <div *ngFor=\"let child of questionLine.children\" class=\"answer-question-lines\">\n      <question-line [questionLine]=\"child\" (onTakePictureQuestionLine)=\"takePicture(child)\" (onQuestionAnswered)=\"questionAnswered($event)\"\n          (onTakeSupportingPictureQuestionLine)=\"takeSupportingPicture(child)\"></question-line>\n    </div>\n\n    <div *ngIf=\"questionLine && questionLine.allowSupportingDescription\" class=\"control-question-line supporting-question-line\">\n      <div class=\"wording\">\n        Please provide motivation for your choice\n      </div>\n      <textarea *ngIf=\"questionLine.allowSupportingDescription\" rows=\"5\" [(ngModel)]=\"questionLine.questionResponse.supportingDescription\" placeholder=\"Type here\">\n      </textarea>\n    </div>\n\n    <div *ngIf=\"questionLine && questionLine.allowSupportingImage\" class=\"image-question-line supporting-question-line\">\n      <div class=\"row\">\n        <div class=\"wording\">\n          Please take a picture to support your choice\n        </div>\n        <div class=\"button\" (click)=\"takeSupportingPicture(questionLine)\">\n            <img src=\"data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAABBBJREFUeJzt289rXUUUB/BP0qZJTdriomjophWXamKh7lyoCyGtWQiKrlxYa924caFS+ie40UbrX+APxF1F0OIriFttGiu1JG0XQkEQbCyJUnNdzAuWmPdm7rv3zX3EfOGQx5t7zznf7503c2bmZkizmMY5jPZ4/7d4Fmu1ZZQZX6OoaC9mz7omPKE6+QI/Y2fm3GvBd+oRoMDLmXOvjKPqI1/ght7HkewYwvfqFaDA6zlJVMFz6idf4CbuycijJ+zAT/ojQIG38lHpDS/pH/kCv2FfmYR2VCRUBiP4DPf2McZu3ME3fYzRM17T36e/bsvYn4lTMnbjF3kEKPBOHlrpeEM+8gVWcCALswTswa/yClDgbA5yKTgtP/kCf+GBWHJDJYiMYG/7byoO4kslp6YacU5YLa50aL/TTYBhPI3n8TgOtb/bSvikU8OM/lZsg2KtjevoEbyLkwnqbQncLcAIPsexhnJpBHf/ps/4n5HnXwFmcaLJRJrCsLAgGrjSMReGhaf/YNOJNIVhvJBw3SVhH2+8bcfwY825LONjvILHhBXdaNv2t787Iczdf9QZ+Kbuc+U8Jja5bw8WIvem2FUcV247a1wQY7Fi7JaEi2a6JPJMheAreFO50nojduFtrPZTgG5PZrzHwIuYqkB8Ix7FtR7yaEm4qJsAEz0Encf91Tn/B5PCWFW7AEe7BJ0tGXAR99XBtgMmlesJLQkXLQgD3kbsxeUSwVbU2+074bD0MaEl8cIFYcAbF7r9rHLkC2HAy4VTiTm1lCBQxa5KH+2n8AGu4M+2XcH7eCTRxy4sDZIAxxOSHsOHwssOnfysYU7aQeirgyLALfEiZ0w4zEj1eb59TzdMCBVj4wJ8FEmU8OTL+p1L8PvpIAgQ6/5Tunf7TraGhyO+Yz+DVo5NzouR9pPK7U6vY0ggWCV2ll3ea5H2Jyv4firSvhRzkEOAW5H2gxV8H4q0/x5zsNX2+UsjhwB7I+3XK/iOdfHoiVQOAWLd9HwF37F7o2eDOQSYjrSfFaakslgT6ocqsbMIEBup54XavyzmhEValdjofyG0LKwiu2FU6M6pPr8SFjzdMIHbET+tHAIU0g5dxoSn2q0q/Fs4u4yRJ+2dpGwCLCYmTShvzwin06ttu4z38FCij1FhdhkYAQph9zYXUt9KaSlBoKqtCru3/cYRYRNl4AQohHXBZL+YC2+G3SiRT0ufCW9ml/RHhAPCcV2ZXBoRYL0nHK6R/BHlnnzjAhTCmHBK+uywGUaFAS/1Nz9QAqzbkrCxsdkBbCdMCPP89YqxW0PtD4OA2/hCqAgvCsKsr+f3CQubaaG8nVHPP0dcGCQBmsCF7Q2RphNoGtsCNJ1A09gWoOkEmsZOYa1epgjZSvjhH9khIZLIyIO8AAAAAElFTkSuQmCC\">\n        </div>\n      </div>\n      <div class=\"image-answer\">\n        <img src=\"{{ questionLine.questionResponse.supportingImage }}\">\n      </div>\n    </div>\n  </div>\n\n  <div *ngIf=\"questionLine && questionLine.answerType === 'RADIO_BUTTON'\" class=\"select-question-line\">\n    <radio-button-component [description]=\"questionLine.wording\" [selected]=\"questionLine.questionResponse.selected\"\n        (selectionListener)=\"radioButtonChanged($event)\"></radio-button-component>\n    <ng-container *ngIf=\"questionLine.questionResponse.selected && questionLine.children.length > 0\">\n      <div *ngFor=\"let child of questionLine.children\" class=\"sub-question-lines\">\n        <question-line [questionLine]=\"child\" (onQuestionAnswered)=\"questionAnswered($event)\"></question-line>\n      </div>\n    </ng-container>\n  </div>\n\n  <div *ngIf=\"questionLine && questionLine.answerType === 'CHECK_BOX'\" class=\"select-question-line\">\n    <checkbox-component [description]=\"questionLine.wording\" [selected]=\"questionLine.questionResponse.selected\"\n        (selectionListener)=\"checkboxChanged($event)\"></checkbox-component>\n    <ng-container *ngIf=\"questionLine.questionResponse.selected && questionLine.children.length > 0\">\n      <div *ngFor=\"let child of questionLine.children\" class=\"sub-question-lines\">\n        <question-line [questionLine]=\"child\"></question-line>\n      </div>\n    </ng-container>\n  </div>\n\n  <div *ngIf=\"questionLine && questionLine.answerType === 'SELECT'\" class=\"control-question-line\">\n    <div class=\"wording\">\n      {{ questionLine.wording }}\n    </div>\n    <div [hidden]=\"!questionLine.hasError\" class=\"error-text\">\n      This question must be answered\n    </div>\n    <select [(ngModel)]=\"questionLine.questionResponse.response\" (change)=\"selectChanged($event.target.value)\">\n      <option *ngFor=\"let selectQuestionOption of selectQuestionOptions\" [value]=\"selectQuestionOption.description\">\n        {{ selectQuestionOption.description }}\n      </option>\n    </select>\n  </div>\n\n  <div *ngIf=\"questionLine && questionLine.answerType === 'TEXT'\" class=\"control-question-line\">\n    <div class=\"wording\">\n      {{ questionLine.wording }}\n    </div>\n    <div [hidden]=\"!questionLine.hasError\" class=\"error-text\">\n      This question must be answered\n    </div>\n    <input type=\"text\" [(ngModel)]=\"questionLine.questionResponse.response\" placeholder=\"Type here\">\n  </div>\n\n  <div *ngIf=\"questionLine && questionLine.answerType === 'TEXT_AREA'\" class=\"control-question-line\">\n    <div class=\"wording\">\n      {{ questionLine.wording }}\n    </div>\n    <div [hidden]=\"!questionLine.hasError\" class=\"error-text\">\n      This question must be answered\n    </div>\n    <textarea rows=\"5\" [(ngModel)]=\"questionLine.questionResponse.response\" placeholder=\"Type here\">\n    </textarea>\n  </div>\n\n  <div *ngIf=\"questionLine && questionLine.answerType === 'NUMBER'\" class=\"control-question-line\">\n    <div class=\"wording\">\n      {{ questionLine.wording }}\n    </div>\n    <div [hidden]=\"!questionLine.hasError\" class=\"error-text\">\n      This question must be answered\n    </div>\n    <input type=\"number\" [(ngModel)]=\"questionLine.questionResponse.response\" placeholder=\"Type here\">\n  </div>\n\n  <div *ngIf=\"questionLine && questionLine.answerType === 'IMAGE'\" class=\"image-question-line\">\n    <div class=\"row\">\n      <div class=\"wording\">\n        {{ questionLine.wording }}\n      </div>\n      <div class=\"button\" (click)=\"takePicture(questionLine)\">\n          <img src=\"data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAABBBJREFUeJzt289rXUUUB/BP0qZJTdriomjophWXamKh7lyoCyGtWQiKrlxYa924caFS+ie40UbrX+APxF1F0OIriFttGiu1JG0XQkEQbCyJUnNdzAuWmPdm7rv3zX3EfOGQx5t7zznf7503c2bmZkizmMY5jPZ4/7d4Fmu1ZZQZX6OoaC9mz7omPKE6+QI/Y2fm3GvBd+oRoMDLmXOvjKPqI1/ght7HkewYwvfqFaDA6zlJVMFz6idf4CbuycijJ+zAT/ojQIG38lHpDS/pH/kCv2FfmYR2VCRUBiP4DPf2McZu3ME3fYzRM17T36e/bsvYn4lTMnbjF3kEKPBOHlrpeEM+8gVWcCALswTswa/yClDgbA5yKTgtP/kCf+GBWHJDJYiMYG/7byoO4kslp6YacU5YLa50aL/TTYBhPI3n8TgOtb/bSvikU8OM/lZsg2KtjevoEbyLkwnqbQncLcAIPsexhnJpBHf/ps/4n5HnXwFmcaLJRJrCsLAgGrjSMReGhaf/YNOJNIVhvJBw3SVhH2+8bcfwY825LONjvILHhBXdaNv2t787Iczdf9QZ+Kbuc+U8Jja5bw8WIvem2FUcV247a1wQY7Fi7JaEi2a6JPJMheAreFO50nojduFtrPZTgG5PZrzHwIuYqkB8Ix7FtR7yaEm4qJsAEz0Encf91Tn/B5PCWFW7AEe7BJ0tGXAR99XBtgMmlesJLQkXLQgD3kbsxeUSwVbU2+074bD0MaEl8cIFYcAbF7r9rHLkC2HAy4VTiTm1lCBQxa5KH+2n8AGu4M+2XcH7eCTRxy4sDZIAxxOSHsOHwssOnfysYU7aQeirgyLALfEiZ0w4zEj1eb59TzdMCBVj4wJ8FEmU8OTL+p1L8PvpIAgQ6/5Tunf7TraGhyO+Yz+DVo5NzouR9pPK7U6vY0ggWCV2ll3ea5H2Jyv4firSvhRzkEOAW5H2gxV8H4q0/x5zsNX2+UsjhwB7I+3XK/iOdfHoiVQOAWLd9HwF37F7o2eDOQSYjrSfFaakslgT6ocqsbMIEBup54XavyzmhEValdjofyG0LKwiu2FU6M6pPr8SFjzdMIHbET+tHAIU0g5dxoSn2q0q/Fs4u4yRJ+2dpGwCLCYmTShvzwin06ttu4z38FCij1FhdhkYAQph9zYXUt9KaSlBoKqtCru3/cYRYRNl4AQohHXBZL+YC2+G3SiRT0ufCW9ml/RHhAPCcV2ZXBoRYL0nHK6R/BHlnnzjAhTCmHBK+uywGUaFAS/1Nz9QAqzbkrCxsdkBbCdMCPP89YqxW0PtD4OA2/hCqAgvCsKsr+f3CQubaaG8nVHPP0dcGCQBmsCF7Q2RphNoGtsCNJ1A09gWoOkEmsZOYa1epgjZSvjhH9khIZLIyIO8AAAAAElFTkSuQmCC\">\n      </div>\n    </div>\n\n    <div class=\"image-answer\">\n      <img src=\"{{ questionLine.questionResponse.response }}\">\n    </div>\n  </div>\n</div>\n",
                styles: [".question-line .statement{width:100%}.question-line .statement .wording{font-size:15px}.question-line .statement .heading{font-size:15px;font-weight:700}.question-line .statement .error-text{font-style:italic;font-size:11px;color:#f04141}.question-line .statement .answer-question-lines{width:100%}.question-line .sub-question-lines{padding-left:32px}.question-line .control-question-line{width:100%}.question-line .control-question-line .wording{width:100%;font-size:15px;padding-bottom:5px}.question-line .control-question-line .error-text{font-style:italic;font-size:11px;color:#f04141;padding-bottom:5px}.question-line .control-question-line input,.question-line .control-question-line select{width:100%;font-size:13px;border:1px solid #58666c;background-color:#fffcf6;box-sizing:border-box;padding:3px 5px;border-radius:5px;height:30px}.question-line .control-question-line textarea{width:100%;font-size:13px;border:1px solid #58666c;background-color:#fffcf6;padding:3px 5px;resize:none;box-sizing:border-box;border-radius:5px}.question-line .supporting-question-line{padding-bottom:20px}.question-line .image-question-line{width:100%}.question-line .image-question-line .row{display:table;width:100%}.question-line .image-question-line .row .wording{display:table-cell;width:90%;font-size:15px;padding-top:7px;vertical-align:top}.question-line .image-question-line .row .button{display:table-cell;width:32px;padding-top:7px;text-align:right;cursor:pointer;box-sizing:border-box}.question-line .image-question-line .row .button img{border:1px solid #58666c;padding:5px;border-radius:5px;box-sizing:border-box;width:24px;height:24px}.question-line .image-question-line .image-answer{width:90%;height:90%;padding:0;box-sizing:border-box;text-align:center;margin-left:auto;margin-right:auto;margin-bottom:8px}.question-line .image-question-line .image-answer img{max-width:100%;max-height:100%}"]
            }] }
];
/** @nocollapse */
QuestionLineComponent.ctorParameters = () => [];
QuestionLineComponent.propDecorators = {
    questionLine: [{ type: Input }],
    onTakePictureQuestionLine: [{ type: Output }],
    onTakeSupportingPictureQuestionLine: [{ type: Output }],
    onQuestionAnswered: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    QuestionLineComponent.prototype.questionLine;
    /** @type {?} */
    QuestionLineComponent.prototype.onTakePictureQuestionLine;
    /** @type {?} */
    QuestionLineComponent.prototype.onTakeSupportingPictureQuestionLine;
    /** @type {?} */
    QuestionLineComponent.prototype.onQuestionAnswered;
    /** @type {?} */
    QuestionLineComponent.prototype.selectQuestionOptions;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicXVlc3Rpb24tbGluZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL3N0ZXBzL3F1ZXN0aW9ubmFpcmUvcXVlc3Rpb24tbGluZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQWdDLEtBQUssRUFBaUIsWUFBWSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUVwSCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDMUQsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFPM0UsTUFBTSxPQUFPLHFCQUFxQjtJQU9oQztRQUxVLDhCQUF5QixHQUFHLElBQUksWUFBWSxFQUFnQixDQUFDO1FBQzdELHdDQUFtQyxHQUFHLElBQUksWUFBWSxFQUFnQixDQUFDO1FBQ3ZFLHVCQUFrQixHQUFHLElBQUksWUFBWSxFQUFnQixDQUFDO0lBSWhFLENBQUM7Ozs7SUFFRCxRQUFRO0lBQ1IsQ0FBQzs7Ozs7SUFFRCxXQUFXLENBQUMsT0FBc0I7UUFDaEMsSUFBSSxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFO1lBQ25ELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQzFELElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO2FBQzFEO1lBRUQsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksS0FBSyxFQUF3QixDQUFDO1lBQy9ELElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLEVBQUU7O29CQUM1QixJQUFJLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQzs7b0JBQzlDLEtBQUssR0FBRyxDQUFDO2dCQUNiLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFOzt3QkFDaEMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO29CQUMxQixJQUFJLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFOzs0QkFDYixvQkFBb0IsR0FBRyxJQUFJLG9CQUFvQixFQUFFO3dCQUNyRCxvQkFBb0IsQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUN4QyxvQkFBb0IsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUMzQyxvQkFBb0IsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO3dCQUNuQyxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUM7d0JBQ3RELEtBQUssRUFBRSxDQUFDO3FCQUNUO2lCQUNGO2FBQ0Y7U0FDRjtJQUNILENBQUM7Ozs7SUFFRCxXQUFXO0lBQ1gsQ0FBQzs7Ozs7SUFFRCxrQkFBa0IsQ0FBQyxTQUFTO1FBQzFCLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNyQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDakUsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxFQUFFO29CQUNwRSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztpQkFDeEU7YUFDRjtZQUVELElBQUksQ0FBQyxTQUFTLEVBQUU7Z0JBQ2QsU0FBUyxHQUFHO29CQUNWLEtBQUssRUFBRSxLQUFLO2lCQUNiLENBQUM7YUFDSDtZQUVELElBQUksQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUM7WUFDOUQsSUFBSSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUM7WUFFckUsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRTtnQkFDL0MsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7YUFDakQ7U0FDRjtJQUNILENBQUM7Ozs7O0lBRUQsZUFBZSxDQUFDLFNBQVM7UUFDdkIsSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNkLFNBQVMsR0FBRztnQkFDVixLQUFLLEVBQUUsS0FBSzthQUNiLENBQUM7U0FDSDtRQUVELElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNyQixJQUFJLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDO1lBQzlELElBQUksQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDO1lBRXJFLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUU7Z0JBQy9DLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO2FBQ2pEO1NBQ0Y7SUFDSCxDQUFDOzs7OztJQUVELGFBQWEsQ0FBQyxXQUFXO1FBQ3ZCLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTs7Z0JBQ2pCLG9CQUFvQixHQUFHLElBQUk7WUFDL0IsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQzFELElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsS0FBSyxXQUFXLEVBQUU7b0JBQzdELG9CQUFvQixHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDckQsTUFBTTtpQkFDUDthQUNGO1lBRUQsZ0VBQWdFO1lBQ2hFLElBQUksb0JBQW9CLEVBQUU7Z0JBQ3hCLElBQUksQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztnQkFDbkQsSUFBSSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsb0JBQW9CLENBQUMsTUFBTSxDQUFDO2dCQUN4RSxJQUFJLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsR0FBRyxvQkFBb0IsQ0FBQyxXQUFXLENBQUM7Z0JBQy9FLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO2FBQ2pEO1NBQ0Y7SUFDSCxDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxZQUEwQjtRQUNwQyxJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ3BELENBQUM7Ozs7O0lBRUQscUJBQXFCLENBQUMsWUFBWTtRQUNoQyxJQUFJLENBQUMsbUNBQW1DLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQzlELENBQUM7Ozs7O0lBRUQsZ0JBQWdCLENBQUMsWUFBMEI7UUFDekMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQ0FBaUMsR0FBRyxZQUFZLENBQUMsT0FBTyxHQUFHLEdBQUcsR0FBRyxZQUFZLENBQUMsb0JBQW9CLEdBQUcsR0FBRyxHQUFHLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUN4SSxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQzdDLENBQUM7OztZQXJIRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGVBQWU7Z0JBQ3pCLDh1UUFBNkM7O2FBRTlDOzs7OzsyQkFFRSxLQUFLO3dDQUNMLE1BQU07a0RBQ04sTUFBTTtpQ0FDTixNQUFNOzs7O0lBSFAsNkNBQW9DOztJQUNwQywwREFBdUU7O0lBQ3ZFLG9FQUFpRjs7SUFDakYsbURBQWdFOztJQUNoRSxzREFBOEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgT25DaGFuZ2VzLCBPbkRlc3Ryb3ksIElucHV0LCBTaW1wbGVDaGFuZ2VzLCBFdmVudEVtaXR0ZXIsIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBRdWVzdGlvbkxpbmUgfSBmcm9tICcuLi8uLi9kb21haW4vcXVlc3Rpb24tbGluZSc7XG5pbXBvcnQgeyBTZWxlY3RRdWVzdGlvbk9wdGlvbiB9IGZyb20gJy4uLy4uL2RvbWFpbi9zZWxlY3QtcXVlc3Rpb24tb3B0aW9uJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAncXVlc3Rpb24tbGluZScsXG4gIHRlbXBsYXRlVXJsOiAnLi9xdWVzdGlvbi1saW5lLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vcXVlc3Rpb24tbGluZS5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIFF1ZXN0aW9uTGluZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95LCBPbkNoYW5nZXMge1xuICBASW5wdXQoKSBxdWVzdGlvbkxpbmU6IFF1ZXN0aW9uTGluZTtcbiAgQE91dHB1dCgpIG9uVGFrZVBpY3R1cmVRdWVzdGlvbkxpbmUgPSBuZXcgRXZlbnRFbWl0dGVyPFF1ZXN0aW9uTGluZT4oKTtcbiAgQE91dHB1dCgpIG9uVGFrZVN1cHBvcnRpbmdQaWN0dXJlUXVlc3Rpb25MaW5lID0gbmV3IEV2ZW50RW1pdHRlcjxRdWVzdGlvbkxpbmU+KCk7XG4gIEBPdXRwdXQoKSBvblF1ZXN0aW9uQW5zd2VyZWQgPSBuZXcgRXZlbnRFbWl0dGVyPFF1ZXN0aW9uTGluZT4oKTtcbiAgc2VsZWN0UXVlc3Rpb25PcHRpb25zOiBTZWxlY3RRdWVzdGlvbk9wdGlvbltdO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XG4gICAgaWYgKHRoaXMucXVlc3Rpb25MaW5lICYmIHRoaXMucXVlc3Rpb25MaW5lLmNoaWxkcmVuKSB7XG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMucXVlc3Rpb25MaW5lLmNoaWxkcmVuLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIHRoaXMucXVlc3Rpb25MaW5lLmNoaWxkcmVuW2ldLnBhcmVudCA9IHRoaXMucXVlc3Rpb25MaW5lO1xuICAgICAgfVxuXG4gICAgICB0aGlzLnNlbGVjdFF1ZXN0aW9uT3B0aW9ucyA9IG5ldyBBcnJheTxTZWxlY3RRdWVzdGlvbk9wdGlvbj4oKTtcbiAgICAgIGlmICh0aGlzLnF1ZXN0aW9uTGluZS5wYXJhbWV0ZXJzKSB7XG4gICAgICAgIGxldCByb3dzID0gdGhpcy5xdWVzdGlvbkxpbmUucGFyYW1ldGVycy5zcGxpdChcInxcIik7XG4gICAgICAgIGxldCBpbmRleCA9IDA7XG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcm93cy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgIGxldCBzID0gcm93c1tpXS5zcGxpdChcIj1cIik7XG4gICAgICAgICAgaWYgKHMubGVuZ3RoID49IDIpIHtcbiAgICAgICAgICAgIGxldCBzZWxlY3RRdWVzdGlvbk9wdGlvbiA9IG5ldyBTZWxlY3RRdWVzdGlvbk9wdGlvbigpO1xuICAgICAgICAgICAgc2VsZWN0UXVlc3Rpb25PcHRpb24uZGVzY3JpcHRpb24gPSBzWzFdO1xuICAgICAgICAgICAgc2VsZWN0UXVlc3Rpb25PcHRpb24ud2VpZ2h0ID0gTnVtYmVyKHNbMl0pO1xuICAgICAgICAgICAgc2VsZWN0UXVlc3Rpb25PcHRpb24uaW5kZXggPSBpbmRleDtcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0UXVlc3Rpb25PcHRpb25zLnB1c2goc2VsZWN0UXVlc3Rpb25PcHRpb24pO1xuICAgICAgICAgICAgaW5kZXgrKztcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBuZ09uRGVzdHJveSgpIHtcbiAgfVxuXG4gIHJhZGlvQnV0dG9uQ2hhbmdlZCgkc2VsZWN0ZWQpIHtcbiAgICBpZiAodGhpcy5xdWVzdGlvbkxpbmUpIHtcbiAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5xdWVzdGlvbkxpbmUucGFyZW50LmNoaWxkcmVuLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIGlmICh0aGlzLnF1ZXN0aW9uTGluZS5wYXJlbnQuY2hpbGRyZW5baV0uaWQgIT09IHRoaXMucXVlc3Rpb25MaW5lLmlkKSB7XG4gICAgICAgICAgdGhpcy5xdWVzdGlvbkxpbmUucGFyZW50LmNoaWxkcmVuW2ldLnF1ZXN0aW9uUmVzcG9uc2Uuc2VsZWN0ZWQgPSBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBpZiAoISRzZWxlY3RlZCkge1xuICAgICAgICAkc2VsZWN0ZWQgPSB7XG4gICAgICAgICAgdmFsdWU6IGZhbHNlLFxuICAgICAgICB9O1xuICAgICAgfVxuXG4gICAgICB0aGlzLnF1ZXN0aW9uTGluZS5xdWVzdGlvblJlc3BvbnNlLnNlbGVjdGVkID0gJHNlbGVjdGVkLnZhbHVlO1xuICAgICAgdGhpcy5xdWVzdGlvbkxpbmUucXVlc3Rpb25SZXNwb25zZS53ZWlnaHQgPSB0aGlzLnF1ZXN0aW9uTGluZS53ZWlnaHQ7XG5cbiAgICAgIGlmICh0aGlzLnF1ZXN0aW9uTGluZS5xdWVzdGlvblJlc3BvbnNlLnNlbGVjdGVkKSB7XG4gICAgICAgIHRoaXMub25RdWVzdGlvbkFuc3dlcmVkLmVtaXQodGhpcy5xdWVzdGlvbkxpbmUpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIGNoZWNrYm94Q2hhbmdlZCgkc2VsZWN0ZWQpIHtcbiAgICBpZiAoISRzZWxlY3RlZCkge1xuICAgICAgJHNlbGVjdGVkID0ge1xuICAgICAgICB2YWx1ZTogZmFsc2UsXG4gICAgICB9O1xuICAgIH1cblxuICAgIGlmICh0aGlzLnF1ZXN0aW9uTGluZSkge1xuICAgICAgdGhpcy5xdWVzdGlvbkxpbmUucXVlc3Rpb25SZXNwb25zZS5zZWxlY3RlZCA9ICRzZWxlY3RlZC52YWx1ZTtcbiAgICAgIHRoaXMucXVlc3Rpb25MaW5lLnF1ZXN0aW9uUmVzcG9uc2Uud2VpZ2h0ID0gdGhpcy5xdWVzdGlvbkxpbmUud2VpZ2h0O1xuXG4gICAgICBpZiAodGhpcy5xdWVzdGlvbkxpbmUucXVlc3Rpb25SZXNwb25zZS5zZWxlY3RlZCkge1xuICAgICAgICB0aGlzLm9uUXVlc3Rpb25BbnN3ZXJlZC5lbWl0KHRoaXMucXVlc3Rpb25MaW5lKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBzZWxlY3RDaGFuZ2VkKGRlc2NyaXB0aW9uKSB7XG4gICAgaWYgKHRoaXMucXVlc3Rpb25MaW5lKSB7XG4gICAgICBsZXQgc2VsZWN0UXVlc3Rpb25PcHRpb24gPSBudWxsO1xuICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLnNlbGVjdFF1ZXN0aW9uT3B0aW9ucy5sZW5ndGg7IGkrKykge1xuICAgICAgICBpZiAodGhpcy5zZWxlY3RRdWVzdGlvbk9wdGlvbnNbaV0uZGVzY3JpcHRpb24gPT09IGRlc2NyaXB0aW9uKSB7XG4gICAgICAgICAgc2VsZWN0UXVlc3Rpb25PcHRpb24gPSB0aGlzLnNlbGVjdFF1ZXN0aW9uT3B0aW9uc1tpXTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAvLyBsZXQgc2VsZWN0UXVlc3Rpb25PcHRpb24gPSB0aGlzLnNlbGVjdFF1ZXN0aW9uT3B0aW9uc1tpbmRleF07XG4gICAgICBpZiAoc2VsZWN0UXVlc3Rpb25PcHRpb24pIHtcbiAgICAgICAgdGhpcy5xdWVzdGlvbkxpbmUucXVlc3Rpb25SZXNwb25zZS5zZWxlY3RlZCA9IHRydWU7XG4gICAgICAgIHRoaXMucXVlc3Rpb25MaW5lLnF1ZXN0aW9uUmVzcG9uc2Uud2VpZ2h0ID0gc2VsZWN0UXVlc3Rpb25PcHRpb24ud2VpZ2h0O1xuICAgICAgICB0aGlzLnF1ZXN0aW9uTGluZS5xdWVzdGlvblJlc3BvbnNlLnJlc3BvbnNlID0gc2VsZWN0UXVlc3Rpb25PcHRpb24uZGVzY3JpcHRpb247XG4gICAgICAgIHRoaXMub25RdWVzdGlvbkFuc3dlcmVkLmVtaXQodGhpcy5xdWVzdGlvbkxpbmUpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHRha2VQaWN0dXJlKHF1ZXN0aW9uTGluZTogUXVlc3Rpb25MaW5lKSB7XG4gICAgdGhpcy5vblRha2VQaWN0dXJlUXVlc3Rpb25MaW5lLmVtaXQocXVlc3Rpb25MaW5lKTtcbiAgfVxuXG4gIHRha2VTdXBwb3J0aW5nUGljdHVyZShxdWVzdGlvbkxpbmUpIHtcbiAgICB0aGlzLm9uVGFrZVN1cHBvcnRpbmdQaWN0dXJlUXVlc3Rpb25MaW5lLmVtaXQocXVlc3Rpb25MaW5lKTtcbiAgfVxuXG4gIHF1ZXN0aW9uQW5zd2VyZWQocXVlc3Rpb25MaW5lOiBRdWVzdGlvbkxpbmUpIHtcbiAgICBjb25zb2xlLmxvZyhcInF1ZXN0aW9uTGluZS5xdWVzdGlvbkFuc3dlcmVkOiBcIiArIHF1ZXN0aW9uTGluZS53b3JkaW5nICsgXCIgXCIgKyBxdWVzdGlvbkxpbmUuaW50ZWdyYXRpb25JbmRpY2F0b3IgKyBcIiBcIiArIHF1ZXN0aW9uTGluZS5pZCk7XG4gICAgdGhpcy5vblF1ZXN0aW9uQW5zd2VyZWQuZW1pdChxdWVzdGlvbkxpbmUpO1xuICB9XG59XG4iXX0=