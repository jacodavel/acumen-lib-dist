/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
export class CheckboxComponent {
    constructor() {
        this.selectionListener = new EventEmitter();
        this.selected = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        // if (this.description) {
        //     if (this.description.toLowerCase() === "yes" || this.description.toLowerCase() === "no") {
        //         this.description = "";
        //     }
        // }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @return {?}
     */
    clicked() {
        this.selected = !this.selected;
        this.selectionListener.emit({
            value: this.selected
        });
    }
}
CheckboxComponent.decorators = [
    { type: Component, args: [{
                selector: 'checkbox-component',
                template: "<div class=\"checkbox-component\" (click)=\"clicked()\">\n  <div class=\"value\">\n    <div class=\"checkbox\">\n      <div *ngIf=\"selected\" class=\"checkbox-selected\">\n      </div>\n    </div>\n  </div>\n  <div class=\"description\">\n    {{ description }}\n  </div>\n</div>\n",
                styles: [".checkbox-component{display:table;margin:.5em 0;width:100%;cursor:pointer}.checkbox-component .value{display:table-cell;vertical-align:middle;text-align:left;width:32px;padding:3px}.checkbox-component .value .checkbox{border:1px solid #58666c;width:14px;height:14px;padding:2px;box-sizing:border-box}.checkbox-component .value .checkbox .checkbox-selected{background-color:#58666c;width:8px;height:8px;box-sizing:border-box}.checkbox-component .description{display:table-cell;vertical-align:middle;text-align:left;width:100%;font-size:15px;padding-left:5px;padding-top:3px;padding-right:5px;line-height:15px}"]
            }] }
];
/** @nocollapse */
CheckboxComponent.ctorParameters = () => [];
CheckboxComponent.propDecorators = {
    description: [{ type: Input }],
    selected: [{ type: Input }],
    selectionListener: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    CheckboxComponent.prototype.description;
    /** @type {?} */
    CheckboxComponent.prototype.selected;
    /** @type {?} */
    CheckboxComponent.prototype.selectionListener;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hlY2tib3gtY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWN1bWVuLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9zdGVwcy9xdWVzdGlvbm5haXJlL2NoZWNrYm94LWNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFxQixNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFPMUYsTUFBTSxPQUFPLGlCQUFpQjtJQUs1QjtRQUZVLHNCQUFpQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFHL0MsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7SUFDeEIsQ0FBQzs7OztJQUVELFFBQVE7UUFDTiwwQkFBMEI7UUFDMUIsaUdBQWlHO1FBQ2pHLGlDQUFpQztRQUNqQyxRQUFRO1FBQ1IsSUFBSTtJQUNOLENBQUM7Ozs7SUFFRCxXQUFXO0lBQ1gsQ0FBQzs7OztJQUVELE9BQU87UUFDTCxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUMvQixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDO1lBQ3hCLEtBQUssRUFBRSxJQUFJLENBQUMsUUFBUTtTQUN2QixDQUFDLENBQUM7SUFDTCxDQUFDOzs7WUE5QkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxvQkFBb0I7Z0JBQzlCLHFTQUF3Qzs7YUFFekM7Ozs7OzBCQUVFLEtBQUs7dUJBQ0wsS0FBSztnQ0FDTCxNQUFNOzs7O0lBRlAsd0NBQTZCOztJQUM3QixxQ0FBMkI7O0lBQzNCLDhDQUFpRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPbkRlc3Ryb3ksIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdjaGVja2JveC1jb21wb25lbnQnLFxuICB0ZW1wbGF0ZVVybDogJy4vY2hlY2tib3gtY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9jaGVja2JveC1jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIENoZWNrYm94Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuICBASW5wdXQoKSBkZXNjcmlwdGlvbjogc3RyaW5nO1xuICBASW5wdXQoKSBzZWxlY3RlZDogYm9vbGVhbjtcbiAgQE91dHB1dCgpIHNlbGVjdGlvbkxpc3RlbmVyID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMuc2VsZWN0ZWQgPSBmYWxzZTtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIC8vIGlmICh0aGlzLmRlc2NyaXB0aW9uKSB7XG4gICAgLy8gICAgIGlmICh0aGlzLmRlc2NyaXB0aW9uLnRvTG93ZXJDYXNlKCkgPT09IFwieWVzXCIgfHwgdGhpcy5kZXNjcmlwdGlvbi50b0xvd2VyQ2FzZSgpID09PSBcIm5vXCIpIHtcbiAgICAvLyAgICAgICAgIHRoaXMuZGVzY3JpcHRpb24gPSBcIlwiO1xuICAgIC8vICAgICB9XG4gICAgLy8gfVxuICB9XG5cbiAgbmdPbkRlc3Ryb3koKSB7XG4gIH1cblxuICBjbGlja2VkKCkge1xuICAgIHRoaXMuc2VsZWN0ZWQgPSAhdGhpcy5zZWxlY3RlZDtcbiAgICB0aGlzLnNlbGVjdGlvbkxpc3RlbmVyLmVtaXQoe1xuICAgICAgICB2YWx1ZTogdGhpcy5zZWxlY3RlZFxuICAgIH0pO1xuICB9XG59XG4iXX0=