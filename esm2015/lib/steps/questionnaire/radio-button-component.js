/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
export class RadioButtonComponent {
    constructor() {
        this.selectionListener = new EventEmitter();
        this.selected = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @return {?}
     */
    clicked() {
        this.selected = !this.selected;
        this.selectionListener.emit({
            value: this.selected
        });
    }
}
RadioButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'radio-button-component',
                template: "<div class=\"radio-button-component\" (click)=\"clicked()\">\n  <div class=\"value\">\n    <div class=\"radio-button\">\n      <div *ngIf=\"selected\" class=\"radio-button-selected\">\n      </div>\n    </div>\n  </div>\n  <div class=\"description\">\n    {{ description }}\n  </div>\n</div>\n",
                styles: [".radio-button-component{display:table;margin:.5em 0;width:100%;cursor:pointer}.radio-button-component .value{display:table-cell;vertical-align:top;text-align:left;width:32px;padding:3px}.radio-button-component .value .radio-button{border:1px solid #58666c;width:14px;height:14px;padding:2px;border-radius:7px;box-sizing:border-box}.radio-button-component .value .radio-button .radio-button-selected{background-color:#58666c;width:8px;height:8px;border-radius:4px;box-sizing:border-box}.radio-button-component .description{display:table-cell;vertical-align:top;text-align:left;width:100%;font-size:15px;padding-left:5px;padding-top:3px;padding-right:5px;line-height:15px}"]
            }] }
];
/** @nocollapse */
RadioButtonComponent.ctorParameters = () => [];
RadioButtonComponent.propDecorators = {
    description: [{ type: Input }],
    selected: [{ type: Input }],
    selectionListener: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    RadioButtonComponent.prototype.description;
    /** @type {?} */
    RadioButtonComponent.prototype.selected;
    /** @type {?} */
    RadioButtonComponent.prototype.selectionListener;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmFkaW8tYnV0dG9uLWNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvc3RlcHMvcXVlc3Rpb25uYWlyZS9yYWRpby1idXR0b24tY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQXFCLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQU8xRixNQUFNLE9BQU8sb0JBQW9CO0lBSy9CO1FBRlUsc0JBQWlCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUcvQyxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztJQUN4QixDQUFDOzs7O0lBRUQsUUFBUTtJQUNSLENBQUM7Ozs7SUFFRCxXQUFXO0lBQ1gsQ0FBQzs7OztJQUVELE9BQU87UUFDTCxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUMvQixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDO1lBQzFCLEtBQUssRUFBRSxJQUFJLENBQUMsUUFBUTtTQUNyQixDQUFDLENBQUM7SUFDTCxDQUFDOzs7WUF6QkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSx3QkFBd0I7Z0JBQ2xDLGlUQUE0Qzs7YUFFN0M7Ozs7OzBCQUVFLEtBQUs7dUJBQ0wsS0FBSztnQ0FDTCxNQUFNOzs7O0lBRlAsMkNBQTZCOztJQUM3Qix3Q0FBMkI7O0lBQzNCLGlEQUFpRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPbkRlc3Ryb3ksIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdyYWRpby1idXR0b24tY29tcG9uZW50JyxcbiAgdGVtcGxhdGVVcmw6ICcuL3JhZGlvLWJ1dHRvbi1jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL3JhZGlvLWJ1dHRvbi1jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIFJhZGlvQnV0dG9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuICBASW5wdXQoKSBkZXNjcmlwdGlvbjogc3RyaW5nO1xuICBASW5wdXQoKSBzZWxlY3RlZDogYm9vbGVhbjtcbiAgQE91dHB1dCgpIHNlbGVjdGlvbkxpc3RlbmVyID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMuc2VsZWN0ZWQgPSBmYWxzZTtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKSB7XG4gIH1cblxuICBjbGlja2VkKCkge1xuICAgIHRoaXMuc2VsZWN0ZWQgPSAhdGhpcy5zZWxlY3RlZDtcbiAgICB0aGlzLnNlbGVjdGlvbkxpc3RlbmVyLmVtaXQoe1xuICAgICAgdmFsdWU6IHRoaXMuc2VsZWN0ZWRcbiAgICB9KTtcbiAgfVxufVxuIl19