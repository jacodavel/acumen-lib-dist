/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { QuestionResponse } from '../../domain/question-response';
import { StepInstance } from '../../domain/step-instance';
export class QuestionnaireComponent {
    /**
     * @param {?} projectService
     */
    constructor(projectService) {
        this.projectService = projectService;
        this.stepCompletedListener = new EventEmitter();
        this.cancelListener = new EventEmitter();
        this.onTakePicture = new EventEmitter();
        this.onQuestionAnswered = new EventEmitter();
        this.pageIndex = -1;
        this.loading = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.loading = true;
        this.projectService.getQuestionnaire(this.stepInstance.id).then((/**
         * @param {?} questionnaire
         * @return {?}
         */
        (questionnaire) => {
            this.questionnaire = questionnaire;
            this.pageIndex = 0;
            this.loading = false;
        }));
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.stepInstance && changes.stepInstance.previousValue && changes.stepInstance.previousValue.id &&
            changes.stepInstance.currentValue.id !== changes.stepInstance.previousValue.id) {
            this.pageIndex = -1;
            this.loading = true;
            this.projectService.getQuestionnaire(this.stepInstance.id).then((/**
             * @param {?} questionnaire
             * @return {?}
             */
            (questionnaire) => {
                this.questionnaire = questionnaire;
                this.pageIndex = 0;
                this.loading = false;
            }));
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @return {?}
     */
    previous() {
        if (this.pageIndex > 0) {
            //            let questionLine = this.questionnaire.questionLines[this.pageIndex];
            try {
                //                this.setAllValidationMessages(questionLine);
                //                this.validateQuestionLine(questionLine);
                this.pageIndex--;
                document.body.scrollTop = 0;
            }
            catch (error) {
                // console.log(error);
            }
        }
    }
    /**
     * @return {?}
     */
    next() {
        if (this.pageIndex < this.questionnaire.questionLines.length - 1) {
            /** @type {?} */
            let questionLine = this.questionnaire.questionLines[this.pageIndex];
            try {
                this.setAllValidationMessages(questionLine);
                this.validateQuestionLine(questionLine);
                this.pageIndex++;
                document.body.scrollTop = 0;
            }
            catch (error) {
                // console.log(error);
            }
        }
    }
    /**
     * @param {?} submit
     * @return {?}
     */
    save(submit) {
        if (!this.loading) {
            this.loading = true;
            /** @type {?} */
            let responses = new Array();
            for (let i = 0; i < this.questionnaire.questionLines.length; i++) {
                this.addResponses(this.questionnaire.questionLines[i], responses);
            }
            this.projectService.updateQuestionnaire(this.stepInstance.id, responses, submit, this.userId).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                if (submit) {
                    this.pageIndex = 0;
                }
                this.stepCompletedListener.emit({
                    value: this.stepInstance
                });
                this.loading = false;
            }));
        }
    }
    /**
     * @return {?}
     */
    cancel() {
        this.cancelListener.emit({});
    }
    /**
     * @private
     * @param {?} questionLine
     * @param {?} responses
     * @return {?}
     */
    addResponses(questionLine, responses) {
        if (!questionLine)
            return;
        if (questionLine.answerType === 'CHECK_BOX' || questionLine.answerType === 'RADIO_BUTTON') {
            /** @type {?} */
            let questionResponse = new QuestionResponse();
            questionResponse.questionLineId = questionLine.id;
            questionResponse.weight = questionLine.weight;
            questionResponse.selected = questionLine.questionResponse.selected;
            responses.push(questionResponse);
        }
        else if (questionLine.answerType === 'TEXT' || questionLine.answerType === 'TEXT_AREA' ||
            questionLine.answerType === 'NUMBER' || questionLine.answerType === 'IMAGE') {
            /** @type {?} */
            let questionResponse = new QuestionResponse();
            questionResponse.questionLineId = questionLine.id;
            questionResponse.weight = questionLine.weight;
            questionResponse.response = questionLine.questionResponse.response;
            responses.push(questionResponse);
        }
        else if (questionLine.answerType === 'SELECT') {
            /** @type {?} */
            let questionResponse = new QuestionResponse();
            questionResponse.questionLineId = questionLine.id;
            questionResponse.weight = questionLine.questionResponse.weight;
            if (questionLine.questionResponse.weight) {
                questionResponse.selected = true;
                questionResponse.response = questionLine.questionResponse.response;
            }
            responses.push(questionResponse);
        }
        else if ((questionLine.lineType === 'SINGLE_SELECT' || questionLine.lineType === 'MULTI_SELECT') &&
            (questionLine.allowSupportingDescription || questionLine.allowSupportingImage)) {
            /** @type {?} */
            let questionResponse = new QuestionResponse();
            questionResponse.questionLineId = questionLine.id;
            questionResponse.supportingDescription = questionLine.questionResponse.supportingDescription;
            questionResponse.supportingImage = questionLine.questionResponse.supportingImage;
            responses.push(questionResponse);
        }
        for (let i = 0; i < questionLine.children.length; i++) {
            this.addResponses(questionLine.children[i], responses);
        }
    }
    ;
    /**
     * @private
     * @param {?} questionLine
     * @return {?}
     */
    validateQuestionLine(questionLine) {
        if (!this.hasValidAnswer(questionLine))
            throw "Invalid response";
        if (questionLine.children) {
            for (var i = 0; i < questionLine.children.length; i++) {
                /** @type {?} */
                var child = questionLine.children[i];
                if (child.answerType === "CHECK_BOX" || child.answerType === "RADIO_BUTTON") {
                    /** @type {?} */
                    var childResponse = questionLine.questionResponse;
                    if (childResponse && childResponse.selected) {
                        this.validateQuestionLine(child);
                    }
                }
                else {
                    this.validateQuestionLine(child);
                }
            }
        }
    }
    /**
     * @private
     * @param {?} questionLine
     * @return {?}
     */
    hasValidAnswer(questionLine) {
        if (!questionLine.lineType || !questionLine.answerType)
            return true;
        if (questionLine.lineType === "MULTI_SELECT" || questionLine.lineType === "SINGLE_SELECT") {
            if (questionLine.required && questionLine.children) {
                for (var i = 0; i < questionLine.children.length; i++) {
                    if (questionLine.children[i].questionResponse && questionLine.children[i].questionResponse.selected)
                        return true;
                }
                return false;
            }
            else {
                return true;
            }
        }
        else if (questionLine.answerType === "NUMBER" || questionLine.answerType === "SELECT" ||
            questionLine.answerType === "TEXT" || questionLine.answerType === "TEXT_AREA" || questionLine.answerType === "IMAGE") {
            if (questionLine.required && (!questionLine.questionResponse || !questionLine.questionResponse.response))
                return false;
            if (questionLine.answerType === "NUMBER") {
                if ((questionLine.required && isNaN(parseInt(questionLine.questionResponse.response))) ||
                    (!questionLine.required && questionLine.questionResponse.response && isNaN(parseInt(questionLine.questionResponse.response)))) {
                    return false;
                }
                try {
                    /** @type {?} */
                    var d = parseFloat(questionLine.questionResponse.response);
                    /** @type {?} */
                    var p = questionLine.parameters.split("|");
                    if (p.length >= 4) {
                        /** @type {?} */
                        var hasLimits = "true" === p[1];
                        /** @type {?} */
                        var max = parseFloat(p[2]);
                        /** @type {?} */
                        var min = parseFloat(p[3]);
                        if (hasLimits && (d < min || d > max) && !questionLine.errorAccepted)
                            return false;
                    }
                }
                catch (error) {
                    return false;
                }
            }
        }
        return true;
    }
    /**
     * @private
     * @param {?} questionLine
     * @return {?}
     */
    setAllValidationMessages(questionLine) {
        this.setValidationMessage(questionLine);
        if (questionLine.children) {
            for (var i = 0; i < questionLine.children.length; i++) {
                /** @type {?} */
                var child = questionLine.children[i];
                if (child.answerType === "CHECK_BOX" || child.answerType === "RADIO_BUTTON") {
                    /** @type {?} */
                    var childResponse = child.questionResponse;
                    if (childResponse && childResponse.selected) {
                        this.setAllValidationMessages(child);
                    }
                }
                else {
                    this.setAllValidationMessages(child);
                }
            }
        }
    }
    /**
     * @private
     * @param {?} questionLine
     * @return {?}
     */
    setValidationMessage(questionLine) {
        if (!questionLine.lineType || !questionLine.answerType) {
            questionLine.hasError = false;
            questionLine.errorMessage = null;
            questionLine.showErrorConfirmation = false;
            return;
        }
        if (questionLine.lineType === "MULTI_SELECT" || questionLine.lineType === "SINGLE_SELECT") {
            if (questionLine.required && questionLine.children) {
                /** @type {?} */
                var selected = false;
                for (var i = 0; i < questionLine.children.length; i++) {
                    if (questionLine.children[i].questionResponse && questionLine.children[i].questionResponse.selected) {
                        selected = true;
                        break;
                    }
                }
                if (!selected) {
                    questionLine.hasError = true;
                    questionLine.showErrorConfirmation = false;
                    questionLine.errorMessage = "An answer must be selected";
                    return;
                }
            }
        }
        else if (questionLine.answerType === "NUMBER" || questionLine.answerType === "SELECT" ||
            questionLine.answerType === "TEXT" || questionLine.answerType === "TEXT_AREA" ||
            questionLine.answerType === "IMAGE") {
            /** @type {?} */
            var questionResponse = questionLine.questionResponse;
            if (questionLine.required && (!questionResponse || !questionResponse.response)) {
                questionLine.hasError = true;
                questionLine.showErrorConfirmation = false;
                questionLine.errorMessage = "An answer must be provided";
                return;
            }
            if (questionLine.answerType === "NUMBER") {
                if ((questionLine.required && isNaN(parseInt(questionResponse.response))) ||
                    (!questionLine.required && questionResponse.response && isNaN(parseInt(questionResponse.response)))) {
                    questionLine.hasError = true;
                    questionLine.showErrorConfirmation = false;
                    questionLine.errorMessage = "An invalid number was entered";
                    return;
                }
                try {
                    /** @type {?} */
                    var d = parseFloat(questionResponse.response);
                    /** @type {?} */
                    var p = questionLine.parameters.split("|");
                    if (p.length >= 4) {
                        /** @type {?} */
                        var hasLimits = "true" === p[1];
                        /** @type {?} */
                        var max = parseFloat(p[2]);
                        /** @type {?} */
                        var min = parseFloat(p[3]);
                        if (hasLimits) {
                            if (d < min) {
                                questionLine.hasError = true;
                                questionLine.showErrorConfirmation = true;
                                questionLine.errorMessage = "The value provided is less than the minimum value of " + min + ".  Are you sure this is correct?";
                                return;
                            }
                            else if (d > max) {
                                questionLine.hasError = true;
                                questionLine.showErrorConfirmation = true;
                                questionLine.errorMessage = "The value provided exceeds the maximum value of " + max + ".  Are you sure this is correct?";
                                return;
                            }
                        }
                    }
                }
                catch (error) {
                    questionLine.hasError = true;
                    questionLine.showErrorConfirmation = false;
                    questionLine.errorMessage = "An invalid number was supplied";
                    return;
                }
            }
        }
        questionLine.hasError = false;
        questionLine.showErrorConfirmation = false;
        questionLine.errorMessage = "";
    }
    /**
     * @param {?} questionLine
     * @return {?}
     */
    takePictureQuestionnaire(questionLine) {
        /** @type {?} */
        let imageContainer = {
            /**
             * @param {?} image
             * @return {?}
             */
            setImage(image) {
                questionLine.questionResponse.response = "data:image/jpeg;base64," + image;
            }
        };
        this.onTakePicture.emit(imageContainer);
    }
    /**
     * @param {?} questionLine
     * @return {?}
     */
    takeSupportingPictureQuestionnaire(questionLine) {
        /** @type {?} */
        let imageContainer = {
            /**
             * @param {?} image
             * @return {?}
             */
            setImage(image) {
                questionLine.questionResponse.supportingImage = "data:image/jpeg;base64," + image;
            }
        };
        this.onTakePicture.emit(imageContainer);
    }
    /**
     * @param {?} questionLine
     * @return {?}
     */
    questionAnswered(questionLine) {
        console.log("questionnaire.questionAnswered: " + questionLine.wording + " " + questionLine.integrationIndicator + " " + questionLine.id);
        this.onQuestionAnswered.emit(questionLine);
    }
}
QuestionnaireComponent.decorators = [
    { type: Component, args: [{
                selector: 'questionnaire',
                template: "<div class=\"questionnaire\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div *ngIf=\"questionnaire\" class=\"heading\">\n    {{ questionnaire.description }}\n    <span class=\"sub-heading\">\n      (Page {{ (pageIndex + 1) }} of {{ this.questionnaire.questionLines.length }})\n    </span>\n  </div>\n  <!-- <loading-indicator [show]=\"loading\"></loading-indicator> -->\n  <!-- <div *ngIf=\"questionnaire\">\n    <div class=\"row\">\n      <div class=\"col-md-12\">\n        <h2 *ngIf=\"wizard\">{{questionnaire.description}}</h2>\n        <h3 *ngIf=\"!wizard\">{{questionnaire.description}}</h3>\n      </div>\n    </div> -->\n\n  <div *ngIf=\"questionnaire && questionnaire.questionLines\" class=\"questionnaire-content\">\n    <question-line [questionLine]=\"questionnaire.questionLines[pageIndex]\" (onTakePictureQuestionLine)=\"takePictureQuestionnaire($event)\"\n        (onQuestionAnswered)=\"questionAnswered($event)\" (onTakeSupportingPictureQuestionLine)=\"takeSupportingPictureQuestionnaire($event)\"></question-line>\n  </div>\n\n  <div class=\"button-section\">\n    <div *ngIf=\"questionnaire && questionnaire.questionLines && pageIndex >= questionnaire.questionLines.length - 1\"\n        class=\"button\" [ngClass]=\"{ 'button-disabled': loading }\" (click)=\"save(true)\">\n      <div class=\"button-text\">Submit</div>\n    </div>\n    <div *ngIf=\"questionnaire && questionnaire.questionLines && pageIndex < questionnaire.questionLines.length - 1\"\n        class=\"button button-outline\" [ngClass]=\"{ 'button-outline-disabled': loading }\" (click)=\"save(false)\">\n      <div class=\"button-text\">Save</div>\n    </div>\n\n    <div *ngIf=\"questionnaire && questionnaire.questionLines && questionnaire.questionLines.length > 1\"\n        class=\"button image-button\" [ngClass]=\"{ 'button-disabled': pageIndex >= questionnaire.questionLines.length - 1 || loading }\" (click)=\"next()\">\n      <div class=\"button-text\">\n          <img src=\"data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAUVJREFUeJzt2EtOw0AURNELbCw7o9hZdgaTtBigBLv9+vM+JfUscnyPZw220+OknIDvx9HSN1kw8RufDkH8jU+DIJ7Hh0cQ/8eHRRDH48MhiPPxYRBEf7x7BHE93i2CsIt3hyDs47dBeF/8/58sRvg48Js78AbcBr3D7fH8+6Dnv9wRAAiMcBQAgiKcAYCACGcBIBhCDwAEQugFgCAIVwAgAMJVAHCOYAEAjhGsAMApgiUAOESwBgBnCCMAwBHCKABwgjASABwgjAaAzRFmAMDGCLMAYFOE1XeCqSbG3S5vccP8aqLiK77ijY9mhfRMVHzFV7zx0ayQnomKr/iKNz6aFdIzUfEVX/      HGR7NCeiYqPmb86jvBLzb/+m0i2JfvmUgc3yYSx7eJxPFtInF8m0gc3yYSx7eJxPFtInF8m0gc3yacxf8AAP323bWbSe8AAAAASUVORK5CYII=\">\n      </div>\n    </div>\n    <div *ngIf=\"questionnaire && questionnaire.questionLines && questionnaire.questionLines.length > 1\"\n        class=\"button image-button\" (click)=\"previous()\">\n      <div class=\"button-text\">\n          <img src=\"data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAUlJREFUeJzt2k1uwjAUReFTujF2xu3O2Fk7icUAtU2cZ8fv50qeoSTnQ0wiYO1pOykn4Hs7uvRJLph4xadDEO/xaRDE7/HhEcT/8WERxP74cAjieHwYBNEf7x5BnI93iyDs4t0hCPv4boTbiZCeCXhMvucyE+O++eV/AqLiK77ijY9mhfRMVHzFV7zx0ayQnomKr/iKNz6aFdIzUfEVX/HGR7NCeiYqPmb87HeCy+1zx2eewAdwH/QM9+36z0HX/3N7ACAwwl4ACIpwBAACIhwFgGAIPQAQCKEXAIIgnAGAAAhnAcA5ggUAOEawAgCnCJYA4BDBGgCcIYwAAEcIowDACcJIAHCAMBoAFkeYAQALI8wCgEURrvif4Nfkey45Eeztcs9E4vg2kTi+TSSObxOJ49tE4vg2kTi+TSSObxOJ49tE4vg2MTj+B2+f9t1j5qkYAAAAAElFTkSuQmCC\">\n      </div>\n    </div>\n\n    <div class=\"button button-outline\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                styles: [".questionnaire{width:100%;height:100%;max-height:100%;position:relative}.questionnaire .heading{position:absolute;top:0;width:100%;height:32px;font-size:16px;padding:8px}.questionnaire .heading .sub-heading{font-size:12px;font-style:italic}.questionnaire .questionnaire-content{position:absolute;top:32px;left:0;bottom:50px;width:100%;overflow-y:scroll;padding:8px;box-sizing:border-box;background-color:#f9f9f9}.questionnaire .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.questionnaire .button-section .button{width:70px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.questionnaire .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.questionnaire .button-section .button .button-text img{padding-left:4px;padding-top:4px;width:16px;height:16px}.questionnaire .button-section .image-button{width:36px;border:1px solid #2b4054;background-color:#fff;color:#2b4054}.questionnaire .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.questionnaire .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b}.questionnaire .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b}"]
            }] }
];
/** @nocollapse */
QuestionnaireComponent.ctorParameters = () => [
    { type: ProjectService }
];
QuestionnaireComponent.propDecorators = {
    stepInstance: [{ type: Input }],
    userId: [{ type: Input }],
    wizard: [{ type: Input }],
    newPagePerSubquestion: [{ type: Input }],
    stepCompletedListener: [{ type: Output }],
    cancelListener: [{ type: Output }],
    onTakePicture: [{ type: Output }],
    onQuestionAnswered: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    QuestionnaireComponent.prototype.stepInstance;
    /** @type {?} */
    QuestionnaireComponent.prototype.userId;
    /** @type {?} */
    QuestionnaireComponent.prototype.wizard;
    /** @type {?} */
    QuestionnaireComponent.prototype.newPagePerSubquestion;
    /** @type {?} */
    QuestionnaireComponent.prototype.questionnaire;
    /** @type {?} */
    QuestionnaireComponent.prototype.pageIndex;
    /** @type {?} */
    QuestionnaireComponent.prototype.stepCompletedListener;
    /** @type {?} */
    QuestionnaireComponent.prototype.cancelListener;
    /** @type {?} */
    QuestionnaireComponent.prototype.onTakePicture;
    /** @type {?} */
    QuestionnaireComponent.prototype.onQuestionAnswered;
    /** @type {?} */
    QuestionnaireComponent.prototype.error;
    /** @type {?} */
    QuestionnaireComponent.prototype.loading;
    /**
     * @type {?}
     * @private
     */
    QuestionnaireComponent.prototype.projectService;
    /* Skipping unhandled member: ;*/
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicXVlc3Rpb25uYWlyZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL3N0ZXBzL3F1ZXN0aW9ubmFpcmUvcXVlc3Rpb25uYWlyZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBcUIsTUFBTSxFQUFhLE1BQU0sZUFBZSxDQUFDO0FBRXJHLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUdoRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNsRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFTMUQsTUFBTSxPQUFPLHNCQUFzQjs7OztJQWNqQyxZQUFvQixjQUE4QjtRQUE5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFQeEMsMEJBQXFCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUMzQyxtQkFBYyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDcEMsa0JBQWEsR0FBRyxJQUFJLFlBQVksRUFBa0IsQ0FBQztRQUNuRCx1QkFBa0IsR0FBRyxJQUFJLFlBQVksRUFBZ0IsQ0FBQztRQUs5RCxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO0lBQ3ZCLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxDQUFDLGFBQWEsRUFBRSxFQUFFO1lBQ2hGLElBQUksQ0FBQyxhQUFhLEdBQUcsYUFBYSxDQUFDO1lBQ25DLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDO1lBQ25CLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxXQUFXLENBQUMsT0FBTztRQUNqQixJQUFJLE9BQU8sQ0FBQyxZQUFZLElBQUksT0FBTyxDQUFDLFlBQVksQ0FBQyxhQUFhLElBQUksT0FBTyxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsRUFBRTtZQUNqRyxPQUFPLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxFQUFFLEtBQUssT0FBTyxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsRUFBRSxFQUFFO1lBQ3BGLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDcEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDcEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUk7Ozs7WUFBQyxDQUFDLGFBQWEsRUFBRSxFQUFFO2dCQUNoRixJQUFJLENBQUMsYUFBYSxHQUFHLGFBQWEsQ0FBQztnQkFDbkMsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7Z0JBQ25CLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQ3ZCLENBQUMsRUFBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDOzs7O0lBRUQsV0FBVztJQUNYLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ04sSUFBSSxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsRUFBRTtZQUM1QixrRkFBa0Y7WUFDNUUsSUFBSTtnQkFDViw4REFBOEQ7Z0JBQzlELDBEQUEwRDtnQkFDbEQsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO2dCQUNqQixRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7YUFDN0I7WUFBQyxPQUFPLEtBQUssRUFBRTtnQkFDZCxzQkFBc0I7YUFDdkI7U0FDRjtJQUNILENBQUM7Ozs7SUFFRCxJQUFJO1FBQ0YsSUFBSSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7O2dCQUM1RCxZQUFZLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUNuRSxJQUFJO2dCQUNGLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDNUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUN4QyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7Z0JBQ2pCLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQzthQUM3QjtZQUFDLE9BQU8sS0FBSyxFQUFFO2dCQUNkLHNCQUFzQjthQUN2QjtTQUNGO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxJQUFJLENBQUMsTUFBZTtRQUNsQixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNqQixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQzs7Z0JBQ2hCLFNBQVMsR0FBRyxJQUFJLEtBQUssRUFBb0I7WUFDN0MsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDaEUsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsRUFBRSxTQUFTLENBQUMsQ0FBQzthQUNuRTtZQUVELElBQUksQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSTs7OztZQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUMxRyxJQUFJLE1BQU0sRUFBRTtvQkFDVixJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQztpQkFDcEI7Z0JBRUQsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQztvQkFDOUIsS0FBSyxFQUFFLElBQUksQ0FBQyxZQUFZO2lCQUN6QixDQUFDLENBQUM7Z0JBRUgsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFDdkIsQ0FBQyxFQUFDLENBQUM7U0FDSjtJQUNILENBQUM7Ozs7SUFFRCxNQUFNO1FBQ0osSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFDeEIsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7OztJQUVPLFlBQVksQ0FBQyxZQUEwQixFQUFFLFNBQTZCO1FBQzVFLElBQUksQ0FBQyxZQUFZO1lBQ2YsT0FBTztRQUVULElBQUksWUFBWSxDQUFDLFVBQVUsS0FBSyxXQUFXLElBQUksWUFBWSxDQUFDLFVBQVUsS0FBSyxjQUFjLEVBQUU7O2dCQUNyRixnQkFBZ0IsR0FBRyxJQUFJLGdCQUFnQixFQUFFO1lBQzdDLGdCQUFnQixDQUFDLGNBQWMsR0FBRyxZQUFZLENBQUMsRUFBRSxDQUFDO1lBQ2xELGdCQUFnQixDQUFDLE1BQU0sR0FBRyxZQUFZLENBQUMsTUFBTSxDQUFDO1lBQzlDLGdCQUFnQixDQUFDLFFBQVEsR0FBRyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDO1lBQ25FLFNBQVMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztTQUNsQzthQUFNLElBQUksWUFBWSxDQUFDLFVBQVUsS0FBSyxNQUFNLElBQUksWUFBWSxDQUFDLFVBQVUsS0FBSyxXQUFXO1lBQ3BGLFlBQVksQ0FBQyxVQUFVLEtBQUssUUFBUSxJQUFJLFlBQVksQ0FBQyxVQUFVLEtBQUssT0FBTyxFQUFFOztnQkFDM0UsZ0JBQWdCLEdBQUcsSUFBSSxnQkFBZ0IsRUFBRTtZQUM3QyxnQkFBZ0IsQ0FBQyxjQUFjLEdBQUcsWUFBWSxDQUFDLEVBQUUsQ0FBQztZQUNsRCxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsWUFBWSxDQUFDLE1BQU0sQ0FBQztZQUM5QyxnQkFBZ0IsQ0FBQyxRQUFRLEdBQUcsWUFBWSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQztZQUNuRSxTQUFTLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7U0FDbEM7YUFBTSxJQUFJLFlBQVksQ0FBQyxVQUFVLEtBQUssUUFBUSxFQUFFOztnQkFDM0MsZ0JBQWdCLEdBQUcsSUFBSSxnQkFBZ0IsRUFBRTtZQUM3QyxnQkFBZ0IsQ0FBQyxjQUFjLEdBQUcsWUFBWSxDQUFDLEVBQUUsQ0FBQztZQUNsRCxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsWUFBWSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQztZQUMvRCxJQUFJLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUU7Z0JBQ3hDLGdCQUFnQixDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7Z0JBQ2pDLGdCQUFnQixDQUFDLFFBQVEsR0FBRyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDO2FBQ3BFO1lBRUQsU0FBUyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1NBQ2xDO2FBQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEtBQUssZUFBZSxJQUFJLFlBQVksQ0FBQyxRQUFRLEtBQUssY0FBYyxDQUFDO1lBQzlGLENBQUMsWUFBWSxDQUFDLDBCQUEwQixJQUFJLFlBQVksQ0FBQyxvQkFBb0IsQ0FBQyxFQUFFOztnQkFDOUUsZ0JBQWdCLEdBQUcsSUFBSSxnQkFBZ0IsRUFBRTtZQUM3QyxnQkFBZ0IsQ0FBQyxjQUFjLEdBQUcsWUFBWSxDQUFDLEVBQUUsQ0FBQztZQUNsRCxnQkFBZ0IsQ0FBQyxxQkFBcUIsR0FBRyxZQUFZLENBQUMsZ0JBQWdCLENBQUMscUJBQXFCLENBQUM7WUFDN0YsZ0JBQWdCLENBQUMsZUFBZSxHQUFHLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLENBQUM7WUFDakYsU0FBUyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1NBQ2xDO1FBRUQsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFlBQVksQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3JELElBQUksQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxTQUFTLENBQUMsQ0FBQztTQUN4RDtJQUNILENBQUM7SUFBQSxDQUFDOzs7Ozs7SUFFTSxvQkFBb0IsQ0FBQyxZQUEwQjtRQUNyRCxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUM7WUFDcEMsTUFBTSxrQkFBa0IsQ0FBQztRQUUzQixJQUFJLFlBQVksQ0FBQyxRQUFRLEVBQUU7WUFDekIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFlBQVksQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFOztvQkFDakQsS0FBSyxHQUFHLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUNwQyxJQUFJLEtBQUssQ0FBQyxVQUFVLEtBQUssV0FBVyxJQUFJLEtBQUssQ0FBQyxVQUFVLEtBQUssY0FBYyxFQUFFOzt3QkFDdkUsYUFBYSxHQUFHLFlBQVksQ0FBQyxnQkFBZ0I7b0JBQ2pELElBQUksYUFBYSxJQUFJLGFBQWEsQ0FBQyxRQUFRLEVBQUU7d0JBQzNDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDbEM7aUJBQ0Y7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNsQzthQUNGO1NBQ0Y7SUFDSCxDQUFDOzs7Ozs7SUFFTyxjQUFjLENBQUMsWUFBMEI7UUFDL0MsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVTtZQUNwRCxPQUFPLElBQUksQ0FBQztRQUVkLElBQUksWUFBWSxDQUFDLFFBQVEsS0FBSyxjQUFjLElBQUksWUFBWSxDQUFDLFFBQVEsS0FBSyxlQUFlLEVBQUU7WUFDekYsSUFBSSxZQUFZLENBQUMsUUFBUSxJQUFJLFlBQVksQ0FBQyxRQUFRLEVBQUU7Z0JBQ2xELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxZQUFZLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDckQsSUFBSSxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixJQUFJLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsUUFBUTt3QkFDakcsT0FBTyxJQUFJLENBQUM7aUJBQ2Y7Z0JBRUQsT0FBTyxLQUFLLENBQUM7YUFDZDtpQkFBTTtnQkFDTCxPQUFPLElBQUksQ0FBQzthQUNiO1NBQ0Y7YUFBTSxJQUFJLFlBQVksQ0FBQyxVQUFVLEtBQUssUUFBUSxJQUFJLFlBQVksQ0FBQyxVQUFVLEtBQUssUUFBUTtZQUMvRSxZQUFZLENBQUMsVUFBVSxLQUFLLE1BQU0sSUFBSSxZQUFZLENBQUMsVUFBVSxLQUFLLFdBQVcsSUFBSSxZQUFZLENBQUMsVUFBVSxLQUFLLE9BQU8sRUFBRTtZQUM1SCxJQUFJLFlBQVksQ0FBQyxRQUFRLElBQUksQ0FBQyxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUM7Z0JBQ3RHLE9BQU8sS0FBSyxDQUFDO1lBRWYsSUFBSSxZQUFZLENBQUMsVUFBVSxLQUFLLFFBQVEsRUFBRTtnQkFDeEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLElBQUksS0FBSyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztvQkFDOUUsQ0FBQyxDQUFDLFlBQVksQ0FBQyxRQUFRLElBQUksWUFBWSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsSUFBSSxLQUFLLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0JBQ3JJLE9BQU8sS0FBSyxDQUFDO2lCQUNkO2dCQUVELElBQUk7O3dCQUNFLENBQUMsR0FBRyxVQUFVLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQzs7d0JBQ3RELENBQUMsR0FBRyxZQUFZLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7b0JBQzFDLElBQUksQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7OzRCQUNiLFNBQVMsR0FBRyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzs7NEJBQzNCLEdBQUcsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDOzs0QkFDdEIsR0FBRyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQzFCLElBQUksU0FBUyxJQUFJLENBQUMsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYTs0QkFDbEUsT0FBTyxLQUFLLENBQUM7cUJBQ2hCO2lCQUNGO2dCQUFDLE9BQU8sS0FBSyxFQUFFO29CQUNkLE9BQU8sS0FBSyxDQUFDO2lCQUNkO2FBQ0Y7U0FDRjtRQUVELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQzs7Ozs7O0lBRU8sd0JBQXdCLENBQUMsWUFBMEI7UUFDekQsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3hDLElBQUksWUFBWSxDQUFDLFFBQVEsRUFBRTtZQUN6QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsWUFBWSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7O29CQUNqRCxLQUFLLEdBQUcsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BDLElBQUksS0FBSyxDQUFDLFVBQVUsS0FBSyxXQUFXLElBQUksS0FBSyxDQUFDLFVBQVUsS0FBSyxjQUFjLEVBQUU7O3dCQUN2RSxhQUFhLEdBQUcsS0FBSyxDQUFDLGdCQUFnQjtvQkFDMUMsSUFBSSxhQUFhLElBQUksYUFBYSxDQUFDLFFBQVEsRUFBRTt3QkFDM0MsSUFBSSxDQUFDLHdCQUF3QixDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUN0QztpQkFDRjtxQkFBTTtvQkFDTCxJQUFJLENBQUMsd0JBQXdCLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3RDO2FBQ0Y7U0FDRjtJQUNILENBQUM7Ozs7OztJQUVPLG9CQUFvQixDQUFDLFlBQTBCO1FBQ3JELElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsRUFBRTtZQUN0RCxZQUFZLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztZQUM5QixZQUFZLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztZQUNqQyxZQUFZLENBQUMscUJBQXFCLEdBQUcsS0FBSyxDQUFDO1lBQzNDLE9BQU87U0FDUjtRQUVELElBQUksWUFBWSxDQUFDLFFBQVEsS0FBSyxjQUFjLElBQUksWUFBWSxDQUFDLFFBQVEsS0FBSyxlQUFlLEVBQUU7WUFDekYsSUFBSSxZQUFZLENBQUMsUUFBUSxJQUFJLFlBQVksQ0FBQyxRQUFRLEVBQUU7O29CQUM5QyxRQUFRLEdBQUcsS0FBSztnQkFDcEIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFlBQVksQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUNyRCxJQUFJLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLElBQUksWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUU7d0JBQ25HLFFBQVEsR0FBRyxJQUFJLENBQUM7d0JBQ2hCLE1BQU07cUJBQ1A7aUJBQ0Y7Z0JBRUQsSUFBSSxDQUFDLFFBQVEsRUFBRTtvQkFDYixZQUFZLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztvQkFDN0IsWUFBWSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztvQkFDM0MsWUFBWSxDQUFDLFlBQVksR0FBRyw0QkFBNEIsQ0FBQztvQkFDekQsT0FBTztpQkFDUjthQUNGO1NBQ0Y7YUFBTSxJQUFJLFlBQVksQ0FBQyxVQUFVLEtBQUssUUFBUSxJQUFJLFlBQVksQ0FBQyxVQUFVLEtBQUssUUFBUTtZQUMvRSxZQUFZLENBQUMsVUFBVSxLQUFLLE1BQU0sSUFBSSxZQUFZLENBQUMsVUFBVSxLQUFLLFdBQVc7WUFDN0UsWUFBWSxDQUFDLFVBQVUsS0FBSyxPQUFPLEVBQUU7O2dCQUN2QyxnQkFBZ0IsR0FBRyxZQUFZLENBQUMsZ0JBQWdCO1lBQ3BELElBQUksWUFBWSxDQUFDLFFBQVEsSUFBSSxDQUFDLENBQUMsZ0JBQWdCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDOUUsWUFBWSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7Z0JBQzdCLFlBQVksQ0FBQyxxQkFBcUIsR0FBRyxLQUFLLENBQUM7Z0JBQzNDLFlBQVksQ0FBQyxZQUFZLEdBQUcsNEJBQTRCLENBQUM7Z0JBQ3pELE9BQU87YUFDUjtZQUVELElBQUksWUFBWSxDQUFDLFVBQVUsS0FBSyxRQUFRLEVBQUU7Z0JBQ3hDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztvQkFDakUsQ0FBQyxDQUFDLFlBQVksQ0FBQyxRQUFRLElBQUksZ0JBQWdCLENBQUMsUUFBUSxJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFO29CQUMzRyxZQUFZLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztvQkFDN0IsWUFBWSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztvQkFDM0MsWUFBWSxDQUFDLFlBQVksR0FBRywrQkFBK0IsQ0FBQztvQkFDNUQsT0FBTztpQkFDUjtnQkFFRCxJQUFJOzt3QkFDRSxDQUFDLEdBQUcsVUFBVSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQzs7d0JBQ3pDLENBQUMsR0FBRyxZQUFZLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7b0JBQzFDLElBQUksQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7OzRCQUNiLFNBQVMsR0FBRyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzs7NEJBQzNCLEdBQUcsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDOzs0QkFDdEIsR0FBRyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQzFCLElBQUksU0FBUyxFQUFFOzRCQUNiLElBQUksQ0FBQyxHQUFHLEdBQUcsRUFBRTtnQ0FDWCxZQUFZLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztnQ0FDN0IsWUFBWSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQztnQ0FDMUMsWUFBWSxDQUFDLFlBQVksR0FBRyx1REFBdUQsR0FBRyxHQUFHLEdBQUcsa0NBQWtDLENBQUM7Z0NBQy9ILE9BQU87NkJBQ1I7aUNBQU0sSUFBSSxDQUFDLEdBQUcsR0FBRyxFQUFFO2dDQUNsQixZQUFZLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztnQ0FDN0IsWUFBWSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQztnQ0FDMUMsWUFBWSxDQUFDLFlBQVksR0FBRyxrREFBa0QsR0FBRyxHQUFHLEdBQUcsa0NBQWtDLENBQUM7Z0NBQzFILE9BQU87NkJBQ1I7eUJBQ0Y7cUJBQ0Y7aUJBQ0Y7Z0JBQUMsT0FBTyxLQUFLLEVBQUU7b0JBQ2QsWUFBWSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7b0JBQzdCLFlBQVksQ0FBQyxxQkFBcUIsR0FBRyxLQUFLLENBQUM7b0JBQzNDLFlBQVksQ0FBQyxZQUFZLEdBQUcsZ0NBQWdDLENBQUM7b0JBQzdELE9BQU87aUJBQ1I7YUFDRjtTQUNGO1FBRUQsWUFBWSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDOUIsWUFBWSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztRQUMzQyxZQUFZLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztJQUNqQyxDQUFDOzs7OztJQUVELHdCQUF3QixDQUFDLFlBQTBCOztZQUM3QyxjQUFjLEdBQW1COzs7OztZQUNuQyxRQUFRLENBQUMsS0FBYTtnQkFDcEIsWUFBWSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsR0FBRyx5QkFBeUIsR0FBRyxLQUFLLENBQUM7WUFDN0UsQ0FBQztTQUNGO1FBQ0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDMUMsQ0FBQzs7Ozs7SUFFRCxrQ0FBa0MsQ0FBQyxZQUEwQjs7WUFDdkQsY0FBYyxHQUFtQjs7Ozs7WUFDbkMsUUFBUSxDQUFDLEtBQWE7Z0JBQ3BCLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLEdBQUcseUJBQXlCLEdBQUcsS0FBSyxDQUFDO1lBQ3BGLENBQUM7U0FDRjtRQUNELElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQzFDLENBQUM7Ozs7O0lBRUQsZ0JBQWdCLENBQUMsWUFBMEI7UUFDekMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQ0FBa0MsR0FBRyxZQUFZLENBQUMsT0FBTyxHQUFHLEdBQUcsR0FBRyxZQUFZLENBQUMsb0JBQW9CLEdBQUcsR0FBRyxHQUFHLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUN6SSxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQzdDLENBQUM7OztZQXhVRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGVBQWU7Z0JBQ3pCLHloSEFBNkM7O2FBRzlDOzs7O1lBWlEsY0FBYzs7OzJCQWNwQixLQUFLO3FCQUNMLEtBQUs7cUJBQ0wsS0FBSztvQ0FDTCxLQUFLO29DQUdMLE1BQU07NkJBQ04sTUFBTTs0QkFDTixNQUFNO2lDQUNOLE1BQU07Ozs7SUFUUCw4Q0FBb0M7O0lBQ3BDLHdDQUF3Qjs7SUFDeEIsd0NBQXlCOztJQUN6Qix1REFBd0M7O0lBQ3hDLCtDQUE2Qjs7SUFDN0IsMkNBQWtCOztJQUNsQix1REFBcUQ7O0lBQ3JELGdEQUE4Qzs7SUFDOUMsK0NBQTZEOztJQUM3RCxvREFBZ0U7O0lBQ2hFLHVDQUFXOztJQUNYLHlDQUFpQjs7Ozs7SUFFTCxnREFBc0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE9uSW5pdCwgT25EZXN0cm95LCBPdXRwdXQsIE9uQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBQcm9qZWN0U2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL3Byb2plY3Quc2VydmljZSc7XG5pbXBvcnQgeyBRdWVzdGlvbm5haXJlIH0gZnJvbSAnLi4vLi4vZG9tYWluL3F1ZXN0aW9ubmFpcmUnO1xuaW1wb3J0IHsgUXVlc3Rpb25MaW5lIH0gZnJvbSAnLi4vLi4vZG9tYWluL3F1ZXN0aW9uLWxpbmUnO1xuaW1wb3J0IHsgUXVlc3Rpb25SZXNwb25zZSB9IGZyb20gJy4uLy4uL2RvbWFpbi9xdWVzdGlvbi1yZXNwb25zZSc7XG5pbXBvcnQgeyBTdGVwSW5zdGFuY2UgfSBmcm9tICcuLi8uLi9kb21haW4vc3RlcC1pbnN0YW5jZSc7XG5pbXBvcnQgeyBJbWFnZUNvbnRhaW5lciB9IGZyb20gJy4uLy4uL2RvbWFpbi9pbWFnZS1jb250YWluZXInO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdxdWVzdGlvbm5haXJlJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3F1ZXN0aW9ubmFpcmUuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9xdWVzdGlvbm5haXJlLmNvbXBvbmVudC5zY3NzJ11cbiAgLy8gZGlyZWN0aXZlczogW1F1ZXN0aW9uTGluZUNvbXBvbmVudCwgVGV4dFF1ZXN0aW9uQ29tcG9uZW50LCBMb2FkaW5nSW5kaWNhdG9yXVxufSlcbmV4cG9ydCBjbGFzcyBRdWVzdGlvbm5haXJlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3ksIE9uQ2hhbmdlcyB7XG4gIEBJbnB1dCgpIHN0ZXBJbnN0YW5jZTogU3RlcEluc3RhbmNlO1xuICBASW5wdXQoKSB1c2VySWQ6IG51bWJlcjtcbiAgQElucHV0KCkgd2l6YXJkOiBib29sZWFuO1xuICBASW5wdXQoKSBuZXdQYWdlUGVyU3VicXVlc3Rpb246IGJvb2xlYW47XG4gIHF1ZXN0aW9ubmFpcmU6IFF1ZXN0aW9ubmFpcmU7XG4gIHBhZ2VJbmRleDogbnVtYmVyO1xuICBAT3V0cHV0KCkgc3RlcENvbXBsZXRlZExpc3RlbmVyID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgY2FuY2VsTGlzdGVuZXIgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBvblRha2VQaWN0dXJlID0gbmV3IEV2ZW50RW1pdHRlcjxJbWFnZUNvbnRhaW5lcj4oKTtcbiAgQE91dHB1dCgpIG9uUXVlc3Rpb25BbnN3ZXJlZCA9IG5ldyBFdmVudEVtaXR0ZXI8UXVlc3Rpb25MaW5lPigpO1xuICBlcnJvcjogYW55O1xuICBsb2FkaW5nOiBib29sZWFuO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcHJvamVjdFNlcnZpY2U6IFByb2plY3RTZXJ2aWNlKSB7XG4gICAgdGhpcy5wYWdlSW5kZXggPSAtMTtcbiAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMubG9hZGluZyA9IHRydWU7XG4gICAgdGhpcy5wcm9qZWN0U2VydmljZS5nZXRRdWVzdGlvbm5haXJlKHRoaXMuc3RlcEluc3RhbmNlLmlkKS50aGVuKChxdWVzdGlvbm5haXJlKSA9PiB7XG4gICAgICB0aGlzLnF1ZXN0aW9ubmFpcmUgPSBxdWVzdGlvbm5haXJlO1xuICAgICAgdGhpcy5wYWdlSW5kZXggPSAwO1xuICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgfSk7XG4gIH1cblxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzKSB7XG4gICAgaWYgKGNoYW5nZXMuc3RlcEluc3RhbmNlICYmIGNoYW5nZXMuc3RlcEluc3RhbmNlLnByZXZpb3VzVmFsdWUgJiYgY2hhbmdlcy5zdGVwSW5zdGFuY2UucHJldmlvdXNWYWx1ZS5pZCAmJlxuICAgICAgICAgIGNoYW5nZXMuc3RlcEluc3RhbmNlLmN1cnJlbnRWYWx1ZS5pZCAhPT0gY2hhbmdlcy5zdGVwSW5zdGFuY2UucHJldmlvdXNWYWx1ZS5pZCkge1xuICAgICAgdGhpcy5wYWdlSW5kZXggPSAtMTtcbiAgICAgIHRoaXMubG9hZGluZyA9IHRydWU7XG4gICAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmdldFF1ZXN0aW9ubmFpcmUodGhpcy5zdGVwSW5zdGFuY2UuaWQpLnRoZW4oKHF1ZXN0aW9ubmFpcmUpID0+IHtcbiAgICAgICAgdGhpcy5xdWVzdGlvbm5haXJlID0gcXVlc3Rpb25uYWlyZTtcbiAgICAgICAgdGhpcy5wYWdlSW5kZXggPSAwO1xuICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIG5nT25EZXN0cm95KCkge1xuICB9XG5cbiAgcHJldmlvdXMoKSB7XG4gICAgaWYgKHRoaXMucGFnZUluZGV4ID4gMCkge1xuLy8gICAgICAgICAgICBsZXQgcXVlc3Rpb25MaW5lID0gdGhpcy5xdWVzdGlvbm5haXJlLnF1ZXN0aW9uTGluZXNbdGhpcy5wYWdlSW5kZXhdO1xuICAgICAgdHJ5IHtcbi8vICAgICAgICAgICAgICAgIHRoaXMuc2V0QWxsVmFsaWRhdGlvbk1lc3NhZ2VzKHF1ZXN0aW9uTGluZSk7XG4vLyAgICAgICAgICAgICAgICB0aGlzLnZhbGlkYXRlUXVlc3Rpb25MaW5lKHF1ZXN0aW9uTGluZSk7XG4gICAgICAgIHRoaXMucGFnZUluZGV4LS07XG4gICAgICAgIGRvY3VtZW50LmJvZHkuc2Nyb2xsVG9wID0gMDtcbiAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBuZXh0KCkge1xuICAgIGlmICh0aGlzLnBhZ2VJbmRleCA8IHRoaXMucXVlc3Rpb25uYWlyZS5xdWVzdGlvbkxpbmVzLmxlbmd0aCAtIDEpIHtcbiAgICAgIGxldCBxdWVzdGlvbkxpbmUgPSB0aGlzLnF1ZXN0aW9ubmFpcmUucXVlc3Rpb25MaW5lc1t0aGlzLnBhZ2VJbmRleF07XG4gICAgICB0cnkge1xuICAgICAgICB0aGlzLnNldEFsbFZhbGlkYXRpb25NZXNzYWdlcyhxdWVzdGlvbkxpbmUpO1xuICAgICAgICB0aGlzLnZhbGlkYXRlUXVlc3Rpb25MaW5lKHF1ZXN0aW9uTGluZSk7XG4gICAgICAgIHRoaXMucGFnZUluZGV4Kys7XG4gICAgICAgIGRvY3VtZW50LmJvZHkuc2Nyb2xsVG9wID0gMDtcbiAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBzYXZlKHN1Ym1pdDogYm9vbGVhbikge1xuICAgIGlmICghdGhpcy5sb2FkaW5nKSB7XG4gICAgICB0aGlzLmxvYWRpbmcgPSB0cnVlO1xuICAgICAgbGV0IHJlc3BvbnNlcyA9IG5ldyBBcnJheTxRdWVzdGlvblJlc3BvbnNlPigpO1xuICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLnF1ZXN0aW9ubmFpcmUucXVlc3Rpb25MaW5lcy5sZW5ndGg7IGkrKykge1xuICAgICAgICB0aGlzLmFkZFJlc3BvbnNlcyh0aGlzLnF1ZXN0aW9ubmFpcmUucXVlc3Rpb25MaW5lc1tpXSwgcmVzcG9uc2VzKTtcbiAgICAgIH1cblxuICAgICAgdGhpcy5wcm9qZWN0U2VydmljZS51cGRhdGVRdWVzdGlvbm5haXJlKHRoaXMuc3RlcEluc3RhbmNlLmlkLCByZXNwb25zZXMsIHN1Ym1pdCwgdGhpcy51c2VySWQpLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgICAgaWYgKHN1Ym1pdCkge1xuICAgICAgICAgIHRoaXMucGFnZUluZGV4ID0gMDtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuc3RlcENvbXBsZXRlZExpc3RlbmVyLmVtaXQoe1xuICAgICAgICAgIHZhbHVlOiB0aGlzLnN0ZXBJbnN0YW5jZVxuICAgICAgICB9KTtcblxuICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIGNhbmNlbCgpIHtcbiAgICB0aGlzLmNhbmNlbExpc3RlbmVyLmVtaXQoe1xuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBhZGRSZXNwb25zZXMocXVlc3Rpb25MaW5lOiBRdWVzdGlvbkxpbmUsIHJlc3BvbnNlczogUXVlc3Rpb25SZXNwb25zZVtdKSB7XG4gICAgaWYgKCFxdWVzdGlvbkxpbmUpXG4gICAgICByZXR1cm47XG5cbiAgICBpZiAocXVlc3Rpb25MaW5lLmFuc3dlclR5cGUgPT09ICdDSEVDS19CT1gnIHx8IHF1ZXN0aW9uTGluZS5hbnN3ZXJUeXBlID09PSAnUkFESU9fQlVUVE9OJykge1xuICAgICAgbGV0IHF1ZXN0aW9uUmVzcG9uc2UgPSBuZXcgUXVlc3Rpb25SZXNwb25zZSgpO1xuICAgICAgcXVlc3Rpb25SZXNwb25zZS5xdWVzdGlvbkxpbmVJZCA9IHF1ZXN0aW9uTGluZS5pZDtcbiAgICAgIHF1ZXN0aW9uUmVzcG9uc2Uud2VpZ2h0ID0gcXVlc3Rpb25MaW5lLndlaWdodDtcbiAgICAgIHF1ZXN0aW9uUmVzcG9uc2Uuc2VsZWN0ZWQgPSBxdWVzdGlvbkxpbmUucXVlc3Rpb25SZXNwb25zZS5zZWxlY3RlZDtcbiAgICAgIHJlc3BvbnNlcy5wdXNoKHF1ZXN0aW9uUmVzcG9uc2UpO1xuICAgIH0gZWxzZSBpZiAocXVlc3Rpb25MaW5lLmFuc3dlclR5cGUgPT09ICdURVhUJyB8fCBxdWVzdGlvbkxpbmUuYW5zd2VyVHlwZSA9PT0gJ1RFWFRfQVJFQScgfHxcbiAgICAgICAgcXVlc3Rpb25MaW5lLmFuc3dlclR5cGUgPT09ICdOVU1CRVInIHx8IHF1ZXN0aW9uTGluZS5hbnN3ZXJUeXBlID09PSAnSU1BR0UnKSB7XG4gICAgICBsZXQgcXVlc3Rpb25SZXNwb25zZSA9IG5ldyBRdWVzdGlvblJlc3BvbnNlKCk7XG4gICAgICBxdWVzdGlvblJlc3BvbnNlLnF1ZXN0aW9uTGluZUlkID0gcXVlc3Rpb25MaW5lLmlkO1xuICAgICAgcXVlc3Rpb25SZXNwb25zZS53ZWlnaHQgPSBxdWVzdGlvbkxpbmUud2VpZ2h0O1xuICAgICAgcXVlc3Rpb25SZXNwb25zZS5yZXNwb25zZSA9IHF1ZXN0aW9uTGluZS5xdWVzdGlvblJlc3BvbnNlLnJlc3BvbnNlO1xuICAgICAgcmVzcG9uc2VzLnB1c2gocXVlc3Rpb25SZXNwb25zZSk7XG4gICAgfSBlbHNlIGlmIChxdWVzdGlvbkxpbmUuYW5zd2VyVHlwZSA9PT0gJ1NFTEVDVCcpIHtcbiAgICAgIGxldCBxdWVzdGlvblJlc3BvbnNlID0gbmV3IFF1ZXN0aW9uUmVzcG9uc2UoKTtcbiAgICAgIHF1ZXN0aW9uUmVzcG9uc2UucXVlc3Rpb25MaW5lSWQgPSBxdWVzdGlvbkxpbmUuaWQ7XG4gICAgICBxdWVzdGlvblJlc3BvbnNlLndlaWdodCA9IHF1ZXN0aW9uTGluZS5xdWVzdGlvblJlc3BvbnNlLndlaWdodDtcbiAgICAgIGlmIChxdWVzdGlvbkxpbmUucXVlc3Rpb25SZXNwb25zZS53ZWlnaHQpIHtcbiAgICAgICAgcXVlc3Rpb25SZXNwb25zZS5zZWxlY3RlZCA9IHRydWU7XG4gICAgICAgIHF1ZXN0aW9uUmVzcG9uc2UucmVzcG9uc2UgPSBxdWVzdGlvbkxpbmUucXVlc3Rpb25SZXNwb25zZS5yZXNwb25zZTtcbiAgICAgIH1cblxuICAgICAgcmVzcG9uc2VzLnB1c2gocXVlc3Rpb25SZXNwb25zZSk7XG4gICAgfSBlbHNlIGlmICgocXVlc3Rpb25MaW5lLmxpbmVUeXBlID09PSAnU0lOR0xFX1NFTEVDVCcgfHwgcXVlc3Rpb25MaW5lLmxpbmVUeXBlID09PSAnTVVMVElfU0VMRUNUJykgJiZcbiAgICAgICAgKHF1ZXN0aW9uTGluZS5hbGxvd1N1cHBvcnRpbmdEZXNjcmlwdGlvbiB8fCBxdWVzdGlvbkxpbmUuYWxsb3dTdXBwb3J0aW5nSW1hZ2UpKSB7XG4gICAgICBsZXQgcXVlc3Rpb25SZXNwb25zZSA9IG5ldyBRdWVzdGlvblJlc3BvbnNlKCk7XG4gICAgICBxdWVzdGlvblJlc3BvbnNlLnF1ZXN0aW9uTGluZUlkID0gcXVlc3Rpb25MaW5lLmlkO1xuICAgICAgcXVlc3Rpb25SZXNwb25zZS5zdXBwb3J0aW5nRGVzY3JpcHRpb24gPSBxdWVzdGlvbkxpbmUucXVlc3Rpb25SZXNwb25zZS5zdXBwb3J0aW5nRGVzY3JpcHRpb247XG4gICAgICBxdWVzdGlvblJlc3BvbnNlLnN1cHBvcnRpbmdJbWFnZSA9IHF1ZXN0aW9uTGluZS5xdWVzdGlvblJlc3BvbnNlLnN1cHBvcnRpbmdJbWFnZTtcbiAgICAgIHJlc3BvbnNlcy5wdXNoKHF1ZXN0aW9uUmVzcG9uc2UpO1xuICAgIH1cblxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcXVlc3Rpb25MaW5lLmNoaWxkcmVuLmxlbmd0aDsgaSsrKSB7XG4gICAgICB0aGlzLmFkZFJlc3BvbnNlcyhxdWVzdGlvbkxpbmUuY2hpbGRyZW5baV0sIHJlc3BvbnNlcyk7XG4gICAgfVxuICB9O1xuXG4gIHByaXZhdGUgdmFsaWRhdGVRdWVzdGlvbkxpbmUocXVlc3Rpb25MaW5lOiBRdWVzdGlvbkxpbmUpIHtcbiAgICBpZiAoIXRoaXMuaGFzVmFsaWRBbnN3ZXIocXVlc3Rpb25MaW5lKSlcbiAgICAgIHRocm93IFwiSW52YWxpZCByZXNwb25zZVwiO1xuXG4gICAgaWYgKHF1ZXN0aW9uTGluZS5jaGlsZHJlbikge1xuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBxdWVzdGlvbkxpbmUuY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgdmFyIGNoaWxkID0gcXVlc3Rpb25MaW5lLmNoaWxkcmVuW2ldO1xuICAgICAgICBpZiAoY2hpbGQuYW5zd2VyVHlwZSA9PT0gXCJDSEVDS19CT1hcIiB8fCBjaGlsZC5hbnN3ZXJUeXBlID09PSBcIlJBRElPX0JVVFRPTlwiKSB7XG4gICAgICAgICAgdmFyIGNoaWxkUmVzcG9uc2UgPSBxdWVzdGlvbkxpbmUucXVlc3Rpb25SZXNwb25zZTtcbiAgICAgICAgICBpZiAoY2hpbGRSZXNwb25zZSAmJiBjaGlsZFJlc3BvbnNlLnNlbGVjdGVkKSB7XG4gICAgICAgICAgICB0aGlzLnZhbGlkYXRlUXVlc3Rpb25MaW5lKGNoaWxkKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGhpcy52YWxpZGF0ZVF1ZXN0aW9uTGluZShjaGlsZCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGhhc1ZhbGlkQW5zd2VyKHF1ZXN0aW9uTGluZTogUXVlc3Rpb25MaW5lKSB7XG4gICAgaWYgKCFxdWVzdGlvbkxpbmUubGluZVR5cGUgfHwgIXF1ZXN0aW9uTGluZS5hbnN3ZXJUeXBlKVxuICAgICAgcmV0dXJuIHRydWU7XG5cbiAgICBpZiAocXVlc3Rpb25MaW5lLmxpbmVUeXBlID09PSBcIk1VTFRJX1NFTEVDVFwiIHx8IHF1ZXN0aW9uTGluZS5saW5lVHlwZSA9PT0gXCJTSU5HTEVfU0VMRUNUXCIpIHtcbiAgICAgIGlmIChxdWVzdGlvbkxpbmUucmVxdWlyZWQgJiYgcXVlc3Rpb25MaW5lLmNoaWxkcmVuKSB7XG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcXVlc3Rpb25MaW5lLmNoaWxkcmVuLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgaWYgKHF1ZXN0aW9uTGluZS5jaGlsZHJlbltpXS5xdWVzdGlvblJlc3BvbnNlICYmIHF1ZXN0aW9uTGluZS5jaGlsZHJlbltpXS5xdWVzdGlvblJlc3BvbnNlLnNlbGVjdGVkKVxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH1cbiAgICB9IGVsc2UgaWYgKHF1ZXN0aW9uTGluZS5hbnN3ZXJUeXBlID09PSBcIk5VTUJFUlwiIHx8IHF1ZXN0aW9uTGluZS5hbnN3ZXJUeXBlID09PSBcIlNFTEVDVFwiIHx8XG4gICAgICAgICAgICBxdWVzdGlvbkxpbmUuYW5zd2VyVHlwZSA9PT0gXCJURVhUXCIgfHwgcXVlc3Rpb25MaW5lLmFuc3dlclR5cGUgPT09IFwiVEVYVF9BUkVBXCIgfHwgcXVlc3Rpb25MaW5lLmFuc3dlclR5cGUgPT09IFwiSU1BR0VcIikge1xuICAgICAgaWYgKHF1ZXN0aW9uTGluZS5yZXF1aXJlZCAmJiAoIXF1ZXN0aW9uTGluZS5xdWVzdGlvblJlc3BvbnNlIHx8ICFxdWVzdGlvbkxpbmUucXVlc3Rpb25SZXNwb25zZS5yZXNwb25zZSkpXG4gICAgICAgIHJldHVybiBmYWxzZTtcblxuICAgICAgaWYgKHF1ZXN0aW9uTGluZS5hbnN3ZXJUeXBlID09PSBcIk5VTUJFUlwiKSB7XG4gICAgICAgIGlmICgocXVlc3Rpb25MaW5lLnJlcXVpcmVkICYmIGlzTmFOKHBhcnNlSW50KHF1ZXN0aW9uTGluZS5xdWVzdGlvblJlc3BvbnNlLnJlc3BvbnNlKSkpIHx8XG4gICAgICAgICAgICAgICAgKCFxdWVzdGlvbkxpbmUucmVxdWlyZWQgJiYgcXVlc3Rpb25MaW5lLnF1ZXN0aW9uUmVzcG9uc2UucmVzcG9uc2UgJiYgaXNOYU4ocGFyc2VJbnQocXVlc3Rpb25MaW5lLnF1ZXN0aW9uUmVzcG9uc2UucmVzcG9uc2UpKSkpIHtcbiAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cblxuICAgICAgICB0cnkge1xuICAgICAgICAgIHZhciBkID0gcGFyc2VGbG9hdChxdWVzdGlvbkxpbmUucXVlc3Rpb25SZXNwb25zZS5yZXNwb25zZSk7XG4gICAgICAgICAgdmFyIHAgPSBxdWVzdGlvbkxpbmUucGFyYW1ldGVycy5zcGxpdChcInxcIik7XG4gICAgICAgICAgaWYgKHAubGVuZ3RoID49IDQpIHtcbiAgICAgICAgICAgIHZhciBoYXNMaW1pdHMgPSBcInRydWVcIiA9PT0gcFsxXTtcbiAgICAgICAgICAgIHZhciBtYXggPSBwYXJzZUZsb2F0KHBbMl0pO1xuICAgICAgICAgICAgdmFyIG1pbiA9IHBhcnNlRmxvYXQocFszXSk7XG4gICAgICAgICAgICBpZiAoaGFzTGltaXRzICYmIChkIDwgbWluIHx8IGQgPiBtYXgpICYmICFxdWVzdGlvbkxpbmUuZXJyb3JBY2NlcHRlZClcbiAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgIH1cbiAgICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuXG4gIHByaXZhdGUgc2V0QWxsVmFsaWRhdGlvbk1lc3NhZ2VzKHF1ZXN0aW9uTGluZTogUXVlc3Rpb25MaW5lKSB7XG4gICAgdGhpcy5zZXRWYWxpZGF0aW9uTWVzc2FnZShxdWVzdGlvbkxpbmUpO1xuICAgIGlmIChxdWVzdGlvbkxpbmUuY2hpbGRyZW4pIHtcbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcXVlc3Rpb25MaW5lLmNoaWxkcmVuLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIHZhciBjaGlsZCA9IHF1ZXN0aW9uTGluZS5jaGlsZHJlbltpXTtcbiAgICAgICAgaWYgKGNoaWxkLmFuc3dlclR5cGUgPT09IFwiQ0hFQ0tfQk9YXCIgfHwgY2hpbGQuYW5zd2VyVHlwZSA9PT0gXCJSQURJT19CVVRUT05cIikge1xuICAgICAgICAgIHZhciBjaGlsZFJlc3BvbnNlID0gY2hpbGQucXVlc3Rpb25SZXNwb25zZTtcbiAgICAgICAgICBpZiAoY2hpbGRSZXNwb25zZSAmJiBjaGlsZFJlc3BvbnNlLnNlbGVjdGVkKSB7XG4gICAgICAgICAgICB0aGlzLnNldEFsbFZhbGlkYXRpb25NZXNzYWdlcyhjaGlsZCk7XG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRoaXMuc2V0QWxsVmFsaWRhdGlvbk1lc3NhZ2VzKGNoaWxkKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgc2V0VmFsaWRhdGlvbk1lc3NhZ2UocXVlc3Rpb25MaW5lOiBRdWVzdGlvbkxpbmUpIHtcbiAgICBpZiAoIXF1ZXN0aW9uTGluZS5saW5lVHlwZSB8fCAhcXVlc3Rpb25MaW5lLmFuc3dlclR5cGUpIHtcbiAgICAgIHF1ZXN0aW9uTGluZS5oYXNFcnJvciA9IGZhbHNlO1xuICAgICAgcXVlc3Rpb25MaW5lLmVycm9yTWVzc2FnZSA9IG51bGw7XG4gICAgICBxdWVzdGlvbkxpbmUuc2hvd0Vycm9yQ29uZmlybWF0aW9uID0gZmFsc2U7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKHF1ZXN0aW9uTGluZS5saW5lVHlwZSA9PT0gXCJNVUxUSV9TRUxFQ1RcIiB8fCBxdWVzdGlvbkxpbmUubGluZVR5cGUgPT09IFwiU0lOR0xFX1NFTEVDVFwiKSB7XG4gICAgICBpZiAocXVlc3Rpb25MaW5lLnJlcXVpcmVkICYmIHF1ZXN0aW9uTGluZS5jaGlsZHJlbikge1xuICAgICAgICB2YXIgc2VsZWN0ZWQgPSBmYWxzZTtcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBxdWVzdGlvbkxpbmUuY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICBpZiAocXVlc3Rpb25MaW5lLmNoaWxkcmVuW2ldLnF1ZXN0aW9uUmVzcG9uc2UgJiYgcXVlc3Rpb25MaW5lLmNoaWxkcmVuW2ldLnF1ZXN0aW9uUmVzcG9uc2Uuc2VsZWN0ZWQpIHtcbiAgICAgICAgICAgIHNlbGVjdGVkID0gdHJ1ZTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGlmICghc2VsZWN0ZWQpIHtcbiAgICAgICAgICBxdWVzdGlvbkxpbmUuaGFzRXJyb3IgPSB0cnVlO1xuICAgICAgICAgIHF1ZXN0aW9uTGluZS5zaG93RXJyb3JDb25maXJtYXRpb24gPSBmYWxzZTtcbiAgICAgICAgICBxdWVzdGlvbkxpbmUuZXJyb3JNZXNzYWdlID0gXCJBbiBhbnN3ZXIgbXVzdCBiZSBzZWxlY3RlZFwiO1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0gZWxzZSBpZiAocXVlc3Rpb25MaW5lLmFuc3dlclR5cGUgPT09IFwiTlVNQkVSXCIgfHwgcXVlc3Rpb25MaW5lLmFuc3dlclR5cGUgPT09IFwiU0VMRUNUXCIgfHxcbiAgICAgICAgICAgIHF1ZXN0aW9uTGluZS5hbnN3ZXJUeXBlID09PSBcIlRFWFRcIiB8fCBxdWVzdGlvbkxpbmUuYW5zd2VyVHlwZSA9PT0gXCJURVhUX0FSRUFcIiB8fFxuICAgICAgICAgICAgcXVlc3Rpb25MaW5lLmFuc3dlclR5cGUgPT09IFwiSU1BR0VcIikge1xuICAgICAgdmFyIHF1ZXN0aW9uUmVzcG9uc2UgPSBxdWVzdGlvbkxpbmUucXVlc3Rpb25SZXNwb25zZTtcbiAgICAgIGlmIChxdWVzdGlvbkxpbmUucmVxdWlyZWQgJiYgKCFxdWVzdGlvblJlc3BvbnNlIHx8ICFxdWVzdGlvblJlc3BvbnNlLnJlc3BvbnNlKSkge1xuICAgICAgICBxdWVzdGlvbkxpbmUuaGFzRXJyb3IgPSB0cnVlO1xuICAgICAgICBxdWVzdGlvbkxpbmUuc2hvd0Vycm9yQ29uZmlybWF0aW9uID0gZmFsc2U7XG4gICAgICAgIHF1ZXN0aW9uTGluZS5lcnJvck1lc3NhZ2UgPSBcIkFuIGFuc3dlciBtdXN0IGJlIHByb3ZpZGVkXCI7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgaWYgKHF1ZXN0aW9uTGluZS5hbnN3ZXJUeXBlID09PSBcIk5VTUJFUlwiKSB7XG4gICAgICAgIGlmICgocXVlc3Rpb25MaW5lLnJlcXVpcmVkICYmIGlzTmFOKHBhcnNlSW50KHF1ZXN0aW9uUmVzcG9uc2UucmVzcG9uc2UpKSkgfHxcbiAgICAgICAgICAgICAgICAoIXF1ZXN0aW9uTGluZS5yZXF1aXJlZCAmJiBxdWVzdGlvblJlc3BvbnNlLnJlc3BvbnNlICYmIGlzTmFOKHBhcnNlSW50KHF1ZXN0aW9uUmVzcG9uc2UucmVzcG9uc2UpKSkpIHtcbiAgICAgICAgICBxdWVzdGlvbkxpbmUuaGFzRXJyb3IgPSB0cnVlO1xuICAgICAgICAgIHF1ZXN0aW9uTGluZS5zaG93RXJyb3JDb25maXJtYXRpb24gPSBmYWxzZTtcbiAgICAgICAgICBxdWVzdGlvbkxpbmUuZXJyb3JNZXNzYWdlID0gXCJBbiBpbnZhbGlkIG51bWJlciB3YXMgZW50ZXJlZFwiO1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgdmFyIGQgPSBwYXJzZUZsb2F0KHF1ZXN0aW9uUmVzcG9uc2UucmVzcG9uc2UpO1xuICAgICAgICAgIHZhciBwID0gcXVlc3Rpb25MaW5lLnBhcmFtZXRlcnMuc3BsaXQoXCJ8XCIpO1xuICAgICAgICAgIGlmIChwLmxlbmd0aCA+PSA0KSB7XG4gICAgICAgICAgICB2YXIgaGFzTGltaXRzID0gXCJ0cnVlXCIgPT09IHBbMV07XG4gICAgICAgICAgICB2YXIgbWF4ID0gcGFyc2VGbG9hdChwWzJdKTtcbiAgICAgICAgICAgIHZhciBtaW4gPSBwYXJzZUZsb2F0KHBbM10pO1xuICAgICAgICAgICAgaWYgKGhhc0xpbWl0cykge1xuICAgICAgICAgICAgICBpZiAoZCA8IG1pbikge1xuICAgICAgICAgICAgICAgIHF1ZXN0aW9uTGluZS5oYXNFcnJvciA9IHRydWU7XG4gICAgICAgICAgICAgICAgcXVlc3Rpb25MaW5lLnNob3dFcnJvckNvbmZpcm1hdGlvbiA9IHRydWU7XG4gICAgICAgICAgICAgICAgcXVlc3Rpb25MaW5lLmVycm9yTWVzc2FnZSA9IFwiVGhlIHZhbHVlIHByb3ZpZGVkIGlzIGxlc3MgdGhhbiB0aGUgbWluaW11bSB2YWx1ZSBvZiBcIiArIG1pbiArIFwiLiAgQXJlIHlvdSBzdXJlIHRoaXMgaXMgY29ycmVjdD9cIjtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgIH0gZWxzZSBpZiAoZCA+IG1heCkge1xuICAgICAgICAgICAgICAgIHF1ZXN0aW9uTGluZS5oYXNFcnJvciA9IHRydWU7XG4gICAgICAgICAgICAgICAgcXVlc3Rpb25MaW5lLnNob3dFcnJvckNvbmZpcm1hdGlvbiA9IHRydWU7XG4gICAgICAgICAgICAgICAgcXVlc3Rpb25MaW5lLmVycm9yTWVzc2FnZSA9IFwiVGhlIHZhbHVlIHByb3ZpZGVkIGV4Y2VlZHMgdGhlIG1heGltdW0gdmFsdWUgb2YgXCIgKyBtYXggKyBcIi4gIEFyZSB5b3Ugc3VyZSB0aGlzIGlzIGNvcnJlY3Q/XCI7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICAgIHF1ZXN0aW9uTGluZS5oYXNFcnJvciA9IHRydWU7XG4gICAgICAgICAgcXVlc3Rpb25MaW5lLnNob3dFcnJvckNvbmZpcm1hdGlvbiA9IGZhbHNlO1xuICAgICAgICAgIHF1ZXN0aW9uTGluZS5lcnJvck1lc3NhZ2UgPSBcIkFuIGludmFsaWQgbnVtYmVyIHdhcyBzdXBwbGllZFwiO1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIHF1ZXN0aW9uTGluZS5oYXNFcnJvciA9IGZhbHNlO1xuICAgIHF1ZXN0aW9uTGluZS5zaG93RXJyb3JDb25maXJtYXRpb24gPSBmYWxzZTtcbiAgICBxdWVzdGlvbkxpbmUuZXJyb3JNZXNzYWdlID0gXCJcIjtcbiAgfVxuXG4gIHRha2VQaWN0dXJlUXVlc3Rpb25uYWlyZShxdWVzdGlvbkxpbmU6IFF1ZXN0aW9uTGluZSkge1xuICAgIGxldCBpbWFnZUNvbnRhaW5lcjogSW1hZ2VDb250YWluZXIgPSB7XG4gICAgICBzZXRJbWFnZShpbWFnZTogc3RyaW5nKSB7XG4gICAgICAgIHF1ZXN0aW9uTGluZS5xdWVzdGlvblJlc3BvbnNlLnJlc3BvbnNlID0gXCJkYXRhOmltYWdlL2pwZWc7YmFzZTY0LFwiICsgaW1hZ2U7XG4gICAgICB9XG4gICAgfTtcbiAgICB0aGlzLm9uVGFrZVBpY3R1cmUuZW1pdChpbWFnZUNvbnRhaW5lcik7XG4gIH1cblxuICB0YWtlU3VwcG9ydGluZ1BpY3R1cmVRdWVzdGlvbm5haXJlKHF1ZXN0aW9uTGluZTogUXVlc3Rpb25MaW5lKSB7XG4gICAgbGV0IGltYWdlQ29udGFpbmVyOiBJbWFnZUNvbnRhaW5lciA9IHtcbiAgICAgIHNldEltYWdlKGltYWdlOiBzdHJpbmcpIHtcbiAgICAgICAgcXVlc3Rpb25MaW5lLnF1ZXN0aW9uUmVzcG9uc2Uuc3VwcG9ydGluZ0ltYWdlID0gXCJkYXRhOmltYWdlL2pwZWc7YmFzZTY0LFwiICsgaW1hZ2U7XG4gICAgICB9XG4gICAgfTtcbiAgICB0aGlzLm9uVGFrZVBpY3R1cmUuZW1pdChpbWFnZUNvbnRhaW5lcik7XG4gIH1cblxuICBxdWVzdGlvbkFuc3dlcmVkKHF1ZXN0aW9uTGluZTogUXVlc3Rpb25MaW5lKSB7XG4gICAgY29uc29sZS5sb2coXCJxdWVzdGlvbm5haXJlLnF1ZXN0aW9uQW5zd2VyZWQ6IFwiICsgcXVlc3Rpb25MaW5lLndvcmRpbmcgKyBcIiBcIiArIHF1ZXN0aW9uTGluZS5pbnRlZ3JhdGlvbkluZGljYXRvciArIFwiIFwiICsgcXVlc3Rpb25MaW5lLmlkKTtcbiAgICB0aGlzLm9uUXVlc3Rpb25BbnN3ZXJlZC5lbWl0KHF1ZXN0aW9uTGluZSk7XG4gIH1cbn1cbiJdfQ==