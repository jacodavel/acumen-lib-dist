/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Output } from '@angular/core';
export class TextQuestionComponent {
    constructor() {
        this.close = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
}
TextQuestionComponent.decorators = [
    { type: Component, args: [{
                selector: 'text-question',
                template: "<!--<form class=\"form-horizontal\">\n    <div class=\"form-group\">\n        <label for=\"textQuestion\" class=\"col-sm-6 text-left\">Text question text</label>\n        <div class=\"col-sm-6\">\n            <input type=\"text\" class=\"form-control\" id=\"textQuestion\" placeholder=\"\">\n        </div>\n    </div>\n</form>-->\n\n<div class=\"row\">\n    <div class=\"col-md-6\"><label class=\"text-muted\">&#9679;&nbsp;&nbsp;Question text</label></div>\n    <div class=\"col-md-6\"><input type=\"text\" class=\"form-control\" id=\"exampleInputEmail1\" placeholder=\"\"></div>\n</div>\n"
            }] }
];
/** @nocollapse */
TextQuestionComponent.ctorParameters = () => [];
TextQuestionComponent.propDecorators = {
    close: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    TextQuestionComponent.prototype.close;
    /** @type {?} */
    TextQuestionComponent.prototype.error;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dC1xdWVzdGlvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL3N0ZXBzL3F1ZXN0aW9ubmFpcmUvdGV4dC1xdWVzdGlvbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUE0QixNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFNMUYsTUFBTSxPQUFPLHFCQUFxQjtJQUloQztRQUhVLFVBQUssR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO0lBSXJDLENBQUM7Ozs7SUFFRCxRQUFRO0lBQ1IsQ0FBQzs7OztJQUVELFdBQVc7SUFDWCxDQUFDOzs7WUFmRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGVBQWU7Z0JBQ3pCLDBsQkFBNkM7YUFDOUM7Ozs7O29CQUVFLE1BQU07Ozs7SUFBUCxzQ0FBcUM7O0lBQ3JDLHNDQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPbkluaXQsIE9uRGVzdHJveSwgT3V0cHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3RleHQtcXVlc3Rpb24nLFxuICB0ZW1wbGF0ZVVybDogJy4vdGV4dC1xdWVzdGlvbi5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgVGV4dFF1ZXN0aW9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuICBAT3V0cHV0KCkgY2xvc2UgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIGVycm9yOiBhbnk7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCkge1xuICB9XG59XG4iXX0=