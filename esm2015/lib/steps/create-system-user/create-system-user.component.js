/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { StepInstance } from '../../domain/step-instance';
export class CreateSystemUserComponent {
    /**
     * @param {?} projectService
     */
    constructor(projectService) {
        this.projectService = projectService;
        this.stepCompletedListener = new EventEmitter();
        this.cancelListener = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.usernameError = false;
        this.usernameExistsError = false;
        this.password1Error = false;
        this.password2Error = false;
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @return {?}
     */
    completeStep() {
        this.usernameError = false;
        this.usernameExistsError = false;
        this.password1Error = false;
        this.password2Error = false;
        if (!this.username) {
            this.usernameError = true;
            return;
        }
        if (!this.password1) {
            this.password1Error = true;
            return;
        }
        if (this.password1 !== this.password2) {
            this.password2Error = true;
            return;
        }
        this.projectService.createSystemUser(this.username, this.password1, this.stepInstance.businessProcessInstance.actor.id).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            if (result["response"] === "Success") {
                this.projectService.completeStep(this.stepInstance.id, this.userId).then((/**
                 * @param {?} result
                 * @return {?}
                 */
                result => {
                    this.stepCompletedListener.emit({
                        value: this.stepInstance,
                        useBusinessProcessId: true
                    });
                }));
            }
            else {
                this.usernameExistsError = true;
            }
        }));
    }
    /**
     * @return {?}
     */
    cancel() {
        this.cancelListener.emit({});
    }
}
CreateSystemUserComponent.decorators = [
    { type: Component, args: [{
                selector: 'create-system-user',
                template: "<div class=\"create-system-user\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div class=\"heading\">\n    Create System User\n  </div>\n\n  <div *ngIf=\"!loading\" class=\"content\">\n    <div class=\"component-row\">\n      <div class=\"component-col\">\n        <div class=\"component-label\">\n          Username\n          <div *ngIf=\"usernameError\" class=\"error-message\">A username must be entered</div>\n          <div *ngIf=\"usernameExistsError\" class=\"error-message\">The username already exists</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"text\" [(ngModel)]=\"username\">\n        </div>\n      </div>\n    </div>\n\n    <div class=\"component-row\">\n      <div class=\"component-col\">\n        <div class=\"component-label\">\n          Password\n          <div *ngIf=\"password1Error\" class=\"error-message\">A password be entered</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"password\" [(ngModel)]=\"password1\">\n        </div>\n      </div>\n      <div class=\"component-col\">\n        <div class=\"component-label\">\n          Re-type password\n          <div *ngIf=\"password2Error\" class=\"error-message\">The second password differs from the first password</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"password\" [(ngModel)]=\"password2\">\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!loading\" class=\"button-section\">\n    <div class=\"button\" (click)=\"completeStep()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div class=\"button button-outline\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n  <div *ngIf=\"loading\" class=\"button-section\">\n    <div class=\"button button-disabled\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div class=\"button button-outline-disabled\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                styles: [".create-system-user{width:100%;height:100%;position:relative}.create-system-user .heading{position:absolute;top:0;width:100%;height:32px;font-size:16px;padding:8px}.create-system-user .content{position:absolute;top:32px;left:0;bottom:50px;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px;box-sizing:border-box}.create-system-user .content .info-message{font-size:15px}.create-system-user .content .component-row{width:100%}.create-system-user .content .component-row .component-col{width:100%;display:inline-block;box-sizing:border-box;padding-top:10px}.create-system-user .content .component-row .component-col .component-label{width:100%;font-size:15px;padding-bottom:5px}.create-system-user .content .component-row .component-col .error-message{font-style:italic;font-size:11px;color:#f04141;padding-bottom:5px}.create-system-user .content .component-row .component-col .component{width:100%}.create-system-user .content .component-row .component-col .component input{width:100%;font-size:13px;border:1px solid #58666c;background-color:#fffcf6;box-sizing:border-box;padding:3px 5px;border-radius:5px;height:30px}.create-system-user .content .component-row:first-child .component-col:first-child{padding-top:0}@media (min-width:600px){.create-system-user .content .component-row .component-col{width:50%}.create-system-user .content .component-row .component-col:first-child{padding-right:16px}.create-system-user .content .component-row:first-child .component-col{padding-top:0}}.create-system-user .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.create-system-user .button-section .button{width:70px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.create-system-user .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.create-system-user .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.create-system-user .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b}.create-system-user .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b}"]
            }] }
];
/** @nocollapse */
CreateSystemUserComponent.ctorParameters = () => [
    { type: ProjectService }
];
CreateSystemUserComponent.propDecorators = {
    stepInstance: [{ type: Input }],
    userId: [{ type: Input }],
    stepCompletedListener: [{ type: Output }],
    cancelListener: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    CreateSystemUserComponent.prototype.stepInstance;
    /** @type {?} */
    CreateSystemUserComponent.prototype.userId;
    /** @type {?} */
    CreateSystemUserComponent.prototype.stepCompletedListener;
    /** @type {?} */
    CreateSystemUserComponent.prototype.cancelListener;
    /** @type {?} */
    CreateSystemUserComponent.prototype.username;
    /** @type {?} */
    CreateSystemUserComponent.prototype.password1;
    /** @type {?} */
    CreateSystemUserComponent.prototype.password2;
    /** @type {?} */
    CreateSystemUserComponent.prototype.loading;
    /** @type {?} */
    CreateSystemUserComponent.prototype.usernameError;
    /** @type {?} */
    CreateSystemUserComponent.prototype.usernameExistsError;
    /** @type {?} */
    CreateSystemUserComponent.prototype.password1Error;
    /** @type {?} */
    CreateSystemUserComponent.prototype.password2Error;
    /**
     * @type {?}
     * @private
     */
    CreateSystemUserComponent.prototype.projectService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3JlYXRlLXN5c3RlbS11c2VyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvc3RlcHMvY3JlYXRlLXN5c3RlbS11c2VyL2NyZWF0ZS1zeXN0ZW0tdXNlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBcUIsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTFGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNoRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFPMUQsTUFBTSxPQUFPLHlCQUF5Qjs7OztJQWVwQyxZQUFvQixjQUE4QjtRQUE5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFaeEMsMEJBQXFCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUMzQyxtQkFBYyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7SUFZOUMsQ0FBQzs7OztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsbUJBQW1CLEdBQUcsS0FBSyxDQUFDO1FBQ2pDLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1FBQzVCLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO0lBQzlCLENBQUM7Ozs7SUFFRCxXQUFXO0lBQ1gsQ0FBQzs7OztJQUVELFlBQVk7UUFDVixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsbUJBQW1CLEdBQUcsS0FBSyxDQUFDO1FBQ2pDLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1FBQzVCLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1FBRTVCLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2xCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1lBQzFCLE9BQU87U0FDUjtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ25CLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1lBQzNCLE9BQU87U0FDUjtRQUVELElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ3JDLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1lBQzNCLE9BQU87U0FDUjtRQUVELElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsdUJBQXVCLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxNQUFNLENBQUMsRUFBRTtZQUNwSSxJQUFJLE1BQU0sQ0FBQyxVQUFVLENBQUMsS0FBSyxTQUFTLEVBQUU7Z0JBQ3BDLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJOzs7O2dCQUFDLE1BQU0sQ0FBQyxFQUFFO29CQUNoRixJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDO3dCQUM5QixLQUFLLEVBQUUsSUFBSSxDQUFDLFlBQVk7d0JBQ3hCLG9CQUFvQixFQUFFLElBQUk7cUJBQzNCLENBQUMsQ0FBQztnQkFDTCxDQUFDLEVBQUMsQ0FBQzthQUNKO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUM7YUFDakM7UUFDSCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7SUFFRCxNQUFNO1FBQ0osSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFDeEIsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7O1lBdkVGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsb0JBQW9CO2dCQUM5Qiw2Z0VBQWtEOzthQUVuRDs7OztZQVBRLGNBQWM7OzsyQkFTcEIsS0FBSztxQkFDTCxLQUFLO29DQUNMLE1BQU07NkJBQ04sTUFBTTs7OztJQUhQLGlEQUFvQzs7SUFDcEMsMkNBQXdCOztJQUN4QiwwREFBcUQ7O0lBQ3JELG1EQUE4Qzs7SUFDOUMsNkNBQWlCOztJQUNqQiw4Q0FBa0I7O0lBQ2xCLDhDQUFrQjs7SUFFbEIsNENBQWlCOztJQUNqQixrREFBdUI7O0lBQ3ZCLHdEQUE2Qjs7SUFDN0IsbURBQXdCOztJQUN4QixtREFBd0I7Ozs7O0lBRVosbURBQXNDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPbkluaXQsIE9uRGVzdHJveSwgT3V0cHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IFByb2plY3RTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvcHJvamVjdC5zZXJ2aWNlJztcbmltcG9ydCB7IFN0ZXBJbnN0YW5jZSB9IGZyb20gJy4uLy4uL2RvbWFpbi9zdGVwLWluc3RhbmNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnY3JlYXRlLXN5c3RlbS11c2VyJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2NyZWF0ZS1zeXN0ZW0tdXNlci5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2NyZWF0ZS1zeXN0ZW0tdXNlci5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIENyZWF0ZVN5c3RlbVVzZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIHN0ZXBJbnN0YW5jZTogU3RlcEluc3RhbmNlO1xuICBASW5wdXQoKSB1c2VySWQ6IG51bWJlcjtcbiAgQE91dHB1dCgpIHN0ZXBDb21wbGV0ZWRMaXN0ZW5lciA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIGNhbmNlbExpc3RlbmVyID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICB1c2VybmFtZTogc3RyaW5nO1xuICBwYXNzd29yZDE6IHN0cmluZztcbiAgcGFzc3dvcmQyOiBzdHJpbmc7XG5cbiAgbG9hZGluZzogYm9vbGVhbjtcbiAgdXNlcm5hbWVFcnJvcjogYm9vbGVhbjtcbiAgdXNlcm5hbWVFeGlzdHNFcnJvcjogYm9vbGVhbjtcbiAgcGFzc3dvcmQxRXJyb3I6IGJvb2xlYW47XG4gIHBhc3N3b3JkMkVycm9yOiBib29sZWFuO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcHJvamVjdFNlcnZpY2U6IFByb2plY3RTZXJ2aWNlKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLnVzZXJuYW1lRXJyb3IgPSBmYWxzZTtcbiAgICB0aGlzLnVzZXJuYW1lRXhpc3RzRXJyb3IgPSBmYWxzZTtcbiAgICB0aGlzLnBhc3N3b3JkMUVycm9yID0gZmFsc2U7XG4gICAgdGhpcy5wYXNzd29yZDJFcnJvciA9IGZhbHNlO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKSB7XG4gIH1cblxuICBjb21wbGV0ZVN0ZXAoKSB7XG4gICAgdGhpcy51c2VybmFtZUVycm9yID0gZmFsc2U7XG4gICAgdGhpcy51c2VybmFtZUV4aXN0c0Vycm9yID0gZmFsc2U7XG4gICAgdGhpcy5wYXNzd29yZDFFcnJvciA9IGZhbHNlO1xuICAgIHRoaXMucGFzc3dvcmQyRXJyb3IgPSBmYWxzZTtcblxuICAgIGlmICghdGhpcy51c2VybmFtZSkge1xuICAgICAgdGhpcy51c2VybmFtZUVycm9yID0gdHJ1ZTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBpZiAoIXRoaXMucGFzc3dvcmQxKSB7XG4gICAgICB0aGlzLnBhc3N3b3JkMUVycm9yID0gdHJ1ZTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBpZiAodGhpcy5wYXNzd29yZDEgIT09IHRoaXMucGFzc3dvcmQyKSB7XG4gICAgICB0aGlzLnBhc3N3b3JkMkVycm9yID0gdHJ1ZTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmNyZWF0ZVN5c3RlbVVzZXIodGhpcy51c2VybmFtZSwgdGhpcy5wYXNzd29yZDEsIHRoaXMuc3RlcEluc3RhbmNlLmJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlLmFjdG9yLmlkKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICBpZiAocmVzdWx0W1wicmVzcG9uc2VcIl0gPT09IFwiU3VjY2Vzc1wiKSB7XG4gICAgICAgIHRoaXMucHJvamVjdFNlcnZpY2UuY29tcGxldGVTdGVwKHRoaXMuc3RlcEluc3RhbmNlLmlkLCB0aGlzLnVzZXJJZCkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICAgIHRoaXMuc3RlcENvbXBsZXRlZExpc3RlbmVyLmVtaXQoe1xuICAgICAgICAgICAgdmFsdWU6IHRoaXMuc3RlcEluc3RhbmNlLFxuICAgICAgICAgICAgdXNlQnVzaW5lc3NQcm9jZXNzSWQ6IHRydWVcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLnVzZXJuYW1lRXhpc3RzRXJyb3IgPSB0cnVlO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgY2FuY2VsKCkge1xuICAgIHRoaXMuY2FuY2VsTGlzdGVuZXIuZW1pdCh7XG4gICAgfSk7XG4gIH1cbn1cbiJdfQ==