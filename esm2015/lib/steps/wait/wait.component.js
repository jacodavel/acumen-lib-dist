/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import * as momentImported from 'moment';
import { ProjectService } from '../../services/project.service';
import { StepInstance } from '../../domain/step-instance';
/** @type {?} */
const moment = momentImported;
export class WaitComponent {
    /**
     * @param {?} projectService
     */
    constructor(projectService) {
        this.projectService = projectService;
        this.stepCompletedListener = new EventEmitter();
        this.cancelListener = new EventEmitter();
        this.showOverride = false;
        this.loading = false;
    }
    /**
     * @param {?} s
     * @return {?}
     */
    set stepInstance(s) {
        this._stepInstance = s;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.projectService.getStepParameters(this._stepInstance.step.id).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            this.showOverride = result[2] && "true" === result[2];
            /** @type {?} */
            let messageParameter = result[3];
            if (messageParameter) {
                if (this._stepInstance.wakeUpDate && messageParameter.indexOf("${completionDate}") > 0) {
                    this.message = messageParameter.replace("${completionDate}", moment(this._stepInstance.wakeUpDate).format("YYYY-MM-DD HH:mm:ss"));
                }
                else {
                    this.message = messageParameter;
                }
            }
            else {
                if (this._stepInstance.wakeUpDate) {
                    this.message = "The process is currently waiting.  It will continue on " + moment(this._stepInstance.wakeUpDate).format("YYYY-MM-DD HH:mm:ss") + ".";
                }
                else {
                    this.message = "The process is currently waiting.";
                }
            }
        }));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @return {?}
     */
    completeStep() {
        this.loading = true;
        this.projectService.completeStep(this._stepInstance.id, this.userId).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            this.loading = false;
            this.stepCompletedListener.emit({
                value: this._stepInstance
            });
        }));
    }
    /**
     * @return {?}
     */
    cancel() {
        this.cancelListener.emit({});
    }
}
WaitComponent.decorators = [
    { type: Component, args: [{
                selector: 'wait',
                template: "<div class=\"wait-step\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div class=\"message-content\">\n    {{ message }}\n  </div>\n\n  <div *ngIf=\"!loading\" class=\"button-section\">\n    <div *ngIf=\"showOverride\" class=\"button\" (click)=\"completeStep()\">\n      <div class=\"button-text\">Override</div>\n    </div>\n    <div class=\"button button-outline\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n  <div *ngIf=\"loading\" class=\"button-section\">\n    <div *ngIf=\"showOverride\" class=\"button button-disabled\">\n      <div class=\"button-text\">Override</div>\n    </div>\n    <div class=\"button button-outline-disabled\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                styles: [".wait-step{width:100%;height:100%;position:relative}.wait-step .message-content{position:absolute;top:0;left:0;bottom:50px;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px;box-sizing:border-box}.wait-step .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.wait-step .button-section .button{width:120px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.wait-step .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.wait-step .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.wait-step .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b;cursor:default}.wait-step .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b;cursor:default}"]
            }] }
];
/** @nocollapse */
WaitComponent.ctorParameters = () => [
    { type: ProjectService }
];
WaitComponent.propDecorators = {
    _stepInstance: [{ type: Input }],
    userId: [{ type: Input }],
    stepCompletedListener: [{ type: Output }],
    cancelListener: [{ type: Output }],
    stepInstance: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    WaitComponent.prototype._stepInstance;
    /** @type {?} */
    WaitComponent.prototype.userId;
    /** @type {?} */
    WaitComponent.prototype.stepCompletedListener;
    /** @type {?} */
    WaitComponent.prototype.cancelListener;
    /** @type {?} */
    WaitComponent.prototype.message;
    /** @type {?} */
    WaitComponent.prototype.showOverride;
    /** @type {?} */
    WaitComponent.prototype.loading;
    /**
     * @type {?}
     * @private
     */
    WaitComponent.prototype.projectService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2FpdC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL3N0ZXBzL3dhaXQvd2FpdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBcUIsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzFGLE9BQU8sS0FBSyxjQUFjLE1BQU0sUUFBUSxDQUFDO0FBRXpDLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNoRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7O01BRXBELE1BQU0sR0FBRyxjQUFjO0FBTzdCLE1BQU0sT0FBTyxhQUFhOzs7O0lBY3hCLFlBQW9CLGNBQThCO1FBQTlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQVh4QywwQkFBcUIsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQzNDLG1CQUFjLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUU5QyxpQkFBWSxHQUFZLEtBQUssQ0FBQztRQUM5QixZQUFPLEdBQVksS0FBSyxDQUFDO0lBUXpCLENBQUM7Ozs7O0lBTkQsSUFDSSxZQUFZLENBQUMsQ0FBZTtRQUM5QixJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQztJQUN6QixDQUFDOzs7O0lBS0QsUUFBUTtRQUNOLElBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSTs7OztRQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQzlFLElBQUksQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLE1BQU0sS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7O2dCQUNsRCxnQkFBZ0IsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ2hDLElBQUksZ0JBQWdCLEVBQUU7Z0JBQ3BCLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLElBQUksZ0JBQWdCLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLEdBQUcsQ0FBQyxFQUFFO29CQUN0RixJQUFJLENBQUMsT0FBTyxHQUFHLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxNQUFNLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDO2lCQUNuSTtxQkFBTTtvQkFDTCxJQUFJLENBQUMsT0FBTyxHQUFHLGdCQUFnQixDQUFDO2lCQUNqQzthQUNGO2lCQUFNO2dCQUNMLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUU7b0JBQ2pDLElBQUksQ0FBQyxPQUFPLEdBQUcseURBQXlELEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUMsTUFBTSxDQUFDLHFCQUFxQixDQUFDLEdBQUcsR0FBRyxDQUFDO2lCQUN0SjtxQkFBTTtvQkFDTCxJQUFJLENBQUMsT0FBTyxHQUFHLG1DQUFtQyxDQUFDO2lCQUNwRDthQUNGO1FBQ0gsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7O0lBRUQsV0FBVztJQUNYLENBQUM7Ozs7SUFFRCxZQUFZO1FBQ1YsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxNQUFNLENBQUMsRUFBRTtZQUNqRixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztZQUNyQixJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDO2dCQUM5QixLQUFLLEVBQUUsSUFBSSxDQUFDLGFBQWE7YUFDMUIsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7O0lBRUQsTUFBTTtRQUNKLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQ3hCLENBQUMsQ0FBQztJQUNMLENBQUM7OztZQTFERixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLE1BQU07Z0JBQ2hCLGl5QkFBb0M7O2FBRXJDOzs7O1lBVFEsY0FBYzs7OzRCQVdwQixLQUFLO3FCQUNMLEtBQUs7b0NBQ0wsTUFBTTs2QkFDTixNQUFNOzJCQUtOLEtBQUs7Ozs7SUFSTixzQ0FBcUM7O0lBQ3JDLCtCQUF3Qjs7SUFDeEIsOENBQXFEOztJQUNyRCx1Q0FBOEM7O0lBQzlDLGdDQUFnQjs7SUFDaEIscUNBQThCOztJQUM5QixnQ0FBeUI7Ozs7O0lBT2IsdUNBQXNDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPbkluaXQsIE9uRGVzdHJveSwgT3V0cHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgKiBhcyBtb21lbnRJbXBvcnRlZCBmcm9tICdtb21lbnQnO1xuXG5pbXBvcnQgeyBQcm9qZWN0U2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL3Byb2plY3Quc2VydmljZSc7XG5pbXBvcnQgeyBTdGVwSW5zdGFuY2UgfSBmcm9tICcuLi8uLi9kb21haW4vc3RlcC1pbnN0YW5jZSc7XG5cbmNvbnN0IG1vbWVudCA9IG1vbWVudEltcG9ydGVkO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd3YWl0JyxcbiAgdGVtcGxhdGVVcmw6ICcuL3dhaXQuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi93YWl0LmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgV2FpdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgQElucHV0KCkgX3N0ZXBJbnN0YW5jZTogU3RlcEluc3RhbmNlO1xuICBASW5wdXQoKSB1c2VySWQ6IG51bWJlcjtcbiAgQE91dHB1dCgpIHN0ZXBDb21wbGV0ZWRMaXN0ZW5lciA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIGNhbmNlbExpc3RlbmVyID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBtZXNzYWdlOiBzdHJpbmc7XG4gIHNob3dPdmVycmlkZTogYm9vbGVhbiA9IGZhbHNlO1xuICBsb2FkaW5nOiBib29sZWFuID0gZmFsc2U7XG5cbiAgQElucHV0KClcbiAgc2V0IHN0ZXBJbnN0YW5jZShzOiBTdGVwSW5zdGFuY2UpIHtcbiAgICB0aGlzLl9zdGVwSW5zdGFuY2UgPSBzO1xuICB9XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBwcm9qZWN0U2VydmljZTogUHJvamVjdFNlcnZpY2UpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMucHJvamVjdFNlcnZpY2UuZ2V0U3RlcFBhcmFtZXRlcnModGhpcy5fc3RlcEluc3RhbmNlLnN0ZXAuaWQpLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgIHRoaXMuc2hvd092ZXJyaWRlID0gcmVzdWx0WzJdICYmIFwidHJ1ZVwiID09PSByZXN1bHRbMl07XG4gICAgICBsZXQgbWVzc2FnZVBhcmFtZXRlciA9IHJlc3VsdFszXTtcbiAgICAgIGlmIChtZXNzYWdlUGFyYW1ldGVyKSB7XG4gICAgICAgIGlmICh0aGlzLl9zdGVwSW5zdGFuY2Uud2FrZVVwRGF0ZSAmJiBtZXNzYWdlUGFyYW1ldGVyLmluZGV4T2YoXCIke2NvbXBsZXRpb25EYXRlfVwiKSA+IDApIHtcbiAgICAgICAgICB0aGlzLm1lc3NhZ2UgPSBtZXNzYWdlUGFyYW1ldGVyLnJlcGxhY2UoXCIke2NvbXBsZXRpb25EYXRlfVwiLCBtb21lbnQodGhpcy5fc3RlcEluc3RhbmNlLndha2VVcERhdGUpLmZvcm1hdChcIllZWVktTU0tREQgSEg6bW06c3NcIikpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRoaXMubWVzc2FnZSA9IG1lc3NhZ2VQYXJhbWV0ZXI7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGlmICh0aGlzLl9zdGVwSW5zdGFuY2Uud2FrZVVwRGF0ZSkge1xuICAgICAgICAgIHRoaXMubWVzc2FnZSA9IFwiVGhlIHByb2Nlc3MgaXMgY3VycmVudGx5IHdhaXRpbmcuICBJdCB3aWxsIGNvbnRpbnVlIG9uIFwiICsgbW9tZW50KHRoaXMuX3N0ZXBJbnN0YW5jZS53YWtlVXBEYXRlKS5mb3JtYXQoXCJZWVlZLU1NLUREIEhIOm1tOnNzXCIpICsgXCIuXCI7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGhpcy5tZXNzYWdlID0gXCJUaGUgcHJvY2VzcyBpcyBjdXJyZW50bHkgd2FpdGluZy5cIjtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKSB7XG4gIH1cblxuICBjb21wbGV0ZVN0ZXAoKSB7XG4gICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcbiAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmNvbXBsZXRlU3RlcCh0aGlzLl9zdGVwSW5zdGFuY2UuaWQsIHRoaXMudXNlcklkKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgIHRoaXMuc3RlcENvbXBsZXRlZExpc3RlbmVyLmVtaXQoe1xuICAgICAgICB2YWx1ZTogdGhpcy5fc3RlcEluc3RhbmNlXG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIGNhbmNlbCgpIHtcbiAgICB0aGlzLmNhbmNlbExpc3RlbmVyLmVtaXQoe1xuICAgIH0pO1xuICB9XG59XG4iXX0=