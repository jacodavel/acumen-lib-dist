/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { ActorService } from '../../services/actor.service';
import { MaintenanceService } from '../../services/maintenance.service';
import { StepInstance } from '../../domain/step-instance';
import { Actor } from '../../domain/actor';
export class UpdateDetailsComponent {
    /**
     * @param {?} projectService
     * @param {?} maintenanceService
     * @param {?} actorService
     */
    constructor(projectService, maintenanceService, actorService) {
        this.projectService = projectService;
        this.maintenanceService = maintenanceService;
        this.actorService = actorService;
        this.stepCompletedListener = new EventEmitter();
        this.cancelListener = new EventEmitter();
        this.actorFieldList = [];
        this.actorFields = [];
        this.actorFieldOptionsMap = {};
        this.actorFieldValues = {};
        this.actorFieldErrors = {};
        this.updateError = false;
        this.firstnameError = false;
        this.surnameError = false;
        this.idNrError = false;
        this.celNrError = false;
        this.emailError = false;
        this.emailConfirmationError = false;
        this.actor = new Actor();
        this.loading = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.loading = true;
        this.projectService.getStepParameters(this.stepInstance.step.id).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            // this.requiresEmail = result[0] && "true" === result[0];
            this.requiresFirstname = result["6"] && "true" === result["6"];
            this.requiresSurname = result["7"] && "true" === result["7"];
            this.requiresCelNr = result["8"] && "true" === result["8"];
            this.requiresEmail = result["0"] && "true" === result["0"];
            this.requiresEmployer = result["1"] && "true" === result["1"];
            if (result["9"]) {
                this.idNrLabel = result["9"];
            }
            else {
                this.idNrLabel = "Id nr";
            }
            // alert(this.stepInstance.actor.idNr + " " + this.stepInstance.businessProcessInstance.actor.idNr);
            this.actor = this.stepInstance.businessProcessInstance.actor;
            if (this.actor.email) {
                this.emailConfirmation = this.actor.email;
            }
            if (this.actor.actorType) {
                this.maintenanceService.getActorFields(this.actor.actorType.id).then((/**
                 * @param {?} actorFields
                 * @return {?}
                 */
                actorFields => {
                    this.actorFieldList = actorFields;
                    if (actorFields) {
                        for (let i = 0; i < actorFields.length; i++) {
                            console.log("trace 9 -> " + i + ", " + actorFields[i].id);
                            this.actorFieldValues[actorFields[i].id] = "";
                            this.actorFieldErrors[actorFields[i].id] = false;
                            if (i % 2 === 0) {
                                console.log("trace 10");
                                /** @type {?} */
                                let actorFieldRow = {};
                                actorFieldRow[0] = actorFields[i];
                                console.log("trace 11");
                                if (i < actorFields.length - 1) {
                                    console.log("trace 12");
                                    actorFieldRow[1] = actorFields[i + 1];
                                }
                                this.actorFields.push(actorFieldRow);
                            }
                            if (actorFields[i].actorFieldType === "SELECT") {
                                console.log("trace 13");
                                this.maintenanceService.getActorFieldOptions(actorFields[i].id).then((/**
                                 * @param {?} actorFieldOptions
                                 * @return {?}
                                 */
                                actorFieldOptions => {
                                    console.log("trace 14");
                                    if (actorFieldOptions) {
                                        console.log("trace 15");
                                        this.actorFieldOptionsMap[actorFields[i].id] = actorFieldOptions;
                                    }
                                }));
                            }
                        }
                    }
                }));
                this.actorService.getActorFieldValues(this.actor.id).then((/**
                 * @param {?} actorFieldValues
                 * @return {?}
                 */
                actorFieldValues => {
                    for (let actorFieldValue of actorFieldValues) {
                        this.actorFieldValues[actorFieldValue.actorFieldId] = actorFieldValue.fieldValue;
                    }
                    this.loading = false;
                }));
            }
            else {
                this.loading = false;
            }
        }));
        // console.log("trace 6");
        // this.projectService.getProcessVariableValue(this.stepInstance.businessProcessInstance.id, "idNr").then(result => {
        //   console.log("trace 7");
        //   if (result) {
        //     console.log("trace 8");
        //     this.actor.idNr = result;
        //   }
        //
        //   this.loading = false;
        // });
    }
    /**
     * @return {?}
     */
    completeStep() {
        this.firstnameError = false;
        this.surnameError = false;
        this.idNrError = false;
        this.celNrError = false;
        this.emailError = false;
        this.emailConfirmationError = false;
        // requiresFirstname: boolean;
        // requiresSurname: boolean;
        // requiresCelNr: boolean;
        // requiresEmployer: boolean;
        if (this.requiresFirstname) {
            if (!this.actor.firstname) {
                this.firstnameError = true;
                return;
            }
        }
        else {
            this.actor.firstname = this.actor.idNr;
        }
        if (this.requiresSurname) {
            if (!this.actor.surname) {
                this.surnameError = true;
                return;
            }
        }
        else {
            this.actor.surname = this.actor.idNr;
        }
        if (!this.actor.idNr) {
            this.idNrError = true;
            return;
        }
        if (this.requiresCelNr && !this.actor.celNr) {
            this.celNrError = true;
            return;
        }
        if (!this.actor.email) {
            this.emailError = true;
            return;
        }
        if (this.actor.email !== this.emailConfirmation) {
            this.emailConfirmationError = true;
            return;
        }
        this.actorFieldErrors = {};
        /** @type {?} */
        let errorFound = false;
        for (const actorField of this.actorFieldList) {
            /** @type {?} */
            let fieldValue = this.actorFieldValues[actorField.id];
            if (actorField.required === true && (!fieldValue || fieldValue === "")) {
                this.actorFieldErrors[actorField.id] = true;
                errorFound = true;
            }
            else {
                this.actorFieldErrors[actorField.id] = false;
            }
        }
        if (errorFound) {
            return;
        }
        this.updateError = false;
        this.loading = true;
        this.projectService.updateActor(this.actor).then((/**
         * @param {?} updateResult
         * @return {?}
         */
        updateResult => {
            if (updateResult) {
                /** @type {?} */
                let actorFieldValues = [];
                for (const actorFieldId of Object.keys(this.actorFieldValues)) {
                    actorFieldValues.push({
                        actorFieldId: Number(actorFieldId),
                        fieldValue: this.actorFieldValues[actorFieldId]
                    });
                }
                this.actorService.updateActorFieldValues(this.actor.id, actorFieldValues).then((/**
                 * @param {?} updateResult2
                 * @return {?}
                 */
                updateResult2 => {
                    if (updateResult2) {
                        this.projectService.completeStep(this.stepInstance.id, this.userId).then((/**
                         * @param {?} result
                         * @return {?}
                         */
                        result => {
                            this.loading = false;
                            this.stepCompletedListener.emit({
                                value: this.stepInstance
                            });
                        }));
                    }
                    else {
                        this.updateError = true;
                        this.loading = false;
                    }
                }));
            }
            else {
                this.updateError = true;
                this.loading = false;
            }
        }));
    }
    /**
     * @return {?}
     */
    cancel() {
        this.cancelListener.emit({});
    }
}
UpdateDetailsComponent.decorators = [
    { type: Component, args: [{
                selector: 'update-details',
                template: "<div class=\"update-details\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div class=\"heading\">\n    Update details\n  </div>\n\n  <div *ngIf=\"!loading && updateError\" class=\"content\">\n    <div class=\"info-message\">\n      Update failed\n    </div>\n  </div>\n\n  <div *ngIf=\"!loading && !updateError\" class=\"content\">\n    <div *ngIf=\"requiresFirstname || requiresSurname\" class=\"component-row\">\n      <div *ngIf=\"requiresFirstname\" class=\"component-col\">\n        <div class=\"component-label\">\n          Firstname\n          <div *ngIf=\"firstnameError\" class=\"error-message\">A firstname must be entered</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"text\" [(ngModel)]=\"actor.firstname\">\n        </div>\n      </div>\n      <div *ngIf=\"requiresSurname\" class=\"component-col\">\n        <div class=\"component-label\">\n          Surname\n          <div *ngIf=\"surnameError\" class=\"error-message\">A surname must be entered</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"text\" [(ngModel)]=\"actor.surname\">\n        </div>\n      </div>\n    </div>\n\n    <div class=\"component-row\">\n      <div class=\"component-col\">\n        <div class=\"component-label\">\n          {{ idNrLabel }}\n          <div *ngIf=\"idNrError\" class=\"error-message\">An {{ idNrLabel }} must be entered</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"text\" [(ngModel)]=\"actor.idNr\">\n        </div>\n      </div>\n      <div *ngIf=\"requiresCelNr\" class=\"component-col\">\n        <div class=\"component-label\">\n          Cel nr\n          <div *ngIf=\"celNrError\" class=\"error-message\">A cel nr must be entered</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"text\" [(ngModel)]=\"actor.celNr\">\n        </div>\n      </div>\n    </div>\n\n    <div class=\"component-row\">\n      <div class=\"component-col\">\n        <div class=\"component-label\">\n          E-mail address\n          <div *ngIf=\"emailError\" class=\"error-message\">An email address must be entered</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"email\" [(ngModel)]=\"actor.email\">\n        </div>\n      </div>\n      <div class=\"component-col\">\n        <div class=\"component-label\">\n          Confirm e-mail address\n          <div *ngIf=\"emailConfirmationError\" class=\"error-message\">The email addresses entered are not the same</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"email\" [(ngModel)]=\"emailConfirmation\">\n        </div>\n      </div>\n    </div>\n\n    <div *ngFor=\"let actorFieldRow of actorFields; let i = index\" class=\"component-row\">\n      <div class=\"component-col\">\n        <div class=\"component-label\">\n          {{ actorFieldRow[0].description }}\n          <div *ngIf=\"actorFieldErrors[actorFieldRow[0].id]\" class=\"error-message\">\n            A value must be entered for {{ actorFieldRow[0].description }}\n          </div>\n        </div>\n        <div class=\"component\">\n          <input *ngIf=\"actorFieldRow[0].actorFieldType === 'TEXT'\" type=\"text\" [(ngModel)]=\"actorFieldValues[actorFieldRow[0].id]\">\n          <select *ngIf=\"actorFieldRow[0].actorFieldType === 'SELECT'\" [(ngModel)]=\"actorFieldValues[actorFieldRow[0].id]\">\n            <option *ngFor=\"let actorFieldOption of actorFieldOptionsMap[actorFieldRow[0].id]\" [value]=\"actorFieldOption.fieldValue\">\n              {{ actorFieldOption.description }}\n            </option>\n          </select>\n        </div>\n      </div>\n      <div *ngIf=\"actorFieldRow[1]\" class=\"component-col\">\n        <div class=\"component-label\">\n          {{ actorFieldRow[1].description }}\n          <div *ngIf=\"actorFieldErrors[actorFieldRow[1].id]\" class=\"error-message\">\n            A value must be entered for {{ actorFieldRow[1].description }}\n          </div>\n        </div>\n        <div class=\"component\">\n          <input *ngIf=\"actorFieldRow[1].actorFieldType === 'TEXT'\" type=\"text\" [(ngModel)]=\"actorFieldValues[actorFieldRow[1].id]\">\n          <select *ngIf=\"actorFieldRow[1].actorFieldType === 'SELECT'\" [(ngModel)]=\"actorFieldValues[actorFieldRow[1].id]\">\n            <option *ngFor=\"let actorFieldOption of actorFieldOptionsMap[actorFieldRow[1].id]\" [value]=\"actorFieldOption.fieldValue\">\n              {{ actorFieldOption.description }}\n            </option>\n          </select>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!loading\" class=\"button-section\">\n    <div class=\"button\" (click)=\"completeStep()\">\n      <div class=\"button-text\">Submit</div>\n    </div>\n    <div class=\"button button-outline\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n  <div *ngIf=\"loading\" class=\"button-section\">\n    <div class=\"button button-disabled\">\n      <div class=\"button-text\">Submit</div>\n    </div>\n    <div class=\"button button-outline-disabled\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                styles: [".update-details{width:100%;height:100%;position:relative}.update-details .heading{position:absolute;top:0;width:100%;height:32px;font-size:16px;padding:8px}.update-details .content{position:absolute;top:32px;left:0;bottom:50px;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px;box-sizing:border-box}.update-details .content .info-message{font-size:15px}.update-details .content .component-row{width:100%}.update-details .content .component-row .component-col{width:100%;display:inline-block;box-sizing:border-box;padding-top:10px}.update-details .content .component-row .component-col .component-label{width:100%;font-size:15px;padding-bottom:5px}.update-details .content .component-row .component-col .error-message{font-style:italic;font-size:11px;color:#f04141;padding-bottom:5px}.update-details .content .component-row .component-col .component{width:100%}.update-details .content .component-row .component-col .component input,.update-details .content .component-row .component-col .component select{width:100%;font-size:13px;border:1px solid #58666c;background-color:#fffcf6;box-sizing:border-box;padding:3px 5px;border-radius:5px;height:30px}.update-details .content .component-row:first-child .component-col:first-child{padding-top:0}@media (min-width:600px){.update-details .content .component-row .component-col{width:50%}.update-details .content .component-row .component-col:first-child{padding-right:16px}.update-details .content .component-row:first-child .component-col{padding-top:0}}.update-details .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.update-details .button-section .button{width:120px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.update-details .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.update-details .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.update-details .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b;cursor:default}.update-details .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b;cursor:default}"]
            }] }
];
/** @nocollapse */
UpdateDetailsComponent.ctorParameters = () => [
    { type: ProjectService },
    { type: MaintenanceService },
    { type: ActorService }
];
UpdateDetailsComponent.propDecorators = {
    stepInstance: [{ type: Input }],
    userId: [{ type: Input }],
    stepCompletedListener: [{ type: Output }],
    cancelListener: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    UpdateDetailsComponent.prototype.stepInstance;
    /** @type {?} */
    UpdateDetailsComponent.prototype.userId;
    /** @type {?} */
    UpdateDetailsComponent.prototype.stepCompletedListener;
    /** @type {?} */
    UpdateDetailsComponent.prototype.cancelListener;
    /** @type {?} */
    UpdateDetailsComponent.prototype.actor;
    /** @type {?} */
    UpdateDetailsComponent.prototype.actorFieldList;
    /** @type {?} */
    UpdateDetailsComponent.prototype.actorFields;
    /** @type {?} */
    UpdateDetailsComponent.prototype.actorFieldOptionsMap;
    /** @type {?} */
    UpdateDetailsComponent.prototype.actorFieldValues;
    /** @type {?} */
    UpdateDetailsComponent.prototype.actorFieldErrors;
    /** @type {?} */
    UpdateDetailsComponent.prototype.emailConfirmation;
    /** @type {?} */
    UpdateDetailsComponent.prototype.loading;
    /** @type {?} */
    UpdateDetailsComponent.prototype.requiresEmail;
    /** @type {?} */
    UpdateDetailsComponent.prototype.requiresFirstname;
    /** @type {?} */
    UpdateDetailsComponent.prototype.requiresSurname;
    /** @type {?} */
    UpdateDetailsComponent.prototype.requiresCelNr;
    /** @type {?} */
    UpdateDetailsComponent.prototype.requiresEmployer;
    /** @type {?} */
    UpdateDetailsComponent.prototype.idNrLabel;
    /** @type {?} */
    UpdateDetailsComponent.prototype.updateError;
    /** @type {?} */
    UpdateDetailsComponent.prototype.firstnameError;
    /** @type {?} */
    UpdateDetailsComponent.prototype.surnameError;
    /** @type {?} */
    UpdateDetailsComponent.prototype.idNrError;
    /** @type {?} */
    UpdateDetailsComponent.prototype.celNrError;
    /** @type {?} */
    UpdateDetailsComponent.prototype.emailError;
    /** @type {?} */
    UpdateDetailsComponent.prototype.emailConfirmationError;
    /**
     * @type {?}
     * @private
     */
    UpdateDetailsComponent.prototype.projectService;
    /**
     * @type {?}
     * @private
     */
    UpdateDetailsComponent.prototype.maintenanceService;
    /**
     * @type {?}
     * @private
     */
    UpdateDetailsComponent.prototype.actorService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBkYXRlLWRldGFpbHMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWN1bWVuLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9zdGVwcy91cGRhdGUtZGV0YWlscy91cGRhdGUtZGV0YWlscy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBVSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFL0UsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQ2hFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUM1RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQUN4RSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDMUQsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBVzNDLE1BQU0sT0FBTyxzQkFBc0I7Ozs7OztJQTZCakMsWUFBb0IsY0FBOEIsRUFBVSxrQkFBc0MsRUFDdEYsWUFBMEI7UUFEbEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQVUsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0RixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQTNCNUIsMEJBQXFCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUMzQyxtQkFBYyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFHOUMsbUJBQWMsR0FBc0IsRUFBRSxDQUFDO1FBQ3ZDLGdCQUFXLEdBQWUsRUFBRSxDQUFDO1FBQzdCLHlCQUFvQixHQUFHLEVBQUUsQ0FBQztRQUMxQixxQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFDdEIscUJBQWdCLEdBQUcsRUFBRSxDQUFDO1FBVXRCLGdCQUFXLEdBQVksS0FBSyxDQUFDO1FBQzdCLG1CQUFjLEdBQVksS0FBSyxDQUFDO1FBQ2hDLGlCQUFZLEdBQVksS0FBSyxDQUFDO1FBQzlCLGNBQVMsR0FBWSxLQUFLLENBQUM7UUFDM0IsZUFBVSxHQUFZLEtBQUssQ0FBQztRQUM1QixlQUFVLEdBQVksS0FBSyxDQUFDO1FBQzVCLDJCQUFzQixHQUFZLEtBQUssQ0FBQztRQUl0QyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksS0FBSyxFQUFFLENBQUM7UUFDekIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7SUFDdkIsQ0FBQzs7OztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNwQixJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxNQUFNLENBQUMsRUFBRTtZQUM3RSwwREFBMEQ7WUFDMUQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxNQUFNLEtBQUssTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQy9ELElBQUksQ0FBQyxlQUFlLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxJQUFJLE1BQU0sS0FBSyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDN0QsSUFBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUksTUFBTSxLQUFLLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUMzRCxJQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxNQUFNLEtBQUssTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzNELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUksTUFBTSxLQUFLLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUU5RCxJQUFJLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDZixJQUFJLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUM5QjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQzthQUMxQjtZQUVELG9HQUFvRztZQUNwRyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsdUJBQXVCLENBQUMsS0FBSyxDQUFDO1lBQzdELElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUU7Z0JBQ3BCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQzthQUMzQztZQUVELElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUU7Z0JBQ3hCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSTs7OztnQkFBQyxXQUFXLENBQUMsRUFBRTtvQkFDakYsSUFBSSxDQUFDLGNBQWMsR0FBRyxXQUFXLENBQUM7b0JBQ2xDLElBQUksV0FBVyxFQUFFO3dCQUNmLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFOzRCQUMzQyxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsR0FBRyxDQUFDLEdBQUcsSUFBSSxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQzs0QkFDMUQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUM7NEJBQzlDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDOzRCQUNqRCxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxFQUFFO2dDQUNmLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7O29DQUNwQixhQUFhLEdBQUcsRUFBRTtnQ0FDdEIsYUFBYSxDQUFDLENBQUMsQ0FBQyxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQ0FDbEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQ0FDeEIsSUFBSSxDQUFDLEdBQUcsV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0NBQzlCLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7b0NBQ3hCLGFBQWEsQ0FBQyxDQUFDLENBQUMsR0FBRyxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2lDQUN2QztnQ0FFRCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQzs2QkFDdEM7NEJBRUQsSUFBSSxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBYyxLQUFLLFFBQVEsRUFBRTtnQ0FDOUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQ0FDeEIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLG9CQUFvQixDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJOzs7O2dDQUFDLGlCQUFpQixDQUFDLEVBQUU7b0NBQ3ZGLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7b0NBQ3hCLElBQUksaUJBQWlCLEVBQUU7d0NBQ3JCLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7d0NBQ3hCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsaUJBQWlCLENBQUM7cUNBQ2xFO2dDQUNILENBQUMsRUFBQyxDQUFDOzZCQUNKO3lCQUNGO3FCQUNGO2dCQUNILENBQUMsRUFBQyxDQUFDO2dCQUVILElBQUksQ0FBQyxZQUFZLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJOzs7O2dCQUFDLGdCQUFnQixDQUFDLEVBQUU7b0JBQzNFLEtBQUssSUFBSSxlQUFlLElBQUksZ0JBQWdCLEVBQUU7d0JBQzVDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLEdBQUcsZUFBZSxDQUFDLFVBQVUsQ0FBQztxQkFDbEY7b0JBRUQsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7Z0JBQ3ZCLENBQUMsRUFBQyxDQUFDO2FBQ0o7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7YUFDdEI7UUFDSCxDQUFDLEVBQUMsQ0FBQztRQUVILDBCQUEwQjtRQUMxQixxSEFBcUg7UUFDckgsNEJBQTRCO1FBQzVCLGtCQUFrQjtRQUNsQiw4QkFBOEI7UUFDOUIsZ0NBQWdDO1FBQ2hDLE1BQU07UUFDTixFQUFFO1FBQ0YsMEJBQTBCO1FBQzFCLE1BQU07SUFDUixDQUFDOzs7O0lBRUQsWUFBWTtRQUNWLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1FBQzVCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzFCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxLQUFLLENBQUM7UUFFcEMsOEJBQThCO1FBQzlCLDRCQUE0QjtRQUM1QiwwQkFBMEI7UUFDMUIsNkJBQTZCO1FBRTdCLElBQUksSUFBSSxDQUFDLGlCQUFpQixFQUFFO1lBQzFCLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRTtnQkFDekIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7Z0JBQzNCLE9BQU87YUFDUjtTQUNGO2FBQU07WUFDTCxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztTQUN4QztRQUVELElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUN4QixJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUU7Z0JBQ3ZCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO2dCQUN6QixPQUFPO2FBQ1I7U0FDRjthQUFNO1lBQ0wsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7U0FDdEM7UUFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUU7WUFDcEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7WUFDdEIsT0FBTztTQUNSO1FBRUQsSUFBSSxJQUFJLENBQUMsYUFBYSxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUU7WUFDM0MsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDdkIsT0FBTztTQUNSO1FBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFO1lBQ3JCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBQ3ZCLE9BQU87U0FDUjtRQUVELElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLGlCQUFpQixFQUFFO1lBQy9DLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUM7WUFDbkMsT0FBTztTQUNSO1FBRUQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEVBQUUsQ0FBQzs7WUFDdkIsVUFBVSxHQUFHLEtBQUs7UUFDdEIsS0FBSyxNQUFNLFVBQVUsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFOztnQkFDeEMsVUFBVSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDO1lBQ3JELElBQUksVUFBVSxDQUFDLFFBQVEsS0FBSyxJQUFJLElBQUksQ0FBQyxDQUFDLFVBQVUsSUFBSSxVQUFVLEtBQUssRUFBRSxDQUFDLEVBQUU7Z0JBQ3RFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDO2dCQUM1QyxVQUFVLEdBQUcsSUFBSSxDQUFDO2FBQ25CO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDO2FBQzlDO1NBQ0Y7UUFFRCxJQUFJLFVBQVUsRUFBRTtZQUNkLE9BQU87U0FDUjtRQUVELElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJOzs7O1FBQUMsWUFBWSxDQUFDLEVBQUU7WUFDOUQsSUFBSSxZQUFZLEVBQUU7O29CQUNaLGdCQUFnQixHQUFHLEVBQUU7Z0JBQ3pCLEtBQUssTUFBTSxZQUFZLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsRUFBRTtvQkFDN0QsZ0JBQWdCLENBQUMsSUFBSSxDQUFDO3dCQUNwQixZQUFZLEVBQUUsTUFBTSxDQUFDLFlBQVksQ0FBQzt3QkFDbEMsVUFBVSxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUM7cUJBQ2hELENBQUMsQ0FBQztpQkFDSjtnQkFFRCxJQUFJLENBQUMsWUFBWSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLGdCQUFnQixDQUFDLENBQUMsSUFBSTs7OztnQkFBQyxhQUFhLENBQUMsRUFBRTtvQkFDN0YsSUFBSSxhQUFhLEVBQUU7d0JBQ2pCLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJOzs7O3dCQUFDLE1BQU0sQ0FBQyxFQUFFOzRCQUNoRixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQzs0QkFDckIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQztnQ0FDOUIsS0FBSyxFQUFFLElBQUksQ0FBQyxZQUFZOzZCQUN6QixDQUFDLENBQUM7d0JBQ0wsQ0FBQyxFQUFDLENBQUM7cUJBQ0o7eUJBQU07d0JBQ0wsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7d0JBQ3hCLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO3FCQUN0QjtnQkFDSCxDQUFDLEVBQUMsQ0FBQzthQUNKO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO2dCQUN4QixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQzthQUN0QjtRQUNILENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVELE1BQU07UUFDSixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxFQUN4QixDQUFDLENBQUM7SUFDTCxDQUFDOzs7WUEvTkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxnQkFBZ0I7Z0JBQzFCLHFuS0FBOEM7O2FBRS9DOzs7O1lBZFEsY0FBYztZQUVkLGtCQUFrQjtZQURsQixZQUFZOzs7MkJBZWxCLEtBQUs7cUJBQ0wsS0FBSztvQ0FDTCxNQUFNOzZCQUNOLE1BQU07Ozs7SUFIUCw4Q0FBb0M7O0lBQ3BDLHdDQUF3Qjs7SUFDeEIsdURBQXFEOztJQUNyRCxnREFBOEM7O0lBRTlDLHVDQUFhOztJQUNiLGdEQUF1Qzs7SUFDdkMsNkNBQTZCOztJQUM3QixzREFBMEI7O0lBQzFCLGtEQUFzQjs7SUFDdEIsa0RBQXNCOztJQUN0QixtREFBMEI7O0lBQzFCLHlDQUFpQjs7SUFDakIsK0NBQXVCOztJQUN2QixtREFBMkI7O0lBQzNCLGlEQUF5Qjs7SUFDekIsK0NBQXVCOztJQUN2QixrREFBMEI7O0lBQzFCLDJDQUFrQjs7SUFFbEIsNkNBQTZCOztJQUM3QixnREFBZ0M7O0lBQ2hDLDhDQUE4Qjs7SUFDOUIsMkNBQTJCOztJQUMzQiw0Q0FBNEI7O0lBQzVCLDRDQUE0Qjs7SUFDNUIsd0RBQXdDOzs7OztJQUU1QixnREFBc0M7Ozs7O0lBQUUsb0RBQThDOzs7OztJQUM5Riw4Q0FBa0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE9uSW5pdCwgT3V0cHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IFByb2plY3RTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvcHJvamVjdC5zZXJ2aWNlJztcbmltcG9ydCB7IEFjdG9yU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2FjdG9yLnNlcnZpY2UnO1xuaW1wb3J0IHsgTWFpbnRlbmFuY2VTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvbWFpbnRlbmFuY2Uuc2VydmljZSc7XG5pbXBvcnQgeyBTdGVwSW5zdGFuY2UgfSBmcm9tICcuLi8uLi9kb21haW4vc3RlcC1pbnN0YW5jZSc7XG5pbXBvcnQgeyBBY3RvciB9IGZyb20gJy4uLy4uL2RvbWFpbi9hY3Rvcic7XG5pbXBvcnQgeyBTZWxlY3Rpb25PcHRpb24gfSBmcm9tICcuLi8uLi9kb21haW4vc2VsZWN0aW9uLW9wdGlvbic7XG5pbXBvcnQgeyBBY3RvckZpZWxkIH0gZnJvbSAnLi4vLi4vZG9tYWluL2FjdG9yLWZpZWxkJztcbmltcG9ydCB7IEFjdG9yRmllbGRPcHRpb24gfSBmcm9tICcuLi8uLi9kb21haW4vYWN0b3ItZmllbGQtb3B0aW9uJztcbmltcG9ydCB7IEFjdG9yRmllbGRWYWx1ZSB9IGZyb20gJy4uLy4uL2RvbWFpbi9hY3Rvci1maWVsZC12YWx1ZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3VwZGF0ZS1kZXRhaWxzJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3VwZGF0ZS1kZXRhaWxzLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vdXBkYXRlLWRldGFpbHMuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBVcGRhdGVEZXRhaWxzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgc3RlcEluc3RhbmNlOiBTdGVwSW5zdGFuY2U7XG4gIEBJbnB1dCgpIHVzZXJJZDogbnVtYmVyO1xuICBAT3V0cHV0KCkgc3RlcENvbXBsZXRlZExpc3RlbmVyID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgY2FuY2VsTGlzdGVuZXIgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgYWN0b3I6IEFjdG9yO1xuICBhY3RvckZpZWxkTGlzdDogQXJyYXk8QWN0b3JGaWVsZD4gPSBbXTtcbiAgYWN0b3JGaWVsZHM6IEFycmF5PGFueT4gPSBbXTtcbiAgYWN0b3JGaWVsZE9wdGlvbnNNYXAgPSB7fTtcbiAgYWN0b3JGaWVsZFZhbHVlcyA9IHt9O1xuICBhY3RvckZpZWxkRXJyb3JzID0ge307XG4gIGVtYWlsQ29uZmlybWF0aW9uOiBzdHJpbmc7XG4gIGxvYWRpbmc6IGJvb2xlYW47XG4gIHJlcXVpcmVzRW1haWw6IGJvb2xlYW47XG4gIHJlcXVpcmVzRmlyc3RuYW1lOiBib29sZWFuO1xuICByZXF1aXJlc1N1cm5hbWU6IGJvb2xlYW47XG4gIHJlcXVpcmVzQ2VsTnI6IGJvb2xlYW47XG4gIHJlcXVpcmVzRW1wbG95ZXI6IGJvb2xlYW47XG4gIGlkTnJMYWJlbDogc3RyaW5nO1xuXG4gIHVwZGF0ZUVycm9yOiBib29sZWFuID0gZmFsc2U7XG4gIGZpcnN0bmFtZUVycm9yOiBib29sZWFuID0gZmFsc2U7XG4gIHN1cm5hbWVFcnJvcjogYm9vbGVhbiA9IGZhbHNlO1xuICBpZE5yRXJyb3I6IGJvb2xlYW4gPSBmYWxzZTtcbiAgY2VsTnJFcnJvcjogYm9vbGVhbiA9IGZhbHNlO1xuICBlbWFpbEVycm9yOiBib29sZWFuID0gZmFsc2U7XG4gIGVtYWlsQ29uZmlybWF0aW9uRXJyb3I6IGJvb2xlYW4gPSBmYWxzZTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHByb2plY3RTZXJ2aWNlOiBQcm9qZWN0U2VydmljZSwgcHJpdmF0ZSBtYWludGVuYW5jZVNlcnZpY2U6IE1haW50ZW5hbmNlU2VydmljZSxcbiAgICAgIHByaXZhdGUgYWN0b3JTZXJ2aWNlOiBBY3RvclNlcnZpY2UpIHtcbiAgICB0aGlzLmFjdG9yID0gbmV3IEFjdG9yKCk7XG4gICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLmxvYWRpbmcgPSB0cnVlO1xuICAgIHRoaXMucHJvamVjdFNlcnZpY2UuZ2V0U3RlcFBhcmFtZXRlcnModGhpcy5zdGVwSW5zdGFuY2Uuc3RlcC5pZCkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgLy8gdGhpcy5yZXF1aXJlc0VtYWlsID0gcmVzdWx0WzBdICYmIFwidHJ1ZVwiID09PSByZXN1bHRbMF07XG4gICAgICB0aGlzLnJlcXVpcmVzRmlyc3RuYW1lID0gcmVzdWx0W1wiNlwiXSAmJiBcInRydWVcIiA9PT0gcmVzdWx0W1wiNlwiXTtcbiAgICAgIHRoaXMucmVxdWlyZXNTdXJuYW1lID0gcmVzdWx0W1wiN1wiXSAmJiBcInRydWVcIiA9PT0gcmVzdWx0W1wiN1wiXTtcbiAgICAgIHRoaXMucmVxdWlyZXNDZWxOciA9IHJlc3VsdFtcIjhcIl0gJiYgXCJ0cnVlXCIgPT09IHJlc3VsdFtcIjhcIl07XG4gICAgICB0aGlzLnJlcXVpcmVzRW1haWwgPSByZXN1bHRbXCIwXCJdICYmIFwidHJ1ZVwiID09PSByZXN1bHRbXCIwXCJdO1xuICAgICAgdGhpcy5yZXF1aXJlc0VtcGxveWVyID0gcmVzdWx0W1wiMVwiXSAmJiBcInRydWVcIiA9PT0gcmVzdWx0W1wiMVwiXTtcblxuICAgICAgaWYgKHJlc3VsdFtcIjlcIl0pIHtcbiAgICAgICAgdGhpcy5pZE5yTGFiZWwgPSByZXN1bHRbXCI5XCJdO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5pZE5yTGFiZWwgPSBcIklkIG5yXCI7XG4gICAgICB9XG5cbiAgICAgIC8vIGFsZXJ0KHRoaXMuc3RlcEluc3RhbmNlLmFjdG9yLmlkTnIgKyBcIiBcIiArIHRoaXMuc3RlcEluc3RhbmNlLmJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlLmFjdG9yLmlkTnIpO1xuICAgICAgdGhpcy5hY3RvciA9IHRoaXMuc3RlcEluc3RhbmNlLmJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlLmFjdG9yO1xuICAgICAgaWYgKHRoaXMuYWN0b3IuZW1haWwpIHtcbiAgICAgICAgdGhpcy5lbWFpbENvbmZpcm1hdGlvbiA9IHRoaXMuYWN0b3IuZW1haWw7XG4gICAgICB9XG5cbiAgICAgIGlmICh0aGlzLmFjdG9yLmFjdG9yVHlwZSkge1xuICAgICAgICB0aGlzLm1haW50ZW5hbmNlU2VydmljZS5nZXRBY3RvckZpZWxkcyh0aGlzLmFjdG9yLmFjdG9yVHlwZS5pZCkudGhlbihhY3RvckZpZWxkcyA9PiB7XG4gICAgICAgICAgdGhpcy5hY3RvckZpZWxkTGlzdCA9IGFjdG9yRmllbGRzO1xuICAgICAgICAgIGlmIChhY3RvckZpZWxkcykge1xuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBhY3RvckZpZWxkcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcInRyYWNlIDkgLT4gXCIgKyBpICsgXCIsIFwiICsgYWN0b3JGaWVsZHNbaV0uaWQpO1xuICAgICAgICAgICAgICB0aGlzLmFjdG9yRmllbGRWYWx1ZXNbYWN0b3JGaWVsZHNbaV0uaWRdID0gXCJcIjtcbiAgICAgICAgICAgICAgdGhpcy5hY3RvckZpZWxkRXJyb3JzW2FjdG9yRmllbGRzW2ldLmlkXSA9IGZhbHNlO1xuICAgICAgICAgICAgICBpZiAoaSAlIDIgPT09IDApIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcInRyYWNlIDEwXCIpO1xuICAgICAgICAgICAgICAgIGxldCBhY3RvckZpZWxkUm93ID0ge307XG4gICAgICAgICAgICAgICAgYWN0b3JGaWVsZFJvd1swXSA9IGFjdG9yRmllbGRzW2ldO1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwidHJhY2UgMTFcIik7XG4gICAgICAgICAgICAgICAgaWYgKGkgPCBhY3RvckZpZWxkcy5sZW5ndGggLSAxKSB7XG4gICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcInRyYWNlIDEyXCIpO1xuICAgICAgICAgICAgICAgICAgYWN0b3JGaWVsZFJvd1sxXSA9IGFjdG9yRmllbGRzW2kgKyAxXTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB0aGlzLmFjdG9yRmllbGRzLnB1c2goYWN0b3JGaWVsZFJvdyk7XG4gICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICBpZiAoYWN0b3JGaWVsZHNbaV0uYWN0b3JGaWVsZFR5cGUgPT09IFwiU0VMRUNUXCIpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcInRyYWNlIDEzXCIpO1xuICAgICAgICAgICAgICAgIHRoaXMubWFpbnRlbmFuY2VTZXJ2aWNlLmdldEFjdG9yRmllbGRPcHRpb25zKGFjdG9yRmllbGRzW2ldLmlkKS50aGVuKGFjdG9yRmllbGRPcHRpb25zID0+IHtcbiAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwidHJhY2UgMTRcIik7XG4gICAgICAgICAgICAgICAgICBpZiAoYWN0b3JGaWVsZE9wdGlvbnMpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJ0cmFjZSAxNVwiKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hY3RvckZpZWxkT3B0aW9uc01hcFthY3RvckZpZWxkc1tpXS5pZF0gPSBhY3RvckZpZWxkT3B0aW9ucztcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgdGhpcy5hY3RvclNlcnZpY2UuZ2V0QWN0b3JGaWVsZFZhbHVlcyh0aGlzLmFjdG9yLmlkKS50aGVuKGFjdG9yRmllbGRWYWx1ZXMgPT4ge1xuICAgICAgICAgIGZvciAobGV0IGFjdG9yRmllbGRWYWx1ZSBvZiBhY3RvckZpZWxkVmFsdWVzKSB7XG4gICAgICAgICAgICB0aGlzLmFjdG9yRmllbGRWYWx1ZXNbYWN0b3JGaWVsZFZhbHVlLmFjdG9yRmllbGRJZF0gPSBhY3RvckZpZWxkVmFsdWUuZmllbGRWYWx1ZTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgfSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIC8vIGNvbnNvbGUubG9nKFwidHJhY2UgNlwiKTtcbiAgICAvLyB0aGlzLnByb2plY3RTZXJ2aWNlLmdldFByb2Nlc3NWYXJpYWJsZVZhbHVlKHRoaXMuc3RlcEluc3RhbmNlLmJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlLmlkLCBcImlkTnJcIikudGhlbihyZXN1bHQgPT4ge1xuICAgIC8vICAgY29uc29sZS5sb2coXCJ0cmFjZSA3XCIpO1xuICAgIC8vICAgaWYgKHJlc3VsdCkge1xuICAgIC8vICAgICBjb25zb2xlLmxvZyhcInRyYWNlIDhcIik7XG4gICAgLy8gICAgIHRoaXMuYWN0b3IuaWROciA9IHJlc3VsdDtcbiAgICAvLyAgIH1cbiAgICAvL1xuICAgIC8vICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgLy8gfSk7XG4gIH1cblxuICBjb21wbGV0ZVN0ZXAoKSB7XG4gICAgdGhpcy5maXJzdG5hbWVFcnJvciA9IGZhbHNlO1xuICAgIHRoaXMuc3VybmFtZUVycm9yID0gZmFsc2U7XG4gICAgdGhpcy5pZE5yRXJyb3IgPSBmYWxzZTtcbiAgICB0aGlzLmNlbE5yRXJyb3IgPSBmYWxzZTtcbiAgICB0aGlzLmVtYWlsRXJyb3IgPSBmYWxzZTtcbiAgICB0aGlzLmVtYWlsQ29uZmlybWF0aW9uRXJyb3IgPSBmYWxzZTtcblxuICAgIC8vIHJlcXVpcmVzRmlyc3RuYW1lOiBib29sZWFuO1xuICAgIC8vIHJlcXVpcmVzU3VybmFtZTogYm9vbGVhbjtcbiAgICAvLyByZXF1aXJlc0NlbE5yOiBib29sZWFuO1xuICAgIC8vIHJlcXVpcmVzRW1wbG95ZXI6IGJvb2xlYW47XG5cbiAgICBpZiAodGhpcy5yZXF1aXJlc0ZpcnN0bmFtZSkge1xuICAgICAgaWYgKCF0aGlzLmFjdG9yLmZpcnN0bmFtZSkge1xuICAgICAgICB0aGlzLmZpcnN0bmFtZUVycm9yID0gdHJ1ZTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmFjdG9yLmZpcnN0bmFtZSA9IHRoaXMuYWN0b3IuaWROcjtcbiAgICB9XG5cbiAgICBpZiAodGhpcy5yZXF1aXJlc1N1cm5hbWUpIHtcbiAgICAgIGlmICghdGhpcy5hY3Rvci5zdXJuYW1lKSB7XG4gICAgICAgIHRoaXMuc3VybmFtZUVycm9yID0gdHJ1ZTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmFjdG9yLnN1cm5hbWUgPSB0aGlzLmFjdG9yLmlkTnI7XG4gICAgfVxuXG4gICAgaWYgKCF0aGlzLmFjdG9yLmlkTnIpIHtcbiAgICAgIHRoaXMuaWROckVycm9yID0gdHJ1ZTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBpZiAodGhpcy5yZXF1aXJlc0NlbE5yICYmICF0aGlzLmFjdG9yLmNlbE5yKSB7XG4gICAgICB0aGlzLmNlbE5yRXJyb3IgPSB0cnVlO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGlmICghdGhpcy5hY3Rvci5lbWFpbCkge1xuICAgICAgdGhpcy5lbWFpbEVycm9yID0gdHJ1ZTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBpZiAodGhpcy5hY3Rvci5lbWFpbCAhPT0gdGhpcy5lbWFpbENvbmZpcm1hdGlvbikge1xuICAgICAgdGhpcy5lbWFpbENvbmZpcm1hdGlvbkVycm9yID0gdHJ1ZTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB0aGlzLmFjdG9yRmllbGRFcnJvcnMgPSB7fTtcbiAgICBsZXQgZXJyb3JGb3VuZCA9IGZhbHNlO1xuICAgIGZvciAoY29uc3QgYWN0b3JGaWVsZCBvZiB0aGlzLmFjdG9yRmllbGRMaXN0KSB7XG4gICAgICBsZXQgZmllbGRWYWx1ZSA9IHRoaXMuYWN0b3JGaWVsZFZhbHVlc1thY3RvckZpZWxkLmlkXTtcbiAgICAgIGlmIChhY3RvckZpZWxkLnJlcXVpcmVkID09PSB0cnVlICYmICghZmllbGRWYWx1ZSB8fCBmaWVsZFZhbHVlID09PSBcIlwiKSkge1xuICAgICAgICB0aGlzLmFjdG9yRmllbGRFcnJvcnNbYWN0b3JGaWVsZC5pZF0gPSB0cnVlO1xuICAgICAgICBlcnJvckZvdW5kID0gdHJ1ZTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuYWN0b3JGaWVsZEVycm9yc1thY3RvckZpZWxkLmlkXSA9IGZhbHNlO1xuICAgICAgfVxuICAgIH1cblxuICAgIGlmIChlcnJvckZvdW5kKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy51cGRhdGVFcnJvciA9IGZhbHNlO1xuICAgIHRoaXMubG9hZGluZyA9IHRydWU7XG4gICAgdGhpcy5wcm9qZWN0U2VydmljZS51cGRhdGVBY3Rvcih0aGlzLmFjdG9yKS50aGVuKHVwZGF0ZVJlc3VsdCA9PiB7XG4gICAgICBpZiAodXBkYXRlUmVzdWx0KSB7XG4gICAgICAgIGxldCBhY3RvckZpZWxkVmFsdWVzID0gW107XG4gICAgICAgIGZvciAoY29uc3QgYWN0b3JGaWVsZElkIG9mIE9iamVjdC5rZXlzKHRoaXMuYWN0b3JGaWVsZFZhbHVlcykpIHtcbiAgICAgICAgICBhY3RvckZpZWxkVmFsdWVzLnB1c2goe1xuICAgICAgICAgICAgYWN0b3JGaWVsZElkOiBOdW1iZXIoYWN0b3JGaWVsZElkKSxcbiAgICAgICAgICAgIGZpZWxkVmFsdWU6IHRoaXMuYWN0b3JGaWVsZFZhbHVlc1thY3RvckZpZWxkSWRdXG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmFjdG9yU2VydmljZS51cGRhdGVBY3RvckZpZWxkVmFsdWVzKHRoaXMuYWN0b3IuaWQsIGFjdG9yRmllbGRWYWx1ZXMpLnRoZW4odXBkYXRlUmVzdWx0MiA9PiB7XG4gICAgICAgICAgaWYgKHVwZGF0ZVJlc3VsdDIpIHtcbiAgICAgICAgICAgIHRoaXMucHJvamVjdFNlcnZpY2UuY29tcGxldGVTdGVwKHRoaXMuc3RlcEluc3RhbmNlLmlkLCB0aGlzLnVzZXJJZCkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgICAgdGhpcy5zdGVwQ29tcGxldGVkTGlzdGVuZXIuZW1pdCh7XG4gICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuc3RlcEluc3RhbmNlXG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMudXBkYXRlRXJyb3IgPSB0cnVlO1xuICAgICAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMudXBkYXRlRXJyb3IgPSB0cnVlO1xuICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIGNhbmNlbCgpIHtcbiAgICB0aGlzLmNhbmNlbExpc3RlbmVyLmVtaXQoe1xuICAgIH0pO1xuICB9XG59XG4iXX0=