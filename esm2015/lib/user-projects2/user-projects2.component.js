/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ActorService } from '../services/actor.service';
import { ProjectService } from '../services/project.service';
import { AcumenConfiguration } from '../services/acumen-configuration';
export class UserProjects2Component {
    /**
     * @param {?} actorService
     * @param {?} projectService
     * @param {?} acumenConfiguration
     */
    constructor(actorService, projectService, acumenConfiguration) {
        this.actorService = actorService;
        this.projectService = projectService;
        this.acumenConfiguration = acumenConfiguration;
        this.onProjectsReceived = new EventEmitter();
        if (acumenConfiguration.backendServiceUrl) {
            this.serverUrl = acumenConfiguration.backendServiceUrl;
        }
        else {
            this.serverUrl = "http://www.healthacumen.co.za/insight/";
        }
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.username) {
            this.actorService.getSystemUser(this.username).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                if (result) {
                    this.userId = result.id;
                    this.actorId = result.actor.id;
                }
                this.refreshProjects();
            })).catch((/**
             * @param {?} error
             * @return {?}
             */
            error => {
                console.log(error);
            }));
        }
        else {
            this.refreshProjects();
        }
    }
    /**
     * @return {?}
     */
    refreshProjects() {
        if (this.actorId) {
            /** @type {?} */
            let projects = [];
            this.projectService.getProjects(this.actorId).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                /** @type {?} */
                let projectIds = {};
                for (let project of result) {
                    if (!projectIds[project.id]) {
                        project.iconUrl = this.serverUrl + "spring/projectIcon/image.png?id=" + project.id;
                        projects.push(project);
                        projectIds[project.id] = project;
                    }
                }
                this.onProjectsReceived.emit(projects);
            })).catch((/**
             * @param {?} error
             * @return {?}
             */
            error => {
                console.error(error);
            }));
        }
        else {
            this.onProjectsReceived.emit([]);
        }
    }
}
UserProjects2Component.decorators = [
    { type: Component, args: [{
                selector: 'acn-user-projects2',
                template: "<div class=\"user-projects2-component\">\n  <ng-content></ng-content>\n</div>\n",
                styles: [".user-projects2-component{width:100%;height:100%;position:relative}"]
            }] }
];
/** @nocollapse */
UserProjects2Component.ctorParameters = () => [
    { type: ActorService },
    { type: ProjectService },
    { type: AcumenConfiguration }
];
UserProjects2Component.propDecorators = {
    username: [{ type: Input }],
    onProjectsReceived: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    UserProjects2Component.prototype.username;
    /** @type {?} */
    UserProjects2Component.prototype.onProjectsReceived;
    /**
     * @type {?}
     * @private
     */
    UserProjects2Component.prototype.actorId;
    /**
     * @type {?}
     * @private
     */
    UserProjects2Component.prototype.userId;
    /**
     * @type {?}
     * @private
     */
    UserProjects2Component.prototype.serverUrl;
    /**
     * @type {?}
     * @private
     */
    UserProjects2Component.prototype.actorService;
    /**
     * @type {?}
     * @private
     */
    UserProjects2Component.prototype.projectService;
    /**
     * @type {?}
     * @private
     */
    UserProjects2Component.prototype.acumenConfiguration;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci1wcm9qZWN0czIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWN1bWVuLWxpYi8iLCJzb3VyY2VzIjpbImxpYi91c2VyLXByb2plY3RzMi91c2VyLXByb2plY3RzMi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFL0UsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ3pELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUU3RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQU92RSxNQUFNLE9BQU8sc0JBQXNCOzs7Ozs7SUFPakMsWUFBb0IsWUFBMEIsRUFBVSxjQUE4QixFQUMxRSxtQkFBd0M7UUFEaEMsaUJBQVksR0FBWixZQUFZLENBQWM7UUFBVSxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDMUUsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQU4xQyx1QkFBa0IsR0FBRyxJQUFJLFlBQVksRUFBa0IsQ0FBQztRQU9oRSxJQUFJLG1CQUFtQixDQUFDLGlCQUFpQixFQUFFO1lBQ3pDLElBQUksQ0FBQyxTQUFTLEdBQUcsbUJBQW1CLENBQUMsaUJBQWlCLENBQUM7U0FDeEQ7YUFBTTtZQUNMLElBQUksQ0FBQyxTQUFTLEdBQUcsd0NBQXdDLENBQUM7U0FDM0Q7SUFDSCxDQUFDOzs7O0lBRUQsUUFBUTtRQUNOLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQixJQUFJLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSTs7OztZQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUMzRCxJQUFJLE1BQU0sRUFBRTtvQkFDVixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxFQUFFLENBQUM7b0JBQ3hCLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUM7aUJBQ2hDO2dCQUVELElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUN6QixDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1lBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ2YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNyQixDQUFDLEVBQUMsQ0FBQztTQUNKO2FBQU07WUFDTCxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7U0FDeEI7SUFDSCxDQUFDOzs7O0lBRUQsZUFBZTtRQUNiLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTs7Z0JBQ1osUUFBUSxHQUFtQixFQUFFO1lBQ2pDLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJOzs7O1lBQUMsTUFBTSxDQUFDLEVBQUU7O29CQUN0RCxVQUFVLEdBQUcsRUFBRTtnQkFDbkIsS0FBSyxJQUFJLE9BQU8sSUFBSSxNQUFNLEVBQUU7b0JBQzFCLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxFQUFFO3dCQUMzQixPQUFPLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLEdBQUcsa0NBQWtDLEdBQUcsT0FBTyxDQUFDLEVBQUUsQ0FBQzt3QkFDbkYsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzt3QkFDdkIsVUFBVSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUM7cUJBQ2xDO2lCQUNGO2dCQUVELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDekMsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztZQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUNmLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDdkIsQ0FBQyxFQUFDLENBQUM7U0FDSjthQUFNO1lBQ0wsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUNsQztJQUNILENBQUM7OztZQTFERixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtnQkFDOUIsMkZBQThDOzthQUUvQzs7OztZQVRRLFlBQVk7WUFDWixjQUFjO1lBRWQsbUJBQW1COzs7dUJBUXpCLEtBQUs7aUNBQ0wsTUFBTTs7OztJQURQLDBDQUEwQjs7SUFDMUIsb0RBQWtFOzs7OztJQUNsRSx5Q0FBd0I7Ozs7O0lBQ3hCLHdDQUF1Qjs7Ozs7SUFDdkIsMkNBQTBCOzs7OztJQUVkLDhDQUFrQzs7Ozs7SUFBRSxnREFBc0M7Ozs7O0lBQ2xGLHFEQUFnRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgQWN0b3JTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvYWN0b3Iuc2VydmljZSc7XG5pbXBvcnQgeyBQcm9qZWN0U2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL3Byb2plY3Quc2VydmljZSc7XG5pbXBvcnQgeyBQcm9qZWN0IH0gZnJvbSAnLi4vZG9tYWluL3Byb2plY3QnO1xuaW1wb3J0IHsgQWN1bWVuQ29uZmlndXJhdGlvbiB9IGZyb20gJy4uL3NlcnZpY2VzL2FjdW1lbi1jb25maWd1cmF0aW9uJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYWNuLXVzZXItcHJvamVjdHMyJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3VzZXItcHJvamVjdHMyLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vdXNlci1wcm9qZWN0czIuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBVc2VyUHJvamVjdHMyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgdXNlcm5hbWU6IHN0cmluZztcbiAgQE91dHB1dCgpIG9uUHJvamVjdHNSZWNlaXZlZCA9IG5ldyBFdmVudEVtaXR0ZXI8QXJyYXk8UHJvamVjdD4+KCk7XG4gIHByaXZhdGUgYWN0b3JJZDogbnVtYmVyO1xuICBwcml2YXRlIHVzZXJJZDogbnVtYmVyO1xuICBwcml2YXRlIHNlcnZlclVybDogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgYWN0b3JTZXJ2aWNlOiBBY3RvclNlcnZpY2UsIHByaXZhdGUgcHJvamVjdFNlcnZpY2U6IFByb2plY3RTZXJ2aWNlLFxuICAgICAgcHJpdmF0ZSBhY3VtZW5Db25maWd1cmF0aW9uOiBBY3VtZW5Db25maWd1cmF0aW9uKSB7XG4gICAgaWYgKGFjdW1lbkNvbmZpZ3VyYXRpb24uYmFja2VuZFNlcnZpY2VVcmwpIHtcbiAgICAgIHRoaXMuc2VydmVyVXJsID0gYWN1bWVuQ29uZmlndXJhdGlvbi5iYWNrZW5kU2VydmljZVVybDtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zZXJ2ZXJVcmwgPSBcImh0dHA6Ly93d3cuaGVhbHRoYWN1bWVuLmNvLnphL2luc2lnaHQvXCI7XG4gICAgfVxuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgaWYgKHRoaXMudXNlcm5hbWUpIHtcbiAgICAgIHRoaXMuYWN0b3JTZXJ2aWNlLmdldFN5c3RlbVVzZXIodGhpcy51c2VybmFtZSkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICBpZiAocmVzdWx0KSB7XG4gICAgICAgICAgdGhpcy51c2VySWQgPSByZXN1bHQuaWQ7XG4gICAgICAgICAgdGhpcy5hY3RvcklkID0gcmVzdWx0LmFjdG9yLmlkO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5yZWZyZXNoUHJvamVjdHMoKTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMucmVmcmVzaFByb2plY3RzKCk7XG4gICAgfVxuICB9XG5cbiAgcmVmcmVzaFByb2plY3RzKCkge1xuICAgIGlmICh0aGlzLmFjdG9ySWQpIHtcbiAgICAgIGxldCBwcm9qZWN0czogQXJyYXk8UHJvamVjdD4gPSBbXTtcbiAgICAgIHRoaXMucHJvamVjdFNlcnZpY2UuZ2V0UHJvamVjdHModGhpcy5hY3RvcklkKS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICAgIGxldCBwcm9qZWN0SWRzID0ge307XG4gICAgICAgIGZvciAobGV0IHByb2plY3Qgb2YgcmVzdWx0KSB7XG4gICAgICAgICAgaWYgKCFwcm9qZWN0SWRzW3Byb2plY3QuaWRdKSB7XG4gICAgICAgICAgICBwcm9qZWN0Lmljb25VcmwgPSB0aGlzLnNlcnZlclVybCArIFwic3ByaW5nL3Byb2plY3RJY29uL2ltYWdlLnBuZz9pZD1cIiArIHByb2plY3QuaWQ7XG4gICAgICAgICAgICBwcm9qZWN0cy5wdXNoKHByb2plY3QpO1xuICAgICAgICAgICAgcHJvamVjdElkc1twcm9qZWN0LmlkXSA9IHByb2plY3Q7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5vblByb2plY3RzUmVjZWl2ZWQuZW1pdChwcm9qZWN0cyk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMub25Qcm9qZWN0c1JlY2VpdmVkLmVtaXQoW10pO1xuICAgIH1cbiAgfVxufVxuIl19