/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
export class StepInstance {
}
if (false) {
    /** @type {?} */
    StepInstance.prototype.id;
    /** @type {?} */
    StepInstance.prototype.businessProcessInstance;
    /** @type {?} */
    StepInstance.prototype.actor;
    /** @type {?} */
    StepInstance.prototype.step;
    /** @type {?} */
    StepInstance.prototype.assignedProjectFunction;
    /** @type {?} */
    StepInstance.prototype.stepInstanceStatus;
    /** @type {?} */
    StepInstance.prototype.activationDate;
    /** @type {?} */
    StepInstance.prototype.systemUser;
    /** @type {?} */
    StepInstance.prototype.wakeUpDate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RlcC1pbnN0YW5jZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvZG9tYWluL3N0ZXAtaW5zdGFuY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUtBLE1BQU0sT0FBTyxZQUFZO0NBVXhCOzs7SUFUQywwQkFBVzs7SUFDWCwrQ0FBaUQ7O0lBQ2pELDZCQUFhOztJQUNiLDRCQUFXOztJQUNYLCtDQUFnQzs7SUFDaEMsMENBQTJCOztJQUMzQixzQ0FBcUI7O0lBQ3JCLGtDQUF1Qjs7SUFDdkIsa0NBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQnVzaW5lc3NQcm9jZXNzSW5zdGFuY2UgfSBmcm9tICcuL2J1c2luZXNzLXByb2Nlc3MtaW5zdGFuY2UnO1xuaW1wb3J0IHsgQWN0b3IgfSBmcm9tICcuL2FjdG9yJztcbmltcG9ydCB7IFN0ZXAgfSBmcm9tICcuL3N0ZXAnO1xuaW1wb3J0IHsgU3lzdGVtVXNlciB9IGZyb20gJy4vc3lzdGVtLXVzZXInO1xuXG5leHBvcnQgY2xhc3MgU3RlcEluc3RhbmNlIHtcbiAgaWQ6IG51bWJlcjtcbiAgYnVzaW5lc3NQcm9jZXNzSW5zdGFuY2U6IEJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlO1xuICBhY3RvcjogQWN0b3I7XG4gIHN0ZXA6IFN0ZXA7XG4gIGFzc2lnbmVkUHJvamVjdEZ1bmN0aW9uOiBzdHJpbmc7XG4gIHN0ZXBJbnN0YW5jZVN0YXR1czogc3RyaW5nO1xuICBhY3RpdmF0aW9uRGF0ZTogRGF0ZTtcbiAgc3lzdGVtVXNlcjogU3lzdGVtVXNlcjtcbiAgd2FrZVVwRGF0ZTogRGF0ZTtcbn1cbiJdfQ==