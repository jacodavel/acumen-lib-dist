/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
export class Actor {
}
if (false) {
    /** @type {?} */
    Actor.prototype.id;
    /** @type {?} */
    Actor.prototype.firstname;
    /** @type {?} */
    Actor.prototype.surname;
    /** @type {?} */
    Actor.prototype.idNr;
    /** @type {?} */
    Actor.prototype.email;
    /** @type {?} */
    Actor.prototype.celNr;
    /** @type {?} */
    Actor.prototype.title;
    /** @type {?} */
    Actor.prototype.actorType;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWN0b3IuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hY3VtZW4tbGliLyIsInNvdXJjZXMiOlsibGliL2RvbWFpbi9hY3Rvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBRUEsTUFBTSxPQUFPLEtBQUs7Q0FVakI7OztJQVRHLG1CQUFXOztJQUNYLDBCQUFrQjs7SUFDbEIsd0JBQWdCOztJQUNoQixxQkFBYTs7SUFDYixzQkFBYzs7SUFDZCxzQkFBYzs7SUFDZCxzQkFBVzs7SUFFWCwwQkFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBY3RvclR5cGUgfSBmcm9tIFwiLi9hY3Rvci10eXBlXCI7XG5cbmV4cG9ydCBjbGFzcyBBY3RvciB7XG4gICAgaWQ6IG51bWJlcjtcbiAgICBmaXJzdG5hbWU6IHN0cmluZztcbiAgICBzdXJuYW1lOiBzdHJpbmc7XG4gICAgaWROcjogc3RyaW5nO1xuICAgIGVtYWlsOiBzdHJpbmc7XG4gICAgY2VsTnI6IHN0cmluZztcbiAgICB0aXRsZTogYW55O1xuXG4gICAgYWN0b3JUeXBlOiBBY3RvclR5cGU7XG59XG4iXX0=