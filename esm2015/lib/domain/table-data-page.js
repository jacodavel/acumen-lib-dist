/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
export class TableDataPage {
    constructor() {
        this.list = [];
        this.recordCount = 0;
    }
}
if (false) {
    /** @type {?} */
    TableDataPage.prototype.list;
    /** @type {?} */
    TableDataPage.prototype.recordCount;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUtZGF0YS1wYWdlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWN1bWVuLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9kb21haW4vdGFibGUtZGF0YS1wYWdlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFDQSxNQUFNLE9BQU8sYUFBYTtJQUl0QjtRQUNJLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO1FBQ2YsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUM7SUFDekIsQ0FBQztDQUNKOzs7SUFQRyw2QkFBWTs7SUFDWixvQ0FBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJcbmV4cG9ydCBjbGFzcyBUYWJsZURhdGFQYWdlIHtcbiAgICBsaXN0OiBhbnlbXTtcbiAgICByZWNvcmRDb3VudDogbnVtYmVyO1xuXG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHRoaXMubGlzdCA9IFtdO1xuICAgICAgICB0aGlzLnJlY29yZENvdW50ID0gMDtcbiAgICB9XG59XG4iXX0=