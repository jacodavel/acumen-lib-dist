/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
export class QuestionLine {
}
if (false) {
    /** @type {?} */
    QuestionLine.prototype.id;
    /** @type {?} */
    QuestionLine.prototype.wording;
    /** @type {?} */
    QuestionLine.prototype.children;
    /** @type {?} */
    QuestionLine.prototype.parent;
    /** @type {?} */
    QuestionLine.prototype.lineType;
    /** @type {?} */
    QuestionLine.prototype.answerType;
    /** @type {?} */
    QuestionLine.prototype.parameters;
    /** @type {?} */
    QuestionLine.prototype.required;
    /** @type {?} */
    QuestionLine.prototype.questionResponse;
    /** @type {?} */
    QuestionLine.prototype.hasError;
    /** @type {?} */
    QuestionLine.prototype.errorAccepted;
    /** @type {?} */
    QuestionLine.prototype.errorMessage;
    /** @type {?} */
    QuestionLine.prototype.showErrorConfirmation;
    /** @type {?} */
    QuestionLine.prototype.weight;
    /** @type {?} */
    QuestionLine.prototype.integrationIndicator;
    /** @type {?} */
    QuestionLine.prototype.allowSupportingDescription;
    /** @type {?} */
    QuestionLine.prototype.allowSupportingImage;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicXVlc3Rpb24tbGluZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvZG9tYWluL3F1ZXN0aW9uLWxpbmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUVBLE1BQU0sT0FBTyxZQUFZO0NBa0J4Qjs7O0lBakJDLDBCQUFXOztJQUNYLCtCQUFnQjs7SUFDaEIsZ0NBQXlCOztJQUN6Qiw4QkFBcUI7O0lBQ3JCLGdDQUFpQjs7SUFDakIsa0NBQW1COztJQUNuQixrQ0FBbUI7O0lBQ25CLGdDQUFrQjs7SUFDbEIsd0NBQW1DOztJQUNuQyxnQ0FBa0I7O0lBQ2xCLHFDQUF1Qjs7SUFDdkIsb0NBQXFCOztJQUNyQiw2Q0FBK0I7O0lBQy9CLDhCQUFlOztJQUNmLDRDQUE2Qjs7SUFDN0Isa0RBQW9DOztJQUNwQyw0Q0FBOEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBRdWVzdGlvblJlc3BvbnNlIH0gZnJvbSAnLi9xdWVzdGlvbi1yZXNwb25zZSc7XG5cbmV4cG9ydCBjbGFzcyBRdWVzdGlvbkxpbmUge1xuICBpZDogbnVtYmVyO1xuICB3b3JkaW5nOiBzdHJpbmc7XG4gIGNoaWxkcmVuOiBbUXVlc3Rpb25MaW5lXTtcbiAgcGFyZW50OiBRdWVzdGlvbkxpbmU7XG4gIGxpbmVUeXBlOiBzdHJpbmc7XG4gIGFuc3dlclR5cGU6IHN0cmluZztcbiAgcGFyYW1ldGVyczogc3RyaW5nO1xuICByZXF1aXJlZDogYm9vbGVhbjtcbiAgcXVlc3Rpb25SZXNwb25zZTogUXVlc3Rpb25SZXNwb25zZTtcbiAgaGFzRXJyb3I6IGJvb2xlYW47XG4gIGVycm9yQWNjZXB0ZWQ6IGJvb2xlYW47XG4gIGVycm9yTWVzc2FnZTogc3RyaW5nO1xuICBzaG93RXJyb3JDb25maXJtYXRpb246IGJvb2xlYW47XG4gIHdlaWdodDogbnVtYmVyO1xuICBpbnRlZ3JhdGlvbkluZGljYXRvcjogc3RyaW5nO1xuICBhbGxvd1N1cHBvcnRpbmdEZXNjcmlwdGlvbjogYm9vbGVhbjtcbiAgYWxsb3dTdXBwb3J0aW5nSW1hZ2U6IGJvb2xlYW47XG59XG4iXX0=