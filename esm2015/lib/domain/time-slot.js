/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
export class TimeSlot {
}
if (false) {
    /** @type {?} */
    TimeSlot.prototype.id;
    /** @type {?} */
    TimeSlot.prototype.timeSlotType;
    /** @type {?} */
    TimeSlot.prototype.description;
    /** @type {?} */
    TimeSlot.prototype.fromDate;
    /** @type {?} */
    TimeSlot.prototype.toDate;
    /** @type {?} */
    TimeSlot.prototype.actor;
    /** @type {?} */
    TimeSlot.prototype.participant;
    /** @type {?} */
    TimeSlot.prototype.date;
    /** @type {?} */
    TimeSlot.prototype.startTime;
    /** @type {?} */
    TimeSlot.prototype.endTime;
    /** @type {?} */
    TimeSlot.prototype.startIndex;
    /** @type {?} */
    TimeSlot.prototype.endIndex;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGltZS1zbG90LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWN1bWVuLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9kb21haW4vdGltZS1zbG90LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFFQSxNQUFNLE9BQU8sUUFBUTtDQWVwQjs7O0lBZEcsc0JBQVc7O0lBQ1gsZ0NBQXFCOztJQUNyQiwrQkFBb0I7O0lBQ3BCLDRCQUFlOztJQUNmLDBCQUFhOztJQUNiLHlCQUFhOztJQUNiLCtCQUFtQjs7SUFFbkIsd0JBQVU7O0lBQ1YsNkJBQWtCOztJQUNsQiwyQkFBZ0I7O0lBRWhCLDhCQUFtQjs7SUFDbkIsNEJBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQWN0b3IgfSBmcm9tICcuL2FjdG9yJztcblxuZXhwb3J0IGNsYXNzIFRpbWVTbG90IHtcbiAgICBpZDogbnVtYmVyO1xuICAgIHRpbWVTbG90VHlwZTogc3RyaW5nO1xuICAgIGRlc2NyaXB0aW9uOiBzdHJpbmc7XG4gICAgZnJvbURhdGU6IERhdGU7XG4gICAgdG9EYXRlOiBEYXRlO1xuICAgIGFjdG9yOiBBY3RvcjtcbiAgICBwYXJ0aWNpcGFudDogQWN0b3I7XG5cbiAgICBkYXRlOiBhbnk7XG4gICAgc3RhcnRUaW1lOiBzdHJpbmc7XG4gICAgZW5kVGltZTogc3RyaW5nO1xuXG4gICAgc3RhcnRJbmRleDogbnVtYmVyO1xuICAgIGVuZEluZGV4OiBudW1iZXI7XG59XG4iXX0=