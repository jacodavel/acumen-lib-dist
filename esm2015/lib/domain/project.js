/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
export class Project {
}
if (false) {
    /** @type {?} */
    Project.prototype.id;
    /** @type {?} */
    Project.prototype.projectName;
    /** @type {?} */
    Project.prototype.description;
    /** @type {?} */
    Project.prototype.guid;
    /** @type {?} */
    Project.prototype.wizardProject;
    /** @type {?} */
    Project.prototype.registrationProject;
    /** @type {?} */
    Project.prototype.actorProjectFunction;
    /** @type {?} */
    Project.prototype.iconUrl;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FjdW1lbi1saWIvIiwic291cmNlcyI6WyJsaWIvZG9tYWluL3Byb2plY3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUNBLE1BQU0sT0FBTyxPQUFPO0NBU25COzs7SUFSRyxxQkFBVzs7SUFDWCw4QkFBb0I7O0lBQ3BCLDhCQUFvQjs7SUFDcEIsdUJBQWE7O0lBQ2IsZ0NBQXVCOztJQUN2QixzQ0FBNkI7O0lBQzdCLHVDQUE2Qjs7SUFDN0IsMEJBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiXG5leHBvcnQgY2xhc3MgUHJvamVjdCB7XG4gICAgaWQ6IG51bWJlcjtcbiAgICBwcm9qZWN0TmFtZTogc3RyaW5nO1xuICAgIGRlc2NyaXB0aW9uOiBzdHJpbmc7XG4gICAgZ3VpZDogc3RyaW5nO1xuICAgIHdpemFyZFByb2plY3Q6IGJvb2xlYW47XG4gICAgcmVnaXN0cmF0aW9uUHJvamVjdDogYm9vbGVhbjtcbiAgICBhY3RvclByb2plY3RGdW5jdGlvbjogc3RyaW5nO1xuICAgIGljb25Vcmw6IHN0cmluZztcbn1cbiJdfQ==