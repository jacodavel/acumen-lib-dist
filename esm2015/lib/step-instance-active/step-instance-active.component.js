/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { DomSanitizer } from '@angular/platform-browser';
import { ActorService } from '../services/actor.service';
import { ProjectService } from '../services/project.service';
import { AnalysisService } from '../services/analysis.service';
export class StepInstanceActiveComponent {
    /**
     * @param {?} actorService
     * @param {?} projectService
     * @param {?} analysisService
     * @param {?} sanitizer
     */
    constructor(actorService, projectService, analysisService, sanitizer) {
        this.actorService = actorService;
        this.projectService = projectService;
        this.analysisService = analysisService;
        this.sanitizer = sanitizer;
        this.isActive = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.subscription = Observable.timer(1000, 10000).subscribe((/**
         * @param {?} t
         * @return {?}
         */
        t => {
            this.loadStepInstances();
        }));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
    /**
     * @private
     * @return {?}
     */
    loadStepInstances() {
        this.projectService.isStepInstanceActive(this.stepId, this.externalId).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            this.isActive = "true" === result.response;
        }));
    }
}
StepInstanceActiveComponent.decorators = [
    { type: Component, args: [{
                selector: 'acn-step-instance-active',
                template: "<div class=\"step-instance-active\">\n  <div *ngIf=\"isActive\">\n    <ng-content>\n    </ng-content>\n  </div>\n</div>\n",
                styles: [".step-instance-active{width:100%;height:100%;position:relative}"]
            }] }
];
/** @nocollapse */
StepInstanceActiveComponent.ctorParameters = () => [
    { type: ActorService },
    { type: ProjectService },
    { type: AnalysisService },
    { type: DomSanitizer }
];
StepInstanceActiveComponent.propDecorators = {
    stepId: [{ type: Input }],
    externalId: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    StepInstanceActiveComponent.prototype.stepId;
    /** @type {?} */
    StepInstanceActiveComponent.prototype.externalId;
    /**
     * @type {?}
     * @private
     */
    StepInstanceActiveComponent.prototype.subscription;
    /** @type {?} */
    StepInstanceActiveComponent.prototype.isActive;
    /**
     * @type {?}
     * @private
     */
    StepInstanceActiveComponent.prototype.actorService;
    /**
     * @type {?}
     * @private
     */
    StepInstanceActiveComponent.prototype.projectService;
    /**
     * @type {?}
     * @private
     */
    StepInstanceActiveComponent.prototype.analysisService;
    /**
     * @type {?}
     * @private
     */
    StepInstanceActiveComponent.prototype.sanitizer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RlcC1pbnN0YW5jZS1hY3RpdmUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWN1bWVuLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9zdGVwLWluc3RhbmNlLWFjdGl2ZS9zdGVwLWluc3RhbmNlLWFjdGl2ZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQXFCLEtBQUssRUFBd0IsTUFBTSxlQUFlLENBQUM7QUFDMUYsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUVyQyxPQUFPLEVBQUUsWUFBWSxFQUFtQixNQUFNLDJCQUEyQixDQUFDO0FBRTFFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUN6RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDN0QsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBYS9ELE1BQU0sT0FBTywyQkFBMkI7Ozs7Ozs7SUFNdEMsWUFBb0IsWUFBMEIsRUFBVSxjQUE4QixFQUMxRSxlQUFnQyxFQUFVLFNBQXVCO1FBRHpELGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQVUsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzFFLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUFVLGNBQVMsR0FBVCxTQUFTLENBQWM7UUFIN0UsYUFBUSxHQUFZLEtBQUssQ0FBQztJQUkxQixDQUFDOzs7O0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxZQUFZLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUMsU0FBUzs7OztRQUFDLENBQUMsQ0FBQyxFQUFFO1lBQzlELElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBQzNCLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVELFdBQVc7UUFDVCxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDckIsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNqQztJQUNILENBQUM7Ozs7O0lBRU8saUJBQWlCO1FBQ3ZCLElBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSTs7OztRQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQ25GLElBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxLQUFLLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFDN0MsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7WUEvQkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSwwQkFBMEI7Z0JBQ3BDLHFJQUFvRDs7YUFFckQ7Ozs7WUFkUSxZQUFZO1lBQ1osY0FBYztZQUNkLGVBQWU7WUFKZixZQUFZOzs7cUJBa0JsQixLQUFLO3lCQUNMLEtBQUs7Ozs7SUFETiw2Q0FBd0I7O0lBQ3hCLGlEQUE0Qjs7Ozs7SUFDNUIsbURBQW1DOztJQUNuQywrQ0FBMEI7Ozs7O0lBRWQsbURBQWtDOzs7OztJQUFFLHFEQUFzQzs7Ozs7SUFDbEYsc0RBQXdDOzs7OztJQUFFLGdEQUErQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBPbkRlc3Ryb3ksIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMvUngnO1xuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcy9SeCc7XG5pbXBvcnQgeyBEb21TYW5pdGl6ZXIsIFNhZmVSZXNvdXJjZVVybCB9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXInO1xuXG5pbXBvcnQgeyBBY3RvclNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9hY3Rvci5zZXJ2aWNlJztcbmltcG9ydCB7IFByb2plY3RTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvcHJvamVjdC5zZXJ2aWNlJztcbmltcG9ydCB7IEFuYWx5c2lzU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2FuYWx5c2lzLnNlcnZpY2UnO1xuaW1wb3J0IHsgU3RlcEluc3RhbmNlIH0gZnJvbSAnLi4vZG9tYWluL3N0ZXAtaW5zdGFuY2UnO1xuaW1wb3J0IHsgUHJvamVjdCB9IGZyb20gJy4uL2RvbWFpbi9wcm9qZWN0JztcbmltcG9ydCB7IEFjdG9yIH0gZnJvbSAnLi4vZG9tYWluL2FjdG9yJztcbmltcG9ydCB7IEJ1c2luZXNzUHJvY2Vzc0luc3RhbmNlIH0gZnJvbSAnLi4vZG9tYWluL2J1c2luZXNzLXByb2Nlc3MtaW5zdGFuY2UnO1xuaW1wb3J0IHsgUXVlc3Rpb25MaW5lIH0gZnJvbSAnLi4vZG9tYWluL3F1ZXN0aW9uLWxpbmUnO1xuaW1wb3J0IHsgSW1hZ2VDb250YWluZXIgfSBmcm9tICcuLi9kb21haW4vaW1hZ2UtY29udGFpbmVyJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYWNuLXN0ZXAtaW5zdGFuY2UtYWN0aXZlJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3N0ZXAtaW5zdGFuY2UtYWN0aXZlLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vc3RlcC1pbnN0YW5jZS1hY3RpdmUuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBTdGVwSW5zdGFuY2VBY3RpdmVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIHN0ZXBJZDogbnVtYmVyO1xuICBASW5wdXQoKSBleHRlcm5hbElkOiBzdHJpbmc7XG4gIHByaXZhdGUgc3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XG4gIGlzQWN0aXZlOiBib29sZWFuID0gZmFsc2U7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBhY3RvclNlcnZpY2U6IEFjdG9yU2VydmljZSwgcHJpdmF0ZSBwcm9qZWN0U2VydmljZTogUHJvamVjdFNlcnZpY2UsXG4gICAgICBwcml2YXRlIGFuYWx5c2lzU2VydmljZTogQW5hbHlzaXNTZXJ2aWNlLCBwcml2YXRlIHNhbml0aXplcjogRG9tU2FuaXRpemVyKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLnN1YnNjcmlwdGlvbiA9IE9ic2VydmFibGUudGltZXIoMTAwMCwgMTAwMDApLnN1YnNjcmliZSh0ID0+IHtcbiAgICAgIHRoaXMubG9hZFN0ZXBJbnN0YW5jZXMoKTtcbiAgICB9KTtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCkge1xuICAgIGlmICh0aGlzLnN1YnNjcmlwdGlvbikge1xuICAgICAgdGhpcy5zdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGxvYWRTdGVwSW5zdGFuY2VzKCkge1xuICAgIHRoaXMucHJvamVjdFNlcnZpY2UuaXNTdGVwSW5zdGFuY2VBY3RpdmUodGhpcy5zdGVwSWQsIHRoaXMuZXh0ZXJuYWxJZCkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgdGhpcy5pc0FjdGl2ZSA9IFwidHJ1ZVwiID09PSByZXN1bHQucmVzcG9uc2U7XG4gICAgfSk7XG4gIH1cbn1cbiJdfQ==