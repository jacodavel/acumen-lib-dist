import { OnInit, EventEmitter } from '@angular/core';
import { StepInstance } from '../domain/step-instance';
import { ImageContainer } from '../domain/image-container';
import { ActorService } from '../services/actor.service';
import { ProjectService } from '../services/project.service';
import { CodeReadListener } from '../steps/code-reader/code-read-listener';
export declare class StepInstanceComponent implements OnInit {
    private actorService;
    private projectService;
    stepInstance: StepInstance;
    userId: number;
    username: string;
    latitude: number;
    longitude: number;
    onStepCompleted: EventEmitter<{}>;
    onStepCancelled: EventEmitter<{}>;
    onOpenCameraListener: EventEmitter<ImageContainer>;
    onReadCode: EventEmitter<CodeReadListener>;
    constructor(actorService: ActorService, projectService: ProjectService);
    ngOnInit(): void;
    stepCompletedListener(event: any): void;
    cancelListener(event: any): void;
    openCamera(imageContainer: ImageContainer): void;
    readCode(codeReadListener: CodeReadListener): void;
}
