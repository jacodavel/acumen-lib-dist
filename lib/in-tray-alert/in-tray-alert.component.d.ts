import { OnInit, EventEmitter, OnDestroy } from '@angular/core';
import { ProjectService } from '../services/project.service';
import { ActorService } from '../services/actor.service';
import { AcumenConfiguration } from '../services/acumen-configuration';
export declare class InTrayAlertComponent implements OnInit, OnDestroy {
    private projectService;
    private actorService;
    private acumenConfiguration;
    assigneeId: number;
    username: string;
    checkInterval: number;
    alertTime: number;
    onSelected: EventEmitter<{}>;
    private subscription;
    private serviceUrl;
    blink: boolean;
    constructor(projectService: ProjectService, actorService: ActorService, acumenConfiguration: AcumenConfiguration);
    ngOnInit(): void;
    ngOnDestroy(): void;
    startPolling(): void;
    selected(): void;
}
