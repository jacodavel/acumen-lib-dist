import { OnInit, OnDestroy } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActorService } from '../services/actor.service';
import { ProjectService } from '../services/project.service';
import { AnalysisService } from '../services/analysis.service';
export declare class StepInstanceActiveComponent implements OnInit, OnDestroy {
    private actorService;
    private projectService;
    private analysisService;
    private sanitizer;
    stepId: number;
    externalId: string;
    private subscription;
    isActive: boolean;
    constructor(actorService: ActorService, projectService: ProjectService, analysisService: AnalysisService, sanitizer: DomSanitizer);
    ngOnInit(): void;
    ngOnDestroy(): void;
    private loadStepInstances;
}
