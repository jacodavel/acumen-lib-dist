import { OnInit, OnDestroy } from '@angular/core';
export declare class LoadingIndicator implements OnInit, OnDestroy {
    show: boolean;
    constructor();
    ngOnInit(): void;
    ngOnDestroy(): void;
}
