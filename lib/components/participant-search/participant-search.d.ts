import { OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { ActorService } from '../../services/actor.service';
import { Project } from '../../domain/project';
import { ProjectActor } from '../../domain/project-actor';
import { ActorSearchDto } from '../../domain/actor-search-dto';
export declare class ParticipantSearch implements OnInit, OnDestroy {
    private projectService;
    private actorService;
    show: boolean;
    project: Project;
    onParticipantSelected: EventEmitter<ProjectActor>;
    searchField: string;
    participants: Array<ActorSearchDto>;
    constructor(projectService: ProjectService, actorService: ActorService);
    ngOnInit(): void;
    ngOnDestroy(): void;
    search(): void;
    participantSelected(participant: ActorSearchDto): void;
}
