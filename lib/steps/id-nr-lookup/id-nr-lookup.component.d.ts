import { EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ProjectService } from '../../services/project.service';
import { StepInstance } from '../../domain/step-instance';
export declare class IdNrLookupComponent implements OnInit, OnDestroy {
    private sanitizer;
    private projectService;
    stepInstance: StepInstance;
    userId: number;
    stepCompletedListener: EventEmitter<{}>;
    idNr: string;
    noIdNr: boolean;
    idNrNotFound: boolean;
    invalidIdNr: boolean;
    hideNoIdNrWarning: boolean;
    hideNoIdNrOption: boolean;
    idNrLabel: string;
    html: any;
    loading: boolean;
    constructor(sanitizer: DomSanitizer, projectService: ProjectService);
    ngOnInit(): void;
    ngOnDestroy(): void;
    continueWithIdNr(): void;
    continueNoIdNr(): void;
    completeStep(): void;
}
