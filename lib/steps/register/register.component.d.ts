import { EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { StepInstance } from '../../domain/step-instance';
export declare class RegisterComponent implements OnInit, OnDestroy {
    private projectService;
    stepInstance: StepInstance;
    userId: number;
    stepCompletedListener: EventEmitter<{}>;
    cancelListener: EventEmitter<{}>;
    html: string;
    agreed: boolean;
    agreedError: boolean;
    agreeWording: string;
    loading: boolean;
    constructor(projectService: ProjectService);
    ngOnInit(): void;
    ngOnDestroy(): void;
    completeStep(): void;
    radioButtonChanged($selected: any): void;
    cancel(): void;
}
