export interface CodeReadListener {
    setValue(value: string): any;
}
