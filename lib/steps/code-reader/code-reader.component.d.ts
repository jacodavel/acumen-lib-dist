import { EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { StepInstance } from '../../domain/step-instance';
import { CodeReadListener } from './code-read-listener';
export declare class CodeReaderComponent implements OnInit, OnDestroy, CodeReadListener {
    private projectService;
    stepInstance: StepInstance;
    userId: number;
    onReadCode: EventEmitter<CodeReadListener>;
    stepCompletedListener: EventEmitter<{}>;
    cancelListener: EventEmitter<{}>;
    _stepInstance: StepInstance;
    loading: boolean;
    code: string;
    description: string;
    variableName: string;
    constructor(projectService: ProjectService);
    ngOnInit(): void;
    ngOnDestroy(): void;
    readCode(): void;
    completeStep(): void;
    cancel(): void;
    setValue(value: string): void;
}
