import { EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ProjectService } from '../../services/project.service';
import { StepInstance } from '../../domain/step-instance';
export declare class MessageComponent implements OnInit, OnDestroy {
    private projectService;
    private sanitizer;
    _stepInstance: StepInstance;
    userId: number;
    stepCompletedListener: EventEmitter<{}>;
    cancelListener: EventEmitter<{}>;
    error: any;
    html: any;
    loading: boolean;
    stepInstance: StepInstance;
    constructor(projectService: ProjectService, sanitizer: DomSanitizer);
    ngOnInit(): void;
    ngOnDestroy(): void;
    completeStep(): void;
    cancel(): void;
}
