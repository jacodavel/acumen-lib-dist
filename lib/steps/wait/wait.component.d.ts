import { EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { StepInstance } from '../../domain/step-instance';
export declare class WaitComponent implements OnInit, OnDestroy {
    private projectService;
    _stepInstance: StepInstance;
    userId: number;
    stepCompletedListener: EventEmitter<{}>;
    cancelListener: EventEmitter<{}>;
    message: string;
    showOverride: boolean;
    loading: boolean;
    stepInstance: StepInstance;
    constructor(projectService: ProjectService);
    ngOnInit(): void;
    ngOnDestroy(): void;
    completeStep(): void;
    cancel(): void;
}
