import { EventEmitter, OnInit, OnDestroy } from '@angular/core';
export declare class CheckboxComponent implements OnInit, OnDestroy {
    description: string;
    selected: boolean;
    selectionListener: EventEmitter<{}>;
    constructor();
    ngOnInit(): void;
    ngOnDestroy(): void;
    clicked(): void;
}
