import { OnInit, OnChanges, OnDestroy, SimpleChanges, EventEmitter } from '@angular/core';
import { QuestionLine } from '../../domain/question-line';
import { SelectQuestionOption } from '../../domain/select-question-option';
export declare class QuestionLineComponent implements OnInit, OnDestroy, OnChanges {
    questionLine: QuestionLine;
    onTakePictureQuestionLine: EventEmitter<QuestionLine>;
    onTakeSupportingPictureQuestionLine: EventEmitter<QuestionLine>;
    onQuestionAnswered: EventEmitter<QuestionLine>;
    selectQuestionOptions: SelectQuestionOption[];
    constructor();
    ngOnInit(): void;
    ngOnChanges(changes: SimpleChanges): void;
    ngOnDestroy(): void;
    radioButtonChanged($selected: any): void;
    checkboxChanged($selected: any): void;
    selectChanged(description: any): void;
    takePicture(questionLine: QuestionLine): void;
    takeSupportingPicture(questionLine: any): void;
    questionAnswered(questionLine: QuestionLine): void;
}
