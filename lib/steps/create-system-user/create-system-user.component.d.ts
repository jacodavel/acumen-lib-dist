import { EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { StepInstance } from '../../domain/step-instance';
export declare class CreateSystemUserComponent implements OnInit, OnDestroy {
    private projectService;
    stepInstance: StepInstance;
    userId: number;
    stepCompletedListener: EventEmitter<{}>;
    cancelListener: EventEmitter<{}>;
    username: string;
    password1: string;
    password2: string;
    loading: boolean;
    usernameError: boolean;
    usernameExistsError: boolean;
    password1Error: boolean;
    password2Error: boolean;
    constructor(projectService: ProjectService);
    ngOnInit(): void;
    ngOnDestroy(): void;
    completeStep(): void;
    cancel(): void;
}
