import { EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { StepInstance } from '../../domain/step-instance';
import { ImageContainer } from '../../domain/image-container';
export declare class FacialAuthenticationComponent implements OnInit, OnDestroy {
    private projectService;
    _stepInstance: StepInstance;
    userId: number;
    uploadText: string;
    onOpenCameraListener: EventEmitter<ImageContainer>;
    stepCompletedListener: EventEmitter<{}>;
    cancelListener: EventEmitter<{}>;
    loading: boolean;
    imageContainer: ImageContainer;
    stepInstance: StepInstance;
    constructor(projectService: ProjectService);
    ngOnInit(): void;
    ngOnDestroy(): void;
    openCamera(): void;
    retake(): void;
    completeStep(): void;
    cancel(): void;
}
