import { OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActorService } from '../services/actor.service';
import { ProjectService } from '../services/project.service';
import { AnalysisService } from '../services/analysis.service';
import { StepInstance } from '../domain/step-instance';
export declare class StepInstanceListComponent implements OnInit, OnDestroy {
    private actorService;
    private projectService;
    private analysisService;
    private sanitizer;
    stepIds: Array<number>;
    onStepInstanceSelected: EventEmitter<StepInstance>;
    stepInstances: Array<StepInstance>;
    selectedStepInstance: StepInstance;
    private subscription;
    loading: boolean;
    descriptions: {};
    constructor(actorService: ActorService, projectService: ProjectService, analysisService: AnalysisService, sanitizer: DomSanitizer);
    ngOnInit(): void;
    ngOnDestroy(): void;
    private loadStepInstances;
    stepInstanceSelected(stepInstance: StepInstance): void;
}
