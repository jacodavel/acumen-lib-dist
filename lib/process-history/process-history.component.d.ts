import { OnInit, OnDestroy } from '@angular/core';
import { ActorService } from '../services/actor.service';
import { ProjectService } from '../services/project.service';
import { AnalysisService } from '../services/analysis.service';
import { StepInstance } from '../domain/step-instance';
import { Project } from '../domain/project';
import { Actor } from '../domain/actor';
export declare class ProcessHistoryComponent implements OnInit, OnDestroy {
    private actorService;
    private projectService;
    private analysisService;
    project: Project;
    projectGuid: string;
    externalActorIdNr: string;
    actor: Actor;
    stepInstances: Array<StepInstance>;
    private subscription;
    loading: boolean;
    constructor(actorService: ActorService, projectService: ProjectService, analysisService: AnalysisService);
    ngOnInit(): void;
    ngOnDestroy(): void;
    private loadStepInstances;
}
