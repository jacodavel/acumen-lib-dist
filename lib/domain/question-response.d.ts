export declare class QuestionResponse {
    id: number;
    response: string;
    comments: string;
    weight: number;
    selected: boolean;
    questionLineId: number;
    supportingDescription: string;
    supportingImage: string;
}
