export declare class Step {
    id: number;
    stepType: String;
    parameters: any;
    parentStep: Step;
}
