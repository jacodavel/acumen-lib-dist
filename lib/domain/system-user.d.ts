import { Actor } from './actor';
export declare class SystemUser {
    id: number;
    username: string;
    systemRole: string;
    guid: string;
    actor: Actor;
    evaluatorProjects: number[];
}
