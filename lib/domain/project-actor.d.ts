import { Actor } from './actor';
import { ProjectRole } from './project-role';
export declare class ProjectActor {
    id: number;
    actor: Actor;
    projectRole: ProjectRole;
    canChangeProcessVariables: boolean;
}
