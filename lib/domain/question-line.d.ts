import { QuestionResponse } from './question-response';
export declare class QuestionLine {
    id: number;
    wording: string;
    children: [QuestionLine];
    parent: QuestionLine;
    lineType: string;
    answerType: string;
    parameters: string;
    required: boolean;
    questionResponse: QuestionResponse;
    hasError: boolean;
    errorAccepted: boolean;
    errorMessage: string;
    showErrorConfirmation: boolean;
    weight: number;
    integrationIndicator: string;
    allowSupportingDescription: boolean;
    allowSupportingImage: boolean;
}
