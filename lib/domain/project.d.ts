export declare class Project {
    id: number;
    projectName: string;
    description: string;
    guid: string;
    wizardProject: boolean;
    registrationProject: boolean;
    actorProjectFunction: string;
    iconUrl: string;
}
