import { QuestionLine } from './question-line';
export declare class Questionnaire {
    id: number;
    description: string;
    questionLines: [QuestionLine];
}
