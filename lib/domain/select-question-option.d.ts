export declare class SelectQuestionOption {
    index: number;
    description: string;
    weight: number;
    selected: boolean;
    response: string;
}
