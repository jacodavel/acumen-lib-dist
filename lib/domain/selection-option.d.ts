export declare class SelectionOption {
    id: number;
    description: string;
    defaultOption: boolean;
}
