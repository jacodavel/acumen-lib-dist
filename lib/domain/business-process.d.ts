import { Project } from './project';
export declare class BusinessProcess {
    id: number;
    name: string;
    description: string;
    project: Project;
}
