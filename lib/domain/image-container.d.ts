export interface ImageContainer {
    imageData?: string;
    setImage(image: string): any;
}
