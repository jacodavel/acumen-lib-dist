export declare class ActorFieldDto {
    sequenceNr: number;
    fieldName: string;
    fieldValue: string;
}
