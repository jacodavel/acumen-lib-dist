import { BusinessProcess } from './business-process';
import { Actor } from './actor';
export declare class BusinessProcessInstance {
    id: number;
    wording: string;
    businessProcess: BusinessProcess;
    actor: Actor;
    processStatus: string;
}
