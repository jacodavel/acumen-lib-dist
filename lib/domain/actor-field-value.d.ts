export declare class ActorFieldValue {
    actorFieldValueId: number;
    actorFieldId: number;
    fieldValue: string;
}
