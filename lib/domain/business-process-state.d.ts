import { BusinessProcessInstance } from './business-process-instance';
import { StepInstance } from './step-instance';
export declare class BusinessProcessState {
    businessProcessInstance: BusinessProcessInstance;
    stepInstance: StepInstance;
}
