export declare class ProjectRole {
    id: number;
    description: string;
    projectFunction: string;
}
