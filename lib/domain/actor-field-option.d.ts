export declare class ActorFieldOption {
    id: number;
    fieldValue: string;
    description: string;
}
