export declare class ActorField {
    id: number;
    description: string;
    actorFieldType: string;
    sequenceNr: number;
    required: boolean;
}
