import { BusinessProcessInstance } from './business-process-instance';
import { Actor } from './actor';
import { Step } from './step';
import { SystemUser } from './system-user';
export declare class StepInstance {
    id: number;
    businessProcessInstance: BusinessProcessInstance;
    actor: Actor;
    step: Step;
    assignedProjectFunction: string;
    stepInstanceStatus: string;
    activationDate: Date;
    systemUser: SystemUser;
    wakeUpDate: Date;
}
