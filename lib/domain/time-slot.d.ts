import { Actor } from './actor';
export declare class TimeSlot {
    id: number;
    timeSlotType: string;
    description: string;
    fromDate: Date;
    toDate: Date;
    actor: Actor;
    participant: Actor;
    date: any;
    startTime: string;
    endTime: string;
    startIndex: number;
    endIndex: number;
}
