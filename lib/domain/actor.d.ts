import { ActorType } from "./actor-type";
export declare class Actor {
    id: number;
    firstname: string;
    surname: string;
    idNr: string;
    email: string;
    celNr: string;
    title: any;
    actorType: ActorType;
}
