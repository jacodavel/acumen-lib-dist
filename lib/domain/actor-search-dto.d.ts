import { ActorFieldDto } from './actor-field-dto';
export declare class ActorSearchDto {
    id: number;
    firstname: string;
    surname: string;
    idNr: string;
    actorFieldValues: Array<ActorFieldDto>;
}
