import { HttpClient } from '@angular/common/http';
import { ActorField } from '../domain/actor-field';
import { ActorFieldOption } from '../domain/actor-field-option';
import { AcumenConfiguration } from '../services/acumen-configuration';
export declare class MaintenanceService {
    private httpClient;
    private acumenConfiguration;
    private backendServiceUrl;
    constructor(httpClient: HttpClient, acumenConfiguration: AcumenConfiguration);
    getActorFields(actorId: number): Promise<ActorField[]>;
    getActorFieldOptions(actorFieldId: number): Promise<ActorFieldOption[]>;
}
