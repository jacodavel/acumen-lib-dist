import { HttpClient } from '@angular/common/http';
import { AcumenConfiguration } from '../services/acumen-configuration';
export declare class AnalysisService {
    private httpClient;
    private acumenConfiguration;
    private backendServiceUrl;
    constructor(httpClient: HttpClient, acumenConfiguration: AcumenConfiguration);
    getProjectCompletionCount(projectGuid: string): Promise<string>;
    getWellnessData(): Promise<any>;
}
