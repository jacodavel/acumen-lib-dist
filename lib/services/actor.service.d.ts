import { HttpClient } from '@angular/common/http';
import { SystemUser } from '../domain/system-user';
import { Actor } from '../domain/actor';
import { ProjectActor } from '../domain/project-actor';
import { ActorFieldValue } from '../domain/actor-field-value';
import { AcumenConfiguration } from '../services/acumen-configuration';
export declare class ActorService {
    private httpClient;
    private acumenConfiguration;
    private backendServiceUrl;
    constructor(httpClient: HttpClient, acumenConfiguration: AcumenConfiguration);
    getSystemUser(username: string): Promise<SystemUser>;
    getActorFromExternalId(externalActorId: string): Promise<Actor>;
    getProjectActorFromExternalId(externalActorId: string, projectId: number): Promise<Actor>;
    getProjectActor(actorId: number, projectId: number): Promise<ProjectActor>;
    getActorFieldValues(actorId: number): Promise<ActorFieldValue[]>;
    updateActorFieldValues(actorId: number, actorFieldValues: Array<ActorFieldValue>): Promise<any>;
}
