import { OnInit, EventEmitter } from '@angular/core';
import { ActorService } from '../services/actor.service';
import { ProjectService } from '../services/project.service';
import { Project } from '../domain/project';
import { AcumenConfiguration } from '../services/acumen-configuration';
export declare class UserProjects2Component implements OnInit {
    private actorService;
    private projectService;
    private acumenConfiguration;
    username: string;
    onProjectsReceived: EventEmitter<Project[]>;
    private actorId;
    private userId;
    private serverUrl;
    constructor(actorService: ActorService, projectService: ProjectService, acumenConfiguration: AcumenConfiguration);
    ngOnInit(): void;
    refreshProjects(): void;
}
