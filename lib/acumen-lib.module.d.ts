import { ModuleWithProviders } from '@angular/core';
import { AcumenConfiguration } from './services/acumen-configuration';
export declare class AcumenLibModule {
    static forRoot(acumenConfiguration: AcumenConfiguration): ModuleWithProviders;
}
