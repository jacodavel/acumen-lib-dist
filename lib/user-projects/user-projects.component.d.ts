import { OnInit, EventEmitter } from '@angular/core';
import { ActorService } from '../services/actor.service';
import { ProjectService } from '../services/project.service';
import { Project } from '../domain/project';
import { AcumenConfiguration } from '../services/acumen-configuration';
export declare class UserProjectsComponent implements OnInit {
    private actorService;
    private projectService;
    private acumenConfiguration;
    username: string;
    actorId: number;
    userId: number;
    onProjectSelected: EventEmitter<Project>;
    serverUrl: any;
    projects: Array<Project>;
    loading: boolean;
    constructor(actorService: ActorService, projectService: ProjectService, acumenConfiguration: AcumenConfiguration);
    ngOnInit(): void;
    refreshProjects(): void;
    projectSelected(project: Project): void;
}
