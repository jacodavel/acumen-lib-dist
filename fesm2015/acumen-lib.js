import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import * as momentImported from 'moment';
import { Injectable, Component, Input, EventEmitter, Output, NgModule, defineInjectable } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AcumenLibService {
    constructor() { }
}
AcumenLibService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
AcumenLibService.ctorParameters = () => [];
/** @nocollapse */ AcumenLibService.ngInjectableDef = defineInjectable({ factory: function AcumenLibService_Factory() { return new AcumenLibService(); }, token: AcumenLibService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AcumenLibComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
AcumenLibComponent.decorators = [
    { type: Component, args: [{
                selector: 'acn-acumen-lib',
                template: `
    <p>
      acumen-lib works!
    </p>
  `
            }] }
];
/** @nocollapse */
AcumenLibComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AcumenConfiguration {
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ProjectService {
    // private backendServiceUrl = 'http://www.healthacumen.co.za/insight/';
    // private backendServiceUrl = 'https://frank.rmsui.co.za/insight/';
    // private backendServiceUrl = 'http://localhost:8888/';
    /**
     * @param {?} httpClient
     * @param {?} acumenConfiguration
     */
    constructor(httpClient, acumenConfiguration) {
        this.httpClient = httpClient;
        this.acumenConfiguration = acumenConfiguration;
        if (acumenConfiguration.backendServiceUrl) {
            this.backendServiceUrl = acumenConfiguration.backendServiceUrl;
        }
        else {
            this.backendServiceUrl = "https://frank.rmsui.co.za/insight/";
        }
    }
    /**
     * @param {?} projectGuid
     * @param {?} actorId
     * @return {?}
     */
    startWizardProject(projectGuid, actorId) {
        /** @type {?} */
        let url = this.backendServiceUrl;
        if (actorId) {
            url += `rest/project/projects/start2/${projectGuid}/${actorId}`;
        }
        else {
            url += `rest/project/projects/start/wizard/${projectGuid}`;
        }
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} businessProcessInstanceId
     * @return {?}
     */
    getBusinessProcessState(businessProcessInstanceId) {
        /** @type {?} */
        let url = this.backendServiceUrl + "rest/project/projects/state/" + businessProcessInstanceId + "?timestamp=" + new Date().getTime();
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} projectGuid
     * @return {?}
     */
    getProject(projectGuid) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/project/${projectGuid}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} actorId
     * @return {?}
     */
    getProjects(actorId) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/actorProjects/${actorId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} systemUserId
     * @return {?}
     */
    getUserProjects(systemUserId) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/projects/${systemUserId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} projectId
     * @return {?}
     */
    getProjectRoles(projectId) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/projectRoles/${projectId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} fromRecord
     * @param {?} recordCount
     * @param {?} projectId
     * @param {?} projectRoleId
     * @param {?} firstname
     * @param {?} surname
     * @param {?} idNr
     * @param {?} memberNr
     * @param {?} dependantCode
     * @param {?} assigneeId
     * @return {?}
     */
    getProjectActors(fromRecord, recordCount, projectId, projectRoleId, firstname, surname, idNr, memberNr, dependantCode, assigneeId) {
        if (!firstname)
            firstname = "null";
        if (!surname)
            surname = "null";
        if (!idNr)
            idNr = "null";
        if (!memberNr)
            memberNr = "null";
        if (!dependantCode)
            dependantCode = "null";
        // assigneeId = -1;
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/projectActors/${fromRecord}/${recordCount}/${projectId}/${projectRoleId}/${firstname}/${surname}/${idNr}/${memberNr}/${dependantCode}/${assigneeId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} fromRecord
     * @param {?} recordCount
     * @param {?} projectId
     * @param {?} projectRoleId
     * @return {?}
     */
    getProjectActors2(fromRecord, recordCount, projectId, projectRoleId) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/projectActors2/${fromRecord}/${recordCount}/${projectId}/${projectRoleId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} fromRecord
     * @param {?} recordCount
     * @param {?} projectRoleId
     * @param {?} searchField
     * @return {?}
     */
    getProjectActors3(fromRecord, recordCount, projectRoleId, searchField) {
        if (!searchField)
            searchField = "null";
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/projectActors3/${fromRecord}/${recordCount}/${projectRoleId}/${searchField}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} participantId
     * @return {?}
     */
    getStepInstances(participantId) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/stepInstances/${participantId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} projectGuid
     * @param {?} userId
     * @param {?} assigneeId
     * @param {?} participantId
     * @return {?}
     */
    getStepInstances2(projectGuid, userId, assigneeId, participantId) {
        if (!projectGuid) {
            projectGuid = "null";
        }
        if (!userId) {
            userId = -1;
        }
        if (!assigneeId) {
            assigneeId = -1;
        }
        if (!participantId) {
            participantId = -1;
        }
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/stepInstances/${projectGuid}/${userId}/${assigneeId}/${participantId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} stepId
     * @return {?}
     */
    getStepParameters(stepId) {
        /** @type {?} */
        let url = this.backendServiceUrl + "rest/project/stepInstances/parameters/" + stepId;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve(response);
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} actorId
     * @return {?}
     */
    getLatestStepAssignmentDate(actorId) {
        /** @type {?} */
        let url = this.backendServiceUrl + "rest/project/stepInstances/latestAssignment/" + actorId;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} stepInstanceId
     * @param {?} systemUserId
     * @return {?}
     */
    completeStep(stepInstanceId, systemUserId) {
        /** @type {?} */
        let url = this.backendServiceUrl;
        if (systemUserId)
            url += `rest/project/stepInstance/completeStep?stepInstanceId=${stepInstanceId}&systemUserId=${systemUserId}`;
        else
            url += `rest/project/stepInstance/completeStep2?stepInstanceId=${stepInstanceId}`;
        /** @type {?} */
        let headers = new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        return this.httpClient.post(url, JSON.stringify({})).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} stepInstanceId
     * @param {?} latitude
     * @param {?} longitude
     * @return {?}
     */
    updateStepLocation(stepInstanceId, latitude, longitude) {
        /** @type {?} */
        let url = this.backendServiceUrl;
        url += `rest/project/updateStepLocation?stepInstanceId=${stepInstanceId}&latitude=${latitude}&longitude=${longitude}`;
        /** @type {?} */
        let headers = new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        return this.httpClient.post(url, JSON.stringify({})).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} stepInstanceId
     * @return {?}
     */
    getQuestionnaire(stepInstanceId) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/stepInstance/questionnaire/${stepInstanceId}`;
        /** @type {?} */
        let headers = new HttpHeaders({
            'Accept': 'application/json'
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} stepInstanceId
     * @param {?} responses
     * @param {?} submit
     * @param {?} systemUserId
     * @return {?}
     */
    updateQuestionnaire(stepInstanceId, responses, submit, systemUserId) {
        /** @type {?} */
        let url = this.backendServiceUrl + 'rest/project/stepInstance/questionnaire/responses';
        /** @type {?} */
        let headers = new HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        /** @type {?} */
        let questionnaireResponse = {
            stepInstanceId: stepInstanceId,
            responses: responses,
            submit: submit,
            systemUserId: systemUserId
        };
        return this.httpClient.post(url, JSON.stringify(questionnaireResponse), { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} stepInstanceId
     * @return {?}
     */
    getMessageStepHtml(stepInstanceId) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/stepInstance/message/${stepInstanceId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} businessProcessInstanceId
     * @param {?} idNr
     * @return {?}
     */
    verifyActorIdNr(businessProcessInstanceId, idNr) {
        /** @type {?} */
        let url = this.backendServiceUrl + 'rest/project/verifyActorIdNr';
        /** @type {?} */
        let headers = new HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        /** @type {?} */
        let idNrLookUp = {
            businessProcessInstanceId: businessProcessInstanceId,
            idNr: idNr
        };
        return this.httpClient.post(url, idNrLookUp, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} businessProcessInstanceId
     * @param {?} key
     * @return {?}
     */
    getProcessVariableValue(businessProcessInstanceId, key) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/businessProcessInstance/${businessProcessInstanceId}/${key}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} businessProcessInstanceId
     * @param {?} key
     * @param {?} value
     * @return {?}
     */
    setProcessVariableValue(businessProcessInstanceId, key, value) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/setProcessVariableValue/${businessProcessInstanceId}/${key}/${value}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} actor
     * @return {?}
     */
    updateActor(actor) {
        /** @type {?} */
        let url = this.backendServiceUrl + 'rest/project/updateActor';
        /** @type {?} */
        let headers = new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        return this.httpClient.post(url, actor).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} participantId
     * @param {?} providerId
     * @param {?} timeSlotType
     * @return {?}
     */
    getTimeSlots(participantId, providerId, timeSlotType) {
        if (!participantId)
            participantId = -1;
        if (!providerId)
            providerId = -1;
        if (!timeSlotType)
            timeSlotType = null;
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/timeSlots/${participantId}/${providerId}/${timeSlotType}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} timeSlots
     * @return {?}
     */
    updateTimeSlots(timeSlots) {
        /** @type {?} */
        let url = this.backendServiceUrl + 'rest/project/updateTimeSlots';
        /** @type {?} */
        let headers = new HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        /** @type {?} */
        let params = {
            timeSlots: timeSlots
        };
        return this.httpClient.post(url, params, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} username
     * @param {?} password
     * @param {?} actorId
     * @return {?}
     */
    createSystemUser(username, password, actorId) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/login/createUser/${username}/${password}/${actorId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} stepInstanceId
     * @param {?} imageData
     * @return {?}
     */
    updateFacialAuthenticationReference(stepInstanceId, imageData) {
        /** @type {?} */
        let url = this.backendServiceUrl + 'rest/project/facialAuth/reference';
        /** @type {?} */
        let headers = new HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        /** @type {?} */
        let params = {
            stepInstanceId: stepInstanceId,
            imageData: imageData
        };
        return this.httpClient.post(url, params, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} stepInstanceId
     * @param {?} imageData
     * @return {?}
     */
    facialAuthentication(stepInstanceId, imageData) {
        /** @type {?} */
        let url = this.backendServiceUrl + 'rest/project/facialAuth/authenticate';
        /** @type {?} */
        let headers = new HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });
        /** @type {?} */
        let params = {
            stepInstanceId: stepInstanceId,
            imageData: imageData
        };
        return this.httpClient.post(url, params, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} projectGuid
     * @param {?} actorId
     * @return {?}
     */
    getProcessHistory(projectGuid, actorId) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/project/stepInstance/progress/${projectGuid}/${actorId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} stepIds
     * @return {?}
     */
    getActiveStepInstances(stepIds) {
        /** @type {?} */
        let url = this.backendServiceUrl + 'rest/project/stepInstance/active';
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        for (let i = 0; i < stepIds.length; i++) {
            if (i === 0) {
                url += "?stepIds=" + stepIds[i];
            }
            else {
                url += "&stepIds=" + stepIds[i];
            }
        }
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} stepId
     * @param {?} externalId
     * @return {?}
     */
    isStepInstanceActive(stepId, externalId) {
        /** @type {?} */
        let url = this.backendServiceUrl + 'rest/project/stepInstance/active/' + stepId + "/" + externalId;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve(response);
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
}
ProjectService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
ProjectService.ctorParameters = () => [
    { type: HttpClient },
    { type: AcumenConfiguration }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ActorService {
    // private backendServiceUrl = 'http://www.healthacumen.co.za/insight/';
    // private backendServiceUrl = 'https://frank.rmsui.co.za/insight/';
    // private backendServiceUrl = 'http://localhost:8888/';
    /**
     * @param {?} httpClient
     * @param {?} acumenConfiguration
     */
    constructor(httpClient, acumenConfiguration) {
        this.httpClient = httpClient;
        this.acumenConfiguration = acumenConfiguration;
        if (acumenConfiguration.backendServiceUrl) {
            this.backendServiceUrl = acumenConfiguration.backendServiceUrl;
        }
        else {
            this.backendServiceUrl = "https://frank.rmsui.co.za/insight/";
        }
    }
    /**
     * @param {?} username
     * @return {?}
     */
    getSystemUser(username) {
        /** @type {?} */
        let url = this.backendServiceUrl + "rest/actor/systemUser/" + username;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} externalActorId
     * @return {?}
     */
    getActorFromExternalId(externalActorId) {
        /** @type {?} */
        let url = this.backendServiceUrl + "rest/actor/externalId/" + externalActorId;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} externalActorId
     * @param {?} projectId
     * @return {?}
     */
    getProjectActorFromExternalId(externalActorId, projectId) {
        /** @type {?} */
        let url = this.backendServiceUrl + "rest/actor/projectParticipant/" + externalActorId + "/" + projectId;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} actorId
     * @param {?} projectId
     * @return {?}
     */
    getProjectActor(actorId, projectId) {
        /** @type {?} */
        let url = this.backendServiceUrl + "rest/project/projectActor/" + projectId + "/" + actorId;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} actorId
     * @return {?}
     */
    getActorFieldValues(actorId) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/actor/actorType/actorFields/actorFieldValues/${actorId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} actorId
     * @param {?} actorFieldValues
     * @return {?}
     */
    updateActorFieldValues(actorId, actorFieldValues) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/actor/actorType/actorFields/actorFieldValues/update`;
        /** @type {?} */
        let headers = new HttpHeaders({
            'Accept': 'application/json'
        });
        /** @type {?} */
        let parameters = {
            actorId: actorId,
            actorFieldValues: actorFieldValues
        };
        return this.httpClient.post(url, parameters, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
}
ActorService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
ActorService.ctorParameters = () => [
    { type: HttpClient },
    { type: AcumenConfiguration }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class InTrayComponent {
    /**
     * @param {?} projectService
     * @param {?} actorService
     * @param {?} acumenConfiguration
     */
    constructor(projectService, actorService, acumenConfiguration) {
        this.projectService = projectService;
        this.actorService = actorService;
        this.acumenConfiguration = acumenConfiguration;
        this.onStepInstanceSelected = new EventEmitter();
        this.stepInstances = [];
        this.loading = false;
        this.selectedStepInstance = null;
        if (acumenConfiguration.backendServiceUrl) {
            this.serverUrl = acumenConfiguration.backendServiceUrl;
        }
        else {
            this.serverUrl = "http://www.healthacumen.co.za/insight/";
        }
    }
    /**
     * @param {?} d
     * @return {?}
     */
    set latestStepCompletionDate(d) {
        // alert("Dates: " + d + " -> " + this._latestStepCompletionDate);
        if (new Date(d).getTime() > new Date(this._latestStepCompletionDate).getTime()) {
            // alert("refresh");
            this.refreshInTray();
        }
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.username) {
            this.actorService.getSystemUser(this.username).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                this.systemUser = result;
                if (result && result.actor) {
                    // this.userId = result.id;
                    this.userId = result.actor.id;
                    this.assigneeId = result.actor.id;
                    this.refreshInTray();
                    this.startPolling();
                }
            }));
        }
        else {
            this.refreshInTray();
            this.startPolling();
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
    /**
     * @private
     * @return {?}
     */
    startPolling() {
        /** @type {?} */
        let timer = Observable.timer(0, 10000);
        this.subscription = timer.subscribe((/**
         * @param {?} t
         * @return {?}
         */
        t => {
            // this.projectService.getLatestStepAssignmentDate(this.assigneeId).then(result => {
            //   if (result) {
            //     if (this._latestStepCompletionDate) {
            //       let t = new Date(this._latestStepCompletionDate).getTime() - new Date(result).getTime();
            //       if (t < 0) {
            this.refreshInTray();
            //       }
            //     }
            //   }
            // });
        }));
    }
    /**
     * @return {?}
     */
    refreshInTray() {
        this._latestStepCompletionDate = new Date();
        this.loading = true;
        this.selectedStepInstance = null;
        this.projectService.getStepInstances2(this.projectGuid, this.userId, this.assigneeId, this.participantId).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            /** @type {?} */
            let stepInstances = [];
            for (let s of result) {
                if (s.stepInstanceStatus === 'WAITING' && s.step.stepType !== "CANCEL_WIZARD") {
                    stepInstances.push(s);
                }
            }
            stepInstances.sort((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            (a, b) => {
                return new Date(b.activationDate).getTime() - new Date(a.activationDate).getTime();
            }));
            this.stepInstances = stepInstances;
            this.loading = false;
        }));
    }
    /**
     * @param {?} stepInstance
     * @return {?}
     */
    stepInstanceSelected(stepInstance) {
        this.selectedStepInstance = stepInstance;
        if (this.selectedStepInstance) {
            this.selectedStepInstance.systemUser = this.systemUser;
        }
        this.onStepInstanceSelected.emit(stepInstance);
    }
    /**
     * @param {?} stepInstance
     * @return {?}
     */
    blinkRow(stepInstance) {
        return new Date().getTime() - new Date(stepInstance.activationDate).getTime() < 1000 * 60 * 3;
    }
}
InTrayComponent.decorators = [
    { type: Component, args: [{
                selector: 'acn-in-tray',
                template: "<div class=\"in-tray-component\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n\n  <div class=\"step-list\">\n    <div *ngFor=\"let stepInstance of stepInstances; odd as isOdd\">\n      <div class=\"in-tray-row\" [ngClass]=\"{ 'oddRow': isOdd, 'selectedRow': selectedStepInstance && selectedStepInstance.id === stepInstance.id }\"\n          (click)=\"stepInstanceSelected(stepInstance)\">\n        <div class=\"project-image\">\n          <img src=\"{{ serverUrl }}spring/projectIcon/image.png?id={{ stepInstance.businessProcessInstance.businessProcess.project.id }}\">\n        </div>\n        <div class=\"description\" [ngClass]=\"{ 'blink': blinkRow(stepInstance) }\">\n          <div class=\"project\">\n            {{ stepInstance.businessProcessInstance.actor.firstname }} {{ stepInstance.businessProcessInstance.actor.surname }},\n            Id: {{ stepInstance.businessProcessInstance.actor.idNr }}\n          </div>\n          <div class=\"step-details\">\n            <div class=\"step\">\n              {{ stepInstance.businessProcessInstance.businessProcess.project.projectName }}: {{ stepInstance.step.stepType }}\n            </div>\n            <div class=\"activation-date\">\n              ({{ stepInstance.activationDate | date: 'yyyy-MM-dd HH:mm:ss' }})\n            </div>\n          </div>\n        </div>\n        <div class=\"cursor-image\">\n          <img src=\"data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAUVJREFUeJzt2EtOw0AURNELbCw7o9hZdgaTtBigBLv9+vM+JfUscnyPZw220+OknIDvx9HSN1kw8RufDkH8jU+DIJ7Hh0cQ/8eHRRDH48MhiPPxYRBEf7x7BHE93i2CsIt3hyDs47dBeF/8/58sRvg48Js78AbcBr3D7fH8+6Dnv9wRAAiMcBQAgiKcAYCACGcBIBhCDwAEQugFgCAIVwAgAMJVAHCOYAEAjhGsAMApgiUAOESwBgBnCCMAwBHCKABwgjASABwgjAaAzRFmAMDGCLMAYFOE1XeCqSbG3S5vccP8aqLiK77ijY9mhfRMVHzFV7zx0ayQnomKr/iKNz6aFdIzUfEVX/      HGR7NCeiYqPmb86jvBLzb/+m0i2JfvmUgc3yYSx7eJxPFtInF8m0gc3yYSx7eJxPFtInF8m0gc3yacxf8AAP323bWbSe8AAAAASUVORK5CYII=\">\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n",
                styles: [".in-tray-component{width:100%;height:100%;position:relative}.in-tray-component .step-list{width:100%;height:100%;overflow-y:scroll}.in-tray-component .step-list .in-tray-row{display:table;width:100%;padding:5px;cursor:pointer}.in-tray-component .step-list .in-tray-row .project-image{display:table-cell;width:32px;text-align:center;vertical-align:middle}.in-tray-component .step-list .in-tray-row .project-image img{width:24px;height:24px}.in-tray-component .step-list .in-tray-row .description{display:table-cell;width:calc(100% - 64px);padding-left:10px;vertical-align:middle}.in-tray-component .step-list .in-tray-row .description .project{font-size:14px}.in-tray-component .step-list .in-tray-row .description .step-details{line-height:15px}.in-tray-component .step-list .in-tray-row .description .step-details .step{font-size:15px}.in-tray-component .step-list .in-tray-row .description .step-details .activation-date{font-size:10px}.in-tray-component .step-list .in-tray-row .blink{animation:1s steps(5,start) infinite blink-animation;-webkit-animation:1s steps(5,start) infinite blink-animation}@keyframes blink-animation{to{color:red}}@-webkit-keyframes blink-animation{to{color:red}}.in-tray-component .step-list .in-tray-row .cursor-image{display:table-cell;width:32px;text-align:center;vertical-align:middle}.in-tray-component .step-list .in-tray-row .cursor-image img{width:16px;height:16px}.in-tray-component .step-list .oddRow{background-color:#f2f2f2}.in-tray-component .step-list .selectedRow{background-color:#abb9d3}"]
            }] }
];
/** @nocollapse */
InTrayComponent.ctorParameters = () => [
    { type: ProjectService },
    { type: ActorService },
    { type: AcumenConfiguration }
];
InTrayComponent.propDecorators = {
    projectGuid: [{ type: Input }],
    userId: [{ type: Input }],
    assigneeId: [{ type: Input }],
    participantId: [{ type: Input }],
    username: [{ type: Input }],
    onStepInstanceSelected: [{ type: Output }],
    latestStepCompletionDate: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class InTrayAlertComponent {
    /**
     * @param {?} projectService
     * @param {?} actorService
     * @param {?} acumenConfiguration
     */
    constructor(projectService, actorService, acumenConfiguration) {
        this.projectService = projectService;
        this.actorService = actorService;
        this.acumenConfiguration = acumenConfiguration;
        this.onSelected = new EventEmitter();
        this.blink = false;
        if (acumenConfiguration.backendServiceUrl) {
            this.serviceUrl = acumenConfiguration.backendServiceUrl;
        }
        else {
            this.serviceUrl = "http://www.healthacumen.co.za/insight/";
        }
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.username) {
            this.actorService.getSystemUser(this.username).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                if (result && result.actor) {
                    this.assigneeId = result.actor.id;
                    this.startPolling();
                }
            }));
        }
        else {
            this.startPolling();
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
    /**
     * @return {?}
     */
    startPolling() {
        /** @type {?} */
        let interval = this.checkInterval;
        if (!interval || interval < 5000) {
            interval = 5000;
        }
        /** @type {?} */
        let timer = Observable.timer(0, interval);
        this.subscription = timer.subscribe((/**
         * @param {?} t
         * @return {?}
         */
        t => {
            this.projectService.getLatestStepAssignmentDate(this.assigneeId).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                // console.log(result);
                if (result) {
                    /** @type {?} */
                    let t = new Date().getTime() - new Date(result).getTime();
                    this.blink = t < this.alertTime;
                }
                else {
                    this.blink = false;
                }
            }));
        }));
    }
    /**
     * @return {?}
     */
    selected() {
        this.onSelected.emit();
    }
}
InTrayAlertComponent.decorators = [
    { type: Component, args: [{
                selector: 'acn-in-tray-alert',
                template: "<div class=\"in-tray-alert-component\" (click)=\"selected()\">\n  <div class=\"in-tray-image\">\n    <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"8\" height=\"8\" viewBox=\"0 0 8 8\" fill=\"#fff\">\n      <path d=\"M.19 0c-.11 0-.19.08-.19.19v7.63c0 .11.08.19.19.19h7.63c.11 0 .19-.08.19-.19v-7.63c0-.11-.08-.19-.19-.19h-7.63zm.81 2h6v3h-1l-1 1h-2l-1-1h-1v-3z\"/>\n    </svg>\n  </div>\n  <div *ngIf=\"blink\" class=\"warning-image\">\n    <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"8\" height=\"8\" viewBox=\"0 0 8 8\" fill=\"red\">\n      <path d=\"M3.09 0c-.06 0-.1.04-.13.09l-2.94 6.81c-.02.05-.03.13-.03.19v.81c0 .05.04.09.09.09h6.81c.05 0 .09-.04.09-.09v-.81c0-.05-.01-.14-.03-.19l-2.94-6.81c-.02-.05-.07-.09-.13-.09h-.81zm-.09 3h1v2h-1v-2zm0 3h1v1h-1v-1z\" />\n    </svg>\n  </div>\n</div>\n",
                styles: [".in-tray-alert-component{width:100%;height:100%;position:relative;cursor:pointer}.in-tray-alert-component .in-tray-image,.in-tray-alert-component .in-tray-image svg{width:100%;height:100%}.in-tray-alert-component .warning-image{position:absolute;width:60%;height:60%;left:30%;top:0;animation:1s steps(5,start) infinite blink-animation;-webkit-animation:1s steps(5,start) infinite blink-animation}.in-tray-alert-component .warning-image svg{width:100%;height:100%}@keyframes blink-animation{to{opacity:0}}@-webkit-keyframes blink-animation{to{opacity:0}}"]
            }] }
];
/** @nocollapse */
InTrayAlertComponent.ctorParameters = () => [
    { type: ProjectService },
    { type: ActorService },
    { type: AcumenConfiguration }
];
InTrayAlertComponent.propDecorators = {
    assigneeId: [{ type: Input }],
    username: [{ type: Input }],
    checkInterval: [{ type: Input }],
    alertTime: [{ type: Input }],
    onSelected: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AnalysisService {
    // private backendServiceUrl = 'http://www.healthacumen.co.za/insight/';
    // private backendServiceUrl = 'https://frank.rmsui.co.za/insight/';
    // private backendServiceUrl = 'http://localhost:8888/';
    /**
     * @param {?} httpClient
     * @param {?} acumenConfiguration
     */
    constructor(httpClient, acumenConfiguration) {
        this.httpClient = httpClient;
        this.acumenConfiguration = acumenConfiguration;
        if (acumenConfiguration.backendServiceUrl) {
            this.backendServiceUrl = acumenConfiguration.backendServiceUrl;
        }
        else {
            this.backendServiceUrl = "https://frank.rmsui.co.za/insight/";
        }
    }
    /**
     * @param {?} projectGuid
     * @return {?}
     */
    getProjectCompletionCount(projectGuid) {
        /** @type {?} */
        let url = this.backendServiceUrl + `countserv?pr=${projectGuid}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @return {?}
     */
    getWellnessData() {
        /** @type {?} */
        let url = this.backendServiceUrl + `welldashdplot?actor=359817&project=123`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
}
AnalysisService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
AnalysisService.ctorParameters = () => [
    { type: HttpClient },
    { type: AcumenConfiguration }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class WizardComponent {
    /**
     * @param {?} actorService
     * @param {?} projectService
     * @param {?} analysisService
     */
    constructor(actorService, projectService, analysisService) {
        this.actorService = actorService;
        this.projectService = projectService;
        this.analysisService = analysisService;
        this.onWizardCancelled = new EventEmitter();
        this.onWizardCompleted = new EventEmitter();
        this.onOpenCameraListener = new EventEmitter();
        this.onQuestionAnswered = new EventEmitter();
        this.onReadCode = new EventEmitter();
        this.processError = false;
        this.processExecutionError = false;
        this.cancelled = false;
        this.showCompletedMessage = false;
        this.showParticipantSearch = false;
        this.loading = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.loading = true;
        this.showWaitIndicator = true;
        this.processError = false;
        this.processExecutionError = false;
        this.actor = null;
        this.businessProcessInstance = null;
        this.stepInstance = null;
        this.showParticipantSearch = false;
        if (this.project) {
            this.initialise();
        }
        else if (this.projectGuid) {
            this.projectService.getProject(this.projectGuid).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                this.project = result;
                this.initialise();
            }));
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @private
     * @return {?}
     */
    initialise() {
        this.actorService.getSystemUser(this.username).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            if (result) {
                this.userId = result.id;
                if (this.externalActorIdNr) {
                    this.actorService.getProjectActorFromExternalId(this.externalActorIdNr, this.project.id).then((/**
                     * @param {?} result2
                     * @return {?}
                     */
                    result2 => {
                        this.actor = result2;
                        if (this.project.wizardProject) {
                            this.startWizard(this.project.guid, null);
                        }
                        else {
                            // if (this.project.actorProjectFunction === "PARTICIPANT") {
                            if (this.actor) {
                                this.startWizard(this.project.guid, this.actor.id);
                            }
                            else {
                                this.showParticipantSearch = true;
                                this.loading = false;
                            }
                        }
                    }));
                }
                else {
                    if (this.project.wizardProject) {
                        this.startWizard(this.project.guid, null);
                    }
                    else {
                        if (this.project.actorProjectFunction === "PARTICIPANT" && result.actor) {
                            this.actor = result.actor;
                            this.startWizard(this.project.guid, this.actor.id);
                        }
                        else {
                            this.showParticipantSearch = true;
                            this.loading = false;
                        }
                    }
                }
            }
        }));
    }
    /**
     * @param {?} participant
     * @return {?}
     */
    onParticipantSelected(participant) {
        this.actor = participant.actor;
        this.showParticipantSearch = false;
        this.loading = true;
        this.startWizard(this.project.guid, this.actor.id);
    }
    /**
     * @private
     * @param {?} projectGuid
     * @param {?} actorId
     * @return {?}
     */
    startWizard(projectGuid, actorId) {
        this.processExecutionError = false;
        this.projectService.startWizardProject(projectGuid, actorId).then((/**
         * @param {?} businessProcessInstance
         * @return {?}
         */
        businessProcessInstance => {
            this.businessProcessInstance = businessProcessInstance;
            this.projectName = businessProcessInstance.businessProcess.project.projectName;
            this.getNextStepDelayed();
            this.loading = false;
        }), (/**
         * @param {?} error
         * @return {?}
         */
        error => {
            this.processError = true;
            this.loading = false;
        }));
    }
    /**
     * @param {?} event
     * @return {?}
     */
    stepCompletedListener(event) {
        if (this.stepInstance && this.latitude && this.longitude) {
            this.projectService.updateStepLocation(this.stepInstance.id, this.latitude, this.longitude).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                // hoef nie iets te doen nie
            }));
        }
        this.getNextStep();
    }
    /**
     * @private
     * @return {?}
     */
    getNextStepDelayed() {
        if (!this.cancelled) {
            /** @type {?} */
            let timer = Observable.timer(1000);
            timer.subscribe((/**
             * @param {?} t
             * @return {?}
             */
            t => {
                this.getNextStep();
            }));
        }
    }
    /**
     * @private
     * @return {?}
     */
    getNextStep() {
        console.log("------------------------------------------------");
        this.projectService.getBusinessProcessState(this.businessProcessInstance.id).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            this.businessProcessInstance = result.businessProcessInstance;
            if (result.stepInstance == null) {
                console.log("stepInstance == null ");
                this.stepInstance = result.stepInstance;
                if (result.businessProcessInstance.processStatus === 'COMPLETED') {
                    console.log("COMPLETED");
                    this.processCompleted();
                    // this.analysisService.getProjectCompletionCount(this.project.guid).then(count => {
                    //     this.completionCount = count;
                    // });
                }
                else if (result.businessProcessInstance.processStatus === 'FAILED') {
                    this.processExecutionError = true;
                }
                else {
                    console.log("getNextStepDelayed 1");
                    this.getNextStepDelayed();
                }
            }
            else if (this.stepInstance && this.stepInstance.id === result.stepInstance.id) {
                this.stepInstance = result.stepInstance;
                if (this.stepInstance.step.stepType === 'CANCEL_WIZARD' ||
                    (this.stepInstance.step.stepType === 'NONE_END_EVENT' && !this.stepInstance.step.parentStep)) {
                    console.log("this.stepInstance.id === result.stepInstance.id 1");
                    this.processCompleted();
                }
                else {
                    console.log("this.stepInstance.id === result.stepInstance.id 2");
                    // this.getNextStepDelayed();
                    this.showStepPanel(result.stepInstance);
                }
            }
            else {
                console.log("this.stepInstance.id === result.stepInstance.id 3");
                this.stepInstance = result.stepInstance;
                if (this.stepInstance.step.stepType === 'CANCEL_WIZARD' ||
                    (this.stepInstance.step.stepType === 'NONE_END_EVENT' && !this.stepInstance.step.parentStep)) {
                    this.processCompleted();
                }
                else {
                    if (this.stepInstance.actor) {
                        console.log("this.stepInstance.actor");
                        if (this.stepInstance.actor.id === this.businessProcessInstance.actor.id) { // || stepInstance.getActor().getId().equals(evaluatorId) {
                            console.log("showStepPanel: " + this.stepInstance.step.stepType);
                            this.showStepPanel(result.stepInstance);
                        }
                        else {
                            console.log("showInProgressPanel 1");
                            this.showInProgressPanel();
                        }
                    }
                    else {
                        console.log("showInProgressPanel 2");
                        this.showInProgressPanel();
                    }
                }
            }
        }));
    }
    /**
     * @private
     * @param {?} stepInstance
     * @return {?}
     */
    showStepPanel(stepInstance) {
        if (stepInstance.step.stepType === 'COMPLETE_QUESTIONNAIRE' || stepInstance.step.stepType === 'MESSAGE' ||
            stepInstance.step.stepType === 'ID_NR_LOOKUP' || stepInstance.step.stepType === 'UPDATE_PERSONAL_DETAILS' ||
            stepInstance.step.stepType === 'REGISTER' || stepInstance.step.stepType === 'CREATE_SYSTEM_USER' ||
            stepInstance.step.stepType === 'SELECT_PROVIDER' || stepInstance.step.stepType === 'SELECT_PROVIDER_2' ||
            stepInstance.step.stepType === 'FACIAL_AUTHENTICATION_REF' || stepInstance.step.stepType === 'FACIAL_AUTHENTICATION' ||
            stepInstance.step.stepType === 'WAIT' || stepInstance.step.stepType === 'REGISTER' ||
            stepInstance.step.stepType === 'CREATE_SYSTEM_USER') {
            this.showWaitIndicator = false;
        }
        else {
            this.getNextStepDelayed();
        }
    }
    /**
     * @private
     * @return {?}
     */
    processCompleted() {
        this.showCompletedMessage = true;
        this.stepInstance = null;
        this.onWizardCompleted.emit();
    }
    /**
     * @private
     * @return {?}
     */
    showInProgressPanel() {
        this.getNextStepDelayed();
    }
    /**
     * @param {?} event
     * @return {?}
     */
    cancelWizard(event) {
        this.stepInstance = null;
        this.cancelled = true;
        this.onWizardCancelled.emit();
    }
    /**
     * @param {?} imageContainer
     * @return {?}
     */
    openCamera(imageContainer) {
        this.onOpenCameraListener.emit(imageContainer);
    }
    /**
     * @param {?} questionLine
     * @return {?}
     */
    questionAnswered(questionLine) {
        this.onQuestionAnswered.emit(questionLine);
    }
    /**
     * @param {?} codeReadListener
     * @return {?}
     */
    readCode(codeReadListener) {
        this.onReadCode.emit(codeReadListener);
    }
}
WizardComponent.decorators = [
    { type: Component, args: [{
                selector: 'acn-wizard',
                template: "<div class=\"wizard-component\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <participant-search [show]=\"showParticipantSearch\" [project]=\"project\" (onParticipantSelected)=\"onParticipantSelected($event)\"></participant-search>\n\n  <!-- <div class=\"step-content\">\n    username: {{ username }}, externalActorIdNr: {{ externalActorIdNr }}\n  </div> -->\n\n  <div *ngIf=\"stepInstance && !showCompletedMessage && stepInstance && stepInstance.step.stepType === 'MESSAGE'\" class=\"step-content\">\n    <message [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelWizard($event)\"></message>\n  </div>\n\n  <div *ngIf=\"stepInstance && !showCompletedMessage && stepInstance && stepInstance.step.stepType === 'COMPLETE_QUESTIONNAIRE'\" class=\"step-content\">\n    <questionnaire [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelWizard($event)\" (onTakePicture)=\"openCamera($event)\" (onQuestionAnswered)=\"questionAnswered($event)\"></questionnaire>\n  </div>\n\n  <div *ngIf=\"stepInstance && !showCompletedMessage && stepInstance.step.stepType === 'WAIT'\" class=\"step-content\">\n    <wait [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelWizard($event)\"></wait>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType === 'FACIAL_AUTHENTICATION_REF'\" class=\"step-content\">\n    <facial-authentication-ref [stepInstance]=\"stepInstance\" [userId]=\"userId\" (onOpenCameraListener)=\"openCamera($event)\"\n        (stepCompletedListener)=\"stepCompletedListener($event)\" (cancelListener)=\"cancelWizard($event)\"></facial-authentication-ref>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType === 'FACIAL_AUTHENTICATION'\" class=\"step-content\">\n    <facial-authentication [stepInstance]=\"stepInstance\" [userId]=\"userId\" (onOpenCameraListener)=\"openCamera($event)\"\n        (stepCompletedListener)=\"stepCompletedListener($event)\" (cancelListener)=\"cancelWizard($event)\"></facial-authentication>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType === 'CODE_READER'\" class=\"step-content\">\n    <code-reader [stepInstance]=\"stepInstance\" [userId]=\"userId\" (onReadCode)=\"readCode($event)\"\n        (stepCompletedListener)=\"stepCompletedListener($event)\" (cancelListener)=\"cancelWizard($event)\"></code-reader>\n  </div>\n\n  <div *ngIf=\"stepInstance && !showCompletedMessage && stepInstance.step.stepType === 'ID_NR_LOOKUP'\" class=\"step-content\">\n      <id-nr-lookup [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n          (cancelListener)=\"cancelWizard($event)\"></id-nr-lookup>\n  </div>\n\n  <div *ngIf=\"stepInstance && !showCompletedMessage && stepInstance && stepInstance.step.stepType === 'UPDATE_PERSONAL_DETAILS'\" class=\"step-content\">\n    <update-details [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelWizard($event)\"></update-details>\n  </div>\n\n  <div *ngIf=\"stepInstance && !showCompletedMessage && stepInstance && stepInstance.step.stepType === 'REGISTER'\" class=\"step-content\">\n    <register [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelWizard($event)\"></register>\n  </div>\n\n  <div *ngIf=\"stepInstance && !showCompletedMessage && stepInstance && stepInstance.step.stepType === 'CREATE_SYSTEM_USER'\" class=\"step-content\">\n    <create-system-user [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelWizard($event)\"></create-system-user>\n  </div>\n\n  <div *ngIf=\"stepInstance && !showCompletedMessage && stepInstance && stepInstance.step.stepType === 'SELECT_PROVIDER_2'\" class=\"step-content\">\n    <select-role2 [makeBooking]=\"true\" [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelWizard($event)\"></select-role2>\n  </div>\n\n  <div *ngIf=\"processError\" class=\"step-info\">\n    <div class=\"info-message\">\n        Error starting process...\n    </div>\n    <div class=\"button-section\">\n      <div class=\"button\" (click)=\"cancelWizard(null)\">\n        <div class=\"button-text\">\n          Cancel\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!processError && !processExecutionError && !showParticipantSearch && !showCompletedMessage && !cancelled && (!stepInstance ||\n          (stepInstance.step.stepType !== 'COMPLETE_QUESTIONNAIRE' && stepInstance.step.stepType !== 'MESSAGE' &&\n           stepInstance.step.stepType !== 'ID_NR_LOOKUP' && stepInstance.step.stepType !== 'UPDATE_PERSONAL_DETAILS' &&\n           stepInstance.step.stepType !== 'SELECT_PROVIDER_2' && stepInstance.step.stepType !== 'WAIT' &&\n           stepInstance.step.stepType !== 'FACIAL_AUTHENTICATION_REF' && stepInstance.step.stepType !== 'FACIAL_AUTHENTICATION' &&\n           stepInstance.step.stepType !== 'CODE_READER' &&\n           stepInstance.step.stepType !== 'REGISTER' && stepInstance.step.stepType !== 'CREATE_SYSTEM_USER'))\" class=\"step-info\">\n    <div class=\"info-message\">\n        Processing, please wait...\n    </div>\n    <div class=\"button-section\">\n      <div class=\"button\" (click)=\"cancelWizard(null)\">\n        <div class=\"button-text\">\n          Cancel\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div *ngIf=\"processExecutionError\" class=\"step-info\">\n    <div class=\"info-message\">\n        Process execution error\n    </div>\n    <div class=\"button-section\">\n      <div class=\"button\" (click)=\"cancelWizard(null)\">\n        <div class=\"button-text\">\n          Cancel\n        </div>\n      </div>\n    </div>\n  </div>\n\n\n  <div *ngIf=\"showCompletedMessage\" class=\"step-info\">\n    <div class=\"info-message\">\n        Process completed\n    </div>\n    <div class=\"button-section\">\n      <div class=\"button\" (click)=\"cancelWizard(null)\">\n        <div class=\"button-text\">\n          Continue\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n",
                styles: [".wizard-component{width:100%;height:100%;max-height:100%;position:relative}.wizard-component .step-content{width:100%;height:100%;position:relative}.wizard-component .step-info{height:100%;font-size:14px;position:relative}.wizard-component .step-info .info-message{position:absolute;top:0;left:0;bottom:50px;box-sizing:border-box;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px}.wizard-component .step-info .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.wizard-component .step-info .button-section .button{width:120px;background-color:#2b4054;height:36px;border-radius:5px;float:right;margin-top:8px;margin-right:8px;display:table;cursor:pointer}.wizard-component .step-info .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px;color:#fff}"]
            }] }
];
/** @nocollapse */
WizardComponent.ctorParameters = () => [
    { type: ActorService },
    { type: ProjectService },
    { type: AnalysisService }
];
WizardComponent.propDecorators = {
    project: [{ type: Input }],
    projectGuid: [{ type: Input }],
    username: [{ type: Input }],
    externalActorIdNr: [{ type: Input }],
    latitude: [{ type: Input }],
    longitude: [{ type: Input }],
    onWizardCancelled: [{ type: Output }],
    onWizardCompleted: [{ type: Output }],
    onOpenCameraListener: [{ type: Output }],
    onQuestionAnswered: [{ type: Output }],
    onReadCode: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class UserProjectsComponent {
    /**
     * @param {?} actorService
     * @param {?} projectService
     * @param {?} acumenConfiguration
     */
    constructor(actorService, projectService, acumenConfiguration) {
        this.actorService = actorService;
        this.projectService = projectService;
        this.acumenConfiguration = acumenConfiguration;
        this.onProjectSelected = new EventEmitter();
        this.projects = [];
        this.loading = false;
        if (acumenConfiguration.backendServiceUrl) {
            this.serverUrl = acumenConfiguration.backendServiceUrl;
        }
        else {
            this.serverUrl = "http://www.healthacumen.co.za/insight/";
        }
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.loading = true;
        if (this.username) {
            this.actorService.getSystemUser(this.username).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                if (result) {
                    this.userId = result.id;
                    this.actorId = result.actor.id;
                }
                this.refreshProjects();
            })).catch((/**
             * @param {?} error
             * @return {?}
             */
            error => {
                this.loading = false;
            }));
        }
        else {
            this.refreshProjects();
        }
    }
    /**
     * @return {?}
     */
    refreshProjects() {
        // if (this.userId) {
        //   this.projectService.getUserProjects(this.userId).then(result => {
        //     this.projects = result;
        //     this.loading = false;
        //   }).catch(error => {
        //     this.loading = false;
        //   });
        // } else {
        //   this.loading = false;
        // }
        if (this.actorId) {
            this.projects = [];
            this.projectService.getProjects(this.actorId).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                /** @type {?} */
                let projectIds = {};
                for (let project of result) {
                    if (!projectIds[project.id]) {
                        this.projects.push(project);
                        projectIds[project.id] = project;
                    }
                }
                this.loading = false;
            })).catch((/**
             * @param {?} error
             * @return {?}
             */
            error => {
                this.loading = false;
            }));
        }
        else {
            this.loading = false;
        }
    }
    /**
     * @param {?} project
     * @return {?}
     */
    projectSelected(project) {
        this.onProjectSelected.emit(project);
    }
}
UserProjectsComponent.decorators = [
    { type: Component, args: [{
                selector: 'acn-user-projects',
                template: "<div class=\"user-projects-component\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n\n  <div *ngIf=\"projects\" class=\"project-list\">\n    <div *ngFor=\"let project of projects; odd as isOdd\" class=\"project-row\" [ngClass]=\"{ 'oddRow': isOdd }\" (click)=\"projectSelected(project)\">\n      <div class=\"project-image\">\n        <img src=\"{{ serverUrl }}spring/projectIcon/image.png?id={{ project.id }}\">\n      </div>\n      <div class=\"description\">\n        {{ project.projectName }}\n\n      </div>\n      <div class=\"cursor-image\">\n        <img src=\"data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAUVJREFUeJzt2EtOw0AURNELbCw7o9hZdgaTtBigBLv9+vM+JfUscnyPZw220+OknIDvx9HSN1kw8RufDkH8jU+DIJ7Hh0cQ/8eHRRDH48MhiPPxYRBEf7x7BHE93i2CsIt3hyDs47dBeF/8/58sRvg48Js78AbcBr3D7fH8+6Dnv9wRAAiMcBQAgiKcAYCACGcBIBhCDwAEQugFgCAIVwAgAMJVAHCOYAEAjhGsAMApgiUAOESwBgBnCCMAwBHCKABwgjASABwgjAaAzRFmAMDGCLMAYFOE1XeCqSbG3S5vccP8aqLiK77ijY9mhfRMVHzFV7zx0ayQnomKr/iKNz6aFdIzUfEVX/      HGR7NCeiYqPmb86jvBLzb/+m0i2JfvmUgc3yYSx7eJxPFtInF8m0gc3yYSx7eJxPFtInF8m0gc3yacxf8AAP323bWbSe8AAAAASUVORK5CYII=\">\n      </div>\n    </div>\n  </div>\n</div>\n",
                styles: [".user-projects-component{width:100%;height:100%;position:relative}.user-projects-component .project-list{width:100%;height:100%;overflow-y:scroll}.user-projects-component .project-list .project-row{display:table;width:100%;padding:5px;cursor:pointer}.user-projects-component .project-list .project-row .project-image{display:table-cell;width:32px;text-align:center;vertical-align:middle}.user-projects-component .project-list .project-row .project-image img{width:24px;height:24px}.user-projects-component .project-list .project-row .description{display:table-cell;width:calc(100% - 64px);padding-left:10px;padding-top:3px;font-size:14px;vertical-align:middle}.user-projects-component .project-list .project-row .cursor-image{display:table-cell;width:32px;text-align:center;vertical-align:middle}.user-projects-component .project-list .project-row .cursor-image img{width:16px;height:16px}.user-projects-component .project-list .oddRow{background-color:#f2f2f2}"]
            }] }
];
/** @nocollapse */
UserProjectsComponent.ctorParameters = () => [
    { type: ActorService },
    { type: ProjectService },
    { type: AcumenConfiguration }
];
UserProjectsComponent.propDecorators = {
    username: [{ type: Input }],
    actorId: [{ type: Input }],
    userId: [{ type: Input }],
    onProjectSelected: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class UserProjects2Component {
    /**
     * @param {?} actorService
     * @param {?} projectService
     * @param {?} acumenConfiguration
     */
    constructor(actorService, projectService, acumenConfiguration) {
        this.actorService = actorService;
        this.projectService = projectService;
        this.acumenConfiguration = acumenConfiguration;
        this.onProjectsReceived = new EventEmitter();
        if (acumenConfiguration.backendServiceUrl) {
            this.serverUrl = acumenConfiguration.backendServiceUrl;
        }
        else {
            this.serverUrl = "http://www.healthacumen.co.za/insight/";
        }
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.username) {
            this.actorService.getSystemUser(this.username).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                if (result) {
                    this.userId = result.id;
                    this.actorId = result.actor.id;
                }
                this.refreshProjects();
            })).catch((/**
             * @param {?} error
             * @return {?}
             */
            error => {
                console.log(error);
            }));
        }
        else {
            this.refreshProjects();
        }
    }
    /**
     * @return {?}
     */
    refreshProjects() {
        if (this.actorId) {
            /** @type {?} */
            let projects = [];
            this.projectService.getProjects(this.actorId).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                /** @type {?} */
                let projectIds = {};
                for (let project of result) {
                    if (!projectIds[project.id]) {
                        project.iconUrl = this.serverUrl + "spring/projectIcon/image.png?id=" + project.id;
                        projects.push(project);
                        projectIds[project.id] = project;
                    }
                }
                this.onProjectsReceived.emit(projects);
            })).catch((/**
             * @param {?} error
             * @return {?}
             */
            error => {
                console.error(error);
            }));
        }
        else {
            this.onProjectsReceived.emit([]);
        }
    }
}
UserProjects2Component.decorators = [
    { type: Component, args: [{
                selector: 'acn-user-projects2',
                template: "<div class=\"user-projects2-component\">\n  <ng-content></ng-content>\n</div>\n",
                styles: [".user-projects2-component{width:100%;height:100%;position:relative}"]
            }] }
];
/** @nocollapse */
UserProjects2Component.ctorParameters = () => [
    { type: ActorService },
    { type: ProjectService },
    { type: AcumenConfiguration }
];
UserProjects2Component.propDecorators = {
    username: [{ type: Input }],
    onProjectsReceived: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class StepInstanceComponent {
    /**
     * @param {?} actorService
     * @param {?} projectService
     */
    constructor(actorService, projectService) {
        this.actorService = actorService;
        this.projectService = projectService;
        this.onStepCompleted = new EventEmitter();
        this.onStepCancelled = new EventEmitter();
        // @Output() onTakePicture = new EventEmitter();
        this.onOpenCameraListener = new EventEmitter();
        this.onReadCode = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.username) {
            this.actorService.getSystemUser(this.username).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                if (result) {
                    this.userId = result.id;
                }
            }));
        }
        else if (!this.userId && this.stepInstance && this.stepInstance.systemUser) {
            this.userId = this.stepInstance.systemUser.id;
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    stepCompletedListener(event) {
        if (this.latitude && this.longitude) {
            this.projectService.updateStepLocation(this.stepInstance.id, this.latitude, this.longitude).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                // hoef nie iets te doen nie
            }));
        }
        this.onStepCompleted.emit();
        this.stepInstance = null;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    cancelListener(event) {
        this.onStepCancelled.emit();
        this.stepInstance = null;
    }
    /**
     * @param {?} imageContainer
     * @return {?}
     */
    openCamera(imageContainer) {
        this.onOpenCameraListener.emit(imageContainer);
    }
    /**
     * @param {?} codeReadListener
     * @return {?}
     */
    readCode(codeReadListener) {
        this.onReadCode.emit(codeReadListener);
    }
}
StepInstanceComponent.decorators = [
    { type: Component, args: [{
                selector: 'acn-step-instance',
                template: "<div class=\"step-instance-component\">\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType === 'MESSAGE'\" class=\"step-content\">\n    <message [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelListener($event)\"></message>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType === 'COMPLETE_QUESTIONNAIRE'\" class=\"step-content\">\n    <questionnaire [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelListener($event)\" (onTakePicture)=\"openCamera($event)\"></questionnaire>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType === 'WAIT'\" class=\"step-content\">\n    <wait [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelListener($event)\"></wait>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType === 'FACIAL_AUTHENTICATION_REF'\" class=\"step-content\">\n    <facial-authentication-ref [stepInstance]=\"stepInstance\" [userId]=\"userId\" (onOpenCameraListener)=\"openCamera($event)\"\n        (stepCompletedListener)=\"stepCompletedListener($event)\" (cancelListener)=\"cancelListener($event)\"></facial-authentication-ref>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType === 'FACIAL_AUTHENTICATION'\" class=\"step-content\">\n    <facial-authentication [stepInstance]=\"stepInstance\" [userId]=\"userId\" (onOpenCameraListener)=\"openCamera($event)\"\n        (stepCompletedListener)=\"stepCompletedListener($event)\" (cancelListener)=\"cancelListener($event)\"></facial-authentication>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType === 'CODE_READER'\" class=\"step-content\">\n    <code-reader [stepInstance]=\"stepInstance\" [userId]=\"userId\" (onReadCode)=\"readCode($event)\"\n        (stepCompletedListener)=\"stepCompletedListener($event)\" (cancelListener)=\"cancelListener($event)\"></code-reader>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType === 'ID_NR_LOOKUP'\" class=\"step-content\">\n      <id-nr-lookup [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n          (cancelListener)=\"cancelListener($event)\"></id-nr-lookup>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance && stepInstance.step.stepType === 'UPDATE_PERSONAL_DETAILS'\" class=\"step-content\">\n    <update-details [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelListener($event)\"></update-details>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance && stepInstance.step.stepType === 'REGISTER'\" class=\"step-content\">\n    <register [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelListener($event)\"></register>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance && stepInstance.step.stepType === 'CREATE_SYSTEM_USER'\" class=\"step-content\">\n    <create-system-user [stepInstance]=\"stepInstance\" [userId]=\"userId\" (stepCompletedListener)=\"stepCompletedListener($event)\"\n        (cancelListener)=\"cancelListener($event)\"></create-system-user>\n  </div>\n\n  <div *ngIf=\"stepInstance && stepInstance.step.stepType !== 'MESSAGE' && stepInstance.step.stepType !== 'COMPLETE_QUESTIONNAIRE' &&\n      stepInstance.step.stepType !== 'WAIT' && stepInstance.step.stepType !== 'FACIAL_AUTHENTICATION_REF' &&\n      stepInstance.step.stepType !== 'FACIAL_AUTHENTICATION' && stepInstance.step.stepType !== 'UPDATE_PERSONAL_DETAILS' &&\n      stepInstance.step.stepType !== 'ID_NR_LOOKUP' && stepInstance.step.stepType !== 'UPDATE_PERSONAL_DETAILS' &&\n      stepInstance.step.stepType !== 'REGISTER' && stepInstance.step.stepType !== 'CREATE_SYSTEM_USER'\" class=\"step-info\">\n    <div class=\"info-message\">\n        No step implementation available for step type {{ stepInstance.step.stepType }}.\n    </div>\n\n    <div class=\"button-section\">\n      <div class=\"button\" (click)=\"cancelListener(null)\">\n        <div class=\"button-text\">\n          Cancel\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n",
                styles: [".step-instance-component,.step-instance-component .step-content{width:100%;height:100%;position:relative}.step-instance-component .step-info{height:100%;font-size:14px;position:relative}.step-instance-component .step-info .info-message{position:absolute;top:0;left:0;bottom:50px;box-sizing:border-box;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px}.step-instance-component .step-info .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.step-instance-component .step-info .button-section .button{width:120px;background-color:#2b4054;height:36px;border-radius:5px;float:right;margin-top:8px;margin-right:8px;display:table;cursor:pointer}.step-instance-component .step-info .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px;color:#fff}"]
            }] }
];
/** @nocollapse */
StepInstanceComponent.ctorParameters = () => [
    { type: ActorService },
    { type: ProjectService }
];
StepInstanceComponent.propDecorators = {
    stepInstance: [{ type: Input }],
    userId: [{ type: Input }],
    username: [{ type: Input }],
    latitude: [{ type: Input }],
    longitude: [{ type: Input }],
    onStepCompleted: [{ type: Output }],
    onStepCancelled: [{ type: Output }],
    onOpenCameraListener: [{ type: Output }],
    onReadCode: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class StepInstanceListComponent {
    /**
     * @param {?} actorService
     * @param {?} projectService
     * @param {?} analysisService
     * @param {?} sanitizer
     */
    constructor(actorService, projectService, analysisService, sanitizer) {
        this.actorService = actorService;
        this.projectService = projectService;
        this.analysisService = analysisService;
        this.sanitizer = sanitizer;
        this.onStepInstanceSelected = new EventEmitter();
        this.stepInstances = [];
        this.selectedStepInstance = null;
        this.loading = false;
        this.descriptions = {};
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.subscription = Observable.timer(1000, 10000).subscribe((/**
         * @param {?} t
         * @return {?}
         */
        t => {
            this.loadStepInstances();
        }));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
    /**
     * @private
     * @return {?}
     */
    loadStepInstances() {
        // this.loading = true;
        // this.descriptions = {};
        this.projectService.getActiveStepInstances(this.stepIds).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            if (result) {
                this.stepInstances = result;
                for (let stepInstance of this.stepInstances) {
                    if (stepInstance.step.stepType === "MESSAGE") {
                        // if (this.descriptions[stepInstance.id]) {
                        this.projectService.getMessageStepHtml(stepInstance.id).then((/**
                         * @param {?} html
                         * @return {?}
                         */
                        (html) => {
                            if (html["text"]) {
                                this.descriptions[stepInstance.id] = this.sanitizer.bypassSecurityTrustHtml(html["text"]);
                            }
                            else {
                                this.descriptions[stepInstance.id] = "<div></div>";
                            }
                        }));
                        // } else {
                        //   this.descriptions[stepInstance.id] = "<div></div>";
                        // }
                    }
                    else if (stepInstance.step.stepType === "PROGRESS_NOTES") {
                        this.descriptions[stepInstance.id] = "<div>" + stepInstance.step.parameters["0"].parameterValue + "</div>";
                    }
                    else {
                        this.descriptions[stepInstance.id] = "<div>" + stepInstance.step.stepType + "</div>";
                    }
                }
            }
            // this.loading = false;
        }), (/**
         * @param {?} error
         * @return {?}
         */
        error => {
            // this.loading = false;
        }));
    }
    /**
     * @param {?} stepInstance
     * @return {?}
     */
    stepInstanceSelected(stepInstance) {
        this.selectedStepInstance = stepInstance;
        this.onStepInstanceSelected.emit(stepInstance);
    }
}
StepInstanceListComponent.decorators = [
    { type: Component, args: [{
                selector: 'acn-step-instance-list',
                template: "<div class=\"step-instance-list\">\n  <!-- <loading-indicator [show]=\"loading\"></loading-indicator> -->\n\n  <div class=\"step-list\">\n    <div *ngFor=\"let stepInstance of stepInstances; odd as isOdd\">\n      <div class=\"in-tray-row\" [ngClass]=\"{ 'oddRow': isOdd, 'selectedRow': selectedStepInstance && selectedStepInstance.id === stepInstance.id }\"\n          (click)=\"stepInstanceSelected(stepInstance)\">\n        <div class=\"description\">\n          <div class=\"project\">\n            <div *ngIf=\"descriptions[stepInstance.id]\" class=\"message-content\" [innerHTML]=\"descriptions[stepInstance.id]\">\n            </div>\n          </div>\n          <div class=\"step-details\">\n            <div class=\"activation-date\">\n              Activated: {{ stepInstance.activationDate | date: 'yyyy-MM-dd HH:mm:ss' }}\n            </div>\n          </div>\n        </div>\n        <div class=\"cursor-image\">\n          <img src=\"data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAUVJREFUeJzt2EtOw0AURNELbCw7o9hZdgaTtBigBLv9+vM+JfUscnyPZw220+OknIDvx9HSN1kw8RufDkH8jU+DIJ7Hh0cQ/8eHRRDH48MhiPPxYRBEf7x7BHE93i2CsIt3hyDs47dBeF/8/58sRvg48Js78AbcBr3D7fH8+6Dnv9wRAAiMcBQAgiKcAYCACGcBIBhCDwAEQugFgCAIVwAgAMJVAHCOYAEAjhGsAMApgiUAOESwBgBnCCMAwBHCKABwgjASABwgjAaAzRFmAMDGCLMAYFOE1XeCqSbG3S5vccP8aqLiK77ijY9mhfRMVHzFV7zx0ayQnomKr/iKNz6aFdIzUfEVX/      HGR7NCeiYqPmb86jvBLzb/+m0i2JfvmUgc3yYSx7eJxPFtInF8m0gc3yYSx7eJxPFtInF8m0gc3yacxf8AAP323bWbSe8AAAAASUVORK5CYII=\">\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n",
                styles: [".step-instance-list{width:100%;height:100%;position:relative}.step-instance-list .step-list{width:100%;height:100%;overflow-y:scroll}.step-instance-list .step-list .in-tray-row{display:table;width:100%;padding:5px;cursor:pointer}.step-instance-list .step-list .in-tray-row .description{display:table-cell;width:calc(100% - 32px);padding-left:10px;vertical-align:middle}.step-instance-list .step-list .in-tray-row .description .project{font-size:14px}.step-instance-list .step-list .in-tray-row .description .step-details{line-height:15px}.step-instance-list .step-list .in-tray-row .description .step-details .step{font-size:15px}.step-instance-list .step-list .in-tray-row .description .step-details .activation-date{font-size:10px}.step-instance-list .step-list .in-tray-row .cursor-image{display:table-cell;width:32px;text-align:center;vertical-align:middle}.step-instance-list .step-list .in-tray-row .cursor-image img{width:16px;height:16px}.step-instance-list .step-list .oddRow{background-color:#f2f2f2}.step-instance-list .step-list .selectedRow{background-color:#abb9d3}"]
            }] }
];
/** @nocollapse */
StepInstanceListComponent.ctorParameters = () => [
    { type: ActorService },
    { type: ProjectService },
    { type: AnalysisService },
    { type: DomSanitizer }
];
StepInstanceListComponent.propDecorators = {
    stepIds: [{ type: Input }],
    onStepInstanceSelected: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class StepInstanceActiveComponent {
    /**
     * @param {?} actorService
     * @param {?} projectService
     * @param {?} analysisService
     * @param {?} sanitizer
     */
    constructor(actorService, projectService, analysisService, sanitizer) {
        this.actorService = actorService;
        this.projectService = projectService;
        this.analysisService = analysisService;
        this.sanitizer = sanitizer;
        this.isActive = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.subscription = Observable.timer(1000, 10000).subscribe((/**
         * @param {?} t
         * @return {?}
         */
        t => {
            this.loadStepInstances();
        }));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
    /**
     * @private
     * @return {?}
     */
    loadStepInstances() {
        this.projectService.isStepInstanceActive(this.stepId, this.externalId).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            this.isActive = "true" === result.response;
        }));
    }
}
StepInstanceActiveComponent.decorators = [
    { type: Component, args: [{
                selector: 'acn-step-instance-active',
                template: "<div class=\"step-instance-active\">\n  <div *ngIf=\"isActive\">\n    <ng-content>\n    </ng-content>\n  </div>\n</div>\n",
                styles: [".step-instance-active{width:100%;height:100%;position:relative}"]
            }] }
];
/** @nocollapse */
StepInstanceActiveComponent.ctorParameters = () => [
    { type: ActorService },
    { type: ProjectService },
    { type: AnalysisService },
    { type: DomSanitizer }
];
StepInstanceActiveComponent.propDecorators = {
    stepId: [{ type: Input }],
    externalId: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ProcessHistoryComponent {
    /**
     * @param {?} actorService
     * @param {?} projectService
     * @param {?} analysisService
     */
    constructor(actorService, projectService, analysisService) {
        this.actorService = actorService;
        this.projectService = projectService;
        this.analysisService = analysisService;
        this.stepInstances = [];
        this.loading = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        // this.loading = true;
        if (this.externalActorIdNr) {
            this.actorService.getActorFromExternalId(this.externalActorIdNr).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                this.actor = result;
                this.subscription = Observable.timer(1000, 5000).subscribe((/**
                 * @param {?} t
                 * @return {?}
                 */
                t => {
                    this.loadStepInstances();
                }));
            }));
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
    /**
     * @private
     * @return {?}
     */
    loadStepInstances() {
        /** @type {?} */
        let projectGuid;
        if (this.project) {
            projectGuid = this.project.guid;
        }
        else {
            projectGuid = this.projectGuid;
        }
        // this.loading = true;
        this.projectService.getProcessHistory(projectGuid, this.actor.id).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            if (result) {
                /** @type {?} */
                let stepInstances = [];
                for (let stepInstance of result) {
                    if (stepInstance.step.stepType === "PROGRESS_NOTES") {
                        stepInstances.push(stepInstance);
                    }
                }
                this.stepInstances = stepInstances;
            }
            // this.loading = false;
        }), (/**
         * @param {?} error
         * @return {?}
         */
        error => {
            // this.loading = false;
        }));
    }
}
ProcessHistoryComponent.decorators = [
    { type: Component, args: [{
                selector: 'acn-process-history',
                template: "<div class=\"process-history-component\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n\n  <div class=\"step-list\">\n    <div *ngFor=\"let stepInstance of stepInstances; odd as isOdd\" class=\"step-row\" [ngClass]=\"{ 'oddRow': isOdd }\">\n      <div class=\"description\">\n        <!-- {{ stepInstance.step.stepType }} -->\n        {{ stepInstance.step.parameters[\"0\"].parameterValue }}\n      </div>\n      <div class=\"step-date\">\n        Completed: {{ stepInstance.activationDate | date: 'yyyy-MM-dd HH:mm:ss' }}\n      </div>\n    </div>\n  </div>\n</div>\n",
                styles: [".process-history-component{width:100%;height:100%;position:relative}.process-history-component .step-list{width:100%;height:100%;overflow-y:scroll}.process-history-component .step-list .step-row{display:table;width:100%;padding:5px;font-size:14px}.process-history-component .step-list .step-row .description{display:table-cell;width:70%;padding-left:10px}.process-history-component .step-list .step-row .step-date{display:table-cell;width:30%;padding-left:10px}.process-history-component .step-list .oddRow{background-color:#f2f2f2}.process-history-component .step-list .selectedRow{background-color:#abb9d3}"]
            }] }
];
/** @nocollapse */
ProcessHistoryComponent.ctorParameters = () => [
    { type: ActorService },
    { type: ProjectService },
    { type: AnalysisService }
];
ProcessHistoryComponent.propDecorators = {
    project: [{ type: Input }],
    projectGuid: [{ type: Input }],
    externalActorIdNr: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LoadingIndicator {
    constructor() {
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
}
LoadingIndicator.decorators = [
    { type: Component, args: [{
                selector: 'loading-indicator',
                template: "<div *ngIf=\"show\" class=\"loading\">\n    <div class=\"loading-image\">\n        <img src=\"data:image/gif;base64,R0lGODlhIAAgAIQAAAQCBISChERGRMTCxDQyNPT29BQSFKyqrHRydNTW1Ly+vBwaHLSytHx6fNze3AwODFRSVDQ2NPz+/BQWFKyurHR2dNza3P///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQJCQAXACwAAAAAIAAgAAAF1eAljmRpnmh6UYJAqbB5ADRwxKdllUINCDiSI0IjOEa9GnBUoFAKKYKPMJrVbqKCFECAmiw+WqLawoqsNDMpEQaMVRSfmrQtxrRFr8mxNeIKBwd6KBZvQYeIMAMIFQOJKgg+CI8nCm0KjwwTCwwikWGTiRM0E55toYijAKUXlmGYiZoTnaY1qJQlCggIsLi+lBYQDw8QO78iCQ8+D8a/EG0QdwEBgyTKYQ8xATQBKNfL2twoz2HRMAXT1SPJy4YSFRUSj8HDEIYXbG7HJe8N8vsAA4oIAQAh+QQJCQAaACwAAAAAIAAgAIQEAgSEgoTEwsQ8Pjzs6uwsKizU0tS8vrwcGhyUkpRkYmTc2twMDgzMzsz09vQ0MjR0cnQEBgSMjozExsQsLizU1tScmpzc3tz8/vx0dnT///8AAAAAAAAAAAAAAAAAAAAF2KAmjmRpnmiqEZJEqLBJIACAvHEu1bWUlwnKI0Ha8Xw/UYIHIIpmtRtJQKFMUhQmhcRylQq1AlabAwPEqOUxN6leU5IqMkmv2+8ijKGBwZ8qZgUVfiQYZmF9dQ4BAQ4iDUw1DXYBNQGPkQCTdZUAlxqGTAWJdIuNI4BhBoQlGA18rLF0AhAZArIiEEwQsgeZBzkXCym6kbwiBxkZwCQLERHDJ8W7oApMCqQVz9EmvpHAakxOIsIq0wDHA5kDdQcQEMwaWZFbrNaRCrELDEwM3IQN1AEYsAlGCAAh+QQJCQAdACwAAAAAIAAgAIQEAgSMiozMysxcWlwkJiT08vSkpqTU1tQUEhRsbmyUlpQ0NjTU0tQsLiz8+vzc3twcGhx8enwEBgSUkpTMzsxsamwsKiysqqzc2twUFhScmpz8/vx8fnz///8AAAAAAAAF4WAnjmRpnmiqruyVIRcrjxkAIHP5RNFTIjZcTvQA3jCkCwJhKG00CkcqYrNFZBPbZFoFXFlZwBaFMSKQLIciqsLwDsO4fE6vpzYBgiUgtY8CXQF0GD4jBF0EcwcSEmgdh1WJcouNf4GDjh0OeQR8fp+gdhsMFBsyBRwLCxwFKAcWNgRwKgWwVQ2tTrY2FqZcXV4mFMAAFCQYmQvEC8LExh0MthbPysDMJQ67AASmBRBdEK1UwF8lB5CyIgbETQWQvLkmGxQMfR1hXWMFEaoR8TICiAkAVaFLhVAdLgwY0ERGCAAh+QQJCQAgACwAAAAAIAAgAIUEAgSEgoTMzsx0cnTs6uwkJiScnpzc3twcGhz09vQMCgycmpx8enw0NjS0srSMiozU1tQsLiykpqTk5uT8/vwEBgR0dnT08vQsKiykoqTk4uT8+vwMDgx8fnyMjozc2tz///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAG/kCQcEgsGo/IpHLJTAQ6CaZ0GAAAOlPjJlrsWLHZoUGhMBQTnQCXOJkoNxWrYq2ckN3IhMJa2Ug1ChUaSgsVCgtZbUxbYY2Oj5CRQhcdDQ0dF0MHAwMHjgQRVlYRBCAQewAKEI1eolYMIAOuA40NrlYNsbONobe5p3KrYRa3ABZCH5zCYRcYrhiZkpMWlgzR0thHGgsLg0gUDwUYARRMGXEAFRJIVaIPSxrofN5FBa4FSwvFiEb2ovhCNniw5KEcCH23+BVpZyXAEGKijoE4IC+dJyMUAhQo8MCgHlcVuJzjkyEMgWKlhGxbcDGMM1EYsoEQgMAKAgEyQVxw4ODaApIgACH5BAkJABsALAAAAAAgACAAhAQCBISGhMzOzDw+POzu7FxeXJyanNza3Pz6/AwKDExOTHRydPT29KSipOTi5FRWVAQGBJSWlNTW1PTy9GxqbNze3Pz+/AwODFRSVHR2dKSmpP///wAAAAAAAAAAAAAAAAXl4CaOZGmeaKqu7HZIbSxKEACPllYUmiWPNBvuASgCHr7TJJOZmCQHksZobKAyxQWrQC0WUIvstgv4KpnO1bRr/Z0QRCPSjULoeAi6fl9yGAwOJQxpdA01ABAaIxEQEBF0DodFEIEMhxAMbgZkjxOXhDERnCKMjpCSiIEiE6A/hpNtfCQOEYCyty0TBCsVCwsVLQQKRQq7JxIJRQk3KsNGCmBUFCsTZMYlFNIkFQEBwKvWx8kAyyMCF0UXAiPOxCkHvswbcUUYI8LE1zEDVAMlBPpkBKASAJcIBBQaUchjUIQFhi1CAAAh+QQJCQAZACwAAAAAIAAgAIQEAgSEhoTExsRERkQkJiSsqqzs7uxsbmwsLiy8urz09vQcGhyUlpTMzswEBgRMTkwsKiysrqz08vR0dnQ0MjS8vrz8+vycnpzU0tT///8AAAAAAAAAAAAAAAAAAAAAAAAF4mAmjuRoTBQyGWXrugYEzADBvng70fSU/yMEb4YYKSoVBbAkHBYzgsVsIcg1GqTd0KeQ0hZKV2OGFRkIPFsmMQQkXmNA2TxBqG7s4Rs+z3V5YEtAUVMVgksKCQlhh42OghI3jy8GDzMPkgYHB5KNljQPIwczB48SbTejAKWOBqhmm52Hn5eTMJ+YtpSyur2+IgoShxEDAxE5DA4ODEsFPAUvCg4zDow4AzwDLxLTAA7CJFcl2DTaL8nLJBbkAxYjzjTQOBLgIxc8FyQFxfKNATwBfonA0M0BBoEiKjx4YAjhpBAAIfkECQkAHgAsAAAAACAAIACEBAIEjI6MzMrMREZE7OrsHB4crKqsbG5s1NbUFBIUnJqcdHZ0DAoMlJaU1NLUVFJU/Pr83N7cBAYElJKUzM7M7O7sLCosrK6sdHJ03NrcFBYUnJ6cfHp8VFZU////AAAABdWgJ44kGU1TVK5s620SIG9uvUZyDqi22SWJDm+ikwV6o0xCl8h4AkXA0QPBaDQLCOsRfXhwRR5Gh2EtiwzRRtcYnWUJFiOaFkUCKdIbEF9xi141B2QsSkwINhAHGgkHWoU/QU5IlJWWl5iYFwMDFyUCApkeBjoGIxQyFJkDOgMjAqmrrSSgoqQ5pqIrBpy5ur/AwcIkBAEBBDUVBwcVSAQFMgXILYMAB0hQOVMs1dc92UYuyswlCB0PiCPP0dOWFjIWJcXHmTEAEsMiHDIL+SIOqvxhCgEAIfkECQkAGAAsAAAAACAAIACEBAIEjI6MREJEzMrMZGJk7OrsFBYUrK6s5OLkdHJ0DA4MTEpM1NLU/P78xMLEfHp8BAYEnJ6cREZEzM7M7O7sHB4cdHZ0xMbE////AAAAAAAAAAAAAAAAAAAAAAAAAAAABdUgJo5kSRVlqq5iIQGAhLJ0+sJxrV6PdY0FHG6mEz2Ej5YQRrwQCD+VYwlwiG4wyegCgUEGKgvVIkLcJIgRQUgIj0kFomiNa6emSyuN642mjjhkOhNPflIWPkVFDQ2KigwSEBALDI40DApCChOWLQEBM1g4WpYFFTAVaV1LEI2OAUIBGKtCrZawOLKiWZ2mqGmYmpWdCJ9pIpCSEsOdzc7P0NErA2CKBwYGBzQTMJxFBjAGNAPdiuAA4uOGIw56JNfZzhEwEdIlCTAJ9iQIAgLH+wLuCwEAO3N1SlNZTjl1dStWYzVFWkFGM2x6d1g3MmhFOUFpcjJxdjh3RjdZdzNnMmpLcmtwQUFmcU8zV3U3ZFFiZS95eDk=\">\n    </div>\n</div>\n",
                styles: [".loading{position:absolute;width:100%;height:100%;left:0;top:0;background:rgba(51,51,51,.1);z-index:1001}.loading .loading-image{position:absolute;top:50%;left:50%;margin-left:-16px;margin-top:-16px}"]
            }] }
];
/** @nocollapse */
LoadingIndicator.ctorParameters = () => [];
LoadingIndicator.propDecorators = {
    show: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ParticipantSearch {
    /**
     * @param {?} projectService
     * @param {?} actorService
     */
    constructor(projectService, actorService) {
        this.projectService = projectService;
        this.actorService = actorService;
        this.onParticipantSelected = new EventEmitter();
        this.participants = [];
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @return {?}
     */
    search() {
        this.projectService.getProjectRoles(this.project.id).then((/**
         * @param {?} projectRoles
         * @return {?}
         */
        (projectRoles) => {
            for (let projectRole of projectRoles) {
                if (projectRole.projectFunction === 'PARTICIPANT') {
                    this.projectService.getProjectActors3(0, 50, projectRole.id, this.searchField).then((/**
                     * @param {?} result
                     * @return {?}
                     */
                    (result) => {
                        this.participants = result.list;
                    }));
                    break;
                }
            }
        }));
    }
    /**
     * @param {?} participant
     * @return {?}
     */
    participantSelected(participant) {
        if (participant) {
            this.actorService.getProjectActor(participant.id, this.project.id).then((/**
             * @param {?} projectActor
             * @return {?}
             */
            (projectActor) => {
                this.onParticipantSelected.emit(projectActor);
            }));
        }
    }
}
ParticipantSearch.decorators = [
    { type: Component, args: [{
                selector: 'participant-search',
                template: "<div *ngIf=\"show\" class=\"paticipant-search\">\n  <div class=\"heading\">\n    Find participant\n  </div>\n  <div class=\"search-row\">\n    <div class=\"input-col\">\n      <input [(ngModel)]=\"searchField\" type=\"text\" id=\"searchField\">\n    </div>\n    <div class=\"button-col\" (click)=\"search()\">\n      <img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAY5JREFUSImt1LtrFGEUBfCfURsFHwEr29UNkS3MFklrQK0EIYUk/5IQ0FSmCCKW1mpAommToCKoK+lsLUKeSFbXFLuT3B13Hjt64INvOPeec+fOnUs2mpjHBrbRwQE+YQFTObm5qGMZf0qct7gxjPgM9kqKJ+cAs2XFf4fEX3iOe7iKsxjFHTxFO8R2ikzqqcq/oVFQUANfUm8ynhUce97qVVoGo/gaclcGBTVDQDuvigw09Lfrr+maD+TSkOIJngWNx2lyI5C3KxrcDRof0+R2IC9XNLgSNPbTZDKa7YricFr/v3EqIUZ0xxPO4FxFg0vhnoz7scFmICcqGjTDvRWJEayG57mKBg/C/U2anHDSu5+oDSlex6GTlTE2KOhVMPmACyXFL+qOZZL7Xf/3OMY17KZMrheI13px6e26nmVyX3eDxnYt4lav0qTiaTzp8VkrPNdkNyOpkyM4lEkNL0uK/CjgXw8ySHATD7GGLd0/fgfv8QiTOI93BSb/jCKT/4Isk1ZOTiWTF0H8M8aPANvFyARlADGFAAAAAElFTkSuQmCC\">\n    </div>\n  </div>\n\n  <div class=\"participant-list\">\n    <div *ngFor=\"let participant of participants; odd as isOdd\" class=\"participant-row\" [ngClass]=\"{ 'oddRow': isOdd }\" (click)=\"participantSelected(participant)\">\n      <div *ngIf=\"!participant.actorFieldValues || participant.actorFieldValues.length === 0\" class=\"description\">\n        <span>{{ participant.firstname }}</span>\n        <span *ngIf=\"participant.firstname !== participant.surname\">&nbsp;{{ participant.surname }}</span>\n        <span *ngIf=\"participant.surname !== participant.idNr\">&nbsp;{{ participant.idNr }}</span>\n      </div>\n      <div *ngIf=\"participant.actorFieldValues && participant.actorFieldValues.length > 0\" class=\"description\">\n        <span *ngFor=\"let actorFieldValue of participant.actorFieldValues; let i = index\">\n          <ng-container *ngIf=\"i === 0\">{{ actorFieldValue.fieldValue }}</ng-container><ng-container *ngIf=\"i > 0\">, {{ actorFieldValue.fieldValue }}</ng-container>\n        </span>\n      </div>\n      <div class=\"cursor-image\">\n        <img src=\"data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAUVJREFUeJzt2EtOw0AURNELbCw7o9hZdgaTtBigBLv9+vM+JfUscnyPZw220+OknIDvx9HSN1kw8RufDkH8jU+DIJ7Hh0cQ/8eHRRDH48MhiPPxYRBEf7x7BHE93i2CsIt3hyDs47dBeF/8/58sRvg48Js78AbcBr3D7fH8+6Dnv9wRAAiMcBQAgiKcAYCACGcBIBhCDwAEQugFgCAIVwAgAMJVAHCOYAEAjhGsAMApgiUAOESwBgBnCCMAwBHCKABwgjASABwgjAaAzRFmAMDGCLMAYFOE1XeCqSbG3S5vccP8aqLiK77ijY9mhfRMVHzFV7zx0ayQnomKr/iKNz6aFdIzUfEVX/      HGR7NCeiYqPmb86jvBLzb/+m0i2JfvmUgc3yYSx7eJxPFtInF8m0gc3yYSx7eJxPFtInF8m0gc3yacxf8AAP323bWbSe8AAAAASUVORK5CYII=\">\n      </div>\n    </div>\n  </div>\n\n  <div class=\"button-section\">\n    <div class=\"button button-outline\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                styles: [".paticipant-search{width:100%;height:100%;position:relative}.paticipant-search .heading{position:absolute;top:0;width:100%;height:32px;font-size:16px;padding:8px}.paticipant-search .top-padding{padding-top:8px}.paticipant-search .search-row{display:table;width:100%;position:absolute;top:32px;height:40px;left:0;padding:8px;box-sizing:border-box;background-color:#f9f9f9;font-size:15px}.paticipant-search .search-row .input-col{display:table-cell;width:90%;font-size:15px;padding-top:7px;vertical-align:top}.paticipant-search .search-row .input-col input{width:100%;font-size:13px;border:1px solid #58666c;background-color:#fffcf6;box-sizing:border-box;padding:3px 5px;border-radius:5px;height:30px}.paticipant-search .search-row .button-col{display:table-cell;width:32px;padding-top:7px;text-align:right;cursor:pointer;box-sizing:border-box}.paticipant-search .search-row .button-col img{border:1px solid #58666c;padding:5px;border-radius:5px;box-sizing:border-box;width:30px;height:30px}.paticipant-search .participant-list{position:absolute;top:90px;bottom:50px;width:100%;overflow-y:scroll}.paticipant-search .participant-list .participant-row{display:table;box-sizing:border-box;width:100%;padding:5px;cursor:pointer}.paticipant-search .participant-list .participant-row .description{display:table-cell;width:calc(100% - 64px);padding-left:2px;padding-top:3px;font-size:14px;vertical-align:middle}.paticipant-search .participant-list .participant-row .cursor-image{display:table-cell;width:32px;text-align:center;vertical-align:middle}.paticipant-search .participant-list .participant-row .cursor-image img{width:16px;height:16px}.paticipant-search .oddRow{background-color:#f2f2f2}.paticipant-search .error-text{font-style:italic;font-size:11px;color:#f04141}.paticipant-search .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.paticipant-search .button-section .button{width:120px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.paticipant-search .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.paticipant-search .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.paticipant-search .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b;cursor:default}.paticipant-search .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b;cursor:default}"]
            }] }
];
/** @nocollapse */
ParticipantSearch.ctorParameters = () => [
    { type: ProjectService },
    { type: ActorService }
];
ParticipantSearch.propDecorators = {
    show: [{ type: Input }],
    project: [{ type: Input }],
    onParticipantSelected: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MessageComponent {
    /**
     * @param {?} projectService
     * @param {?} sanitizer
     */
    constructor(projectService, sanitizer) {
        this.projectService = projectService;
        this.sanitizer = sanitizer;
        this.stepCompletedListener = new EventEmitter();
        this.cancelListener = new EventEmitter();
        this.loading = false;
    }
    /**
     * @param {?} s
     * @return {?}
     */
    set stepInstance(s) {
        this._stepInstance = s;
        this.loading = true;
        this.projectService.getMessageStepHtml(this._stepInstance.id).then((/**
         * @param {?} html
         * @return {?}
         */
        (html) => {
            if (html["text"]) {
                this.html = this.sanitizer.bypassSecurityTrustHtml(html["text"]);
            }
            else {
                this.html = "<div></div>";
            }
            this.loading = false;
        }));
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @return {?}
     */
    completeStep() {
        this.loading = true;
        this.projectService.completeStep(this._stepInstance.id, this.userId).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            this.loading = false;
            this.stepCompletedListener.emit({
                value: this._stepInstance
            });
        }));
    }
    /**
     * @return {?}
     */
    cancel() {
        this.cancelListener.emit({});
    }
}
MessageComponent.decorators = [
    { type: Component, args: [{
                selector: 'message',
                template: "<div class=\"message-step\">\n  <div *ngIf=\"!html\" class=\"info-message\">\n    Loading information. Please wait...\n  </div>\n  <div *ngIf=\"html\" class=\"message-content\" [innerHTML]=\"html\">\n  </div>\n\n  <div *ngIf=\"!loading\" class=\"button-section\">\n    <div class=\"button\" (click)=\"completeStep()\">\n      <div class=\"button-text\">Complete</div>\n    </div>\n    <div class=\"button button-outline\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n  <div *ngIf=\"loading\" class=\"button-section\">\n    <div class=\"button button-disabled\" (click)=\"completeStep()\">\n      <div class=\"button-text\">Complete</div>\n    </div>\n    <div class=\"button button-outline-disabled\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                styles: [".message-step{width:100%;height:100%;position:relative}.message-step .info-message{position:absolute;top:0;left:0;bottom:50px;width:100%;background-color:#f9f9f9;padding:8px;box-sizing:border-box}.message-step .message-content{position:absolute;top:0;left:0;bottom:50px;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px;box-sizing:border-box}.message-step .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.message-step .button-section .button{width:120px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.message-step .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.message-step .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.message-step .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b}.message-step .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b}"]
            }] }
];
/** @nocollapse */
MessageComponent.ctorParameters = () => [
    { type: ProjectService },
    { type: DomSanitizer }
];
MessageComponent.propDecorators = {
    _stepInstance: [{ type: Input }],
    userId: [{ type: Input }],
    stepCompletedListener: [{ type: Output }],
    cancelListener: [{ type: Output }],
    stepInstance: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CreateSystemUserComponent {
    /**
     * @param {?} projectService
     */
    constructor(projectService) {
        this.projectService = projectService;
        this.stepCompletedListener = new EventEmitter();
        this.cancelListener = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.usernameError = false;
        this.usernameExistsError = false;
        this.password1Error = false;
        this.password2Error = false;
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @return {?}
     */
    completeStep() {
        this.usernameError = false;
        this.usernameExistsError = false;
        this.password1Error = false;
        this.password2Error = false;
        if (!this.username) {
            this.usernameError = true;
            return;
        }
        if (!this.password1) {
            this.password1Error = true;
            return;
        }
        if (this.password1 !== this.password2) {
            this.password2Error = true;
            return;
        }
        this.projectService.createSystemUser(this.username, this.password1, this.stepInstance.businessProcessInstance.actor.id).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            if (result["response"] === "Success") {
                this.projectService.completeStep(this.stepInstance.id, this.userId).then((/**
                 * @param {?} result
                 * @return {?}
                 */
                result => {
                    this.stepCompletedListener.emit({
                        value: this.stepInstance,
                        useBusinessProcessId: true
                    });
                }));
            }
            else {
                this.usernameExistsError = true;
            }
        }));
    }
    /**
     * @return {?}
     */
    cancel() {
        this.cancelListener.emit({});
    }
}
CreateSystemUserComponent.decorators = [
    { type: Component, args: [{
                selector: 'create-system-user',
                template: "<div class=\"create-system-user\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div class=\"heading\">\n    Create System User\n  </div>\n\n  <div *ngIf=\"!loading\" class=\"content\">\n    <div class=\"component-row\">\n      <div class=\"component-col\">\n        <div class=\"component-label\">\n          Username\n          <div *ngIf=\"usernameError\" class=\"error-message\">A username must be entered</div>\n          <div *ngIf=\"usernameExistsError\" class=\"error-message\">The username already exists</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"text\" [(ngModel)]=\"username\">\n        </div>\n      </div>\n    </div>\n\n    <div class=\"component-row\">\n      <div class=\"component-col\">\n        <div class=\"component-label\">\n          Password\n          <div *ngIf=\"password1Error\" class=\"error-message\">A password be entered</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"password\" [(ngModel)]=\"password1\">\n        </div>\n      </div>\n      <div class=\"component-col\">\n        <div class=\"component-label\">\n          Re-type password\n          <div *ngIf=\"password2Error\" class=\"error-message\">The second password differs from the first password</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"password\" [(ngModel)]=\"password2\">\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!loading\" class=\"button-section\">\n    <div class=\"button\" (click)=\"completeStep()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div class=\"button button-outline\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n  <div *ngIf=\"loading\" class=\"button-section\">\n    <div class=\"button button-disabled\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div class=\"button button-outline-disabled\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                styles: [".create-system-user{width:100%;height:100%;position:relative}.create-system-user .heading{position:absolute;top:0;width:100%;height:32px;font-size:16px;padding:8px}.create-system-user .content{position:absolute;top:32px;left:0;bottom:50px;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px;box-sizing:border-box}.create-system-user .content .info-message{font-size:15px}.create-system-user .content .component-row{width:100%}.create-system-user .content .component-row .component-col{width:100%;display:inline-block;box-sizing:border-box;padding-top:10px}.create-system-user .content .component-row .component-col .component-label{width:100%;font-size:15px;padding-bottom:5px}.create-system-user .content .component-row .component-col .error-message{font-style:italic;font-size:11px;color:#f04141;padding-bottom:5px}.create-system-user .content .component-row .component-col .component{width:100%}.create-system-user .content .component-row .component-col .component input{width:100%;font-size:13px;border:1px solid #58666c;background-color:#fffcf6;box-sizing:border-box;padding:3px 5px;border-radius:5px;height:30px}.create-system-user .content .component-row:first-child .component-col:first-child{padding-top:0}@media (min-width:600px){.create-system-user .content .component-row .component-col{width:50%}.create-system-user .content .component-row .component-col:first-child{padding-right:16px}.create-system-user .content .component-row:first-child .component-col{padding-top:0}}.create-system-user .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.create-system-user .button-section .button{width:70px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.create-system-user .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.create-system-user .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.create-system-user .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b}.create-system-user .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b}"]
            }] }
];
/** @nocollapse */
CreateSystemUserComponent.ctorParameters = () => [
    { type: ProjectService }
];
CreateSystemUserComponent.propDecorators = {
    stepInstance: [{ type: Input }],
    userId: [{ type: Input }],
    stepCompletedListener: [{ type: Output }],
    cancelListener: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class IdNrLookupComponent {
    /**
     * @param {?} sanitizer
     * @param {?} projectService
     */
    constructor(sanitizer, projectService) {
        this.sanitizer = sanitizer;
        this.projectService = projectService;
        this.stepCompletedListener = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.noIdNr = false;
        this.idNrNotFound = false;
        this.invalidIdNr = false;
        this.hideNoIdNrWarning = this.stepInstance.step.parameters["0"] && this.stepInstance.step.parameters["0"].parameterValue;
        this.hideNoIdNrOption = this.stepInstance.step.parameters["1"] && this.stepInstance.step.parameters["1"].parameterValue;
        if (this.stepInstance.step.parameters["2"]) {
            this.idNrLabel = this.stepInstance.step.parameters["2"].parameterValue;
        }
        if (!this.idNrLabel) {
            this.idNrLabel = "Id number";
        }
        this.loading = true;
        this.projectService.getMessageStepHtml(this.stepInstance.id).then((/**
         * @param {?} html
         * @return {?}
         */
        (html) => {
            this.loading = false;
            if (html["text"]) {
                this.html = this.sanitizer.bypassSecurityTrustHtml(html["text"]);
            }
        }));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @return {?}
     */
    continueWithIdNr() {
        this.noIdNr = false;
        this.idNrNotFound = false;
        this.invalidIdNr = false;
        if (!this.idNr) {
            this.noIdNr = true;
            return;
        }
        // if (this.idNr.length !== 13 || !(/^\d+$/.test(this.idNr))) {
        //     this.invalidIdNr = true;
        //     return;
        // }
        this.projectService.verifyActorIdNr(this.stepInstance.businessProcessInstance.id, this.idNr).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            if (result.response === "true") {
                this.completeStep();
            }
            else {
                if (!this.hideNoIdNrWarning) {
                    this.idNrNotFound = true;
                }
                else {
                    this.completeStep();
                }
            }
        }));
    }
    /**
     * @return {?}
     */
    continueNoIdNr() {
        this.completeStep();
    }
    /**
     * @return {?}
     */
    completeStep() {
        this.projectService.completeStep(this.stepInstance.id, this.userId).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            this.stepCompletedListener.emit({
                value: this.stepInstance,
                useBusinessProcessId: true
            });
        }));
    }
}
IdNrLookupComponent.decorators = [
    { type: Component, args: [{
                selector: 'id-nr-lookup',
                template: "<div class=\"id-nr-lookup\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div class=\"heading\">\n    Registration\n  </div>\n\n  <div *ngIf=\"!this.loading && hideNoIdNrOption\" class=\"content\">\n    <div *ngIf=\"!html\" class=\"wording\">\n      Please provide a valid {{ idNrLabel }}.\n    </div>\n    <div *ngIf=\"!html\" class=\"wording\">\n      This number is encrypted immediately and is not shared with any other party.\n    </div>\n    <div *ngIf=\"!html\" class=\"wording\">\n      It is only used as a unique identifier for this application. If you register\n      for future projects, it will also be used to link your projects.\n    </div>\n    <div *ngIf=\"!html\" class=\"wording\">\n      Schemes will notify us when you are hospitalised or registered for a disease so\n      that the relevant surveys can be sent to you.\n    </div>\n    <div *ngIf=\"html\" class=\"wording\" [innerHTML]=\"html\">\n    </div>\n\n    <div>\n      <input [(ngModel)]=\"idNr\" type=\"text\" id=\"idNrInput\" placeholder=\"{{ idNrLabel }}\">\n    </div>\n\n    <div *ngIf=\"idNrNotFound && !hideNoIdNrWarning\" class=\"error-text\">\n      No details were found on the system for the specified {{ idNrLabel }}.  Are you sure it is correct?\n    </div>\n    <div *ngIf=\"noIdNr\" class=\"error-text\">\n      Please provide your {{ idNrLabel }}\n    </div>\n    <div *ngIf=\"invalidIdNr && hideNoIdNrWarning\" class=\"error-text\">\n      Please provide a valid {{ idNrLabel }}\n    </div>\n  </div>\n  <div *ngIf=\"hideNoIdNrOption\" class=\"button-section\">\n    <div *ngIf=\"!idNrNotFound\" class=\"button\" (click)=\"continueWithIdNr()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div *ngIf=\"idNrNotFound\" class=\"button button-outline\" (click)=\"completeStep()\">\n      <div class=\"button-text\">{{ idNrLabel }} is correct</div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!this.loading && !hideNoIdNrOption\" class=\"content\">\n    <div *ngIf=\"!html\" class=\"wording\">\n      To continue with the registration process, please enter your {{ idNrLabel }}:\n    </div>\n    <div *ngIf=\"html\" class=\"wording\" [innerHTML]=\"html\">\n    </div>\n    <div>\n      <input class=\"form-control\" [(ngModel)]=\"idNr\" type=\"text\" id=\"idNrInput\">\n    </div>\n\n    <div *ngIf=\"idNrNotFound && !hideNoIdNrWarning\" class=\"error-text\">\n      No details were found on the system for the specified {{ idNrLabel }}.  Are you sure it is correct?\n    </div>\n    <div *ngIf=\"noIdNr\" class=\"error-text\">\n      Please provide your {{ idNrLabel }}\n    </div>\n    <div *ngIf=\"invalidIdNr && hideNoIdNrWarning\" class=\"error-text\">\n      Please provide a valid {{ idNrLabel }}\n    </div>\n    <div class=\"wording top-padding\">\n      Alternatively, if no {{ idNrLabel }} is avalaible, please click on \"No {{ idNrLabel }}\" below to continue.\n    </div>\n  </div>\n  <div *ngIf=\"!hideNoIdNrOption\" class=\"button-section\">\n    <div *ngIf=\"!idNrNotFound\" class=\"button\" (click)=\"continueWithIdNr()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div *ngIf=\"idNrNotFound\" class=\"button\" (click)=\"completeStep()\">\n      <div class=\"button-text\">{{ idNrLabel }} is correct</div>\n    </div>\n    <div class=\"button button-outline\" (click)=\"continueNoIdNr()\">\n      <div class=\"button-text\">No {{ idNrLabel }}</div>\n    </div>\n  </div>\n</div>\n",
                styles: [".id-nr-lookup{width:100%;height:100%;position:relative}.id-nr-lookup .heading{position:absolute;top:0;width:100%;height:32px;font-size:16px;padding:8px}.id-nr-lookup .top-padding{padding-top:8px}.id-nr-lookup .content{position:absolute;top:32px;left:0;bottom:50px;width:100%;overflow-y:scroll;padding:8px;box-sizing:border-box;background-color:#f9f9f9;font-size:15px}.id-nr-lookup .content .wording{width:100%;font-size:15px;padding-bottom:5px}.id-nr-lookup .content input{width:200;font-size:13px;border:1px solid #58666c;background-color:#fffcf6;box-sizing:border-box;padding:3px 5px;margin-bottom:5px;height:30px;border-radius:5px}.id-nr-lookup .error-text{font-style:italic;font-size:11px;color:#f04141}.id-nr-lookup .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.id-nr-lookup .button-section .button{width:120px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.id-nr-lookup .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.id-nr-lookup .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.id-nr-lookup .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b;cursor:default}.id-nr-lookup .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b;cursor:default}"]
            }] }
];
/** @nocollapse */
IdNrLookupComponent.ctorParameters = () => [
    { type: DomSanitizer },
    { type: ProjectService }
];
IdNrLookupComponent.propDecorators = {
    stepInstance: [{ type: Input }],
    userId: [{ type: Input }],
    stepCompletedListener: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class RegisterComponent {
    /**
     * @param {?} projectService
     */
    constructor(projectService) {
        this.projectService = projectService;
        this.stepCompletedListener = new EventEmitter();
        this.cancelListener = new EventEmitter();
        this.agreedError = false;
        this.loading = false;
        this.agreeWording = "I agree to the terms and conditions as stated above.";
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.agreed = false;
        this.agreedError = false;
        this.loading = true;
        this.projectService.getStepParameters(this.stepInstance.step.id).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            this.loading = false;
            this.html = result["0"]; // && this.stepInstance.step.parameters["0"].parameterValue;
        }));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @return {?}
     */
    completeStep() {
        this.agreedError = false;
        if (!this.agreed) {
            this.agreedError = true;
            return;
        }
        this.loading = true;
        this.projectService.completeStep(this.stepInstance.id, this.userId).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            this.loading = false;
            this.stepCompletedListener.emit({
                value: this.stepInstance,
                useBusinessProcessId: true
            });
        }));
    }
    /**
     * @param {?} $selected
     * @return {?}
     */
    radioButtonChanged($selected) {
        this.agreed = $selected.value;
    }
    /**
     * @return {?}
     */
    cancel() {
        this.cancelListener.emit({});
    }
}
RegisterComponent.decorators = [
    { type: Component, args: [{
                selector: 'register',
                template: "<div class=\"register\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div class=\"heading\">\n    Register\n  </div>\n\n  <div class=\"content\">\n    <div *ngIf=\"html\" class=\"message\" [innerHTML]=\"html\">>\n    </div>\n    <div class=\"agree-checkbox\">\n      <checkbox-component [selected]=\"agreed\" (selectionListener)=\"radioButtonChanged($event);\"\n          [description]=\"agreeWording\"></checkbox-component>\n      <!-- <checkbox-component [selected]=\"agreed\" (selectionListener)=\"radioButtonChanged($event);\"\n        [description]=\"agreeWording\"></checkbox-component> -->\n    </div>\n    <div *ngIf=\"agreedError\" class=\"agree-error\">\n      You must agree to the terms and conditions to register.\n    </div>\n  </div>\n\n  <div *ngIf=\"!loading\" class=\"button-section\">\n    <div class=\"button\" (click)=\"completeStep()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div class=\"button button-outline\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n  <div *ngIf=\"loading\" class=\"button-section\">\n    <div class=\"button button-disabled\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div class=\"button button-outline-disabled\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                styles: [".register{width:100%;height:100%;position:relative}.register .heading{position:absolute;top:0;width:100%;height:32px;font-size:16px;padding:8px}.register .content{position:absolute;top:32px;left:0;bottom:50px;width:100%;overflow-y:scroll;padding:8px;box-sizing:border-box;background-color:#f9f9f9}.register .content .agree-checkbox{width:100%;font-size:15px;padding-bottom:5px}.register .content .agree-error{font-style:italic;font-size:11px;color:#f04141;padding-bottom:5px}.register .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.register .button-section .button{width:70px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.register .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.register .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.register .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b}.register .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b}"]
            }] }
];
/** @nocollapse */
RegisterComponent.ctorParameters = () => [
    { type: ProjectService }
];
RegisterComponent.propDecorators = {
    stepInstance: [{ type: Input }],
    userId: [{ type: Input }],
    stepCompletedListener: [{ type: Output }],
    cancelListener: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MaintenanceService {
    // private backendServiceUrl = 'http://www.healthacumen.co.za/insight/';
    // private backendServiceUrl = 'https://frank.rmsui.co.za/insight/';
    // private backendServiceUrl = 'http://localhost:8888/';
    /**
     * @param {?} httpClient
     * @param {?} acumenConfiguration
     */
    constructor(httpClient, acumenConfiguration) {
        this.httpClient = httpClient;
        this.acumenConfiguration = acumenConfiguration;
        if (acumenConfiguration.backendServiceUrl) {
            this.backendServiceUrl = acumenConfiguration.backendServiceUrl;
        }
        else {
            this.backendServiceUrl = "https://frank.rmsui.co.za/insight/";
        }
    }
    // getTitles(): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/titles`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    //
    // getEmployers(): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/employers`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    //
    // getWorksites(): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/worksites`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       let workSites = response;
    //       for (let i = 0; i < workSites.length; i++) {
    //           workSites[i].defaultOption = workSites[i]["defaultWorkSite"];
    //       }
    //
    //       return workSites;
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    //   }
    //
    // getBuildings(employerId: number): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/buildings/${employerId}`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    //
    // getMedicalAids(): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/medicalaids`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    //
    // getSubSites(workSiteId: number): Promise<SelectionOption[]> {
    //   let url = this.backendServiceUrl + `rest/maintenance/subSites/${workSiteId}`;
    //   const headers = new HttpHeaders({
    //     'Accept': 'application/json',
    //   });
    //
    //   return this.httpClient.get<SelectionOption[]>(url, {headers: headers}).toPromise()
    //     .then(response => {
    //       return Promise.resolve(response as SelectionOption[]);
    //     }).catch(error => {
    //       return Promise.reject(error);
    //     });
    // }
    /**
     * @param {?} actorId
     * @return {?}
     */
    getActorFields(actorId) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/maintenance/actorType/actorFields/${actorId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
    /**
     * @param {?} actorFieldId
     * @return {?}
     */
    getActorFieldOptions(actorFieldId) {
        /** @type {?} */
        let url = this.backendServiceUrl + `rest/maintenance/actorType/actorFields/actorFieldOptions/${actorFieldId}`;
        /** @type {?} */
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        return this.httpClient.get(url, { headers: headers }).toPromise()
            .then((/**
         * @param {?} response
         * @return {?}
         */
        response => {
            return Promise.resolve((/** @type {?} */ (response)));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            return Promise.reject(error);
        }));
    }
}
MaintenanceService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
MaintenanceService.ctorParameters = () => [
    { type: HttpClient },
    { type: AcumenConfiguration }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class Actor {
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class UpdateDetailsComponent {
    /**
     * @param {?} projectService
     * @param {?} maintenanceService
     * @param {?} actorService
     */
    constructor(projectService, maintenanceService, actorService) {
        this.projectService = projectService;
        this.maintenanceService = maintenanceService;
        this.actorService = actorService;
        this.stepCompletedListener = new EventEmitter();
        this.cancelListener = new EventEmitter();
        this.actorFieldList = [];
        this.actorFields = [];
        this.actorFieldOptionsMap = {};
        this.actorFieldValues = {};
        this.actorFieldErrors = {};
        this.updateError = false;
        this.firstnameError = false;
        this.surnameError = false;
        this.idNrError = false;
        this.celNrError = false;
        this.emailError = false;
        this.emailConfirmationError = false;
        this.actor = new Actor();
        this.loading = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.loading = true;
        this.projectService.getStepParameters(this.stepInstance.step.id).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            // this.requiresEmail = result[0] && "true" === result[0];
            this.requiresFirstname = result["6"] && "true" === result["6"];
            this.requiresSurname = result["7"] && "true" === result["7"];
            this.requiresCelNr = result["8"] && "true" === result["8"];
            this.requiresEmail = result["0"] && "true" === result["0"];
            this.requiresEmployer = result["1"] && "true" === result["1"];
            if (result["9"]) {
                this.idNrLabel = result["9"];
            }
            else {
                this.idNrLabel = "Id nr";
            }
            // alert(this.stepInstance.actor.idNr + " " + this.stepInstance.businessProcessInstance.actor.idNr);
            this.actor = this.stepInstance.businessProcessInstance.actor;
            if (this.actor.email) {
                this.emailConfirmation = this.actor.email;
            }
            if (this.actor.actorType) {
                this.maintenanceService.getActorFields(this.actor.actorType.id).then((/**
                 * @param {?} actorFields
                 * @return {?}
                 */
                actorFields => {
                    this.actorFieldList = actorFields;
                    if (actorFields) {
                        for (let i = 0; i < actorFields.length; i++) {
                            console.log("trace 9 -> " + i + ", " + actorFields[i].id);
                            this.actorFieldValues[actorFields[i].id] = "";
                            this.actorFieldErrors[actorFields[i].id] = false;
                            if (i % 2 === 0) {
                                console.log("trace 10");
                                /** @type {?} */
                                let actorFieldRow = {};
                                actorFieldRow[0] = actorFields[i];
                                console.log("trace 11");
                                if (i < actorFields.length - 1) {
                                    console.log("trace 12");
                                    actorFieldRow[1] = actorFields[i + 1];
                                }
                                this.actorFields.push(actorFieldRow);
                            }
                            if (actorFields[i].actorFieldType === "SELECT") {
                                console.log("trace 13");
                                this.maintenanceService.getActorFieldOptions(actorFields[i].id).then((/**
                                 * @param {?} actorFieldOptions
                                 * @return {?}
                                 */
                                actorFieldOptions => {
                                    console.log("trace 14");
                                    if (actorFieldOptions) {
                                        console.log("trace 15");
                                        this.actorFieldOptionsMap[actorFields[i].id] = actorFieldOptions;
                                    }
                                }));
                            }
                        }
                    }
                }));
                this.actorService.getActorFieldValues(this.actor.id).then((/**
                 * @param {?} actorFieldValues
                 * @return {?}
                 */
                actorFieldValues => {
                    for (let actorFieldValue of actorFieldValues) {
                        this.actorFieldValues[actorFieldValue.actorFieldId] = actorFieldValue.fieldValue;
                    }
                    this.loading = false;
                }));
            }
            else {
                this.loading = false;
            }
        }));
        // console.log("trace 6");
        // this.projectService.getProcessVariableValue(this.stepInstance.businessProcessInstance.id, "idNr").then(result => {
        //   console.log("trace 7");
        //   if (result) {
        //     console.log("trace 8");
        //     this.actor.idNr = result;
        //   }
        //
        //   this.loading = false;
        // });
    }
    /**
     * @return {?}
     */
    completeStep() {
        this.firstnameError = false;
        this.surnameError = false;
        this.idNrError = false;
        this.celNrError = false;
        this.emailError = false;
        this.emailConfirmationError = false;
        // requiresFirstname: boolean;
        // requiresSurname: boolean;
        // requiresCelNr: boolean;
        // requiresEmployer: boolean;
        if (this.requiresFirstname) {
            if (!this.actor.firstname) {
                this.firstnameError = true;
                return;
            }
        }
        else {
            this.actor.firstname = this.actor.idNr;
        }
        if (this.requiresSurname) {
            if (!this.actor.surname) {
                this.surnameError = true;
                return;
            }
        }
        else {
            this.actor.surname = this.actor.idNr;
        }
        if (!this.actor.idNr) {
            this.idNrError = true;
            return;
        }
        if (this.requiresCelNr && !this.actor.celNr) {
            this.celNrError = true;
            return;
        }
        if (!this.actor.email) {
            this.emailError = true;
            return;
        }
        if (this.actor.email !== this.emailConfirmation) {
            this.emailConfirmationError = true;
            return;
        }
        this.actorFieldErrors = {};
        /** @type {?} */
        let errorFound = false;
        for (const actorField of this.actorFieldList) {
            /** @type {?} */
            let fieldValue = this.actorFieldValues[actorField.id];
            if (actorField.required === true && (!fieldValue || fieldValue === "")) {
                this.actorFieldErrors[actorField.id] = true;
                errorFound = true;
            }
            else {
                this.actorFieldErrors[actorField.id] = false;
            }
        }
        if (errorFound) {
            return;
        }
        this.updateError = false;
        this.loading = true;
        this.projectService.updateActor(this.actor).then((/**
         * @param {?} updateResult
         * @return {?}
         */
        updateResult => {
            if (updateResult) {
                /** @type {?} */
                let actorFieldValues = [];
                for (const actorFieldId of Object.keys(this.actorFieldValues)) {
                    actorFieldValues.push({
                        actorFieldId: Number(actorFieldId),
                        fieldValue: this.actorFieldValues[actorFieldId]
                    });
                }
                this.actorService.updateActorFieldValues(this.actor.id, actorFieldValues).then((/**
                 * @param {?} updateResult2
                 * @return {?}
                 */
                updateResult2 => {
                    if (updateResult2) {
                        this.projectService.completeStep(this.stepInstance.id, this.userId).then((/**
                         * @param {?} result
                         * @return {?}
                         */
                        result => {
                            this.loading = false;
                            this.stepCompletedListener.emit({
                                value: this.stepInstance
                            });
                        }));
                    }
                    else {
                        this.updateError = true;
                        this.loading = false;
                    }
                }));
            }
            else {
                this.updateError = true;
                this.loading = false;
            }
        }));
    }
    /**
     * @return {?}
     */
    cancel() {
        this.cancelListener.emit({});
    }
}
UpdateDetailsComponent.decorators = [
    { type: Component, args: [{
                selector: 'update-details',
                template: "<div class=\"update-details\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div class=\"heading\">\n    Update details\n  </div>\n\n  <div *ngIf=\"!loading && updateError\" class=\"content\">\n    <div class=\"info-message\">\n      Update failed\n    </div>\n  </div>\n\n  <div *ngIf=\"!loading && !updateError\" class=\"content\">\n    <div *ngIf=\"requiresFirstname || requiresSurname\" class=\"component-row\">\n      <div *ngIf=\"requiresFirstname\" class=\"component-col\">\n        <div class=\"component-label\">\n          Firstname\n          <div *ngIf=\"firstnameError\" class=\"error-message\">A firstname must be entered</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"text\" [(ngModel)]=\"actor.firstname\">\n        </div>\n      </div>\n      <div *ngIf=\"requiresSurname\" class=\"component-col\">\n        <div class=\"component-label\">\n          Surname\n          <div *ngIf=\"surnameError\" class=\"error-message\">A surname must be entered</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"text\" [(ngModel)]=\"actor.surname\">\n        </div>\n      </div>\n    </div>\n\n    <div class=\"component-row\">\n      <div class=\"component-col\">\n        <div class=\"component-label\">\n          {{ idNrLabel }}\n          <div *ngIf=\"idNrError\" class=\"error-message\">An {{ idNrLabel }} must be entered</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"text\" [(ngModel)]=\"actor.idNr\">\n        </div>\n      </div>\n      <div *ngIf=\"requiresCelNr\" class=\"component-col\">\n        <div class=\"component-label\">\n          Cel nr\n          <div *ngIf=\"celNrError\" class=\"error-message\">A cel nr must be entered</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"text\" [(ngModel)]=\"actor.celNr\">\n        </div>\n      </div>\n    </div>\n\n    <div class=\"component-row\">\n      <div class=\"component-col\">\n        <div class=\"component-label\">\n          E-mail address\n          <div *ngIf=\"emailError\" class=\"error-message\">An email address must be entered</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"email\" [(ngModel)]=\"actor.email\">\n        </div>\n      </div>\n      <div class=\"component-col\">\n        <div class=\"component-label\">\n          Confirm e-mail address\n          <div *ngIf=\"emailConfirmationError\" class=\"error-message\">The email addresses entered are not the same</div>\n        </div>\n        <div class=\"component\">\n          <input type=\"email\" [(ngModel)]=\"emailConfirmation\">\n        </div>\n      </div>\n    </div>\n\n    <div *ngFor=\"let actorFieldRow of actorFields; let i = index\" class=\"component-row\">\n      <div class=\"component-col\">\n        <div class=\"component-label\">\n          {{ actorFieldRow[0].description }}\n          <div *ngIf=\"actorFieldErrors[actorFieldRow[0].id]\" class=\"error-message\">\n            A value must be entered for {{ actorFieldRow[0].description }}\n          </div>\n        </div>\n        <div class=\"component\">\n          <input *ngIf=\"actorFieldRow[0].actorFieldType === 'TEXT'\" type=\"text\" [(ngModel)]=\"actorFieldValues[actorFieldRow[0].id]\">\n          <select *ngIf=\"actorFieldRow[0].actorFieldType === 'SELECT'\" [(ngModel)]=\"actorFieldValues[actorFieldRow[0].id]\">\n            <option *ngFor=\"let actorFieldOption of actorFieldOptionsMap[actorFieldRow[0].id]\" [value]=\"actorFieldOption.fieldValue\">\n              {{ actorFieldOption.description }}\n            </option>\n          </select>\n        </div>\n      </div>\n      <div *ngIf=\"actorFieldRow[1]\" class=\"component-col\">\n        <div class=\"component-label\">\n          {{ actorFieldRow[1].description }}\n          <div *ngIf=\"actorFieldErrors[actorFieldRow[1].id]\" class=\"error-message\">\n            A value must be entered for {{ actorFieldRow[1].description }}\n          </div>\n        </div>\n        <div class=\"component\">\n          <input *ngIf=\"actorFieldRow[1].actorFieldType === 'TEXT'\" type=\"text\" [(ngModel)]=\"actorFieldValues[actorFieldRow[1].id]\">\n          <select *ngIf=\"actorFieldRow[1].actorFieldType === 'SELECT'\" [(ngModel)]=\"actorFieldValues[actorFieldRow[1].id]\">\n            <option *ngFor=\"let actorFieldOption of actorFieldOptionsMap[actorFieldRow[1].id]\" [value]=\"actorFieldOption.fieldValue\">\n              {{ actorFieldOption.description }}\n            </option>\n          </select>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!loading\" class=\"button-section\">\n    <div class=\"button\" (click)=\"completeStep()\">\n      <div class=\"button-text\">Submit</div>\n    </div>\n    <div class=\"button button-outline\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n  <div *ngIf=\"loading\" class=\"button-section\">\n    <div class=\"button button-disabled\">\n      <div class=\"button-text\">Submit</div>\n    </div>\n    <div class=\"button button-outline-disabled\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                styles: [".update-details{width:100%;height:100%;position:relative}.update-details .heading{position:absolute;top:0;width:100%;height:32px;font-size:16px;padding:8px}.update-details .content{position:absolute;top:32px;left:0;bottom:50px;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px;box-sizing:border-box}.update-details .content .info-message{font-size:15px}.update-details .content .component-row{width:100%}.update-details .content .component-row .component-col{width:100%;display:inline-block;box-sizing:border-box;padding-top:10px}.update-details .content .component-row .component-col .component-label{width:100%;font-size:15px;padding-bottom:5px}.update-details .content .component-row .component-col .error-message{font-style:italic;font-size:11px;color:#f04141;padding-bottom:5px}.update-details .content .component-row .component-col .component{width:100%}.update-details .content .component-row .component-col .component input,.update-details .content .component-row .component-col .component select{width:100%;font-size:13px;border:1px solid #58666c;background-color:#fffcf6;box-sizing:border-box;padding:3px 5px;border-radius:5px;height:30px}.update-details .content .component-row:first-child .component-col:first-child{padding-top:0}@media (min-width:600px){.update-details .content .component-row .component-col{width:50%}.update-details .content .component-row .component-col:first-child{padding-right:16px}.update-details .content .component-row:first-child .component-col{padding-top:0}}.update-details .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.update-details .button-section .button{width:120px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.update-details .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.update-details .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.update-details .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b;cursor:default}.update-details .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b;cursor:default}"]
            }] }
];
/** @nocollapse */
UpdateDetailsComponent.ctorParameters = () => [
    { type: ProjectService },
    { type: MaintenanceService },
    { type: ActorService }
];
UpdateDetailsComponent.propDecorators = {
    stepInstance: [{ type: Input }],
    userId: [{ type: Input }],
    stepCompletedListener: [{ type: Output }],
    cancelListener: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TextQuestionComponent {
    constructor() {
        this.close = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
}
TextQuestionComponent.decorators = [
    { type: Component, args: [{
                selector: 'text-question',
                template: "<!--<form class=\"form-horizontal\">\n    <div class=\"form-group\">\n        <label for=\"textQuestion\" class=\"col-sm-6 text-left\">Text question text</label>\n        <div class=\"col-sm-6\">\n            <input type=\"text\" class=\"form-control\" id=\"textQuestion\" placeholder=\"\">\n        </div>\n    </div>\n</form>-->\n\n<div class=\"row\">\n    <div class=\"col-md-6\"><label class=\"text-muted\">&#9679;&nbsp;&nbsp;Question text</label></div>\n    <div class=\"col-md-6\"><input type=\"text\" class=\"form-control\" id=\"exampleInputEmail1\" placeholder=\"\"></div>\n</div>\n"
            }] }
];
/** @nocollapse */
TextQuestionComponent.ctorParameters = () => [];
TextQuestionComponent.propDecorators = {
    close: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CheckboxComponent {
    constructor() {
        this.selectionListener = new EventEmitter();
        this.selected = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        // if (this.description) {
        //     if (this.description.toLowerCase() === "yes" || this.description.toLowerCase() === "no") {
        //         this.description = "";
        //     }
        // }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @return {?}
     */
    clicked() {
        this.selected = !this.selected;
        this.selectionListener.emit({
            value: this.selected
        });
    }
}
CheckboxComponent.decorators = [
    { type: Component, args: [{
                selector: 'checkbox-component',
                template: "<div class=\"checkbox-component\" (click)=\"clicked()\">\n  <div class=\"value\">\n    <div class=\"checkbox\">\n      <div *ngIf=\"selected\" class=\"checkbox-selected\">\n      </div>\n    </div>\n  </div>\n  <div class=\"description\">\n    {{ description }}\n  </div>\n</div>\n",
                styles: [".checkbox-component{display:table;margin:.5em 0;width:100%;cursor:pointer}.checkbox-component .value{display:table-cell;vertical-align:middle;text-align:left;width:32px;padding:3px}.checkbox-component .value .checkbox{border:1px solid #58666c;width:14px;height:14px;padding:2px;box-sizing:border-box}.checkbox-component .value .checkbox .checkbox-selected{background-color:#58666c;width:8px;height:8px;box-sizing:border-box}.checkbox-component .description{display:table-cell;vertical-align:middle;text-align:left;width:100%;font-size:15px;padding-left:5px;padding-top:3px;padding-right:5px;line-height:15px}"]
            }] }
];
/** @nocollapse */
CheckboxComponent.ctorParameters = () => [];
CheckboxComponent.propDecorators = {
    description: [{ type: Input }],
    selected: [{ type: Input }],
    selectionListener: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class RadioButtonComponent {
    constructor() {
        this.selectionListener = new EventEmitter();
        this.selected = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @return {?}
     */
    clicked() {
        this.selected = !this.selected;
        this.selectionListener.emit({
            value: this.selected
        });
    }
}
RadioButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'radio-button-component',
                template: "<div class=\"radio-button-component\" (click)=\"clicked()\">\n  <div class=\"value\">\n    <div class=\"radio-button\">\n      <div *ngIf=\"selected\" class=\"radio-button-selected\">\n      </div>\n    </div>\n  </div>\n  <div class=\"description\">\n    {{ description }}\n  </div>\n</div>\n",
                styles: [".radio-button-component{display:table;margin:.5em 0;width:100%;cursor:pointer}.radio-button-component .value{display:table-cell;vertical-align:top;text-align:left;width:32px;padding:3px}.radio-button-component .value .radio-button{border:1px solid #58666c;width:14px;height:14px;padding:2px;border-radius:7px;box-sizing:border-box}.radio-button-component .value .radio-button .radio-button-selected{background-color:#58666c;width:8px;height:8px;border-radius:4px;box-sizing:border-box}.radio-button-component .description{display:table-cell;vertical-align:top;text-align:left;width:100%;font-size:15px;padding-left:5px;padding-top:3px;padding-right:5px;line-height:15px}"]
            }] }
];
/** @nocollapse */
RadioButtonComponent.ctorParameters = () => [];
RadioButtonComponent.propDecorators = {
    description: [{ type: Input }],
    selected: [{ type: Input }],
    selectionListener: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SelectQuestionOption {
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class QuestionLineComponent {
    constructor() {
        this.onTakePictureQuestionLine = new EventEmitter();
        this.onTakeSupportingPictureQuestionLine = new EventEmitter();
        this.onQuestionAnswered = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (this.questionLine && this.questionLine.children) {
            for (let i = 0; i < this.questionLine.children.length; i++) {
                this.questionLine.children[i].parent = this.questionLine;
            }
            this.selectQuestionOptions = new Array();
            if (this.questionLine.parameters) {
                /** @type {?} */
                let rows = this.questionLine.parameters.split("|");
                /** @type {?} */
                let index = 0;
                for (let i = 0; i < rows.length; i++) {
                    /** @type {?} */
                    let s = rows[i].split("=");
                    if (s.length >= 2) {
                        /** @type {?} */
                        let selectQuestionOption = new SelectQuestionOption();
                        selectQuestionOption.description = s[1];
                        selectQuestionOption.weight = Number(s[2]);
                        selectQuestionOption.index = index;
                        this.selectQuestionOptions.push(selectQuestionOption);
                        index++;
                    }
                }
            }
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @param {?} $selected
     * @return {?}
     */
    radioButtonChanged($selected) {
        if (this.questionLine) {
            for (let i = 0; i < this.questionLine.parent.children.length; i++) {
                if (this.questionLine.parent.children[i].id !== this.questionLine.id) {
                    this.questionLine.parent.children[i].questionResponse.selected = false;
                }
            }
            if (!$selected) {
                $selected = {
                    value: false,
                };
            }
            this.questionLine.questionResponse.selected = $selected.value;
            this.questionLine.questionResponse.weight = this.questionLine.weight;
            if (this.questionLine.questionResponse.selected) {
                this.onQuestionAnswered.emit(this.questionLine);
            }
        }
    }
    /**
     * @param {?} $selected
     * @return {?}
     */
    checkboxChanged($selected) {
        if (!$selected) {
            $selected = {
                value: false,
            };
        }
        if (this.questionLine) {
            this.questionLine.questionResponse.selected = $selected.value;
            this.questionLine.questionResponse.weight = this.questionLine.weight;
            if (this.questionLine.questionResponse.selected) {
                this.onQuestionAnswered.emit(this.questionLine);
            }
        }
    }
    /**
     * @param {?} description
     * @return {?}
     */
    selectChanged(description) {
        if (this.questionLine) {
            /** @type {?} */
            let selectQuestionOption = null;
            for (let i = 0; i < this.selectQuestionOptions.length; i++) {
                if (this.selectQuestionOptions[i].description === description) {
                    selectQuestionOption = this.selectQuestionOptions[i];
                    break;
                }
            }
            // let selectQuestionOption = this.selectQuestionOptions[index];
            if (selectQuestionOption) {
                this.questionLine.questionResponse.selected = true;
                this.questionLine.questionResponse.weight = selectQuestionOption.weight;
                this.questionLine.questionResponse.response = selectQuestionOption.description;
                this.onQuestionAnswered.emit(this.questionLine);
            }
        }
    }
    /**
     * @param {?} questionLine
     * @return {?}
     */
    takePicture(questionLine) {
        this.onTakePictureQuestionLine.emit(questionLine);
    }
    /**
     * @param {?} questionLine
     * @return {?}
     */
    takeSupportingPicture(questionLine) {
        this.onTakeSupportingPictureQuestionLine.emit(questionLine);
    }
    /**
     * @param {?} questionLine
     * @return {?}
     */
    questionAnswered(questionLine) {
        console.log("questionLine.questionAnswered: " + questionLine.wording + " " + questionLine.integrationIndicator + " " + questionLine.id);
        this.onQuestionAnswered.emit(questionLine);
    }
}
QuestionLineComponent.decorators = [
    { type: Component, args: [{
                selector: 'question-line',
                template: "<div class=\"question-line\">\n  <div *ngIf=\"questionLine && questionLine.answerType === 'NONE'\" class=\"statement\">\n    <div class=\"wording\" [ngClass]=\"{ 'heading': questionLine.lineType === 'HEADING' }\">\n      {{ questionLine.wording }}\n    </div>\n    <div [hidden]=\"!questionLine.hasError\" class=\"error-text\">\n      This question must be answered\n    </div>\n    <div *ngFor=\"let child of questionLine.children\" class=\"answer-question-lines\">\n      <question-line [questionLine]=\"child\" (onTakePictureQuestionLine)=\"takePicture(child)\" (onQuestionAnswered)=\"questionAnswered($event)\"\n          (onTakeSupportingPictureQuestionLine)=\"takeSupportingPicture(child)\"></question-line>\n    </div>\n\n    <div *ngIf=\"questionLine && questionLine.allowSupportingDescription\" class=\"control-question-line supporting-question-line\">\n      <div class=\"wording\">\n        Please provide motivation for your choice\n      </div>\n      <textarea *ngIf=\"questionLine.allowSupportingDescription\" rows=\"5\" [(ngModel)]=\"questionLine.questionResponse.supportingDescription\" placeholder=\"Type here\">\n      </textarea>\n    </div>\n\n    <div *ngIf=\"questionLine && questionLine.allowSupportingImage\" class=\"image-question-line supporting-question-line\">\n      <div class=\"row\">\n        <div class=\"wording\">\n          Please take a picture to support your choice\n        </div>\n        <div class=\"button\" (click)=\"takeSupportingPicture(questionLine)\">\n            <img src=\"data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAABBBJREFUeJzt289rXUUUB/BP0qZJTdriomjophWXamKh7lyoCyGtWQiKrlxYa924caFS+ie40UbrX+APxF1F0OIriFttGiu1JG0XQkEQbCyJUnNdzAuWmPdm7rv3zX3EfOGQx5t7zznf7503c2bmZkizmMY5jPZ4/7d4Fmu1ZZQZX6OoaC9mz7omPKE6+QI/Y2fm3GvBd+oRoMDLmXOvjKPqI1/ght7HkewYwvfqFaDA6zlJVMFz6idf4CbuycijJ+zAT/ojQIG38lHpDS/pH/kCv2FfmYR2VCRUBiP4DPf2McZu3ME3fYzRM17T36e/bsvYn4lTMnbjF3kEKPBOHlrpeEM+8gVWcCALswTswa/yClDgbA5yKTgtP/kCf+GBWHJDJYiMYG/7byoO4kslp6YacU5YLa50aL/TTYBhPI3n8TgOtb/bSvikU8OM/lZsg2KtjevoEbyLkwnqbQncLcAIPsexhnJpBHf/ps/4n5HnXwFmcaLJRJrCsLAgGrjSMReGhaf/YNOJNIVhvJBw3SVhH2+8bcfwY825LONjvILHhBXdaNv2t787Iczdf9QZ+Kbuc+U8Jja5bw8WIvem2FUcV247a1wQY7Fi7JaEi2a6JPJMheAreFO50nojduFtrPZTgG5PZrzHwIuYqkB8Ix7FtR7yaEm4qJsAEz0Encf91Tn/B5PCWFW7AEe7BJ0tGXAR99XBtgMmlesJLQkXLQgD3kbsxeUSwVbU2+074bD0MaEl8cIFYcAbF7r9rHLkC2HAy4VTiTm1lCBQxa5KH+2n8AGu4M+2XcH7eCTRxy4sDZIAxxOSHsOHwssOnfysYU7aQeirgyLALfEiZ0w4zEj1eb59TzdMCBVj4wJ8FEmU8OTL+p1L8PvpIAgQ6/5Tunf7TraGhyO+Yz+DVo5NzouR9pPK7U6vY0ggWCV2ll3ea5H2Jyv4firSvhRzkEOAW5H2gxV8H4q0/x5zsNX2+UsjhwB7I+3XK/iOdfHoiVQOAWLd9HwF37F7o2eDOQSYjrSfFaakslgT6ocqsbMIEBup54XavyzmhEValdjofyG0LKwiu2FU6M6pPr8SFjzdMIHbET+tHAIU0g5dxoSn2q0q/Fs4u4yRJ+2dpGwCLCYmTShvzwin06ttu4z38FCij1FhdhkYAQph9zYXUt9KaSlBoKqtCru3/cYRYRNl4AQohHXBZL+YC2+G3SiRT0ufCW9ml/RHhAPCcV2ZXBoRYL0nHK6R/BHlnnzjAhTCmHBK+uywGUaFAS/1Nz9QAqzbkrCxsdkBbCdMCPP89YqxW0PtD4OA2/hCqAgvCsKsr+f3CQubaaG8nVHPP0dcGCQBmsCF7Q2RphNoGtsCNJ1A09gWoOkEmsZOYa1epgjZSvjhH9khIZLIyIO8AAAAAElFTkSuQmCC\">\n        </div>\n      </div>\n      <div class=\"image-answer\">\n        <img src=\"{{ questionLine.questionResponse.supportingImage }}\">\n      </div>\n    </div>\n  </div>\n\n  <div *ngIf=\"questionLine && questionLine.answerType === 'RADIO_BUTTON'\" class=\"select-question-line\">\n    <radio-button-component [description]=\"questionLine.wording\" [selected]=\"questionLine.questionResponse.selected\"\n        (selectionListener)=\"radioButtonChanged($event)\"></radio-button-component>\n    <ng-container *ngIf=\"questionLine.questionResponse.selected && questionLine.children.length > 0\">\n      <div *ngFor=\"let child of questionLine.children\" class=\"sub-question-lines\">\n        <question-line [questionLine]=\"child\" (onQuestionAnswered)=\"questionAnswered($event)\"></question-line>\n      </div>\n    </ng-container>\n  </div>\n\n  <div *ngIf=\"questionLine && questionLine.answerType === 'CHECK_BOX'\" class=\"select-question-line\">\n    <checkbox-component [description]=\"questionLine.wording\" [selected]=\"questionLine.questionResponse.selected\"\n        (selectionListener)=\"checkboxChanged($event)\"></checkbox-component>\n    <ng-container *ngIf=\"questionLine.questionResponse.selected && questionLine.children.length > 0\">\n      <div *ngFor=\"let child of questionLine.children\" class=\"sub-question-lines\">\n        <question-line [questionLine]=\"child\"></question-line>\n      </div>\n    </ng-container>\n  </div>\n\n  <div *ngIf=\"questionLine && questionLine.answerType === 'SELECT'\" class=\"control-question-line\">\n    <div class=\"wording\">\n      {{ questionLine.wording }}\n    </div>\n    <div [hidden]=\"!questionLine.hasError\" class=\"error-text\">\n      This question must be answered\n    </div>\n    <select [(ngModel)]=\"questionLine.questionResponse.response\" (change)=\"selectChanged($event.target.value)\">\n      <option *ngFor=\"let selectQuestionOption of selectQuestionOptions\" [value]=\"selectQuestionOption.description\">\n        {{ selectQuestionOption.description }}\n      </option>\n    </select>\n  </div>\n\n  <div *ngIf=\"questionLine && questionLine.answerType === 'TEXT'\" class=\"control-question-line\">\n    <div class=\"wording\">\n      {{ questionLine.wording }}\n    </div>\n    <div [hidden]=\"!questionLine.hasError\" class=\"error-text\">\n      This question must be answered\n    </div>\n    <input type=\"text\" [(ngModel)]=\"questionLine.questionResponse.response\" placeholder=\"Type here\">\n  </div>\n\n  <div *ngIf=\"questionLine && questionLine.answerType === 'TEXT_AREA'\" class=\"control-question-line\">\n    <div class=\"wording\">\n      {{ questionLine.wording }}\n    </div>\n    <div [hidden]=\"!questionLine.hasError\" class=\"error-text\">\n      This question must be answered\n    </div>\n    <textarea rows=\"5\" [(ngModel)]=\"questionLine.questionResponse.response\" placeholder=\"Type here\">\n    </textarea>\n  </div>\n\n  <div *ngIf=\"questionLine && questionLine.answerType === 'NUMBER'\" class=\"control-question-line\">\n    <div class=\"wording\">\n      {{ questionLine.wording }}\n    </div>\n    <div [hidden]=\"!questionLine.hasError\" class=\"error-text\">\n      This question must be answered\n    </div>\n    <input type=\"number\" [(ngModel)]=\"questionLine.questionResponse.response\" placeholder=\"Type here\">\n  </div>\n\n  <div *ngIf=\"questionLine && questionLine.answerType === 'IMAGE'\" class=\"image-question-line\">\n    <div class=\"row\">\n      <div class=\"wording\">\n        {{ questionLine.wording }}\n      </div>\n      <div class=\"button\" (click)=\"takePicture(questionLine)\">\n          <img src=\"data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAABBBJREFUeJzt289rXUUUB/BP0qZJTdriomjophWXamKh7lyoCyGtWQiKrlxYa924caFS+ie40UbrX+APxF1F0OIriFttGiu1JG0XQkEQbCyJUnNdzAuWmPdm7rv3zX3EfOGQx5t7zznf7503c2bmZkizmMY5jPZ4/7d4Fmu1ZZQZX6OoaC9mz7omPKE6+QI/Y2fm3GvBd+oRoMDLmXOvjKPqI1/ght7HkewYwvfqFaDA6zlJVMFz6idf4CbuycijJ+zAT/ojQIG38lHpDS/pH/kCv2FfmYR2VCRUBiP4DPf2McZu3ME3fYzRM17T36e/bsvYn4lTMnbjF3kEKPBOHlrpeEM+8gVWcCALswTswa/yClDgbA5yKTgtP/kCf+GBWHJDJYiMYG/7byoO4kslp6YacU5YLa50aL/TTYBhPI3n8TgOtb/bSvikU8OM/lZsg2KtjevoEbyLkwnqbQncLcAIPsexhnJpBHf/ps/4n5HnXwFmcaLJRJrCsLAgGrjSMReGhaf/YNOJNIVhvJBw3SVhH2+8bcfwY825LONjvILHhBXdaNv2t787Iczdf9QZ+Kbuc+U8Jja5bw8WIvem2FUcV247a1wQY7Fi7JaEi2a6JPJMheAreFO50nojduFtrPZTgG5PZrzHwIuYqkB8Ix7FtR7yaEm4qJsAEz0Encf91Tn/B5PCWFW7AEe7BJ0tGXAR99XBtgMmlesJLQkXLQgD3kbsxeUSwVbU2+074bD0MaEl8cIFYcAbF7r9rHLkC2HAy4VTiTm1lCBQxa5KH+2n8AGu4M+2XcH7eCTRxy4sDZIAxxOSHsOHwssOnfysYU7aQeirgyLALfEiZ0w4zEj1eb59TzdMCBVj4wJ8FEmU8OTL+p1L8PvpIAgQ6/5Tunf7TraGhyO+Yz+DVo5NzouR9pPK7U6vY0ggWCV2ll3ea5H2Jyv4firSvhRzkEOAW5H2gxV8H4q0/x5zsNX2+UsjhwB7I+3XK/iOdfHoiVQOAWLd9HwF37F7o2eDOQSYjrSfFaakslgT6ocqsbMIEBup54XavyzmhEValdjofyG0LKwiu2FU6M6pPr8SFjzdMIHbET+tHAIU0g5dxoSn2q0q/Fs4u4yRJ+2dpGwCLCYmTShvzwin06ttu4z38FCij1FhdhkYAQph9zYXUt9KaSlBoKqtCru3/cYRYRNl4AQohHXBZL+YC2+G3SiRT0ufCW9ml/RHhAPCcV2ZXBoRYL0nHK6R/BHlnnzjAhTCmHBK+uywGUaFAS/1Nz9QAqzbkrCxsdkBbCdMCPP89YqxW0PtD4OA2/hCqAgvCsKsr+f3CQubaaG8nVHPP0dcGCQBmsCF7Q2RphNoGtsCNJ1A09gWoOkEmsZOYa1epgjZSvjhH9khIZLIyIO8AAAAAElFTkSuQmCC\">\n      </div>\n    </div>\n\n    <div class=\"image-answer\">\n      <img src=\"{{ questionLine.questionResponse.response }}\">\n    </div>\n  </div>\n</div>\n",
                styles: [".question-line .statement{width:100%}.question-line .statement .wording{font-size:15px}.question-line .statement .heading{font-size:15px;font-weight:700}.question-line .statement .error-text{font-style:italic;font-size:11px;color:#f04141}.question-line .statement .answer-question-lines{width:100%}.question-line .sub-question-lines{padding-left:32px}.question-line .control-question-line{width:100%}.question-line .control-question-line .wording{width:100%;font-size:15px;padding-bottom:5px}.question-line .control-question-line .error-text{font-style:italic;font-size:11px;color:#f04141;padding-bottom:5px}.question-line .control-question-line input,.question-line .control-question-line select{width:100%;font-size:13px;border:1px solid #58666c;background-color:#fffcf6;box-sizing:border-box;padding:3px 5px;border-radius:5px;height:30px}.question-line .control-question-line textarea{width:100%;font-size:13px;border:1px solid #58666c;background-color:#fffcf6;padding:3px 5px;resize:none;box-sizing:border-box;border-radius:5px}.question-line .supporting-question-line{padding-bottom:20px}.question-line .image-question-line{width:100%}.question-line .image-question-line .row{display:table;width:100%}.question-line .image-question-line .row .wording{display:table-cell;width:90%;font-size:15px;padding-top:7px;vertical-align:top}.question-line .image-question-line .row .button{display:table-cell;width:32px;padding-top:7px;text-align:right;cursor:pointer;box-sizing:border-box}.question-line .image-question-line .row .button img{border:1px solid #58666c;padding:5px;border-radius:5px;box-sizing:border-box;width:24px;height:24px}.question-line .image-question-line .image-answer{width:90%;height:90%;padding:0;box-sizing:border-box;text-align:center;margin-left:auto;margin-right:auto;margin-bottom:8px}.question-line .image-question-line .image-answer img{max-width:100%;max-height:100%}"]
            }] }
];
/** @nocollapse */
QuestionLineComponent.ctorParameters = () => [];
QuestionLineComponent.propDecorators = {
    questionLine: [{ type: Input }],
    onTakePictureQuestionLine: [{ type: Output }],
    onTakeSupportingPictureQuestionLine: [{ type: Output }],
    onQuestionAnswered: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class QuestionResponse {
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class QuestionnaireComponent {
    /**
     * @param {?} projectService
     */
    constructor(projectService) {
        this.projectService = projectService;
        this.stepCompletedListener = new EventEmitter();
        this.cancelListener = new EventEmitter();
        this.onTakePicture = new EventEmitter();
        this.onQuestionAnswered = new EventEmitter();
        this.pageIndex = -1;
        this.loading = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.loading = true;
        this.projectService.getQuestionnaire(this.stepInstance.id).then((/**
         * @param {?} questionnaire
         * @return {?}
         */
        (questionnaire) => {
            this.questionnaire = questionnaire;
            this.pageIndex = 0;
            this.loading = false;
        }));
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.stepInstance && changes.stepInstance.previousValue && changes.stepInstance.previousValue.id &&
            changes.stepInstance.currentValue.id !== changes.stepInstance.previousValue.id) {
            this.pageIndex = -1;
            this.loading = true;
            this.projectService.getQuestionnaire(this.stepInstance.id).then((/**
             * @param {?} questionnaire
             * @return {?}
             */
            (questionnaire) => {
                this.questionnaire = questionnaire;
                this.pageIndex = 0;
                this.loading = false;
            }));
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @return {?}
     */
    previous() {
        if (this.pageIndex > 0) {
            //            let questionLine = this.questionnaire.questionLines[this.pageIndex];
            try {
                //                this.setAllValidationMessages(questionLine);
                //                this.validateQuestionLine(questionLine);
                this.pageIndex--;
                document.body.scrollTop = 0;
            }
            catch (error) {
                // console.log(error);
            }
        }
    }
    /**
     * @return {?}
     */
    next() {
        if (this.pageIndex < this.questionnaire.questionLines.length - 1) {
            /** @type {?} */
            let questionLine = this.questionnaire.questionLines[this.pageIndex];
            try {
                this.setAllValidationMessages(questionLine);
                this.validateQuestionLine(questionLine);
                this.pageIndex++;
                document.body.scrollTop = 0;
            }
            catch (error) {
                // console.log(error);
            }
        }
    }
    /**
     * @param {?} submit
     * @return {?}
     */
    save(submit) {
        if (!this.loading) {
            this.loading = true;
            /** @type {?} */
            let responses = new Array();
            for (let i = 0; i < this.questionnaire.questionLines.length; i++) {
                this.addResponses(this.questionnaire.questionLines[i], responses);
            }
            this.projectService.updateQuestionnaire(this.stepInstance.id, responses, submit, this.userId).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                if (submit) {
                    this.pageIndex = 0;
                }
                this.stepCompletedListener.emit({
                    value: this.stepInstance
                });
                this.loading = false;
            }));
        }
    }
    /**
     * @return {?}
     */
    cancel() {
        this.cancelListener.emit({});
    }
    /**
     * @private
     * @param {?} questionLine
     * @param {?} responses
     * @return {?}
     */
    addResponses(questionLine, responses) {
        if (!questionLine)
            return;
        if (questionLine.answerType === 'CHECK_BOX' || questionLine.answerType === 'RADIO_BUTTON') {
            /** @type {?} */
            let questionResponse = new QuestionResponse();
            questionResponse.questionLineId = questionLine.id;
            questionResponse.weight = questionLine.weight;
            questionResponse.selected = questionLine.questionResponse.selected;
            responses.push(questionResponse);
        }
        else if (questionLine.answerType === 'TEXT' || questionLine.answerType === 'TEXT_AREA' ||
            questionLine.answerType === 'NUMBER' || questionLine.answerType === 'IMAGE') {
            /** @type {?} */
            let questionResponse = new QuestionResponse();
            questionResponse.questionLineId = questionLine.id;
            questionResponse.weight = questionLine.weight;
            questionResponse.response = questionLine.questionResponse.response;
            responses.push(questionResponse);
        }
        else if (questionLine.answerType === 'SELECT') {
            /** @type {?} */
            let questionResponse = new QuestionResponse();
            questionResponse.questionLineId = questionLine.id;
            questionResponse.weight = questionLine.questionResponse.weight;
            if (questionLine.questionResponse.weight) {
                questionResponse.selected = true;
                questionResponse.response = questionLine.questionResponse.response;
            }
            responses.push(questionResponse);
        }
        else if ((questionLine.lineType === 'SINGLE_SELECT' || questionLine.lineType === 'MULTI_SELECT') &&
            (questionLine.allowSupportingDescription || questionLine.allowSupportingImage)) {
            /** @type {?} */
            let questionResponse = new QuestionResponse();
            questionResponse.questionLineId = questionLine.id;
            questionResponse.supportingDescription = questionLine.questionResponse.supportingDescription;
            questionResponse.supportingImage = questionLine.questionResponse.supportingImage;
            responses.push(questionResponse);
        }
        for (let i = 0; i < questionLine.children.length; i++) {
            this.addResponses(questionLine.children[i], responses);
        }
    }
    ;
    /**
     * @private
     * @param {?} questionLine
     * @return {?}
     */
    validateQuestionLine(questionLine) {
        if (!this.hasValidAnswer(questionLine))
            throw "Invalid response";
        if (questionLine.children) {
            for (var i = 0; i < questionLine.children.length; i++) {
                /** @type {?} */
                var child = questionLine.children[i];
                if (child.answerType === "CHECK_BOX" || child.answerType === "RADIO_BUTTON") {
                    /** @type {?} */
                    var childResponse = questionLine.questionResponse;
                    if (childResponse && childResponse.selected) {
                        this.validateQuestionLine(child);
                    }
                }
                else {
                    this.validateQuestionLine(child);
                }
            }
        }
    }
    /**
     * @private
     * @param {?} questionLine
     * @return {?}
     */
    hasValidAnswer(questionLine) {
        if (!questionLine.lineType || !questionLine.answerType)
            return true;
        if (questionLine.lineType === "MULTI_SELECT" || questionLine.lineType === "SINGLE_SELECT") {
            if (questionLine.required && questionLine.children) {
                for (var i = 0; i < questionLine.children.length; i++) {
                    if (questionLine.children[i].questionResponse && questionLine.children[i].questionResponse.selected)
                        return true;
                }
                return false;
            }
            else {
                return true;
            }
        }
        else if (questionLine.answerType === "NUMBER" || questionLine.answerType === "SELECT" ||
            questionLine.answerType === "TEXT" || questionLine.answerType === "TEXT_AREA" || questionLine.answerType === "IMAGE") {
            if (questionLine.required && (!questionLine.questionResponse || !questionLine.questionResponse.response))
                return false;
            if (questionLine.answerType === "NUMBER") {
                if ((questionLine.required && isNaN(parseInt(questionLine.questionResponse.response))) ||
                    (!questionLine.required && questionLine.questionResponse.response && isNaN(parseInt(questionLine.questionResponse.response)))) {
                    return false;
                }
                try {
                    /** @type {?} */
                    var d = parseFloat(questionLine.questionResponse.response);
                    /** @type {?} */
                    var p = questionLine.parameters.split("|");
                    if (p.length >= 4) {
                        /** @type {?} */
                        var hasLimits = "true" === p[1];
                        /** @type {?} */
                        var max = parseFloat(p[2]);
                        /** @type {?} */
                        var min = parseFloat(p[3]);
                        if (hasLimits && (d < min || d > max) && !questionLine.errorAccepted)
                            return false;
                    }
                }
                catch (error) {
                    return false;
                }
            }
        }
        return true;
    }
    /**
     * @private
     * @param {?} questionLine
     * @return {?}
     */
    setAllValidationMessages(questionLine) {
        this.setValidationMessage(questionLine);
        if (questionLine.children) {
            for (var i = 0; i < questionLine.children.length; i++) {
                /** @type {?} */
                var child = questionLine.children[i];
                if (child.answerType === "CHECK_BOX" || child.answerType === "RADIO_BUTTON") {
                    /** @type {?} */
                    var childResponse = child.questionResponse;
                    if (childResponse && childResponse.selected) {
                        this.setAllValidationMessages(child);
                    }
                }
                else {
                    this.setAllValidationMessages(child);
                }
            }
        }
    }
    /**
     * @private
     * @param {?} questionLine
     * @return {?}
     */
    setValidationMessage(questionLine) {
        if (!questionLine.lineType || !questionLine.answerType) {
            questionLine.hasError = false;
            questionLine.errorMessage = null;
            questionLine.showErrorConfirmation = false;
            return;
        }
        if (questionLine.lineType === "MULTI_SELECT" || questionLine.lineType === "SINGLE_SELECT") {
            if (questionLine.required && questionLine.children) {
                /** @type {?} */
                var selected = false;
                for (var i = 0; i < questionLine.children.length; i++) {
                    if (questionLine.children[i].questionResponse && questionLine.children[i].questionResponse.selected) {
                        selected = true;
                        break;
                    }
                }
                if (!selected) {
                    questionLine.hasError = true;
                    questionLine.showErrorConfirmation = false;
                    questionLine.errorMessage = "An answer must be selected";
                    return;
                }
            }
        }
        else if (questionLine.answerType === "NUMBER" || questionLine.answerType === "SELECT" ||
            questionLine.answerType === "TEXT" || questionLine.answerType === "TEXT_AREA" ||
            questionLine.answerType === "IMAGE") {
            /** @type {?} */
            var questionResponse = questionLine.questionResponse;
            if (questionLine.required && (!questionResponse || !questionResponse.response)) {
                questionLine.hasError = true;
                questionLine.showErrorConfirmation = false;
                questionLine.errorMessage = "An answer must be provided";
                return;
            }
            if (questionLine.answerType === "NUMBER") {
                if ((questionLine.required && isNaN(parseInt(questionResponse.response))) ||
                    (!questionLine.required && questionResponse.response && isNaN(parseInt(questionResponse.response)))) {
                    questionLine.hasError = true;
                    questionLine.showErrorConfirmation = false;
                    questionLine.errorMessage = "An invalid number was entered";
                    return;
                }
                try {
                    /** @type {?} */
                    var d = parseFloat(questionResponse.response);
                    /** @type {?} */
                    var p = questionLine.parameters.split("|");
                    if (p.length >= 4) {
                        /** @type {?} */
                        var hasLimits = "true" === p[1];
                        /** @type {?} */
                        var max = parseFloat(p[2]);
                        /** @type {?} */
                        var min = parseFloat(p[3]);
                        if (hasLimits) {
                            if (d < min) {
                                questionLine.hasError = true;
                                questionLine.showErrorConfirmation = true;
                                questionLine.errorMessage = "The value provided is less than the minimum value of " + min + ".  Are you sure this is correct?";
                                return;
                            }
                            else if (d > max) {
                                questionLine.hasError = true;
                                questionLine.showErrorConfirmation = true;
                                questionLine.errorMessage = "The value provided exceeds the maximum value of " + max + ".  Are you sure this is correct?";
                                return;
                            }
                        }
                    }
                }
                catch (error) {
                    questionLine.hasError = true;
                    questionLine.showErrorConfirmation = false;
                    questionLine.errorMessage = "An invalid number was supplied";
                    return;
                }
            }
        }
        questionLine.hasError = false;
        questionLine.showErrorConfirmation = false;
        questionLine.errorMessage = "";
    }
    /**
     * @param {?} questionLine
     * @return {?}
     */
    takePictureQuestionnaire(questionLine) {
        /** @type {?} */
        let imageContainer = {
            /**
             * @param {?} image
             * @return {?}
             */
            setImage(image) {
                questionLine.questionResponse.response = "data:image/jpeg;base64," + image;
            }
        };
        this.onTakePicture.emit(imageContainer);
    }
    /**
     * @param {?} questionLine
     * @return {?}
     */
    takeSupportingPictureQuestionnaire(questionLine) {
        /** @type {?} */
        let imageContainer = {
            /**
             * @param {?} image
             * @return {?}
             */
            setImage(image) {
                questionLine.questionResponse.supportingImage = "data:image/jpeg;base64," + image;
            }
        };
        this.onTakePicture.emit(imageContainer);
    }
    /**
     * @param {?} questionLine
     * @return {?}
     */
    questionAnswered(questionLine) {
        console.log("questionnaire.questionAnswered: " + questionLine.wording + " " + questionLine.integrationIndicator + " " + questionLine.id);
        this.onQuestionAnswered.emit(questionLine);
    }
}
QuestionnaireComponent.decorators = [
    { type: Component, args: [{
                selector: 'questionnaire',
                template: "<div class=\"questionnaire\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div *ngIf=\"questionnaire\" class=\"heading\">\n    {{ questionnaire.description }}\n    <span class=\"sub-heading\">\n      (Page {{ (pageIndex + 1) }} of {{ this.questionnaire.questionLines.length }})\n    </span>\n  </div>\n  <!-- <loading-indicator [show]=\"loading\"></loading-indicator> -->\n  <!-- <div *ngIf=\"questionnaire\">\n    <div class=\"row\">\n      <div class=\"col-md-12\">\n        <h2 *ngIf=\"wizard\">{{questionnaire.description}}</h2>\n        <h3 *ngIf=\"!wizard\">{{questionnaire.description}}</h3>\n      </div>\n    </div> -->\n\n  <div *ngIf=\"questionnaire && questionnaire.questionLines\" class=\"questionnaire-content\">\n    <question-line [questionLine]=\"questionnaire.questionLines[pageIndex]\" (onTakePictureQuestionLine)=\"takePictureQuestionnaire($event)\"\n        (onQuestionAnswered)=\"questionAnswered($event)\" (onTakeSupportingPictureQuestionLine)=\"takeSupportingPictureQuestionnaire($event)\"></question-line>\n  </div>\n\n  <div class=\"button-section\">\n    <div *ngIf=\"questionnaire && questionnaire.questionLines && pageIndex >= questionnaire.questionLines.length - 1\"\n        class=\"button\" [ngClass]=\"{ 'button-disabled': loading }\" (click)=\"save(true)\">\n      <div class=\"button-text\">Submit</div>\n    </div>\n    <div *ngIf=\"questionnaire && questionnaire.questionLines && pageIndex < questionnaire.questionLines.length - 1\"\n        class=\"button button-outline\" [ngClass]=\"{ 'button-outline-disabled': loading }\" (click)=\"save(false)\">\n      <div class=\"button-text\">Save</div>\n    </div>\n\n    <div *ngIf=\"questionnaire && questionnaire.questionLines && questionnaire.questionLines.length > 1\"\n        class=\"button image-button\" [ngClass]=\"{ 'button-disabled': pageIndex >= questionnaire.questionLines.length - 1 || loading }\" (click)=\"next()\">\n      <div class=\"button-text\">\n          <img src=\"data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAUVJREFUeJzt2EtOw0AURNELbCw7o9hZdgaTtBigBLv9+vM+JfUscnyPZw220+OknIDvx9HSN1kw8RufDkH8jU+DIJ7Hh0cQ/8eHRRDH48MhiPPxYRBEf7x7BHE93i2CsIt3hyDs47dBeF/8/58sRvg48Js78AbcBr3D7fH8+6Dnv9wRAAiMcBQAgiKcAYCACGcBIBhCDwAEQugFgCAIVwAgAMJVAHCOYAEAjhGsAMApgiUAOESwBgBnCCMAwBHCKABwgjASABwgjAaAzRFmAMDGCLMAYFOE1XeCqSbG3S5vccP8aqLiK77ijY9mhfRMVHzFV7zx0ayQnomKr/iKNz6aFdIzUfEVX/      HGR7NCeiYqPmb86jvBLzb/+m0i2JfvmUgc3yYSx7eJxPFtInF8m0gc3yYSx7eJxPFtInF8m0gc3yacxf8AAP323bWbSe8AAAAASUVORK5CYII=\">\n      </div>\n    </div>\n    <div *ngIf=\"questionnaire && questionnaire.questionLines && questionnaire.questionLines.length > 1\"\n        class=\"button image-button\" (click)=\"previous()\">\n      <div class=\"button-text\">\n          <img src=\"data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAUlJREFUeJzt2k1uwjAUReFTujF2xu3O2Fk7icUAtU2cZ8fv50qeoSTnQ0wiYO1pOykn4Hs7uvRJLph4xadDEO/xaRDE7/HhEcT/8WERxP74cAjieHwYBNEf7x5BnI93iyDs4t0hCPv4boTbiZCeCXhMvucyE+O++eV/AqLiK77ijY9mhfRMVHzFV7zx0ayQnomKr/iKNz6aFdIzUfEVX/HGR7NCeiYqPmb87HeCy+1zx2eewAdwH/QM9+36z0HX/3N7ACAwwl4ACIpwBAACIhwFgGAIPQAQCKEXAIIgnAGAAAhnAcA5ggUAOEawAgCnCJYA4BDBGgCcIYwAAEcIowDACcJIAHCAMBoAFkeYAQALI8wCgEURrvif4Nfkey45Eeztcs9E4vg2kTi+TSSObxOJ49tE4vg2kTi+TSSObxOJ49tE4vg2MTj+B2+f9t1j5qkYAAAAAElFTkSuQmCC\">\n      </div>\n    </div>\n\n    <div class=\"button button-outline\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                styles: [".questionnaire{width:100%;height:100%;max-height:100%;position:relative}.questionnaire .heading{position:absolute;top:0;width:100%;height:32px;font-size:16px;padding:8px}.questionnaire .heading .sub-heading{font-size:12px;font-style:italic}.questionnaire .questionnaire-content{position:absolute;top:32px;left:0;bottom:50px;width:100%;overflow-y:scroll;padding:8px;box-sizing:border-box;background-color:#f9f9f9}.questionnaire .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.questionnaire .button-section .button{width:70px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.questionnaire .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.questionnaire .button-section .button .button-text img{padding-left:4px;padding-top:4px;width:16px;height:16px}.questionnaire .button-section .image-button{width:36px;border:1px solid #2b4054;background-color:#fff;color:#2b4054}.questionnaire .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.questionnaire .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b}.questionnaire .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b}"]
            }] }
];
/** @nocollapse */
QuestionnaireComponent.ctorParameters = () => [
    { type: ProjectService }
];
QuestionnaireComponent.propDecorators = {
    stepInstance: [{ type: Input }],
    userId: [{ type: Input }],
    wizard: [{ type: Input }],
    newPagePerSubquestion: [{ type: Input }],
    stepCompletedListener: [{ type: Output }],
    cancelListener: [{ type: Output }],
    onTakePicture: [{ type: Output }],
    onQuestionAnswered: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SelectRole2Component {
    /**
     * @param {?} projectService
     */
    constructor(projectService) {
        this.projectService = projectService;
        this.stepCompletedListener = new EventEmitter();
        this.bookedTimeSlots = [];
        this.newTimeSlots = [];
        this.showCalendar = false;
        this.showSelectedProviderError = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.stepInstance && this.stepInstance.step.parameters["0"]) {
            this.showWaitIndicator = true;
            /** @type {?} */
            let projectId = this.stepInstance.businessProcessInstance.businessProcess.project.id;
            /** @type {?} */
            let projectRoleId = this.stepInstance.step.parameters["0"].parameterValue;
            this.projectService.getProjectActors2(0, 100, projectId, projectRoleId).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                this.tableDataPage = result;
                this.projectService.getProcessVariableValue(this.stepInstance.businessProcessInstance.id, "providerId").then((/**
                 * @param {?} providerId
                 * @return {?}
                 */
                providerId => {
                    if (providerId) {
                        for (let i = 0; i < this.tableDataPage.list.length; i++) {
                            if (Number(providerId) === this.tableDataPage.list[i].actor.id) {
                                this.provider = this.tableDataPage.list[i].actor;
                                break;
                            }
                        }
                    }
                    this.showWaitIndicator = false;
                }));
            }));
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @param {?} provider
     * @return {?}
     */
    selectProvider(provider) {
        if (provider) {
            this.provider = provider.actor;
        }
        else {
            this.provider = null;
        }
    }
    /**
     * @return {?}
     */
    continueWithSelectedProvider() {
        this.showSelectedProviderError = false;
        if (!this.provider) {
            this.showSelectedProviderError = true;
            return;
        }
        this.showWaitIndicator = true;
        this.projectService.setProcessVariableValue(this.stepInstance.businessProcessInstance.id, "providerId", this.provider.id.toString()).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            if (this.makeBooking) {
                this.projectService.getTimeSlots(null, this.provider.id, null).then((/**
                 * @param {?} result
                 * @return {?}
                 */
                result => {
                    this.bookedTimeSlots = result;
                    this.showCalendar = true;
                    this.showWaitIndicator = false;
                }));
            }
            else {
                this.completeStep();
            }
        }));
    }
    /**
     * @param {?} event
     * @return {?}
     */
    timeSlotSelected(event) {
        this.newTimeSlots = event.value;
    }
    /**
     * @return {?}
     */
    updateTimeSlots() {
        if (this.newTimeSlots) {
            this.showWaitIndicator = true;
            this.projectService.updateTimeSlots(this.newTimeSlots).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                this.completeStep();
            }));
        }
    }
    /**
     * @private
     * @return {?}
     */
    completeStep() {
        this.showWaitIndicator = true;
        this.projectService.completeStep(this.stepInstance.id, this.userId).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            this.stepCompletedListener.emit({
                value: this.stepInstance
            });
            this.showWaitIndicator = false;
        }));
    }
}
SelectRole2Component.decorators = [
    { type: Component, args: [{
                selector: 'select-role2',
                template: "<div class=\"select-role2\">\n  <loading-indicator [show]=\"showWaitIndicator\"></loading-indicator>\n  <div *ngIf=\"!showCalendar\"class=\"heading\">\n    Select Provider\n  </div>\n  <div *ngIf=\"showCalendar\"class=\"heading\">\n    Make Appointment with {{ provider.title.description }} {{ provider.firstname }} {{ provider.surname }}\n  </div>\n\n  <div *ngIf=\"!showCalendar\" class=\"content\">\n    <div class=\"col-md-12\">\n      <table class=\"provider-table\">\n        <tr>\n          <th>Title</th>\n          <th>Firstname</th>\n          <th>Surname</th>\n          <th>E-mail</th>\n          <th>Contact Nrs</th>\n          <th>Address</th>\n        </tr>\n        <tbody *ngIf=\"tableDataPage\">\n          <tr *ngFor=\"let projectActor of tableDataPage.list\" (click)=\"selectProvider(projectActor)\">\n            <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n              <span *ngIf=\"projectActor.actor.title\">{{ projectActor.actor.title[\"description\"] }}</span>\n            </td>\n            <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n              {{ projectActor.actor.firstname }}\n            </td>\n            <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n              {{ projectActor.actor.surname }}\n            </td>\n            <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n              {{ projectActor.actor.email }}\n            </td>\n            <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n              <div *ngIf=\"projectActor.actor.celNr\">Cel: {{ projectActor.actor.celNr }}</div>\n              <div *ngIf=\"projectActor.actor.homeTelNr\">Home: {{ projectActor.actor.homeTelNr }}</div>\n              <div *ngIf=\"projectActor.actor.workTelNr\">Work: {{ projectActor.actor.workTelNr }}</div>\n            </td>\n            <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n              <div *ngIf=\"projectActor.actor.physicalAddress1\">{{ projectActor.actor.physicalAddress1 }}</div>\n              <div *ngIf=\"projectActor.actor.physicalAddress2\">{{ projectActor.actor.physicalAddress2 }}</div>\n              <div *ngIf=\"projectActor.actor.physicalAddress3\">{{ projectActor.actor.physicalAddress3 }}</div>\n              <div *ngIf=\"projectActor.actor.physicalAddress4\">{{ projectActor.actor.physicalAddress4 }}</div>\n            </td>\n          </tr>\n        </tbody>\n      </table>\n    </div>\n  </div>\n  <div *ngIf=\"!showCalendar\" class=\"button-section\">\n    <div *ngIf=\"provider\" class=\"button\" (click)=\"continueWithSelectedProvider()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div *ngIf=\"!provider\" class=\"button button-disabled\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n  </div>\n\n  <div *ngIf=\"showCalendar\" class=\"content\">\n    Calendar\n  </div>\n  <div *ngIf=\"showCalendar\" class=\"button-section\">\n    <div *ngIf=\"provider\" class=\"button\" (click)=\"updateTimeSlots()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div *ngIf=\"!provider\" class=\"button button-disabled\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n  </div>\n</div>\n\n\n<!--\n<loading-indicator [show]=\"showWaitIndicator\"></loading-indicator>\n\n<div *ngIf=\"!showCalendar\" class=\"row\">\n    <div class=\"col-md-12\">\n        <h4 class=\"control-label\">\n            Select Provider\n        </h4>\n    </div>\n</div>\n\n<div *ngIf=\"!showCalendar\" class=\"row\">\n    <div class=\"col-md-12\">\n        <table class=\"table table-hover table-bordered\">\n            <tr>\n                <th>Title</th>\n                <th>Firstname</th>\n                <th>Surname</th>\n                <th>E-mail</th>\n                <th>Contact Nrs</th>\n                <th>Address</th>\n            </tr>\n            <tbody *ngIf=\"tableDataPage\">\n                <tr *ngFor=\"let projectActor of tableDataPage.list\" (click)=\"selectProvider(projectActor)\">\n                    <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n                        <span *ngIf=\"projectActor.actor.title\">{{ projectActor.actor.title[\"description\"] }}</span>\n                    </td>\n                    <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n                        {{ projectActor.actor.firstname }}\n                    </td>\n                    <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n                        {{ projectActor.actor.surname }}\n                    </td>\n                    <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n                        {{ projectActor.actor.email }}\n                    </td>\n                    <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n                        <div *ngIf=\"projectActor.actor.celNr\">Cel: {{ projectActor.actor.celNr }}</div>\n                        <div *ngIf=\"projectActor.actor.homeTelNr\">Home: {{ projectActor.actor.homeTelNr }}</div>\n                        <div *ngIf=\"projectActor.actor.workTelNr\">Work: {{ projectActor.actor.workTelNr }}</div>\n                    </td>\n                    <td [ngClass]=\"{'info': provider != null && provider.id === projectActor.actor.id }\">\n                        <div *ngIf=\"projectActor.actor.physicalAddress1\">{{ projectActor.actor.physicalAddress1 }}</div>\n                        <div *ngIf=\"projectActor.actor.physicalAddress2\">{{ projectActor.actor.physicalAddress2 }}</div>\n                        <div *ngIf=\"projectActor.actor.physicalAddress3\">{{ projectActor.actor.physicalAddress3 }}</div>\n                        <div *ngIf=\"projectActor.actor.physicalAddress4\">{{ projectActor.actor.physicalAddress4 }}</div>\n                    </td>\n                </tr>\n            </tbody>\n        </table>\n    </div>\n</div>\n\n<div *ngIf=\"!showCalendar && showSelectedProviderError\" class=\"row\">\n    <div class=\"col-md-12\">\n        <div class=\"alert alert-danger\" role=\"alert\">A provider must be selected</div>\n    </div>\n</div>\n\n<div *ngIf=\"!showCalendar\" class=\"row\">\n    <div class=\"col-md-12\">\n        <button type=\"button\" class=\"btn btn-default\" (click)=\"continueWithSelectedProvider()\"\n                [ngClass]=\"{ 'disabled': !provider }\">\n            Continue\n        </button>\n    </div>\n</div>\n\n<div *ngIf=\"showCalendar\" class=\"row\">\n    <div class=\"col-md-12\">\n        <h4 class=\"control-label\">\n            Make Appointment with {{ provider.title.description }} {{ provider.firstname }} {{ provider.surname }}\n        </h4>\n    </div>\n</div>\n\n<div *ngIf=\"provider && showCalendar\" class=\"row\">\n    <div class=\"col-md-12\">\n        <h4 class=\"control-label\">\n            <calendar-control [showAddButton]=\"true\" [provider]=\"provider\" [participant]=\"stepInstance.businessProcessInstance.actor\"\n                [appointments]=\"bookedTimeSlots\" (selectionListener)=\"timeSlotSelected($event)\"></calendar-control>\n        </h4>\n    </div>\n</div>\n\n<div *ngIf=\"provider && showCalendar\" class=\"row\">\n    <div class=\"col-md-12\">\n        <button type=\"button\" class=\"btn btn-default\" (click)=\"updateTimeSlots()\">\n            Continue\n        </button>\n    </div>\n</div> -->\n",
                styles: [".select-role2{width:100%;height:100%;position:relative}.select-role2 .heading{position:absolute;top:0;width:100%;height:32px;font-size:16px;padding:8px}.select-role2 .top-padding{padding-top:8px}.select-role2 .content{position:absolute;top:32px;left:0;bottom:50px;width:100%;overflow-y:scroll;padding:8px;box-sizing:border-box;background-color:#f9f9f9;font-size:15px}.select-role2 .content .wording{width:100%;font-size:15px;padding-bottom:5px}.select-role2 .content input{width:200;font-size:12px;border:1px solid #58666c;background-color:#fffcf6;box-sizing:border-box;padding:3px 5px;margin-bottom:5px}.select-role2 .error-text{font-style:italic;font-size:11px;color:#f04141}.select-role2 .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.select-role2 .button-section .button{width:120px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.select-role2 .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.select-role2 .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.select-role2 .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b;cursor:default}.select-role2 .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b;cursor:default}"]
            }] }
];
/** @nocollapse */
SelectRole2Component.ctorParameters = () => [
    { type: ProjectService }
];
SelectRole2Component.propDecorators = {
    makeBooking: [{ type: Input }],
    stepInstance: [{ type: Input }],
    userId: [{ type: Input }],
    stepCompletedListener: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const moment = momentImported;
class WaitComponent {
    /**
     * @param {?} projectService
     */
    constructor(projectService) {
        this.projectService = projectService;
        this.stepCompletedListener = new EventEmitter();
        this.cancelListener = new EventEmitter();
        this.showOverride = false;
        this.loading = false;
    }
    /**
     * @param {?} s
     * @return {?}
     */
    set stepInstance(s) {
        this._stepInstance = s;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.projectService.getStepParameters(this._stepInstance.step.id).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            this.showOverride = result[2] && "true" === result[2];
            /** @type {?} */
            let messageParameter = result[3];
            if (messageParameter) {
                if (this._stepInstance.wakeUpDate && messageParameter.indexOf("${completionDate}") > 0) {
                    this.message = messageParameter.replace("${completionDate}", moment(this._stepInstance.wakeUpDate).format("YYYY-MM-DD HH:mm:ss"));
                }
                else {
                    this.message = messageParameter;
                }
            }
            else {
                if (this._stepInstance.wakeUpDate) {
                    this.message = "The process is currently waiting.  It will continue on " + moment(this._stepInstance.wakeUpDate).format("YYYY-MM-DD HH:mm:ss") + ".";
                }
                else {
                    this.message = "The process is currently waiting.";
                }
            }
        }));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @return {?}
     */
    completeStep() {
        this.loading = true;
        this.projectService.completeStep(this._stepInstance.id, this.userId).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            this.loading = false;
            this.stepCompletedListener.emit({
                value: this._stepInstance
            });
        }));
    }
    /**
     * @return {?}
     */
    cancel() {
        this.cancelListener.emit({});
    }
}
WaitComponent.decorators = [
    { type: Component, args: [{
                selector: 'wait',
                template: "<div class=\"wait-step\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div class=\"message-content\">\n    {{ message }}\n  </div>\n\n  <div *ngIf=\"!loading\" class=\"button-section\">\n    <div *ngIf=\"showOverride\" class=\"button\" (click)=\"completeStep()\">\n      <div class=\"button-text\">Override</div>\n    </div>\n    <div class=\"button button-outline\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n  <div *ngIf=\"loading\" class=\"button-section\">\n    <div *ngIf=\"showOverride\" class=\"button button-disabled\">\n      <div class=\"button-text\">Override</div>\n    </div>\n    <div class=\"button button-outline-disabled\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                styles: [".wait-step{width:100%;height:100%;position:relative}.wait-step .message-content{position:absolute;top:0;left:0;bottom:50px;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px;box-sizing:border-box}.wait-step .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.wait-step .button-section .button{width:120px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.wait-step .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.wait-step .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.wait-step .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b;cursor:default}.wait-step .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b;cursor:default}"]
            }] }
];
/** @nocollapse */
WaitComponent.ctorParameters = () => [
    { type: ProjectService }
];
WaitComponent.propDecorators = {
    _stepInstance: [{ type: Input }],
    userId: [{ type: Input }],
    stepCompletedListener: [{ type: Output }],
    cancelListener: [{ type: Output }],
    stepInstance: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FacialAuthenticationRefComponent {
    /**
     * @param {?} projectService
     */
    constructor(projectService) {
        this.projectService = projectService;
        this.onOpenCameraListener = new EventEmitter();
        this.stepCompletedListener = new EventEmitter();
        this.cancelListener = new EventEmitter();
        this.loading = false;
        this.imageContainer = {
            imageData: null,
            /**
             * @param {?} image
             * @return {?}
             */
            setImage(image) {
                console.log("facial-authentication-ref image received");
                this.imageData = image;
            }
        };
    }
    /**
     * @param {?} s
     * @return {?}
     */
    set stepInstance(s) {
        this._stepInstance = s;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (!this.uploadText) {
            this.uploadText = "Open camera";
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @return {?}
     */
    openCamera() {
        this.onOpenCameraListener.emit(this.imageContainer);
    }
    /**
     * @return {?}
     */
    retake() {
        this.imageContainer.imageData = null;
        this.onOpenCameraListener.emit(this.imageContainer);
    }
    /**
     * @return {?}
     */
    completeStep() {
        this.loading = true;
        console.log("completing facial-authentication-ref");
        this.projectService.updateFacialAuthenticationReference(this._stepInstance.id, this.imageContainer.imageData).then((/**
         * @param {?} imageUpdateResult
         * @return {?}
         */
        imageUpdateResult => {
            console.log("facial-authentication-ref update succeeded");
            this.projectService.completeStep(this._stepInstance.id, this.userId).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                console.log("facial-authentication-ref completion succeeded");
                this.loading = false;
                this.stepCompletedListener.emit({
                    value: this._stepInstance
                });
            })).catch((/**
             * @param {?} error
             * @return {?}
             */
            error => {
                console.log("facial-authentication-ref completion failed: " + error);
                this.loading = false;
                this.stepCompletedListener.emit({
                    value: this._stepInstance
                });
            }));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            console.log("facial-authentication-ref update failed: " + error);
        }));
    }
    /**
     * @return {?}
     */
    cancel() {
        this.cancelListener.emit({});
    }
}
FacialAuthenticationRefComponent.decorators = [
    { type: Component, args: [{
                selector: 'facial-authentication-ref',
                template: "<div class=\"facial-authentication-ref-step\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div *ngIf=\"!imageContainer.imageData\" class=\"message-content\">\n    Please provide a reference image that will be used for future facial authentication.\n  </div>\n  <div *ngIf=\"imageContainer.imageData\" class=\"image-content\">\n    <img src=\"{{ 'data:image/jpeg;base64,' + imageContainer.imageData }}\">\n  </div>\n\n  <div *ngIf=\"!loading\" class=\"button-section\">\n    <div *ngIf=\"!imageContainer.imageData\" class=\"button\" (click)=\"openCamera()\">\n      <div class=\"button-text\">{{ uploadText }}</div>\n    </div>\n    <div *ngIf=\"imageContainer.imageData\" class=\"button\" (click)=\"completeStep()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div *ngIf=\"imageContainer.imageData\" class=\"button button-outline\" (click)=\"retake()\">\n      <div class=\"button-text\">Retake</div>\n    </div>\n    <div class=\"button button-outline\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n  <div *ngIf=\"loading\" class=\"button-section\">\n    <div *ngIf=\"!imageContainer.imageData\" class=\"button button-disabled\">\n      <div class=\"button-text\">{{ uploadText }}</div>\n    </div>\n    <div *ngIf=\"imageContainer.imageData\" class=\"button button-disabled\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div *ngIf=\"imageContainer.imageData\" class=\"button button-outline-disabled\">\n      <div class=\"button-text\">Retake</div>\n    </div>\n    <div class=\"button button-outline-disabled\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                styles: [".facial-authentication-ref-step{width:100%;height:100%;position:relative}.facial-authentication-ref-step .message-content{position:absolute;top:0;left:0;bottom:50px;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px;box-sizing:border-box}.facial-authentication-ref-step .image-content{position:absolute;top:0;left:0;bottom:50px;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px;box-sizing:border-box;text-align:center}.facial-authentication-ref-step .image-content img{max-width:100%;max-height:100%}.facial-authentication-ref-step .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.facial-authentication-ref-step .button-section .button{width:100px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.facial-authentication-ref-step .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.facial-authentication-ref-step .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.facial-authentication-ref-step .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b;cursor:default}.facial-authentication-ref-step .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b;cursor:default}"]
            }] }
];
/** @nocollapse */
FacialAuthenticationRefComponent.ctorParameters = () => [
    { type: ProjectService }
];
FacialAuthenticationRefComponent.propDecorators = {
    _stepInstance: [{ type: Input }],
    userId: [{ type: Input }],
    uploadText: [{ type: Input }],
    onOpenCameraListener: [{ type: Output }],
    stepCompletedListener: [{ type: Output }],
    cancelListener: [{ type: Output }],
    stepInstance: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FacialAuthenticationComponent {
    /**
     * @param {?} projectService
     */
    constructor(projectService) {
        this.projectService = projectService;
        this.onOpenCameraListener = new EventEmitter();
        this.stepCompletedListener = new EventEmitter();
        this.cancelListener = new EventEmitter();
        this.loading = false;
        this.imageContainer = {
            imageData: null,
            /**
             * @param {?} image
             * @return {?}
             */
            setImage(image) {
                this.imageData = image;
            }
        };
    }
    /**
     * @param {?} s
     * @return {?}
     */
    set stepInstance(s) {
        this._stepInstance = s;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (!this.uploadText) {
            this.uploadText = "Open camera";
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @return {?}
     */
    openCamera() {
        this.onOpenCameraListener.emit(this.imageContainer);
    }
    /**
     * @return {?}
     */
    retake() {
        this.imageContainer.imageData = null;
        this.onOpenCameraListener.emit(this.imageContainer);
    }
    /**
     * @return {?}
     */
    completeStep() {
        this.loading = true;
        this.projectService.facialAuthentication(this._stepInstance.id, this.imageContainer.imageData).then((/**
         * @param {?} imageUpdateResult
         * @return {?}
         */
        imageUpdateResult => {
            this.projectService.completeStep(this._stepInstance.id, this.userId).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                this.loading = false;
                this.stepCompletedListener.emit({
                    value: this._stepInstance
                });
            })).catch((/**
             * @param {?} error
             * @return {?}
             */
            error => {
                this.loading = false;
                this.stepCompletedListener.emit({
                    value: this._stepInstance
                });
            }));
        }));
    }
    /**
     * @return {?}
     */
    cancel() {
        this.cancelListener.emit({});
    }
}
FacialAuthenticationComponent.decorators = [
    { type: Component, args: [{
                selector: 'facial-authentication',
                template: "<div class=\"facial-authentication-step\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div *ngIf=\"!imageContainer.imageData\" class=\"message-content\">\n    Please provide an image that will be used for facial authentication.\n  </div>\n  <div *ngIf=\"imageContainer.imageData\" class=\"image-content\">\n    <img src=\"{{ 'data:image/jpeg;base64,' + imageContainer.imageData }}\">\n  </div>\n\n  <div *ngIf=\"!loading\" class=\"button-section\">\n    <div *ngIf=\"!imageContainer.imageData\" class=\"button\" (click)=\"openCamera()\">\n      <div class=\"button-text\">{{ uploadText }}</div>\n    </div>\n    <div *ngIf=\"imageContainer.imageData\" class=\"button\" (click)=\"completeStep()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div *ngIf=\"imageContainer.imageData\" class=\"button button-outline\" (click)=\"retake()\">\n      <div class=\"button-text\">Retake</div>\n    </div>\n    <div class=\"button button-outline\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n  <div *ngIf=\"loading\" class=\"button-section\">\n    <div *ngIf=\"!imageContainer.imageData\" class=\"button button-disabled\">\n      <div class=\"button-text\">{{ uploadText }}</div>\n    </div>\n    <div *ngIf=\"imageContainer.imageData\" class=\"button button-disabled\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div *ngIf=\"imageContainer.imageData\" class=\"button button-outline-disabled\">\n      <div class=\"button-text\">Retake</div>\n    </div>\n    <div class=\"button button-outline-disabled\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                styles: [".facial-authentication-step{width:100%;height:100%;position:relative}.facial-authentication-step .message-content{position:absolute;top:0;left:0;bottom:50px;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px;box-sizing:border-box}.facial-authentication-step .image-content{position:absolute;top:0;left:0;bottom:50px;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px;box-sizing:border-box;text-align:center}.facial-authentication-step .image-content img{max-width:100%;max-height:100%}.facial-authentication-step .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.facial-authentication-step .button-section .button{width:100px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.facial-authentication-step .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.facial-authentication-step .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.facial-authentication-step .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b;cursor:default}.facial-authentication-step .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b;cursor:default}"]
            }] }
];
/** @nocollapse */
FacialAuthenticationComponent.ctorParameters = () => [
    { type: ProjectService }
];
FacialAuthenticationComponent.propDecorators = {
    _stepInstance: [{ type: Input }],
    userId: [{ type: Input }],
    uploadText: [{ type: Input }],
    onOpenCameraListener: [{ type: Output }],
    stepCompletedListener: [{ type: Output }],
    cancelListener: [{ type: Output }],
    stepInstance: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CodeReaderComponent {
    /**
     * @param {?} projectService
     */
    constructor(projectService) {
        this.projectService = projectService;
        this.onReadCode = new EventEmitter();
        this.stepCompletedListener = new EventEmitter();
        this.cancelListener = new EventEmitter();
        this.loading = false;
        this.description = "Code";
        this.variableName = "variableName";
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.loading = true;
        this.projectService.getStepParameters(this.stepInstance.step.id).then((/**
         * @param {?} result
         * @return {?}
         */
        result => {
            this.loading = false;
            this.description = this.stepInstance.step.parameters["0"] && this.stepInstance.step.parameters["0"].parameterValue;
            this.variableName = this.stepInstance.step.parameters["1"] && this.stepInstance.step.parameters["1"].parameterValue;
        }));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
    }
    /**
     * @return {?}
     */
    readCode() {
        this.onReadCode.emit(this);
    }
    /**
     * @return {?}
     */
    completeStep() {
        this.loading = true;
        this.projectService.setProcessVariableValue(this.stepInstance.businessProcessInstance.id, this.variableName, this.code).then((/**
         * @param {?} imageUpdateResult
         * @return {?}
         */
        imageUpdateResult => {
            this.projectService.completeStep(this.stepInstance.id, this.userId).then((/**
             * @param {?} result
             * @return {?}
             */
            result => {
                this.loading = false;
                this.stepCompletedListener.emit({
                    value: this.stepInstance
                });
            })).catch((/**
             * @param {?} error
             * @return {?}
             */
            error => {
                this.loading = false;
                this.stepCompletedListener.emit({
                    value: this.stepInstance
                });
            }));
        }));
    }
    /**
     * @return {?}
     */
    cancel() {
        this.cancelListener.emit({});
    }
    /**
     * @param {?} value
     * @return {?}
     */
    setValue(value) {
        this.code = value;
    }
}
CodeReaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'code-reader',
                template: "<div class=\"code-reader-step\">\n  <loading-indicator [show]=\"loading\"></loading-indicator>\n  <div *ngIf=\"!loading\" class=\"content\">\n    <div class=\"component-row\">\n      <div class=\"component-col\">\n        <div *ngIf=\"!description\" class=\"component-label\">\n          Value\n        </div>\n        <div *ngIf=\"description\" class=\"component-label\">\n          {{ description }}\n        </div>\n        <div class=\"component\">\n          <input type=\"text\" [(ngModel)]=\"code\" readonly>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div *ngIf=\"!loading\" class=\"button-section\">\n    <div class=\"button\" (click)=\"readCode()\">\n      <div class=\"button-text\">Scan</div>\n    </div>\n    <div class=\"button\" (click)=\"completeStep()\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div class=\"button button-outline\" (click)=\"cancel()\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n  <div *ngIf=\"loading\" class=\"button-section\">\n    <div class=\"button button-disabled\">\n      <div class=\"button-text\">Scan</div>\n    </div>\n    <div class=\"button button-disabled\">\n      <div class=\"button-text\">Continue</div>\n    </div>\n    <div class=\"button button-outline-disabled\">\n      <div class=\"button-text\">Cancel</div>\n    </div>\n  </div>\n</div>\n",
                styles: [".code-reader-step{width:100%;height:100%;position:relative}.code-reader-step .heading{position:absolute;top:0;width:100%;height:32px;font-size:16px;padding:8px}.code-reader-step .content{position:absolute;top:0;left:0;bottom:50px;width:100%;overflow-y:scroll;background-color:#f9f9f9;padding:8px;box-sizing:border-box}.code-reader-step .content .info-message{font-size:15px}.code-reader-step .content .component-row{width:100%}.code-reader-step .content .component-row .component-col{width:100%;display:inline-block;box-sizing:border-box;padding-top:10px}.code-reader-step .content .component-row .component-col .component-label{width:100%;font-size:15px;padding-bottom:5px}.code-reader-step .content .component-row .component-col .error-message{font-style:italic;font-size:11px;color:#f04141;padding-bottom:5px}.code-reader-step .content .component-row .component-col .component{width:100%}.code-reader-step .content .component-row .component-col .component input{width:100%;font-size:13px;border:1px solid #58666c;background-color:#fffcf6;box-sizing:border-box;padding:3px 5px;border-radius:5px;height:30px}.code-reader-step .content .component-row:first-child .component-col:first-child{padding-top:0}@media (min-width:600px){.code-reader-step .content .component-row .component-col{width:50%}.code-reader-step .content .component-row .component-col:first-child{padding-right:16px}.code-reader-step .content .component-row:first-child .component-col{padding-top:0}}.code-reader-step .button-section{position:absolute;left:0;bottom:0;height:50px;width:100%}.code-reader-step .button-section .button{width:70px;border:1px solid #2b4054;background-color:#2b4054;color:#fff;height:34px;border-radius:5px;float:right;margin-top:8px;display:table;cursor:pointer;margin-right:8px}.code-reader-step .button-section .button .button-text{display:table-cell;width:100%;height:100%;vertical-align:middle;text-align:center;font-size:15px}.code-reader-step .button-section .button-outline{border:1px solid #2b4054;background-color:#fff;color:#2b4054}.code-reader-step .button-section .button-disabled{border:1px solid #9b9b9b;background-color:#585858;color:#9b9b9b}.code-reader-step .button-section .button-outline-disabled{border:1px solid #9b9b9b;background-color:#fff;color:#9b9b9b}"]
            }] }
];
/** @nocollapse */
CodeReaderComponent.ctorParameters = () => [
    { type: ProjectService }
];
CodeReaderComponent.propDecorators = {
    stepInstance: [{ type: Input }],
    userId: [{ type: Input }],
    onReadCode: [{ type: Output }],
    stepCompletedListener: [{ type: Output }],
    cancelListener: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AcumenLibModule {
    /**
     * @param {?} acumenConfiguration
     * @return {?}
     */
    static forRoot(acumenConfiguration) {
        return {
            ngModule: AcumenLibModule,
            providers: [
                {
                    provide: AcumenConfiguration,
                    useValue: acumenConfiguration,
                },
            ],
        };
    }
}
AcumenLibModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    AcumenLibComponent,
                    InTrayComponent,
                    InTrayAlertComponent,
                    WizardComponent,
                    UserProjectsComponent,
                    UserProjects2Component,
                    StepInstanceComponent,
                    StepInstanceListComponent,
                    StepInstanceActiveComponent,
                    ProcessHistoryComponent,
                    LoadingIndicator,
                    ParticipantSearch,
                    MessageComponent,
                    CreateSystemUserComponent,
                    IdNrLookupComponent,
                    RegisterComponent,
                    UpdateDetailsComponent,
                    TextQuestionComponent,
                    CheckboxComponent,
                    RadioButtonComponent,
                    QuestionLineComponent,
                    QuestionnaireComponent,
                    SelectRole2Component,
                    WaitComponent,
                    FacialAuthenticationRefComponent,
                    FacialAuthenticationComponent,
                    CodeReaderComponent
                ],
                imports: [
                    FormsModule,
                    // BrowserModule,
                    CommonModule,
                    HttpClientModule
                ],
                exports: [
                    AcumenLibComponent,
                    InTrayComponent,
                    InTrayAlertComponent,
                    WizardComponent,
                    UserProjectsComponent,
                    UserProjects2Component,
                    StepInstanceComponent,
                    StepInstanceListComponent,
                    StepInstanceActiveComponent,
                    ProcessHistoryComponent
                ],
                providers: [
                    ProjectService,
                    MaintenanceService,
                    ActorService,
                    AnalysisService
                ]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { AcumenLibService, AcumenLibComponent, AcumenLibModule, InTrayComponent, InTrayAlertComponent, WizardComponent, UserProjectsComponent, UserProjects2Component, StepInstanceComponent, StepInstanceListComponent, StepInstanceActiveComponent, AcumenConfiguration, LoadingIndicator as ɵe, ParticipantSearch as ɵf, ProcessHistoryComponent as ɵd, ActorService as ɵb, AnalysisService as ɵc, MaintenanceService as ɵl, ProjectService as ɵa, CodeReaderComponent as ɵv, CreateSystemUserComponent as ɵh, FacialAuthenticationRefComponent as ɵt, FacialAuthenticationComponent as ɵu, IdNrLookupComponent as ɵi, MessageComponent as ɵg, CheckboxComponent as ɵn, QuestionLineComponent as ɵp, QuestionnaireComponent as ɵq, RadioButtonComponent as ɵo, TextQuestionComponent as ɵm, RegisterComponent as ɵj, SelectRole2Component as ɵr, UpdateDetailsComponent as ɵk, WaitComponent as ɵs };

//# sourceMappingURL=acumen-lib.js.map